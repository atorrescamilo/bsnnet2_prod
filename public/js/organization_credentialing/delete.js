$(function(){

    $('#myGrid').on('click','.btn-delete',function(e){
        e.preventDefault();
        var id=$(this).data('id');
        var form = $('#form');
        var url = form.attr('action').replace('ORGANIZATION_ID', id);
        var data = {'id': id};

        console.log(data)
        bootbox.dialog({
            message: 'Are you sure?',
            title: 'Delete',
            buttons: {
                success: {
                    label: '<i class="fas fa-ban"></i> Cancel',
                    className: 'btn-success',
                    callback: function() {

                    }
                },
                danger: {
                    label: '<i class="far fa-trash-alt"></i> Delete',
                    className: 'btn-danger',
                    callback: function() {
                        $.post(url, data, function (result) {
                           window.location.replace("http://127.0.0.1:8000/admin/facilities_credentialing/list");
                        })
                    }
                }
            }

        });

    });

});