/* ------------------------------------------------------------------------------
 *
 *  # Steps wizard
 *
 *  Demo JS code for form_wizard.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var FormWizard = function () {
    //
    // Setup module components
    //

    // Wizard
    var _componentWizard = function () {
        if (!$().steps) {
            console.warn('Warning - steps.min.js is not loaded.');
            return;
        }

        //
        // Wizard with validation
        //
        // Stop function if validation is missing
        if (!$().validate) {
            console.warn('Warning - validate.min.js is not loaded.');
            return;
        }

        // Show form
        var form = $('.steps-validation').show();

        // Initialize wizard
        $('.steps-validation').steps({
            headerTag: 'h6',
            bodyTag: 'fieldset',
            titleTemplate: '<span class="number">#index#</span> #title#',
            labels: {
                previous: '<i class="icon-arrow-left13 mr-2" /> Previous',
                next: 'Next <i class="icon-arrow-right14 ml-2" />',
                finish: 'Save  <i class="fas fa-save ml-2"></i>'
            },
            transitionEffect: 'fade',
            autoFocus: true,
            onStepChanging: function (event, currentIndex, newIndex) {

                // Allways allow previous action even if the current form is not valid!
                if (currentIndex > newIndex) {
                    return true;
                }

                var form = $('#form');
                $('#page').val(currentIndex);

                //first page save data via ajax
                if (currentIndex == 0) {
                    var npi=$('#npi_number').val()
                    var name=$('#name').val()
                    var email=$('#email').val()
                    var provider_type=$('#provider_type').val()

                    var valid=true

                    if(npi==""){
                        $('#error_npi_number').removeClass('sr-only')
                        valid=false
                    }else{
                        $('#error_npi_number').addClass('sr-only')

                        var url=$('#form_check_npi_exist').attr('action')

                        var data={
                            'npi': npi
                        }

                        var error_npi_exist=$('#error_npi_exist')
                        $.post(url, data, function (result) {
                            console.log(result.exist)
                           if(result.exist==1){
                               valid=false;
                               error_npi_exist.removeClass('sr-only')
                           }else{
                               error_npi_exist.addClass('sr-only')
                           }
                        })
                    }

                    if(name==""){
                        $('#error_name').removeClass('sr-only')
                        valid=false
                    }else{
                        $('#error_name').addClass('sr-only')
                    }

                    if(email==""){
                        $('#error_email').removeClass('sr-only')
                        valid=false
                    }else{
                        $('#error_email').addClass('sr-only')
                    }
                    if(provider_type=="0"){
                        $('#error_provider_type').removeClass('sr-only')
                        valid=false
                    }else{
                        $('#error_provider_type').addClass('sr-only')
                    }

                    if(valid==false){
                        return false
                    }else{
                        return true
                    }
                }
                if (currentIndex == 1) {
                    var valid=true

                    var p1_1= $('#p1_1')
                    var p1_2= $('#p1_2')
                    var p1_3= $('#p1_3')

                    var p2_1= $('#p2_1')
                    var p2_2= $('#p2_2')
                    var p2_3= $('#p2_3')

                    var p3_1= $('#p3_1')
                    var p3_2= $('#p3_2')
                    var p3_3= $('#p3_3')

                    var p4_1= $('#p4_1')
                    var p4_2= $('#p4_2')
                    var p4_3= $('#p4_3')

                    var p5_1= $('#p5_1')
                    var p5_2= $('#p5_2')
                    var p5_3= $('#p5_3')

                    var error_p1=$('#error_p1')
                    var error_p2=$('#error_p2')
                    var error_p3=$('#error_p3')
                    var error_p4=$('#error_p4')
                    var error_p5=$('#error_p5')

                    if(!(p1_1.is(':checked') || p1_2.is(':checked') || p1_3.is(':checked')) ){
                        valid=false
                        error_p1.removeClass('sr-only')
                    }else{
                        error_p1.addClass('sr-only')
                    }
                    if(!(p2_1.is(':checked') || p2_2.is(':checked') || p2_3.is(':checked')) ){
                        valid=false
                        error_p2.removeClass('sr-only')
                    }else{
                        error_p2.addClass('sr-only')
                    }
                    if(!(p3_1.is(':checked') || p3_2.is(':checked') || p3_3.is(':checked')) ){
                        valid=false
                        error_p3.removeClass('sr-only')
                    }else{
                        error_p3.addClass('sr-only')
                    }
                    if(!(p4_1.is(':checked') || p4_2.is(':checked') || p4_3.is(':checked')) ){
                        valid=false
                        error_p4.removeClass('sr-only')
                    }else{
                        error_p4.addClass('sr-only')
                    }
                    if(!(p5_1.is(':checked') || p5_2.is(':checked') || p5_3.is(':checked')) ){
                        valid=false
                        error_p5.removeClass('sr-only')
                    }else{
                        error_p5.addClass('sr-only')
                    }

                    if(valid==true){
                        return true
                    }else{
                        return false
                    }
                }

                if (currentIndex == 2) {

                }


                // Needed in some cases if the user went back (clean up)
                if (currentIndex < newIndex) {

                    // To remove error styles
                    form.find('.body:eq(' + newIndex + ') label.error').remove();
                    form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
                }

                form.validate().settings.ignore = ':disabled,:hidden';
                return form.valid();
            },
            onFinishing: function (event, currentIndex) {
                form.validate().settings.ignore = ':disabled';
                return form.valid();
            },
            onFinished: function (event, currentIndex) {

                var valid = true

                var cc1_a_1=$('#cc1_a_1'); var cc1_a_2=$('#cc1_a_2'); var cc1_a_3=$('#cc1_a_3')
                var cc1_b_1=$('#cc1_b_1'); var cc1_b_2=$('#cc1_b_2'); var cc1_b_3=$('#cc1_b_3')
                var cc1_c_1=$('#cc1_c_1'); var cc1_c_2=$('#cc1_c_2'); var cc1_c_3=$('#cc1_c_3')

                var cc2_a_1=$('#cc2_a_1'); var cc2_a_2=$('#cc2_a_2'); var cc2_a_3=$('#cc2_a_3')
                var cc2_b_1=$('#cc2_b_1'); var cc2_b_2=$('#cc2_b_2'); var cc2_b_3=$('#cc2_b_3')

                var cc3_a_1=$('#cc3_a_1'); var cc3_a_2=$('#cc3_a_2'); var cc3_a_3=$('#cc3_a_3')
                var cc3_b_1=$('#cc3_b_1'); var cc3_b_2=$('#cc3_b_2'); var cc3_b_3=$('#cc3_b_3')
                var cc3_c_1=$('#cc3_c_1'); var cc3_c_2=$('#cc3_c_2'); var cc3_c_3=$('#cc3_c_3')
                var cc3_d_1=$('#cc3_d_1'); var cc3_d_2=$('#cc3_d_2'); var cc3_d_3=$('#cc3_d_3')
                var cc3_e_1=$('#cc3_e_1'); var cc3_e_2=$('#cc3_e_2'); var cc3_e_3=$('#cc3_e_3')
                var cc3_f_1=$('#cc3_f_1'); var cc3_f_2=$('#cc3_f_2'); var cc3_f_3=$('#cc3_f_3')

                var cc4_a_1=$('#cc4_a_1'); var cc4_a_2=$('#cc4_a_2'); var cc4_a_3=$('#cc4_a_3')
                var cc4_b_1=$('#cc4_b_1'); var cc4_b_2=$('#cc4_b_2'); var cc4_b_3=$('#cc4_b_3')
                var cc4_c_1=$('#cc4_c_1'); var cc4_c_2=$('#cc4_c_2'); var cc4_c_3=$('#cc4_c_3')
                var cc4_d_1=$('#cc4_d_1'); var cc4_d_2=$('#cc4_d_2'); var cc4_d_3=$('#cc4_d_3')
                var cc4_e_1=$('#cc4_e_1'); var cc4_e_2=$('#cc4_e_2'); var cc4_e_3=$('#cc4_e_3')
                var cc4_f_1=$('#cc4_f_1'); var cc4_f_2=$('#cc4_f_2'); var cc4_f_3=$('#cc4_f_3')

                var cc5_a_1=$('#cc5_a_1'); var cc5_a_2=$('#cc5_a_2'); var cc5_a_3=$('#cc5_a_3')
                var cc5_b_1=$('#cc5_b_1'); var cc5_b_2=$('#cc5_b_2'); var cc5_b_3=$('#cc5_b_3')
                var cc5_c_1=$('#cc5_c_1'); var cc5_c_2=$('#cc5_c_2'); var cc5_c_3=$('#cc5_c_3')

                var cc6_a_1=$('#cc6_a_1'); var cc6_a_2=$('#cc6_a_2'); var cc6_a_3=$('#cc6_a_3')
                var cc6_b_1=$('#cc6_b_1'); var cc6_b_2=$('#cc6_b_2'); var cc6_b_3=$('#cc6_b_3')
                var cc6_c_1=$('#cc6_c_1'); var cc6_c_2=$('#cc6_c_2'); var cc6_c_3=$('#cc6_c_3')

                var cc7_a_1=$('#cc7_a_1'); var cc7_a_2=$('#cc7_a_2'); var cc7_a_3=$('#cc7_a_3')
                var cc7_b_1=$('#cc7_b_1'); var cc7_b_2=$('#cc7_b_2'); var cc7_b_3=$('#cc7_b_3')
                var cc7_c_1=$('#cc7_c_1'); var cc7_c_2=$('#cc7_c_2'); var cc7_c_3=$('#cc7_c_3')

                var error_cc1_a=$('#error_cc1_a')
                var error_cc1_b=$('#error_cc1_b')
                var error_cc1_c=$('#error_cc1_c')
                var error_cc2_a=$('#error_cc2_a')
                var error_cc2_b=$('#error_cc2_b')
                var error_cc3_a=$('#error_cc3_a')
                var error_cc3_b=$('#error_cc3_b')
                var error_cc3_c=$('#error_cc3_c')
                var error_cc3_d=$('#error_cc3_d')
                var error_cc3_e=$('#error_cc3_e')
                var error_cc3_f=$('#error_cc3_f')
                var error_cc4_a=$('#error_cc4_a')
                var error_cc4_b=$('#error_cc4_b')
                var error_cc4_c=$('#error_cc4_c')
                var error_cc4_d=$('#error_cc4_d')
                var error_cc4_e=$('#error_cc4_e')
                var error_cc4_f=$('#error_cc4_f')
                var error_cc5_a=$('#error_cc5_a')
                var error_cc5_b=$('#error_cc5_b')
                var error_cc5_c=$('#error_cc5_c')
                var error_cc6_a=$('#error_cc6_a')
                var error_cc6_b=$('#error_cc6_b')
                var error_cc6_c=$('#error_cc6_c')
                var error_cc7_a=$('#error_cc7_a')
                var error_cc7_b=$('#error_cc7_b')
                var error_cc7_c=$('#error_cc7_c')


                if(!(cc1_a_1.is(':checked') || cc1_a_2.is(':checked') || cc1_a_3.is(':checked')) ){ valid=false; error_cc1_a.removeClass('sr-only') } else{
                    error_cc1_a.addClass('sr-only')
                }
                if(!(cc1_b_1.is(':checked') || cc1_b_2.is(':checked') || cc1_b_3.is(':checked')) ){ valid=false; error_cc1_b.removeClass('sr-only') } else{
                    error_cc1_b.addClass('sr-only')
                }
                if(!(cc1_c_1.is(':checked') || cc1_c_2.is(':checked') || cc1_c_3.is(':checked')) ){ valid=false; error_cc1_c.removeClass('sr-only') } else{
                    error_cc1_c.addClass('sr-only')
                }
                if(!(cc2_a_1.is(':checked') || cc2_a_2.is(':checked') || cc2_a_3.is(':checked')) ){ valid=false; error_cc2_a.removeClass('sr-only') } else{
                    error_cc2_a.addClass('sr-only')
                }
                if(!(cc2_b_1.is(':checked') || cc2_b_2.is(':checked') || cc2_b_3.is(':checked')) ){ valid=false; error_cc2_b.removeClass('sr-only') } else{
                    error_cc2_b.addClass('sr-only')
                }
                if(!(cc3_a_1.is(':checked') || cc3_a_2.is(':checked') || cc3_a_3.is(':checked')) ){ valid=false; error_cc3_a.removeClass('sr-only') } else{
                    error_cc3_a.addClass('sr-only')
                }
                if(!(cc3_b_1.is(':checked') || cc3_b_2.is(':checked') || cc3_b_3.is(':checked')) ){ valid=false; error_cc3_b.removeClass('sr-only') } else{
                    error_cc3_b.addClass('sr-only')
                }
                if(!(cc3_c_1.is(':checked') || cc3_c_2.is(':checked') || cc3_c_3.is(':checked')) ){ valid=false; error_cc3_c.removeClass('sr-only') } else{
                    error_cc3_c.addClass('sr-only')
                }
                if(!(cc3_d_1.is(':checked') || cc3_d_2.is(':checked') || cc3_d_3.is(':checked')) ){ valid=false; error_cc3_d.removeClass('sr-only') } else{
                    error_cc3_d.addClass('sr-only')
                }
                if(!(cc3_e_1.is(':checked') || cc3_e_2.is(':checked') || cc3_e_3.is(':checked')) ){ valid=false; error_cc3_e.removeClass('sr-only') } else{
                    error_cc3_e.addClass('sr-only')
                }
                if(!(cc3_f_1.is(':checked') || cc3_f_2.is(':checked') || cc3_f_3.is(':checked')) ){ valid=false; error_cc3_f.removeClass('sr-only') } else{
                    error_cc3_f.addClass('sr-only')
                }
                if(!(cc4_a_1.is(':checked') || cc4_a_2.is(':checked') || cc4_a_3.is(':checked')) ){ valid=false; error_cc4_a.removeClass('sr-only') } else{
                    error_cc4_a.addClass('sr-only')
                }
                if(!(cc4_b_1.is(':checked') || cc4_b_2.is(':checked') || cc4_b_3.is(':checked')) ){ valid=false; error_cc4_b.removeClass('sr-only') } else{
                    error_cc4_b.addClass('sr-only')
                }
                if(!(cc4_c_1.is(':checked') || cc4_c_2.is(':checked') || cc4_c_3.is(':checked')) ){ valid=false; error_cc4_c.removeClass('sr-only') } else{
                    error_cc4_c.addClass('sr-only')
                }
                if(!(cc4_d_1.is(':checked') || cc4_d_2.is(':checked') || cc4_d_3.is(':checked')) ){ valid=false; error_cc4_d.removeClass('sr-only') } else{
                    error_cc4_d.addClass('sr-only')
                }
                if(!(cc4_e_1.is(':checked') || cc4_e_2.is(':checked') || cc4_e_3.is(':checked')) ){ valid=false; error_cc4_e.removeClass('sr-only') } else{
                    error_cc4_e.addClass('sr-only')
                }
                if(!(cc4_f_1.is(':checked') || cc4_f_2.is(':checked') || cc4_f_3.is(':checked')) ){ valid=false; error_cc4_f.removeClass('sr-only') } else{
                    error_cc4_f.addClass('sr-only')
                }
                if(!(cc5_a_1.is(':checked') || cc5_a_2.is(':checked') || cc5_a_3.is(':checked')) ){ valid=false; error_cc5_a.removeClass('sr-only') } else{
                    error_cc5_a.addClass('sr-only')
                }
                if(!(cc5_b_1.is(':checked') || cc5_b_2.is(':checked') || cc5_b_3.is(':checked')) ){ valid=false; error_cc5_b.removeClass('sr-only') } else{
                    error_cc5_b.addClass('sr-only')
                }
                if(!(cc5_c_1.is(':checked') || cc5_c_2.is(':checked') || cc5_c_3.is(':checked')) ){ valid=false; error_cc5_c.removeClass('sr-only') } else{
                    error_cc5_c.addClass('sr-only')
                }
                if(!(cc6_a_1.is(':checked') || cc6_a_2.is(':checked') || cc6_a_3.is(':checked')) ){ valid=false; error_cc6_a.removeClass('sr-only') } else{
                    error_cc6_a.addClass('sr-only')
                }
                if(!(cc6_b_1.is(':checked') || cc6_b_2.is(':checked') || cc6_b_3.is(':checked')) ){ valid=false; error_cc6_b.removeClass('sr-only') } else{
                    error_cc6_b.addClass('sr-only')
                }
                if(!(cc6_c_1.is(':checked') || cc6_c_2.is(':checked') || cc6_c_3.is(':checked')) ){ valid=false; error_cc6_c.removeClass('sr-only') } else{
                    error_cc6_c.addClass('sr-only')
                }
                if(!(cc7_a_1.is(':checked') || cc7_a_2.is(':checked') || cc7_a_3.is(':checked')) ){ valid=false; error_cc7_a.removeClass('sr-only') } else{
                    error_cc7_a.addClass('sr-only')
                }
                if(!(cc7_b_1.is(':checked') || cc7_b_2.is(':checked') || cc7_b_3.is(':checked')) ){ valid=false; error_cc7_b.removeClass('sr-only') } else{
                    error_cc7_b.addClass('sr-only')
                }
                if(!(cc7_c_1.is(':checked') || cc7_c_2.is(':checked') || cc7_c_3.is(':checked')) ){ valid=false; error_cc7_c.removeClass('sr-only') } else{
                    error_cc7_c.addClass('sr-only')
                }

                if(valid==true){
                    $('#form_save_survey').submit()
                }else{
                    $('#modal_warning').modal('show')
                }

            }
        });

        // Initialize validation
        $('.steps-validation').validate({
            ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
            errorClass: 'validation-invalid-label',
            highlight: function (element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function (element, errorClass) {
                $(element).removeClass(errorClass);
            },

            // Different components require proper error label placement
            errorPlacement: function (error, element) {

                // Unstyled checkboxes, radios
                if (element.parents().hasClass('form-check')) {
                    error.appendTo(element.parents('.form-check').parent());
                }

                // Input with icons and Select2
                else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {
                    error.appendTo(element.parent());
                }

                // Input group, styled file input
                else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {
                    error.appendTo(element.parent().parent());
                }

                // Other elements
                else {
                    error.insertAfter(element);
                }
            },
            rules: {
                email: {
                    email: true
                }
            }
        });
    };

    // Uniform
    var _componentUniform = function () {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.form-input-styled').uniform({
            fileButtonClass: 'action btn bg-blue'
        });
    };

    // Select2 select
    var _componentSelect2 = function () {
        if (!$().select2) {
            console.warn('Warning - select2.min.js is not loaded.');
            return;
        }

        // Initialize
        var $select = $('.form-control-select2').select2({
            minimumResultsForSearch: Infinity,
            width: '100%'
        });

        // Trigger value change when selection is made
        $select.on('change', function () {
            $(this).trigger('blur');
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _componentWizard();
            _componentUniform();
            _componentSelect2();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function () {
    FormWizard.init();
});