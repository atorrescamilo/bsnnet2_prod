/* ------------------------------------------------------------------------------
 *
 *  # Steps wizard
 *
 *  Demo JS code for form_wizard.html page
 *
 * ---------------------------------------------------------------------------- */

// Setup module
// ------------------------------

var FormWizard = function() {
    //
    // Setup module components
    //

    // Wizard
    var _componentWizard = function() {
        if (!$().steps) {
            console.warn('Warning - steps.min.js is not loaded.');
            return;
        }

        // Enable all steps and make them clickable
        $('.steps-enable-all').steps({
            headerTag: 'h6',
            bodyTag: 'fieldset',
            transitionEffect: 'fade',
            enableAllSteps: true,
            titleTemplate: '<span class="number">#index#</span> #title#',
            labels: {
                previous: '<i class="icon-arrow-left13 mr-2" /> Previous',
                next: 'Next <i class="icon-arrow-right14 ml-2" />',
                finish: 'Submit form <i class="far fa-save ml-2"></i>'
            },
            onStepChanging: function (event, currentIndex, newIndex) {
                if (currentIndex > newIndex) {
                    return true;
                }

                if (currentIndex == 0) {

                    var next=true
                    var name=$('#name').val();
                    var error_name=$('#error_name');

                    if(name==""){
                        error_name.removeClass('sr-only')
                        next=false
                    }else{
                        error_name.addClass('sr-only')
                    }

                    var accreditationsSelected = "";
                    var accreditations = $('input[name=accreditations]')

                    accreditations.each(function (index, $this) {
                        var accreditation = $(this);
                        if (accreditation.is(':checked')) {
                            accreditationsSelected += accreditation.val() + ","
                        }
                    })

                    var error_accreditation=$('#error_accreditation')
                    if(accreditationsSelected==""){
                        next=false
                        error_accreditation.removeClass('sr-only')
                    }else{
                        error_accreditation.addClass('sr-only')
                    }

                    var npi=$('#npi').val();
                    var error_npi=$('#error_npi');
                    if(npi==""){
                        next=false
                        error_npi.removeClass('sr-only')
                    }else{
                        error_npi.addClass('sr-only')
                    }

                    var billing_npi=$('#billing_npi').val();
                    var error_billing_npi=$('#error_billing_npi')
                    if(billing_npi==""){
                        next=false
                        error_billing_npi.removeClass('sr-only')
                    }else{
                        error_billing_npi.addClass('sr-only')
                    }

                    var billing_phone=$('#billing_phone').val();
                    var error_billing_phone=$('#error_billing_phone')
                    if(billing_phone==""){
                        next=false
                        error_billing_phone.removeClass('sr-only')
                    }else{
                        error_billing_phone.addClass('sr-only')
                    }

                    var tin_number=$('#tin_number').val()
                    var error_tin_number=$('#error_tin_number')
                    if(tin_number==""){
                        next=false
                        error_tin_number.removeClass('sr-only')
                    }else{
                        error_tin_number.addClass('sr-only')
                    }

                    var lega_name_tin_owner=$('#lega_name_tin_owner').val()
                    var error_lega_name_tin_owner=$('#error_lega_name_tin_owner')

                    if(lega_name_tin_owner==""){
                        next=false
                        error_lega_name_tin_owner.removeClass('sr-only')
                    }else{
                        error_lega_name_tin_owner.addClass('sr-only')
                    }

                    var phone=$('#phone').val()
                    var error_phone=$('#error_phone')
                    if(phone==""){
                        next=false
                        error_phone.removeClass('sr-only')
                    }else{
                        error_phone.addClass('sr-only')
                    }

                    var american_sign_language_yes=$('#american_sign_language_yes')
                    var american_sign_language_no=$('#american_sign_language_no')

                    var american_sign_language=$('#error_american_sign_language')

                    if(american_sign_language_yes.is(':checked') || american_sign_language_no.is(':checked')){
                        american_sign_language.addClass('sr-only')
                    }else{
                        next=false
                        american_sign_language.removeClass('sr-only')
                    }

                    return next
                }
                if (currentIndex == 1) {
                    return true
                }
            },
            onFinished: function (event, currentIndex) {
                var form = $('#form');

                var accreditationsSelected = "";
                var accreditations = $('input[name=accreditations]');

                accreditations.each(function (index, $this) {
                    var accreditation = $(this);

                    if (accreditation.is(':checked')) {
                        accreditationsSelected += accreditation.val() + ",";
                    }
                });

                $('#accreditationSelected').val(accreditationsSelected)
                var clasificationSelected = "";
                var clasifications = $('input[name=clasifications]');

                clasifications.each(function (index, $this) {
                    var clasification = $(this);

                    if (clasification.is(':checked')) {
                        clasificationSelected += clasification.val() + ",";
                    }
                });

                $('#clasificationSelected').val(clasificationSelected)

                var specialtiesSelected = "";
                var specialties = $('input[name=specialties]');

                specialties.each(function (index, $this) {
                    var specialty = $(this);

                    if (specialty.is(':checked')) {
                        specialtiesSelected += specialty.val() + ",";
                    }
                });

                $('#specialtiesSelected').val(specialtiesSelected)


                form.submit()
            }
        });

    };

    // Uniform
    var _componentUniform = function() {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.form-input-styled').uniform({
            fileButtonClass: 'action btn bg-blue'
        });
    };

    // Select2 select
    var _componentSelect2 = function() {
        if (!$().select2) {
            console.warn('Warning - select2.min.js is not loaded.');
            return;
        }

        // Initialize
        var $select = $('.form-control-select2').select2({
            minimumResultsForSearch: Infinity,
            width: '100%'
        });

        // Trigger value change when selection is made
        $select.on('change', function() {
            $(this).trigger('blur');
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _componentWizard();
            _componentUniform();
            _componentSelect2();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    FormWizard.init();
});