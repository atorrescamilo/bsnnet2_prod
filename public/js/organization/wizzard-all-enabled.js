/* ------------------------------------------------------------------------------
 *
 *  # Steps wizard
 *
 *  Demo JS code for form_wizard.html page
 *
 * ---------------------------------------------------------------------------- */

// Setup module
// ------------------------------

var FormWizard = function() {
    //
    // Setup module components
    //

    // Wizard
    var _componentWizard = function() {
        if (!$().steps) {
            console.warn('Warning - steps.min.js is not loaded.');
            return;
        }

        // Enable all steps and make them clickable
        $('.steps-enable-all').steps({
            headerTag: 'h6',
            bodyTag: 'fieldset',
            transitionEffect: 'fade',
            enableAllSteps: true,
            titleTemplate: '<span class="number">#index#</span> #title#',
            labels: {
                previous: '<i class="icon-arrow-left13 mr-2" /> Previous',
                next: 'Next <i class="icon-arrow-right14 ml-2" />',
                finish: 'Submit form <i class="far fa-save ml-2"></i>'
            },
            onFinished: function (event, currentIndex) {

                var form = $('#form');

                var accreditationsSelected = "";
                var accreditations = $('input[name=accreditations]');

                accreditations.each(function (index, $this) {
                    var accreditation = $(this);

                    if (accreditation.is(':checked')) {
                        accreditationsSelected += accreditation.val() + ",";
                    }
                });

                $('#accreditationSelected').val(accreditationsSelected)

                var clasificationSelected = "";
                var clasifications = $('input[name=clasifications]');

                clasifications.each(function (index, $this) {
                    var clasification = $(this);

                    if (clasification.is(':checked')) {
                        clasificationSelected += clasification.val() + ",";
                    }
                });

                $('#clasificationSelected').val(clasificationSelected)

                var specialtiesSelected = "";
                var specialties = $('input[name=specialties]');

                specialties.each(function (index, $this) {
                    var specialty = $(this);

                    if (specialty.is(':checked')) {
                        specialtiesSelected += specialty.val() + ",";
                    }
                });

                $('#specialtiesSelected').val(specialtiesSelected)

                var ratesSelected = "";
                var ratesValueSelected="";
                var rates = $('input[name=rates]');

                rates.each(function (index, $this) {
                    var rate = $(this);
                    var rateValue=""
                    if (rate.is(':checked')) {
                        ratesSelected += rate.val() + ",";
                        rateValue=$('#rate_value_'+rate.val()).val()
                    }else{
                        rateValue+="0"
                    }
                    ratesValueSelected+=rateValue+",";
                });

                $('#ratesSelected').val(ratesSelected)
                $('#ratesValueSelected').val(ratesValueSelected)

                var data = form.serialize()
                var url = form.attr('action')

                form.submit();
                /*
                $.post(url, data, function (result) {

                })

                $('#progress_upload').removeClass('sr-only');
                var data2 = new FormData(form[0]);

                $.ajax({
                    type: 'POST',
                    url: url,
                    data: data2,
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    cache: false,
                    success: function (response) {
                        var errorTest=response.errorTest

                        uploadError = response.uploadError;
                        $('#progress_upload').addClass('sr-only');

                        if ($('#provider_agreement_file').val() != "") {
                            if (uploadError[0] == 1) {
                                $('#provider_agreement_file_1').removeClass('sr-only');
                            } else {
                                $('#provider_agreement_file_0').removeClass('sr-only');
                            }
                        }
                        if ($('#state_licence_file').val() != "") {
                            if (uploadError[1] == 1) {
                                $('#state_licence_file_1').removeClass('sr-only');
                            } else {
                                $('#state_licence_file_0').removeClass('sr-only');
                            }
                        }

                        if ($('#provider_data_form_file').val() != "") {
                            if (uploadError[2] == 1) {
                                $('#provider_data_form_file_1').removeClass('sr-only');
                            } else {
                                $('#provider_data_form_file_0').removeClass('sr-only');
                            }
                        }

                        if ($('#w9_file').val() != "") {
                            if (uploadError[3] == 1) {
                                $('#w9_file_1').removeClass('sr-only');
                            } else {
                                $('#w9_file_0').removeClass('sr-only');
                            }
                        }

                        if ($('#roster_file').val() != "") {
                            if (uploadError[4] == 1) {
                                $('#roster_file_1').removeClass('sr-only');
                            } else {
                                $('#roster_file_0').removeClass('sr-only');
                            }
                        }

                        if ($('#general_liability_coverage_file').val() != "") {
                            if (uploadError[5] == 1) {
                                $('#general_liability_coverage_file_1').removeClass('sr-only');
                            } else {
                                $('#general_liability_coverage_file_0').removeClass('sr-only');
                            }
                        }

                        if ($('#site_visit_survery_file').val() != "") {
                            if (uploadError[6] == 1) {
                                $('#site_visit_survery_file_1').removeClass('sr-only');
                            } else {
                                $('#site_visit_survery_file_0').removeClass('sr-only');
                            }
                        }

                        if ($('#accreditation_certificate_file').val() != "") {
                            if (uploadError[7] == 1) {
                                $('#accreditation_certificate_file_1').removeClass('sr-only');
                            } else {
                                $('#accreditation_certificate_file_0').removeClass('sr-only');
                            }
                        }

                        if($('#loa_loi_file').val()!=""){
                            if (uploadError[8] == 1) {
                                $('#loa_loi_file_1').removeClass('sr-only');
                            } else {
                                $('#loa_loi_file_0').removeClass('sr-only');
                            }
                        }

                        $('#progress_upload').addClass('sr-only');
                    },
                    error: function (response, desc, err) {
                        if (response.responseJSON && response.responseJSON.message) {

                        } else {

                        }
                    }
                }); */

            }
        });

    };

    // Uniform
    var _componentUniform = function() {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.form-input-styled').uniform({
            fileButtonClass: 'action btn bg-blue'
        });
    };

    // Select2 select
    var _componentSelect2 = function() {
        if (!$().select2) {
            console.warn('Warning - select2.min.js is not loaded.');
            return;
        }

        // Initialize
        var $select = $('.form-control-select2').select2({
            minimumResultsForSearch: Infinity,
            width: '100%'
        });

        // Trigger value change when selection is made
        $select.on('change', function() {
            $(this).trigger('blur');
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function() {
            _componentWizard();
            _componentUniform();
            _componentSelect2();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function() {
    FormWizard.init();
});