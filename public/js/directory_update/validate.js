$(function(){
    $('.btn-save').click(function(e){
        e.preventDefault()

        //validating fields
        var date=$('#date').val()
        var error_date=$('#error_date')

        var send=true
        if(date==""){
            send=false
            error_date.removeClass('sr-only')
        }else{
            error_date.addClass('sr-only')
        }

        var organization_name=$('#organization_name').val()
        var error_organization_name=$('#error_organization_name')

        if(organization_name==""){
            send=false
            error_organization_name.removeClass('sr-only')
        }else {
            error_organization_name.addClass('sr-only')
        }

        var authorized_representative_name=$('#authorized_representative_name').val()
        var error_authorized_representative_name=$('#error_authorized_representative_name')

        if(authorized_representative_name==""){
            send=false
            error_authorized_representative_name.removeClass('sr-only')
        }else {
            error_authorized_representative_name.addClass('sr-only')
        }

        var authorized_representative_phone=$('#authorized_representative_phone').val()
        var error_authorized_representative_phone=$('#error_authorized_representative_phone')

        if(authorized_representative_phone==""){
            send=false
            error_authorized_representative_phone.removeClass('sr-only')
        }else {
            error_authorized_representative_phone.addClass('sr-only')
        }

        var authorized_representative_email=$('#authorized_representative_email').val()
        var error_authorized_representative_email=$('#error_authorized_representative_email')

        if(authorized_representative_email==""){
            send=false
            error_authorized_representative_email.removeClass('sr-only')
        }else {
            error_authorized_representative_email.addClass('sr-only')
        }

        var provider_name_correct=false

        if($('#provider_name_correct').is(':checked')){
            provider_name_correct=true
        }


        if(send){
            $('#form').submit()
        }


    })
})
