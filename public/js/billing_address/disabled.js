$(function(){

    $('#grid_address').on('click','.btn-disabled-address',function(e){
        e.preventDefault();
        var id=$(this).data('id');
        Disabled(0,id)
    });

    $('#grid_address').on('click','.btn-enable-address',function(e){
        e.preventDefault();
        var id=$(this).data('id');
        Disabled(1,id)
    });

    function Disabled(action,id){

        var form = $('#form-disabled-addr');
        var url = form.attr('action');
        var data = {
            'id': id,
            'action': action
        };

        var title=""
        var btn_disabled=""
        if(action==0){
            title="Disable the Billing Address"
            btn_disabled="Disabled"
        }else{
            title="Enable the Billing Address"
            btn_disabled="Enable"
        }

        bootbox.dialog({
            message: 'Are you sure?',
            title: title,
            buttons: {
                success: {
                    label: '<i class="fas fa-ban"></i> Cancel',
                    className: 'btn-danger',
                    callback: function() {

                    }
                },
                danger: {
                    label: '<i class="fas fa-check"></i> '+btn_disabled,
                    className: 'btn-success',
                    callback: function() {
                        $.post(url, data, function (result) {
                            var disabled=result.disabled;

                            if(disabled==1){
                                var selectedData = gridAddressOptions.api.getSelectedRows();
                                var res = gridAddressOptions.api.updateRowData({remove: selectedData});
                            }
                        })
                    }
                }
            }

        });
    }

});