$(function(){

    $('.bnt-send').click(function(e){
        e.preventDefault();

        $('#new').val(0);

        if(validateForm()){
            $('#form').submit();
        }
    })


    $('.bnt-send-new').click(function(e){

        e.preventDefault();
        $('#new').val(1);

        if(validateForm()){
            $('#form').submit();
        }

    })


    function  validateForm() {

        var bestSelected=""
        var best_options = $('input[name=bests]')

        best_options.each(function (index, $this) {
            var best_option = $(this);

            if (best_option.is(':checked')) {
                bestSelected += best_option.val() + ",";
            }
        });

        $('#bestSelected').val(bestSelected)

        var send=true;

        var first_name=$('#first_name');
        var error_first_name=$('#error_first_name');

        if(first_name.val()==""){
            error_first_name.removeClass('sr-only')
            send=false;
        }else{
            error_first_name.addClass('sr-only')
        }

        var last_name=$('#last_name');
        var error_last_name=$('#error_last_name');

        if(last_name.val()==""){
            error_last_name.removeClass('sr-only')
            send=false;
        }else{
            error_last_name.addClass('sr-only')
        }

        return send;
    }





})