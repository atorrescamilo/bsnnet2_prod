/* ------------------------------------------------------------------------------
 *
 *  # Steps wizard
 *
 *  Demo JS code for form_wizard.html page
 *
 * ---------------------------------------------------------------------------- */


// Setup module
// ------------------------------

var FormWizard = function () {
    //
    // Setup module components
    //

    // Wizard
    var _componentWizard = function () {
        if (!$().steps) {
            console.warn('Warning - steps.min.js is not loaded.');
            return;
        }

        //
        // Wizard with validation
        //
        // Stop function if validation is missing
        if (!$().validate) {
            console.warn('Warning - validate.min.js is not loaded.');
            return;
        }

        // Show form
        var form = $('.steps-validation').show();

        // Initialize wizard
        $('.steps-validation').steps({
            headerTag: 'h6',
            bodyTag: 'fieldset',
            titleTemplate: '<span class="number">#index#</span> #title#',
            labels: {
                previous: '<i class="icon-arrow-left13 mr-2" /> Previous',
                next: 'Next <i class="icon-arrow-right14 ml-2" />',
                finish: 'Save  <i class="fas fa-save ml-2"></i>'
            },
            transitionEffect: 'fade',
            autoFocus: true,
            onStepChanging: function (event, currentIndex, newIndex) {

                // Allways allow previous action even if the current form is not valid!
                if (currentIndex > newIndex) {
                    return true;
                }

                var form = $('#form');
                $('#page').val(currentIndex);

                //first page save data via ajax
                if (currentIndex == 0) {
                   var check_main_info=$('#correct_info_nppes_yes')
                    if(!check_main_info.is(':checked')){
                        return  false
                    }else{
                        return true
                    }
                }
                if (currentIndex == 2) {
                    Captcha('mainCaptcha')
                }

                // Needed in some cases if the user went back (clean up)
                if (currentIndex < newIndex) {

                    // To remove error styles
                    form.find('.body:eq(' + newIndex + ') label.error').remove();
                    form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
                }

                form.validate().settings.ignore = ':disabled,:hidden';
                return form.valid();
            },
            onFinishing: function (event, currentIndex) {
                form.validate().settings.ignore = ':disabled';
                return form.valid();
            },
            onFinished: function (event, currentIndex) {

                var send=true
                var focused=false;

                var npi=$('#npi_number').val()
                $('#npi_number_1').val(npi)

                var name=$('#name')
                var error_name=$('#error_name')
                var contact_name=$('#contact_name')
                var error_contact_name=$('#error_contact_name')
                var contact_email=$('#contact_email')
                var error_contact_email=$('#error_contact_email')
                var error_files=$('#error_files')

                var textInput=$('#txtInput').val()
                var text_img_captcha=$('#text_img_captcha').val()
                var error_captcha=$('#error_captcha')
                text_img_captcha=text_img_captcha.split(' ').join('')

                var files=$('#files_join')

               if(name.val()==""){
                    error_name.removeClass('sr-only')
                    name.addClass('border-red')
                    send=false
                    if(focused==false){
                        name.focus()
                        focused=true
                    }
                }else{
                    error_name.addClass('sr-only')
                    name.removeClass('border-red')
                }

                if(contact_name.val()==""){
                    error_contact_name.removeClass('sr-only')
                    contact_name.addClass('border-red')
                    send=false
                    if(focused==false){
                        contact_name.focus()
                        focused=true
                    }
                }else{
                    error_contact_name.addClass('sr-only')
                    contact_name.removeClass('border-red')
                }

                if(contact_name.val()==""){
                    error_contact_name.removeClass('sr-only')
                    contact_name.addClass('border-red')
                    send=false
                    if(focused==false){
                        contact_name.focus()
                        focused=true
                    }
                }else{
                    error_contact_name.addClass('sr-only')
                    contact_name.removeClass('border-red')
                }

                if(contact_email.val()==""){
                    error_contact_email.removeClass('sr-only')
                    contact_email.addClass('border-red')
                    send=false
                    if(focused==false){
                        contact_email.focus()
                        focused=true
                    }
                }else{
                    error_contact_email.addClass('sr-only')
                    contact_email.removeClass('border-red')
                }
                if(files.val()==""){
                    error_files.removeClass('sr-only')
                    files.addClass('border-red')
                    send=false
                    if(focused==false){
                        files.focus()
                        focused=true
                    }
                }else{
                    error_files.addClass('sr-only')
                    files.removeClass('border-red')
                }
                if(textInput!=text_img_captcha){
                    send=false
                    error_captcha.removeClass('sr-only')
                }else{
                    error_captcha.addClass('sr-only')
                }

                var languagesSelected = "";
                var languages = $('input[name=languages]');
                languages.each(function (index, $this) {
                    var language = $(this);

                    if (language.is(':checked')) {
                        languagesSelected += language.val() + ",";
                    }
                });

                $('#languagesSelected').val(languagesSelected)

                if(send){
                    var form=$('#form')
                    form.submit()
                }
            }
        });

        // Initialize validation
        $('.steps-validation').validate({
            ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
            errorClass: 'validation-invalid-label',
            highlight: function (element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function (element, errorClass) {
                $(element).removeClass(errorClass);
            },

            // Different components require proper error label placement
            errorPlacement: function (error, element) {

                // Unstyled checkboxes, radios
                if (element.parents().hasClass('form-check')) {
                    error.appendTo(element.parents('.form-check').parent());
                }

                // Input with icons and Select2
                else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {
                    error.appendTo(element.parent());
                }

                // Input group, styled file input
                else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {
                    error.appendTo(element.parent().parent());
                }

                // Other elements
                else {
                    error.insertAfter(element);
                }
            },
            rules: {
                email: {
                    email: true
                }
            }
        });
    };

    // Uniform
    var _componentUniform = function () {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.form-input-styled').uniform({
            fileButtonClass: 'action btn bg-blue'
        });
    };

    // Select2 select
    var _componentSelect2 = function () {
        if (!$().select2) {
            console.warn('Warning - select2.min.js is not loaded.');
            return;
        }

        // Initialize
        var $select = $('.form-control-select2').select2({
            minimumResultsForSearch: Infinity,
            width: '100%'
        });

        // Trigger value change when selection is made
        $select.on('change', function () {
            $(this).trigger('blur');
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _componentWizard();
            _componentUniform();
            _componentSelect2();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function () {
    FormWizard.init();
});