/* ------------------------------------------------------------------------------
 *
 *  # Steps wizard
 *
 *  Demo JS code for form_wizard.html page
 *
 * ---------------------------------------------------------------------------- */

// Setup module
// ------------------------------

var FormWizard = function () {

    //
    // Setup module components
    //

    // Wizard
    var _componentWizard = function () {
        if (!$().steps) {
            console.warn('Warning - steps.min.js is not loaded.');
            return;
        }

        //
        // Wizard with validation
        //

        // Stop function if validation is missing
        if (!$().validate) {
            console.warn('Warning - validate.min.js is not loaded.');
            return;
        }

        // Show form
        var form = $('.steps-validation').show();


        // Initialize wizard
        $('.steps-validation').steps({
            headerTag: 'h6',
            bodyTag: 'fieldset',
            titleTemplate: '<span class="number">#index#</span> #title#',
            labels: {
                previous: '<i class="icon-arrow-left13 mr-2" /> Previous',
                next: 'Next <i class="icon-arrow-right14 ml-2" />',
                finish: 'Save  <i class="fas fa-save ml-2"></i>'
            },
            transitionEffect: 'fade',
            autoFocus: true,
            onStepChanging: function (event, currentIndex, newIndex) {

                // Allways allow previous action even if the current form is not valid!
                if (currentIndex > newIndex) {
                    return true;
                }

                // Needed in some cases if the user went back (clean up)
                if (currentIndex < newIndex) {

                    // To remove error styles
                    form.find('.body:eq(' + newIndex + ') label.error').remove();
                    form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
                }

                if (currentIndex == 0) {

                    var next =true

                    var name=$('#name').val()
                    var error_name=$('#error_name')

                    if(name==""){
                        next=false
                        error_name.removeClass('sr-only')
                    }else{
                        error_name.addClass('sr-only')
                    }

                    var npi=$('#npi_number').val()
                    var error_npi=$('#error_npi')
                    if(npi==""){
                        next=false
                        error_npi.removeClass('sr-only')
                    }else{
                        error_npi.addClass('sr-only')
                    }

                    var last_name=$('#last_name').val()
                    var error_last_name=$('#error_last_name')
                    if(last_name==""){
                        next=false
                        error_last_name.removeClass('sr-only')
                    }else{
                        error_last_name.addClass('sr-only')
                    }

                    var gender=$('#gender').val()
                    var error_gender=$('#error_gender')

                    if(gender==""){
                        next=false
                        error_gender.removeClass('sr-only')
                    }else{
                        error_gender.addClass('sr-only')
                    }

                    var social=$('#social').val()
                    var error_social=$('#error_social')

                    if(social==""){
                        next=false
                        error_social.removeClass('sr-only')
                    }else{
                        error_social.addClass('sr-only')
                    }

                    var general_category=$('#general_category').val()
                    var error_general_category =$('#error_general_category')

                    if(general_category.length==0){
                        next=false
                        error_general_category.removeClass('sr-only')
                    }else{
                        error_general_category.addClass('sr-only')
                    }


                    var medicare=$('#medicare').val()
                    var error_medicare=$('#error_medicare')

                    var medicare_out=$('#medicare_out')

                    if(medicare==""){
                        if(medicare_out.is(':checked')==false){
                            next=false
                            error_medicare.removeClass('sr-only')
                        }else{
                            error_medicare.addClass('sr-only')
                        }

                    }else{
                        error_medicare.addClass('sr-only')
                    }

                    var medicaid=$('#medicare').val()
                    var error_medicaid=$('#error_medicaid')

                    if(medicaid==""){
                        next=false
                        error_medicaid.removeClass('sr-only')
                    }else{
                        error_medicaid.addClass('sr-only')
                    }

                    var state_lic=$('#state_lic').val()
                    var error_state_lic=$('#error_state_lic')

                    if(state_lic==""){
                        next=false
                        error_state_lic.removeClass('sr-only')
                    }else{
                        error_state_lic.addClass('sr-only')
                    }

                    var license_expires_date=$('#license_expires_date').val()
                    var error_license_expires_date=$('#error_license_expires_date')

                    if(license_expires_date==""){
                        next=false
                        error_license_expires_date.removeClass('sr-only')
                    }else{
                        error_license_expires_date.addClass('sr-only')
                    }

                    var board_certified_yes=$('#board_certified_yes')
                    var board_certified_no=$('#board_certified_no')
                    var error_board_certified=$('#error_board_certified')

                    if(board_certified_yes.is(':checked') || board_certified_no.is(':checked')){
                        error_board_certified.addClass('sr-only')
                    }else{
                        next=false
                        error_board_certified.removeClass('sr-only')
                    }

                    var is_hospital_privileges_yes=$('#is_hospital_privileges_yes')
                    var is_hospital_privileges_no=$('#is_hospital_privileges_no')
                    var error_is_hospital_privileges=$('#error_is_hospital_privileges')

                    if(is_hospital_privileges_yes.is(':checked') || is_hospital_privileges_no.is(':checked')){
                        error_is_hospital_privileges.addClass('sr-only')
                    }else{
                        next=false
                        error_is_hospital_privileges.removeClass('sr-only')
                    }

                    var has_telemedicine_service_yes=$('#has_telemedicine_service_yes')
                    var has_telemedicine_service_no=$('#has_telemedicine_service_no')
                    var error_has_telemedicine_service=$('#error_has_telemedicine_service')

                    if(has_telemedicine_service_yes.is(':checked') || has_telemedicine_service_no.is(':checked')){
                        error_has_telemedicine_service.addClass('sr-only')
                    }else{
                        next=false
                        error_has_telemedicine_service.removeClass('sr-only')
                    }
                
                    var are_you_pcp_yes=$('#are_you_pcp_yes')
                    var are_you_pcp_no=$('#are_you_pcp_no')
                    var error_are_you_pcp=$('#error_are_you_pcp')

                    if(are_you_pcp_yes.is(':checked') || are_you_pcp_no.is(':checked')){
                        error_are_you_pcp.addClass('sr-only')
                    }else{
                        next=false
                        error_are_you_pcp.removeClass('sr-only')
                    }

                    var accepting_new_patients_yes=$('#accepting_new_patients_yes')
                    var accepting_new_patients_no=$('#accepting_new_patients_no')
                    var error_accepting_new_patients=$('#error_accepting_new_patients')

                    if(accepting_new_patients_yes.is(':checked') || accepting_new_patients_no.is(':checked')){
                        error_accepting_new_patients.addClass('sr-only')
                    }else{
                        next=false
                        error_accepting_new_patients.removeClass('sr-only')
                    }

                    var gender_acceptance=$('#gender_acceptance').val()
                    var error_gender_acceptance=$('#error_gender_acceptance')

                    if(gender_acceptance==0){
                        next=false
                        error_gender_acceptance.removeClass('sr-only')
                    }else{
                        error_gender_acceptance.addClass('sr-only')
                    }

                    return next
                }

                //valdiate the Languages
                if (currentIndex == 1) {
                    next =true

                    var languagesSelected = ""
                    var error_languages=$('#error_languages')

                    var languages = $('input[name=languages]')

                    languages.each(function (index, $this) {
                        var language = $(this)

                        if (language.is(':checked')) {
                            languagesSelected += language.val() + ","
                        }
                    })

                    if(languagesSelected==""){
                        next=false
                        error_languages.removeClass('sr-only')
                    }else{
                        error_languages.addClass('sr-only')
                    }

                    return  next
                }

                //validate the Provider Type
                if (currentIndex == 3) {
                    next =true
                    var error_providertypes=$('#error_providertypes')
                    var providertypesSelected=""
                    var providertypes = $('input[name=providertypes]')

                    providertypes.each(function (index, $this) {
                        var providertype = $(this)

                        if (providertype.is(':checked')) {
                            providertypesSelected += providertype.val() + ","
                        }
                    })

                    if(providertypesSelected==""){
                        next=false
                        error_providertypes.removeClass('sr-only')
                    }else{
                        error_providertypes.addClass('sr-only')
                    }

                    return  next
                }

                //validate the Degrees
                if (currentIndex == 2) {
                    next =true
                    var error_degrees=$('#error_degrees')
                    var degreesSelected=""
                    var degrees = $('input[name=degrees]')

                    degrees.each(function (index, $this) {
                        var degree = $(this)

                        if (degree.is(':checked')) {
                            degreesSelected += degree.val() + ","
                        }
                    })

                    if(degreesSelected==""){
                        next=false
                        error_degrees.removeClass('sr-only')
                    }else{
                        error_degrees.addClass('sr-only')
                    }

                    return  next
                }
            },
            onFinishing: function (event, currentIndex) {
                console.log('OK2')
               return true
            },
            onFinished: function (event, currentIndex) {


                var languagesSelected = ""
                var providertypesSelected=""
                var degreesSelected=""
                var specialtiesSelected=""
                var lineSelected=""

                var languages = $('input[name=languages]')

                languages.each(function (index, $this) {
                    var language = $(this)

                    if (language.is(':checked')) {
                        languagesSelected += language.val() + ","
                    }
                })

                var providertypes = $('input[name=providertypes]')

                providertypes.each(function (index, $this) {
                    var providertype = $(this)

                    if (providertype.is(':checked')) {
                        providertypesSelected += providertype.val() + ","
                    }
                })

                var degrees = $('input[name=degrees]')

                degrees.each(function (index, $this) {
                    var degree = $(this)

                    if (degree.is(':checked')) {
                        degreesSelected += degree.val() + ","
                    }
                })

                var specialties = $('input[name=specialties]')

                specialties.each(function (index, $this) {
                    var specialty = $(this)

                    if (specialty.is(':checked')) {
                        specialtiesSelected += specialty.val() + ","
                    }
                })

                var lines = $('input[name=lineofbusiness]')

                lines.each(function (index, $this) {
                    var line = $(this)

                    if (line.is(':checked')) {
                        lineSelected += line.val() + ","
                    }
                })

                $('#languagesSelected').val(languagesSelected)
                $('#providertypesSelected').val(providertypesSelected)
                $('#degreesSelected').val(degreesSelected)
                $('#specialtiesSelected').val(specialtiesSelected)
                $('#lineofbusinessSelected').val(lineSelected)


                var error_specialties=$('#error_specialties')

                if(specialtiesSelected==""){
                    error_specialties.removeClass('sr-only')
                }else{
                    error_specialties.addClass('sr-only')
                    var form=$('#form')
                    form.submit();
                }
            }
        });


        // Initialize validation
        $('.steps-validation').validate({
            ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
            errorClass: 'validation-invalid-label',
            highlight: function (element, errorClass) {
                $(element).removeClass(errorClass);
            },
            unhighlight: function (element, errorClass) {
                $(element).removeClass(errorClass);
            },

            // Different components require proper error label placement
            errorPlacement: function (error, element) {

                // Unstyled checkboxes, radios
                if (element.parents().hasClass('form-check')) {
                    error.appendTo(element.parents('.form-check').parent());
                }

                // Input with icons and Select2
                else if (element.parents().hasClass('form-group-feedback') || element.hasClass('select2-hidden-accessible')) {
                    error.appendTo(element.parent());
                }

                // Input group, styled file input
                else if (element.parent().is('.uniform-uploader, .uniform-select') || element.parents().hasClass('input-group')) {
                    error.appendTo(element.parent().parent());
                }

                // Other elements
                else {
                    error.insertAfter(element);
                }
            },
            rules: {
                email: {
                    email: true
                }
            }
        });
    };

    // Uniform
    var _componentUniform = function () {
        if (!$().uniform) {
            console.warn('Warning - uniform.min.js is not loaded.');
            return;
        }

        // Initialize
        $('.form-input-styled').uniform({
            fileButtonClass: 'action btn bg-blue'
        });
    };

    // Select2 select
    var _componentSelect2 = function () {
        if (!$().select2) {
            console.warn('Warning - select2.min.js is not loaded.');
            return;
        }

        // Initialize
        var $select = $('.form-control-select2').select2({
            minimumResultsForSearch: Infinity,
            width: '100%'
        });

        // Trigger value change when selection is made
        $select.on('change', function () {
            $(this).trigger('blur');
        });
    };


    //
    // Return objects assigned to module
    //

    return {
        init: function () {
            _componentWizard();
            _componentUniform();
            _componentSelect2();
        }
    }
}();


// Initialize module
// ------------------------------

document.addEventListener('DOMContentLoaded', function () {
    FormWizard.init();
});