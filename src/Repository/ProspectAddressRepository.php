<?php

namespace App\Repository;

use App\Entity\ProspectAddress;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProspectAddress|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProspectAddress|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProspectAddress[]    findAll()
 * @method ProspectAddress[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProspectAddressRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProspectAddress::class);
    }

    // /**
    //  * @return ProspectAddress[] Returns an array of ProspectAddress objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProspectAddress
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
