<?php

namespace App\Repository;

use App\Entity\OrganizationP;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrganizationP|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrganizationP|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrganizationP[]    findAll()
 * @method OrganizationP[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganizationPRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrganizationP::class);
    }

    // /**
    //  * @return OrganizationP[] Returns an array of OrganizationP objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrganizationP
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
