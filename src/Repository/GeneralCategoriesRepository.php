<?php

namespace App\Repository;

use App\Entity\GeneralCategories;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method GeneralCategories|null find($id, $lockMode = null, $lockVersion = null)
 * @method GeneralCategories|null findOneBy(array $criteria, array $orderBy = null)
 * @method GeneralCategories[]    findAll()
 * @method GeneralCategories[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GeneralCategoriesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, GeneralCategories::class);
    }

    // /**
    //  * @return GeneralCategories[] Returns an array of GeneralCategories objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?GeneralCategories
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
