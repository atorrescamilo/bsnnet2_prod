<?php

namespace App\Repository;

use App\Entity\ContactP;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ContactP|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactP|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactP[]    findAll()
 * @method ContactP[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactPRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContactP::class);
    }

    // /**
    //  * @return ContactP[] Returns an array of ContactP objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContactP
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
