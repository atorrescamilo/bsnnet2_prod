<?php

namespace App\Repository;

use App\Entity\FacilityType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FacilityType|null find($id, $lockMode = null, $lockVersion = null)
 * @method FacilityType|null findOneBy(array $criteria, array $orderBy = null)
 * @method FacilityType[]    findAll()
 * @method FacilityType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FacilityTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FacilityType::class);
    }

    // /**
    //  * @return FacilityType[] Returns an array of FacilityType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FacilityType
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
