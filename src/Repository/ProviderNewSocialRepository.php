<?php

namespace App\Repository;

use App\Entity\ProviderNewSocial;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProviderNewSocial|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProviderNewSocial|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProviderNewSocial[]    findAll()
 * @method ProviderNewSocial[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProviderNewSocialRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderNewSocial::class);
    }

    // /**
    //  * @return ProviderNewSocial[] Returns an array of ProviderNewSocial objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProviderNewSocial
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
