<?php

namespace App\Repository;

use App\Entity\PayerLineCounty;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PayerLineCounty|null find($id, $lockMode = null, $lockVersion = null)
 * @method PayerLineCounty|null findOneBy(array $criteria, array $orderBy = null)
 * @method PayerLineCounty[]    findAll()
 * @method PayerLineCounty[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PayerLineCountyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PayerLineCounty::class);
    }

    // /**
    //  * @return PayerLineCounty[] Returns an array of PayerLineCounty objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PayerLineCounty
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
