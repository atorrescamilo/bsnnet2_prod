<?php

namespace App\Repository;

use App\Entity\PayerCustomerCounty;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PayerCustomerCounty|null find($id, $lockMode = null, $lockVersion = null)
 * @method PayerCustomerCounty|null findOneBy(array $criteria, array $orderBy = null)
 * @method PayerCustomerCounty[]    findAll()
 * @method PayerCustomerCounty[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PayerCustomerCountyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PayerCustomerCounty::class);
    }

    // /**
    //  * @return PayerCustomerCounty[] Returns an array of PayerCustomerCounty objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PayerCustomerCounty
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
