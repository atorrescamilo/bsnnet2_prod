<?php

namespace App\Repository;

use App\Entity\SanctionReason;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SanctionReason|null find($id, $lockMode = null, $lockVersion = null)
 * @method SanctionReason|null findOneBy(array $criteria, array $orderBy = null)
 * @method SanctionReason[]    findAll()
 * @method SanctionReason[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SanctionReasonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SanctionReason::class);
    }

    // /**
    //  * @return SanctionReason[] Returns an array of SanctionReason objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SanctionReason
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
