<?php

namespace App\Repository;

use App\Entity\CredSimpleRecord;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CredSimpleRecord|null find($id, $lockMode = null, $lockVersion = null)
 * @method CredSimpleRecord|null findOneBy(array $criteria, array $orderBy = null)
 * @method CredSimpleRecord[]    findAll()
 * @method CredSimpleRecord[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CredSimpleRecordRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CredSimpleRecord::class);
    }

    // /**
    //  * @return CredSimpleRecord[] Returns an array of CredSimpleRecord objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CredSimpleRecord
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
