<?php

namespace App\Repository;

use App\Entity\AETNAPNVLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AETNAPNVLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method AETNAPNVLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method AETNAPNVLog[]    findAll()
 * @method AETNAPNVLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AETNAPNVLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AETNAPNVLog::class);
    }

    // /**
    //  * @return AETNAPNVLog[] Returns an array of AETNAPNVLog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AETNAPNVLog
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
