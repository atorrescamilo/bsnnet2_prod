<?php

namespace App\Repository;

use App\Entity\PDataMedicare;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PDataMedicare|null find($id, $lockMode = null, $lockVersion = null)
 * @method PDataMedicare|null findOneBy(array $criteria, array $orderBy = null)
 * @method PDataMedicare[]    findAll()
 * @method PDataMedicare[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PDataMedicareRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PDataMedicare::class);
    }

    // /**
    //  * @return PDataMedicare[] Returns an array of PDataMedicare objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PDataMedicare
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
