<?php

namespace App\Repository;

use App\Entity\PayerRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PayerRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method PayerRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method PayerRole[]    findAll()
 * @method PayerRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PayerRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PayerRole::class);
    }

    // /**
    //  * @return PayerRole[] Returns an array of PayerRole objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PayerRole
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
