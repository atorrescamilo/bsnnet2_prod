<?php

namespace App\Repository;

use App\Entity\ProviderMedicaidPml;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProviderMedicaidPml|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProviderMedicaidPml|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProviderMedicaidPml[]    findAll()
 * @method ProviderMedicaidPml[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProviderMedicaidPmlRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderMedicaidPml::class);
    }

    // /**
    //  * @return ProviderMedicaidPml[] Returns an array of ProviderMedicaidPml objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProviderMedicaidPml
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
