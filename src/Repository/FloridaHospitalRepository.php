<?php

namespace App\Repository;

use App\Entity\FloridaHospital;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FloridaHospital|null find($id, $lockMode = null, $lockVersion = null)
 * @method FloridaHospital|null findOneBy(array $criteria, array $orderBy = null)
 * @method FloridaHospital[]    findAll()
 * @method FloridaHospital[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FloridaHospitalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FloridaHospital::class);
    }

    // /**
    //  * @return FloridaHospital[] Returns an array of FloridaHospital objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FloridaHospital
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
