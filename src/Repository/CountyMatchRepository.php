<?php

namespace App\Repository;

use App\Entity\CountyMatch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CountyMatch|null find($id, $lockMode = null, $lockVersion = null)
 * @method CountyMatch|null findOneBy(array $criteria, array $orderBy = null)
 * @method CountyMatch[]    findAll()
 * @method CountyMatch[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CountyMatchRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CountyMatch::class);
    }

    // /**
    //  * @return CountyMatch[] Returns an array of CountyMatch objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CountyMatch
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
