<?php

namespace App\Repository;

use App\Entity\CredentialingStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CredentialingStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method CredentialingStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method CredentialingStatus[]    findAll()
 * @method CredentialingStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CredentialingStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CredentialingStatus::class);
    }

    // /**
    //  * @return CredentialingStatus[] Returns an array of CredentialingStatus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CredentialingStatus
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
