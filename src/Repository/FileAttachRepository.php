<?php

namespace App\Repository;

use App\Entity\FileAttach;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FileAttach|null find($id, $lockMode = null, $lockVersion = null)
 * @method FileAttach|null findOneBy(array $criteria, array $orderBy = null)
 * @method FileAttach[]    findAll()
 * @method FileAttach[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FileAttachRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FileAttach::class);
    }

    // /**
    //  * @return FileAttach[] Returns an array of FileAttach objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FileAttach
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
