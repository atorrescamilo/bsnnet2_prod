<?php

namespace App\Repository;

use App\Entity\ProviderHospitalAffiliations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProviderHospitalAffiliations|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProviderHospitalAffiliations|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProviderHospitalAffiliations[]    findAll()
 * @method ProviderHospitalAffiliations[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProviderHospitalAffiliationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderHospitalAffiliations::class);
    }

    // /**
    //  * @return ProviderHospitalAffiliations[] Returns an array of ProviderHospitalAffiliations objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProviderHospitalAffiliations
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
