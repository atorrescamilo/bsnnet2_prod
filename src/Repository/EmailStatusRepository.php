<?php

namespace App\Repository;

use App\Entity\EmailStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EmailStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmailStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmailStatus[]    findAll()
 * @method EmailStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmailStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmailStatus::class);
    }

    // /**
    //  * @return EmailStatus[] Returns an array of EmailStatus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EmailStatus
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
