<?php

namespace App\Repository;

use App\Entity\MMMRosterIndividualData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MMMRosterIndividualData|null find($id, $lockMode = null, $lockVersion = null)
 * @method MMMRosterIndividualData|null findOneBy(array $criteria, array $orderBy = null)
 * @method MMMRosterIndividualData[]    findAll()
 * @method MMMRosterIndividualData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MMMRosterIndividualDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MMMRosterIndividualData::class);
    }

    // /**
    //  * @return MMMRosterIndividualData[] Returns an array of MMMRosterIndividualData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MMMRosterIndividualData
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
