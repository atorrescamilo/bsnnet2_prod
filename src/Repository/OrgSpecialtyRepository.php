<?php

namespace App\Repository;

use App\Entity\OrgSpecialty;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrgSpecialty|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrgSpecialty|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrgSpecialty[]    findAll()
 * @method OrgSpecialty[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrgSpecialtyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrgSpecialty::class);
    }

    // /**
    //  * @return OrgSpecialty[] Returns an array of OrgSpecialty objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrgSpecialty
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
