<?php

namespace App\Repository;

use App\Entity\BillingAddressCvo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BillingAddressCvo|null find($id, $lockMode = null, $lockVersion = null)
 * @method BillingAddressCvo|null findOneBy(array $criteria, array $orderBy = null)
 * @method BillingAddressCvo[]    findAll()
 * @method BillingAddressCvo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BillingAddressCvoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, BillingAddressCvo::class);
    }

    // /**
    //  * @return BillingAddressCvo[] Returns an array of BillingAddressCvo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('b.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?BillingAddressCvo
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
