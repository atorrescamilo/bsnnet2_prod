<?php

namespace App\Repository;

use App\Entity\OrganizationPayerExcluded;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrganizationPayerExcluded|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrganizationPayerExcluded|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrganizationPayerExcluded[]    findAll()
 * @method OrganizationPayerExcluded[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganizationPayerExcludedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrganizationPayerExcluded::class);
    }

    // /**
    //  * @return OrganizationPayerExcluded[] Returns an array of OrganizationPayerExcluded objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrganizationPayerExcluded
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
