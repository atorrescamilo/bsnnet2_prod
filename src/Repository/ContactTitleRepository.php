<?php

namespace App\Repository;

use App\Entity\ContactTitle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ContactTitle|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactTitle|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactTitle[]    findAll()
 * @method ContactTitle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactTitleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContactTitle::class);
    }

    // /**
    //  * @return ContactTitle[] Returns an array of ContactTitle objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContactTitle
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
