<?php

namespace App\Repository;

use App\Entity\DirectoryUpdate;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method DirectoryUpdate|null find($id, $lockMode = null, $lockVersion = null)
 * @method DirectoryUpdate|null findOneBy(array $criteria, array $orderBy = null)
 * @method DirectoryUpdate[]    findAll()
 * @method DirectoryUpdate[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DirectoryUpdateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DirectoryUpdate::class);
    }

    // /**
    //  * @return DirectoryUpdate[] Returns an array of DirectoryUpdate objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DirectoryUpdate
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
