<?php

namespace App\Repository;

use App\Entity\ProviderP;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProviderP|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProviderP|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProviderP[]    findAll()
 * @method ProviderP[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProviderPRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderP::class);
    }

    // /**
    //  * @return ProviderP[] Returns an array of ProviderP objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProviderP
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
