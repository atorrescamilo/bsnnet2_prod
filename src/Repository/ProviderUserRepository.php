<?php

namespace App\Repository;

use App\Entity\ProviderUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProviderUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProviderUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProviderUser[]    findAll()
 * @method ProviderUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProviderUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderUser::class);
    }

    // /**
    //  * @return ProviderUser[] Returns an array of ProviderUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProviderUser
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
