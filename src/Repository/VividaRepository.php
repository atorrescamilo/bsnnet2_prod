<?php

namespace App\Repository;

use App\Entity\Vivida;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Vivida|null find($id, $lockMode = null, $lockVersion = null)
 * @method Vivida|null findOneBy(array $criteria, array $orderBy = null)
 * @method Vivida[]    findAll()
 * @method Vivida[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VividaRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Vivida::class);
    }

    // /**
    //  * @return Vivida[] Returns an array of Vivida objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('v.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Vivida
    {
        return $this->createQueryBuilder('v')
            ->andWhere('v.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
