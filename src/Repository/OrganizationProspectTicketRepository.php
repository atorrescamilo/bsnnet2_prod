<?php

namespace App\Repository;

use App\Entity\OrganizationProspectTicket;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrganizationProspectTicket|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrganizationProspectTicket|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrganizationProspectTicket[]    findAll()
 * @method OrganizationProspectTicket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganizationProspectTicketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrganizationProspectTicket::class);
    }

    // /**
    //  * @return OrganizationProspectTicket[] Returns an array of OrganizationProspectTicket objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrganizationProspectTicket
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
