<?php

namespace App\Repository;

use App\Entity\ProviderPayerExcluded;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProviderPayerExcluded|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProviderPayerExcluded|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProviderPayerExcluded[]    findAll()
 * @method ProviderPayerExcluded[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProviderPayerExcludedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderPayerExcluded::class);
    }

    // /**
    //  * @return ProviderPayerExcluded[] Returns an array of ProviderPayerExcluded objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProviderPayerExcluded
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
