<?php

namespace App\Repository;

use App\Entity\JoinStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method JoinStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method JoinStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method JoinStatus[]    findAll()
 * @method JoinStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JoinStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JoinStatus::class);
    }

    // /**
    //  * @return JoinStatus[] Returns an array of JoinStatus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JoinStatus
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
