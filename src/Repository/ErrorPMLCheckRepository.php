<?php

namespace App\Repository;

use App\Entity\ErrorPMLCheck;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ErrorPMLCheck|null find($id, $lockMode = null, $lockVersion = null)
 * @method ErrorPMLCheck|null findOneBy(array $criteria, array $orderBy = null)
 * @method ErrorPMLCheck[]    findAll()
 * @method ErrorPMLCheck[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ErrorPMLCheckRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ErrorPMLCheck::class);
    }

    // /**
    //  * @return ErrorPMLCheck[] Returns an array of ErrorPMLCheck objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ErrorPMLCheck
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
