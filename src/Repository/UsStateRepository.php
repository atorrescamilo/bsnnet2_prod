<?php

namespace App\Repository;

use App\Entity\UsState;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UsState|null find($id, $lockMode = null, $lockVersion = null)
 * @method UsState|null findOneBy(array $criteria, array $orderBy = null)
 * @method UsState[]    findAll()
 * @method UsState[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UsStateRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UsState::class);
    }

    // /**
    //  * @return UsState[] Returns an array of UsState objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UsState
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
