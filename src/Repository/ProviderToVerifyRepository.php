<?php

namespace App\Repository;

use App\Entity\ProviderToVerify;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProviderToVerify|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProviderToVerify|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProviderToVerify[]    findAll()
 * @method ProviderToVerify[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProviderToVerifyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderToVerify::class);
    }

    // /**
    //  * @return ProviderToVerify[] Returns an array of ProviderToVerify objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProviderToVerify
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
