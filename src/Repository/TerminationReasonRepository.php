<?php

namespace App\Repository;

use App\Entity\TerminationReason;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method TerminationReason|null find($id, $lockMode = null, $lockVersion = null)
 * @method TerminationReason|null findOneBy(array $criteria, array $orderBy = null)
 * @method TerminationReason[]    findAll()
 * @method TerminationReason[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TerminationReasonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TerminationReason::class);
    }

    // /**
    //  * @return TerminationReason[] Returns an array of TerminationReason objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TerminationReason
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
