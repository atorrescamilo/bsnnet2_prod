<?php

namespace App\Repository;

use App\Entity\PayerUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PayerUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method PayerUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method PayerUser[]    findAll()
 * @method PayerUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PayerUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PayerUser::class);
    }

    // /**
    //  * @return PayerUser[] Returns an array of PayerUser objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PayerUser
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
