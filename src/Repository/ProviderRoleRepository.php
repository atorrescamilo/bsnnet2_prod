<?php

namespace App\Repository;

use App\Entity\ProviderRole;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProviderRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProviderRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProviderRole[]    findAll()
 * @method ProviderRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProviderRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderRole::class);
    }

    // /**
    //  * @return ProviderRole[] Returns an array of ProviderRole objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProviderRole
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
