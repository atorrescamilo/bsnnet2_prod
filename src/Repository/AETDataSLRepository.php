<?php

namespace App\Repository;

use App\Entity\AETDataSL;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AETDataSL|null find($id, $lockMode = null, $lockVersion = null)
 * @method AETDataSL|null findOneBy(array $criteria, array $orderBy = null)
 * @method AETDataSL[]    findAll()
 * @method AETDataSL[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AETDataSLRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AETDataSL::class);
    }

    // /**
    //  * @return AETDataSL[] Returns an array of AETDataSL objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AETDataSL
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
