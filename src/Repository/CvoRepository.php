<?php

namespace App\Repository;

use App\Entity\Cvo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Cvo|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cvo|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cvo[]    findAll()
 * @method Cvo[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CvoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Cvo::class);
    }

    // /**
    //  * @return Cvo[] Returns an array of Cvo objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Cvo
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
