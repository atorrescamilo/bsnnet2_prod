<?php

namespace App\Repository;

use App\Entity\PNVSLLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PNVSLLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method PNVSLLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method PNVSLLog[]    findAll()
 * @method PNVSLLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PNVSLLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PNVSLLog::class);
    }

    // /**
    //  * @return PNVSLLog[] Returns an array of PNVSLLog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PNVSLLog
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
