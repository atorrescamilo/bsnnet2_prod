<?php

namespace App\Repository;

use App\Entity\AETData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AETData|null find($id, $lockMode = null, $lockVersion = null)
 * @method AETData|null findOneBy(array $criteria, array $orderBy = null)
 * @method AETData[]    findAll()
 * @method AETData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AETDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AETData::class);
    }

    // /**
    //  * @return AETData[] Returns an array of AETData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AETData
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
