<?php

namespace App\Repository;

use App\Entity\LineOfBusiness;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LineOfBusiness|null find($id, $lockMode = null, $lockVersion = null)
 * @method LineOfBusiness|null findOneBy(array $criteria, array $orderBy = null)
 * @method LineOfBusiness[]    findAll()
 * @method LineOfBusiness[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LineOfBusinessRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LineOfBusiness::class);
    }

    // /**
    //  * @return LineOfBusiness[] Returns an array of LineOfBusiness objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LineOfBusiness
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
