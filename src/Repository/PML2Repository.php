<?php

namespace App\Repository;

use App\Entity\PML2;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PML2|null find($id, $lockMode = null, $lockVersion = null)
 * @method PML2|null findOneBy(array $criteria, array $orderBy = null)
 * @method PML2[]    findAll()
 * @method PML2[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PML2Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PML2::class);
    }

    // /**
    //  * @return PML2[] Returns an array of PML2 objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PML2
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
