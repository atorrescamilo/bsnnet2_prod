<?php

namespace App\Repository;

use App\Entity\OrganizationTrainingAttestation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrganizationTrainingAttestation|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrganizationTrainingAttestation|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrganizationTrainingAttestation[]    findAll()
 * @method OrganizationTrainingAttestation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganizationTrainingAttestationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrganizationTrainingAttestation::class);
    }

    // /**
    //  * @return OrganizationTrainingAttestation[] Returns an array of OrganizationTrainingAttestation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrganizationTrainingAttestation
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
