<?php

namespace App\Repository;

use App\Entity\EmailQueued;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EmailQueued|null find($id, $lockMode = null, $lockVersion = null)
 * @method EmailQueued|null findOneBy(array $criteria, array $orderBy = null)
 * @method EmailQueued[]    findAll()
 * @method EmailQueued[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EmailQueuedRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EmailQueued::class);
    }

    // /**
    //  * @return EmailQueued[] Returns an array of EmailQueued objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EmailQueued
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
