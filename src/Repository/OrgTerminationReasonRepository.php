<?php

namespace App\Repository;

use App\Entity\OrgTerminationReason;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrgTerminationReason|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrgTerminationReason|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrgTerminationReason[]    findAll()
 * @method OrgTerminationReason[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrgTerminationReasonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrgTerminationReason::class);
    }

    // /**
    //  * @return OrgTerminationReason[] Returns an array of OrgTerminationReason objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrgTerminationReason
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
