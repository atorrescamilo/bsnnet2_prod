<?php

namespace App\Repository;

use App\Entity\AETFHKCMedicaidData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AETFHKCMedicaidData|null find($id, $lockMode = null, $lockVersion = null)
 * @method AETFHKCMedicaidData|null findOneBy(array $criteria, array $orderBy = null)
 * @method AETFHKCMedicaidData[]    findAll()
 * @method AETFHKCMedicaidData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AETFHKCMedicaidDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AETFHKCMedicaidData::class);
    }

    // /**
    //  * @return AETFHKCMedicaidData[] Returns an array of AETFHKCMedicaidData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AETFHKCMedicaidData
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
