<?php

namespace App\Repository;

use App\Entity\FCCData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FCCData|null find($id, $lockMode = null, $lockVersion = null)
 * @method FCCData|null findOneBy(array $criteria, array $orderBy = null)
 * @method FCCData[]    findAll()
 * @method FCCData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FCCDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FCCData::class);
    }

    // /**
    //  * @return FCCData[] Returns an array of FCCData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FCCData
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
