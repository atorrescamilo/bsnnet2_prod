<?php

namespace App\Repository;

use App\Entity\FacilityAppLocation;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FacilityAppLocation|null find($id, $lockMode = null, $lockVersion = null)
 * @method FacilityAppLocation|null findOneBy(array $criteria, array $orderBy = null)
 * @method FacilityAppLocation[]    findAll()
 * @method FacilityAppLocation[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FacilityAppLocationRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FacilityAppLocation::class);
    }

    // /**
    //  * @return FacilityAppLocation[] Returns an array of FacilityAppLocation objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FacilityAppLocation
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
