<?php

namespace App\Repository;

use App\Entity\FacilityApp;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FacilityApp|null find($id, $lockMode = null, $lockVersion = null)
 * @method FacilityApp|null findOneBy(array $criteria, array $orderBy = null)
 * @method FacilityApp[]    findAll()
 * @method FacilityApp[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FacilityAppRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FacilityApp::class);
    }

    // /**
    //  * @return FacilityApp[] Returns an array of FacilityApp objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FacilityApp
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
