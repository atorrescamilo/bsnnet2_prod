<?php

namespace App\Repository;

use App\Entity\AgesRange;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AgesRange|null find($id, $lockMode = null, $lockVersion = null)
 * @method AgesRange|null findOneBy(array $criteria, array $orderBy = null)
 * @method AgesRange[]    findAll()
 * @method AgesRange[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AgesRangeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AgesRange::class);
    }

    // /**
    //  * @return AgesRange[] Returns an array of AgesRange objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AgesRange
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
