<?php

namespace App\Repository;

use App\Entity\OrganizationStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrganizationStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrganizationStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrganizationStatus[]    findAll()
 * @method OrganizationStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganizationStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrganizationStatus::class);
    }

    // /**
    //  * @return OrganizationStatus[] Returns an array of OrganizationStatus objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrganizationStatus
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
