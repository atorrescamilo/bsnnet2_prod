<?php

namespace App\Repository;

use App\Entity\UserOrgRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method UserOrgRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserOrgRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserOrgRequest[]    findAll()
 * @method UserOrgRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserOrgRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, UserOrgRequest::class);
    }

    // /**
    //  * @return UserOrgRequest[] Returns an array of UserOrgRequest objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?UserOrgRequest
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
