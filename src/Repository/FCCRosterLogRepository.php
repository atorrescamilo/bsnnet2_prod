<?php

namespace App\Repository;

use App\Entity\FCCRosterLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FCCRosterLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method FCCRosterLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method FCCRosterLog[]    findAll()
 * @method FCCRosterLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FCCRosterLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FCCRosterLog::class);
    }

    // /**
    //  * @return FCCRosterLog[] Returns an array of FCCRosterLog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FCCRosterLog
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
