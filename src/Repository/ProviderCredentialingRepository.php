<?php

namespace App\Repository;

use App\Entity\ProviderCredentialing;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProviderCredentialing|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProviderCredentialing|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProviderCredentialing[]    findAll()
 * @method ProviderCredentialing[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProviderCredentialingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderCredentialing::class);
    }

    // /**
    //  * @return ProviderCredentialing[] Returns an array of ProviderCredentialing objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProviderCredentialing
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
