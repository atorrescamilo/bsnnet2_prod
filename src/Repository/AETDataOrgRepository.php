<?php

namespace App\Repository;

use App\Entity\AETDataOrg;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AETDataOrg|null find($id, $lockMode = null, $lockVersion = null)
 * @method AETDataOrg|null findOneBy(array $criteria, array $orderBy = null)
 * @method AETDataOrg[]    findAll()
 * @method AETDataOrg[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AETDataOrgRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AETDataOrg::class);
    }

    // /**
    //  * @return AETDataOrg[] Returns an array of AETDataOrg objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AETDataOrg
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
