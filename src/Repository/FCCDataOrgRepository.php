<?php

namespace App\Repository;

use App\Entity\FCCDataOrg;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FCCDataOrg|null find($id, $lockMode = null, $lockVersion = null)
 * @method FCCDataOrg|null findOneBy(array $criteria, array $orderBy = null)
 * @method FCCDataOrg[]    findAll()
 * @method FCCDataOrg[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FCCDataOrgRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FCCDataOrg::class);
    }

    // /**
    //  * @return FCCDataOrg[] Returns an array of FCCDataOrg objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FCCDataOrg
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
