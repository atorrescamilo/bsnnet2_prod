<?php

namespace App\Repository;

use App\Entity\LegalType;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method LegalType|null find($id, $lockMode = null, $lockVersion = null)
 * @method LegalType|null findOneBy(array $criteria, array $orderBy = null)
 * @method LegalType[]    findAll()
 * @method LegalType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LegalTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, LegalType::class);
    }

    // /**
    //  * @return LegalType[] Returns an array of LegalType objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LegalType
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
