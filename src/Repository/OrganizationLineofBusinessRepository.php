<?php

namespace App\Repository;

use App\Entity\OrganizationLineofBusiness;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrganizationLineofBusiness|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrganizationLineofBusiness|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrganizationLineofBusiness[]    findAll()
 * @method OrganizationLineofBusiness[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganizationLineofBusinessRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrganizationLineofBusiness::class);
    }

    // /**
    //  * @return OrganizationLineofBusiness[] Returns an array of OrganizationLineofBusiness objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrganizationLineofBusiness
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
