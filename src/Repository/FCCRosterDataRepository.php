<?php

namespace App\Repository;

use App\Entity\FCCRosterData;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FCCRosterData|null find($id, $lockMode = null, $lockVersion = null)
 * @method FCCRosterData|null findOneBy(array $criteria, array $orderBy = null)
 * @method FCCRosterData[]    findAll()
 * @method FCCRosterData[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FCCRosterDataRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FCCRosterData::class);
    }

    // /**
    //  * @return FCCRosterData[] Returns an array of FCCRosterData objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FCCRosterData
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
