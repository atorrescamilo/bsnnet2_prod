<?php

namespace App\Repository;

use App\Entity\EapOnly;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EapOnly|null find($id, $lockMode = null, $lockVersion = null)
 * @method EapOnly|null findOneBy(array $criteria, array $orderBy = null)
 * @method EapOnly[]    findAll()
 * @method EapOnly[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EapOnlyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EapOnly::class);
    }

    // /**
    //  * @return EapOnly[] Returns an array of EapOnly objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?EapOnly
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
