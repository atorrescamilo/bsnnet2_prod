<?php

namespace App\Repository;

use App\Entity\OrganizationValidateRoster;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrganizationValidateRoster|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrganizationValidateRoster|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrganizationValidateRoster[]    findAll()
 * @method OrganizationValidateRoster[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrganizationValidateRosterRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrganizationValidateRoster::class);
    }

    // /**
    //  * @return OrganizationValidateRoster[] Returns an array of OrganizationValidateRoster objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrganizationValidateRoster
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
