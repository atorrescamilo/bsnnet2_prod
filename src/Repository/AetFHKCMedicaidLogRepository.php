<?php

namespace App\Repository;

use App\Entity\AetFHKCMedicaidLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AetFHKCMedicaidLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method AetFHKCMedicaidLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method AetFHKCMedicaidLog[]    findAll()
 * @method AetFHKCMedicaidLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AetFHKCMedicaidLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AetFHKCMedicaidLog::class);
    }

    // /**
    //  * @return AetFHKCMedicaidLog[] Returns an array of AetFHKCMedicaidLog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AetFHKCMedicaidLog
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
