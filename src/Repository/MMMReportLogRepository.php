<?php

namespace App\Repository;

use App\Entity\MMMReportLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MMMReportLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method MMMReportLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method MMMReportLog[]    findAll()
 * @method MMMReportLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MMMReportLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MMMReportLog::class);
    }

    // /**
    //  * @return MMMReportLog[] Returns an array of MMMReportLog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MMMReportLog
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
