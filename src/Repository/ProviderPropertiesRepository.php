<?php

namespace App\Repository;

use App\Entity\ProviderProperties;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;


/**
 * @method ProviderProperties|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProviderProperties|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProviderProperties[]    findAll()
 * @method ProviderProperties[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProviderPropertiesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderProperties::class);
    }

    // /**
    //  * @return ProviderProperties[] Returns an array of ProviderProperties objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProviderProperties
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
