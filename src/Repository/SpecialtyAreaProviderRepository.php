<?php

namespace App\Repository;

use App\Entity\SpecialtyAreaProvider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method SpecialtyAreaProvider|null find($id, $lockMode = null, $lockVersion = null)
 * @method SpecialtyAreaProvider|null findOneBy(array $criteria, array $orderBy = null)
 * @method SpecialtyAreaProvider[]    findAll()
 * @method SpecialtyAreaProvider[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpecialtyAreaProviderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SpecialtyAreaProvider::class);
    }

    // /**
    //  * @return SpecialtyAreaProvider[] Returns an array of SpecialtyAreaProvider objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SpecialtyAreaProvider
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
