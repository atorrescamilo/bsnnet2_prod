<?php

namespace App\Repository;

use App\Entity\ProviderCredFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProviderCredFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProviderCredFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProviderCredFile[]    findAll()
 * @method ProviderCredFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProviderCredFileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProviderCredFile::class);
    }

    // /**
    //  * @return ProviderCredFile[] Returns an array of ProviderCredFile objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProviderCredFile
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
