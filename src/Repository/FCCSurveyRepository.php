<?php

namespace App\Repository;

use App\Entity\FCCSurvey;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method FCCSurvey|null find($id, $lockMode = null, $lockVersion = null)
 * @method FCCSurvey|null findOneBy(array $criteria, array $orderBy = null)
 * @method FCCSurvey[]    findAll()
 * @method FCCSurvey[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FCCSurveyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FCCSurvey::class);
    }

    // /**
    //  * @return FCCSurvey[] Returns an array of FCCSurvey objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('f.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?FCCSurvey
    {
        return $this->createQueryBuilder('f')
            ->andWhere('f.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
