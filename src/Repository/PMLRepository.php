<?php

namespace App\Repository;

use App\Entity\PML;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PML|null find($id, $lockMode = null, $lockVersion = null)
 * @method PML|null findOneBy(array $criteria, array $orderBy = null)
 * @method PML[]    findAll()
 * @method PML[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PMLRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PML::class);
    }

    // /**
    //  * @return PML[] Returns an array of PML objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PML
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
