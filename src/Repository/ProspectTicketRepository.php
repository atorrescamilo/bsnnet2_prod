<?php

namespace App\Repository;

use App\Entity\ProspectTicket;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProspectTicket|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProspectTicket|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProspectTicket[]    findAll()
 * @method ProspectTicket[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProspectTicketRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProspectTicket::class);
    }

    // /**
    //  * @return ProspectTicket[] Returns an array of ProspectTicket objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ProspectTicket
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
