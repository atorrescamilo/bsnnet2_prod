<?php

namespace App\Repository;

use App\Entity\PaymentTier;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PaymentTier|null find($id, $lockMode = null, $lockVersion = null)
 * @method PaymentTier|null findOneBy(array $criteria, array $orderBy = null)
 * @method PaymentTier[]    findAll()
 * @method PaymentTier[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentTierRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PaymentTier::class);
    }

    // /**
    //  * @return PaymentTier[] Returns an array of PaymentTier objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PaymentTier
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
