<?php

namespace App\Repository;

use App\Entity\AETNAPNVSLLog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AETNAPNVSLLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method AETNAPNVSLLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method AETNAPNVSLLog[]    findAll()
 * @method AETNAPNVSLLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AETNAPNVSLLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AETNAPNVSLLog::class);
    }

    // /**
    //  * @return AETNAPNVSLLog[] Returns an array of AETNAPNVSLLog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AETNAPNVSLLog
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
