<?php

namespace App\Controller;

use App\Entity\MMMIndicator;
use App\Entity\PayerContact;
use App\Entity\PayerCustomerCounty;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Payer;
use App\Entity\Provider;
use App\Entity\ProviderFields;
use App\Entity\PayerLineCounty;
use App\Service\CustomerCountyFileUploader;
use PhpOffice\PhpSpreadsheet\Reader;

/**
 * @Route("/admin/payer")
 */
class PayerController extends AbstractController{

    /**
     * @Route("/index", name="admin_payer")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $payers = $em->getRepository('App\Entity\Payer')->findAll();
            $delete_form_ajax = $this->createCustomForm('PAYER_ID', 'DELETE', 'admin_delete_payer');

            return $this->render('payer/index.html.twig', array('payers' => $payers, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_payer")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $counties = $em->getRepository('App\Entity\CountyMatch')->findAll(array('name' => 'ASC'));
            $lineOfBusiness = $em->getRepository('App\Entity\LineOfBusiness')->findAll();

            return $this->render('payer/add.html.twig', array('counties' => $counties, 'lineOfBusiness' => $lineOfBusiness));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new-contact/{id}", name="admin_new_payer_contact", defaults={"id": null})
     */
    public function addContact($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {


            return $this->render('payer/add_contact.html.twig',['payer_id'=>$id]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit-contact/{id}", name="admin_edit_payer_contact", defaults={"id": null})
     */
    public function editContact($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $contact=$em->getRepository('App:PayerContact')->find($id);

            $payer=$contact->getPayer()->getId();
            if($contact){
                return $this->render('payer/edit_contact.html.twig',['id'=>$id,'payer_id'=>$payer,'contact'=>$contact]);
            }
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view-contact/{id}", name="admin_view_payer_contact", defaults={"id": null})
     */
    public function viewContact($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $contact=$em->getRepository('App:PayerContact')->find($id);

            $payer=$contact->getPayer()->getId();
            if($contact){
                return $this->render('payer/view_contact.html.twig',['id'=>$id,'payer_id'=>$payer,'contact'=>$contact]);
            }
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create-contact", name="admin_create_payer_contact", methods={"POST"})
     */
    public function createContact(Request $request){

        $em = $this->getDoctrine()->getManager();

        $first_name=$request->get('first_name');
        $last_name=$request->get('last_name');
        $phone=$request->get('phone');
        $phone_ext=$request->get('phone_ext');
        $mobile=$request->get('mobile');
        $fax=$request->get('fax');
        $email=$request->get('email');
        $description=$request->get('description');
        $new=$request->get('new');
        $payer_id=$request->get('payer');
        $title=$request->get('title');
        $category=$request->get('category');

        $contact=new PayerContact();
        $contact->setFirstName($first_name);
        $contact->setLastName($last_name);
        $contact->setPhone($phone);
        $contact->setPhoneExt($phone_ext);
        $contact->setMobile($mobile);
        $contact->setFax($fax);
        $contact->setEmail($email);
        $contact->setDescription($description);
        $contact->setTitle($title);
        $contact->setCategory($category);

        $payer=$em->getRepository('App:Payer')->find($payer_id);
        $contact->setPayer($payer);

        $em->persist($contact);
        $em->flush();


        if ($new == 1) {
            return $this->redirectToRoute('admin_new_payer_contact', ['id' => $payer_id]);
        }

        return $this->redirectToRoute('admin_view_payer', ['id' => $payer_id]);
    }

    /**
     * @Route("/updsated-contact", name="admin_updated_payer", methods={"POST"})
     */
    public function updatedContact(Request $request){
        $em = $this->getDoctrine()->getManager();

        $first_name=$request->get('first_name');
        $last_name=$request->get('last_name');
        $phone=$request->get('phone');
        $phone_ext=$request->get('phone_ext');
        $mobile=$request->get('mobile');
        $fax=$request->get('fax');
        $email=$request->get('email');
        $description=$request->get('description');
        $new=$request->get('new');
        $payer_id=$request->get('payer');
        $id=$request->get('id');
        $title=$request->get('title');
        $category=$request->get('category');

        $contact=$em->getRepository('App:PayerContact')->find($id);

        $contact->setFirstName($first_name);
        $contact->setLastName($last_name);
        $contact->setPhone($phone);
        $contact->setPhoneExt($phone_ext);
        $contact->setMobile($mobile);
        $contact->setFax($fax);
        $contact->setEmail($email);
        $contact->setDescription($description);
        $payer=$em->getRepository('App:Payer')->find($payer_id);
        $contact->setPayer($payer);
        $contact->setTitle($title);
        $contact->setCategory($category);

        $em->persist($contact);
        $em->flush();

        if ($new == 1) {
            return $this->redirectToRoute('admin_edit_payer_contact', ['id' => $id]);
        }

        return $this->redirectToRoute('admin_view_payer', ['id' => $payer_id]);
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_payer", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Payer')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_payer');
            }

            $counties = $em->getRepository('App\Entity\CountyMatch')->findAll(array('name' => 'ASC'));
            $lineOfBusiness = $em->getRepository('App\Entity\LineOfBusiness')->findAll();

            $payerLine = $document->getLineOfBusiness();
            $payerCounties = $em->getRepository('App\Entity\PayerLineCounty')->findBy(array('payer' => $id));

            return $this->render('payer/edit.html.twig', array('document' => $document, 'counties' => $counties, 'lineOfBusiness' => $lineOfBusiness,
                'payerline' => $payerLine, 'payerCounties' => $payerCounties));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_payer", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $document = $em->getRepository('App\Entity\Payer')->find($id);

            $PayerLineCounty = $em->getRepository('App\Entity\PayerLineCounty')->findBy(array('payer' => $id));
            //load data for CRUD on Payment Tier
            $payments = $em->getRepository('App\Entity\PaymentTier')->findBy(array('payer' => $id));
            //all counties by payers
            $customers=$em->getRepository('App\Entity\PayerCustomerCounty')->findBy(array('payer'=>$id));

            $providers_ready=[];
            $providers_ready_final=[];
            $providers_ready_final_ids=[];
            $pro_ind_npis=[];
            //rules for FCC
            if($id==4){
                $sql="SELECT p.id, p.first_name,p.last_name,p.npi_number,organization.name as org_name,p.state_lic,
                GROUP_CONCAT(DISTINCT payer.id ORDER BY payer.id ASC SEPARATOR ', ') AS `payers`,
                GROUP_CONCAT(DISTINCT fccdata.npi ORDER BY fccdata.id ASC SEPARATOR ', ') AS `fccdata`,
                GROUP_CONCAT(DISTINCT provider_credentialing.credentialing_status_id ORDER BY provider_credentialing.id ASC SEPARATOR ', ') AS `individual_credentialing`
                FROM  provider p
                LEFT JOIN organization ON organization.id = p.org_id
                LEFT JOIN provider_credentialing ON provider_credentialing.provider_id = p.id
                LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
                LEFT JOIN fccdata ON fccdata.npi = p.npi_number
                LEFT JOIN payer ON provider_payer.payer_id = payer.id
                WHERE p.disabled=0 
                GROUP BY p.id";

                $stmt = $conn->prepare($sql);
                $stmt->execute();
                $providers_ready= $stmt->fetchAll();

                $exclude_npis=[];
                $exclude_ids=[]; //providers excludes on a organization
                $sql="SELECT p.provider_id, p.payer_id, provider.npi_number as npi
                FROM  provider_payer_exclude p
                LEFT JOIN provider ON provider.id = p.provider_id";

                $stmt = $conn->prepare($sql);
                $stmt->execute();
                $excludes_p_ids= $stmt->fetchAll();

                foreach ($excludes_p_ids as $ids_excludes){
                    $exclude_npis[]=$ids_excludes['npi'];
                }
                //get exclude organization
                $sql2="SELECT organization.id as organization_id
                FROM  organization_payer_exclude o
                LEFT JOIN organization ON organization.id = o.organization_id
                LEFT JOIN payer ON payer.id = o.payer_id
                WHERE payer.id=4";

                $stmt2 = $conn->prepare($sql2);
                $stmt2->execute();
                $excludes_o_ids= $stmt2->fetchAll();

                foreach ($excludes_o_ids as $org_id){
                    $providers_org=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$org_id['organization_id']));
                    if($providers_org!=null){
                        foreach ($providers_org as $provider){
                            $exclude_ids[]=$provider->getId();
                        }
                    }
                }

                foreach ($providers_ready as $provider_r){
                    if($provider_r['individual_credentialing']!="") {
                        if(stristr($provider_r['individual_credentialing'],'4') and $provider_r['individual_credentialing']!=14){
                            if (!stristr($provider_r['payers'], '4') and $provider_r['fccdata'] != "" and
                                !in_array($provider_r['id'], $exclude_ids) and !in_array($provider_r['npi_number'], $exclude_npis)) {
                                $providers_ready_final[] = $provider_r;
                            }
                        }
                    }
                }

                //get all facilities providers
                $sql2="SELECT p.id, p.first_name,p.last_name,p.npi_number,organization.name as org_name,p.state_lic,
                GROUP_CONCAT(DISTINCT payer.id ORDER BY payer.id ASC SEPARATOR ', ') AS `payers`,
                GROUP_CONCAT(DISTINCT fccdata.npi ORDER BY fccdata.id ASC SEPARATOR ', ') AS `fccdata`,
                GROUP_CONCAT(DISTINCT organization_credentialing.credentialing_status_id ORDER BY organization_credentialing.id ASC SEPARATOR ', ') AS `facility_credentialing`
                FROM  provider p
                LEFT JOIN organization ON organization.id = p.org_id
                LEFT JOIN organization_credentialing ON organization_credentialing.organization_id = organization.id
                LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
                LEFT JOIN fccdata ON fccdata.npi = p.npi_number
                LEFT JOIN payer ON provider_payer.payer_id = payer.id
                WHERE p.disabled=0 
                GROUP BY p.id";

                $stmt2 = $conn->prepare($sql2);
                $stmt2->execute();
                $providers_facilities_ready= $stmt2->fetchAll();

                foreach ($providers_facilities_ready as $provider_r){
                    if($provider_r['facility_credentialing']!="") {
                        if (!stristr($provider_r['payers'], '4') and $provider_r['fccdata'] != "" and
                            !in_array($provider_r['id'], $exclude_ids) and !in_array($provider_r['npi_number'], $exclude_npis)) {
                            $providers_ready_final[] = $provider_r;
                        }
                    }
                }
            }

            //Payer MMM of Florida Rules
            if($id==1){
                $sql="SELECT p.id, p.first_name,p.last_name,p.npi_number,organization.name as org_name,p.state_lic,p.caqh,p.medicare,
                GROUP_CONCAT(DISTINCT provider_credentialing.credentialing_status_id ORDER BY provider_credentialing.id ASC SEPARATOR ', ') AS `individual_credentialing`,
                GROUP_CONCAT(DISTINCT payer.id ORDER BY payer.id ASC SEPARATOR ', ') AS `payers`,
                GROUP_CONCAT(DISTINCT billing_address.region ORDER BY billing_address.id ASC SEPARATOR ', ') AS `regions`,
                GROUP_CONCAT(DISTINCT billing_address.group_medicare ORDER BY billing_address.id ASC SEPARATOR ', ') AS `group_medicare`,
                GROUP_CONCAT(DISTINCT degree.name ORDER BY degree.id ASC SEPARATOR ', ') AS `degrees`,
                IF(p.is_facility, 'Yes', 'No' ) AS `is_facility`
                FROM  provider p
                LEFT JOIN organization ON organization.id = p.org_id
                LEFT JOIN provider_credentialing ON provider_credentialing.provider_id = p.id
                LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
                LEFT JOIN payer ON provider_payer.payer_id = payer.id
                LEFT JOIN provider_degree ON provider_degree.provider_id = p.id
                LEFT JOIN degree ON provider_degree.degree_id = degree.id
                LEFT JOIN provider_billing_address ON provider_billing_address.provider_id = p.id
                LEFT JOIN billing_address ON billing_address.id = provider_billing_address.billing_address_id
                WHERE p.disabled=0
                GROUP BY p.id";

                $stmt = $conn->prepare($sql);
                $stmt->execute();
                $providers_ready= $stmt->fetchAll();

                foreach ($providers_ready as $provider_r){
                    if($provider_r['individual_credentialing']!=""){
                        if(!stristr($provider_r['payers'],'1') ){
                            if(stristr($provider_r['individual_credentialing'],'4') and $provider_r['individual_credentialing']!=14){
                                if(stristr($provider_r['regions'],'9') or stristr($provider_r['regions'],'10') or stristr($provider_r['regions'],'11')){
                                    if($provider_r['medicare']!="" and $provider_r['medicare']!="-" and $provider_r['medicare']!="N/A" or ($provider_r['group_medicare']!="" and $provider_r['group_medicare']!="-")){
                                        if(!in_array($provider_r['id'],$providers_ready_final_ids)){
                                            $providers_ready_final[]=$provider_r;
                                            $providers_ready_final_ids[]=$provider_r['id'];
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //query for get the providers credentialed by facilities
                $sql2="SELECT p.id, p.first_name,p.last_name,p.npi_number,organization.name as org_name,p.state_lic,p.caqh,p.medicare,
                GROUP_CONCAT(DISTINCT organization_credentialing.credentialing_status_id ORDER BY organization_credentialing.id ASC SEPARATOR ', ') AS `facility_credentialing`,
                GROUP_CONCAT(DISTINCT payer.id ORDER BY payer.id ASC SEPARATOR ', ') AS `payers`,
                GROUP_CONCAT(DISTINCT billing_address.region ORDER BY billing_address.id ASC SEPARATOR ', ') AS `regions`,
                GROUP_CONCAT(DISTINCT billing_address.group_medicare ORDER BY billing_address.id ASC SEPARATOR ', ') AS `group_medicare`,
                GROUP_CONCAT(DISTINCT degree.name ORDER BY degree.id ASC SEPARATOR ', ') AS `degrees`, 
                IF(p.is_facility, 'Yes', 'No' ) AS `is_facility`
                FROM  provider p
                LEFT JOIN organization ON organization.id = p.org_id
                LEFT JOIN organization_credentialing ON organization_credentialing.organization_id = organization.id
                LEFT JOIN billing_address  on organization.id = billing_address.organization_id    
                LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
                LEFT JOIN payer ON provider_payer.payer_id = payer.id
                LEFT JOIN provider_degree ON provider_degree.provider_id = p.id
                LEFT JOIN degree ON provider_degree.degree_id = degree.id
                WHERE p.disabled=0
                GROUP BY p.id";

                $stmt2 = $conn->prepare($sql2);
                $stmt2->execute();
                $providers_ready_f= $stmt2->fetchAll();

                foreach ($providers_ready_f as $provider_f){
                    if($provider_f['facility_credentialing']!=""){
                        if(!stristr($provider_f['payers'],'1') ){
                            if(stristr($provider_f['regions'],'9') or stristr($provider_f['regions'],'10') or stristr($provider_f['regions'],'11')){
                                if($provider_f['medicare']!="" and $provider_f['medicare']!="-" and $provider_f['medicare']!="N/A" or ($provider_f['group_medicare']!="" and $provider_f['group_medicare']!="-")){
                                    if(!in_array($provider_f['id'],$providers_ready_final_ids)){
                                        $providers_ready_final[]=$provider_f;
                                        $providers_ready_final_ids[]=$provider_f['id'];
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $sql_provider_payer="SELECT p.id, p.first_name,p.last_name,p.npi_number,organization.name as org_name, p.disabled
                FROM  provider p
                LEFT JOIN organization ON organization.id = p.org_id
                LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
                LEFT JOIN payer ON provider_payer.payer_id = payer.id
                WHERE payer.id=".$id."
                GROUP BY p.id";

            $stmt3 = $conn->prepare($sql_provider_payer);
            $stmt3->execute();
            $providers_payer= $stmt3->fetchAll();

            //get all excluded providers
            $sql_excluded="SELECT p.id, p.first_name,p.last_name,p.npi_number,organization.name as org_name
                FROM  provider p
                LEFT JOIN organization ON organization.id = p.org_id
                LEFT JOIN provider_payer_exclude ON provider_payer_exclude.provider_id = p.id
                WHERE provider_payer_exclude.payer_id=$id";

            $stmt4 = $conn->prepare($sql_excluded);
            $stmt4->execute();
            $providers_excluded= $stmt4->fetchAll();

            $delete_provider_exclude_form_ajax = $this->createCustomForm('PROVIDER_EXCLUDED_ID', 'DELETE', 'admin_delete_provider_excluded');
            $delete_organization_exclude_form_ajax = $this->createCustomForm('ORGANIZATION_EXCLUDED_ID', 'DELETE', 'admin_delete_organization_excluded');
            $delete_payer_contact_form_ajax = $this->createCustomForm('CONTACT_ID', 'DELETE', 'admin_delete_payer_contact');
            $delete_provider_asigned_form_ajax = $this->createCustomForm('PROVIDER_ID', 'DELETE', 'admin_delete_provider_asigned');
            $delete_payer_customer_county_form_ajax = $this->createCustomForm('CUSTOMER_COUNTY_ID', 'DELETE', 'admin_delete_county_customer');

            $organizations_excluded=$em->getRepository('App:OrganizationPayerExcluded')->findBy(array('payer'=>$id));

            $current_year=date('Y');
            $mmm_indicators=$em->getRepository('App\Entity\MMMIndicator')->findBy(array('year'=>$current_year,'payer'=>1),array('id'=>'ASC'));

            $data=[];
            foreach ($mmm_indicators as $indicator){
                $c_data=explode(',',$indicator->getData());
                $data[$indicator->getTopic()][$indicator->getIndicator()]=$c_data;
            }

            //get contacts for the payer
            $contacts=$em->getRepository('App:PayerContact')->findBy(array('payer'=>$document->getId()));
            $lines_of_business=$em->getRepository('App:LineOfBusiness')->findAll();

            $provider_to_remove=[];

            foreach ($providers_ready_final as $provider_f_check){
                if($id==1){

                }
            }

            return $this->render('payer/view.html.twig', array('document' => $document, 'counties' => $PayerLineCounty, 'payments' => $payments,'customers'=>$customers,
                'providers_ready'=>$providers_ready_final,'providers_payer'=>$providers_payer,'providers_excluded'=>$providers_excluded,
                'mmm_indicators'=>$mmm_indicators,'data'=>$data,'contacts'=>$contacts,'lines_of_business'=>$lines_of_business,
                'delete_provider_exclude_form_ajax' => $delete_provider_exclude_form_ajax->createView(),'organizations_excluded'=>$organizations_excluded,
                'delete_organization_exclude_form_ajax' => $delete_organization_exclude_form_ajax->createView(),
                'delete_payer_contact_form_ajax'=>$delete_payer_contact_form_ajax->createView(),
                'delete_provider_asigned_form_ajax'=>$delete_provider_asigned_form_ajax->createView(),
                'delete_payer_customer_county_form_ajax'=>$delete_payer_customer_county_form_ajax->createView()
                ));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_payer")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $db = $em->getConnection();

            $name = $request->get('name');
            $description = $request->get('description');
            $enabled = $request->get('enabled');
            $new = $request->get('new');
            $lineofbusinees = $request->get('linesselected');
            $counties = $request->get('countiesselected');
            $alias=$request->get('alias');

            if ($enabled == "")
                $enabled = false;
            else
                $enabled = true;

            $payer = new Payer();
            $payer->setName($name);
            $payer->setDescription($description);
            $payer->setEnabled($enabled);
            $payer->setAlias($alias);

            $em->persist($payer);
            $em->flush();

            $id = $payer->getId();

            $lineofbusinees = substr($lineofbusinees, 0, -1);
            $lbs = explode(",", $lineofbusinees);

            foreach ($lbs as $line) {
                $query1 = "INSERT INTO payer_lineofbusiness VALUES( $id, $line);";
                $stmt1 = $db->prepare($query1);
                $stmt1->execute();
            }

            if(isset($counties) and $counties!=null and $counties!=""){
                $counties = substr($counties, 0, -1);
                $countiesArray = explode(",", $counties);


                foreach ($countiesArray as $county) {

                    $idLine = (int)substr($county, 0, 1);
                    $idCounty = (int)substr($county, 1);

                    $line = $em->getRepository('App\Entity\LineOfBusiness')->find($idLine);
                    $county = $em->getRepository('App\Entity\CountyMatch')->find($idCounty);
                    $payer = $em->getRepository('App\Entity\Payer')->find($id);

                    $obj = new PayerLineCounty();
                    $obj->setLineOfBusiness($line);
                    $obj->setCounty($county);
                    $obj->setPayer($payer);

                    $em->persist($obj);
                    $em->flush();
                }
            }

            $this->addFlash(
                'success',
                'Payer has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_payer');
            }

            return $this->redirectToRoute('admin_payer');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_payer")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $db = $em->getConnection();

            $name = $request->get('name');
            $description = $request->get('description');
            $enabled = $request->get('enabled');
            $alias=$request->get('alias');
            $lineofbusinees = $request->get('linesselected');
            $counties = $request->get('countiesselected');

            $new = $request->get('new');
            $id = $request->get('id');

            if ($enabled == "")
                $enabled = false;
            else
                $enabled = true;

            $payer = $em->getRepository('App\Entity\Payer')->find($id);

            if ($payer == null) {
                $this->addFlash(
                    "danger",
                    "The Payer can't been updated!"
                );

                return $this->redirectToRoute('admin_payer');
            }

            if ($payer != null) {
                $payer->setName($name);
                $payer->setAlias($alias);
                $payer->setDescription($description);
                $payer->setEnabled($enabled);

                $em->persist($payer);
                $em->flush();

                //search and delete lines before saved
                $linesBack = $payer->getLineOfBusiness();

                if ($linesBack != null) {
                    foreach ($linesBack as $lb) {
                        if ($lb != null) {
                            $payer->removeLineOfBusiness($lb);
                        }
                    }
                }
                $em->persist($payer);
                $em->flush();

                $lineofbusinees = substr($lineofbusinees, 0, -1);
                $lbs = explode(",", $lineofbusinees);

                foreach ($lbs as $line) {
                    $query1 = "INSERT INTO payer_lineofbusiness VALUES( $id, $line);";
                    $stmt1 = $db->prepare($query1);
                    $stmt1->execute();
                }

                //remove Actual counties selelected
                $plc = $em->getRepository('App\Entity\PayerLineCounty')->find($id);

                if ($plc != null) {
                    foreach ($plc as $p) {
                        $em->remove($p);
                        $em->flush();
                    }
                }

                $counties = substr($counties, 0, -1);
                $countiesArray = explode(",", $counties);

                foreach ($countiesArray as $county) {
                    $idLine = (int)substr($county, 0, 1);
                    $idCounty = (int)substr($county, 1);

                    $line = $em->getRepository('App\Entity\LineOfBusiness')->find($idLine);
                    $county = $em->getRepository('App\Entity\CountyMatch')->find($idCounty);
                    $payer = $em->getRepository('App\Entity\Payer')->find($id);

                    $obj = new PayerLineCounty();
                    $obj->setLineOfBusiness($line);
                    $obj->setCounty($county);
                    $obj->setPayer($payer);

                    $em->persist($obj);
                    $em->flush();
                }
            }

            $this->addFlash(
                "success",
                "Payer has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_payer');
            }

            return $this->redirectToRoute('admin_payer');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_payer",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $payer = $em->getRepository('App\Entity\Payer')->find($id);
            $removed = 0;
            $message = "";

            if ($payer) {
                try {
                    $em->remove($payer);
                    $em->flush();
                    $removed = 1;
                    $message = "The Payer has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The payer can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete-provider-excluded/{id}", name="admin_delete_provider_excluded",methods={"POST","DELETE"})
     */
    public function deleteProviderExcludedAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $payer_id = $request->get('payer_id');
            $provider_id = $request->get('provider_id');

            $payer = $em->getRepository('App\Entity\Payer')->find($payer_id);
            $provider = $em->getRepository('App\Entity\Provider')->find($provider_id);

            $removed = 0;
            $message = "";

            if ($payer!=null and $provider!=null) {
                try {

                    $provider->removePayerExcluded($payer);
                    $em->persist($provider);
                    $em->flush();

                    $removed = 1;
                    $message = "The Provider Excluded has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The payer can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete-organization-excluded/{id}", name="admin_delete_organization_excluded",methods={"POST","DELETE"})
     */
    public function deleteOrganizationExcludedAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $payer_id = $request->get('payer_id');
            $provider_id = $request->get('organization_id');

            $payer = $em->getRepository('App\Entity\Payer')->find($payer_id);
            $organization = $em->getRepository('App\Entity\Organization')->find($provider_id);

            $removed = 0;
            $message = "";

            if ($payer!=null and $organization!=null) {
                try {

                    $organization->removePayerExcluded($payer);
                    $em->persist($organization);
                    $em->flush();

                    $removed = 1;
                    $message = "The Organization Excluded has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Organization Excluded can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete-payer-contact/{id}", name="admin_delete_payer_contact",methods={"POST","DELETE"})
     */
    public function deletePayerContactdAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $contact_id = $request->get('id');

            $contact = $em->getRepository('App:PayerContact')->find($contact_id);

            $removed = 0;
            $message = "";

            if ($contact!=null) {
                try {

                    $em->remove($contact);
                    $em->flush();

                    $removed = 1;
                    $message = "The Payer Contact has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Payer Contact can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete-provider-asigned/{id}", name="admin_delete_provider_asigned",methods={"POST","DELETE"})
     */
    public function deletePayerProviderAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $provider_id = $request->get('provider_id');
            $payer_id = $request->get('payer_id');

            $provider = $em->getRepository('App\Entity\Provider')->find($provider_id);
            $payer = $em->getRepository('App\Entity\Payer')->find($payer_id);


            $removed = 0;
            $message = "";

            if ($provider!=null && $payer!=null) {
                try {

                    $provider->removePayer($payer);
                    $em->persist($provider);
                    $em->flush();

                    $removed = 1;
                    $message = "The Asigned Provider has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Asigned Provider can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete-multiple-providers-asigned", name="admin_delete_multiple_providers_asigned",methods={"POST","DELETE"})
     */
    public function deleteMultipleProvidersAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $payer_id = $request->get('payer_id');

            $payer = $em->getRepository('App\Entity\Payer')->find($payer_id);

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";


            foreach ($ids as $id) {
                $provider = $em->getRepository('App\Entity\Provider')->find($id);
                if ($provider != null && $payer != null) {
                    try {

                        $provider->removePayer($payer);
                        $em->persist($provider);
                        $em->flush();

                        $removed = 1;
                        $message = "The Asigned Provider has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Asigned Provider can't be removed";
                    }
                }
            }
            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_payer",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $payer = $payer = $em->getRepository('App\Entity\Payer')->find($id);

                if ($payer) {
                    try {
                        $em->remove($payer);
                        $em->flush();
                        $removed = 1;
                        $message = "The Payer has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Payer can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/add_payer_provider_ajax", name="admin_add_provider_payer_ajax",methods={"POST"})
     */
    public function addPayer(Request $request){
        $em=$this->getDoctrine()->getManager();

        $id = $request->get('id');
        $payer=$request->get('payer');

        $payerObj=$em->getRepository('App\Entity\Payer')->find($payer);
        $provider = $em->getRepository('App\Entity\Provider')->find($id);

        if($provider!=null){
            $existPayer=false;
            $payers=$provider->getPayers();

            if($payers!=null){
                foreach ($payers as $p){
                    if($p!=null){
                        if($p->getId()==$payer){
                            $existPayer=true;
                        }
                    }
                }
            }
            if($existPayer==false){
                $provider->addPayer($payerObj);
                $em->persist($provider);
                $em->flush();
            }
        }

        return new Response(
            json_encode(array('id' =>111)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/add_payer_provider_ajax_m", name="admin_add_provider_payer_ajax_m",methods={"POST"})
     */
    public function addPayerProviders(Request $request){
        $em=$this->getDoctrine()->getManager();

        $ids = $request->get('ids');
        $payer=$request->get('payer');

        $payerObj=$em->getRepository('App\Entity\Payer')->find($payer);

        foreach ($ids as $id){
            $provider = $em->getRepository('App\Entity\Provider')->find($id);

            if($provider!=null){
                $existPayer=false;
                $payers=$provider->getPayers();

                if($payers!=null){
                    foreach ($payers as $p){
                        if($p!=null){
                            if($p->getId()==$payer){
                                $existPayer=true;
                            }
                        }
                    }
                }
                if($existPayer==false){
                    $provider->addPayer($payerObj);
                    $em->persist($provider);
                    $em->flush();
                }
            }
        }

        return new Response(
            json_encode(array('id' =>111)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/remove_payer_exclude_provider_ajax", name="admin_remove_provider_payer_exclude_ajax",methods={"POST"})
     */
    public function removePayerExclude(Request $request){
        $em=$this->getDoctrine()->getManager();

        $id = $request->get('id');
        $payer=$request->get('payer');

        return new Response(
            json_encode(array('id' =>111)), 200, array('Content-Type' => 'application/json')
        );
    }


    /**
     * @Route("/keys-report-mmm-ajax", name="keys_report_mmm_ajax",methods={"POST"})
     */
    public function mmmKeysReportAjax(Request $request){
        $em=$this->getDoctrine()->getManager();
        $year=$request->get('year');
        $conn = $em->getConnection();


        $sql_provider_payer="SELECT p.id
                FROM  provider p
                LEFT JOIN organization ON organization.id = p.org_id
                LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
                LEFT JOIN payer ON provider_payer.payer_id = payer.id
                WHERE payer.id=1 and p.disabled=1
                GROUP BY p.id";

        $stmt3 = $conn->prepare($sql_provider_payer);
        $stmt3->execute();
        $providers_payer= $stmt3->fetchAll();

        $total=count($providers_payer);

        return new Response(
            json_encode(array('id' =>$total)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/create-data-mmm-indicator/{payer}/{year}", name="admin_create_data_mmm_indicators")
     */
    public function createDataMMMIndicator($payer, $year){
        $em=$this->getDoctrine()->getManager();

        $data="0,0,0,0,0,0,0,0,0,0,0,0,0";

        $contrating_indicators=[
            'Number of providers that requested to be part of the network',
            'Number of providers that signed contract',
            'Number of provider voluntarily terminated from the network',
            'Number of providers involuntarily terminated by the network'];

        foreach ($contrating_indicators as $contrating_indicator){

            $mmm_indicator=new MMMIndicator();
            $mmm_indicator->setTopic('Contrating');
            $mmm_indicator->setIndicator($contrating_indicator);
            $mmm_indicator->setData($data);
            $mmm_indicator->setYear($year);
            $mmm_indicator->setPayer($em->getRepository('App\Entity\Payer')->find($payer));

            $em->persist($mmm_indicator);
            $em->flush();
        }

        $credentialing_indicators=[
            'Number of applications received',
            'Number of applications reviewed',
            'Number of applications approved',
            'Number of applications denied',
            'Number of applications pended'
        ];

        foreach ($credentialing_indicators as $credentialing_indicator){
            $mmm_indicator=new MMMIndicator();
            $mmm_indicator->setTopic('Credentialing');
            $mmm_indicator->setIndicator($credentialing_indicator);
            $mmm_indicator->setData($data);
            $mmm_indicator->setYear($year);
            $mmm_indicator->setPayer($em->getRepository('App\Entity\Payer')->find($payer));
            $em->persist($mmm_indicator);
            $em->flush();
        }

        $init_credentialing_indicators=[
            'New Cases approved in Credentialing Committee (Initial Credentialing)',
            'New Cases denied in Credentialing Committee (Initial Credentialing)',
            'TAT Initial Credentialing ≤ 180 days',
            'Results of quality audit completed to initial credentialing provider files'
        ];

        foreach ($init_credentialing_indicators as $init_credentialing_indicator){
            $mmm_indicator=new MMMIndicator();
            $mmm_indicator->setTopic('Initial Credentialing');
            $mmm_indicator->setIndicator($init_credentialing_indicator);
            $mmm_indicator->setData($data);
            $mmm_indicator->setYear($year);
            $mmm_indicator->setPayer($em->getRepository('App\Entity\Payer')->find($payer));
            $em->persist($mmm_indicator);
            $em->flush();
        }

        $recredentialing_indicators=[
            'Number of participating providers that are due for recredentialing (36 months period)',
            'Re-Credentialing cases approved in Credentialing Committee',
            'Re-credentialing cases denied in Credenting Committee',
            'Number of files terminated for not compliance with ≤ 36 months cycle',
            'TAT Re-Credentialing ≤ 36 months',
            'Results of quality audit completed to recredentialing provider files'
        ];

        foreach ($recredentialing_indicators as $recredentialing_indicator){
            $mmm_indicator=new MMMIndicator();
            $mmm_indicator->setTopic('Recredentialing');
            $mmm_indicator->setIndicator($recredentialing_indicator);
            $mmm_indicator->setData($data);
            $mmm_indicator->setYear($year);
            $mmm_indicator->setPayer($em->getRepository('App\Entity\Payer')->find($payer));
            $em->persist($mmm_indicator);
            $em->flush();
        }


        return new Response('All process completed');
    }

    /**
     * @Route("/create-report-key-indicator", name="admin_create_report_key_indicator",methods={"POST"})
     */
    public function exportKeyIndicatorMMM(Request $request){
        set_time_limit(3600);
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');

        $spreadsheet = $reader->load($url.'mmm_dashboard_template.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $em=$this->getDoctrine()->getManager();

        $year=$request->get('year_data');
        $payer=$request->get('current_payer');

        $contrating_indicators=$em->getRepository('App\Entity\MMMIndicator')->findBy(array('topic'=>'Contrating','year'=>$year,'payer'=>$payer),array('id'=>'ASC'));

        $cont=5;
        foreach ($contrating_indicators as $indicator){
            $cell_B='B'.$cont; $cell_C='C'.$cont; $cell_D='D'.$cont; $cell_E='E'.$cont; $cell_F='F'.$cont; $cell_G='G'.$cont;
            $cell_H='H'.$cont; $cell_I='I'.$cont; $cell_J='J'.$cont; $cell_K='K'.$cont; $cell_L='L'.$cont; $cell_M='M'.$cont;
            $cell_N='N'.$cont;
            $data=explode(",",$indicator->getData());

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_B, $data[0]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $data[1]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $data[2]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, $data[3]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $data[4]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_G, $data[5]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_H, $data[6]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $data[7]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $data[8]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $data[9]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $data[10]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $data[11]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, $data[12]);
            $cont++;
        }

        $spreadsheet->setActiveSheetIndex(1);
        $credentialing_indicators=$em->getRepository('App\Entity\MMMIndicator')->findBy(array('topic'=>'Credentialing','year'=>$year,'payer'=>$payer),array('id'=>'ASC'));
        $cont=5;
        foreach ($credentialing_indicators as $indicator){
            $cell_B='B'.$cont; $cell_C='C'.$cont; $cell_D='D'.$cont; $cell_E='E'.$cont; $cell_F='F'.$cont; $cell_G='G'.$cont;
            $cell_H='H'.$cont; $cell_I='I'.$cont; $cell_J='J'.$cont; $cell_K='K'.$cont; $cell_L='L'.$cont; $cell_M='M'.$cont;
            $cell_N='N'.$cont;
            $data=explode(",",$indicator->getData());

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_B, $data[0]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $data[1]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $data[2]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, $data[3]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $data[4]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_G, $data[5]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_H, $data[6]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $data[7]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $data[8]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $data[9]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $data[10]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $data[11]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, $data[12]);
            $cont++;
        }

        $init_credentialing_indicators=$em->getRepository('App\Entity\MMMIndicator')->findBy(array('topic'=>'Initial Credentialing','year'=>$year,'payer'=>$payer),array('id'=>'ASC'));
        $cont=11;
        foreach ($init_credentialing_indicators as $indicator){
            $cell_B='B'.$cont; $cell_C='C'.$cont; $cell_D='D'.$cont; $cell_E='E'.$cont; $cell_F='F'.$cont; $cell_G='G'.$cont;
            $cell_H='H'.$cont; $cell_I='I'.$cont; $cell_J='J'.$cont; $cell_K='K'.$cont; $cell_L='L'.$cont; $cell_M='M'.$cont;
            $cell_N='N'.$cont;
            $data=explode(",",$indicator->getData());

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_B, $data[0]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $data[1]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $data[2]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, $data[3]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $data[4]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_G, $data[5]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_H, $data[6]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $data[7]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $data[8]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $data[9]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $data[10]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $data[11]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, $data[12]);
            $cont++;
        }

        $re_credentialing_indicators=$em->getRepository('App\Entity\MMMIndicator')->findBy(array('topic'=>'Recredentialing','year'=>$year,'payer'=>$payer),array('id'=>'ASC'));
        $cont=16;
        foreach ($re_credentialing_indicators as $indicator){
            $cell_B='B'.$cont; $cell_C='C'.$cont; $cell_D='D'.$cont; $cell_E='E'.$cont; $cell_F='F'.$cont; $cell_G='G'.$cont;
            $cell_H='H'.$cont; $cell_I='I'.$cont; $cell_J='J'.$cont; $cell_K='K'.$cont; $cell_L='L'.$cont; $cell_M='M'.$cont;
            $cell_N='N'.$cont;
            $data=explode(",",$indicator->getData());

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_B, $data[0]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $data[1]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $data[2]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, $data[3]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $data[4]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_G, $data[5]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_H, $data[6]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $data[7]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $data[8]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $data[9]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $data[10]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $data[11]);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, $data[12]);
            $cont++;
        }

        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="BSN_Dashboards_MMM_'.date('Ymd').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }

    /**
     * @Route("/getIndicatorDataAjax", name="admin_indicators_ajax",methods={"POST","GET"})
     */
    public function IndicatorAjax(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $year = $request->get('year');
        $payer=$request->get('payer');
        $mmm_indicators=$em->getRepository('App\Entity\MMMIndicator')->findBy(array('year'=>$year,'payer'=>$payer),array('id'=>'ASC'));

        $data=[];
        $ids_btn_data=[];

        foreach ($mmm_indicators as $indicator){
            $c_data=explode(',',$indicator->getData());
            $data[$indicator->getTopic()][$indicator->getIndicator()]=$c_data;
            $ids_btn_data[]=$indicator->getId();
        }

        return new Response(
            json_encode(array('data' =>$data,'ids_btn_data'=>$ids_btn_data)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/save-indicator-ajax", name="admin_save_indicator_ajax",methods={"POST"})
     */
    public function saveIndicatorAjax(Request $request){
        $em=$this->getDoctrine()->getManager();

        $id = $request->get('id');
        $newdata=$request->get('data');
        $year = $request->get('year');
        $payer=$request->get('payer');
        if($payer==3) {
            if ($year == '2020') {
                $id = intval($id + 19);
            }
        }

        //set ids for MMM
        if($payer==1){
            $id=intval($id);
            if($year=='2020'){
                $id=intval($id-19);
            }
        }

        $data_indicator=$em->getRepository('App\Entity\MMMIndicator')->find($id);
        $data_indicator->setData($newdata);
        $em->persist($data_indicator);
        $em->flush();

        return new Response(
            json_encode(array('id' =>$id)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/uplaod-customer-county/{payer}", name="admin_upload-customer-county")
     */
    public function uploadCustomerCounty($payer){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            $em=$this->getDoctrine()->getManager();
            $lines_of_business=$em->getRepository('App:LineOfBusiness')->findAll();
            return $this->render('payer/upload-customer-county.html.twig',['payer'=>$payer,'lines_of_business'=>$lines_of_business]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/uplaod-customer-process", name="admin_upload-customer-county-process", methods={"POST"})
    */
    public function uploadCustomerCountyProcess(Request $request, CustomerCountyFileUploader $fileUploader){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em=$this->getDoctrine()->getManager();

            $payer_id=$request->get('payer');
            $payer=$em->getRepository('App:Payer')->find($payer_id);
            $line_business=$request->get('line_business');

            $line_businessObJ=$em->getRepository('App:LineOfBusiness')->find($line_business);

            $data_array=[];
            $wrong_data_array=[];

            $file = $request->files->get('file_customer_county');
            if(isset($file) and $file!=""){
                $extension=$file->guessExtension();
                $file_name="join_".uniqid()."_".date('Ymdsi');

                $file = $fileUploader->upload($file, $file_name);
                if ($file != "error") {
                   //read file name
                    $projectDir = $this->getParameter('kernel.project_dir');
                    $url= $projectDir."/public/uploads/customer_county";
                    $fileToRead = $url."/" . $file_name.".".$extension;

                    $reader = new Reader\Xlsx();
                    $reader->setReadDataOnly(true);
                    $spreadsheet = $reader->load($fileToRead);
                    $spreadsheet->setActiveSheetIndex(0);
                    $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
                    $cont=1;

                    foreach ($sheetData as $sheet) {
                        if ($cont > 1) {
                            if ($sheet['A'] != null or $sheet['A'] != "") {
                                $county_name=$sheet['A'];
                                $value=$sheet['B'];

                                $county=$em->getRepository('App:CountyMatch')->findOneBy(array('name'=>$county_name));
                                $data=[];
                                $wrong_data=[];
                                if($county){
                                    $unique_id=uniqid();
                                    $data['id']=$unique_id;
                                    $data['county']=$county;
                                    $data['value']=$value;

                                    $data_array[]=$data;

                                    //save or update the data
                                    $customer_county=$em->getRepository('App:PayerCustomerCounty')->findOneBy(array('payer'=>$payer_id,'county'=>$county->getId(),'lineofbusiness'=>$line_business));

                                    if($customer_county==null){
                                        $customer_county=new PayerCustomerCounty();
                                    }

                                    $customer_county->setPayer($payer);
                                    $customer_county->setCounty($county);
                                    $customer_county->setLineofbusiness($line_businessObJ);
                                    $customer_county->setTotal($value);

                                    $em->persist($customer_county);
                                }else{
                                    $unique_id=uniqid();
                                    $wrong_data['id']=$unique_id;
                                    $wrong_data['county']=$county_name;
                                    $wrong_data['value']=$value;

                                    $wrong_data_array[]=$wrong_data;
                                }
                            }
                        }
                        $cont++;
                    }

                    $em->flush();
                }
            }

            $all_counties=$em->getRepository('App:CountyMatch')->findAll();

            return  $this->render('payer/preview-customer-county.html.twig',[
                'data_array'=>$data_array,'wrong_data_array'=>$wrong_data_array,'payer'=>$payer,'line_business'=>$line_business,'line_businessObJ'=>$line_businessObJ,
                'all_counties'=>$all_counties
            ]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete-provider-payer", name="admin_delete_payer_provider",methods={"POST","DELETE"})
     */
    public function deleteProviderPayerAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $provider_id = $request->get('provider_id');
            $payer_id = $request->get('payer_id');

            $provider = $em->getRepository('App:Provider')->find($provider_id);
            $removed = 0;
            $message = "";

            if ($provider) {
                try {


                    $removed = 1;
                    $message = "The Payer has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The payer can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

}