<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Organization;
use App\Entity\Provider;
use App\Entity\ViewProvider;
use App\Service\OrganizationFileUploader;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * @Route("/admin/organization_p")
 */
class OrganizationPProccessController extends AbstractController{

    /**
     * @Route("/index", name="admin_organizationp_list")
     *
     */
    public function index(){
        $em=$this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App\Entity\OrganizationP')->findAll();

        return $this->render('organization/organizationp_list.html.twig', array('organizations'=>$organizations));
    }


}
