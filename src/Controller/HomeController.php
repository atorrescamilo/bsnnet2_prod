<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\Organization;
use App\Entity\OrganizationContact;
use App\Entity\Provider;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/admin")
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/home", name="home")
     */
    public function index(ValidatorInterface $validator){

        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $roles = $em->getRepository('App\Entity\Role')->findAll();

            $organizationsT = $em->getRepository('App\Entity\Organization')->findBy(array('disabled'=>0));
            $providers = $em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));
            $payers = $em->getRepository('App\Entity\Payer')->findAll();
            $prospects = $em->getRepository('App\Entity\Prospect')->findAll();

            $providersResult=array();
            $addressResult=array();
            $contactResult=array();
            $organization=$this->getUser()->getOrganization();

            $hasFcc=0;
            $hasMMM=0;
            $credentialingProcess=[];
            $org_facility=false;
            if($organization!=null){
                $org_id=$organization->getId();
                $providersResult=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));
                $addressResult=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                $contactResult=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));

                $sql="SELECT p.id,GROUP_CONCAT(DISTINCT `payer`.`id` ORDER BY `payer`.`id` ASC SEPARATOR ',') AS `payers`
                FROM  provider p
                LEFT JOIN organization ON organization.id = p.org_id
                LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
                LEFT JOIN payer ON payer.id = provider_payer.payer_id
                WHERE p.disabled=0 and p.org_id=$org_id
                GROUP BY p.id
                ORDER BY p.id";

                $stmt = $conn->prepare($sql);
                $stmt->execute();
                $prdrs= $stmt->fetchAll();


                foreach ($prdrs as $pro){
                   if(stristr($pro['payers'], '1')){
                       $hasMMM=1;
                   }
                    if(stristr($pro['payers'], '4')){
                        $hasFcc=1;
                    }
                }

                $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                if($address!=null){
                    foreach ($address as $addr){
                        if($addr->getIsFacility()==true){
                            $org_facility=true;
                            break;
                        }
                    }
                }

                $credentialingProcess=$em->getRepository('App\Entity\OrganizationCredentialing')->findBy(array('organization'=>$organization->getId()));
            }

            $users=$em->getRepository('App\Entity\User')->findBy(array(),array('id'=>'DESC'));
            $usersP=array();

            foreach ($users as $user){
                if($user->getOrganization()!=null and $user->getEnabled()==true){
                   $usersP[]=$user;
                }
            }
            $totaluserP=count($usersP);

            //section for check the items to be check
            $organization_check=false;
            $providers_check=false;
            $contacts_check=false;
            $training_attestation=false;

            //validation for information
            $check_org_accreditation=false;
            $check_org_groupnpi=false;
            $check_org_billingnpi=false;
            $check_org_tin=false;
            $check_org_legal_name_tin_owner=false;
            $check_org_billing_phone=false;
            $check_org_phone=false;

            $contactResult2=array();
            $contact_check=array();
            if($organization!=null){
                $contactResult2=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));
                $currentYear=date('Y');
                $training=$em->getRepository('App\Entity\OrganizationTrainingAttestation')->findOneBy(array('organization'=>$organization->getId(),'year'=>$currentYear));

                if($training==false){
                    $training_attestation=true;
                }

                $org_npi=$organization->getGroupNpi();
                if($org_npi==null or $org_npi==""){
                    $organization_check=true;
                    $check_org_groupnpi=true;
                }

                $org_billingnpi=$organization->getBillingNpi();
                if($org_billingnpi=="" or $org_billingnpi==null){
                    $organization_check=true;
                    $check_org_billingnpi=true;
                }

                $org_tin=$organization->getTinNumber();
                if($org_tin=="" or $org_tin==null){
                    $organization_check=true;
                    $check_org_tin=true;
                }

                $org_legal_name_tin_owner=$organization->getLegalNameTinOwner();
                if($org_legal_name_tin_owner=="" or $org_legal_name_tin_owner==null){
                    $organization_check=true;
                    $check_org_legal_name_tin_owner=true;
                }

                $org_billing_phone=$organization->getBillingPhone();
                if($org_billing_phone=="" or $org_billing_phone==null) {
                    $organization_check=true;
                    $check_org_billing_phone=true;
                }

                $org_phone=$organization->getPhone();
                if($org_phone==null or $org_phone==""){
                    $organization_check=true;
                    $check_org_phone=true;
                }
            }

            if(count($contactResult2)==0){
                $contacts_check=true;
            }else{
                foreach ($contactResult2 as $contact2){
                     if($contact2->getName()==""){
                         $contact_check[]=$contact2;
                         $contacts_check=true;
                     }
                    if($contact2->getPhone()==""){
                        $contact_check[]=$contact2;
                        $contacts_check=true;
                    }
                    if(sizeof($contact2->getBestChoices())==0){
                        $contact_check[]=$contact2;
                        $contacts_check=true;
                    }
                    if($contact2->getEmail()==""){
                        $contact_check[]=$contact2;
                        $contacts_check=true;
                    }
                }
            }

            //get address for check status
            $addresses_check=[];
            $addr_check=false;
            $address_check=false;
            $addr_type_check=false;
            $columns_addr=[];
            $columns_addr['phone']=false;

            $primary_addr=false;
            $billing_addr=false;
            $mailing_addr=false;
            $is_facility_addr=false;

            if($organization!=null){
                $addresses=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));

                if(count($addresses)==0){
                    $address_check=true;
                    $addr_check=true;
                }else{
                    foreach ($addresses as $addr){

                        $errors = $validator->validate($addr);
                        if(count($errors)>0){
                            $addresses_check[]=$addr;
                            $address_check=true;
                            $addr_check=true;
                        }

                        if($addr->getPhoneNumber()=="" or $addr->getPhoneNumber()==null){
                            $columns_addr['phone']=true;
                            $addresses_check[]=$addr;
                            $address_check=true;
                            $addr_check=true;
                        }

                        if($addr->getPrimaryAddr()==true){
                            $primary_addr=true;
                        }
                        if($addr->getBillingAddr()==true){
                            $billing_addr=true;
                        }
                        if($addr->getMailingAddr()==true){
                            $mailing_addr=true;
                        }
                        if($addr->getIsFacility()==true){
                            $is_facility_addr=true;
                        }
                    }
                }
            }

            if($primary_addr==false or $billing_addr==false or $mailing_addr==false){
                $addr_type_check=true;
                $address_check=true;
            }

            $provider_check=[];
            $providers_id=[];
            //get providers for check status on provider portal
            foreach ($providersResult as $provider){

                $id=$provider->getId();
                $errors = $validator->validate($provider);

                if(count($errors)>0){
                    $providers_check=true;
                    $provider_check[]=$provider;
                    $providers_id[]=$id;
                }
            }

            $userValdiate=$em->getRepository('App\Entity\ProviderToVerify')->findAll();
            $currentYear=date('Y');
            $cmsTrainings=$em->getRepository('App\Entity\OrganizationTrainingAttestation')->findBy(array('year'=>$currentYear));

            $billing=$em->getRepository('App\Entity\BillingAddress')->findAll();

            $totalBilling=count($billing);

            //validate Roster
            $validateRoster=null;
            $currentDateF=date('m-Y');

            if($organization!=null){
               $validateRoster=$em->getRepository('App\Entity\OrganizationValidateRoster')->findOneBy(array('date'=>$currentDateF,'organization'=>$organization));
            }

            //get requests to access to organization
            $requestsOrg=$em->getRepository('App\Entity\UserOrgRequest')->findAll();

            //load all line of business
            $lineofBusiness=$em->getRepository('App\Entity\LineOfBusiness')->findAll();
            $organization_lob=[];
            $organization_lob_completed=false;

            if($organization!=null){
                $organization_lob=$em->getRepository('App\Entity\OrganizationLineofBusiness')->findBy(array('organization'=>$organization->getId()));
            }

            if(count($organization_lob)==4){
                $organization_lob_completed=true;
            }

            $conn = $em->getConnection();
            //organization statistics
            $sql_disabled="SELECT o.id FROM  organization o WHERE o.disabled=1";
            $sql_active="SELECT o.id FROM  organization o WHERE o.status_id=2";
            $sql_pending="SELECT o.id FROM  organization o WHERE o.status_id=1";

            $stmt = $conn->prepare($sql_disabled);
            $stmt->execute();
            $organizations_disabled= $stmt->fetchAll();

            $stmt1 = $conn->prepare($sql_active);
            $stmt1->execute();
            $organizations_active= $stmt1->fetchAll();

            $stmt2 = $conn->prepare($sql_pending);
            $stmt2->execute();
            $organizations_pending= $stmt2->fetchAll();

            $organizations_status=[];

            $organizations_status[0]=count($organizations_disabled);
            $organizations_status[1]=count($organizations_pending);
            $organizations_status[2]=count($organizations_active);

            $organization_types=[];
            $sql_pending="SELECT o.id FROM  organization o WHERE o.provider_type='Individual'";
            $stmt3 = $conn->prepare($sql_pending);
            $stmt3->execute();
            $organizations_individual= $stmt3->fetchAll();

            $sql_pending="SELECT o.id FROM  organization o WHERE o.provider_type='Group'";
            $stmt4 = $conn->prepare($sql_pending);
            $stmt4->execute();
            $organizations_group= $stmt4->fetchAll();

            $sql_pending="SELECT o.id FROM  organization o WHERE o.provider_type='Organization'";
            $stmt5 = $conn->prepare($sql_pending);
            $stmt5->execute();
            $organizations_facilities= $stmt5->fetchAll();

            $organization_types[0]=count($organizations_individual);
            $organization_types[1]=count($organizations_group);
            $organization_types[2]=count($organizations_facilities);

            $providersCred=[];
            $prov=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization));
            if($prov){
                foreach ($prov as $item){
                    $cred=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('npi_number'=>$item->getNpiNumber()));
                    if($cred!=null and count($cred)>0){
                        $providersCred[]=$item;
                    }
                }
            }

            //check if the organization have ready and valid Directory update
            $total_Records=0;
            $show_alert_directory_update=true;
            if($organization!=null){
                $directory_update_records=$em->getRepository('App:DirectoryUpdate')->findBy(array('organization'=>$organization),array('id'=>'ASC'));
                if($directory_update_records){
                    $total_Records=count($directory_update_records);
                    $record=$directory_update_records[intval($total_Records-1)];

                    $last_date=$record->getDate();
                    $last_date_array=explode('/',$last_date);

                    $begin= $last_date_array[2]."-".$last_date_array[0]."-".$last_date_array[1]." 00:00:00";

                    $current_date=date('Y-m-d H:i:s');

                    $datetime1=new \DateTime($begin);
                    $datetime2=new \DateTime($current_date);
                    $interval=$datetime2->diff($datetime1);
                    $intervalMeses=$interval->format("%m");
                    $intervalAnos = $interval->format("%y")*12;
                    $moths=$intervalMeses+$intervalAnos;

                    if($total_Records>0 and $moths<3){
                        $show_alert_directory_update=false;
                    }
                }
            }

            $organizations=$em->getRepository('App:Organization')->findBy(array('disabled'=>0));

            //join us status
            $joinUs=$em->getRepository('App:JOIN')->findAll();

            $join_pending=0;
            $join_canceled=0;
            $join_approved=0;

            foreach ($joinUs as $ju){
                if($ju->getStatus()->getId()==1){
                    $join_pending++;
                }
                if($ju->getStatus()->getId()==2){
                    $join_canceled++;
                }
                if($ju->getStatus()->getId()==3){
                    $join_approved++;
                }
            }


            return $this->render('home/index.html.twig', [
                'users' => $users, 'roles' => $roles, 'organizations' => $organizationsT,'organizations'=>$organizations,
                'providers' => $providers,'organization'=>$organization,'organization_lob_completed'=>$organization_lob_completed,
                'payers' => $payers,'cmsTrainings'=>$cmsTrainings,'hasMMM'=>$hasMMM,'hasFcc'=>$hasFcc,
                'prospect' => $prospects,'requestsOrg'=>$requestsOrg,
                'providersResult'=>$providersResult,'organization_types'=>$organization_types,
                'addressResult'=>$addressResult,'organization_lob'=>$organization_lob,
                'contactResult'=>$contactResult,'totaluserP'=>$totaluserP,'userValdiate'=>$userValdiate,'usersP'=>$usersP,
                'organization_check'=>$organization_check,'providers_check'=>$providers_check,'address_check'=>$address_check,
                'contacts_check'=>$contacts_check,'training_attestation'=>$training_attestation,'contactResult2'=>$contactResult2,
                'contact_check'=>$contact_check,'addresses_check'=>$addresses_check,'lineofBusiness'=>$lineofBusiness,
                'columns_addr'=>$columns_addr,'provider_check'=>$provider_check, 'totalBilling'=>$totalBilling,'validateRoster'=>$validateRoster,
                'addr_type_check'=>$addr_type_check,'addr_check'=>$addr_check,'organizations_status'=>$organizations_status,
                'primary_addr'=>$primary_addr,'billing_addr'=>$billing_addr,'mailing_addr'=>$mailing_addr,
                'credentialingProcess'=>$credentialingProcess,'providersCred'=>$providersCred,'org_facility'=>$org_facility,
                'check_org_accreditation'=>$check_org_accreditation,'check_org_groupnpi'=>$check_org_groupnpi,'check_org_billingnpi'=>$check_org_billingnpi,
                'check_org_tin'=>$check_org_tin,'check_org_legal_name_tin_owner'=>$check_org_legal_name_tin_owner,'check_org_billing_phone'=>$check_org_billing_phone,
                'check_org_phone'=>$check_org_phone,'show_alert_directory_update'=>$show_alert_directory_update,
                'join_pending'=>$join_pending,'join_canceled'=>$join_canceled,'join_approved'=>$join_approved
            ]);

        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/globla_search", name="global_search")
     */
    public function search(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $filter=$request->get('filter');
            $contResult=0;

            $result=array();
            $totalResult=array();

            //test search
            $organizations=$em->getRepository('App\Entity\Organization')->findAll();

            if($organizations!=null){
                foreach ($organizations as $organization){
                    $result['id']=$organization->getId();

                    $totalResult[]=$result;
                    $contResult++;
                }
            }

            return new Response(
                json_encode(array('contResult' => $contResult,'results'=>$totalResult)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/user-organizations", name="user_organizations")
     */
    public function userOrganizations(Request $request){
        $user=$this->getUser();
        $organizations=$user->getOrganizations();
        $organization=$user->getOrganization();

        return $this->render('home/organizations.html.twig',['organizations'=>$organizations,'organization'=>$organization]);
    }

    /**
     * @Route("/toolbar-notifications", name="toolbar_notification")
     */
    public function toolbarNotification(Request $request){
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $sql="SELECT eq.id FROM email_queued eq GROUP BY eq.id";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $emails_queued= $stmt->fetchAll();
        $total_on_queue=count($emails_queued);

        $sql2="SELECT * FROM `join` WHERE `status_id`=1";

        $stmt2 = $conn->prepare($sql2);
        $stmt2->execute();
        $joins_pending= $stmt2->fetchAll();
        $total_joins=count($joins_pending);

        return $this->render('home/notifications.html.twig', ['total_on_queue'=>$total_on_queue,'total_joins'=>$total_joins]);
    }





}
