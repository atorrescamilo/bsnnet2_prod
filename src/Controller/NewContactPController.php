<?php

namespace App\Controller;

use App\Entity\ChangeLog;
use App\Entity\ContactP;
use App\Entity\OrganizationContact;
use App\Form\ContactType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/admin/org-contacts")
 */
class NewContactPController extends AbstractController
{
    /**
     * @Route("/index", name="new_contact_p_index")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em=$this->getDoctrine()->getManager();
            $organization=$this->getUser()->getOrganization()->getId();

            $contacts = $em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization' => $organization));
            $delete_form_ajax = $this->createCustomForm('CONTACT_ID', 'DELETE', 'admin_delete_organizationcontact');

            $organizations=$em->getRepository('App\Entity\Organization')->findAll();
            $organizationsResult=[];

            $currentOrganizations= [];
            if($this->getUser()->getOrganizations()!=null){
                foreach ($this->getUser()->getOrganizations() as $currentOrg){
                    $currentOrganizations[]=$currentOrg->getId();
                }
            }
            foreach ($organizations as $org){
                if(!in_array($org->getId(),$currentOrganizations)){
                    $organizationsResult[]=$org;
                }
            }

            return $this->render('new_contact_p/index.html.twig', [
                'contacts' => $contacts,'organization'=>$organization,
                'delete_form_ajax' => $delete_form_ajax->createView()
            ]);
        }else{
            return $this->redirectToRoute('admin_login');
        }

    }

    /**
     * @Route("/new", name="new_contact_p_new")
     *
     */
    public function add(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $contact=new OrganizationContact();
            $form=$this->createForm(ContactType::class, $contact);

            $organization=$this->getUser()->getOrganization();

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $contact->setOrganization($organization);
                $em->persist($contact);
                $em->flush();

                $action_type=$form['action_type']->getData();
                $this->SaveLog(1,'Contact',$contact->getId(),'New contact','provider_portal');
                $this->addFlash(
                    "success",
                    "The Contact has been created successfully!"
                );
                if($action_type==1){
                    return  $this->redirectToRoute('new_contact_p_index');
                }else{
                    return  $this->redirectToRoute('new_contact_p_new');
                }
            }

            return $this->render('new_contact_p/form.html.twig',  ['form'=>$form->createView(),'action'=>'New']);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="new_contact_p_view", defaults={"id": null})
     *
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\OrganizationContact')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('provider_contact');
            }

            return $this->render('new_contact_p/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="new_contact_p_edit", defaults={"id": null})
     *
     */
    public function edit(Request $request, $id, ValidatorInterface $validator){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $contact=$em->getRepository('App:OrganizationContact')->find($id);
            $form=$this->createForm(ContactType::class, $contact);

            $form->handleRequest($request);
            $errors = $validator->validate($contact);
            if($form->isSubmitted() && $form->isValid()){

                $contact->setUpdatedAt(new \DateTime());
                $em->persist($contact);
                $em->flush();

                $action_type=$form['action_type']->getData();

                $this->SaveLog(2,'Contact',$contact->getId(),'Edit contact','provider_portal');

                $this->addFlash(
                    "success",
                    "The Contact has been updated successfully!"
                );
                if($action_type==1){
                    return  $this->redirectToRoute('new_contact_p_index');
                }else{
                    return  $this->redirectToRoute('new_contact_p_new');
                }
            }

            return $this->render('new_contact_p/form.html.twig',  ['form'=>$form->createView(),'action'=>'Edit','errors'=>$errors]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_organizationcontact",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $organizationcontact = $em->getRepository('App\Entity\OrganizationContact')->find($id);
            $removed = 0;
            $message = "";

            if ($organizationcontact) {
                try {
                    $em->remove($organizationcontact);
                    $em->flush();
                    $removed = 1;
                    $message = "The Contact has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Contact can't be removed";
                }
            }

            $this->SaveLog(3,'Contact',$organizationcontact->getId(),'Delete contact','provider_portal');

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_organizationcontact",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $organizationcontact = $organizationcontact = $em->getRepository('App\Entity\OrganizationContact')->find($id);

                if ($organizationcontact) {
                    try {
                        $em->remove($organizationcontact);
                        $em->flush();
                        $removed = 1;
                        $message = "The Contact has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Contact can't be removed";
                    }
                }
            }

            $this->SaveLog(3,'Contact',$organizationcontact->getId(),'Delete many contacts','provider_portal');

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }

    private function SaveLog($action_id,$entity,$entity_id,$note,$source){
        $em=$this->getDoctrine()->getManager();
        $user=$this->getUser();

        $log=new ChangeLog();
        $log->setUser($user);
        $action=$em->getRepository('App\Entity\ActionLog')->find($action_id);
        $log->setAction($action);
        $log->setEntityId($entity_id);
        $log->setEntityClass($entity);
        $log->setSource($source);
        $log->setNote($note);

        $em->persist($log);
        $em->flush();
    }
}
