<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\ChangeLog;
use App\Form\AddressPType;
use App\Form\ProviderPType;
use App\Service\BillingAddressFileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/admin/billingp")
 */
class BillingNewPController extends AbstractController
{
    /**
     * @Route("/index", name="provider_billing_index")
     *
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            //temp organization
            $organization = $this->getUser()->getOrganization();

            $delete_form_ajax = $this->createCustomForm('BILLINGADDRESS_ID', 'DELETE', 'admin_delete_billing');

            $billing_address=[];

            if($organization){
                $billing_address = $em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $organization->getId()));
            }

            return $this->render('billing_new_p/index.html.twig', array('delete_form_ajax' => $delete_form_ajax->createView(), 'billing_address' => $billing_address));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new_old", name="provider_billing_new_old")
     *
     */
    public function add_old(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $organization = $this->getUser()->getOrganization()->getId();
            $serviceSettings = $em->getRepository('App\Entity\ServiceSettings')->findBy(array(),array('name'=>'ASC'));
            $facilityType = $em->getRepository('App\Entity\FacilityType')->findBy(array(),array('name'=>'ASC'));

            $totalserviceSettings=count($serviceSettings);
            $serviceSettings_first_column_elements=($totalserviceSettings/3);
            if(is_float($serviceSettings_first_column_elements)){
                $serviceSettings_first_column_elements=intval($serviceSettings_first_column_elements+1);
            }
            $serviceSettings_rest_columns_element=intval( ($totalserviceSettings-$serviceSettings_first_column_elements)/2);

            $totalfacilityType=count($facilityType);
            $facilityType_first_column_elements=($totalfacilityType/3);
            if(is_float($facilityType_first_column_elements)){
                $facilityType_first_column_elements=intval($facilityType_first_column_elements+1);
            }
            $facilityType_rest_columns_element=intval( ($totalfacilityType-$facilityType_first_column_elements)/2);


            $google_apikey = $this->getParameter('google_apikey');

            $providers = $em->getRepository('App\Entity\Provider')->findBy(array('organization' => $organization));

            //get if exist the hours Operation Schema
            $hoursOperationSchema="";

            $us_states=$em->getRepository('App\Entity\UsState')->findAll();

            $taxonomies=$em->getRepository('App\Entity\TaxonomyCode')->findAll();

            return $this->render('billing_new_p/add.html.twig', array('hoursOperationSchema'=>$hoursOperationSchema,'google_apikey' => $google_apikey, 'serviceSettings' => $serviceSettings, 'facilityType' => $facilityType, 'providers' => $providers,
                'totalserviceSettings'=>$totalserviceSettings,'serviceSettings_first_column_elements'=>$serviceSettings_first_column_elements,'serviceSettings_rest_columns_element'=>$serviceSettings_rest_columns_element,
                'totalfacilityType'=>$totalfacilityType,'facilityType_first_column_elements'=>$facilityType_first_column_elements,'facilityType_rest_columns_element'=>$facilityType_rest_columns_element,
                'taxonomies'=>$taxonomies, 'org' => $organization,'us_states'=>$us_states));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="provider_billing_new")
     *
     */
    public function add(Request  $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $organization = $this->getUser()->getOrganization();
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $google_apikey = $this->getParameter('google_apikey');
            $address = new BillingAddress();
            $formOptions = array('org_id' => $organization->getId());
            $form = $this->createForm(AddressPType::class, $address, $formOptions);


            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $action_type=$form['action_type']->getData();


                $county_data = $form['county']->getData();
                $sql="SELECT id FROM `county_match` WHERE `name` LIKE '%$county_data%'";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                $counties= $stmt->fetchAll();
                foreach ($counties as $county) {
                    $county_ob = $em->getRepository('App\Entity\CountyMatch')->find($county['id']);
                    if ($county_ob) {
                        $address->setCountyMatch($county_ob);
                    }
                }

                // set country
                $address->setCountry("United States");

                $address->setNaBeds(false);
                if ($form['group_medicaid']->getData()!=='') {
                    $address->setApplyGroupMedicaid(true);
                }
                else{
                    $address->setApplyGroupMedicaid(false);
                }

                if ($form['group_medicare']->getData()!=='') {
                    $address->setApplyGroupMedicare(true);
                }
                else{
                    $address->setApplyGroupMedicare(false);
                }

                $min_value_ar_array = [];
                $max_value_ar_array = [];
                $min_value_ar = -1;
                $max_value_ar = -1;
                $age_ranges_data = $form['age_ranges']->getData();
                for ($i = 0; $i<sizeof($age_ranges_data); $i++) {
                    array_push($min_value_ar_array, $age_ranges_data[$i]->getMinAge());
                    array_push($max_value_ar_array, $age_ranges_data[$i]->getMaxAge());
                }
                $min_value_ar = min($min_value_ar_array);
                $max_value_ar = max($max_value_ar_array);
                $address->setOfficeAgeLimit($min_value_ar);
                $address->setOfficeAgeLimitMax($max_value_ar);
                $address->setDisabled(false);


                $address->setOrganization($organization);
                //save address finally
                $em->persist($address);
                $em->flush();

                //adding in providers this address
                $providers_id=$form['providers_id']->getData();
                $providers = substr($providers_id, 0, -1);
                $lbs = explode(",", $providers);
                foreach ($lbs as $lb){
                    $provider = $em->getRepository('App:Provider')->find($lb);
                    if($provider){
                        $provider->addBillingAddress($address);
                        $em->persist($provider);
                    }
                }
                $em->flush();

                if($action_type==1){
                    return $this->redirectToRoute('provider_billing_index');
                }else{
                    return $this->redirectToRoute('provider_billing_new');
                }
            }

            return $this->render('billing_new_p/form.html.twig', array('form' => $form->createView(), 'action' => 'New', 'org' => $organization,
                'google_apikey' => $google_apikey, 'errors'=>null));
        }else{
            return $this->redirectToRoute('admin_login');
        }

    }

    /**
     * @Route("/create", name="provider_billing_create")
     *
     */
    public function create(Request $request, BillingAddressFileUploader $fileUploader){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $db = $em->getConnection();

            $save_status = $request->get('save_status');
            $form_step = $request->get('form_step');
            $org=$this->getUser()->getOrganization();

            if ($save_status == 0) {
                $billinAddress = new BillingAddress();
            } else {
                $billinAddress = $em->getRepository('App\Entity\BillingAddress')->find($save_status);
            }

            if ($form_step == 1) {
                $street = $request->get('street');
                $suite_number = $request->get('suite_number');
                $city = $request->get('city');
                $state = $request->get('state');
                $zip_code = $request->get('zip_code');
                $country = $request->get('country');
                $phone = $request->get('phone');
                $region=$request->get('region');
                $county=$request->get('county');
                $google_addr=$request->get('google_addr');
                $latitude=$request->get('latitude');
                $longitude=$request->get('longitude');
                $location_name=$request->get('location_name');

                $county=strtoupper($county);
                $countyMatch=$em->getRepository('App\Entity\CountyMatch')->findOneBy(array('name'=>$county));

                if($countyMatch!=null){
                    $billinAddress->setCountyMatch($countyMatch);
                }

                $billinAddress->setStreet($street);
                $billinAddress->setSuiteNumber($suite_number);
                $billinAddress->setCity($city);
                $billinAddress->setRegion($region);
                $billinAddress->setCounty($county);
                $billinAddress->setUsState($state);
                $billinAddress->setZipCode($zip_code);
                $billinAddress->setCountry($country);
                $billinAddress->setPhoneNumber($phone);
                $billinAddress->setOrganization($org);
                $billinAddress->setGoogleAddr($google_addr);
                $billinAddress->setLat($latitude);
                $billinAddress->setLng($longitude);
                $billinAddress->setLocationName($location_name);

                $em->persist($billinAddress);
                $em->flush();

            }
            if ($form_step == 2) {
                $business_hours = $request->get('business_hours_data');
                $serviceSelected = $request->get('serviceSelected');
                $is_see_patients_address = $request->get('is_see_patients_address');

                $sqlDelete = "DELETE FROM `billing_address_service_settings` WHERE `billing_address_service_settings`.`billing_address_id` =  $save_status";
                $stmt = $db->prepare($sqlDelete);
                $stmt->execute();

                $services = substr($serviceSelected, 0, -1);
                $lbs = explode(",", $services);

                $billinAddress->setBusinessHours($business_hours);

                if($is_see_patients_address==1){
                    $billinAddress->setSeePatientsAddr(true);
                }else{
                    $billinAddress->setSeePatientsAddr(false);
                }

                if ($lbs != null) {
                    foreach ($lbs as $service) {
                        $serviceObj = $em->getRepository('App\Entity\ServiceSettings')->find($service);

                        if ($serviceObj != null) {
                            $billinAddress->addServiceSetting($serviceObj);
                        }
                    }

                    $em->persist($billinAddress);
                }
                $em->flush();
            }
            if ($form_step == 3) {
                $inpatient_beds=$request->get('inpatient_beds');
                $number_of_beds = $request->get('number_of_beds');
                $location_npi = $request->get('location_npi');
                $location_npi2 = $request->get('location_npi2');
                $group_medicaid = $request->get('group_medicaid');
                $group_medicare = $request->get('group_medicare');
                $limit_age_min = $request->get('limit_age_min');
                $limit_age_max = $request->get('limit_age_max');
                $taxonomies=$request->get('taxonomies');

                $billinAddress->setHaveBeds($inpatient_beds);
                $billinAddress->setBeds($number_of_beds);
                $billinAddress->setLocationNpi($location_npi);
                $billinAddress->setLocationNpi2($location_npi2);
                $billinAddress->setGroupMedicaid($group_medicaid);
                $billinAddress->setGroupMedicare($group_medicare);
                $actualTaxonomies=$billinAddress->getTaxonomyCodes();

                if($actualTaxonomies!=null){
                    foreach ($actualTaxonomies as $at){
                        $billinAddress->removeTaxonomyCode($at);
                        $em->persist($billinAddress);
                        $em->flush();
                    }
                }

                if($taxonomies!=""){
                    foreach ($taxonomies as $taxonomy){
                        $taxonomyObj=$em->getRepository('App\Entity\TaxonomyCode')->find($taxonomy);
                        if($taxonomyObj!=null){
                            $billinAddress->addTaxonomyCode($taxonomyObj);
                        }
                    }
                }

                if ($limit_age_min != "")
                    $billinAddress->setOfficeAgeLimit($limit_age_min);
                if ($limit_age_max != "")
                    $billinAddress->setOfficeAgeLimitMax($limit_age_max);

                $em->persist($billinAddress);
                $em->flush();
            }

            if ($form_step == 4) {
                $wheelchair_accessibility = $request->get('wheelchair_accessibility');
                $tdd = $request->get('tdd');
                $tdd_number=$request->get('tdd_number');
                $private_provider_transportation = $request->get('private_provider_transportation');
                $is_primary_address = $request->get('is_primary_address');
                $is_billing_address = $request->get('is_billing_address');
                $is_mailing_address = $request->get('is_mailing_address');
                $is_facility = $request->get('is_facility');
                $facilityTypesSelected = $request->get('facilityTypesSelected');
                $facility_number = $request->get('facility_number');
                $facility_exemption_number = $request->get('facility_exemption_number');
                $ahca_number=$request->get('ahca_number');

                $billinAddress->setWheelchair($wheelchair_accessibility);
                $billinAddress->setTdd($tdd);
                $billinAddress->setTddNumber($tdd_number);
                $billinAddress->setPrivateProviderTransportation($private_provider_transportation);
                $billinAddress->setPrimaryAddr($is_primary_address);
                $billinAddress->setBillingAddr($is_billing_address);
                $billinAddress->setMailingAddr($is_mailing_address);
                $billinAddress->setIsFacility($is_facility);
                $billinAddress->setAhcaNumber($ahca_number);

                if ($facilityTypesSelected != "") {
                    $facilities=$billinAddress->getFacilityType();

                    if($facilities){
                        foreach ($facilities as $facility){
                            $billinAddress->removeFacilityType($facility);
                        }
                    }

                    $facilities = substr($facilityTypesSelected, 0, -1);
                    $lbs = explode(",", $facilities);

                    foreach ($lbs as $facility) {
                        if ($facility != null) {
                            $facilityObj = $em->getRepository('App\Entity\FacilityType')->find($facility);
                            if ($facilityObj != null) {
                                $billinAddress->addFacilityType($facilityObj);
                            }
                        }
                    }
                }
                if ($facility_number == "") {
                    $billinAddress->setFacilityNumber(null);
                } else {
                    $billinAddress->setFacilityNumber(intval($facility_number));
                }

                if ($facility_exemption_number == "") {
                    $billinAddress->setFacilityExemptionNumber(null);
                } else {
                    $billinAddress->setFacilityExemptionNumber($facility_exemption_number);
                }

                $em->persist($billinAddress);
                $em->flush();
            }

            if ($form_step == 6) {
                $providerSelected = $request->get('providerSelected');

                $sqlDelete = "DELETE FROM `provider_billing_address` WHERE `provider_billing_address`.`billing_address_id` =  $save_status";
                $stmt = $db->prepare($sqlDelete);
                $stmt->execute();

                $providers = substr($providerSelected, 0, -1);
                $lbs = explode(",", $providers);

                if ($lbs != null) {
                    foreach ($lbs as $provider) {
                        $providerObj = $em->getRepository('App\Entity\Provider')->find($provider);
                        if ($providerObj != null) {
                            $providerObj->addBillingAddress($billinAddress);
                        }
                        $em->persist($providerObj);
                        $em->flush();
                    }
                }
            }

            if ($save_status == 0) {
                $this->SaveLog(1,'Address',$billinAddress->getId(),'New Address','provider_portal');
            }else{
                $this->SaveLog(1,'Address',$billinAddress->getId(),'Edit Address','provider_portal');
            }

            //update the providers facility status
            $organization=$billinAddress->getOrganization();
            $allLocations=$em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization->getId()));
            $providers=$em->getRepository('App:Provider')->findBy(array('organization'=>$organization->getId()));
            $is_facility=false;
            if($allLocations){
                foreach ($allLocations as $addr){
                    if($addr->getIsFacility()==true){
                        $is_facility=true;
                        break;
                    }
                }
            }

            if($providers){
                foreach ($providers as $provider){
                    $provider->setIsFacility($is_facility);
                    $em->persist($provider);
                }
            }

            $em->flush();

            //check the address number and if is only one assign it to all providers in the organization
            $all_address=$em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization->getId()));
            if($all_address){
                if(count($all_address)==1){
                    $providers=$em->getRepository('App:Provider')->findBy(array('organization'=>$organization->getId()));

                    if($providers){
                        foreach ($providers as $provider){
                            if($provider->getBillingAddress()==null or count($provider->getBillingAddress())==0){
                                foreach ($all_address as $addr1){
                                    $provider->addBillingAddress($addr1);
                                    $em->persist($provider);
                                }
                            }
                        }
                    }
                }
                $em->flush();
            }

            return new Response(
                json_encode(array('id' => $billinAddress->getId())), 200, array('Content-Type' => 'application/json')
            );

        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="provider_billing_view",defaults={"id": null,"org":null})
     *
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\BillingAddress')->find($id);

            $serviceSettings = $em->getRepository('App\Entity\ServiceSettings')->findAll();
            $facilityType = $em->getRepository('App\Entity\FacilityType')->findAll('');
            $cvos = $em->getRepository('App\Entity\Cvo')->findAll(array('name' => 'ASC'));
            $payers = $em->getRepository('App\Entity\Payer')->findAll(array('name' => 'ASC'));

            $AddressCvo = $em->getRepository('App\Entity\BillingAddressCvo')->findAll();

            $providers=$document->getProviders();

            return $this->render('billing_new_p/view.html.twig', array('document' => $document, 'serviceSettings' => $serviceSettings, 'facilityType' => $facilityType,
                'cvos' => $cvos, 'payers' => $payers, 'addresscvo' => $AddressCvo,'providerOnAddress'=>$providers));
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/edit_old/{id}", name="provider_billing_edit_old",defaults={"id": null})
     *
     */
    public function edit_old($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $db = $em->getConnection();
            $user = $this->getUser();
            $document = $em->getRepository('App\Entity\BillingAddress')->find($id);
            $organization=$user->getOrganization();

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('provider_billing');
            }

            $serviceSettings = $em->getRepository('App\Entity\ServiceSettings')->findBy(array(),array('name'=>'ASC'));
            $facilityType = $em->getRepository('App\Entity\FacilityType')->findBy(array(),array('name'=>'ASC'));

            $google_apikey = $this->getParameter('google_apikey');
            $providers = $em->getRepository('App\Entity\Provider')->findBy(array('organization' => $organization->getId()));

            $totalserviceSettings=count($serviceSettings);
            $serviceSettings_first_column_elements=($totalserviceSettings/3);
            if(is_float($serviceSettings_first_column_elements)){
                $serviceSettings_first_column_elements=intval($serviceSettings_first_column_elements+1);
            }
            $serviceSettings_rest_columns_element=intval( ($totalserviceSettings-$serviceSettings_first_column_elements)/2);

            $totalfacilityType=count($facilityType);
            $facilityType_first_column_elements=($totalfacilityType/3);
            if(is_float($facilityType_first_column_elements)){
                $facilityType_first_column_elements=intval($facilityType_first_column_elements+1);
            }
            $facilityType_rest_columns_element=intval( ($totalfacilityType-$facilityType_first_column_elements)/2);


            $query1 = "SELECT * FROM provider_billing_address WHERE  provider_billing_address.billing_address_id=$id;";
            $stmt1 = $db->prepare($query1);
            $stmt1->execute();

            $providersBillingAddress=$stmt1->fetchAll();
            $providerOnAddress=array();

            if($providersBillingAddress!=null){
                foreach ($providersBillingAddress as $pba){
                    $provider=$em->getRepository('App\Entity\Provider')->find($pba['provider_id']);
                    if($provider){
                        $providerOnAddress[]=$provider;
                    }
                }
            }

            $taxonomies=$em->getRepository('App\Entity\TaxonomyCode')->findAll();

            $currentTaxonomies=$document->getTaxonomyCodes();

            $us_states=$em->getRepository('App\Entity\UsState')->findAll();

            return $this->render('billing_new_p/edit.html.twig', array('document' => $document, 'google_apikey' => $google_apikey,'taxonomies'=>$taxonomies,'currentTaxonomies'=>$currentTaxonomies,
                'org' => $organization, 'serviceSettings' => $serviceSettings, 'facilityType' => $facilityType,'providers' => $providers,'providerOnAddress'=>$providerOnAddress,
                'totalserviceSettings'=>$totalserviceSettings,'serviceSettings_first_column_elements'=>$serviceSettings_first_column_elements,'serviceSettings_rest_columns_element'=>$serviceSettings_rest_columns_element,
                'totalfacilityType'=>$totalfacilityType,'facilityType_first_column_elements'=>$facilityType_first_column_elements,'facilityType_rest_columns_element'=>$facilityType_rest_columns_element,
                'org'=>$organization,'us_states'=>$us_states));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="provider_billing_edit",defaults={"id": null})
     *
     */
    public function edit(Request $request, $id, ValidatorInterface $validator){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            $organization = $this->getUser()->getOrganization();
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $google_apikey = $this->getParameter('google_apikey');
            $address = $em->getRepository('App:BillingAddress')->find($id);

            $formOptions = array('org_id' => $organization->getId());
            $form = $this->createForm(AddressPType::class, $address, $formOptions);

            $errors = $validator->validate($address);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $action_type=$form['action_type']->getData();

                $county_data = $form['county']->getData();
                $sql="SELECT id FROM `county_match` WHERE `name` LIKE '%$county_data%'";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                $counties= $stmt->fetchAll();
                foreach ($counties as $county) {
                    $county_ob = $em->getRepository('App\Entity\CountyMatch')->find($county['id']);
                    if ($county_ob) {
                        $address->setCountyMatch($county_ob);
                    }
                }

                // set country
                $address->setCountry("United States");

                $address->setNaBeds(false);

                if ($form['group_medicaid']->getData()!=='') {
                    $address->setApplyGroupMedicaid(true);
                }
                else{
                    $address->setApplyGroupMedicaid(false);
                }

                if ($form['group_medicare']->getData()!=='') {
                    $address->setApplyGroupMedicare(true);
                }
                else{
                    $address->setApplyGroupMedicare(false);
                }

                $min_value_ar_array = [];
                $max_value_ar_array = [];
                $min_value_ar = -1;
                $max_value_ar = -1;
                $age_ranges_data = $form['age_ranges']->getData();
                for ($i = 0; $i<sizeof($age_ranges_data); $i++) {
                    array_push($min_value_ar_array, $age_ranges_data[$i]->getMinAge());
                    array_push($max_value_ar_array, $age_ranges_data[$i]->getMaxAge());
                }
                $min_value_ar = min($min_value_ar_array);
                $max_value_ar = max($max_value_ar_array);
                $address->setOfficeAgeLimit($min_value_ar);
                $address->setOfficeAgeLimitMax($max_value_ar);

                $address->setDisabled(false);

                $address->setOrganization($organization);
                //save address finally
                $em->persist($address);
                $em->flush();

                //adding in providers this address
                //first removing one (this) address to this providers adding in providers this address
                $providers_this_org = $em->getRepository('App:Provider')->findBy(array('organization'=>$organization->getId()));
                foreach ($providers_this_org as $provider_this_org){
                    $provider_this_org->removeBillingAddress($address);
                    $em->persist($provider_this_org);
                }
                $em->flush();
                //then adding in providers this address
                $providers_id=$form['providers_id']->getData();
                $providers = substr($providers_id, 0, -1);
                $lbs = explode(",", $providers);
                foreach ($lbs as $lb){
                    $provider = $em->getRepository('App:Provider')->find($lb);
                    if($provider){
                        $provider->addBillingAddress($address);
                        $em->persist($provider);
                    }
                }
                $em->flush();

                if($action_type==1){
                    return $this->redirectToRoute('provider_billing_index');
                }else{
                    return $this->redirectToRoute('provider_billing_new');
                }
            }
            return $this->render('billing_new_p/form.html.twig', array('form' => $form->createView(), 'action' => 'Edit', 'org' => $organization,
                'google_apikey' => $google_apikey,'errors'=>$errors));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }

    private function SaveLog($action_id,$entity,$entity_id,$note,$source){
        $em=$this->getDoctrine()->getManager();
        $user=$this->getUser();

        $log=new ChangeLog();
        $log->setUser($user);
        $action=$em->getRepository('App\Entity\ActionLog')->find($action_id);
        $log->setAction($action);
        $log->setEntityId($entity_id);
        $log->setEntityClass($entity);
        $log->setSource($source);
        $log->setNote($note);

        $em->persist($log);
        $em->flush();
    }

}
