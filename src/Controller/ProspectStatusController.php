<?php

namespace App\Controller;

use App\Entity\ProspectStatus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/prospect_status")
 */
class ProspectStatusController extends AbstractController
{
    /**
     * @Route("/index", name="prospect_status_index")
     */
    public function index()
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $status = $em->getRepository('App\Entity\ProspectStatus')->findAll();

            $delete_form_ajax = $this->createCustomForm('PROSPECT_ID', 'DELETE', 'admin_delete_prospect_status');


            return $this->render('prospect_status/index.html.twig', array('status' => $status, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_prospect_status_new")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('prospect_status/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_prospect_status")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');

            $cvo = new ProspectStatus();
            $cvo->setName($name);
            $cvo->setDescription($description);

            $em->persist($cvo);
            $em->flush();

            $this->addFlash(
                'success',
                'The Prospect Status has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_prospect_status_new');
            }

            return $this->redirectToRoute('prospect_status_index');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_prospect_status", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ProspectStatus')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('prospect_status_index');
            }

            return $this->render('prospect_status/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_prospect_status", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ProspectStatus')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('prospect_status_index');
            }

            return $this->render('prospect_status/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_prospect_status")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $cvo = $em->getRepository('App\Entity\ProspectStatus')->find($id);

            if ($cvo == null) {
                $this->addFlash(
                    "danger",
                    "The Prospect Status can't been updated!"
                );

                return $this->redirectToRoute('prospect_status_index');
            }

            if ($cvo != null) {
                $cvo->setName($name);
                $cvo->setDescription($description);

                $em->persist($cvo);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "The Prospect Status has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_prospect_status_new');
            }

            return $this->redirectToRoute('prospect_status_index');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_prospect_status",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $prospect  = $em->getRepository('App\Entity\ProspectStatus')->find($id);
            $removed = 0;
            $message = "";

            if ($prospect) {
                try {
                    $em->remove($prospect);
                    $em->flush();
                    $removed = 1;
                    $message = "The Prospect has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Prospect can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_prospect_status",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $prospect= $em->getRepository('App\Entity\ProspectStatus')->find($id);

                if ($prospect) {
                    try {
                        $em->remove($prospect);
                        $em->flush();
                        $removed = 1;
                        $message = "The Prospects Status has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Prospects Status can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }
}
