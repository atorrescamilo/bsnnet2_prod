<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\ServiceSettings;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/servicesetting")
 */
class ServiceSettingController extends AbstractController{
    /**
     * @Route("/index", name="admin_servicesetting")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $servicesettings = $em->getRepository('App\Entity\ServiceSettings')->findBy(array(),array('name'=>'ASC'));

            $delete_form_ajax = $this->createCustomForm('SERVICESETTING_ID', 'DELETE', 'admin_delete_servicesetting');


            return $this->render('servicesetting/index.html.twig', array('servicesettings' => $servicesettings, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_servicesetting")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('servicesetting/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_servicesetting", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ServiceSettings')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_servicesetting');
            }

            return $this->render('servicesetting/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_servicesetting", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ServiceSettings')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_servicesetting');
            }

            return $this->render('servicesetting/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_servicesetting")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $code = $request->get('code');
            $description=$request->get('description');
            $new = $request->get('new');

            $servicesetting = new ServiceSettings();
            $servicesetting->setName($name);
            $servicesetting->setDescription($description);
            $servicesetting->setCode($code);

            $em->persist($servicesetting);
            $em->flush();

            $this->addFlash(
                'success',
                'The Service Settings has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_servicesetting');
            }

            return $this->redirectToRoute('admin_servicesetting');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_servicesetting")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $code = $request->get('code');
            $description=$request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $servicesetting = $em->getRepository('App\Entity\ServiceSettings')->find($id);

            if ($servicesetting == null) {
                $this->addFlash(
                    "danger",
                    "The Service Settings can't been updated!"
                );

                return $this->redirectToRoute('admin_servicesetting');
            }

            if ($servicesetting != null) {
                $servicesetting->setName($name);
                $servicesetting->setCode($code);
                $servicesetting->setDescription($description);

                $em->persist($servicesetting);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "The Service Settings has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_servicesetting');
            }

            return $this->redirectToRoute('admin_servicesetting');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_servicesetting",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $servicesetting = $servicesetting = $em->getRepository('App\Entity\ServiceSettings')->find($id);
            $removed = 0;
            $message = "";

            if ($servicesetting) {
                try {
                    $em->remove($servicesetting);
                    $em->flush();
                    $removed = 1;
                    $message = "The Service Settings has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Service Setting can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_servicesetting",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $servicesetting = $servicesetting = $em->getRepository('App\Entity\ServiceSettings')->find($id);

                if ($servicesetting) {
                    try {
                        $em->remove($servicesetting);
                        $em->flush();
                        $removed = 1;
                        $message = "The Service Settings has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The ServiceSettings can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/isUnique", name="admin_isunique_servicesetting",methods={"POST"})
     */
    public function isUnique(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $code = $request->get('code');

            $servicesetting = $em->getRepository('App\Entity\ServiceSettings')->findOneBy(array('code' => $code));

            $isUnique = 0;
            if ($servicesetting != null) {
                $isUnique = 1;
            }

            return new Response(
                json_encode(array('isUnique' => $isUnique)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}

