<?php

namespace App\Controller;

use App\Entity\ChangeLog;
use App\Entity\OrganizationTrainingAttestation;
use Dompdf\Dompdf;
use Dompdf\Options;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class TrainingAttestationController extends AbstractController
{
    /**
     * @Route("/training/attestation", name="training_attestation_index")
    */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em=$this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $user=$this->getUser();
            $organization=$user->getOrganization();
            $orgId=$organization->getId();
            $providers=[];

            if($organization!=null){
                $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0,'organization'=>$organization->getId()));
            }

            $sql="SELECT  GROUP_CONCAT(DISTINCT `payer`.`id` ORDER BY `payer`.`id` ASC SEPARATOR ',') AS `payer_id`
            FROM  organization o 
            LEFT JOIN provider ON provider.org_id = o.id
            LEFT JOIN `provider_payer` ON `provider_payer`.`provider_id` = `provider`.`id`
            LEFT JOIN `payer` ON `payer`.`id` = `provider_payer`.`payer_id`
            WHERE o.disabled=0 and o.status_id=2 and o.id=".$orgId."
            GROUP BY o.id
            ORDER BY o.name";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $payers= $stmt->fetchAll();

            $is_MMM=false;
            $ids=$payers[0]['payer_id'];
            $idsrarray=explode(",",$ids);
            if(in_array(1,$idsrarray)){
                $is_MMM=true;
            }

            $allCmsTatining=$em->getRepository('App:CmsTraining')->findAll();
            $trainingReault=[];

            if($allCmsTatining!=null){
                foreach ($allCmsTatining as $cms_training){
                    if($cms_training->getId()==4 and $is_MMM==true){
                        $trainingReault[]=$cms_training;
                    }
                    if($cms_training->getId()==5 and $is_MMM==true){

                    }
                    if($cms_training->getId()!=4){
                        $trainingReault[]=$cms_training;
                    }
                }
            }

            $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$orgId));

            $currentYear=date('Y');
            $orgTrainingAttestation=$em->getRepository('App\Entity\OrganizationTrainingAttestation')->findOneBy(array('year'=>$currentYear,'organization'=>$organization));
            $cmsTrainings=array();
            if($orgTrainingAttestation!=null){
                $cmsTrainings=$orgTrainingAttestation->getCMSTrainings();
            }


            return $this->render('training_attestation/index.html.twig',['providers' => $providers,'payers'=>$payers,'trainings'=>$trainingReault,
                'organization'=>$organization,'contacts'=>$contacts,'is_MMM'=>$is_MMM,
                'document'=>$orgTrainingAttestation,'cmsTrainings'=>$cmsTrainings]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_attestation")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em=$this->getDoctrine()->getManager();

            $currentYear=date('Y');
            $organization=$request->get('organization');
            $orgObj=$em->getRepository('App\Entity\Organization')->find($organization);
            $year=$request->get('year');
            $authorized_representative_name=$request->get('authorized_representative_name');
            $authorized_representative_phone=$request->get('authorized_representative_phone');
            $authorized_representative_email=$request->get('authorized_representative_email');
            $authorized_representative_signature=$request->get('authorized_representative_signature');
            $date=$request->get('date');

            $orgTrainingAttestation=$em->getRepository('App\Entity\OrganizationTrainingAttestation')->findOneBy(array('year'=>$currentYear,'organization'=>$organization));

            if($orgTrainingAttestation==null){
                $orgTrainingAttestation=new OrganizationTrainingAttestation();
                $this->SaveLog(1,'TrainingAttestation',null,'New Training Attestation');
            }else{
                $this->SaveLog(2,'TrainingAttestation',$orgTrainingAttestation->getId(),'New Training Attestation');
            }

            $orgTrainingAttestation->setOrganization($orgObj);
            $orgTrainingAttestation->setYear($year);
            $orgTrainingAttestation->setRepresentativeName($authorized_representative_name);
            $orgTrainingAttestation->setRepresentativePhone($authorized_representative_phone);
            $orgTrainingAttestation->setRepresentativeEmail($authorized_representative_email);
            $orgTrainingAttestation->setRepresentativeSignature($authorized_representative_signature);
            $orgTrainingAttestation->setDate($date);

            $trainingsSelected=$request->get('trainingsSelected');

            //remove lines of bussiness
            $trainings = $orgTrainingAttestation->getCMSTrainings();
            if ($trainings != null) {
                foreach ($trainings as $training) {
                    $orgTrainingAttestation->removeCMSTraining($training);
                }
            }

            $trainingsSelected = substr($trainingsSelected, 0, -1);
            $lbs = explode(",", $trainingsSelected);

            if ($lbs != "") {
                foreach ($lbs as $lb) {
                    $training = $em->getRepository('App\Entity\CmsTraining')->find($lb);
                    if ($training != null) {
                        $orgTrainingAttestation->addCMSTraining($training);
                    }
                }
            }

            $em->persist($orgTrainingAttestation);
            $em->flush();

            return $this->redirectToRoute('home');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/training/attestation-list", name="training_attestation_list")
     */
    public function list(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $trainings=$em->getRepository('App\Entity\OrganizationTrainingAttestation')->findAll();

            return $this->render('training_attestation/list.html.twig', array('trainings' => $trainings));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/training-attestation/view/{id}", name="training_attestation_view", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $training=$em->getRepository('App\Entity\OrganizationTrainingAttestation')->find($id);

            $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$training->getOrganization()->getId(),'disabled'=>0),array('first_name'=>'ASC'));

            return $this->render('training_attestation/view.html.twig', array('training' => $training,'providers'=>$providers));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/export-pdf/{id}", name="admin_training_export_pdf",defaults={"id": null})
     */
    public function exportPDF($id) {
        $em=$this->getDoctrine()->getManager();

        $training=$em->getRepository('App\Entity\OrganizationTrainingAttestation')->find($id);

        $options = new Options();
        $options->set('defaultFont', 'Courier');

        $html="<p>Hola Mundo</p>";
        $html.="<h1>Test Cabecera</h1>";

        $dompdf = new Dompdf($options);
        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();

        $organizationName=$training->getOrganization()->getName();
        $filename="Training-Attestation-".$organizationName."-".date('m-d-Y-H-i-s');

        $dompdf->stream($filename);
    }

    private function SaveLog($action_id,$entity,$entity_id,$note){
        $em=$this->getDoctrine()->getManager();
        $user=$this->getUser();

        $log=new ChangeLog();
        $log->setUser($user);
        $action=$em->getRepository('App\Entity\ActionLog')->find($action_id);
        $log->setAction($action);
        $log->setEntityId($entity_id);
        $log->setEntityClass($entity);
        $log->setNote($note);

        $em->persist($log);
        $em->flush();
    }
}
