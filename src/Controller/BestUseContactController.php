<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\ContactPBestChoice;

/**
 * @Route("/admin/contact-best")
 */
class BestUseContactController extends AbstractController
{
    /**
     * @Route("/index", name="admin_contact_best")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $contact_bests = $em->getRepository('App\Entity\ContactPBestChoice')->findBy(array(),array('name'=>'ASC'));
            $delete_form_ajax = $this->createCustomForm('CONTACTBEST_ID', 'DELETE', 'admin_delete_contact_best');


            return $this->render('contact_best/index.html.twig', array('contact_bests' => $contact_bests, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/new", name="admin_new_contact_best")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->render('contact_best/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_contact_best", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ContactPBestChoice')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_contact_best');
            }

            return $this->render('contact_best/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_contact_best", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ContactPBestChoice')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_contact_best');
            }

            return $this->render('contact_best/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_contact_best")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');

            $contact_best = new ContactPBestChoice();
            $contact_best->setName($name);
            $contact_best->setDescription($description);

            $em->persist($contact_best);
            $em->flush();

            $this->addFlash(
                'success',
                'Contact best for has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_contact_best');
            }

            return $this->redirectToRoute('admin_contact_best');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_contact_best")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $obj = $em->getRepository('App\Entity\ContactPBestChoice')->find($id);

            if ($obj == null) {
                $this->addFlash(
                    "danger",
                    "The Contact best for can't been updated!"
                );

                return $this->redirectToRoute('admin_contact_best');
            }

            if ($obj != null) {
                $obj->setName($name);
                $obj->setDescription($description);

                $em->persist($obj);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "Contact best for has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_contact_best');
            }

            return $this->redirectToRoute('admin_contact_best');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_contact_best",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $obj = $em->getRepository('App\Entity\ContactPBestChoice')->find($id);
            $removed = 0;
            $message = "The Contact best for can't be removed";

            if ($obj) {
                try {
                    $em->remove($obj);
                    $em->flush();
                    $removed = 1;
                    $message = "The Contact best for has been Successfully removed";
                } catch (Exception $ex) {
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_cvo",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "The Contacts best for selected can't be removed";

            foreach ($ids as $id) {
                $cvo  = $em->getRepository('App\Entity\ContactPBestChoice')->find($id);

                if ($cvo) {
                    try {
                        $em->remove($cvo);
                        $em->flush();
                        $removed = 1;
                        $message = "The Contact best for has been Successfully removed";
                    } catch (Exception $ex) {
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }



    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }
}
