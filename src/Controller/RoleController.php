<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Role;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/role")
 */
class RoleController extends AbstractController{
    /**
     * @Route("/index", name="admin_role")
    */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $roles = $em->getRepository('App\Entity\Role')->findAll();
            $delete_form_ajax = $this->createCustomForm('ROLE_ID', 'DELETE', 'admin_delete_role');

            return $this->render('role/index.html.twig', array('roles' => $roles, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_role")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('role/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_role", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Role')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_role');
            }

            return $this->render('role/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_role", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Role')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_role');
            }

            return $this->render('role/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_role")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');

            $role = new Role();
            $role->setName($name);
            $role->setDescription($description);

            $em->persist($role);
            $em->flush();

            $this->addFlash(
                'success',
                'Role has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_role');
            }

            return $this->redirectToRoute('admin_role');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_role")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $role = $em->getRepository('App\Entity\Role')->find($id);

            if ($role == null) {
                $this->addFlash(
                    "danger",
                    "The Role can't been updated!"
                );

                return $this->redirectToRoute('admin_role');
            }

            if ($role != null) {
                $role->setName($name);
                $role->setDescription($description);

                $em->persist($role);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "Role has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_role');
            }

            return $this->redirectToRoute('admin_role');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_role",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $role = $em->getRepository('App\Entity\Role')->find($id);
            $removed = 0;
            $message = "";

            if ($role) {
                try {
                    $em->remove($role);
                    $em->flush();
                    $removed = 1;
                    $message = "The Role has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Role can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_role",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $role  = $em->getRepository('App\Entity\Role')->find($id);

                if ($role) {
                    try {
                        $em->remove($role);
                        $em->flush();
                        $removed = 1;
                        $message = "The Role has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Role can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
