<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\FCCRosterLog;
use App\Entity\Organization;
use App\Entity\OrganizationCredentialing;
use App\Entity\Provider;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\Reader;

/**
 * @Route("/admin")
 */
class TestController extends AbstractController
{
    /**
     * @Route("/setptsp", name="test_setptsp")
     */
    public function setPTSp(){
        $em=$this->getDoctrine()->getManager();

        $providers=$em->getRepository('App:Provider')->findBy(array('organization'=>617));

        $pt=$em->getRepository('App:ProviderType')->find(12);
        $sp=$em->getRepository('App:Specialty')->find(21);

        foreach ($providers as $provider){
            if(count($provider->getProviderType())==0){
                $provider->addProviderType($pt);
                $provider->addPrimarySpecialty($sp);
                $em->persist($provider);
                $em->flush();
            }
        }

        die();
    }

    /**
     * @Route("/remove-notvalid-pt", name="remove-notvalid-pt-provider")
     */
    public function removeNotValidPT(){
        $em=$this->getDoctrine()->getManager();

        $providers=$em->getRepository('App:Provider')->findAll();

        foreach ($providers as $provider){
          $providerTypes=$provider->getProviderType();

          if($providerTypes){

              foreach ($providerTypes as $providerType){
                  if($providerType->getId()==23 or $providerType->getId()==24 or $providerType->getId()==25 or $providerType->getId()==26 or $providerType->getId()==28
                      or $providerType->getId()==29 or $providerType->getId()==30 or $providerType->getId()==31 or $providerType->getId()==32 or $providerType->getId()==32
                      or $providerType->getId()==35 or $providerType->getId()==37 or $providerType->getId()==38 or $providerType->getId()==42){

                      $provider->removeProviderType($providerType);
                      $em->persist($provider);
                  }
              }
          }
        }

        $em->flush();

        die();
    }

    /**
     * @Route("/test", name="test")
     */
    public function index(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "fcc_roster_log/FCC_BSN_Roster20210208.xlsx";
        $projectDir = $this->getParameter('kernel.project_dir');

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=1;
        $cont_out=2;

        $url= $projectDir."/public/template_xls/";
        $spreadsheet = $reader->load($url.'directory_template.xlsx');

        foreach ($sheetData as $sheet) {
            if ($cont >= 1) {
                if ($sheet['A'] != null or $sheet['A'] != "") {

                    $npi=$sheet['A'];

                    $in_bsn="No";
                    $provider_type="";
                    $name="";
                    $street="";
                    $zipCode="";
                    $phone="";
                    $city="";
                    $state="";
                    $status="";

                    $org=$em->getRepository('App\Entity\Organization')->findOneBy(array('group_npi'=>$npi));
                    if($org!=null){
                        $in_bsn="Yes";
                        $name=$org->getName();
                        $provider_type="NPI-2";
                    }else{
                        $provider=$em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number'=>$npi));
                        if($provider!=null){
                            $in_bsn="Yes";
                            $name=$provider->getFirstName(). " ". $provider->getLastName();
                            $provider_type="NPI-1";
                        }
                    }

                    $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=1&skip=&pretty=on&version=2.1";
                    $json=file_get_contents($url_result);
                    $datos=json_decode($json,true);

                    if(count($datos)>1){
                        if($in_bsn=="No"){
                            $provider_type=$datos['results'][0]['enumeration_type'];
                            if($provider_type=="NPI-1"){
                                $name=$datos['results'][0]['basic']['first_name']." ".$datos['results'][0]['basic']['last_name'];
                            }
                            if($provider_type=="NPI-2"){
                                $name=$datos['results'][0]['basic']['name'];
                            }
                        }
                        try{
                            $address=$datos['results'][0]['addresses'];

                            foreach ($address as $addr){
                                if($addr['address_purpose']=="LOCATION") {
                                    $street = $addr['address_1'];
                                    $codeP = $addr['postal_code'];
                                    $zipCode = substr($codeP, 0, 5);
                                    $phone = $addr['telephone_number'];
                                    $city = $addr['city'];
                                    $state = $addr['state'];
                                }
                            }
                        }
                        catch (\Exception $exception){

                        }
                        try{
                            $taxonomies=$datos['results'][0]['taxonomies'];

                            $taxonomy_G="";
                            foreach ($taxonomies as $taxonomy){
                                if($taxonomy['primary']==true){
                                    $taxonomy_G=$taxonomy['desc']." - (".$taxonomy['code'].")";
                                }
                            }

                            $status=$datos['results'][0]['basic']['status'];
                        }
                        catch (\Exception $exception){

                        }
                    }

                    $cell_A='A'.$cont_out;
                    $cell_B='B'.$cont_out;
                    $cell_C='C'.$cont_out;
                    $cell_D='D'.$cont_out;
                    $cell_E='E'.$cont_out;
                    $cell_F='F'.$cont_out;
                    $cell_G='G'.$cont_out;
                    $cell_H='H'.$cont_out;
                    $cell_I='I'.$cont_out;
                    $cell_J='J'.$cont_out;
                    $cell_K='K'.$cont_out;

                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, $name);
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_B, strval($npi));
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $in_bsn);
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $provider_type);
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, $street);
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $phone);
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $zipCode);
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $state);
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $city);
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_G, $taxonomy_G);
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_H, $status);

                    $cont_out++;
                }
            }
            $cont++;
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="directory.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        // Return a text response to the browser saying that the excel was succesfully created

        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/test-2", name="test-2")
     */
    public function saveRosterReportLog(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "fcc_roster_log/FCC_BSN_Roster20210208.xlsx";

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=1;

        foreach ($sheetData as $sheet) {
            if ($cont >= 1) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    echo $sheet['A']."<br/>";
                }
            }

            $cont++;
        }

        return new Response('All OK');
    }

    /**
     * @Route("/set-provider-application-receipt-date", name="provder_application-receipt-date")
     */
    public function saveProviderApplicationDate(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "fcc_pnv_log/FCC_BSN_PNV_20211013.xlsx";

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=1;

        foreach ($sheetData as $sheet) {
            if ($cont > 1) {
                if ($sheet['A'] != null or $sheet['A'] != "") {

                    $npi=$sheet['G'];
                    $date=$sheet['N'];

                    $year=substr($date,0,4);
                    $moth=substr($date,4,2);
                    $day=substr($date,6,2);

                    $date_str=$moth."/".$day."/".$year;

                    $provider=$em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number'=>$npi));
                    if($provider){
                      $provider->setProviderApplicationReceiptDate($date_str);
                      $em->persist($provider);
                    }
                }
            }

            $cont++;
        }

        $em->flush();

        //verify for all providers
        $providers=$em->getRepository('App\Entity\Provider')->findAll();

        foreach ($providers as $provider){
            if($provider->getProviderApplicationReceiptDate()==null or $provider->getProviderApplicationReceiptDate()==""){
                if($provider->getCreatedAt()!=null and $provider->getCreatedAt()!=""){
                    $created_date=$provider->getCreatedAt()->format('m/d/Y');
                    $provider->setProviderApplicationReceiptDate($created_date);
                    $em->persist($provider);
                }
            }
        }
        $em->flush();
        return new Response('All OK');
    }

    /**
     * @Route("/providers-without-address", name="provder_without_address")
     */
    public function getProvidersWithOutAddress(){
        $em=$this->getDoctrine()->getManager();

        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));

        $cont=1;
        foreach ($providers as $provider){
            if($provider->getNpiNumber()!=""){
                $addrs=$provider->getBillingAddress();
                if(count($addrs)==0){
                    $organization=$provider->getOrganization();
                    if($organization and $organization->getOrganizationStatus()->getId()==2){
                        $org_address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                        if(count($org_address)!=0){
                            echo  $provider->getLastName()."<br/>";
                            //$provider->getFirstName()." ".$provider->getLastName()
                            $cont++;
                        }
                    }
                }
            }
        }

        die();
    }

    /**
     * @Route("/providers-fix-address", name="provder_fix_address")
     */
    public function getProvidersFixAddress(){
        $em=$this->getDoctrine()->getManager();

        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));

        foreach ($providers as $provider){
            $addrs=$provider->getBillingAddress();
            if(count($addrs)==0){
               $organization=$provider->getOrganization();
               if($organization){
                   $org_address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                   if(count($org_address)==1){
                       echo $provider->getId()." ".$provider->getNpiNumber()."<br/>";
                       foreach ($org_address as $addr_o){
                           $provider->addBillingAddress($addr_o);
                           $em->persist($provider);
                       }
                       $em->flush();
                   }
               }
            }
        }

        die();
    }

    /**
     * @Route("/move-facilities-credentialing", name="move-facilities-credentialing")
     */
    public function movefacilitiesCredentialing(){
        $em=$this->getDoctrine()->getManager();

        $currents=$em->getRepository('App\Entity\BillingAddressCvo')->findAll();

        $organization_ids=[];
        foreach ($currents as $item){
            $address=$item->getBillingAddress();
            $organization=$address->getOrganization();


            if(!in_array($organization->getId(),$organization_ids)){
                $organization_ids[]=$organization->getId();

                $org_credentialing=new OrganizationCredentialing();
                $org_credentialing->setOrganization($organization);
                $org_credentialing->setCredentialingStatus($item->getCredentialingStatus());
                $org_credentialing->setCvo($item->getCvo());
                $org_credentialing->setCredentialingDate($item->getCredentialingDate());
                $org_credentialing->setCredentialingReceivedDate($item->getCredentialingReceivedDate());
                $org_credentialing->setCredentialingAcceptedDate($item->getCredentialingAcceptedDate());
                $org_credentialing->setCredentialingCompleteDate($item->getCredentialingCompleteDate());
                $org_credentialing->setSanctionDate($item->getSanctionDate());
                $org_credentialing->setCredentialingDeniedDate($item->getCredentialingDeniedDate());
                $org_credentialing->setCredentialingEffectiveDate($item->getCredentialingEffectiveDate());

                $em->persist($org_credentialing);
            }
        }

        $em->flush();

        die();
    }

    /**
     * @Route("/set-pt-ftom-degree", name="move-facilities-credentialing")
     */
    public function setProviderTypeFromDegree(){
        $em=$this->getDoctrine()->getManager();
        $providers=$em->getRepository('App:Provider')->findAll();

        $cont=1;
        foreach ($providers as $provider){
            $degrees=$provider->getDegree();
            $providerTypes=$provider->getProviderType();
            if(count($degrees)>0){
                if(count($providerTypes)==0){
                   /*
                    foreach ($degrees as $degree){
                        if($degree->getId()==1 or $degree->getId()==34){
                             $providerTyte=$em->getRepository('App:ProviderType')->find(12);
                             $provider->addProviderType($providerTyte);
                        }

                        if($degree->getId()==2 or $degree->getId()==24 or $degree->getId()==20){
                            $providerTyte=$em->getRepository('App:ProviderType')->find(40);
                            $provider->addProviderType($providerTyte);
                        }

                        if($degree->getId()==16 or $degree->getId()==22 or $degree->getId()==30 or $degree->getId()==32 or $degree->getId()==9 or $degree->getId()==4 or $degree->getId()==26 or $degree->getId()==25){
                            $providerTyte=$em->getRepository('App:ProviderType')->find(14);
                            $provider->addProviderType($providerTyte);
                        }

                        if($degree->getId()==3){
                            $providerTyte=$em->getRepository('App:ProviderType')->find(10);
                            $provider->addProviderType($providerTyte);
                        }

                        if($degree->getId()==5 or $degree->getId()==6 or $degree->getId()==18 or $degree->getId()==10 or $degree->getId()==11){
                            $providerTyte=$em->getRepository('App:ProviderType')->find(4);
                            $provider->addProviderType($providerTyte);
                        }

                        if($degree->getId()==7){
                            $providerTyte=$em->getRepository('App:ProviderType')->find(9);
                            $provider->addProviderType($providerTyte);
                        }

                        if($degree->getId()==19){
                            $providerTyte=$em->getRepository('App:ProviderType')->find(11);
                            $provider->addProviderType($providerTyte);
                        }

                        if($degree->getId()==13){
                            $providerTyte=$em->getRepository('App:ProviderType')->find(39);
                            $provider->addProviderType($providerTyte);
                        }

                        $em->flush();
                        $em->persist($provider);
                    }*/

                   echo $cont." ".$provider->getNpiNumber()."<br/>";
                   $cont++;
                }
            }

        }

        die();
    }

    /**
     * @Route("/medicare-6-7-11", name="medicare_report_6_7_11")
     */
    public function medicareReport6_7_11(){
        $em=$this->getDoctrine()->getManager();
        $providers=$em->getRepository('App:Provider')->findBy(array('disabled'=>0));

        $cont1=0;
        $cont2=0;
        $cont3=0;
        $cont4=0;
        $cont5=0;
        $cont6=0;
        $cont7=0;
        $cont8=0;
        $cont9=0;
        $cont10=0;
        $cont11=0;

        $cont=1;
        foreach ($providers as $provider){
            $organization=$provider->getOrganization();
            if($provider->getNpiNumber()!="" and $provider->getNpiNumber()!="NOT AVAILABLE"){

                $is_credentialing=false;
                $credentialings=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status'=>4, 'npi_number'=>$provider->getNpiNumber()));

                if($credentialings and count($credentialings)>0){
                    $is_credentialing=true;
                }else{
                    $is_facility=$provider->getIsFacility();

                    if($is_facility){
                        $fac_cred=$em->getRepository('App:OrganizationCredentialing')->findBy(array('organization'=>$organization->getId(),'credentialing_status'=>4));

                        if($fac_cred and count($fac_cred)>0){
                            $is_credentialing=true;
                        }
                    }
                }

                if($this->hasValidMedicaid($provider) and $organization->getOrganizationStatus()->getId()==2 and $is_credentialing){
                    $address=$provider->getBillingAddress();
                    if($address){
                        foreach ($address as $addr){
                            if($addr->getRegion()==1){
                                $cont1++;
                            }
                            if($addr->getRegion()==2){
                                $cont2++;
                            }
                            if($addr->getRegion()==3){
                                $cont3++;
                            }
                            if($addr->getRegion()==4){
                                $cont4++;
                            }
                            if($addr->getRegion()==5){
                                $cont5++;
                            }
                            if($addr->getRegion()==6){
                                $cont6++;
                            }
                            if($addr->getRegion()==7){
                                $cont7++;
                            }
                            if($addr->getRegion()==8){
                                $cont8++;
                            }
                            if($addr->getRegion()==9){
                                $cont9++;
                            }
                            if($addr->getRegion()==10){
                                $cont10++;
                            }
                            if($addr->getRegion()==11){
                                $cont11++;
                            }
                        }
                    }
                }
            }
        }

        echo $cont1."<br/>";
        echo $cont2."<br/>";
        echo $cont3."<br/>";
        echo $cont4."<br/>";
        echo $cont5."<br/>";
        echo $cont6."<br/>";
        echo $cont7."<br/>";
        echo $cont8."<br/>";
        echo $cont9."<br/>";
        echo $cont10."<br/>";
        echo $cont11."<br/>";

        die();
    }


    /**
     * @Route("/childern-by-county-2", name="medicaid_by_county_2")
     */
    public function MedicaidByCounty(){
        $em=$this->getDoctrine()->getManager();
        $providers=$em->getRepository('App:Provider')->findBy(array('disabled'=>0));

        $ALACHUA=0;
        $BAKER=0;
        $BAY=0;
        $BRADFORD=0;
        $BREVARD=0;
        $BROWARD=0;
        $CALHOUN=0;
        $CHARLOTTE=0;
        $CITRUS=0;
        $CLAY=0;
        $COLLIER=0;
        $COLUMBIA=0;
        $MIAMIDADE=0;
        $DESOTO=0;
        $DIXIE=0;
        $DUVAL=0;
        $ESCAMBIA=0;
        $FLAGLER=0;
        $FRANKLIN=0;
        $GADSDEN=0;
        $GILCHRIST=0;
        $GLADES=0;
        $GULF=0;
        $HAMILTON=0;
        $HARDEE=0;
        $HENDRY=0;
        $HERNANDO=0;
        $HIGHLANDS=0;
        $HILLSBOROUGH=0;
        $HOLMES=0;
        $INDIANRIVER=0;
        $JACKSON=0;
        $JEFFERSON=0;
        $LAFAYETTE=0;
        $LAKE=0;
        $LEE=0;
        $LEON=0;
        $LEVY=0;
        $LIBERTY=0;
        $MADISON=0;
        $MANATTEE=0;
        $MARION=0;
        $MARTIN=0;
        $MONROE=0;
        $NASSAU=0;
        $OKALOOSA=0;
        $OKEECHOBEE=0;
        $ORANGE=0;
        $OSCEOLA=0;
        $PALMBEACH=0;
        $PASCO=0;
        $PINELLAS=0;
        $POLK=0;
        $PUTNAM=0;
        $STJOHNS=0;
        $STLUCIE=0;
        $SANTAROSA=0;
        $SARASOTA=0;
        $SEMINOLE=0;
        $SUMTER=0;
        $SUWANNEE=0;
        $TAYLOR=0;
        $UNION=0;
        $VOLUSIA=0;
        $WAKULLA=0;
        $WALTON=0;
        $WASHINGTON=0;

        $region1=0;
        $region2=0;
        $region3=0;
        $region4=0;
        $region5=0;
        $region6=0;
        $region7=0;
        $region8=0;
        $region9=0;
        $region10=0;
        $region11=0;

        $region_medicaid_1=0;
        $region_medicaid_2=0;
        $region_medicaid_3=0;
        $region_medicaid_4=0;
        $region_medicaid_5=0;
        $region_medicaid_6=0;
        $region_medicaid_7=0;
        $region_medicaid_8=0;
        $region_medicaid_9=0;
        $region_medicaid_10=0;
        $region_medicaid_11=0;

        $region_medicare_1=0;
        $region_medicare_2=0;
        $region_medicare_3=0;
        $region_medicare_4=0;
        $region_medicare_5=0;
        $region_medicare_6=0;
        $region_medicare_7=0;
        $region_medicare_8=0;
        $region_medicare_9=0;
        $region_medicare_10=0;
        $region_medicare_11=0;


        $region_comercial_1=0;
        $region_comercial_2=0;
        $region_comercial_3=0;
        $region_comercial_4=0;
        $region_comercial_5=0;
        $region_comercial_6=0;
        $region_comercial_7=0;
        $region_comercial_8=0;
        $region_comercial_9=0;
        $region_comercial_10=0;
        $region_comercial_11=0;

        $total=0;
        foreach ($providers as $provider){
            $organization=$provider->getOrganization();
            $is_credentialing=false;
            $credentialings=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status'=>4, 'npi_number'=>$provider->getNpiNumber()));

            if($credentialings and count($credentialings)>0){
                $is_credentialing=true;
            }else{
                $is_facility=$provider->getIsFacility();

                if($is_facility){
                    $fac_cred=$em->getRepository('App:OrganizationCredentialing')->findBy(
                        array('organization'=>$organization->getId(),'credentialing_status'=>4)
                    );

                    if($fac_cred and count($fac_cred)>0){
                        $is_credentialing=true;
                    }
                }
            }

            if($provider->getNpiNumber()!="" and $provider->getNpiNumber()!="NOT AVAILABLE"){
                $address=$provider->getBillingAddress();
                $cont_addr=0;
                if($address and count($address)>0){
                    foreach ($address as $addr){
                        $min=intval($addr->getOfficeAgeLimit());
                        if($min<100){
                            if($addr->getCountyMatch()!=null){
                                if($addr->getCountyMatch()->getId()==68){
                                    $ALACHUA++;$total++;$region3++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_3++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_3++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_3++; }
                                }
                                if($addr->getCountyMatch()->getId()==69){
                                    $BAKER++;$total++;$region4++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_4++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_4++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_4++; }
                                }
                                if($addr->getCountyMatch()->getId()==70){
                                    $BAY++;$total++;$region2++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_2++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_2++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_2++; }
                                }
                                if($addr->getCountyMatch()->getId()==71){
                                    $BRADFORD++;$total++;$region3++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_3++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_3++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_3++; }
                                }
                                if($addr->getCountyMatch()->getId()==72){
                                    $BREVARD++;$total++;$region7++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_7++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_7++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_7++; }
                                }
                                if($addr->getCountyMatch()->getId()==73){
                                    $BROWARD++;$total++;$region10++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_10++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_10++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_10++; }
                                }
                                if($addr->getCountyMatch()->getId()==74){
                                    $CALHOUN++;$total++;$region2++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_2++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_2++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_2++; }
                                }
                                if($addr->getCountyMatch()->getId()==75){
                                    $CHARLOTTE++;$total++;$region8++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_8++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_8++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_8++; }
                                }
                                if($addr->getCountyMatch()->getId()==76){
                                    $CITRUS++;$total++;$region3++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_3++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_3++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_3++; }
                                }
                                if($addr->getCountyMatch()->getId()==77){
                                    $CLAY++;$total++;$region4++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_4++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_4++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_4++; }
                                }
                                if($addr->getCountyMatch()->getId()==78){
                                    $COLLIER++;$total++;$region8++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_8++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_8++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_8++; }
                                }
                                if($addr->getCountyMatch()->getId()==80){
                                    $MIAMIDADE++;$total++;$region11++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_11++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_11++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_11++; }
                                }
                                if($addr->getCountyMatch()->getId()==81){
                                    $DESOTO++;$total++;$region8++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_8++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_8++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_8++; }
                                }
                                if($addr->getCountyMatch()->getId()==82){
                                    $DIXIE++;$total++;$region3++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_3++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_3++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_3++; }
                                }
                                if($addr->getCountyMatch()->getId()==83){
                                    $DUVAL++;$total++;$region4++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_4++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_4++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_4++; }
                                }
                                if($addr->getCountyMatch()->getId()==84){
                                    $ESCAMBIA++;$total++;$region1++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_1++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_1++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_1++; }
                                }
                                if($addr->getCountyMatch()->getId()==85){
                                    $FLAGLER++;$total++;$region4++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_4++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_4++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_4++; }
                                }
                                if($addr->getCountyMatch()->getId()==86){
                                    $FRANKLIN++;$total++;$region2++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_2++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_2++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_2++; }
                                }
                                if($addr->getCountyMatch()->getId()==87){
                                    $GADSDEN++;$total++;$region2++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_2++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_2++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_2++; }
                                }
                                if($addr->getCountyMatch()->getId()==88){
                                    $GILCHRIST++;$total++;$region3++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_3++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_3++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_3++; }
                                }
                                if($addr->getCountyMatch()->getId()==89){
                                    $GLADES++;$total++;$region8++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_8++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_8++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_8++; }
                                }
                                if($addr->getCountyMatch()->getId()==90){
                                    $GULF++;$total++;$region2++;
                                    if($this->hasValidMedicaid($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicaid_2++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_2++; }
                                }
                                if($addr->getCountyMatch()->getId()==91){
                                    $HAMILTON++;$total++;$region3++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_3++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_3++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_3++; }
                                }
                                if($addr->getCountyMatch()->getId()==92){
                                    $HARDEE++;$total++;$region6++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_6++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_6++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_6++; }
                                }
                                if($addr->getCountyMatch()->getId()==93){
                                    $HENDRY++;$total++;$region8++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_8++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_8++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_8++; }
                                }
                                if($addr->getCountyMatch()->getId()==94){
                                    $HERNANDO++;$total++;$region3++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_3++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_3++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_3++; }
                                }
                                if($addr->getCountyMatch()->getId()==95){
                                    $HIGHLANDS++;$total++;$region6++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_6++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_6++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_6++; }
                                }
                                if($addr->getCountyMatch()->getId()==96){
                                    $HILLSBOROUGH++;$total++;$region6++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_6++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_6++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_6++; }
                                }
                                if($addr->getCountyMatch()->getId()==97){
                                    $HOLMES++;$total++;$region2++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_2++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_2++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_2++; }
                                }
                                if($addr->getCountyMatch()->getId()==98){
                                    $INDIANRIVER++;$total++;$region9++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_9++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_9++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_9++; }
                                }
                                if($addr->getCountyMatch()->getId()==99){
                                    $JACKSON++;$total++;$region2++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_2++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_2++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_2++; }
                                }
                                if($addr->getCountyMatch()->getId()==100){
                                    $JEFFERSON++;$total++;$region2++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_2++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_2++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_2++; }
                                }
                                if($addr->getCountyMatch()->getId()==101){
                                    $LAFAYETTE++;$total++;$region3++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_3++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_3++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_3++; }
                                }
                                if($addr->getCountyMatch()->getId()==102){
                                    $LAKE++;$total++;$region3++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_3++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_3++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_3++; }
                                }
                                if($addr->getCountyMatch()->getId()==103){
                                    $LEE++;$total++;$region8++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_8++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_8++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_8++; }
                                }
                                if($addr->getCountyMatch()->getId()==104){
                                    $LEON++;$total++;$region2++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_2++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_2++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_2++; }
                                }
                                if($addr->getCountyMatch()->getId()==105){
                                    $LEVY++;$total++;$region3++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_3++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_3++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_3++; }
                                }
                                if($addr->getCountyMatch()->getId()==106){
                                    $LIBERTY++;$total++;$region2++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_2++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_2++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_2++; }
                                }
                                if($addr->getCountyMatch()->getId()==107){
                                    $MADISON++;$total++;$region2++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_2++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_2++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_2++; }
                                }
                                if($addr->getCountyMatch()->getId()==108){
                                    $MANATTEE++;$total++;$region6++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_6++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_6++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_6++; }
                                }
                                if($addr->getCountyMatch()->getId()==109){
                                    $MARION++;$total++;$region3++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_3++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_3++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_3++; }
                                }
                                if($addr->getCountyMatch()->getId()==110){
                                    $MARTIN++;$total++;$region9++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_9++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_9++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_9++; }
                                }
                                if($addr->getCountyMatch()->getId()==111){
                                    $MONROE++;$total++;$region11++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_11++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_11++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_11++; }
                                }
                                if($addr->getCountyMatch()->getId()==112){
                                    $NASSAU++;$total++;$region4++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_4++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_4++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_4++; }
                                }
                                if($addr->getCountyMatch()->getId()==113){
                                    $OKALOOSA++;$total++;$region1++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_1++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_1++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_1++; }
                                }
                                if($addr->getCountyMatch()->getId()==114){
                                    $OKEECHOBEE++;$total++;$region9++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_9++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_9++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_9++; }
                                }
                                if($addr->getCountyMatch()->getId()==115){
                                    $ORANGE++;$total++;$region7++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_7++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_7++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_7++; }
                                }
                                if($addr->getCountyMatch()->getId()==116){
                                    $OSCEOLA++;$total++;$region7++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_7++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_7++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_7++; }
                                }
                                if($addr->getCountyMatch()->getId()==117){
                                    $PALMBEACH++;$total++;$region9++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_9++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_9++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_9++; }
                                }
                                if($addr->getCountyMatch()->getId()==118){
                                    $PASCO++;$total++;$region5++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_5++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_5++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_5++; }
                                }
                                if($addr->getCountyMatch()->getId()==119){
                                    $PINELLAS++;$total++;$region5++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_5++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_5++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_5++; }
                                }
                                if($addr->getCountyMatch()->getId()==120){
                                    $POLK++;$total++;$region6++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_6++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_6++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_6++; }
                                }
                                if($addr->getCountyMatch()->getId()==121){
                                    $PUTNAM++;$total++;$region3++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_3++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_3++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_3++; }
                                }
                                if($addr->getCountyMatch()->getId()==122){
                                    $STJOHNS++;$total++;$region4++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_4++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_4++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_4++; }
                                }
                                if($addr->getCountyMatch()->getId()==123){
                                    $STLUCIE++;$total++;$region9++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_9++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_9++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_9++; }
                                }
                                if($addr->getCountyMatch()->getId()==124){
                                    $SANTAROSA++;$total++;$region1++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_1++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_1++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_1++; }
                                }
                                if($addr->getCountyMatch()->getId()==125){
                                    $SARASOTA++;$total++;$region8++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_8++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_8++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_8++; }
                                }
                                if($addr->getCountyMatch()->getId()==126){
                                    $SEMINOLE++;$total++;$region7++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_7++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_7++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_7++; }
                                }
                                if($addr->getCountyMatch()->getId()==127){
                                    $SUMTER++;$total++;$region3++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_3++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_3++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_3++; }
                                }
                                if($addr->getCountyMatch()->getId()==128){
                                    $SUWANNEE++;$total++;$region3++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_3++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_3++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_3++; }
                                }
                                if($addr->getCountyMatch()->getId()==129){
                                    $TAYLOR++;$total++;$region2++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_2++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_2++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_2++; }
                                }
                                if($addr->getCountyMatch()->getId()==130){
                                    $UNION++;$total++;$region3++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_3++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_3++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_3++; }
                                }
                                if($addr->getCountyMatch()->getId()==131){
                                    $VOLUSIA++;$total++;$region4++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_4++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_4++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_4++; }
                                }
                                if($addr->getCountyMatch()->getId()==132){
                                    $WAKULLA++;$total++;$region2++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_2++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_2++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_2++; }
                                }
                                if($addr->getCountyMatch()->getId()==133){
                                    $WALTON++;$total++;$region1++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_1++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_1++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_1++; }
                                }
                                if($addr->getCountyMatch()->getId()==134){
                                    $WASHINGTON++;$total++;$region2++;
                                    if($this->hasValidMedicaid($provider)) { $region_medicaid_2++; }
                                    if($this->hasValidMedicare($provider) and $this->hasValidMedicaid($provider)==false) { $region_medicare_2++; }
                                    if($this->hasValidMedicaid($provider)==false and $this->hasValidMedicare($provider)==false){ $region_comercial_2++; }
                                }
                            }

                            $cont_addr++;
                        }
                    }
                }
            }
        }


        echo $ALACHUA."<br/>";
        echo $BAKER."<br/>";
        echo $BAY."<br/>";
        echo $BRADFORD."<br/>";
        echo $BREVARD."<br/>";
        echo $BROWARD."<br/>";
        echo $CALHOUN."<br/>";
        echo $CHARLOTTE."<br/>";
        echo $CITRUS."<br/>";
        echo $CLAY."<br/>";
        echo $COLLIER."<br/>";
        echo $COLUMBIA."<br/>";
        echo $MIAMIDADE."<br/>";
        echo $DESOTO."<br/>";
        echo $DIXIE."<br/>";
        echo $DUVAL."<br/>";
        echo $ESCAMBIA."<br/>";
        echo $FLAGLER."<br/>";
        echo $FRANKLIN."<br/>";
        echo $GADSDEN."<br/>";
        echo $GILCHRIST."<br/>";
        echo $GLADES."<br/>";
        echo $GULF."<br/>";
        echo $HAMILTON."<br/>";
        echo $HARDEE."<br/>";
        echo $HENDRY."<br/>";
        echo $HERNANDO."<br/>";
        echo $HIGHLANDS."<br/>";
        echo $HILLSBOROUGH."<br/>";
        echo $HOLMES."<br/>";
        echo $INDIANRIVER."<br/>";
        echo $JACKSON."<br/>";
        echo $JEFFERSON."<br/>";
        echo $LAFAYETTE."<br/>";
        echo $LAKE."<br/>";
        echo $LEE."<br/>";
        echo $LEON."<br/>";
        echo $LEVY."<br/>";
        echo $LIBERTY."<br/>";
        echo $MADISON."<br/>";
        echo $MANATTEE."<br/>";
        echo $MARION."<br/>";
        echo $MARTIN."<br/>";
        echo $MONROE."<br/>";
        echo $NASSAU."<br/>";
        echo $OKALOOSA."<br/>";
        echo $OKEECHOBEE."<br/>";
        echo $ORANGE."<br/>";
        echo $OSCEOLA."<br/>";
        echo $PALMBEACH."<br/>";
        echo $PASCO."<br/>";
        echo $PINELLAS."<br/>";
        echo $POLK."<br/>";
        echo $PUTNAM."<br/>";
        echo $STJOHNS."<br/>";
        echo $STLUCIE."<br/>";
        echo $SANTAROSA."<br/>";
        echo $SARASOTA."<br/>";
        echo $SEMINOLE."<br/>";
        echo $SUMTER."<br/>";
        echo $SUWANNEE."<br/>";
        echo $TAYLOR."<br/>";
        echo $UNION."<br/>";
        echo $VOLUSIA."<br/>";
        echo $WAKULLA."<br/>";
        echo $WALTON."<br/>";
        echo $WASHINGTON."<br/>";

        echo "----------<br/>";
        echo $region1."<br/>";
        echo $region2."<br/>";
        echo $region3."<br/>";
        echo $region4."<br/>";
        echo $region5."<br/>";
        echo $region6."<br/>";
        echo $region7."<br/>";
        echo $region8."<br/>";
        echo $region9."<br/>";
        echo $region10."<br/>";
        echo $region11."<br/>";


        echo "Only Medicaid<br/>";
        echo $region_medicaid_1."<br/>";
        echo $region_medicaid_2."<br/>";
        echo $region_medicaid_3."<br/>";
        echo $region_medicaid_4."<br/>";
        echo $region_medicaid_5."<br/>";
        echo $region_medicaid_6."<br/>";
        echo $region_medicaid_7."<br/>";
        echo $region_medicaid_8."<br/>";
        echo $region_medicaid_9."<br/>";
        echo $region_medicaid_10."<br/>";
        echo $region_medicaid_11."<br/>";

        echo "Only Comercial<br/>";
        echo $region_comercial_1."<br/>";
        echo $region_comercial_2."<br/>";
        echo $region_comercial_3."<br/>";
        echo $region_comercial_4."<br/>";
        echo $region_comercial_5."<br/>";
        echo $region_comercial_6."<br/>";
        echo $region_comercial_7."<br/>";
        echo $region_comercial_8."<br/>";
        echo $region_comercial_9."<br/>";
        echo $region_comercial_10."<br/>";
        echo $region_comercial_11."<br/>";

        echo "Only Medicare<br/>";
        echo $region_medicare_1."<br/>";
        echo $region_medicare_2."<br/>";
        echo $region_medicare_3."<br/>";
        echo $region_medicare_4."<br/>";
        echo $region_medicare_5."<br/>";
        echo $region_medicare_6."<br/>";
        echo $region_medicare_7."<br/>";
        echo $region_medicare_8."<br/>";
        echo $region_medicare_9."<br/>";
        echo $region_medicare_10."<br/>";
        echo $region_medicare_11."<br/>";

        die();
    }

    private function hasValidMedicaid($provider){
        $em=$this->getDoctrine()->getManager();
        $medicaid=$provider->getMedicaid();

        if($medicaid!="-" and $medicaid!=null  and $medicaid!="" and $medicaid!="blank" and $medicaid!="" and $medicaid!="N/A" and $medicaid!="n/a" and $medicaid!="credentialing in process" and
        $medicaid!="Pending"){
            return  true;
        }else{
            $pml=$em->getRepository('App:PML2')->findBy(array('colN'=>$provider->getNpiNumber(),'colS'=>'A', 'colW'=>'A'));
            if($pml and count($pml)>0){
                return true;
            }
        }

       return false;
    }


    private function hasValidMedicare($provider){
        $em=$this->getDoctrine()->getManager();
        $medicare=$provider->getMedicare();

        if($medicare!="-" and $medicare!=null  and $medicare!="" and $medicare!="blank" and $medicare!="" and $medicare!="N/A" and $medicare!="n/a" and $medicare!="credentialing in process" and
            $medicare!="Pending"){
            return  true;
        }else{
            $organization=$provider->getOrganization();
            $address=$em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization->getId()));
            if($address){
                foreach ($address as $addr){
                    $addr_medicare=$addr->getGroupMedicare();
                    if($addr_medicare!="-" and $addr_medicare!=null  and $addr_medicare!="" and $addr_medicare!="blank" and $addr_medicare!="" and $addr_medicare!="N/A" and $addr_medicare!="n/a"
                        and $addr_medicare!="credentialing in process" and $addr_medicare!="Pending"){
                        return  true;
                    }
                }
            }

            $providers=$em->getRepository('App:Provider')->findBy(array('organization'=>$organization->getId()));

            if($providers){
                foreach ($providers as $pro){
                    $o_medicare=$pro->getMedicare();
                    if($o_medicare!="-" and $o_medicare!=null  and $o_medicare!="" and $o_medicare!="blank" and $o_medicare!="" and $o_medicare!="N/A" and $o_medicare!="n/a"
                        and $o_medicare!="credentialing in process" and $o_medicare!="Pending"){
                        return  true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * @Route("/facilities-by-counties-data", name="facilities_by_Counties_data")
     */
    public function facilitiesByCountiesData(){
        $em=$this->getDoctrine()->getManager();
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');
        $date=date('m/d/Y');

        $spreadsheet = $reader->load($url.'facilities-template.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $organizations=$em->getRepository('App:Organization')->findBy(array('organization_status'=>2, 'disabled'=>0));

        $cont=3;
        $spreadsheet->setActiveSheetIndex(0);
        foreach ($organizations as $organization){
            $address=$em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization->getId()));

            $is_facility=false;
            $is_county=false;
            if($address!=null){
                foreach ($address as $addr){
                    //$addr=new BillingAddress();
                    if($addr->getIsFacility()){
                        $is_facility=true;

                        if($addr->getCountyMatch()){
                            if($addr->getCountyMatch()->getId()==115 or $addr->getCountyMatch()->getId()==131 or $addr->getCountyMatch()->getId()==96){
                                $is_county=true;
                            }
                        }
                    }
                }
            }

            if($is_facility and $is_county) {
                foreach ($address as $addr) {
                    if ($addr->getCountyMatch()) {
                        if ($addr->getCountyMatch()->getId() == 115 or $addr->getCountyMatch()->getId() == 131 or $addr->getCountyMatch()->getId() == 96) {
                            $providers = $addr->getProviders();

                            $totalProviders = count($providers);

                            $cell_A = 'A' . $cont;
                            $cell_B = 'B' . $cont;
                            $cell_C = 'C' . $cont;
                            $cell_D = 'D' . $cont;
                            $cell_E = 'E' . $cont;
                            $cell_F = 'F' . $cont;
                            $cell_G = 'G' . $cont;

                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $organization->getName());
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $addr->getStreet());
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $addr->getSuiteNumber());
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, 'FL');
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $addr->getCounty());
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $addr->getPhoneNumber());
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, strval($totalProviders));
                            $cont++;
                        }
                    }
                }
            }
        }


        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Facilities_Report.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }


    /**
     * @Route("/childern-by-county-map", name="medicaid_by_county")
     */
    public function MedicaidByCountyMAP(){
        $google_apikey = $this->getParameter('google_apikey');


        return $this->render('test/map.html.twig',[
            'google_apikey' => $google_apikey
        ]);
    }

    /**
     * @Route("/providers-in-progress", name="providers-in-progress-report")
     */
    public function providersInProgress(){
            $em=$this->getDoctrine()->getManager();

            $providers=$em->getRepository('App:Provider')->findBy(array('disabled'=>0));

            foreach ($providers as $provider){
                $npi=$provider->getNpiNumber();
                $organization=$provider->getOrganization();

                if($organization->getOrganizationStatus()->getId()!=2){
                     /* if($provider->getIsFacility()){
                          echo "Yes<br/>";
                      }else{
                          echo "No<br/>";
                      }*/
                       /* $degresses=$provider->getDegree();
                        $degree_str="";
                        if($degresses!=null and count($degresses)>0){
                            foreach ($degresses as $degress){
                                $degree_str.=$degress->getName().", ";
                            }
                        }

                        $degree_str=substr($degree_str,0,-2);

                        echo $degree_str."<br/>"; */

                }else{

                    $credentialing=$em->getRepository('App:CredSimpleRecord')->findBy(array('npi'=>$npi));
                    $credentialing_facility=$em->getRepository('App:OrganizationCredentialing')->findBy(array('organization'=>$organization->getId()));

                    if($credentialing==null and $credentialing_facility==null){
                        if($npi!="NOT AVAILABLE" and $npi!="000000" and $npi!=""){
                           // echo $provider->getMedicare()."<br/>";
                           /* if($provider->getIsFacility()){
                                echo "Yes<br/>";
                            }else{
                                echo "No<br/>";
                            }*/

                            $degresses=$provider->getDegree();
                            $degree_str="";
                            if($degresses!=null and count($degresses)>0){
                                foreach ($degresses as $degress){
                                    $degree_str.=$degress->getName().", ";
                                }
                            }

                            $degree_str=substr($degree_str,0,-2);

                            echo $degree_str."<br/>";
                        }

                    }

                }

            }

            die();
    }

    /**
     * @Route("/providers-payer-status", name="providers-payer-status-report")
     */
    public function providersPayerStatus()
    {
        $em = $this->getDoctrine()->getManager();

        $providers=$em->getRepository('App:Provider')->findBy(array('disabled'=>0));

        foreach ($providers as $provider) {
            $npi = $provider->getNpiNumber();

        }


        die();
    }

    /**
     * @Route("/organizations-notes", name="organizations-notes")
     */
    public function Organizations_NOTES()
    {
        $em = $this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App:Organization')->findBy(array('disabled'=>0));
        foreach ($organizations as $organization){
            if($organization->getNotes()!="" or $organization->getNonStandardRates()){
                echo $organization->getGroupNpi()." ".$organization->getNonStandardRates()."<br/>";
            }
        }


        die();
    }

    /**
     * @Route("/organizations-rates-status", name="organizations-rates-status-report")
     */
    public function OrganizationRatesStatus(){
        $em=$this->getDoctrine()->getManager();
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');

        $spreadsheet = $reader->load($url.'organizations-rates-template.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $organizations=$em->getRepository('App:Organization')->findBy(array('disabled'=>0));

        $cont=2;
        $spreadsheet->setActiveSheetIndex(0);
        foreach ($organizations as $organization){

            $cell_A = 'A' . $cont;
            $cell_B = 'B' . $cont;
            $cell_C = 'C' . $cont;
            $cell_D = 'D' . $cont;
            $cell_E = 'E' . $cont;
            $cell_F = 'F' . $cont;
            $cell_G = 'G' . $cont;
            $cell_H = 'H' . $cont;

            $status="active";
            if($organization->getOrganizationStatus()->getId()==1){
                $status="pending";
            }

            $standard_rates="Yes";

            if($organization->getStandardRates()==true){
                $standard_rates="No";
            }

            $medicare="";
            $medicaid="";
            $commercial="";
            $eap="";

            $org_rates=$em->getRepository('App:OrganizationRates')->findBy(array('organization'=>$organization->getId()));
            if($org_rates){
                foreach ($org_rates as $rate){
                    if($rate->getRate()->getId()==1){
                        $medicare=strval($rate->getPercent());
                    }
                    if($rate->getRate()->getId()==2){
                        $medicaid=strval($rate->getPercent());
                    }
                    if($rate->getRate()->getId()==3){
                        $commercial=strval($rate->getPercent());
                    }
                    if($rate->getRate()->getId()==4){
                        $eap=strval($rate->getPercent());
                    }
                }
            }

            $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, strtoupper($organization->getName()));
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $organization->getGroupNpi());
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $standard_rates);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $medicare);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $medicaid);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $commercial);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $eap);

            $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $status);
            $cont++;
        }


        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Organization_Rates_Report.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }
}
