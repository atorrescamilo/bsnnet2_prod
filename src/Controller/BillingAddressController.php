<?php

namespace App\Controller;

use App\Entity\AddressFacilityLicense;
use App\Entity\ChangeLog;
use App\Entity\Organization;
use App\Entity\Provider;
use App\Entity\ViewProvider;
use App\Form\ProviderPType;
use App\Utils\My_Mcript;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\BillingAddress;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Service\BillingAddressFileUploader;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader;

/**
 * @Route("/admin/billing")
 */
class BillingAddressController extends AbstractController{

    /**
     * @Route("/new/{org}", name="admin_new_billing",defaults={"org": null})
     */
    public function add($org){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $organizationProviders = $em->getRepository('App\Entity\ViewProvider')->findBy(array('organization'=>$org));
            $google_apikey = $this->getParameter('google_apikey');

            $serviceSettings = $em->getRepository('App\Entity\ServiceSettings')->findAll();
            $facilityType = $em->getRepository('App\Entity\FacilityType')->findAll('');
            $cvos = $em->getRepository('App\Entity\Cvo')->findAll(array('name' => 'ASC'));
            $payers = $em->getRepository('App\Entity\Payer')->findAll(array('name' => 'ASC'));
            $taxonomies=$em->getRepository('App\Entity\TaxonomyCode')->findAll();
            $age_range=$em->getRepository('App:AgesRange')->findAll();

            return $this->render('billing/add.html.twig', array('providers' => $organizationProviders, 'serviceSettings' => $serviceSettings, 'facilityType' => $facilityType,
                'cvos' => $cvos, 'payers' => $payers, 'org' => $org, 'google_apikey' => $google_apikey,'taxonomies'=>$taxonomies,'age_ranges'=>$age_range));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new-2/{org}", name="admin_new_billing-2",defaults={"org": null})
     */
    public function add2($org){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $organizationProviders = $em->getRepository('App\Entity\ViewProvider')->findBy(array('organization'=>$org));
            $google_apikey = $this->getParameter('google_apikey');




            return $this->render('billing/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}/{org}", name="admin_edit_billing",defaults={"id": null,"org":null})
     */
    public function edit($id, $org){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $db = $em->getConnection();

            $document = $em->getRepository('App\Entity\BillingAddress')->find($id);

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('admin_billing');
            }

            $organizationProviders = $em->getRepository('App:Provider')->findBy(array('organization'=>$org));
            $google_apikey = $this->getParameter('google_apikey');

            $serviceSettings = $em->getRepository('App\Entity\ServiceSettings')->findAll();
            $facilityType = $em->getRepository('App\Entity\FacilityType')->findAll('');
            $cvos = $em->getRepository('App\Entity\Cvo')->findAll(array('name' => 'ASC'));
            $payers = $em->getRepository('App\Entity\Payer')->findAll(array('name' => 'ASC'));

            $query1 = "SELECT * FROM provider_billing_address WHERE  provider_billing_address.billing_address_id=$id;";
            $stmt1 = $db->prepare($query1);
            $stmt1->execute();

            $providersBillingAddress=$stmt1->fetchAll();
            $providerOnAddress=array();

            if($providersBillingAddress!=null){
                foreach ($providersBillingAddress as $pba){
                    $provider=$em->getRepository('App\Entity\Provider')->find($pba['provider_id']);
                    if($provider){
                        $providerOnAddress[]=$provider;
                    }
                }
            }

            $taxonomies=$em->getRepository('App\Entity\TaxonomyCode')->findAll();
            $age_range=$em->getRepository('App:AgesRange')->findAll();

            $facility_licenses=$em->getRepository('App:AddressFacilityLicense')->findBy(array('address'=>$id));


            return $this->render('billing/edit.html.twig', array('providerOnAddress'=>$providerOnAddress, 'document' => $document,'org' => $org,'providers' => $organizationProviders, 'serviceSettings' => $serviceSettings, 'facilityType' => $facilityType,
                'cvos' => $cvos, 'payers' => $payers, 'org' => $org, 'google_apikey' => $google_apikey,'taxonomies'=>$taxonomies,'age_ranges'=>$age_range,
                'facility_licenses'=>$facility_licenses));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}/{org}", name="admin_view_billing",defaults={"id": null,"org":null})
     */
    public function view($id,$org){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $db = $em->getConnection();

            $document = $em->getRepository('App\Entity\BillingAddress')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_view_organization', ['id' => $org, 'tab:' => 2]);
            }

            $organizationProviders = $em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$org));

            $serviceSettings = $em->getRepository('App\Entity\ServiceSettings')->findAll();
            $facilityType = $em->getRepository('App\Entity\FacilityType')->findAll('');
            $cvos = $em->getRepository('App\Entity\Cvo')->findAll(array('name' => 'ASC'));
            $payers = $em->getRepository('App\Entity\Payer')->findAll(array('name' => 'ASC'));

            $AddressCvo = $em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$id));

            $query1 = "SELECT * FROM provider_billing_address WHERE  provider_billing_address.billing_address_id=$id;";
            $stmt1 = $db->prepare($query1);
            $stmt1->execute();

            $providersBillingAddress=$stmt1->fetchAll();
            $providerOnAddress=array();

            if($providersBillingAddress!=null){
                foreach ($providersBillingAddress as $pba){
                    $provider=$em->getRepository('App\Entity\Provider')->find($pba['provider_id']);
                    if($provider){
                        $providerOnAddress[]=$provider;
                    }
                }
            }

            return $this->render('billing/view.html.twig', array('providerOnAddress'=>$providerOnAddress,'document' => $document, 'org' => $org, 'providers' => $organizationProviders, 'serviceSettings' => $serviceSettings, 'facilityType' => $facilityType,
                'cvos' => $cvos, 'payers' => $payers, 'addresscvo' => $AddressCvo));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_billing")
     */
    public function create(Request $request, BillingAddressFileUploader $fileUploader){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $db = $em->getConnection();

            $save_status = $request->get('save_status');
            $form_step = $request->get('form_step');


            if ($save_status == 0) {
                $billinAddress = new BillingAddress();
            } else {
                $billinAddress = $em->getRepository('App\Entity\BillingAddress')->find($save_status);
            }

            if ($form_step == 1) {
                $street = $request->get('street');
                $suite_number = $request->get('suite_number');
                $city = $request->get('city');
                $region = $request->get('region');
                $county=$request->get('county');
                $state = $request->get('state');
                $zip_code = $request->get('zip_code');
                $country = $request->get('country');
                $latitude = $request->get('latitude');
                $longitude = $request->get('longitude');
                $phone = $request->get('phone');
                $org = $request->get('org');
                $organization = $em->getRepository('App\Entity\Organization')->find($org);
                $location_name=$request->get('location_name');

                $county=strtoupper($county);
                $countyMatch=$em->getRepository('App\Entity\CountyMatch')->findOneBy(array('name'=>$county));

                if($countyMatch!=null){
                    $billinAddress->setCountyMatch($countyMatch);
                }

                $billinAddress->setStreet($street);
                $billinAddress->setLocationName($location_name);
                $billinAddress->setSuiteNumber($suite_number);
                $billinAddress->setCity($city);
                $billinAddress->setRegion($region);
                $billinAddress->setUsState($state);
                $billinAddress->setZipCode($zip_code);
                $billinAddress->setCounty($county);
                $billinAddress->setCountry($country);
                if ($latitude != "")
                    $billinAddress->setLat($latitude);
                if ($longitude != "")
                    $billinAddress->setLng($longitude);
                $billinAddress->setPhoneNumber($phone);
                $billinAddress->setOrganization($organization);
                $em->persist($billinAddress);
                $em->flush();
            }
            if ($form_step == 2) {
                $business_hours = $request->get('business_hours_data');
                $serviceSelected = $request->get('serviceSelected');
                $is_see_patients_address = $request->get('is_see_patients_address');
                $appointment=$request->get('appointment');


                $sqlDelete = "DELETE FROM `billing_address_service_settings` WHERE `billing_address_service_settings`.`billing_address_id` =  $save_status";
                $stmt = $db->prepare($sqlDelete);
                $stmt->execute();

                $services = substr($serviceSelected, 0, -1);
                $lbs = explode(",", $services);

                if ($lbs != null) {
                    foreach ($lbs as $service) {
                        $serviceObj = $em->getRepository('App\Entity\ServiceSettings')->find($service);
                        if ($serviceObj != null) {
                            $billinAddress->addServiceSetting($serviceObj);
                        }
                    }
                }

                if($is_see_patients_address==1){
                    $billinAddress->setSeePatientsAddr(true);
                }else{
                    $billinAddress->setSeePatientsAddr(false);
                }

                $billinAddress->setBusinessHours($business_hours);
                $billinAddress->setAppointment($appointment);
                $em->persist($billinAddress);
                $em->flush();
            }
            if ($form_step == 3) {
                $number_of_beds = $request->get('number_of_beds');
                $inpatient_beds=$request->get('inpatient_beds');
                $location_npi = $request->get('location_npi');
                $location_npi2 = $request->get('location_npi2');
                $group_medicaid = $request->get('group_medicaid');
                $group_medicare = $request->get('group_medicare');
                $limit_age_min = $request->get('limit_age_min');
                $limit_age_max = $request->get('limit_age_max');
                $taxonomies=$request->get('taxonomies');
                $agesRangeSelected = $request->get('agesRanges');

                $billinAddress->setBeds($number_of_beds);
                $billinAddress->setHaveBeds($inpatient_beds);
                $billinAddress->setLocationNpi($location_npi);
                $billinAddress->setLocationNpi2($location_npi2);
                $billinAddress->setGroupMedicaid($group_medicaid);
                $billinAddress->setGroupMedicare($group_medicare);

                //remove age ranges
                $ages_range = $billinAddress->getAgeRanges();
                if ($ages_range != null) {
                    foreach ($ages_range as $age_range) {
                        $billinAddress->removeAgeRange($age_range);
                    }
                }

                $agesRange = substr($agesRangeSelected, 0, -1);
                $lbs = explode(",", $agesRange);

                if ($lbs != "") {
                    foreach ($lbs as $lb) {
                        $Age = $em->getRepository('App:AgesRange')->find($lb);
                        if ($Age != null) {
                            $billinAddress->addAgeRange($Age);
                            $em->persist($billinAddress);
                            $em->flush();
                        }
                    }
                }

                $actualTaxonomies=$billinAddress->getTaxonomyCodes();

                if($actualTaxonomies!=null){
                    foreach ($actualTaxonomies as $at){
                        $billinAddress->removeTaxonomyCode($at);
                        $em->persist($billinAddress);
                        $em->flush();
                    }
                }

                if($taxonomies!=""){
                   foreach ($taxonomies as $taxonomy){
                       $taxonomyObj=$em->getRepository('App\Entity\TaxonomyCode')->find($taxonomy);
                       if($taxonomyObj!=null){
                           $billinAddress->addTaxonomyCode($taxonomyObj);
                       }
                   }
                }

                if ($limit_age_min != "")
                    $billinAddress->setOfficeAgeLimit($limit_age_min);
                if ($limit_age_max != "")
                    $billinAddress->setOfficeAgeLimitMax($limit_age_max);

                $em->persist($billinAddress);
                $em->flush();
            }

            if ($form_step == 4) {
                $wheelchair_accessibility = $request->get('wheelchair_accessibility');
                $tdd = $request->get('tdd');
                $private_provider_transportation = $request->get('private_provider_transportation');
                $is_primary_address = $request->get('is_primary_address');
                $is_billing_address = $request->get('is_billing_address');
                $is_mailing_address = $request->get('is_mailing_address');
                $is_facility = $request->get('is_facility');
                $support_ehr=$request->get('support_ehr');
                $facilityTypesSelected = $request->get('facilityTypesSelected');
                $facility_exemption_number = $request->get('facility_exemption_number');
                $facility_license_data=$request->get('facility_license_data');

                $billinAddress->setWheelchair($wheelchair_accessibility);
                $billinAddress->setTdd($tdd);
                $billinAddress->setSupportEHR($support_ehr);
                $billinAddress->setPrivateProviderTransportation($private_provider_transportation);
                $billinAddress->setPrimaryAddr($is_primary_address);
                $billinAddress->setBillingAddr($is_billing_address);
                $billinAddress->setMailingAddr($is_mailing_address);
                $billinAddress->setIsFacility($is_facility);


                if ($facilityTypesSelected != "") {
                    $sqlDelete = "DELETE FROM `billing_address_facility_type` WHERE `billing_address_facility_type`.`billing_address_id` = $save_status";
                    $stmt = $db->prepare($sqlDelete);
                    $stmt->execute();

                    $facilities = substr($facilityTypesSelected, 0, -1);
                    $lbs = explode(",", $facilities);

                    foreach ($lbs as $facility) {
                        if ($facility != null) {
                            $facilityObj = $em->getRepository('App\Entity\FacilityType')->find($facility);
                            if ($facilityObj != null) {
                                $billinAddress->addFacilityType($facilityObj);
                            }
                        }
                    }
                }

                if ($facility_exemption_number == "") {
                    $billinAddress->setFacilityExemptionNumber(null);
                } else {
                    $billinAddress->setFacilityExemptionNumber($facility_exemption_number);
                }

                //process Facility Licenses Data
                $facility_license_data=substr($facility_license_data, 0, -1);

                if($facility_license_data!=""){

                    //remove all current records
                    $addr_facility_licenses=$em->getRepository('App:AddressFacilityLicense')->findBy(array('address'=>$billinAddress->getId()));
                    if($addr_facility_licenses){
                        foreach ($addr_facility_licenses as $addr_facility_license){
                            $em->remove($addr_facility_license);
                        }
                        $em->flush();
                    }

                    $facility_licenses = explode("|", $facility_license_data);
                    foreach ($facility_licenses as $facility_license){
                        $lbs=explode(",", $facility_license);
                        $facility_license_obj=new AddressFacilityLicense();
                        $facility_license_obj->setAhcaNumber($lbs[0]);
                        $facility_license_obj->setLicenseNumber($lbs[1]);
                        $facility_license_obj->setEffectiveDate($lbs[2]);
                        $facility_license_obj->setExpirationDate($lbs[3]);
                        $facility_license_obj->setFacilityType($lbs[4]);
                        $facility_license_obj->setBeds($lbs[5]);
                        $facility_license_obj->setAddress($billinAddress);

                        $em->persist($facility_license_obj);
                    }
                    $em->flush();
                }

                $em->persist($billinAddress);
                $em->flush();

                //verify all addresses for check the facility status and set provider to Facility Status
                $organization=$billinAddress->getOrganization();
                $allLocations=$em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization->getId()));
                $providers=$em->getRepository('App:Provider')->findBy(array('organization'=>$organization->getId()));
                $is_facility=false;
                if($allLocations){
                    foreach ($allLocations as $addr){
                        if($addr->getIsFacility()==true){
                            $is_facility=true;
                            break;
                        }
                    }
                }

                if($providers){
                    foreach ($providers as $provider){
                        $provider->setIsFacility($is_facility);
                        $em->persist($provider);
                    }
                }

                //check the address number and if is only one assign it to all providers in the organization
                $all_address=$em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization->getId()));
                if($all_address){
                    if(count($all_address)==1){
                        $providers=$em->getRepository('App:Provider')->findBy(array('organization'=>$organization->getId()));

                        if($providers){
                            foreach ($providers as $provider){
                                if($provider->getBillingAddress()==null or count($provider->getBillingAddress())==0){
                                    foreach ($all_address as $addr1){
                                        $provider->addBillingAddress($addr1);
                                        $em->persist($provider);
                                    }
                                }
                            }
                        }
                    }
                    $em->flush();
                }

                $em->flush();
            }

            //upload files
            $uploadError = array();
            if ($form_step == 5) {

                $credentialing_application_file = $request->files->get('credentialing_application_file');
                $state_licence_file = $request->files->get('state_licence_file');
                $provider_data_form_file = $request->files->get('provider_data_form_file');
                $w9_file = $request->files->get('w9_file');
                $ownership_disclosure_form_file = $request->files->get('ownership_disclosure_form_file');
                $roster_file = $request->files->get('roster_file');
                $site_visit_survey_file = $request->files->get('site_visit_survey_file');
                $accreditation_certificate_file = $request->files->get('accreditation_certificate_file');
                $loa_loi_file = $request->files->get('loa_loi_file');

                if ($credentialing_application_file != "") {
                    $file = $fileUploader->upload($credentialing_application_file, $save_status, "credentialing_application_file");
                    if ($file != "error") {
                        $billinAddress->setCredentialingApplicationFile($file);
                        $uploadError[0] = 0;
                    } else {
                        $uploadError[0] = 1;
                    }
                }
                if ($state_licence_file != "") {
                    $file = $fileUploader->upload($state_licence_file, $save_status, "state_licence_file");
                    if ($file != "error") {
                        $billinAddress->setStateLicenseFile($file);
                        $uploadError[1] = 0;
                    } else {
                        $uploadError[1] = 1;
                    }
                }
                if ($provider_data_form_file != "") {
                    $file = $fileUploader->upload($provider_data_form_file, $save_status, "provider_data_form_file");
                    if ($file != "error") {
                        $billinAddress->setProviderDataFormFile($file);
                        $uploadError[2] = 0;
                    } else {
                        $uploadError[2] = 1;
                    }
                }
                if ($w9_file != "") {
                    $file = $fileUploader->upload($w9_file, $save_status, "w9_file");
                    if ($file != "error") {
                        $billinAddress->setW9File($file);
                        $uploadError[3] = 0;
                    } else {
                        $uploadError[3] = 1;
                    }
                }
                if ($ownership_disclosure_form_file != "") {
                    $file = $fileUploader->upload($ownership_disclosure_form_file, $save_status, "ownership_disclosure_form_file");
                    if ($file != "error") {
                        $billinAddress->setOwnershipDisclosureFormFile($file);
                        $uploadError[4] = 0;
                    } else {
                        $uploadError[4] = 1;
                    }
                }
                if ($roster_file != "") {
                    $file = $fileUploader->upload($roster_file, $save_status, "roster_file");
                    if ($file != "error") {
                        $billinAddress->setRosterFile($file);
                        $uploadError[5] = 0;
                    } else {
                        $uploadError[5] = 1;
                    }
                }
                if ($site_visit_survey_file != "") {
                    $file = $fileUploader->upload($site_visit_survey_file, $save_status, "site_visit_survey_file");
                    if ($file != "error") {
                        $billinAddress->setSiteVisitSurveyFile($file);
                        $uploadError[6] = 0;
                    } else {
                        $uploadError[6] = 1;
                    }
                }
                if ($accreditation_certificate_file != "") {
                    $file = $fileUploader->upload($accreditation_certificate_file, $save_status, "accreditation_certificate_file");
                    if ($file != "error") {
                        $billinAddress->setAccreditationCertificateFile($file);
                        $uploadError[7] = 0;
                    } else {
                        $uploadError[7] = 1;
                    }
                }
                if ($loa_loi_file != "") {
                    $file = $fileUploader->upload($loa_loi_file, $save_status, "loa_loi_file");
                    if ($file != "error") {
                        $billinAddress->setLoaLoiFile($file);
                        $uploadError[8] = 0;
                    } else {
                        $uploadError[8] = 1;
                    }
                }

                $em->persist($billinAddress);
                $em->flush();

            }

            if ($form_step == 6) {
                $providerSelected = $request->get('providerSelected');

                $sqlDelete = "DELETE FROM `provider_billing_address` WHERE `provider_billing_address`.`billing_address_id` =  $save_status";
                $stmt = $db->prepare($sqlDelete);
                $stmt->execute();

                $providers = substr($providerSelected, 0, -1);
                $lbs = explode(",", $providers);

                if ($lbs != null) {
                    foreach ($lbs as $provider) {
                        $providerObj = $em->getRepository('App\Entity\Provider')->find($provider);
                        if ($providerObj != null) {
                            $providerObj->addBillingAddress($billinAddress);
                        }
                        $em->persist($providerObj);
                        $em->flush();
                    }
                }

            }

            return new Response(
                json_encode(array('id' => $billinAddress->getId())), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }

    }

    /**
     * @Route("/update", name="admin_update_billing")
     */
    public function update(Request $request, BillingAddressFileUploader $fileUploader){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $db = $em->getConnection();

            $save_status = $request->get('save_status');
            $form_step = $request->get('form_step');


            if ($save_status == 0) {
                $billinAddress = new BillingAddress();
            } else {
                $billinAddress = $em->getRepository('App\Entity\BillingAddress')->find($save_status);
            }

            if ($form_step == 1) {
                $business_hours = $request->get('business_hours_data');
                $street = $request->get('street');
                $suite_number = $request->get('suite_number');
                $city = $request->get('city');
                $region = $request->get('region');
                $state = $request->get('state');
                $zip_code = $request->get('zip_code');
                $country = $request->get('country');
                $latitude = $request->get('latitude');
                $longitude = $request->get('longitude');
                $phone = $request->get('phone');
                $org = $request->get('org');
                $organization = $em->getRepository('App\Entity\Organization')->find($org);

                $billinAddress->setBusinessHours($business_hours);
                $billinAddress->setStreet($street);
                $billinAddress->setSuiteNumber($suite_number);
                $billinAddress->setCity($city);
                $billinAddress->setRegion($region);
                $billinAddress->setUsState($state);
                $billinAddress->setZipCode($zip_code);
                $billinAddress->setCountry($country);
                if ($latitude != "")
                    $billinAddress->setLat($latitude);
                if ($longitude != "")
                    $billinAddress->setLng($longitude);
                $billinAddress->setPhoneNumber($phone);
                $billinAddress->setOrganization($organization);

                $em->persist($billinAddress);
                $em->flush();
            }
            if ($form_step == 2) {

                $serviceSelected = $request->get('serviceSelected');

                $sqlDelete = "DELETE FROM `billing_address_service_settings` WHERE `billing_address_service_settings`.`billing_address_id` =  $save_status";
                $stmt = $db->prepare($sqlDelete);
                $stmt->execute();

                $services = substr($serviceSelected, 0, -1);
                $lbs = explode(",", $services);

                if ($lbs != null) {
                    foreach ($lbs as $service) {
                        $serviceObj = $em->getRepository('App\Entity\ServiceSettings')->find($service);

                        if ($serviceObj != null) {
                            $billinAddress->addServiceSetting($serviceObj);
                        }
                    }

                    $em->persist($billinAddress);
                    $em->flush();
                }
            }
            if ($form_step == 3) {
                $number_of_beds = $request->get('number_of_beds');
                $location_npi = $request->get('location_npi');
                $location_npi2 = $request->get('location_npi2');
                $group_medicaid = $request->get('group_medicaid');
                $group_medicare = $request->get('group_medicare');
                $limit_age_min = $request->get('limit_age_min');
                $limit_age_max = $request->get('limit_age_max');
                $taxonomies=$request->get('taxonomies');

                $billinAddress->setBeds($number_of_beds);
                $billinAddress->setLocationNpi($location_npi);
                $billinAddress->setLocationNpi2($location_npi2);
                $billinAddress->setGroupMedicaid($group_medicaid);
                $billinAddress->setGroupMedicare($group_medicare);

                if ($limit_age_min != "")
                    $billinAddress->setOfficeAgeLimit($limit_age_min);
                if ($limit_age_max != "")
                    $billinAddress->setOfficeAgeLimitMax($limit_age_max);

                $actualTaxonomies=$billinAddress->getTaxonomyCodes();

                if($actualTaxonomies!=null){
                    foreach ($actualTaxonomies as $at){
                        $billinAddress->removeTaxonomyCode($at);
                        $em->persist($billinAddress);
                        $em->flush();
                    }
                }

                if($taxonomies!=""){
                    foreach ($taxonomies as $taxonomy){
                        $taxonomyObj=$em->getRepository('App\Entity\TaxonomyCode')->find($taxonomy);
                        if($taxonomyObj!=null){
                            $billinAddress->addTaxonomyCode($taxonomyObj);
                        }
                    }
                }

                $em->persist($billinAddress);
                $em->flush();
            }

            if ($form_step == 4) {
                $wheelchair_accessibility = $request->get('wheelchair_accessibility');
                $tdd = $request->get('tdd');
                $private_provider_transportation = $request->get('private_provider_transportation');
                $is_primary_address = $request->get('is_primary_address');
                $is_see_patients_address=$request->get('is_see_patients_address');
                $is_billing_address = $request->get('is_billing_address');
                $is_mailing_address = $request->get('is_mailing_address');
                $is_facility = $request->get('is_facility');
                $facilityTypesSelected = $request->get('facilityTypesSelected');
                $facility_number = $request->get('facility_number');
                $facility_exemption_number = $request->get('facility_exemption_number');
                $support_ehr=$request->get('support_ehr');

                $billinAddress->setWheelchair($wheelchair_accessibility);
                $billinAddress->setTdd($tdd);
                $billinAddress->setSupportEHR($support_ehr);
                $billinAddress->setPrivateProviderTransportation($private_provider_transportation);
                $billinAddress->setPrimaryAddr($is_primary_address);
                $billinAddress->setSeePatientsAddr($is_see_patients_address);
                $billinAddress->setBillingAddr($is_billing_address);
                $billinAddress->setMailingAddr($is_mailing_address);
                $billinAddress->setIsFacility($is_facility);

                if ($facilityTypesSelected != "") {
                    $sqlDelete = "DELETE FROM `billing_address_facility_type` WHERE `billing_address_facility_type`.`billing_address_id` = $save_status";
                    $stmt = $db->prepare($sqlDelete);
                    $stmt->execute();

                    $facilities = substr($facilityTypesSelected, 0, -1);
                    $lbs = explode(",", $facilities);

                    foreach ($lbs as $facility) {
                        if ($facility != null) {
                            $facilityObj = $em->getRepository('App\Entity\FacilityType')->find($facility);
                            if ($facilityObj != null) {
                                $billinAddress->addFacilityType($facilityObj);
                            }
                        }
                    }
                }
                if ($facility_number == "") {
                    $billinAddress->setFacilityNumber(null);
                } else {
                    $billinAddress->setFacilityNumber(intval($facility_number));
                }

                if ($facility_exemption_number == "") {
                    $billinAddress->setFacilityExemptionNumber($facility_exemption_number);
                } else {
                    $billinAddress->setFacilityExemptionNumber($facility_exemption_number);
                }


                $em->persist($billinAddress);
                $em->flush();
            }

            //upload files
            $uploadError = array();
            if ($form_step == 5) {

                $credentialing_application_file = $request->files->get('credentialing_application_file');
                $state_licence_file = $request->files->get('state_licence_file');
                $provider_data_form_file = $request->files->get('provider_data_form_file');
                $w9_file = $request->files->get('w9_file');
                $ownership_disclosure_form_file = $request->files->get('ownership_disclosure_form_file');
                $roster_file = $request->files->get('roster_file');
                $site_visit_survey_file = $request->files->get('site_visit_survey_file');
                $accreditation_certificate_file = $request->files->get('accreditation_certificate_file');
                $loa_loi_file = $request->files->get('loa_loi_file');

                if ($credentialing_application_file != "") {
                    $file = $fileUploader->upload($credentialing_application_file, $save_status, "credentialing_application_file");
                    if ($file != "error") {
                        $billinAddress->setCredentialingApplicationFile($file);
                        $uploadError[0] = 0;
                    } else {
                        $uploadError[0] = 1;
                    }
                }
                if ($state_licence_file != "") {
                    $file = $fileUploader->upload($state_licence_file, $save_status, "state_licence_file");
                    if ($file != "error") {
                        $billinAddress->setStateLicenseFile($file);
                        $uploadError[1] = 0;
                    } else {
                        $uploadError[1] = 1;
                    }
                }
                if ($provider_data_form_file != "") {
                    $file = $fileUploader->upload($provider_data_form_file, $save_status, "provider_data_form_file");
                    if ($file != "error") {
                        $billinAddress->setProviderDataFormFile($file);
                        $uploadError[2] = 0;
                    } else {
                        $uploadError[2] = 1;
                    }
                }
                if ($w9_file != "") {
                    $file = $fileUploader->upload($w9_file, $save_status, "w9_file");
                    if ($file != "error") {
                        $billinAddress->setW9File($file);
                        $uploadError[3] = 0;
                    } else {
                        $uploadError[3] = 1;
                    }
                }
                if ($ownership_disclosure_form_file != "") {
                    $file = $fileUploader->upload($ownership_disclosure_form_file, $save_status, "ownership_disclosure_form_file");
                    if ($file != "error") {
                        $billinAddress->setOwnershipDisclosureFormFile($file);
                        $uploadError[4] = 0;
                    } else {
                        $uploadError[4] = 1;
                    }
                }
                if ($roster_file != "") {
                    $file = $fileUploader->upload($roster_file, $save_status, "roster_file");
                    if ($file != "error") {
                        $billinAddress->setRosterFile($file);
                        $uploadError[5] = 0;
                    } else {
                        $uploadError[5] = 1;
                    }
                }
                if ($site_visit_survey_file != "") {
                    $file = $fileUploader->upload($site_visit_survey_file, $save_status, "site_visit_survey_file");
                    if ($file != "error") {
                        $billinAddress->setSiteVisitSurveyFile($file);
                        $uploadError[6] = 0;
                    } else {
                        $uploadError[6] = 1;
                    }
                }
                if ($accreditation_certificate_file != "") {
                    $file = $fileUploader->upload($accreditation_certificate_file, $save_status, "accreditation_certificate_file");
                    if ($file != "error") {
                        $billinAddress->setAccreditationCertificateFile($file);
                        $uploadError[7] = 0;
                    } else {
                        $uploadError[7] = 1;
                    }
                }
                if ($loa_loi_file != "") {
                    $file = $fileUploader->upload($loa_loi_file, $save_status, "loa_loi_file");
                    if ($file != "error") {
                        $billinAddress->setLoaLoiFile($file);
                        $uploadError[8] = 0;
                    } else {
                        $uploadError[8] = 1;
                    }
                }

                $em->persist($billinAddress);
                $em->flush();

            }

            if ($form_step == 6) {
                $providerSelected = $request->get('providerSelected');

                $sqlDelete = "DELETE FROM `provider_billing_address` WHERE `provider_billing_address`.`billing_address_id` =  $save_status";
                $stmt = $db->prepare($sqlDelete);
                $stmt->execute();

                $providers = substr($providerSelected, 0, -1);
                $lbs = explode(",", $providers);

                if ($lbs != null) {
                    foreach ($lbs as $provider) {
                        $providerObj = $em->getRepository('App\Entity\Provider')->find($provider);
                        if ($providerObj != null) {
                            $providerObj->addBillingAddress($billinAddress);
                        }
                        $em->persist($providerObj);
                        $em->flush();
                    }
                }

            }


            return new Response(
                json_encode(array('id' => $billinAddress->getId())), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_billing",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $billing = $em->getRepository('App\Entity\BillingAddress')->find($id);
            $removed = 0;
            $message = "";

            if ($billing) {
                try {
                    $em->remove($billing);
                    $em->flush();
                    $removed = 1;
                    $message = "The Billing Address has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Billing Address can't be removed";
                }
            }

            $this->SaveLog(3,'Address',$billing->getId(),'Delete Address','');

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_billing",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $billing = $billing = $em->getRepository('App\Entity\BillingAddress')->find($id);

                if ($billing) {
                    try {
                        $em->remove($billing);
                        $em->flush();
                        $removed = 1;
                        $message = "The Billing Address has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Billing Address can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/ag-report", name="admin_billing_agreport")
     */
    public function aggrid(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $sql="SELECT b.id, organization.name as organization, organization.id as org_id, b.street,b.zip_code,b.city,county_match.name as county,b.suite_number,b.region,b.us_state,
            b.facility_number,b.facility_exemption_number,b.location_npi,b.group_medicaid,b.group_medicare,b.office_age_limit,b.office_age_limit_max,b.lat,b.lng,
            IF(SUM(IF(b.is_facility, 1, 0)) > 0, 'Yes', 'No') AS `is_facility`,
            IF(SUM(IF(b.wheelchair, 1, 0)) > 0, 'Yes', 'No') AS `wheelchair`,
            IF(SUM(IF(b.tdd, 1, 0)) > 0, 'Yes', 'No') AS `tdd`,
            IF(SUM(IF(b.primary_addr, 1, 0)) > 0, 'Yes', 'No') AS `primary_addr`,
            IF(SUM(IF(b.mailing_addr, 1, 0)) > 0, 'Yes', 'No') AS `mailing_addr`,
            IF(SUM(IF(b.billing_addr, 1, 0)) > 0, 'Yes', 'No') AS `billing_addr`,
            IF(SUM(IF(b.disabled, 1, 0)) > 0, 'Yes', 'No') AS `disabled`,
            IF(SUM(IF(b.private_provider_transportation, 1, 0)) > 0, 'Yes', 'No') AS `private_provider_transportation`,
            GROUP_CONCAT(DISTINCT service_settings.name ORDER BY service_settings.id ASC SEPARATOR ', ') AS service_settings,
            GROUP_CONCAT(DISTINCT taxonomy_code.code ORDER BY taxonomy_code.id ASC SEPARATOR ', ') AS taxonomies
            FROM  billing_address b 
            LEFT JOIN organization ON b.organization_id = organization.id
            LEFT JOIN county_match ON b.county_match_id = county_match.id
            LEFT JOIN billing_address_service_settings ON b.id = billing_address_service_settings.billing_address_id
            LEFT JOIN service_settings ON service_settings.id = billing_address_service_settings.service_settings_id
            LEFT JOIN billing_address_taxonomy_code ON b.id = billing_address_taxonomy_code.billing_id
            LEFT JOIN taxonomy_code ON taxonomy_code.id = billing_address_taxonomy_code.taxonomy_id
            GROUP BY b.id
            ORDER BY b.id";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $billings= $stmt->fetchAll();

            return $this->render('billing/aggrid.html.twig', array('billings' => $billings));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/get_addr_ajax", name="admin_get_billing_ajax",methods={"POST"})
     */
    public function getAddrAjax(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $id=$request->get('id');
        $payer=$request->get('payer_id');

        $address=$em->getRepository('App\Entity\BillingAddress')->find($id);

        $addressResult=array();
        $addressResult['id']=$address->getId();
        $addressResult['bussinees']=$address->getBusinessHours();
        $addressResult['organization']=$address->getOrganization()->getName();
        $addressResult['street']=$address->getStreet();
        $addressResult['suite_number']=$address->getSuiteNumber();
        $addressResult['city']=$address->getCity();
        $addressResult['county']=$address->getCounty();
        $addressResult['state']=$address->getUsState();
        $addressResult['region']=$address->getRegion();
        $addressResult['zip_code']=$address->getZipCode();
        $addressResult['country']=$address->getCounty();
        $addressResult['phone_number']=$address->getPhoneNumber();
        $addressResult['beds']=$address->getBeds();
        $addressResult['npi']=$address->getLocationNpi();
        $addressResult['npi2']=$address->getLocationNpi2();
        $addressResult['medicaid']=$address->getGroupMedicaid();
        $addressResult['medicare']=$address->getGroupMedicare();
        $addressResult['taxonomy']=$address->getTaxonomyCode();
        $addressResult['age_limit_min']=$address->getOfficeAgeLimit();
        $addressResult['age_limit_max']=$address->getOfficeAgeLimitMax();
        $addressResult['whellchair']=$address->getWheelchair();
        $addressResult['tdd']=$address->getId();
        $addressResult['private_transportation']=$address->getId();
        $addressResult['primary_address']=$address->getId();
        $addressResult['billing_address']=$address->getBillingAddr();
        $addressResult['mailing_address']=$address->getMailingAddr();
        $addressResult['is_facility']=$address->getIsFacility();
        $addressResult['facility_number']=$address->getFacilityNumber();
        $addressResult['facility_expedition_number']=$address->getFacilityExemptionNumber();

        $servicesettingsResult=array();
        $ssResult=array();
        $servicesettings=$address->getServiceSettings();
        $totalServiceSetting=0;

        if(count($servicesettings)>0){
            $totalServiceSetting=1;
            foreach ($servicesettings as $servicesetting){
                $servicesettingsResult['id']=$servicesetting->getId();
                $servicesettingsResult['name']=$servicesetting->getName();
                $ssResult[]=$servicesettingsResult;
            }
        }

        $facilityTypeResult=array();
        $fType=array();

        $facilityTypes=$address->getFacilityType();
        $totalFacilityTypes=0;

        if(count($facilityTypes)>0){
            $totalFacilityTypes=1;
            foreach ($facilityTypes as $facitity){
                $fType['id']=$facitity->getId();
                $fType['name']=$facitity->getName();
                $facilityTypeResult[]=$fType;
            }
        }

        //get providers aigned to this payer on this address
        $sql="SELECT p.id,p.first_name, p.last_name, p.degrees,  p.provider_types, p.phone_number, p.specialties, p.languages_str, p.npi_number
            FROM  v_provider_payer p 
            LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
            LEFT JOIN payer ON payer.id = provider_payer.payer_id
            LEFT JOIN provider_billing_address ON provider_billing_address.provider_id=p.id
            LEFT JOIN billing_address on billing_address.id=provider_billing_address.billing_address_id
            LEFT JOIN organization on billing_address.organization_id=organization.id
            WHERE p.disabled=0 and payer.id=".$payer." and billing_address.id=".$id."
            GROUP BY p.id
            ORDER BY p.id";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $providerResult= $stmt->fetchAll();

        $organization=$address->getOrganization();
        $orgResult=array();
        $specialtyAreasR=array();
        $specialtyArea=array();

        $orgClassification=[];
        $orgClassificationR=[];

        if($organization){
            $orgResult['name']=$organization->getName();
            $orgResult['type']=$organization->getProviderType();
            $orgResult['billing_phone']=$organization->getBillingPhone();
            $orgResult['phone']=$organization->getPhone();
            $orgResult['faxno']=$organization->getFaxno();
            $orgResult['website']=$organization->getWebsite();

            $specialtyAreas=$organization->getSpecialtyAreas();
            if($specialtyAreas){
                foreach ($specialtyAreas as $sa){
                    $specialtyArea['name']=$sa->getName();
                    $specialtyAreasR[]=$specialtyArea;
                }
            }
            $orgResult['specialtyareas']=$specialtyAreasR;

            $orgClassifications=$organization->getOrgSpecialtiesArray();
            if($orgClassifications){
                foreach ($orgClassifications as $orgC){
                    $orgClassification['name']=$orgC->getName();
                    $orgClassificationR[]=$orgClassification;
                }
            }

            $orgResult['orgClassifications']=$orgClassificationR;

        }

        return new Response(
            json_encode(array('test' => 1,'address'=>$addressResult,'ssResult'=>$ssResult,'totalServiceSetting'=>$totalServiceSetting,'organization'=>$orgResult,
                'facilityTipes'=>$facilityTypeResult,'totalFacilityTypes'=>$totalFacilityTypes,'providers'=>$providerResult)), 200, array('Content-Type' => 'application/json')
        );
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }

    /**
     * @Route("/set-taxonomy-code", name="admin_billing_set_taxonomy")
    */
    public function updateTaxonomy(){
        $em = $this->getDoctrine()->getManager();

        $addresses=$em->getRepository('App\Entity\BillingAddress')->findAll();

        foreach ($addresses as $address){
            $taxonomy=$address->getTaxonomyCode();
            if($taxonomy!=null and $taxonomy!="" and $taxonomy!="-" and $taxonomy!="N/A" and $taxonomy!="n/a"){
                $taxonomyObj=$em->getRepository('App\Entity\TaxonomyCode')->findOneBy(array('code'=>$taxonomy));
                if($taxonomyObj!=null){
                    $address->addTaxonomyCode($taxonomyObj);
                    $em->persist($address);
                    $em->flush();
                }

            }
        }

        return new Response('Process Finish');
    }

    /**
     * @Route("/getLocationsWithMedicaid", name="admin_billing_withMedicaid")
     */
    public function addressWithMedicaid(){
        $em = $this->getDoctrine()->getManager();
        $address=$em->getRepository('App\Entity\BillingAddress')->findAll();

        $cont=1;
        foreach ($address as $addr){
            //$addr=new BillingAddress();
            $group_medicaid=$addr->getGroupMedicaid();
            if($group_medicaid!=null){
                echo $addr->getId()."<br/>";
            }else{
                $providers=$addr->getProviders();
                $contMedicaid=0;
                if($providers!=null){
                    foreach ($providers as $provider){
                        if($provider!=null){
                            $medicaid=$provider->getMedicaid();
                            if($medicaid!="" and $medicaid!="-" and $medicaid!="In process" and $medicaid!="LCSW" and $medicaid!="N/A" and $medicaid!="n/a" and $medicaid!="blank" and $medicaid!="00PENDING" and $medicaid!="00pending"){
                                $contMedicaid++;
                            }

                        }
                    }

                    if($contMedicaid>0){
                         echo $addr->getId()."<br/>";

                    }
                }
            }
        }

        die();
    }

    /**
     * @Route("/disabled", name="admin_disabled_billing",methods={"POST"})
     */
    public function disableAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');
            $action=$request->get('action');

            $billing = $billing = $em->getRepository('App\Entity\BillingAddress')->find($id);
            $disabled = 0;

            if ($billing) {

                if($action==0){
                    $billing->setDisabled(true);
                    $disabled = 1;
                }
                if($action==1){
                    $billing->setDisabled(false);
                }

                $em->persist($billing);
                $em->flush();
            }

            return new Response(
                json_encode(array('disabled' => $disabled)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/check-unique-status", name="admin_check_unique_status",methods={"POST"})
     */
    public function check_unique_billing(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');
            $org_id=$request->get('org_id');
            $status=$request->get('status');

            $unique=true;
            $cont=0;
            $billingAddress=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$org_id));
            if($billingAddress!=null){
               foreach ($billingAddress as $addr){
                   if($addr->getId()!=$id){

                       if($addr->getBillingAddr()==true and $status=='billing'){
                           $cont++;
                       }

                       if($addr->getPrimaryAddr()==true and $status=='primary'){
                           $cont++;
                       }

                       if($addr->getMailingAddr()==true and $status=='mailling'){
                           $cont++;
                       }
                   }
               }
            }

            if($cont>0){
                $unique=false;
            }

            return new Response(
                json_encode(array('unique' => $unique)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/set-unique-status", name="admin_set_unique_status",methods={"POST"})
     */
    public function set_unique_status(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');
            $org_id=$request->get('org_id');
            $status=$request->get('status');


            $billingAddress=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$org_id));
            if($billingAddress!=null){
                foreach ($billingAddress as $addr){
                    if($status=='billing') {
                         $addr->setBillingAddr(false);
                         $em->persist($addr);
                    }
                    if($status=='primary') {
                        $addr->setPrimaryAddr(false);
                        $em->persist($addr);
                    }
                    if($status=='mailling') {
                        $addr->setMailingAddr(false);
                        $em->persist($addr);
                    }
                }
                $em->flush();
            }

            $address=$em->getRepository('App\Entity\BillingAddress')->find($id);

            if($address!=null){
                if($status=='billing') {
                    $address->setBillingAddr(true);
                    $em->persist($address);
                }
                if($status=='primary') {
                    $address->setPrimaryAddr(true);
                    $em->persist($address);
                }
                if($status=='mailling') {
                    $address->setMailingAddr(true);
                    $em->persist($address);
                }
                $em->flush();
            }

            return new Response(
                json_encode(array('ok' => 1)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function SaveLog($action_id,$entity,$entity_id,$note,$source){
        $em=$this->getDoctrine()->getManager();
        $user=$this->getUser();

        $log=new ChangeLog();
        $log->setUser($user);
        $action=$em->getRepository('App\Entity\ActionLog')->find($action_id);
        $log->setAction($action);
        $log->setEntityId($entity_id);
        $log->setEntityClass($entity);
        $log->setSource($source);
        $log->setNote($note);

        $em->persist($log);
        $em->flush();
    }

    /**
     * @Route("/set-phone-number", name="admin_set_addr_phonenumber")
     */
    public function set_phone_number() {
        $em=$this->getDoctrine()->getManager();
        $address=$em->getRepository('App\Entity\BillingAddress')->findAll();

        foreach ($address as $addr){
            if($addr->getPhoneNumber()==""){
                $organization=$addr->getOrganization();
                $newPhone=$organization->getPhone();
                $addr->setPhoneNumber($newPhone);
                $em->persist($addr);
            }
        }

        $em->flush();
        echo "All done!";
        die();
    }

    /**
     * @Route("/set-max-age", name="admin_set_addr_pmaxage")
     */
    public function set_max_age() {
        $em=$this->getDoctrine()->getManager();
        $address=$em->getRepository('App\Entity\BillingAddress')->findAll();

        foreach ($address as $addr){
            if($addr->getOfficeAgeLimitMax()=="" or $addr->getOfficeAgeLimitMax()==0){
                $addr->setOfficeAgeLimitMax(99);
            }

            if($addr->getOfficeAgeLimit()==""){
               $addr->setOfficeAgeLimit(0);
            }

            $maxAge=intval($addr->getOfficeAgeLimitMax());

            if($maxAge>99){
                $addr->setOfficeAgeLimitMax(99);
            }
            $em->persist($addr);
        }

        $em->flush();
        echo "All done!";
        die();
    }

    /**
     * @Route("/set-see-patiens", name="admin_set_see_patiens")
     */
    public function set_see_patiens() {
        $em=$this->getDoctrine()->getManager();
        $address=$em->getRepository('App\Entity\BillingAddress')->findAll();

        foreach ($address as $addr){
            $addr->setSeePatientsAddr(1);
            $em->persist($addr);
        }
        $em->flush();

        foreach ($address as $addr){
            if($addr->getBusinessHours()==""){
              $addr->setSeePatientsAddr(0);
              $em->persist($addr);
            }else{
                $street_check=$addr->getStreet();
                if(strpos($street_check,'BOX')==true or strpos($street_check,'Box')==true or strpos($street_check,'box')==true){
                    $addr->setSeePatientsAddr(0);
                    $em->persist($addr);
                }
            }
        }

        $em->flush();
        echo "All done!";
        die();
    }


    /**
     * @Route("/load-addrs", name="vericare_adds", methods={"GET"})
     */
    public function load(){
        set_time_limit(7200);
        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "template_xls/vericare_locations.xlsx";

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=1;

        $organization=$em->getRepository('App\Entity\Organization')->find(599);

        foreach ($sheetData as $sheet) {
            if($cont>1){
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    $locationName=$sheet['A'];
                    $street=$sheet['B'];
                    $city=$sheet['C'];
                    $county=$sheet['D'];

                    $countyM=$em->getRepository('App\Entity\CountyMatch')->findOneBy(array('name'=>$county));
                    $region="";
                    if($county=="Escambia" or $county=="Okaloosa" or $county=="Santa Rosa" or $county=="Walton"){
                        $region=1;
                    }
                    if($county=="Bay" or $county=="Calhoun" or $county=="Franklin" or $county=="Gadsden" or $county=="Gulf" or $county=="Holmes" or $county=="Jackson" or $county=="Jefferson"
                        or $county=="Leon" or $county=="Liberty" or $county=="Madison" or $county=="Taylor" or $county=="Wakulla" or $county=="Washington"){
                        $region=2;
                    }

                    if($county=="Alachua" or $county=="Bradford" or $county=="Citrus" or $county=="Columbia" or $county=="Dixie" or $county=="Gilchrist" or $county=="Hamilton"
                        or $county=="Hernando" or $county=="Lafayette" or $county=="Lake" or $county=="Levy" or $county=="Marion" or $county=="Putnam"
                        or $county=="Sumter" or $county=="Suwannee" or $county=="Union"){
                        $region=3;
                    }

                    if($county=="Baker" or $county=="Clay" or $county=="Duval" or $county=="Flagler" or $county=="Nassau" or $county=="St. Johns" or $county=="Volusia"){
                        $region=4;
                    }

                    if($county=="Pasco" or $county=="Pinellas"){
                        $region=5;
                    }

                    if($county=="Hardee" or $county=="Highlands" or $county=="Hillsborough" or $county=="Manatee" or $county=="Polk"){
                        $region=6;
                    }

                    if($county=="Brevard" or $county=="Orange" or $county=="Osceola" or $county=="Seminole"){
                        $region=7;
                    }

                    if($county=="Charlotte" or $county=="Collier" or $county=="DeSoto" or $county=="Glades" or $county=="Hendry" or $county=="Lee" or $county=="Sarasota"){
                        $region=8;
                    }

                    if($county=="Indian River" or $county=="Martin" or $county=="Okeechobee" or $county=="Palm Beach" or $county=="St. Lucie"){
                        $region=9;
                    }

                    if($county=="Broward"){
                        $region=10;
                    }

                    if($county=="Miami-Dade" or $county=="Monroe"){
                        $region=11;
                    }

                    $addr=new BillingAddress();
                    if($countyM){
                        $addr->setCountyMatch($countyM);
                    }
                    $addr->setStreet($street);
                    $addr->setLocationName($locationName);
                    $addr->setCity($city);
                    $addr->setCounty($county);
                    $addr->setOrganization($organization);
                    $addr->setUsState('FL');
                    $addr->setRegion($region);

                    $em->persist($addr);
                }
            }

            $cont++;
        }
        $em->flush();
        die();
    }

    /**
     * @Route("/process-set-age-ranges", name="process_set_age_ranges")
     */
    public function processSetAgeranges(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();

        $address=$em->getRepository('App:BillingAddress')->findAll();
        $ageRanges=$em->getRepository('App:AgesRange')->findAll();

        foreach ($address as $addr){
            $min=intval($addr->getOfficeAgeLimit());
            $max=intval($addr->getOfficeAgeLimitMax());

            if($max==0){
                $max=99;
            }

            foreach ($ageRanges as $ageRange){
                $age_min=intval($ageRange->getMinAge());
                $age_max=intval($ageRange->getMaxAge());

                if($min<=$age_min  and $max>=$age_max){
                    $addr->addAgeRange($ageRange);
                }else{
                    if($min>$age_min and $min<=$age_max and $max>=$age_max){
                        $addr->addAgeRange($ageRange);
                    }
                }
                $em->persist($addr);
            }
        }
        $em->flush();

        die();
    }



}
