<?php

namespace App\Controller;

use App\Entity\ContactP;
use App\Entity\ContactPBestChoice;
use App\Entity\FacilityApp;
use App\Entity\OrganizationP;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\ProviderP;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Utils\My_Mcript;

/**
 * @Route("/provider")
 */
class ProviderFormDataController extends AbstractController
{

    /**
     * @Route("/dashboard", name="provider_dashboard")
     *
     */
    public function home()
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            //get user conected
            $userId = $this->getUser()->getId();
            $organizationObj = $em->getRepository('App\Entity\OrganizationP')->findOneBy(array('provider_user' => $userId));

            $org_completed_profile = 0;
            $app_facility_completed = 0;

            if ($organizationObj) {
                $org_completed_profile = $this->PercentOrganizationComplete($organizationObj);
            }

            $facilityAppObj = $em->getRepository('App\Entity\FacilityApp')->findOneBy(array('provider_user' => $userId));

            if ($facilityAppObj) {
                $app_facility_completed = $this->PercentFacilityAppComplete($facilityAppObj);
            }

            $idOrg = 0;
            if ($organizationObj != null) {
                $idOrg = $organizationObj->getId();
            }

            $contacts = $em->getRepository('App\Entity\ContactP')->findBy(array('user' => $userId));
            $address = $em->getRepository('App\Entity\BillingAddressP')->findBy(array('user' => $userId));
            $providers = $em->getRepository('App\Entity\ProviderP')->findBy(array('user' => $userId));

            //get a facility address for this user
            $addressT = $em->getRepository('App\Entity\BillingAddressP')->findBy(array('user' => $userId, 'is_facility' => 1));

            $cont_address = count($addressT);

            return $this->render('provider_p/index.html.twig', array('org_completed_profile' => $org_completed_profile,
                'contacts' => $contacts, 'cont_address' => $cont_address, 'providers' => $providers, 'address' => $address, 'idOrg' => $idOrg, 'app_facility_completed' => $app_facility_completed, 'organization' => $organizationObj));
        } else {
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/download", name="provider_download")
     *
     */
    public function download()
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->render('provider_p/download.html.twig');
        } else {
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/org_setdata", name="provider_organization_data")
     *
     */
    public function add()
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $organization_types = $em->getRepository('App\Entity\OrgSpecialty')->findBy(array(), array('name' => 'ASC'));
            $us_states = $em->getRepository('App\Entity\UsState')->findBy(array(), array('name' => 'ASC'));
            $specialties = $em->getRepository('App\Entity\SpecialtyAreas')->findBy(array(), array('name' => 'ASC'));
            $accreditations = $em->getRepository('App\Entity\Accreditation')->findAll();

            $providerUser = $this->getUser()->getId();
            $organizationObj = $em->getRepository('App\Entity\OrganizationP')->findOneBy(array('provider_user' => $providerUser));

            $totalOrgClasification = count($organization_types);
            $first_column_elements = ($totalOrgClasification / 3);
            if (is_float($first_column_elements)) {
                $first_column_elements = intval($first_column_elements + 1);
            }
            $rest_columns_element = intval(($totalOrgClasification - $first_column_elements) / 2);

            //columns count for specialty Areas
            $totalSpecialties = count($specialties);
            $sa_first_column_elements = ($totalSpecialties / 3);
            if (is_float($sa_first_column_elements)) {
                $sa_first_column_elements = intval($sa_first_column_elements + 1);
            }
            $sa_rest_columns_element = intval(($totalSpecialties - $sa_first_column_elements) / 2);


            $accreditationsSelected = null;
            $clasificationsSelected = null;
            $specialiesSelected = null;

            if ($organizationObj) {
                $accreditationsSelected = $organizationObj->getAccreditations();
                $clasificationsSelected = $organizationObj->getOrgSpecialties();
                $specialiesSelected = $organizationObj->getSpecialtyAreas();
            }


            return $this->render('provider_p/add.html.twig', array('clasifications' => $organization_types, 'us_states' => $us_states,
                'accreditations' => $accreditations, 'specialties' => $specialties, 'organization' => $organizationObj, 'accreditationsSelected' => $accreditationsSelected,
                'clasificationsSelected' => $clasificationsSelected, 'specialiesSelected' => $specialiesSelected, 'first_column_elements' => $first_column_elements,
                'rest_columns_element' => $rest_columns_element, 'totalOrgClasification' => $totalOrgClasification, 'totalSpecialties' => $totalSpecialties,
                'sa_first_column_elements' => $sa_first_column_elements, 'sa_rest_columns_element' => $sa_rest_columns_element));
        } else {
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/providers_roster",name="provider_list")
     *
     */
    public function listProvider()
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $userId = $this->getUser()->getId();

            $providers = $em->getRepository('App\Entity\ProviderP')->findBy(array('user' => $userId));
            $delete_form_ajax = $this->createCustomForm('PROVIDER_ID', 'DELETE', 'provider_delete_provider');

            return $this->render('provider_p/providers_list.html.twig', array('providers' => $providers, 'delete_form_ajax' => $delete_form_ajax->createView()));
        } else {
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/providers_address",name="provider_list_address")
     *
     */
    public function listAddress()
    {

    }

    /**
     * @Route("/providers_roster_add",name="provider_roster_add")
     *
     */
    public function newProviderP()
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $generalCategories = $em->getRepository('App\Entity\GeneralCategories')->findAll();
            $lineofbusiness = $em->getRepository('App\Entity\LineOfBusiness')->findAll();
            $terminationReason = $em->getRepository('App\Entity\TerminationReason')->findAll();
            $languages = $em->getRepository('App\Entity\Languages')->findBy(array(), array('ord' => 'ASC'));
            $providertypes = $em->getRepository('App\Entity\ProviderType')->findBy(array(), array('name' => 'ASC'));
            $degrees = $em->getRepository('App\Entity\Degree')->findBy(array(), array('name' => 'ASC'));
            $specialties = $em->getRepository('App\Entity\Specialty')->findBy(array(), array('name' => 'ASC'));


            $us_states = $em->getRepository('App\Entity\UsState')->findAll();

            $totaLanguages = count($languages);
            $language_first_column_elements = ($totaLanguages / 3);
            if (is_float($language_first_column_elements)) {
                $language_first_column_elements = intval($language_first_column_elements + 1);
            }
            $language_rest_columns_element = intval(($totaLanguages - $language_first_column_elements) / 2);

            $totalprovidertypes = count($providertypes);
            $pt_first_column_elements = ($totalprovidertypes / 3);
            if (is_float($pt_first_column_elements)) {
                $pt_first_column_elements = intval($pt_first_column_elements + 1);
            }
            $pt_rest_columns_element = intval(($totalprovidertypes - $pt_first_column_elements) / 2);

            $totaldegrees = count($degrees);
            $degrees_first_column_elements = ($totaldegrees / 3);
            if (is_float($degrees_first_column_elements)) {
                $degrees_first_column_elements = intval($degrees_first_column_elements + 1);
            }
            $degrees_rest_columns_element = intval(($totaldegrees - $degrees_first_column_elements) / 2);

            $totalspecialties = count($specialties);
            $specialties_first_column_elements = ($totalspecialties / 3);
            if (is_float($specialties_first_column_elements)) {
                $specialties_first_column_elements = intval($specialties_first_column_elements + 1);
            }
            $specialties_rest_columns_element = intval(($totalspecialties - $specialties_first_column_elements) / 2);


            return $this->render('provider_p/add_provider_roster.html.twig', array(
                'generalCategories' => $generalCategories, 'lineofbusiness' => $lineofbusiness, 'terminationreasons' => $terminationReason,
                'languages' => $languages, 'providertypes' => $providertypes, 'degrees' => $degrees, 'specialties' => $specialties,
                'us_states' => $us_states, 'totaLanguages' => $totaLanguages, 'language_first_column_elements' => $language_first_column_elements, 'language_rest_columns_element' => $language_rest_columns_element,
                'totalprovidertypes' => $totalprovidertypes, 'pt_first_column_elements' => $pt_first_column_elements, 'pt_rest_columns_element' => $pt_rest_columns_element,
                'totaldegrees' => $totaldegrees, 'degrees_first_column_elements' => $degrees_first_column_elements, 'degrees_rest_columns_element' => $degrees_rest_columns_element,
                'totalspecialties' => $totalspecialties, 'specialties_first_column_elements' => $specialties_first_column_elements, 'specialties_rest_columns_element' => $specialties_rest_columns_element
            ));

        } else {
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/providers_roster_edit/{id}",name="provider_roster_edit",defaults={"id": null})
     *
     */
    public function editProviderP($id)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $generalCategories = $em->getRepository('App\Entity\GeneralCategories')->findAll();
            $lineofbusiness = $em->getRepository('App\Entity\LineOfBusiness')->findAll();
            $terminationReason = $em->getRepository('App\Entity\TerminationReason')->findAll();
            $languages = $em->getRepository('App\Entity\Languages')->findAll(array('ord' => 'ASC'));
            $providertypes = $em->getRepository('App\Entity\ProviderType')->findAll(array('name' => 'ASC'));
            $degrees = $em->getRepository('App\Entity\Degree')->findAll(array('name' => 'ASC'));
            $specialties = $em->getRepository('App\Entity\Specialty')->findAll(array('name' => 'ASC'));
            $us_states = $em->getRepository('App\Entity\UsState')->findAll();


            $provider = $em->getRepository('App\Entity\ProviderP')->find($id);

            $private_key = $this->getParameter('private_key');
            $encoder = new My_Mcript($private_key);
            $social = "";
            if ($provider) {
                $social = $encoder->decryptthis($provider->getSocial());
            }

            return $this->render('provider_p/edit_provider_roster.html.twig', array(
                'generalCategories' => $generalCategories, 'lineofbusiness' => $lineofbusiness, 'terminationreasons' => $terminationReason,
                'languages' => $languages, 'providertypes' => $providertypes, 'degrees' => $degrees, 'specialties' => $specialties, 'provider' => $provider,
                'social' => $social, 'us_states' => $us_states
            ));

        } else {
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/providers_roster_view/{id}",name="provider_roster_view",defaults={"id": null})
     *
     */
    public function viewProviderP($id)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $generalCategories = $em->getRepository('App\Entity\GeneralCategories')->findAll();
            $lineofbusiness = $em->getRepository('App\Entity\LineOfBusiness')->findAll();
            $terminationReason = $em->getRepository('App\Entity\TerminationReason')->findAll();
            $languages = $em->getRepository('App\Entity\Languages')->findAll(array('ord' => 'ASC'));
            $providertypes = $em->getRepository('App\Entity\ProviderType')->findAll(array('name' => 'ASC'));
            $degrees = $em->getRepository('App\Entity\Degree')->findAll(array('name' => 'ASC'));
            $specialties = $em->getRepository('App\Entity\Specialty')->findAll(array('name' => 'ASC'));
            $us_states = $em->getRepository('App\Entity\UsState')->findAll();


            $provider = $em->getRepository('App\Entity\ProviderP')->find($id);

            $private_key = $this->getParameter('private_key');
            $encoder = new My_Mcript($private_key);
            $social = "";
            if ($provider) {
                $social = $encoder->decryptthis($provider->getSocial());
            }

            return $this->render('provider_p/view_provider_roster.html.twig', array(
                'generalCategories' => $generalCategories, 'lineofbusiness' => $lineofbusiness, 'terminationreasons' => $terminationReason,
                'languages' => $languages, 'providertypes' => $providertypes, 'degrees' => $degrees, 'specialties' => $specialties, 'document' => $provider,
                'social' => $social, 'us_states' => $us_states
            ));

        } else {
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/providers_roster_save",name="provider_roster_save")
     *
     */
    public function saveProviderP(Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $private_key = $this->getParameter('private_key');
            $encoder = new My_Mcript($private_key);
            $user = $this->getUser();

            $name = $request->get('name');
            $last_name = $request->get('last_name');
            $initial = $request->get('initial');
            $npi_number = $request->get('npi_number');
            $general_category = $request->get('general_category');
            $dateofbirth = $request->get('dateofbirth');
            $gender = $request->get('gender');
            $email = $request->get('email');
            $social = $request->get('social');
            $medicare = $request->get('medicare');
            $medicaid = $request->get('medicaid');
            $caqh = $request->get('caqh');
            $taxonomy = $request->get('taxonomy');
            $lineofbusiness = $request->get('lineofbusiness');
            $dateagmt = $request->get('dateagmt');
            $state_lic = $request->get('state_lic');
            $licstate = $request->get('licstate');
            $board_certified = $request->get('board_certified');
            $board_name = $request->get('board_name');
            $interested_telemedicine_services = $request->get('interested_telemedicine_services');
            $alf_and_ltc = $request->get('alf_and_ltc');
            $accepting_new_patients = $request->get('accepting_new_patients');
            $are_you_pcp = $request->get('are_you_pcp');
            $gender_acceptance = $request->get('gender_acceptance');
            $disabled = $request->get('disabled');
            $termination_reason = $request->get('termination_reason');
            $notes = $request->get('notes');

            $languagesSelected = $request->get('languagesSelected');
            $providertypesSelected = $request->get('providertypesSelected');
            $degreesSelected = $request->get('degreesSelected');
            $specialtiesSelected = $request->get('specialtiesSelected');


            $provider = new ProviderP();

            $provider->setFirstName($name);
            $provider->setLastName($last_name);
            $provider->setInitial($initial);
            $provider->setNpiNumber($npi_number);
            $provider->setDateOfBirth($dateofbirth);
            $provider->setGender($gender);
            $provider->setGenderAcceptance($gender_acceptance);
            $provider->setEmail($email);

            $social_encode = $encoder->encryptthis($social);
            $provider->setSocial($social_encode);

            $provider->setMedicareNumber($medicare);
            $provider->setMedicaidNumber($medicaid);
            $provider->setCaqh($caqh);
            $provider->setTaxonomyCode($taxonomy);
            $provider->setDateOfAgmt($dateagmt);
            $provider->setStateLic($state_lic);
            $provider->setLicState($licstate);
            $provider->setBoardCertified($board_certified);
            $provider->setBoardName($board_name);
            $provider->setInteresedInTelemedicineServices($interested_telemedicine_services);
            $provider->setAlfAndLtcFacilities($alf_and_ltc);
            $provider->setAcceptingNewPatients($accepting_new_patients);
            $provider->setAreYouPcp($are_you_pcp);
            $provider->setDisabled($disabled);

            $trObj = $em->getRepository('App\Entity\TerminationReason')->find($termination_reason);
            if ($trObj) {
                $provider->setTerminationReason($trObj);
            }

            $provider->setNotes($notes);
            $provider->setUser($user);

            if ($general_category != "") {
                foreach ($general_category as $gc) {
                    $gcObj = $em->getRepository('App\Entity\GeneralCategories')->find($gc);

                    if ($gcObj) {
                        $provider->addGeneralCategory($gcObj);
                    }
                }
            }

            if ($lineofbusiness != "") {
                foreach ($lineofbusiness as $lb) {
                    $lbObj = $em->getRepository('App\Entity\LineOfBusiness')->find($lb);

                    if ($lbObj) {
                        $provider->addLineOfBusines($lbObj);
                    }
                }
            }

            $specialties = substr($specialtiesSelected, 0, -1);
            $lbs = explode(",", $specialties);

            foreach ($lbs as $specialty) {
                if ($provider != null) {
                    $specialtyObj = $em->getRepository('App\Entity\Specialty')->find($specialty);
                    if ($specialtyObj != null) {
                        $provider->addPrimarySpecialty($specialtyObj);
                    }
                }
            }

            $degrees = substr($degreesSelected, 0, -1);
            $lbs = explode(",", $degrees);

            foreach ($lbs as $degree) {
                if ($provider != null) {
                    $degreeObj = $em->getRepository('App\Entity\Degree')->find($degree);
                    if ($degreeObj != null) {
                        $provider->addDegree($degreeObj);
                    }
                }
            }

            $providertypes = substr($providertypesSelected, 0, -1);
            $lbs = explode(",", $providertypes);

            foreach ($lbs as $providertype) {
                if ($provider != null) {
                    $providerT = $em->getRepository('App\Entity\ProviderType')->find($providertype);
                    if ($providerT != null) {
                        $provider->addProviderType($providerT);
                    }
                }
            }

            $languages = substr($languagesSelected, 0, -1);
            $lbs = explode(",", $languages);

            foreach ($lbs as $language) {
                if ($provider != null) {
                    $lang = $em->getRepository('App\Entity\Languages')->find($language);
                    if ($lang != null) {
                        $provider->addLanguage($lang);
                    }
                }
            }

            $em->persist($provider);
            $em->flush();

            return $this->redirectToRoute('provider_list');

        } else {
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/providers_roster_update",name="provider_roster_update")
     *
     */
    public function updateProviderP(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $private_key = $this->getParameter('private_key');
            $encoder = new My_Mcript($private_key);
            $user = $this->getUser();

            $name = $request->get('name');
            $last_name = $request->get('last_name');
            $initial = $request->get('initial');
            $npi_number = $request->get('npi_number');
            $general_category = $request->get('general_category');
            $dateofbirth = $request->get('dateofbirth');
            $gender = $request->get('gender');
            $email = $request->get('email');
            $social = $request->get('social');
            $medicare = $request->get('medicare');
            $medicaid = $request->get('medicaid');
            $caqh = $request->get('caqh');
            $taxonomy = $request->get('taxonomy');
            $lineofbusiness = $request->get('lineofbusiness');
            $dateagmt = $request->get('dateagmt');
            $state_lic = $request->get('state_lic');
            $licstate = $request->get('licstate');
            $board_certified = $request->get('board_certified');
            $board_name = $request->get('board_name');
            $interested_telemedicine_services = $request->get('interested_telemedicine_services');
            $alf_and_ltc = $request->get('alf_and_ltc');
            $accepting_new_patients = $request->get('accepting_new_patients');
            $are_you_pcp = $request->get('are_you_pcp');
            $gender_acceptance = $request->get('gender_acceptance');
            $disabled = $request->get('disabled');
            $termination_reason = $request->get('termination_reason');
            $notes = $request->get('notes');

            $languagesSelected = $request->get('languagesSelected');
            $providertypesSelected = $request->get('providertypesSelected');
            $degreesSelected = $request->get('degreesSelected');
            $specialtiesSelected = $request->get('specialtiesSelected');

            $provider_id = $request->get('provider_id');
            $provider = $em->getRepository('App\Entity\ProviderP')->find($provider_id);

            if ($provider == null) {
                return $this->redirectToRoute('provider_login');
            }

            $provider->setFirstName($name);
            $provider->setLastName($last_name);
            $provider->setInitial($initial);
            $provider->setNpiNumber($npi_number);
            $provider->setDateOfBirth($dateofbirth);
            $provider->setGender($gender);
            $provider->setGenderAcceptance($gender_acceptance);
            $provider->setEmail($email);

            $social_encode = $encoder->encryptthis($social);
            $provider->setSocial($social_encode);

            $provider->setMedicareNumber($medicare);
            $provider->setMedicaidNumber($medicaid);
            $provider->setCaqh($caqh);
            $provider->setTaxonomyCode($taxonomy);
            $provider->setDateOfAgmt($dateagmt);
            $provider->setStateLic($state_lic);
            $provider->setLicState($licstate);
            $provider->setBoardCertified($board_certified);
            $provider->setBoardName($board_name);
            $provider->setInteresedInTelemedicineServices($interested_telemedicine_services);
            $provider->setAlfAndLtcFacilities($alf_and_ltc);
            $provider->setAcceptingNewPatients($accepting_new_patients);
            $provider->setAreYouPcp($are_you_pcp);
            $provider->setDisabled($disabled);

            $trObj = $em->getRepository('App\Entity\TerminationReason')->find($termination_reason);
            if ($trObj) {
                $provider->setTerminationReason($trObj);
            }

            $provider->setNotes($notes);
            $provider->setUser($user);

            $languagesCurrent = $provider->getLanguages();
            if ($languagesCurrent) {
                foreach ($languagesCurrent as $lc) {
                    $provider->removeLanguage($lc);
                }
            }

            $generalCategoriesCurrent = $provider->getGeneralCategories();
            if ($generalCategoriesCurrent) {
                foreach ($generalCategoriesCurrent as $gc) {
                    $provider->removeGeneralCategory($gc);
                }
            }

            $lineOfBusinessCurrent = $provider->getLineOfBusiness();
            if ($lineOfBusinessCurrent) {
                foreach ($lineOfBusinessCurrent as $lbc) {
                    $provider->removeLineOfBusines($lbc);
                }
            }

            $providerTypeCurrent = $provider->getProviderType();
            if ($providerTypeCurrent) {
                foreach ($providerTypeCurrent as $ptc) {
                    $provider->removeProviderType($ptc);
                }
            }

            $primarySpecialtiesCurrent = $provider->getPrimarySpecialties();
            if ($primarySpecialtiesCurrent) {
                foreach ($primarySpecialtiesCurrent as $psc) {
                    $provider->removePrimarySpecialty($psc);
                }
            }

            $degreeCurrent = $provider->getDegree();
            if ($degreeCurrent) {
                foreach ($degreeCurrent as $dc) {
                    $provider->removeDegree($dc);
                }
            }

            $em->persist($provider);
            $em->flush();

            if ($general_category != "") {
                foreach ($general_category as $gc) {
                    $gcObj = $em->getRepository('App\Entity\GeneralCategories')->find($gc);

                    if ($gcObj) {
                        $provider->addGeneralCategory($gcObj);
                    }
                }
            }

            if ($lineofbusiness != "") {
                foreach ($lineofbusiness as $lb) {
                    $lbObj = $em->getRepository('App\Entity\LineOfBusiness')->find($lb);

                    if ($lbObj) {
                        $provider->addLineOfBusines($lbObj);
                    }
                }
            }

            $specialties = substr($specialtiesSelected, 0, -1);
            $lbs = explode(",", $specialties);

            foreach ($lbs as $specialty) {
                if ($provider != null) {
                    $specialtyObj = $em->getRepository('App\Entity\Specialty')->find($specialty);
                    if ($specialtyObj != null) {
                        $provider->addPrimarySpecialty($specialtyObj);
                    }
                }
            }

            $degrees = substr($degreesSelected, 0, -1);
            $lbs = explode(",", $degrees);

            foreach ($lbs as $degree) {
                if ($provider != null) {
                    $degreeObj = $em->getRepository('App\Entity\Degree')->find($degree);
                    if ($degreeObj != null) {
                        $provider->addDegree($degreeObj);
                    }
                }
            }

            $providertypes = substr($providertypesSelected, 0, -1);
            $lbs = explode(",", $providertypes);

            foreach ($lbs as $providertype) {
                if ($provider != null) {
                    $providerT = $em->getRepository('App\Entity\ProviderType')->find($providertype);
                    if ($providerT != null) {
                        $provider->addProviderType($providerT);
                    }
                }
            }

            $languages = substr($languagesSelected, 0, -1);
            $lbs = explode(",", $languages);

            foreach ($lbs as $language) {
                if ($provider != null) {
                    $lang = $em->getRepository('App\Entity\Languages')->find($language);
                    if ($lang != null) {
                        $provider->addLanguage($lang);
                    }
                }
            }

            $em->persist($provider);
            $em->flush();

            return $this->redirectToRoute('provider_list');
        } else {
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="provider_org_save")
     *
     */
    public function create(Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $save_status = $request->get('save_status');
            $organization_type = $request->get('organization_type');
            $organization_name = $request->get('organization_name');

            $npi = $request->get('npi');
            $npi2 = $request->get('npi2');
            $taxonomy_code = $request->get('taxonomy_code');
            $tin_number = $request->get('tin_number');
            $lega_name_tin_owner = $request->get('lega_name_tin_owner');
            $fein = $request->get('fein');
            $caqh = $request->get('caqh');
            $name_ehr_emr = $request->get('name_ehr_emr');
            $billing_phone = $request->get('billing_phone');
            $phone = $request->get('phone');
            $faxno = $request->get('faxno');
            $website = $request->get('website');
            $hospital_afiliation = $request->get('hospital_afiliation');
            $american_sign_language = $request->get('american_sign_language');
            $accreditationSelected = $request->get('accreditationSelected');

            $clasificationsSelected = $request->get('clasificationsSelected');
            $specialtiesSelected = $request->get('specialtiesSelected');

            //get user conectec
            $providerUser = $this->getUser();

            $flag = 0;
            //$organization=new OrganizationP();
            if ($save_status == 0) {
                $organization = new OrganizationP();
                $flag = 1;
            } else {
                $organization = $em->getRepository('App\Entity\OrganizationP')->find($save_status);

                //set update  date
                $organization->setUpdatedOn();
            }

            $organization->setName($organization_name);
            $organization->setProviderType($organization_type);
            $organization->setGroupNpi($npi);
            $organization->setGroupNpi2($npi2);
            $organization->setTinNumber($tin_number);
            $organization->setTaxonomyCode($taxonomy_code);
            $organization->setLegalNameTinOwner($lega_name_tin_owner);
            $organization->setFein($fein);
            $organization->setCaqh($caqh);
            $organization->setNameOfEhrEmr($name_ehr_emr);
            $organization->setBillingPhone($billing_phone);
            $organization->setPhone($phone);
            $organization->setFaxno($faxno);
            $organization->setWebsite($website);
            $organization->setHospitalAffiliations($hospital_afiliation);
            $organization->setAmericanSignLanguage($american_sign_language);
            $organization->setOrganizationStatus($em->getRepository('App\Entity\OrganizationStatus')->find(1));

            if ($providerUser != null) {
                $organization->setProviderUser($providerUser);
            }

            if ($organization_type == "Individual" and $save_status != 0) {
                $organization->setTaxonomyCode(null);

                //remove organization accreditation
                $listAccreditation = $organization->getAccreditations();

                if ($listAccreditation) {
                    foreach ($listAccreditation as $accreditation) {
                        $organization->removeAccreditation($accreditation);
                    }
                }
            }

            //set all Accreditation Selected
            if ($accreditationSelected != "") {
                $listAccreditation = $organization->getAccreditations();
                if ($listAccreditation) {
                    foreach ($listAccreditation as $accreditation) {
                        $organization->removeAccreditation($accreditation);
                    }
                }

                $accreditations = substr($accreditationSelected, 0, -1);
                $lbs = explode(",", $accreditations);

                foreach ($lbs as $accreditation) {
                    if ($accreditation != null) {
                        $accreditationObj = $em->getRepository('App\Entity\Accreditation')->find($accreditation);
                        if ($accreditationObj != null) {
                            $organization->addAccreditation($accreditationObj);
                        }
                    }
                }
            } else {
                $listAccreditation = $organization->getAccreditations();
                if ($listAccreditation) {
                    foreach ($listAccreditation as $accreditation) {
                        $organization->removeAccreditation($accreditation);
                    }
                }
            }

            //Set all Organization Clasification Selected

            if ($clasificationsSelected != "") {
                $listClasifications = $organization->getOrgSpecialties();
                if ($listClasifications) {
                    foreach ($listClasifications as $clasification) {
                        $organization->removeOrgSpecialty($clasification);
                    }
                }

                $clasifications = substr($clasificationsSelected, 0, -1);
                $lbs = explode(",", $clasifications);

                foreach ($lbs as $clasification) {
                    if ($clasification != null) {
                        $clasificationObj = $em->getRepository('App\Entity\OrgSpecialty')->find($clasification);
                        if ($clasificationObj != null) {
                            $organization->addOrgSpecialty($clasificationObj);
                        }
                    }
                }
            } else {
                $listClasifications = $organization->getOrgSpecialties();
                if ($listClasifications) {
                    foreach ($listClasifications as $clasification) {
                        $organization->removeOrgSpecialty($clasification);
                    }
                }
            }

            //set all Specialties Selected
            if ($specialtiesSelected != "") {
                $listSpecialties = $organization->getSpecialtyAreas();
                if ($listSpecialties) {
                    foreach ($listSpecialties as $specialty) {
                        $organization->removeSpecialtyArea($specialty);
                    }
                }

                $specialties = substr($specialtiesSelected, 0, -1);
                $lbs = explode(",", $specialties);

                foreach ($lbs as $specialty) {
                    if ($specialty != null) {
                        $specialtyObj = $em->getRepository('App\Entity\SpecialtyAreas')->find($specialty);
                        if ($specialtyObj != null) {
                            $organization->addSpecialtyArea($specialtyObj);
                        }
                    }
                }
            } else {
                $listSpecialties = $organization->getSpecialtyAreas();
                if ($listSpecialties) {
                    foreach ($listSpecialties as $specialty) {
                        $organization->removeSpecialtyArea($specialty);
                    }
                }
            }

            $em->persist($organization);
            $em->flush();

            if ($flag == 1) {
                return $this->redirectToRoute('provider_list');
            }

            return $this->redirectToRoute('provider_dashboard');


        } else {
            return $this->redirectToRoute('admin_login');
        }
    }

    public function PercentFacilityAppComplete(FacilityApp $facility)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $totalProperty = 35;
            $percent = 0;
            $totalCompleted = 0;

            if ($facility->getName()) {
                $totalCompleted++;
            }
            if ($facility->getFacilityDba()) {
                $totalCompleted++;
            }
            if ($facility->getNpiNumber()) {
                $totalCompleted++;
            }
            if ($facility->getTaxIdentification()) {
                $totalCompleted++;
            }
            if ($facility->getMedicaidNumber()) {
                $totalCompleted++;
            }
            if ($facility->getMedicareNumber()) {
                $totalCompleted++;
            }
            if ($facility->getContactName()) {
                $totalCompleted++;
            }
            if ($facility->getContactLastName()) {
                $totalCompleted++;
            }
            if ($facility->getContactEmail()) {
                $totalCompleted++;
            }
            if ($facility->getContactPhoneNumber()) {
                $totalCompleted++;
            }
            if ($facility->getContactFax()) {
                $totalCompleted++;
            }
            if ($facility->getAdditionalServices()) {
                $totalCompleted++;
            }
            if ($facility->getOtherAdditionalService()) {
                $totalCompleted++;
            }
            if ($facility->getFacilityType()) {
                $totalCompleted++;
            }
            if ($facility->getFacilityOwner()) {
                $totalCompleted++;
            }
            if ($facility->getLegalType()) {
                $totalCompleted++;
            }
            if ($facility->getStateLicenseNumber()) {
                $totalCompleted++;
            }
            if ($facility->getStateLicenseExpirationDate()) {
                $totalCompleted++;
            }
            if ($facility->getLicenseCityCounty()) {
                $totalCompleted++;
            }
            if ($facility->getCityLicenseNumber()) {
                $totalCompleted++;
            }
            if ($facility->getCityLicenseExpirationDate()) {
                $totalCompleted++;
            }
            if ($facility->getAccreditationBody()) {
                $totalCompleted++;
            }
            if ($facility->getDateLastAccreditation()) {
                $totalCompleted++;
            }
            if ($facility->getAccreditationExpirationDate()) {
                $totalCompleted++;
            }
            if ($facility->getDateRecentSurveyConducted()) {
                $totalCompleted++;
            }
            if ($facility->getAccreditationMoreContext()) {
                $totalCompleted++;
            }
            if ($facility->getInsuranceAgency()) {
                $totalCompleted++;
            }
            if ($facility->getPolicyNumber()) {
                $totalCompleted++;
            }
            if ($facility->getSingleOccurrenceAmount()) {
                $totalCompleted++;
            }
            if ($facility->getAggredateAmount()) {
                $totalCompleted++;
            }
            if ($facility->getIssuesDate()) {
                $totalCompleted++;
            }
            if ($facility->getExpirationDate()) {
                $totalCompleted++;
            }
            if ($facility->getContextConvictionsUnderFederal()) {
                $totalCompleted++;
            }
            if ($facility->getContectFederalSanstionsLimitations()) {
                $totalCompleted++;
            }


            $percent = (100 * $totalCompleted) / $totalProperty;
            $percent = floatval($percent);
            $percent = number_format($percent, 0);

            return $percent;
        } else {
            return $this->redirectToRoute('provider_login');
        }
    }

    public function PercentOrganizationComplete(OrganizationP $organization)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $totalProperty = 19;
            $percent = 0;
            $totalCompleted = 0;


            if ($organization->getName()) {
                $totalCompleted++;
            }

            if ($organization->getProviderType()) {
                $totalCompleted++;

                if ($organization->getProviderType() != "Facility") {
                    $totalProperty = 17;
                }
            }

            if ($organization->getGroupNpi()) {
                $totalCompleted++;
            }

            if ($organization->getGroupNpi2()) {
                $totalCompleted++;
            }

            if ($organization->getTaxonomyCode()) {
                if ($organization->getProviderType() == "Facility") {
                    $totalCompleted++;
                }
            }

            if ($organization->getCaqh()) {
                $totalCompleted++;
            }

            if ($organization->getAccreditations()) {
                if ($organization->getProviderType() == "Facility") {
                    $totalCompleted++;
                }
            }

            if ($organization->getAmericanSignLanguage()) {
                $totalCompleted++;
            }

            if ($organization->getBillingPhone()) {
                $totalCompleted++;
            }

            if ($organization->getFaxno()) {
                $totalCompleted++;
            }

            if ($organization->getFein()) {
                $totalCompleted++;
            }

            if ($organization->getHospitalAffiliations()) {
                $totalCompleted++;
            }

            if ($organization->getTinNumber()) {
                $totalCompleted++;
            }

            if ($organization->getLegalNameTinOwner()) {
                $totalCompleted++;
            }

            if ($organization->getNameOfEhrEmr()) {
                $totalCompleted++;
            }

            if ($organization->getPhone()) {
                $totalCompleted++;
            }

            if ($organization->getWebsite()) {
                $totalCompleted++;
            }


            if ($organization->getSpecialtyAreas()) {
                $totalCompleted++;
            }

            if ($organization->getOrgSpecialties()) {
                $totalCompleted++;
            }

            $percent = (100 * $totalCompleted) / $totalProperty;
            $percent = floatval($percent);
            $percent = number_format($percent, 0);

            return $percent;
        } else {
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="provider_delete_provider",methods={"POST","DELETE"})
     *
     */
    public function deleteAction(Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $provider = $role = $em->getRepository('App\Entity\ProviderP')->find($id);
            $removed = 0;
            $message = "";


            if ($provider) {
                try {
                    $languagesCurrent = $provider->getLanguages();
                    if ($languagesCurrent) {
                        foreach ($languagesCurrent as $lc) {
                            $provider->removeLanguage($lc);
                        }
                    }

                    $generalCategoriesCurrent = $provider->getGeneralCategories();
                    if ($generalCategoriesCurrent) {
                        foreach ($generalCategoriesCurrent as $gc) {
                            $provider->removeGeneralCategory($gc);
                        }
                    }

                    $lineOfBusinessCurrent = $provider->getLineOfBusiness();
                    if ($lineOfBusinessCurrent) {
                        foreach ($lineOfBusinessCurrent as $lbc) {
                            $provider->removeLineOfBusines($lbc);
                        }
                    }

                    $providerTypeCurrent = $provider->getProviderType();
                    if ($providerTypeCurrent) {
                        foreach ($providerTypeCurrent as $ptc) {
                            $provider->removeProviderType($ptc);
                        }
                    }

                    $primarySpecialtiesCurrent = $provider->getPrimarySpecialties();
                    if ($primarySpecialtiesCurrent) {
                        foreach ($primarySpecialtiesCurrent as $psc) {
                            $provider->removePrimarySpecialty($psc);
                        }
                    }

                    $degreeCurrent = $provider->getDegree();
                    if ($degreeCurrent) {
                        foreach ($degreeCurrent as $dc) {
                            $provider->removeDegree($dc);
                        }
                    }

                    $em->remove($provider);
                    $em->flush();
                    $removed = 1;
                    $message = "The Provider has been Successfully removed";
                } catch (Exception $ex) {
                    $message = "The provider can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        } else {
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_role",methods={"POST","DELETE"})
     *
     */
    public function deleteMultipleAction(Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $role = $role = $em->getRepository('App\Entity\Role')->find($id);

                if ($role) {
                    try {
                        $em->remove($role);
                        $em->flush();
                        $removed = 1;
                        $message = "The Role has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Role can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        } else {
            return $this->redirectToRoute('admin_login');
        }
    }


    private function createCustomForm($id, $method, $route)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }

    /**
     * @Route("/my_profile", name="provider_user_profile")
     *
     */
    public function profile()
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();

            $roles = $em->getRepository('App\Entity\ProviderRole')->findAll();

            return $this->render('provideruser/profile.html.twig', array('user' => $user, 'roles' => $roles));
        } else {
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/view_ajax_providerp", name="view_ajax_providerp")
     */
    public function viewProviderPAjax(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id=$request->get('id');
        $private_key = $this->getParameter('private_key');
        $encoder = new My_Mcript($private_key);

        $provider=$em->getRepository('App\Entity\ProviderP')->find($id);

        $providerResult=array();
        if($provider){

            $providerResult['fullname']=$provider->getFirstName()." ".$provider->getLastName();
            $providerResult['initial']=$provider->getInitial();
            $providerResult['npi']=$provider->getNpiNumber();
            $providerResult['dob']=$provider->getDateOfBirth();
            $providerResult['gender']=$provider->getGender();
            $providerResult['email']=$provider->getEmail();
            $providerResult['social']= $encoder->decryptthis($provider->getSocial());
            $providerResult['medicare']=$provider->getMedicareNumber();
            $providerResult['medicaid']=$provider->getMedicaidNumber();
            $providerResult['caqh']=$provider->getCaqh();
            $providerResult['taxonomy']=$provider->getTaxonomyCode();
            $providerResult['agmt']=$provider->getDateOfAgmt();
            $providerResult['statelic']=$provider->getStateLic();
            $providerResult['licstate']=$provider->getLicState();
            $providerResult['boardcertified']=$provider->getBoardCertified();
            $providerResult['boardname']=$provider->getBoardName();
            $providerResult['telemedicine']=$provider->getInteresedInTelemedicineServices();
            $providerResult['alf_ltc']=$provider->getAlfAndLtcFacilities();
            $providerResult['newpatients']=$provider->getAcceptingNewPatients();
            $providerResult['pcp']=$provider->getAreYouPcp();
            $providerResult['gender_acceptance']=$provider->getGenderAcceptance();
            $providerResult['disabled']=$provider->getDisabled();
            $providerResult['notes']=$provider->getNotes();

            $providerGeneralCategories=array();

            $generalCategories=$provider->getGeneralCategories();
            foreach ($generalCategories as $generalCategory){
                 $orgR=array();
                 $orgR['name']=$generalCategory->getName();

                $providerGeneralCategories[]=$orgR;
            }

            $languagesR=array();
            $languages=$provider->getLanguages();
            foreach ($languages as $language){
                $lang=array();
                $lang['name']=$language->getName();
                $languagesR[]=$lang;
            }

            $lineofbussinessR=array();
            $lineofbussiness=$provider->getLineOfBusiness();
            foreach ($lineofbussiness as $lineofb){
                $line=array();
                $line['name']=$lineofb->getName();

                $lineofbussinessR[]=$line;
            }

            $degreesR=array();
            $degrees=$provider->getDegree();
            foreach ($degrees as $degree){
                $degreeR=array();
                $degreeR['name']=$degree->getName();

                $degreesR[]=$degreeR;
            }

            $primarySpecialtiesR=array();
            $primarySpecialties=$provider->getPrimarySpecialties();
            foreach ($primarySpecialties as $primarySpecialty){
                $primarySpecialtyR=array();
                $primarySpecialtyR['name']=$primarySpecialty->getName();

                $primarySpecialtiesR[]=$primarySpecialtyR;
            }

            $providerTypesR=array();
            $providerTypes=$provider->getProviderType();
            foreach ($providerTypes as $providerType){
                $providerTypeR=array();
                $providerTypeR['name']=$providerType->getName();

                $providerTypesR[]=$providerTypeR;
            }
        }else{
            return new Response(
                json_encode(array('provider'=>null)), 200, array('Content-Type' => 'application/json')
            );
        }

        return new Response(
            json_encode(array('id'=>$id,'provider'=>$providerResult,'providerGeneralCategories'=>$providerGeneralCategories,
                'languages'=>$languagesR,'lineofbussiness'=>$lineofbussinessR,'degrees'=>$degreesR,'primarySpecialties'=>$primarySpecialtiesR,
                'providerTypesR'=>$providerTypesR)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/view_ajax_contactp", name="view_ajax_contactp")
     */
    public function viewContactPAjax(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id=$request->get('id');

        $contact=$em->getRepository('App\Entity\ContactP')->find($id);

        $contactR=array();
        $contactGoodFor=array();
        if($contact){
            $contactR['name']=$contact->getName();
            $contactR['title']=$contact->getTitle()->getName();
            $contactR['description']=$contact->getDescription();
            $contactR['phone']=$contact->getPhone();
            $contactR['ext']=$contact->getPhoneExt();
            $contactR['mobile']=$contact->getMobile();
            $contactR['fax']=$contact->getFax();
            $contactR['email']=$contact->getEmail();
            $contactR['main']=$contact->getMain();
            $contactR['address']=$contact->getAddress();
            $contactR['city']=$contact->getCity();
            $contactR['state']=$contact->getState();
            $contactR['zip_code']=$contact->getZipCode();


            $contactgoogFor=$contact->getBestChoices();
            foreach ($contactgoogFor as $contactgf){
                $cgf=array();
                $cgf['name']=$contactgf->getName();
                $contactGoodFor[]=$cgf;
            }
        }else{
            return new Response(
                json_encode(array('contact'=>null,'contactGoodFor'=>null)), 200, array('Content-Type' => 'application/json')
            );
        }

        return new Response(
            json_encode(array('contact'=>$contactR,'contact_best'=>$contactGoodFor)), 200, array('Content-Type' => 'application/json')
        );
    }
}

