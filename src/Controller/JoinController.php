<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\EmailQueued;
use App\Entity\JOIN;
use App\Entity\JoinFile;
use App\Entity\Organization;
use App\Entity\OrganizationContact;
use App\Entity\Prospect;
use App\Entity\ProspectAddress;
use App\Form\JoinType;
use App\Service\JoinFileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class JoinController extends AbstractController
{
    /**
     * @Route("/join-us", name="join_us")
     */
    public function index(){
        $em=$this->getDoctrine()->getManager();
        $counties=$em->getRepository('App\Entity\CountyMatch')->findAll();
        $languages=$em->getRepository('App\Entity\Languages')->findAll();

        return $this->render('join/index.html.twig', [
            'counties' => $counties,'languages'=>$languages
        ]);
    }

    /**
     * @Route("/join-us-2", name="join_us_2")
     */
    public function add(Request $request){
        $em=$this->getDoctrine()->getManager();

        $join=new JOIN();
        $form=$this->createForm(JoinType::class, $join);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

        }

        return $this->render('join/form.html.twig', [
            'form'=>$form->createView()
        ]);
    }

    /**
     * @Route("/admin/join-us/list", name="join_us_index")
     */
    public function list(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $joins = $em->getRepository('App\Entity\JOIN')->findBy(array(),array('id'=>'DESC'));
            $delete_form_ajax = $this->createCustomForm('JOIN_ID', 'DELETE', 'admin_delete_join');

            return $this->render('join/list.html.twig', ['joins' => $joins, 'delete_form_ajax' => $delete_form_ajax->createView()]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/admin/join-us/view/{id}", name="join_us_view",defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\JOIN')->find($id);
            $files=$em->getRepository('App\Entity\JoinFile')->findBy(array('join'=>$id));
            $pdataArray=array();
            $addressPdata=array();

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('join_us_index');
            }

            $json=$document->getNppesData();
            $datos=json_decode($json,true);

            if(count($datos)>=1){
                $addressPdata=$datos['results'][0]['addresses'];
                $pdataArray['npi']=$datos['results'][0]['number'];
                $pdataArray['name']=$datos['results'][0]['basic']['organization_name'];
                $pdataArray['status']=$datos['results'][0]['basic']['status'];
                $pdataArray['npi_type']=$datos['results'][0]['enumeration_type'];

                $address=$datos['results'][0]['addresses'];

                foreach ($address as $addr){
                    if($addr['address_purpose']=="LOCATION"){
                        $addr_str="";
                        $addr_str=$addr['address_1'].",".$addr['address_2']." \n".$addr['city']." , ".$addr['state']." ";
                        $codeP=$addr['postal_code'];
                        $codeP=substr($codeP,0,5)."-".substr($codeP,5,4);
                        $addr_str.=$codeP;
                        $pdataArray['prymary_location']=$addr_str;
                        $pdataArray['phone']=$addr['telephone_number'];
                    }
                }

                $taxonomies=$datos['results'][0]['taxonomies'];

                foreach ($taxonomies as $taxonomy){
                    if($taxonomy['primary']==true){
                        $pdataArray['taxonomy']=$taxonomy['desc']." - (".$taxonomy['code'].")";
                    }
                }
            }

            $pmls= [];
            $conn = $em->getConnection();
            $npi=$document->getNpiNumber();
            if($npi!="" and $npi!=null){
                $sql="SELECT p.col_a as medicaid, p.col_w as status,p.col_n as npi_number, p.col_d, p.col_e,p.col_f, p.col_v, p.col_l,p.col_u, p.col_g, p.col_v,p.col_m
            FROM  pml p 
            WHERE p.col_n=$npi";

                $stmt = $conn->prepare($sql);
                $stmt->execute();
                $pmls= $stmt->fetchAll();
            }

            return $this->render('join/view.html.twig', array('document' => $document,'files'=>$files,'pdata'=>$pdataArray, 'addressPdata'=>$addressPdata,'pmls'=>$pmls));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/save-join-save", name="save_join_us",methods={"POST"})
     */
    public function save(Request  $request, JoinFileUploader $fileUploader){
        $em=$this->getDoctrine()->getManager();

        $name=$request->get('name');
        $npi_number=$request->get('npi_number_1');
        $contact_name=$request->get('contact_name');
        $contact_email=$request->get('contact_email');
        $fqhc=$request->get('fqhc');
        $cmhc=$request->get('cmhc');
        $rural_health_center=$request->get('rural_health_center');
        $other_state=$request->get('other_state');
        $npppes_data=$request->get('npppes_data');

        $street=$request->get('street');
        $suite_number=$request->get('suite_number');
        $city=$request->get('city');
        $county=$request->get('county');
        $phone_number=$request->get('phone_number');
        $counties=$request->get('counties');
        $status=$em->getRepository('App\Entity\JoinStatus')->find(1);
        $other_language=$request->get('other_language');
        $notes=$request->get('notes');
        
        //verify exits recors by NPI
        $current_join=$em->getRepository('App\Entity\JOIN')->findOneBy(array('npi_number'=>$npi_number));

        $languagesSelected = $request->get('languagesSelected');
        $languages = substr($languagesSelected, 0, -1);
        $lbs = explode(",", $languages);

        if($current_join==null){
            $join=new JOIN();
            $join->setName($name);
            $join->setNpiNumber($npi_number);
            $join->setContactName($contact_name);
            $join->setContactEmail($contact_email);
            $join->setNppesData($npppes_data);
            $join->setStatus($status);
            $join->setOtherLanguage($other_language);
            $join->setNotes($notes);

            foreach ($lbs as $language) {
                if ($join != null) {
                    $lang = $em->getRepository('App\Entity\Languages')->find($language);
                    if ($lang != null) {
                        $join->addLanguage($lang);
                    }
                }
            }

            if($fqhc=="-"){
                $join->setFqhc(false);
            }else{
                $join->setFqhc($fqhc);
            }

            if($cmhc=="-"){
                $join->setCmhc(false);
            }else{
                $join->setCmhc($cmhc);
            }

            if($rural_health_center=="-"){
                $join->setRuralHealthCenter(false);
            }else{
                $join->setRuralHealthCenter($rural_health_center);
            }
            if($other_state=="-"){
                $join->setOtherState(false);
            }else{
                $join->setOtherState($other_state);
            }
            $join->setStreet($street);
            $join->setSuiteNumber($suite_number);
            $join->setCity($city);
            $join->setCounty($county);
            $join->setPhoneNumber($phone_number);

            if($counties!=""){
                foreach ($counties as $county){
                    $CountyMath=$em->getRepository('App\Entity\CountyMatch')->find($county);
                    $join->addCounty($CountyMath);
                }
            }

            $em->persist($join);
            $em->flush();

            $files = $request->files->get('files_join');
            if(isset($files) and $files!=""){
                foreach ($files as $file){
                    $id=$join->getId();
                    $extension=$file->guessExtension();
                    $file_name="join_".uniqid()."_".$id;
                    $file = $fileUploader->upload($file, $file_name);
                    if ($file != "error") {
                        $join_file=new JoinFile();
                        $join_file->setPath($file_name.".".$extension);
                        $join_file->setJoin($join);
                        $em->persist($join_file);
                        $em->flush();
                    }
                }
            }
        }

       return $this->redirectToRoute('join_us_thanks');
    }

    /**
     * @Route("/join-us-thanks", name="join_us_thanks")
     */
    public function joinUsThanks() {

        return $this->render('join/final.html.twig');
    }

    /**
     * @Route("/checknpi", name="join_check_npi",methods={"POST"})
     */
    public function checkNPI(Request $request) {
        $npi = $request->get('npi');
        $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=1&skip=&pretty=on&version=2.1";
        $json=file_get_contents($url_result);

        return new Response(
            json_encode(array('json_result' =>$json)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/get-pml-npi", name="join_get_pml_npi",methods={"POST"})
     */
    public function getPMLNPI(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $npi = $request->get('npi');

        $sql="SELECT p.col_a as medicaid, p.col_w as status,p.col_n as npi_number, p.col_d, p.col_e,p.col_f, p.col_v, p.col_l,p.col_u, p.col_g
            FROM  pml p 
            WHERE p.col_n=$npi";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $pmls= $stmt->fetchAll();

        $organization=$em->getRepository('App\Entity\Organization')->findOneBy(array('group_npi'=>$npi));

        $org_exit=1;
        if($organization==null){
            $org_exit=0;
        }

        return new Response(
            json_encode(array('pmls'=>$pmls,'org_exit'=>$org_exit)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/admin/join/delete/{id}", name="admin_delete_join",methods={"POST","DELETE"})
     */
    public function delete(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $join = $em->getRepository('App\Entity\JOIN')->find($id);
            $removed = 0;
            $message = "";

            $joinFiles=$em->getRepository('App\Entity\JoinFile')->findBy(array('join'=>$id));
            if ($joinFiles) {
                foreach ($joinFiles as $join_file){
                    $em->remove($join_file);
                    $em->flush();
                }
            }

            if ($join) {
                try {
                    $em->remove($join);
                    $em->flush();
                    $removed = 1;
                    $message = "The Join Us record has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Join Us record can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/admin/join-us/convert-to-organization", name="join_us_to_organization",methods={"POST"})
     */
    public function convertToOrganization(Request  $request){
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');
        $organization_type=$request->get('organization_type');

        $join=$em->getRepository('App:JOIN')->find($id);
        if($join!=null){
            $organization_status=$em->getRepository('App\Entity\OrganizationStatus')->find(1);

            $organization=new Organization();
            $organization->setName($join->getName());
            $organization->setGroupNpi($join->getNpiNumber());
            $organization->setOrganizationStatus($organization_status);
            $organization->setProviderType($organization_type);
            $organization->setDisabled(0);
            $em->persist($organization);
            $em->flush();

            $org_contact=new OrganizationContact();
            $org_contact->setName($join->getContactName());
            $org_contact->setEmail($join->getContactEmail());
            $org_contact->setPhone($join->getPhoneNumber());
            $org_contact->setOrganization($organization);
            $em->persist($org_contact);

            $addr=new BillingAddress();
            $addr->setCity($join->getCity());
            $addr->setOrganization($organization);
            $addr->setStreet($join->getStreet());
            $addr->setSuiteNumber($join->getSuiteNumber());
            $addr->setPhoneNumber($join->getPhoneNumber());
            $addr->setUsState('FL');
            $addr->setCounty($join->getCounty());
            $em->persist($addr);

            //change join status
            $join_status=$em->getRepository('App\Entity\JoinStatus')->find(3);
            $join->setStatus($join_status);
            $em->persist($join);

            $em->flush();
        }

        return new Response(
            json_encode(array('org_id' =>$organization->getId())), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/admin/join-us/delete_multiple", name="admin_delete_multiple_join",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $join= $em->getRepository('App\Entity\JOIN')->find($id);

                $joinFiles=$em->getRepository('App\Entity\JoinFile')->findBy(array('join'=>$id));
                if ($joinFiles) {
                    foreach ($joinFiles as $join_file){
                        $em->remove($join_file);
                        $em->flush();
                    }
                }
                if ($join) {
                    try {
                        $em->remove($join);
                        $em->flush();
                        $removed = 1;
                        $message = "The Join Us has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Join Us can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }

    /**
     * @Route("/admin/join-us/convert-to-prospect", name="join_us_to_prospect",methods={"POST"})
     */
    public function convertToProspect(Request  $request){
        $em = $this->getDoctrine()->getManager();

        $id = $request->get('id');
        $join=$em->getRepository('App\Entity\JOIN')->find($id);
        if($join!=null){
            //change join status
            $join_status=$em->getRepository('App\Entity\JoinStatus')->find(2);
            $join->setStatus($join_status);
            $em->persist($join);

            $prospect=new Prospect();
            $prospect->setName($join->getName());
            $prospect->setGroupNpi($join->getNpiNumber());
            $prospect->setEmail($join->getContactEmail());
            $prospect->setPhone($join->getPhoneNumber());
            $prospect->setContactName($join->getContactName());

            $addr=new ProspectAddress();
            $addr->setPhone($join->getPhoneNumber());
            $addr->setCity($join->getCity());
            $addr->setStreet($join->getStreet());
            $addr->setSuite($join->getSuiteNumber());
            $addr->setState('FL');
            $addr->setCounty($join->getCounty());
            $em->persist($addr);
            $em->flush();

            $prospect->addAddress($addr);
            $em->persist($prospect);

            //create the new Emailqueued
            $emailQueue=new EmailQueued();
            $status=$em->getRepository('App\Entity\EmailStatus')->find(1);
            $email_type=$em->getRepository('App\Entity\EmailType')->find(13);
            $datetosend=date('m/d/Y');

            $emailQueue->setStatus($status);
            $emailQueue->setOrganization(null);
            $emailQueue->setOrganizationName($join->getName());
            $emailQueue->setEmailType($email_type);
            $emailQueue->setDateToSend(new \DateTime($datetosend));
            $emailQueue->setSentTo($join->getContactEmail());
            $emailQueue->setContact(null);
            $emailQueue->setProviders(null);
            $emailQueue->setProspect(null);

            $em->persist($emailQueue);
            $em->flush();

        }

        return new Response(
            json_encode(array('id' => $id)), 200, array('Content-Type' => 'application/json')
        );
    }
}
