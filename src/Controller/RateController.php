<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Rate;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/rate")
 */
class RateController extends AbstractController{
    /**
     * @Route("/index", name="admin_rate")
    */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $rates = $em->getRepository('App\Entity\Rate')->findAll();

            $delete_form_ajax = $this->createCustomForm('RATE_ID', 'DELETE', 'admin_delete_rate');


            return $this->render('rate/index.html.twig', array('rates' => $rates, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_rate")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('rate/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_rate", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Rate')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_rate');
            }

            return $this->render('rate/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_rate", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Rate')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_rate');
            }

            return $this->render('rate/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_rate")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $percent = $request->get('percent');
            $description = $request->get('description');
            $new = $request->get('new');

            $rate = new Rate();
            $rate->setName($name);
            $rate->setPercent($percent);
            $rate->setDescription($description);

            $em->persist($rate);
            $em->flush();

            $this->addFlash(
                'success',
                'Rate has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_rate');
            }

            return $this->redirectToRoute('admin_rate');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_rate")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $percent = $request->get('percent');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $rate = $em->getRepository('App\Entity\Rate')->find($id);

            if ($rate == null) {
                $this->addFlash(
                    "danger",
                    "The Rate can't been updated!"
                );

                return $this->redirectToRoute('admin_rate');
            }

            if ($rate != null) {
                $rate->setName($name);
                $rate->setPercent($percent);
                $rate->setDescription($description);

                $em->persist($rate);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "Rate has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_rate');
            }

            return $this->redirectToRoute('admin_rate');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_rate",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $rate = $rate = $em->getRepository('App\Entity\Rate')->find($id);
            $removed = 0;
            $message = "";

            if ($rate) {
                try {
                    $em->remove($rate);
                    $em->flush();
                    $removed = 1;
                    $message = "The Rate has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The rate can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_rate",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $rate = $rate = $em->getRepository('App\Entity\Rate')->find($id);

                if ($rate) {
                    try {
                        $em->remove($rate);
                        $em->flush();
                        $removed = 1;
                        $message = "The Rates has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Rates can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
