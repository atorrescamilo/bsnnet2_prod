<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\OrganizationStatus;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/organization_status")
 */
class OrganizationStatusController extends AbstractController{
    /**
     * @Route("/index", name="admin_organization_status")
     *
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $organization_status = $em->getRepository('App\Entity\OrganizationStatus')->findAll();

            $delete_form_ajax = $this->createCustomForm('ORGANIZATIONSTATUS_ID', 'DELETE', 'admin_delete_organization_status');

            return $this->render('organization_status/index.html.twig', array('organization_status' => $organization_status, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_organization_status")
     *
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->render('organization_status/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_organization_status", defaults={"id": null})
     *
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\OrganizationStatus')->find($id);

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('admin_organization_status');
            }

            return $this->render('organization_status/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_organization_status", defaults={"id": null})
     *
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\OrganizationStatus')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_organization_status');
            }

            return $this->render('organization_status/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_organization_status")
     *
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');

            $organization_status = new OrganizationStatus();
            $organization_status->setName($name);
            $organization_status->setDescription($description);

            $em->persist($organization_status);
            $em->flush();

            $this->addFlash(
                'success',
                'Organization Status has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_organization_status');
            }

            return $this->redirectToRoute('admin_organization_status');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_organization_status")
     *
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $organization_status = $em->getRepository('App\Entity\OrganizationStatus')->find($id);

            if ($organization_status == null) {
                $this->addFlash(
                    "danger",
                    "The Organization Status can't been updated!"
                );

                return $this->redirectToRoute('admin_organization_status');
            }

            if ($organization_status != null) {
                $organization_status->setName($name);
                $organization_status->setDescription($description);

                $em->persist($organization_status);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "The Organization Status has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_organization_status');
            }

            return $this->redirectToRoute('admin_organization_status');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_organization_status",methods={"POST","DELETE"})
     *
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $organization_status = $organization_status = $em->getRepository('App\Entity\OrganizationStatus')->find($id);
            $removed = 0;
            $message = "";

            if ($organization_status) {
                try {
                    $em->remove($organization_status);
                    $em->flush();
                    $removed = 1;
                    $message = "The Organization Status has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Organization Status can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_organization_status",methods={"POST","DELETE"})
     *
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $organization_status = $organization_status = $em->getRepository('App\Entity\OrganizationStatus')->find($id);

                if ($organization_status) {
                    try {
                        $em->remove($organization_status);
                        $em->flush();
                        $removed = 1;
                        $message = "The Role has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Role can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
