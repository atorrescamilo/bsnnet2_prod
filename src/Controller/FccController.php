<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\FCCRosterLog;
use App\Entity\HospitalAHCA;
use App\Entity\Organization;
use App\Entity\PML;
use App\Entity\PML2;
use App\Entity\Provider;
use App\Utils\My_Mcript;
use Metadata\Tests\Driver\Fixture\B\B;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader;


/**
 * @Route("/admin/fcc-report")
 */
class FccController extends AbstractController
{
    /**
     * @Route("/roster", name="fcc_roster_report")
     */
    public function roster(){
        set_time_limit(3600);

        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');
        $date=date('m/d/Y');

        $spreadsheet = $reader->load($url.'fcc_roster_template.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $em=$this->getDoctrine()->getManager();
        $providers=$em->getRepository('App\Entity\ViewProvider')->findBy(array('disabled'=>0));
        $organizations=$em->getRepository('App\Entity\ViewOrganization')->findAll();
        $cont=2;

        $pt_order=[25,26,5,68,16,4,1,66,30,29,31,32,91,39,77,67,81,97,8,14,7];
        $specialty_order=[42,43,44,66,67,905,968,300,301,302,303,304,305,306,916,904,200,201,901,966,76,177,930,929,172,173,176,177,931,174];

        foreach ($providers as $provider){
            $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>'A'));

            $address=$provider->getBillingAddress();
            $private_key=$this->getParameter('private_key');
            $encoder=new My_Mcript($private_key);

            $hospitals=$provider->getHospitals();

            $ahca_id="";
            if($hospitals!=null){
                foreach ($hospitals as $hospital){
                    $ahca_id=$hospital->getAhcaNumber();
                }
            }

            $irs="";
            $irs_type="";

            $social=$encoder->decryptthis($provider->getSocial());
            $orga=$provider->getOrganization();
            if($orga->getTinNumber()!=""){
                $irs=$orga->getTinNumber();
                $irs_type="T";
            }else{
                $irs=$social;
                $irs_type="S";
            }

            $key_cont=1;

            $isDr="";
            $degrees=$provider->getDegree();
            if($degrees!=null){
                foreach ($degrees as $degree){
                    $isDr=$degree->getName();
                }
            }

            $genderCode=$provider->getGender();
            if($genderCode!=""){
                $genderCode=substr($genderCode,0,1);
                $genderCode=strtoupper($genderCode);
            }else{
                $genderCode="";
            }

            $language1="";$language2="";$language3="";$language4="";$language5="";$language6="";$language7="";$language8="";
            $languages=$provider->getLanguages();
            if($languages!=null){
                if(count($languages)>0){
                    $language1=$languages[0]->getLangCd();
                }
                if(count($languages)>1){
                    $language2=$languages[1]->getLangCd();
                }
                if(count($languages)>2){
                    $language3=$languages[2]->getLangCd();
                }
                if(count($languages)>3){
                    $language4=$languages[3]->getLangCd();
                }
                if(count($languages)>4){
                    $language5=$languages[4]->getLangCd();
                }
                if(count($languages)>5){
                    $language6=$languages[5]->getLangCd();
                }
                if(count($languages)>6){
                    $language7=$languages[6]->getLangCd();
                }
                if(count($languages)>7){
                    $language8=$languages[7]->getLangCd();
                }
            }

            $is_valid=false;
            $provider_type="";
            $specialtiesCodes1="";
            $specialtiesCodes2="";
            $specialtiesCodes3="";
            $specialtiesCodes4="";
            $taxonomy1="";$taxonomy2="";$taxonomy3="";
            $taxonomies=array();
            $medicaids=array();
            $medicaid="";
            $ind_or_group="";
            $providerTypesArray=array();

            $specialty1 = "";
            $specialty2 = "";
            $specialty3 = "";
            $specialty4 = "";
            $specialtiesArray=array();

            if($pmls!=null){
                foreach ($pmls as $pml){
                    if($pml->getColW()=="A"){
                        $is_valid=true;
                        $pt=$pml->getColD();
                        if($pt!=""){
                            $providerTypesArray[]=$pt;
                        }
                    }

                    $spe=$pml->getColE();
                    if($spe!=""){
                        if(!in_array($spe,$specialtiesArray)){
                            $specialtiesArray[]=$spe;
                        }
                    }
                }
                $code="";
                $match_pt=0;
                foreach ($providerTypesArray as $spt){
                    foreach ($pt_order as $pt_o){
                        if($pt_o==$spt and $match_pt==0){
                            $code=$spt;
                            $match_pt=1;
                        }
                    }
                }

                if(strlen($code)==1){
                    $code="0".$code;
                }
                $provider_type=$code;

                if(count($specialtiesArray)>=1){
                    $specialty1=$specialtiesArray[0];
                }
                if(count($specialtiesArray)>=2){
                    $specialty2=$specialtiesArray[1];
                }
                if(count($specialtiesArray)>=3){
                    $specialty3=$specialtiesArray[2];
                }
                if(count($specialtiesArray)>=4){
                    $specialty4=$specialtiesArray[3];
                }

                if(count($pmls)>=1){
                    $specialtiesCodes1= $pmls[0]->getColE();
                    if(strlen($specialtiesCodes1)==2){
                        $specialtiesCodes1="0".$specialtiesCodes1;
                    }
                    if($pmls[0]->getColF()!=""){
                        $taxonomies[]=$pmls[0]->getColF();
                    }

                    $medicaids[]=$pmls[0]->getColA();
                    $ind_or_group=$pmls[0]->getColU();
                }

                if(count($pmls)>=2){
                    $specialtiesCodes2= $pmls[1]->getColE();
                    if(strlen($specialtiesCodes2)==2){
                        $specialtiesCodes2="0".$specialtiesCodes2;
                    }
                    if($pmls[1]->getColF()!=""){
                        $taxonomies[]=$pmls[1]->getColF();
                    }
                    $taxonomies[]=$pmls[1]->getColA();
                }

                if(count($pmls)>=3){
                    $specialtiesCodes3= $pmls[2]->getColE();
                    if(strlen($specialtiesCodes3)==2){
                        $specialtiesCodes3="0".$specialtiesCodes3;
                    }
                    if($pmls[2]->getColF()!=""){
                        $taxonomies[]=$pmls[2]->getColF();
                    }
                    $taxonomies[]=$pmls[2]->getColA();
                }

                if(count($pmls)>=4){
                    $specialtiesCodes4= $pmls[3]->getColE();
                    if(strlen($specialtiesCodes4)==2){
                        $specialtiesCodes4="0".$specialtiesCodes4;
                    }
                }

                if(count($taxonomies)>=1){
                    $taxonomy1=$taxonomies[0];
                }
                if(count($taxonomies)>=2){
                    $taxonomy2=$taxonomies[1];
                }
                if(count($taxonomies)>=3){
                    $taxonomy3=$taxonomies[2];
                }

                if(count($medicaids)>=1){
                    if($medicaids[0]!=""){
                        $medicaid=$medicaids[0];
                    }else{
                        if(count($medicaids)>=2){
                            if($medicaids[1]!=""){
                                $medicaid=$medicaids[1];
                            }
                        }
                    }
                }
            }

            $effectiveDate="";
            $credentialings=$em->getRepository('App\Entity\ProviderCredentialing')->findOneBy(array('credentialing_status'=>4, 'provider'=>$provider->getId()));

            if($credentialings!=null){
                $effective_date=$credentialings->getCredentialingEffectiveDate();
                if($effective_date!=""){
                    $effD=explode('/',$effective_date);

                    $effectiveDate=$effD[2].$effD[0].$effD[1];
                }
            }

            if($effectiveDate==""){
                $pro_org=$provider->getOrganization();
                if($pro_org!=null){
                    $address=$provider->getBillingAddress();
                    if($address!=null){
                        foreach ($address as $addr){
                            $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId()));
                            if($credentialingsF!=null){
                                foreach ($credentialingsF as $cf){
                                    $dateFac=$cf->getCredentialingAcceptedDate();
                                    $effD=explode('/',$dateFac);

                                    $year=$effD[2];
                                    $moth1=$effD[0];

                                    $moth='';

                                    if($moth1=='01'){
                                        $moth='02';
                                    }
                                    if($moth1=='02'){
                                        $moth='03';
                                    }
                                    if($moth1=='03'){
                                        $moth='04';
                                    }
                                    if($moth1=='04'){
                                        $moth='05';
                                    }
                                    if($moth1=='05'){
                                        $moth='06';
                                    }
                                    if($moth1=='06'){
                                        $moth='07';
                                    }
                                    if($moth1=='07'){
                                        $moth='08';
                                    }
                                    if($moth1=='08'){
                                        $moth='09';
                                    }
                                    if($moth1=='09'){
                                        $moth='10';
                                    }
                                    if($moth1=='10'){
                                        $moth='11';
                                    }
                                    if($moth1=='11'){
                                        $moth='12';
                                    }
                                    if($moth1=='12'){
                                        $moth='01';
                                        $year=intval($year)+1;
                                        $year=strval($year);
                                    }

                                    $effectiveDate=$year.$moth.'01';
                                }
                            }
                        }
                    }
                }
            }

            if($pmls!=null and $is_valid==true){
                if(true) {
                    if ($address != null) {
                        foreach ($address as $addr) {
                            $street_check = $addr->getStreet();
                            $pass = true;

                            if(stristr($license, 'ARNP')) {
                                $license=  str_replace("ARNP","APRN",$license);
                            }

                            if (strpos($street_check, 'BOX') == true or strpos($street_check, 'Box') == true or strpos($street_check, 'box') == true) {
                                $pass = false;
                            }

                            if ($pass == true) {
                                if($effectiveDate!=""){
                                    $site_index = "";
                                    if ($key_cont < 10) {
                                        $key = $provider->getNpiNumber() . "-" . '00' . $key_cont;
                                        $site_index = "00" . $key_cont;
                                    } else {
                                        $key = $provider->getNpiNumber() . "-" . '0' . $key_cont;
                                        $site_index = "0" . $key_cont;
                                    }

                                    $cell_A = 'A' . $cont;$cell_C = 'C' . $cont;$cell_D = 'D' . $cont;$cell_E = 'E' . $cont;
                                    $cell_F = 'F' . $cont;$cell_G = 'G' . $cont;$cell_H = 'H' . $cont;$cell_I = 'I' . $cont;
                                    $cell_K = 'K' . $cont;$cell_M = 'M' . $cont;$cell_O = 'O' . $cont;$cell_U = 'U' . $cont;
                                    $cell_V = 'V' . $cont;$cell_W = 'W' . $cont;$cell_X = 'X' . $cont;$cell_Y = 'Y' . $cont;
                                    $cell_Z = 'Z' . $cont;$cell_AA = 'AA' . $cont;$cell_AB = 'AB' . $cont;$cell_AC = 'AC' . $cont;
                                    $cell_AD = 'AD' . $cont;$cell_AE = 'AE' . $cont;$cell_AF = 'AF' . $cont;$cell_AG = 'AG' . $cont;
                                    $cell_AI = 'AI' . $cont;$cell_AJ = 'AJ' . $cont;$cell_AL = 'AL' . $cont;$cell_AM = 'AM' . $cont;
                                    $cell_AN = 'AN' . $cont;$cell_AO = 'AO' . $cont;$cell_AP = 'AP' . $cont;$cell_AQ = 'AQ' . $cont;
                                    $cell_AR = 'AR' . $cont;$cell_AT = 'AT' . $cont;$cell_AU = 'AU' . $cont;$cell_AW = 'AW' . $cont;
                                    $cell_AX = 'AX' . $cont;$cell_AY = 'AY' . $cont;$cell_BA = 'BA' . $cont;$cell_BB = 'BB' . $cont;
                                    $cell_BC = 'BC' . $cont;$cell_BD = 'BD' . $cont;$cell_BJ = 'BJ' . $cont;$cell_BK = 'BK' . $cont;
                                    $cell_BL = 'BL' . $cont;$cell_BM = 'BM' . $cont;$cell_BN = 'BN' . $cont;$cell_BO = 'BO' . $cont;
                                    $cell_BP = 'BP' . $cont;$cell_BQ = 'BQ' . $cont;$cell_BR = 'BR' . $cont;$cell_BS = 'BS' . $cont;
                                    $cell_BT = 'BT' . $cont;$cell_BU = 'BU' . $cont;$cell_BV = 'BV' . $cont;$cell_BW = 'BW' . $cont;
                                    $cell_BX = 'BX' . $cont;$cell_DI = 'DI' . $cont;$cell_DJ = 'DJ' . $cont;$cell_DK = 'DK' . $cont;
                                    $cell_DL = 'DL' . $cont;$cell_DN = 'DN' . $cont;


                                    $stateLic = "";
                                    if ($provider->getStateLic() != "N/A" and $provider->getStateLic() != "") {
                                        $stateLic = $provider->getStateLic();
                                    }

                                    $eClaimProviderType = "SP";

                                    $pro_degrees = $provider->getDegree();
                                    if ($pro_degrees != null) {
                                        foreach ($pro_degrees as $degree) {
                                            if ($degree->getId() == 1) {
                                                $eClaimProviderType = "NP";
                                                break;
                                            }
                                            if ($degree->getId() == 19) {
                                                $eClaimProviderType = "PA";
                                                break;
                                            }
                                        }
                                    }


                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $key);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $provider->getLastName());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $provider->getFirstName());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $isDr);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, 'Y');
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $eClaimProviderType);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $provider_type);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $specialty1);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $specialty2);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_M, $specialty3);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_O, $specialty4);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_U, $provider->getNpiNumber());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_V, $medicaid);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_W, $stateLic);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_X, $ahca_id);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_Y, $genderCode);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_Z, $language1);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AA, $language2);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AB, $language3);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AC, $language4);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AD, $language5);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AE, $language6);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AF, $language7);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AG, $language8);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AJ, $effectiveDate);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AI, $site_index);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AN, $addr->getStreet());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AO, $addr->getSuiteNumber());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AP, $addr->getCity());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AQ, $addr->getUsState());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AR, $addr->getZipCode());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AT, $addr->getPhoneNumber());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AL, $irs_type);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AM, $irs);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_BA, $ind_or_group);

                                    if ($addr->getCountyMatch() != null) {
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AU, $addr->getCountyMatch()->getCountyCode());
                                    }

                                    $ada = "N";
                                    if ($addr->getWheelchair() == true) {
                                        $ada = "Y";
                                    }

                                    $genderA = "";
                                    $genderA = $provider->getGenderAcceptance();
                                    if ($genderA == "") {
                                        $genderA = "B";
                                    } else {
                                        $genderA = strtoupper($provider->getGenderAcceptance());
                                    }

                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AW, $ada);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AX, 'Y');
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AY, $genderA);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_BB, $taxonomy1);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_BC, $taxonomy2);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_BD, $taxonomy3);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_BJ, "N");

                                    $bussinesOurs = $addr->getBusinessHours();
                                    if ($bussinesOurs != "") {
                                        $bussinesOurs = substr($bussinesOurs, 1);
                                        $bussinesOurs = substr($bussinesOurs, 0, -1);

                                        $bussinesOurs = str_replace('{', '', $bussinesOurs);
                                        $bussinesOurs = str_replace('}', '', $bussinesOurs);
                                        $bussinesOurs = str_replace('"', '', $bussinesOurs);

                                        $daysHours = explode(",", $bussinesOurs);

                                        //sunday
                                        $dayActive = explode(":", $daysHours[18])[1];
                                        $startHour = substr($daysHours[19], 9);
                                        $tillHour = substr($daysHours[20], 9);

                                        if ($dayActive == "true") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BW, strtoupper($startHour));
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BX, strtoupper($tillHour));
                                        }

                                        //monday
                                        $dayActive = explode(":", $daysHours[0])[1];
                                        $startHour = substr($daysHours[1], 9);
                                        $tillHour = substr($daysHours[2], 9);
                                        if ($dayActive == "true") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BK, strtoupper($startHour));
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BL, strtoupper($tillHour));
                                        }

                                        //tuesday
                                        $dayActive = explode(":", $daysHours[3])[1];
                                        $startHour = substr($daysHours[4], 9);
                                        $tillHour = substr($daysHours[5], 9);
                                        if ($dayActive == "true") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BM, strtoupper($startHour));
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BN, strtoupper($tillHour));
                                        }

                                        //Wednesday
                                        $dayActive = explode(":", $daysHours[6])[1];
                                        $startHour = substr($daysHours[7], 9);
                                        $tillHour = substr($daysHours[8], 9);
                                        if ($dayActive == "true") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BO, strtoupper($startHour));
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BP, strtoupper($tillHour));
                                        }

                                        //Thurs
                                        $dayActive = explode(":", $daysHours[9])[1];
                                        $startHour = substr($daysHours[10], 9);
                                        $tillHour = substr($daysHours[11], 9);
                                        if ($dayActive == "true") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BQ, strtoupper($startHour));
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BR, strtoupper($tillHour));
                                        }

                                        //Frid
                                        $dayActive = explode(":", $daysHours[12])[1];
                                        $startHour = substr($daysHours[13], 9);
                                        $tillHour = substr($daysHours[14], 9);
                                        if ($dayActive == "true") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BS, strtoupper($startHour));
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BT, strtoupper($tillHour));
                                        }

                                        //sat
                                        $dayActive = explode(":", $daysHours[15])[1];
                                        $startHour = substr($daysHours[16], 9);
                                        $tillHour = substr($daysHours[17], 9);
                                        if ($dayActive == "true") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BU, strtoupper($startHour));
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BV, strtoupper($tillHour));
                                        }
                                    }

                                    //get billing Address
                                    $orgId = null;
                                    if ($provider->getOrganization() != null) {
                                        $orgId = $provider->getOrganization()->getId();
                                    }
                                    $orgAddress = $em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $orgId));

                                    if (count($orgAddress) == 1) {
                                        foreach ($orgAddress as $addrB) {
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DI, $addrB->getStreet());
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DJ, $addrB->getCity());
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DK, $addrB->getUsState());
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DL, $addrB->getZipCode());
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DN, $addrB->getPhoneNumber());
                                        }
                                    }

                                    $street = "";
                                    $city = "";
                                    $usState = "";
                                    $zipCode = "";
                                    $phoneNumber = "";
                                    $hasBilling = false;

                                    if (count($orgAddress) > 1) {
                                        foreach ($orgAddress as $addrB) {
                                            if ($addrB->getBillingAddr() == true) {
                                                $hasBilling = true;
                                                $street = $addrB->getStreet();
                                                $city = $addrB->getCity();
                                                $usState = $addrB->getUsState();
                                                $zipCode = $addrB->getZipCode();
                                                $phoneNumber = $addrB->getPhoneNumber();
                                            }
                                        }

                                        if ($hasBilling == true) {
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DI, $street);
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DJ, $city);
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DK, $usState);
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DL, $zipCode);
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DN, $phoneNumber);
                                        }
                                    }

                                    $key_cont++;
                                    $cont++;
                                }
                            }
                        }
                    }
                }
            }
        }

        foreach ($organizations as $organization){
            $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
            $hasMedicaid=false;
            if($address!=null) {
                foreach ($address as $addr) {
                    $medicaid=$addr->getGroupMedicaid();
                    if($medicaid!=""){
                        $hasMedicaid=true;
                    }
                }
            }

            $key_cont=1;
            if($address!=null and $hasMedicaid==true){
                foreach ($address as $addr){
                    $street_check=$addr->getStreet();
                    $pass=true;

                    if(strpos($street_check,'BOX')==true or strpos($street_check,'Box')==true or strpos($street_check,'box')==true){
                        $pass=false;
                    }

                    if($pass==true){
                        $pmls=array();
                        if($addr->getLocationNpi()!=""){
                            $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$organization->getGroupNpi()));
                        }else{
                            $npiR=$organization->getGroupNpi();
                            $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$npiR,'colW'=>'A'));
                        }

                        $site_index = "";
                        $ind_or_group = "";

                        if ($key_cont < 10) {
                            $key = $organization->getGroupNpi() . "-" . '00' . $key_cont;
                            $site_index = "00" . $key_cont;
                        } else {
                            $key = $organization->getGroupNpi() . "-" . '0' . $key_cont;
                            $site_index = "0" . $key_cont;
                        }

                        $providerType = "";
                        $providerTypesArray=array();
                        $specialty1 = "";
                        $specialty2 = "";
                        $specialty3 = "";
                        $specialty4 = "";

                        $taxonomyCode1="";
                        $taxonomyCode2="";
                        $taxonomyCode3="";

                        $types=array();
                        $specialtiesArray=array();
                        $taxonomiesCode=array();

                        if(count($pmls)>0){
                            foreach ($pmls as $pml){

                                $type=$pml->getColD();
                                if($type!="" and $type!=null){
                                    $providerTypesArray[]=$type;
                                }

                                $spe=$pml->getColE();
                                $tax=$pml->getColF();

                                if($type!=""){
                                    $types[]=$type;
                                }
                                if($spe!=""){
                                    if(!in_array($spe,$specialtiesArray)){
                                        $specialtiesArray[]=$spe;
                                    }
                                }
                                if($tax!=""){
                                    if(!in_array($tax,$taxonomiesCode)){
                                        $taxonomiesCode[]=$tax;
                                    }
                                }
                            }

                            $code="";
                            $match_pt=0;
                            foreach ($providerTypesArray as $spt){
                                foreach ($pt_order as $pt_o){
                                    if($pt_o==$spt and $match_pt==0){
                                        $code=$spt;
                                        $match_pt=1;
                                    }
                                }
                            }

                            if(strlen($code)==1){
                                $code="0".$code;
                            }

                            $providerType=$code;

                            if(count($specialtiesArray)>=1){
                                $specialty1=$specialtiesArray[0];
                            }
                            if(count($specialtiesArray)>=2){
                                $specialty2=$specialtiesArray[1];
                            }
                            if(count($specialtiesArray)>=3){
                                $specialty3=$specialtiesArray[2];
                            }
                            if(count($specialtiesArray)>=4){
                                $specialty4=$specialtiesArray[3];
                            }

                            if(count($taxonomiesCode)>=1){
                                $taxonomyCode1=$taxonomiesCode[0];
                            }
                            if(count($taxonomiesCode)>=2){
                                $taxonomyCode2=$taxonomiesCode[1];
                            }
                            if(count($taxonomiesCode)>=3){
                                $taxonomyCode3=$taxonomiesCode[2];
                            }

                            $eClaimProviderType="";

                            if($organization->getHospitalAffiliations()!=""){
                                $eClaimProviderType="HS";
                            }

                            $OrgatizationClasification=$organization->getOrgSpecialties();
                            if($OrgatizationClasification){
                                foreach ($OrgatizationClasification as $orgClas){
                                    if($orgClas->getName()=="General Hospital" or $orgClas->getName()=="Hospital"){
                                        $eClaimProviderType="HS";
                                    }
                                }
                            }

                            $effectiveDate="";
                            $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                            if($address!=null){
                                foreach ($address as $addr){
                                    $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId()));
                                    if($credentialingsF!=null){
                                        foreach ($credentialingsF as $cf){
                                            $dateFac=$cf->getCredentialingAcceptedDate();
                                            $effD=explode('/',$dateFac);
                                            $effectiveDate=$effD[2].$effD[0].$effD[1];
                                        }
                                    }
                                }
                            }

                            $cell_A = 'A' . $cont;$cell_B = 'B' . $cont;$cell_C = 'C' . $cont;$cell_D = 'D' . $cont;
                            $cell_E = 'E' . $cont;$cell_F = 'F' . $cont;$cell_G = 'G' . $cont;$cell_H = 'H' . $cont;
                            $cell_I = 'I' . $cont;$cell_J = 'J' . $cont;$cell_K = 'K' . $cont;$cell_L = 'L' . $cont;
                            $cell_M = 'M' . $cont;$cell_N = 'N' . $cont;$cell_O = 'O' . $cont;$cell_P = 'P' . $cont;
                            $cell_Q = 'Q' . $cont;$cell_R = 'R' . $cont;$cell_S = 'S' . $cont;$cell_T = 'T' . $cont;$cell_U = 'U' . $cont;$cell_V = 'V' . $cont;
                            $cell_W = 'W' . $cont;$cell_X = 'X' . $cont;$cell_Y = 'Y' . $cont;$cell_Z = 'Z' . $cont;

                            $cell_AA = 'AA' . $cont;$cell_AB = 'AB' . $cont;$cell_AC = 'AC' . $cont;$cell_AD = 'AD' . $cont;
                            $cell_AE = 'AE' . $cont;$cell_AF = 'AF' . $cont;$cell_AG = 'AG' . $cont;$cell_AH = 'AH' . $cont;$cell_AI = 'AI' . $cont;$cell_AJ = 'AJ' . $cont;$cell_AK = 'AK' . $cont;
                            $cell_AL = 'AL' . $cont;$cell_AM = 'AM' . $cont;$cell_AN = 'AN' . $cont;$cell_AO = 'AO' . $cont;$cell_AP = 'AP' . $cont;
                            $cell_AQ = 'AQ' . $cont;$cell_AR = 'AR' . $cont;$cell_AS = 'AS' . $cont;$cell_AT = 'AT' . $cont;$cell_AU = 'AU' . $cont;
                            $cell_AV = 'AV' . $cont;$cell_AW = 'AW' . $cont;$cell_AX = 'AX' . $cont;$cell_AY = 'AY' . $cont;$cell_AZ = 'AZ' . $cont;

                            $cell_BA = 'BA' . $cont;$cell_BB = 'BB' . $cont;$cell_BC = 'BC' . $cont;$cell_BD = 'BD' . $cont;$cell_BE = 'BE' . $cont;
                            $cell_BF = 'BF' . $cont;$cell_BG = 'BG' . $cont;$cell_BH = 'BH' . $cont;$cell_BI = 'BI' . $cont;$cell_BJ = 'BJ' . $cont;
                            $cell_BK = 'BK' . $cont;$cell_BL = 'BL' . $cont;$cell_BM = 'BM' . $cont;$cell_BN = 'BN' . $cont;$cell_BO = 'BO' . $cont;
                            $cell_BP = 'BP' . $cont;$cell_BQ = 'BQ' . $cont;$cell_BR = 'BR' . $cont;$cell_BS = 'BS' . $cont;$cell_BT = 'BT' . $cont;
                            $cell_BU = 'BU' . $cont;$cell_BV = 'BV' . $cont;$cell_BW = 'BW' . $cont;$cell_BX = 'BX' . $cont;$cell_BY = 'BY' . $cont;$cell_BZ = 'BZ' . $cont;

                            $cell_CA = 'CA' . $cont;$cell_CB = 'CB' . $cont;
                            $cell_CC = 'CC' . $cont;$cell_CD = 'CD' . $cont;$cell_CE = 'CE' . $cont;$cell_CF = 'CF' . $cont;
                            $cell_CG = 'CG' . $cont;$cell_CH = 'CH' . $cont;$cell_CI = 'CI' . $cont;$cell_CJ = 'CJ' . $cont;
                            $cell_CK = 'CK' . $cont;$cell_CL = 'CL' . $cont;$cell_CM = 'CM' . $cont;$cell_CN = 'CN' . $cont;
                            $cell_CO = 'CO' . $cont;$cell_CP = 'CP' . $cont;$cell_CQ = 'CQ' . $cont;$cell_CR = 'CR' . $cont;
                            $cell_CS = 'CS' . $cont;$cell_CT = 'CT' . $cont;$cell_CU = 'CU' . $cont;$cell_CV = 'CV' . $cont;
                            $cell_CW = 'CW' . $cont;$cell_CX = 'CX' . $cont;$cell_CY = 'CY' . $cont;$cell_CZ = 'CZ' . $cont;

                            $cell_DA = 'DA' . $cont;$cell_DB = 'DB' . $cont;$cell_DC = 'DC' . $cont;$cell_DD = 'DD' . $cont;$cell_DE = 'DE' . $cont;
                            $cell_DF = 'DF' . $cont;$cell_DG = 'DG' . $cont;$cell_DH = 'DH' . $cont;$cell_DI = 'DI' . $cont;$cell_DJ = 'DJ' . $cont;$cell_DK = 'DK' . $cont;$cell_DL = 'DL' . $cont;$cell_DM = 'DM' . $cont;
                            $cell_DN = 'DN' . $cont;$cell_DO = 'DO' . $cont;$cell_DP = 'DP' . $cont;

                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, $key);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_G, $eClaimProviderType);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI, $site_index);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ, $effectiveDate);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, strtoupper($organization->getName()));
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, 'N');
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_U, $organization->getGroupNpi());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_V, $addr->getGroupMedicaid());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_H, $providerType);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $specialty1);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $specialty2);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $specialty3);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, $specialty4);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, 'O');
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AY, 'BOTH');

                            $languagesId = $organization->getLanguagesId();
                            $language1 = "";
                            $language2 = "";
                            $language3 = "";
                            $language4 = "";
                            $language5 = "";
                            $language6 = "";
                            $language7 = "";
                            $language8 = "";
                            $languagesArray = array();
                            if ($languagesId != null) {
                                $ids = explode(',', $languagesId);
                                foreach ($ids as $id) {
                                    $language = $em->getRepository('App\Entity\Languages')->find($id);
                                    if ($language != null) {
                                        $languagesArray[] = $language;
                                    }
                                }
                            }

                            if ($languagesArray != null) {
                                if (count($languagesArray) > 0) {
                                    $language1 = $languagesArray[0]->getLangCd();
                                }
                                if (count($languagesArray) > 1) {
                                    $language2 = $languagesArray[1]->getLangCd();
                                }
                                if (count($languagesArray) > 2) {
                                    $language3 = $languagesArray[2]->getLangCd();
                                }
                                if (count($languagesArray) > 3) {
                                    $language4 = $languagesArray[3]->getLangCd();
                                }
                                if (count($languagesArray) > 4) {
                                    $language5 = $languagesArray[4]->getLangCd();
                                }
                                if (count($languagesArray) > 5) {
                                    $language6 = $languagesArray[5]->getLangCd();
                                }
                                if (count($languagesArray) > 6) {
                                    $language7 = $languagesArray[6]->getLangCd();
                                }
                                if (count($languagesArray) > 7) {
                                    $language8 = $languagesArray[7]->getLangCd();
                                }
                            }

                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_Z, $language1);
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AA, $language2);
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AB, $language3);
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AC, $language4);
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AD, $language5);
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AE, $language6);
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AF, $language7);
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AG, $language8);

                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AN, $addr->getStreet());
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AO, $addr->getSuiteNumber());
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AP, $addr->getCity());
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AQ, $addr->getUsState());
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AR, $addr->getZipCode());
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AT, $addr->getPhoneNumber());

                            if ($addr->getCountyMatch() != null) {
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AU, $addr->getCountyMatch()->getCountyCode());
                            }

                            $ada = "N";
                            if ($addr->getWheelchair() == true) {
                                $ada = "Y";
                            }

                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AW, $ada);
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AX, 'Y');
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BB, $taxonomyCode1);
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BC, $taxonomyCode2);
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BD, $taxonomyCode3);

                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BJ, "N");

                            $bussinesOurs = $addr->getBusinessHours();
                            if($bussinesOurs!=""){
                                $bussinesOurs = substr($bussinesOurs, 1);
                                $bussinesOurs = substr($bussinesOurs, 0, -1);

                                $bussinesOurs = str_replace('{', '', $bussinesOurs);
                                $bussinesOurs = str_replace('}', '', $bussinesOurs);
                                $bussinesOurs = str_replace('"', '', $bussinesOurs);

                                $daysHours = explode(",", $bussinesOurs);

                                //sunday
                                $dayActive = explode(":", $daysHours[18])[1];
                                $startHour = substr($daysHours[19], 9);
                                $tillHour = substr($daysHours[20], 9);

                                if ($dayActive == "true") {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BW, strtoupper($startHour));
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BX, strtoupper($tillHour));
                                }

                                //monday
                                $dayActive = explode(":", $daysHours[0])[1];
                                $startHour = substr($daysHours[1], 9);
                                $tillHour = substr($daysHours[2], 9);
                                if ($dayActive == "true") {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BK, strtoupper($startHour));
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BL, strtoupper($tillHour));
                                }

                                //tuesday
                                $dayActive = explode(":", $daysHours[3])[1];
                                $startHour = substr($daysHours[4], 9);
                                $tillHour = substr($daysHours[5], 9);
                                if ($dayActive == "true") {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BM, strtoupper($startHour));
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BN, strtoupper($tillHour));
                                }

                                //Wednesday
                                $dayActive = explode(":", $daysHours[6])[1];
                                $startHour = substr($daysHours[7], 9);
                                $tillHour = substr($daysHours[8], 9);
                                if ($dayActive == "true") {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BO, strtoupper($startHour));
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BP, strtoupper($tillHour));
                                }

                                //Thurs
                                $dayActive = explode(":", $daysHours[9])[1];
                                $startHour = substr($daysHours[10], 9);
                                $tillHour = substr($daysHours[11], 9);
                                if ($dayActive == "true") {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BQ, strtoupper($startHour));
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BR, strtoupper($tillHour));
                                }

                                //Frid
                                $dayActive = explode(":", $daysHours[12])[1];
                                $startHour = substr($daysHours[13], 9);
                                $tillHour = substr($daysHours[14], 9);
                                if ($dayActive == "true") {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BS, strtoupper($startHour));
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BT, strtoupper($tillHour));
                                }

                                //sat
                                $dayActive = explode(":", $daysHours[15])[1];
                                $startHour = substr($daysHours[16], 9);
                                $tillHour = substr($daysHours[17], 9);
                                if ($dayActive == "true") {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BU, strtoupper($startHour));
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BV, strtoupper($tillHour));
                                }
                            }

                            //get billing Address
                            $orgAddress = $em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $organization->getId()));

                            if (count($orgAddress) == 1) {
                                foreach ($orgAddress as $addrB) {
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_DI, $addrB->getStreet());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_DJ, $addrB->getCity());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_DK, $addrB->getUsState());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_DL, $addrB->getZipCode());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_DN, $addrB->getPhoneNumber());
                                }
                            }

                            $street = "";
                            $city = "";
                            $usState = "";
                            $zipCode = "";
                            $phoneNumber = "";
                            $hasBilling = false;

                            if (count($orgAddress) > 1) {
                                foreach ($orgAddress as $addrB) {
                                    if ($addrB->getBillingAddr() == true) {
                                        $hasBilling = true;
                                        $street = $addrB->getStreet();
                                        $city = $addrB->getCity();
                                        $usState = $addrB->getUsState();
                                        $zipCode = $addrB->getZipCode();
                                        $phoneNumber = $addrB->getPhoneNumber();
                                    }
                                }

                                if ($hasBilling == true) {
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_DI, $street);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_DJ, $city);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_DK, $usState);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_DL, $zipCode);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_DN, $phoneNumber);
                                }
                            }

                            $tin = $organization->getTinNumber();

                            if ($tin != "") {
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AL, 'T');
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AM, $tin);
                            }

                            $cont++;
                            $key_cont++;
                        }
                    }
                }
            }
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="fcc_roster-report.xls"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/laod-ahca-id", name="fcc_roster_loadahca-id")
     */
    public function loadahcaID(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "template_xls/FCC_PNV_Files_PRD.xls";

        $reader = new Reader\Xls();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=0;

        foreach ($sheetData as $sheet) {
            if ($cont >= 1) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    $npi=$sheet['G'];
                    $ahca_id=$sheet['L'];

                    if($npi!="" and $ahca_id!=""){
                        $tofirsth_digits=substr($ahca_id,0,2);
                        if($tofirsth_digits=="00"){
                            $ahca_id=substr($ahca_id,2);
                        }

                        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('npi_number'=>$npi));

                        if($providers!=null){
                            foreach ($providers as $provider){
                                $hospital=$em->getRepository('App\Entity\HospitalAHCA')->findOneBy(array('ahca_number'=>$ahca_id));

                                if($hospital!=null){
                                    $provider->addHospital($hospital);
                                    $em->persist($provider);
                                    $em->flush();
                                }
                            }
                        }
                    }
                }
            }
            $cont++;
        }

        die();
    }

    /**
     * @Route("/laod-hospitals", name="fcc_roster_loadahca-hospitals")
     */
    public function loadahcaHospital(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "template_xls/FloridaHealthFinder.xls";

        $reader = new Reader\Xls();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=0;

        foreach ($sheetData as $sheet) {
            if ($cont > 0) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    $hospital=new HospitalAHCA();
                    $hospital->setAhcaNumber($sheet['A']);
                    $hospital->setLicenceNumber($sheet['B']);
                    $hospital->setLicenceEffectiveDate($sheet['C']);
                    $hospital->setName($sheet['F']);
                    $hospital->setStreetAddress($sheet['G']);
                    $hospital->setSuiteNumber($sheet['H']);
                    $hospital->setCity($sheet['I']);
                    $hospital->setState($sheet['J']);
                    $hospital->setZipcode($sheet['K']);
                    $hospital->setCounty($sheet['L']);
                    $hospital->setPhone($sheet['M']);
                    $hospital->setWeb($sheet['Z']);

                    $em->persist($hospital);
                    $em->flush();
                }
            }
            $cont++;
        }

        die();
    }

    /**
     * @Route("/set-pml2", name="set-pml2")
     */
    public function setPML2(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App\Entity\Organization')->findBy(array('disabled'=>0));

        foreach ($organizations as $organization){
            $npi=$organization->getGroupNpi();
            if($npi!=""){
                $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$npi));

                if($pmls!=null){
                    foreach ($pmls as $pml){
                        $pml2=new PML2();
                        $pml2->setColA($pml->getColA());
                        $pml2->setColB($pml->getColB());
                        $pml2->setColC($pml->getColC());
                        $pml2->setColD($pml->getColD());
                        $pml2->setColE($pml->getColE());
                        $pml2->setColF($pml->getColF());
                        $pml2->setColG($pml->getColG());
                        $pml2->setColH($pml->getColH());
                        $pml2->setColI($pml->getColI());
                        $pml2->setColJ($pml->getColJ());
                        $pml2->setColK($pml->getColK());
                        $pml2->setColL($pml->getColL());
                        $pml2->setColM($pml->getColM());
                        $pml2->setColN($pml->getColN());
                        $pml2->setColO($pml->getColO());
                        $pml2->setColP($pml->getColP());
                        $pml2->setColQ($pml->getColQ());
                        $pml2->setColR($pml->getColR());
                        $pml2->setColS($pml->getColS());
                        $pml2->setColT($pml->getColT());
                        $pml2->setColU($pml->getColU());
                        $pml2->setColV($pml->getColV());
                        $pml2->setColW($pml->getColW());
                        $pml2->setColX($pml->getColX());
                        $pml2->setColY($pml->getColY());

                        $em->persist($pml2);
                    }
                    $em->flush();

                }
            }
        }

        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));

        foreach ($providers as $provider){
            $npi=$provider->getNpiNumber();

            if($npi!="" and $npi!="NOT AVAILABLE"){
                $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$npi));

                if($pmls!=null){
                    foreach ($pmls as $pml){
                        $pml2=new PML2();
                        $pml2->setColA($pml->getColA());
                        $pml2->setColB($pml->getColB());
                        $pml2->setColC($pml->getColC());
                        $pml2->setColD($pml->getColD());
                        $pml2->setColE($pml->getColE());
                        $pml2->setColF($pml->getColF());
                        $pml2->setColG($pml->getColG());
                        $pml2->setColH($pml->getColH());
                        $pml2->setColI($pml->getColI());
                        $pml2->setColJ($pml->getColJ());
                        $pml2->setColK($pml->getColK());
                        $pml2->setColL($pml->getColL());
                        $pml2->setColM($pml->getColM());
                        $pml2->setColN($pml->getColN());
                        $pml2->setColO($pml->getColO());
                        $pml2->setColP($pml->getColP());
                        $pml2->setColQ($pml->getColQ());
                        $pml2->setColR($pml->getColR());
                        $pml2->setColS($pml->getColS());
                        $pml2->setColT($pml->getColT());
                        $pml2->setColU($pml->getColU());
                        $pml2->setColV($pml->getColV());
                        $pml2->setColW($pml->getColW());
                        $pml2->setColX($pml->getColX());
                        $pml2->setColY($pml->getColY());

                        $em->persist($pml2);
                    }
                    $em->flush();
                }
            }

        }

        $address=$em->getRepository('App\Entity\BillingAddress')->findAll();

        foreach ($address as $addr){
            $npi=$addr->getLocationNpi();
            $org=$addr->getOrganization();
            $orgNPI=$org->getGroupNpi();
            if($npi!=$orgNPI){
                if($npi!=""){
                    $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$npi));
                    if($pmls!=null){
                        foreach ($pmls as $pml){
                            $pml2=new PML2();
                            $pml2->setColA($pml->getColA());
                            $pml2->setColB($pml->getColB());
                            $pml2->setColC($pml->getColC());
                            $pml2->setColD($pml->getColD());
                            $pml2->setColE($pml->getColE());
                            $pml2->setColF($pml->getColF());
                            $pml2->setColG($pml->getColG());
                            $pml2->setColH($pml->getColH());
                            $pml2->setColI($pml->getColI());
                            $pml2->setColJ($pml->getColJ());
                            $pml2->setColK($pml->getColK());
                            $pml2->setColL($pml->getColL());
                            $pml2->setColM($pml->getColM());
                            $pml2->setColN($pml->getColN());
                            $pml2->setColO($pml->getColO());
                            $pml2->setColP($pml->getColP());
                            $pml2->setColQ($pml->getColQ());
                            $pml2->setColR($pml->getColR());
                            $pml2->setColS($pml->getColS());
                            $pml2->setColT($pml->getColT());
                            $pml2->setColU($pml->getColU());
                            $pml2->setColV($pml->getColV());
                            $pml2->setColW($pml->getColW());
                            $pml2->setColX($pml->getColX());
                            $pml2->setColY($pml->getColY());

                            $em->persist($pml2);
                        }
                        $em->flush();
                    }
                }
            }

        }

        return new Response('OK');
    }

    /**
     * @Route("/disruptor-report", name="fcc_disruptor_report")
     */
    public function disruptorReport(){
        set_time_limit(3600);

        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');;

        $spreadsheet = $reader->load($url.'fcc_roster_template.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $em=$this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $providers=$em->getRepository('App\Entity\ViewProvider')->findBy(array('disabled'=>0,'valid_npi_number'=>1));
        $organizations=$em->getRepository('App\Entity\ViewOrganization')->findBy(array('disabled'=>0,'organization_status'=>2));
        $cont=2;

        $organizations_result=[];
        $pt_order=['25','26','30','29','31','07','32','81','97','01','04','16','68','66','05','77','67','08','14','25','26','30','29','31','07','32','81','97'];

        $exclude_npis=[];
        $exclude_ids=[]; //providers excludes on a organization
        $sql="SELECT p.provider_id, p.payer_id, provider.npi_number as npi
            FROM  provider_payer_exclude p
            LEFT JOIN provider ON provider.id = p.provider_id";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $excludes_p_ids= $stmt->fetchAll();

        foreach ($excludes_p_ids as $ids_excludes){
            $exclude_npis[]=$ids_excludes['npi'];
        }

        $providerNPIS=[];
        //get exclude organization
        $sql2="SELECT organization.id as organization_id
            FROM  organization_payer_exclude o
            LEFT JOIN organization ON organization.id = o.organization_id
            LEFT JOIN payer ON payer.id = o.payer_id
            WHERE payer.id=4";

        $stmt2 = $conn->prepare($sql2);
        $stmt2->execute();
        $excludes_o_ids= $stmt2->fetchAll();

        foreach ($excludes_o_ids as $org_id){
            $providers_org=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$org_id['organization_id']));
            if($providers_org!=null){
                foreach ($providers_org as $provider){
                    $exclude_ids[]=$provider->getId();
                }
            }
        }

        foreach ($providers as $provider){
            $npi_number=$provider->getNpiNumber();
            $id=$provider->getId();
            if(!in_array($id,$exclude_ids) and $npi_number!="N/A" and $npi_number!="" and $npi_number!="NOT AVAILABLE" and in_array($npi_number,$providerNPIS)==false and !in_array($npi_number,$exclude_npis) ){
                $providerNPIS[]=$npi_number;
                $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>'A'));
                $pareffdt="";

                $pro_org=$provider->getOrganization();
                $contCredF=0;
                if($pro_org!=null){
                    $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$pro_org->getId()));
                    if($address!=null){
                        foreach ($address as $addr){
                            $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId()));
                            if($credentialingsF!=null){
                                foreach ($credentialingsF as $cf){
                                    $contCredF++;
                                }
                            }
                        }
                    }
                }

                $is_credentialing=false;

                $credRecord=$em->getRepository('App\Entity\ProviderCredentialing')->findOneBy(array('credentialing_status'=>4, 'provider'=>$provider->getId()));
                if($credRecord!=null){
                    $effective_date=$credRecord->getCredentialingEffectiveDate();
                    if($effective_date!=""){
                        $effD=explode('/',$effective_date);
                        $pareffdt=$effD[2].$effD[0].'01';
                    }

                    $is_credentialing=true;
                }

                if($contCredF>0 and $credRecord==null){
                    $is_credentialing=true;
                    $pro_org=$provider->getOrganization();
                    if($pro_org!=null){
                        $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$pro_org->getId()));
                        if($address!=null){
                            foreach ($address as $addr){
                                $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId(),'credentialing_status'=>4));
                                if($credentialingsF!=null){
                                    foreach ($credentialingsF as $cf){
                                        $dateFacEffe=$cf->getCredentialingEffectiveDate();

                                        if($dateFacEffe!=""){
                                            $dateFacEffe=explode('/',$dateFacEffe);
                                            $year=$dateFacEffe[2];
                                            $moth=$dateFacEffe[0];

                                            $pareffdt=$year.$moth.'01';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $address=$provider->getBillingAddress();
                $private_key=$this->getParameter('private_key');
                $encoder=new My_Mcript($private_key);
                $hospitals=$provider->getHospitals();

                $ahca_id="";
                if($hospitals!=null){
                    foreach ($hospitals as $hospital){
                        $ahca_id=$hospital->getAhcaNumber();
                    }
                }

                $social=$encoder->decryptthis($provider->getSocial());
                $orga=$provider->getOrganization();
                if($orga->getTinNumber()!=""){
                    $irs=$orga->getTinNumber();
                    $irs_type="T";
                }else{
                    $irs=$social;
                    $irs_type="S";
                }

                $irs=str_replace("-","",$irs);
                if(strlen($irs)==8){
                    $irs="0".$irs;
                }

                $key_cont=1;

                $isDr="";
                $degrees=$provider->getDegree();
                if($degrees!=null){
                    foreach ($degrees as $degree){
                        $isDr=$degree->getName();
                    }
                }

                $genderCode=$provider->getGender();
                if($genderCode!=""){
                    $genderCode=substr($genderCode,0,1);
                    $genderCode=strtoupper($genderCode);
                }else{
                    $genderCode="";
                }

                $language1="";$language2="";$language3="";$language4="";$language5="";$language6="";$language7="";$language8="";
                $languages=$provider->getLanguages();
                if($languages!=null){
                    if(count($languages)>0){
                        $language1=$languages[0]->getLangCd();
                    }
                    if(count($languages)>1){
                        $language2=$languages[1]->getLangCd();
                    }
                    if(count($languages)>2){
                        $language3=$languages[2]->getLangCd();
                    }
                    if(count($languages)>3){
                        $language4=$languages[3]->getLangCd();
                    }
                    if(count($languages)>4){
                        $language5=$languages[4]->getLangCd();
                    }
                    if(count($languages)>5){
                        $language6=$languages[5]->getLangCd();
                    }
                    if(count($languages)>6){
                        $language7=$languages[6]->getLangCd();
                    }
                    if(count($languages)>7){
                        $language8=$languages[7]->getLangCd();
                    }
                }

                if($language1==""){
                    $language1="ENG";
                }

                $is_valid=false;
                $provider_type="";
                $taxonomy1="";$taxonomy2="";$taxonomy3="";
                $taxonomies=array();
                $medicaids=array();
                $medicaid="";
                $ind_or_group="";
                $providerTypesArray=array();

                $specialty1 = "";
                $specialty2 = "";
                $specialty3 = "";
                $specialty4 = "";
                $specialtiesArray=array();
                $stateLic="";
                $code="";

                if($pmls!=null){
                    foreach ($pmls as $pml){
                        if($pml->getColW()=="A"){
                            $is_valid=true;
                            $pt=$pml->getColD();
                            if($pt!=""){
                                $providerTypesArray[]=$pt;
                                $stateLic=$pml->getColV();
                            }
                        }

                        $spe=$pml->getColE();
                        if($spe!=""){
                            if(!in_array($spe,$specialtiesArray)){
                                $specialtiesArray[]=$spe;
                            }
                        }
                    }

                    $match_pt=0;
                    foreach ($providerTypesArray as $spt){
                        foreach ($pt_order as $pt_o){
                            if($pt_o==$spt and $match_pt==0){
                                $code=$spt;
                                $match_pt=1;
                                break 2;
                            }
                        }
                    }

                    if(strlen($code)==1){
                        $code="00".$code;
                    }

                    if(strlen($code)==2){
                        $code="0".$code;
                    }
                    $provider_type=$code;

                    if(count($specialtiesArray)>=1){
                        $specialty1=$specialtiesArray[0]."01";
                    }
                    if(count($specialtiesArray)>=2){
                        $specialty2=$specialtiesArray[1]."01";
                    }
                    if(count($specialtiesArray)>=3){
                        $specialty3=$specialtiesArray[2]."01";
                    }
                    if(count($specialtiesArray)>=4){
                        $specialty4=$specialtiesArray[3]."01";
                    }

                    if(count($pmls)>=1){
                        $specialtiesCodes1= $pmls[0]->getColE();
                        if(strlen($specialtiesCodes1)==2){
                            $specialtiesCodes1="0".$specialtiesCodes1;
                        }
                        if($pmls[0]->getColF()!=""){
                            $taxonomies[]=$pmls[0]->getColF();
                        }

                        $medicaids[]=$pmls[0]->getColA();
                        $ind_or_group=$pmls[0]->getColU();
                    }

                    if(count($pmls)>=2){
                        $specialtiesCodes2= $pmls[1]->getColE();
                        if(strlen($specialtiesCodes2)==2){
                            $specialtiesCodes2="0".$specialtiesCodes2;
                        }
                        if($pmls[1]->getColF()!=""){
                            $taxonomies[]=$pmls[1]->getColF();
                        }
                        $medicaids[]=$pmls[1]->getColA();
                    }

                    if(count($pmls)>=3){
                        $specialtiesCodes3= $pmls[2]->getColE();
                        if(strlen($specialtiesCodes3)==2){
                            $specialtiesCodes3="0".$specialtiesCodes3;
                        }
                        if($pmls[2]->getColF()!=""){
                            $taxonomies[]=$pmls[2]->getColF();
                        }
                        $medicaids[]=$pmls[2]->getColA();
                    }

                    if(count($pmls)>=4){
                        $specialtiesCodes4= $pmls[3]->getColE();
                        if(strlen($specialtiesCodes4)==2){
                            $specialtiesCodes4="0".$specialtiesCodes4;
                        }
                    }

                    if(count($taxonomies)>=1){
                        $taxonomy1=$taxonomies[0];
                    }
                    if(count($taxonomies)>=2){
                        $taxonomy2=$taxonomies[1];
                    }
                    if(count($taxonomies)>=3){
                        $taxonomy3=$taxonomies[2];
                    }

                    if($taxonomy1==""){
                        $pro_taxonomies=$provider->getTaxonomyCodes();
                        if($pro_taxonomies!=null){
                            if(count($pro_taxonomies)>=1){
                                $taxonomy1=$pro_taxonomies[0]->getCode();
                            }
                            if(count($pro_taxonomies)>=2){
                                $taxonomy1=$pro_taxonomies[1]->getCode();
                            }
                            if(count($pro_taxonomies)>=3){
                                $taxonomy1=$pro_taxonomies[2]->getCode();
                            }
                        }
                    }

                    if(count($medicaids)>=1){
                        if($medicaids[0]!=""){
                            $medicaid=$medicaids[0];
                        }else{
                            if(count($medicaids)>=2){
                                if($medicaids[1]!=""){
                                    $medicaid=$medicaids[1];
                                }
                            }
                        }
                    }
                }

                if($pmls!=null and $is_valid==true and $is_credentialing==true){
                    $degrees=$provider->getDegree();
                    $degreePass=0;
                    if($degrees!=null){
                        foreach ($degrees as $degree){
                            if($degree->getId()==20 or $degree->getId()==24 or $degree->getId()==22 or $degree->getId()==30 or $degree->getId()==32){
                                $degreePass=1;
                            }
                        }
                    }

                    if($address!=null and $degreePass==0){
                        foreach ($address as $addr){
                            $street_check=$addr->getStreet();
                            $addr_pass=true;
                            if(stristr($street_check, 'BOX') or stristr($street_check, 'Box') or stristr($street_check, 'box')) {
                                $addr_pass=false;
                            }
                            if($addr_pass==true){
                                $eClaimProviderType = "SP";
                                $pro_degrees = $provider->getDegree();
                                if ($pro_degrees != null) {
                                    foreach ($pro_degrees as $degree) {
                                        if ($degree->getId() == 1) {
                                            $eClaimProviderType = "NP";
                                            break;
                                        }
                                        if ($degree->getId() == 19) {
                                            if($specialty1!='92901' and $specialty2!='92901' and $specialty3!='92901' and $specialty4!='92901'){
                                                $eClaimProviderType = "PA";
                                                break;
                                            }
                                        }
                                    }
                                }

                                if($key_cont<10){
                                    $key=$provider->getNpiNumber()."-".'00'.$key_cont;
                                    $site_index="00".$key_cont;
                                }else{
                                    $key=$provider->getNpiNumber()."-".'0'.$key_cont;
                                    $site_index="0".$key_cont;
                                }

                                $cell_A='A'.$cont;$cell_F='F'.$cont;$cell_E='E'.$cont;$cell_C='C'.$cont;
                                $cell_G='G'.$cont;$cell_H='H'.$cont;$cell_I='I'.$cont;$cell_J='J'.$cont;$cell_K='K'.$cont;$cell_L='L'.$cont;$cell_M='M'.$cont;
                                $cell_N='N'.$cont;$cell_O='O'.$cont;$cell_P='P'.$cont;$cell_Q='Q'.$cont;$cell_R='R'.$cont;$cell_S='S'.$cont;$cell_T='T'.$cont;
                                $cell_U='U'.$cont;$cell_V='V'.$cont;$cell_W='W'.$cont;$cell_X='X'.$cont;$cell_Y='Y'.$cont;$cell_Z='Z'.$cont;

                                $cell_AA='AA'.$cont;$cell_AB='AB'.$cont;$cell_AC='AC'.$cont;$cell_AD='AD'.$cont;$cell_AE='AE'.$cont;
                                $cell_AF='AF'.$cont;$cell_AG='AG'.$cont;$cell_AH='AH'.$cont;$cell_AI='AI'.$cont;$cell_AJ='AJ'.$cont;
                                $cell_AK='AK'.$cont;$cell_AL='AL'.$cont;$cell_AM='AM'.$cont;$cell_AN='AN'.$cont;$cell_AO='AO'.$cont;
                                $cell_AP='AP'.$cont;$cell_AQ='AQ'.$cont;$cell_AR='AR'.$cont;$cell_AS='AS'.$cont;$cell_AT='AT'.$cont;
                                $cell_AU='AU'.$cont;$cell_AV='AV'.$cont;$cell_AW='AW'.$cont;$cell_AX='AX'.$cont;$cell_AY='AY'.$cont;$cell_AZ='AZ'.$cont;

                                $cell_BA='BA'.$cont;$cell_BB='BB'.$cont;$cell_BC='BC'.$cont;$cell_BD='BD'.$cont;$cell_BE='BE'.$cont;$cell_BF='BF'.$cont;
                                $cell_BG='BG'.$cont;$cell_BH='BH'.$cont;$cell_BI='BI'.$cont;$cell_BJ='BJ'.$cont;$cell_BK='BK'.$cont;$cell_BL='BL'.$cont;
                                $cell_BM='BM'.$cont;$cell_BN='BN'.$cont;$cell_BO='BO'.$cont;$cell_BP='BP'.$cont;$cell_BQ='BQ'.$cont;$cell_BR='BR'.$cont;
                                $cell_BS='BS'.$cont;$cell_BT='BT'.$cont;$cell_BU='BU'.$cont;$cell_BV='BV'.$cont;$cell_BW='BW'.$cont;$cell_BX='BX'.$cont;
                                $cell_BY='BY'.$cont;$cell_BZ='BZ'.$cont;

                                $cell_CA='CA'.$cont;$cell_CB='CB'.$cont;$cell_CC='CC'.$cont;$cell_CD='CD'.$cont;$cell_CE='CE'.$cont;$cell_CF='CF'.$cont;
                                $cell_CG='CG'.$cont;$cell_CH='CH'.$cont;$cell_CI='CI'.$cont;$cell_CJ='CJ'.$cont;$cell_CK='CK'.$cont;$cell_CL='CL'.$cont;
                                $cell_CM='CM'.$cont;$cell_CN='CN'.$cont;$cell_CO='CO'.$cont;$cell_CP='CP'.$cont;$cell_CQ='CQ'.$cont;$cell_CR='CR'.$cont;
                                $cell_CS='CS'.$cont;$cell_CT='CT'.$cont;$cell_CU='CU'.$cont;$cell_CV='CV'.$cont;$cell_CW='CW'.$cont;$cell_CX='CX'.$cont;
                                $cell_CY='CY'.$cont;$cell_CZ='CZ'.$cont;

                                $cell_DA='DA'.$cont;$cell_DB='DB'.$cont;$cell_DC='DC'.$cont;$cell_DD='DD'.$cont;$cell_DE='DE'.$cont;$cell_DF='DF'.$cont;
                                $cell_DG='DG'.$cont;$cell_DH='DH'.$cont;$cell_DI='DI'.$cont;$cell_DJ='DJ'.$cont;$cell_DK='DK'.$cont;$cell_DL='DL'.$cont;
                                $cell_DM='DM'.$cont;$cell_DN='DN'.$cont;$cell_DO='DO'.$cont;$cell_DP='DP'.$cont;$cell_DQ='DQ'.$cont;$cell_DR='DR'.$cont;
                                $cell_DT='DT'.$cont;$cell_DU='DU'.$cont;;$cell_DV='DV'.$cont;$cell_DY='DY'.$cont;$cell_DZ='DZ'.$cont;

                                $minAge=strval($addr->getOfficeAgeLimit());
                                $maxAge=strval($addr->getOfficeAgeLimitMax());

                                if(strlen($minAge)==1){
                                    $minAge='0'.$minAge;
                                }
                                $minAge=$minAge."Y";

                                if(strlen($maxAge)==1){
                                    $maxAge='0'.$maxAge;
                                }
                                $maxAge=$maxAge."Y";

                                $org_id=$provider->getOrganization()->getId();
                                $medicaid_percent="100";
                                $org_rate=$em->getRepository('App\Entity\OrganizationRates')->findOneBy(array('organization'=>$org_id,'rate'=>2));
                                if($org_rate!=null){
                                    $medicaid_percent=$org_rate->getPercent();
                                }
                                $medicaid_percent=$medicaid_percent."%";

                                $hasWeekend="N";
                                $hasEvening="N";
                                $exempt="";
                                if($addr->getSeePatientsAddr()==false){
                                    $exempt="DE";
                                }

                                $organizationId=$provider->getOrganization();
                                if(!in_array($organizationId,$organizations_result)){
                                    $organizations_result[]=$organizationId;
                                }

                                $phone_number = $addr->getPhoneNumber();
                                $phone_number = str_replace('-', "", $phone_number);

                                $license=$provider->getStateLic();
                                if($license=="not found" or $license=="NOTFOUND"){ $license=""; }
                                $license=str_replace(" ","",$license);

                                if($license==""){
                                    if($provider_type=="032"){
                                        $license="99999";
                                    }
                                }

                                $license=strval($license);

                                $license_match=false;
                                $cont_license=0;
                                foreach ($pml as $pt) {
                                    if($pt->getColV()!="" and $pt->getColV()!=null){
                                        $license_pml=$pt->getColV();
                                        $cont_license++;
                                        if($license_pml==$license){
                                            $license_match=true;
                                        }
                                    }
                                }

                                if(stristr($license, 'ARNP')) {
                                    $license=  str_replace("ARNP","APRN",$license);
                                }

                                $has_rbt=false;
                                if(stristr($license, 'RBT')) {
                                    $has_rbt=true;
                                }

                                $first_latter_license=substr($license,0,1);
                                $not_match_pml=0;
                                if($license=="" and $cont_license>0){
                                    if($provider_type=="039"){
                                        $not_match_pml=1;
                                    }
                                }

                                if($license!="" and $license_match==false){
                                   if($provider_type=="039"){
                                        $not_match_pml=1;
                                    }
                                }

                                if($first_latter_license!="I" and $first_latter_license!="i" and $license!="n/a" and $license!="N/A" and $license!="notavailable" and $license!="NotAvailable"
                                    and $has_rbt==false and $not_match_pml==0 and $license!="" and $provider_type!=""){

                                    $degrees=$provider->getDegree();
                                    $degreePass=0;
                                    if($degrees!=null){
                                        foreach ($degrees as $degree){
                                            if($degree->getId()==20 or $degree->getId()==24 or $degree->getId()==22 or $degree->getId()==30 or $degree->getId()==32){
                                                $degreePass=1;
                                            }
                                        }
                                    }

                                    if($degreePass==0){
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $key);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $provider->getLastName());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $provider->getFirstName());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $isDr);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, 'Y');
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $eClaimProviderType);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $provider_type);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_M, $specialty1);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_P, $specialty2);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_S, $specialty3);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_V, $specialty4);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_Z, $provider->getNpiNumber());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AA, $medicaid);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AB, $license);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AC, $ahca_id);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AD, $genderCode);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AE, $language1);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AF, $language2);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AG, $language3);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AH, $language4);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AI, $language5);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AJ, $language6);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AK, $language7);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AL, $language8);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BO, $site_index);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $site_index);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BQ, $irs_type);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BR, $irs);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BT, $addr->getStreet());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BS, $addr->getSuiteNumber());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BU, $addr->getCity());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BV, $addr->getUsState());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BW, $addr->getZipCode());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BY, $phone_number);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_CF, $ind_or_group);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_DU, 'MCD');
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_DV, $medicaid_percent);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_DY, $minAge);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_DZ, $maxAge);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $exempt);
                                        //$spreadsheet->getActiveSheet(0)->setCellValue($cell_BP, $pareffdt);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BP, '20210101');

                                        $ada="N";
                                        if($addr->getWheelchair()==true){
                                            $ada="Y";
                                        }

                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_CB, $ada);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_CC, 'Y');
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_CD, 'B');

                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_CG, $taxonomy1);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_CH, $taxonomy2);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_CI, $taxonomy3);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_CO, "N");

                                        $bussinesOurs=$addr->getBusinessHours();
                                        if($bussinesOurs!=""){
                                            $bussinesOurs=substr($bussinesOurs,1);
                                            $bussinesOurs=substr($bussinesOurs,0,-1);

                                            $bussinesOurs=str_replace('{','',$bussinesOurs);
                                            $bussinesOurs=str_replace('}','',$bussinesOurs);
                                            $bussinesOurs=str_replace('"','',$bussinesOurs);

                                            $daysHours = explode(",", $bussinesOurs);

                                            //sunday
                                            $dayActive=explode(":",$daysHours[18])[1];
                                            $startHour=substr($daysHours[19],9);
                                            $tillHour=substr($daysHours[20],9);

                                            if($dayActive=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DD,strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DE,strtoupper($tillHour));
                                                $hasWeekend="Y";

                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //monday
                                            $dayActive=explode(":",$daysHours[0])[1];
                                            $startHour=substr($daysHours[1],9);
                                            $tillHour=substr($daysHours[2],9);
                                            if($dayActive=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CR,strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CS,strtoupper($tillHour));

                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //tuesday
                                            $dayActive=explode(":",$daysHours[3])[1];
                                            $startHour=substr($daysHours[4],9);
                                            $tillHour=substr($daysHours[5],9);
                                            if($dayActive=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CT,strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CU,strtoupper($tillHour));

                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //Wednesday
                                            $dayActive=explode(":",$daysHours[6])[1];
                                            $startHour=substr($daysHours[7],9);
                                            $tillHour=substr($daysHours[8],9);
                                            if($dayActive=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CV,strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CW,strtoupper($tillHour));

                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //Thurs
                                            $dayActive=explode(":",$daysHours[9])[1];
                                            $startHour=substr($daysHours[10],9);
                                            $tillHour=substr($daysHours[11],9);
                                            if($dayActive=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CX,strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CY,strtoupper($tillHour));

                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //Frid
                                            $dayActive=explode(":",$daysHours[12])[1];
                                            $startHour=substr($daysHours[13],9);
                                            $tillHour=substr($daysHours[14],9);
                                            if($dayActive=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CZ,strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DA,strtoupper($tillHour));

                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //sat
                                            $dayActive=explode(":",$daysHours[15])[1];
                                            $startHour=substr($daysHours[16],9);
                                            $tillHour=substr($daysHours[17],9);
                                            if($dayActive=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DB,strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DC,strtoupper($tillHour));
                                                $hasWeekend="Y";

                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }
                                        }

                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_CP, $hasEvening);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_CQ, $hasWeekend);

                                        //get billing Address
                                        $orgId=null;
                                        if($provider->getOrganization()!=null){
                                            $orgId=$provider->getOrganization()->getId();
                                        }
                                        $orgAddress=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$orgId));

                                        if(count($orgAddress)==1){
                                            foreach ($orgAddress as $addrB){

                                                $phone_number_B = $addrB->getPhoneNumber();
                                                $phone_number_B = str_replace('-', "", $phone_number_B);

                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DO, $addrB->getStreet());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DP, $addrB->getCity());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DQ, $addrB->getUsState());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DR, $addrB->getZipCode());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DT, $phone_number_B);
                                            }
                                        }

                                        $street="";
                                        $city="";
                                        $usState="";
                                        $zipCode="";
                                        $hasBilling=false;

                                        if(count($orgAddress)>1){
                                            foreach ($orgAddress as $addrB){
                                                if($addrB->getBillingAddr()==true){
                                                    $hasBilling=true;
                                                    $street=$addrB->getStreet();
                                                    $city=$addrB->getCity();
                                                    $usState=$addrB->getUsState();
                                                    $zipCode=$addrB->getZipCode();
                                                }
                                            }

                                            if($hasBilling==true){
                                                $phone_number_B = $addrB->getPhoneNumber();
                                                $phone_number_B = str_replace('-', "", $phone_number_B);

                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DO, $street);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DP, $city);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DQ, $usState);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DR, $zipCode);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DT, $phone_number_B);
                                            }
                                        }

                                        $key_cont++;
                                        $cont++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        foreach ($organizations_result as $organizationID){
            $organization=$em->getRepository('App\Entity\ViewOrganization')->find($organizationID);

            if($organization!=null and $organization->getDisabled()==0){
                $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                $key_cont=1;

                if($address!=null){
                    foreach ($address as $addr){
                        $street_check=$addr->getStreet();
                        $pass=1;

                        if(stristr($street_check, 'BOX')==true or stristr($street_check, 'Box')==true or stristr($street_check, 'box')==true) {
                            $pass=0;
                        }

                        if($pass==1){
                            $pmls=array();
                            if($addr->getLocationNpi()!=""){
                                $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$organization->getGroupNpi(),'colW'=>'A'));
                            }else{
                                $npiR=$organization->getGroupNpi();
                                $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$npiR,'colW'=>'A'));
                            }

                            $site_index = "";
                            $ind_or_group = "";

                            if ($key_cont < 10) {
                                $key = $organization->getGroupNpi() . "-" . '00' . $key_cont;
                                $site_index = "00" . $key_cont;
                            } else {
                                $key = $organization->getGroupNpi() . "-" . '0' . $key_cont;
                                $site_index = "0" . $key_cont;
                            }

                            $providerType = "";
                            $providerTypesArray=array();
                            $specialty1 = "";
                            $specialty2 = "";
                            $specialty3 = "";
                            $specialty4 = "";

                            $taxonomyCode1="";
                            $taxonomyCode2="";
                            $taxonomyCode3="";

                            $types=array();
                            $specialtiesArray=array();
                            $taxonomiesCode=array();

                            if(count($pmls)>0){
                                foreach ($pmls as $pml){
                                    $type=$pml->getColD();
                                    if($type!="" and $type!=null){
                                        $providerTypesArray[]=$type;
                                    }

                                    $spe=$pml->getColE();
                                    $tax=$pml->getColF();

                                    if($type!=""){
                                        $types[]=$type;
                                    }
                                    if($spe!=""){
                                        if(!in_array($spe,$specialtiesArray)){
                                            $specialtiesArray[]=$spe;
                                        }
                                    }
                                    if($tax!=""){
                                        if(!in_array($tax,$taxonomiesCode)){
                                            $taxonomiesCode[]=$tax;
                                        }
                                    }
                                }

                                $code="";
                                $match_pt=0;
                                foreach ($providerTypesArray as $spt){
                                    foreach ($pt_order as $pt_o){
                                        if($pt_o==$spt and $match_pt==0){
                                            $code=$spt;
                                            $match_pt=1;
                                        }
                                    }
                                }

                                if(strlen($code)==1){
                                    $code="00".$code;
                                }
                                if(strlen($code)==2){
                                    $code="0".$code;
                                }

                                $providerType=$code;

                                if(count($specialtiesArray)>=1){
                                    $specialty1=$specialtiesArray[0]."01";
                                }
                                if(count($specialtiesArray)>=2){
                                    $specialty2=$specialtiesArray[1]."01";
                                }
                                if(count($specialtiesArray)>=3){
                                    $specialty3=$specialtiesArray[2]."01";
                                }
                                if(count($specialtiesArray)>=4){
                                    $specialty4=$specialtiesArray[3]."01";
                                }
                                if(count($taxonomiesCode)>=1){
                                    $taxonomyCode1=$taxonomiesCode[0];
                                }
                                if(count($taxonomiesCode)>=2){
                                    $taxonomyCode2=$taxonomiesCode[1];
                                }
                                if(count($taxonomiesCode)>=3){
                                    $taxonomyCode3=$taxonomiesCode[2];
                                }
                            }

                            $pareffdt="";
                            $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));

                            if($address!=null){
                                foreach ($address as $addr3){
                                    $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr3->getId(),'credentialing_status'=>4));
                                    if($credentialingsF!=null){
                                        foreach ($credentialingsF as $cf){
                                            $dateFac=$cf->getCredentialingAcceptedDate();
                                            $dateFacEffe=$cf->getCredentialingEffectiveDate();

                                            $year="";
                                            $moth='';
                                            if($dateFacEffe!=""){
                                                $effD=explode('/',$dateFacEffe);
                                                $year=$effD[2];
                                                $moth=$effD[0];
                                            }
                                            $pareffdt=$year.$moth.'01';
                                        }
                                    }else{
                                        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId()));
                                        if($providers!=null){
                                            foreach ($providers as $provider){
                                                $credentialings_Tem=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status'=>4,'provider'=>$provider->getId()));
                                                $datesCExist=[];
                                                if($credentialings_Tem!=null){
                                                    foreach ($credentialings_Tem as $credeTem){
                                                        $dateT=$credeTem->getCredentialingEffectiveDate();
                                                        if($dateT!=""){
                                                            if(!in_array($dateT,$datesCExist)){
                                                                $datesCExist[]=$dateT;
                                                            }
                                                        }
                                                    }
                                                }
                                                //total of different dates from credentialing providers
                                                $totalDates=count($datesCExist);
                                                if($totalDates==1){
                                                    $dateC1= explode('/',$datesCExist[0]);
                                                    $yearT=$dateC1[2];
                                                    $mothT=$dateC1[0];
                                                    $pareffdt=$yearT.$mothT."01";
                                                }
                                                if($totalDates>1){
                                                    $dateC1= explode('/',$datesCExist[0]);
                                                    $yearT=$dateC1[2];
                                                    $mothT=$dateC1[0];
                                                    $pareffdt=$yearT.$mothT."01";
                                                }

                                                if(count($specialtiesArray)==0){
                                                    $pmls=$em->getRepository('App\Entity\Pml')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>"A"));
                                                    if($pmls!=null){
                                                        foreach ($pmls as $pml) {
                                                            $spe=$pml->getColE();
                                                            if($spe!=""){
                                                                if(!in_array($spe,$specialtiesArray)){
                                                                    $specialtiesArray[]=$spe;
                                                                }
                                                            }
                                                            $type=$pml->getColD();
                                                            if($type!="" and $type!=null){
                                                                if(!in_array($type,$providerTypesArray)){
                                                                    $providerTypesArray[]=$type;
                                                                }
                                                            }
                                                        }

                                                        $code="";
                                                        $match_pt=0;
                                                        foreach ($providerTypesArray as $spt){
                                                            foreach ($pt_order as $pt_o){
                                                                if($pt_o==$spt and $match_pt==0){
                                                                    $code=$spt;
                                                                    $match_pt=1;
                                                                }
                                                            }
                                                        }

                                                        if(strlen($code)==1){
                                                            $code="00".$code;
                                                        }
                                                        if(strlen($code)==2){
                                                            $code="0".$code;
                                                        }
                                                        $providerType=$code;
                                                        if(count($specialtiesArray)>=1){
                                                            $specialty1=$specialtiesArray[0]."01";
                                                        }
                                                        if(count($specialtiesArray)>=2){
                                                            $specialty2=$specialtiesArray[1]."01";
                                                        }
                                                        if(count($specialtiesArray)>=3){
                                                            $specialty3=$specialtiesArray[2]."01";
                                                        }
                                                        if(count($specialtiesArray)>=4){
                                                            $specialty4=$specialtiesArray[3]."01";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            $street_check2=$addr->getStreet();
                            if(stristr($street_check2, 'BOX')==false and stristr($street_check2, 'Box')==false and stristr($street_check2, 'box')==false) {
                                $cell_A = 'A' . $cont;$cell_B = 'B' . $cont;$cell_C = 'C' . $cont;$cell_D = 'D' . $cont;
                                $cell_E = 'E' . $cont;$cell_F = 'F' . $cont;$cell_G = 'G' . $cont;$cell_H = 'H' . $cont;
                                $cell_I = 'I' . $cont;$cell_J = 'J' . $cont;$cell_K = 'K' . $cont;$cell_L = 'L' . $cont;
                                $cell_M = 'M' . $cont;$cell_N = 'N' . $cont;$cell_O = 'O' . $cont;$cell_P = 'P' . $cont;
                                $cell_Q = 'Q' . $cont;$cell_R = 'R' . $cont;$cell_S = 'S' . $cont;$cell_T = 'T' . $cont;$cell_U = 'U' . $cont;$cell_V = 'V' . $cont;
                                $cell_W = 'W' . $cont;$cell_X = 'X' . $cont;$cell_Y = 'Y' . $cont;$cell_Z = 'Z' . $cont;

                                $cell_AA = 'AA' . $cont;$cell_AB = 'AB' . $cont;$cell_AC = 'AC' . $cont;$cell_AD = 'AD' . $cont;
                                $cell_AE = 'AE' . $cont;$cell_AF = 'AF' . $cont;$cell_AG = 'AG' . $cont;$cell_AH = 'AH' . $cont;$cell_AI = 'AI' . $cont;$cell_AJ = 'AJ' . $cont;$cell_AK = 'AK' . $cont;
                                $cell_AL = 'AL' . $cont;$cell_AM = 'AM' . $cont;$cell_AN = 'AN' . $cont;$cell_AO = 'AO' . $cont;$cell_AP = 'AP' . $cont;
                                $cell_AQ = 'AQ' . $cont;$cell_AR = 'AR' . $cont;$cell_AS = 'AS' . $cont;$cell_AT = 'AT' . $cont;$cell_AU = 'AU' . $cont;
                                $cell_AV = 'AV' . $cont;$cell_AW = 'AW' . $cont;$cell_AX = 'AX' . $cont;$cell_AY = 'AY' . $cont;$cell_AZ = 'AZ' . $cont;

                                $cell_BA = 'BA' . $cont;$cell_BB = 'BB' . $cont;$cell_BC = 'BC' . $cont;$cell_BD = 'BD' . $cont;$cell_BE = 'BE' . $cont;
                                $cell_BF = 'BF' . $cont;$cell_BG = 'BG' . $cont;$cell_BH = 'BH' . $cont;$cell_BI = 'BI' . $cont;$cell_BJ = 'BJ' . $cont;
                                $cell_BK = 'BK' . $cont;$cell_BL = 'BL' . $cont;$cell_BM = 'BM' . $cont;$cell_BN = 'BN' . $cont;$cell_BO = 'BO' . $cont;
                                $cell_BP = 'BP' . $cont;$cell_BQ = 'BQ' . $cont;$cell_BR = 'BR' . $cont;$cell_BS = 'BS' . $cont;$cell_BT = 'BT' . $cont;
                                $cell_BU = 'BU' . $cont;$cell_BV = 'BV' . $cont;$cell_BW = 'BW' . $cont;$cell_BX = 'BX' . $cont;$cell_BY = 'BY' . $cont;$cell_BZ = 'BZ' . $cont;

                                $cell_CA = 'CA' . $cont;$cell_CB = 'CB' . $cont;
                                $cell_CC = 'CC' . $cont;$cell_CD = 'CD' . $cont;$cell_CE = 'CE' . $cont;$cell_CF = 'CF' . $cont;
                                $cell_CG = 'CG' . $cont;$cell_CH = 'CH' . $cont;$cell_CI = 'CI' . $cont;$cell_CJ = 'CJ' . $cont;
                                $cell_CK = 'CK' . $cont;$cell_CL = 'CL' . $cont;$cell_CM = 'CM' . $cont;$cell_CN = 'CN' . $cont;
                                $cell_CO = 'CO' . $cont;$cell_CP = 'CP' . $cont;$cell_CQ = 'CQ' . $cont;$cell_CR = 'CR' . $cont;
                                $cell_CS = 'CS' . $cont;$cell_CT = 'CT' . $cont;$cell_CU = 'CU' . $cont;$cell_CV = 'CV' . $cont;
                                $cell_CW = 'CW' . $cont;$cell_CX = 'CX' . $cont;$cell_CY = 'CY' . $cont;$cell_CZ = 'CZ' . $cont;

                                $cell_DA = 'DA' . $cont;$cell_DB = 'DB' . $cont;$cell_DC = 'DC' . $cont;$cell_DD = 'DD' . $cont;$cell_DE = 'DE' . $cont;
                                $cell_DF = 'DF' . $cont;$cell_DG = 'DG' . $cont;$cell_DH = 'DH' . $cont;$cell_DI = 'DI' . $cont;$cell_DJ = 'DJ' . $cont;$cell_DK = 'DK' . $cont;$cell_DL = 'DL' . $cont;$cell_DM = 'DM' . $cont;
                                $cell_DN = 'DN' . $cont;$cell_DO = 'DO' . $cont;$cell_DP = 'DP' . $cont;
                                $cell_DQ = 'DQ' . $cont;$cell_DR = 'DR' . $cont;$cell_DT = 'DT' . $cont;
                                $cell_DU='DU'.$cont;;$cell_DV='DV'.$cont;$cell_DY='DY'.$cont;$cell_DZ='DZ'.$cont;

                                $type3 = false;
                                $orgClasifications = $organization->getOrgSpecialtiesArray();

                                if ($orgClasifications != null) {
                                    foreach ($orgClasifications as $orgC) {
                                        if ($orgC->getName() == "State Mental Hospital") {
                                            $type3 = true;
                                        }
                                    }
                                }
                                if($type3==true){
                                    $ahca_id="";
                                    $address2=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                                    if($address2!=null){
                                        foreach ($address2 as $addr2){
                                            $ahca_id=$addr2->getAhcaNumber();
                                            if($ahca_id!=""){
                                                if(strlen($ahca_id)==6){
                                                    $ahca_id="00".$ahca_id;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AC, $ahca_id);
                                }

                                $minAge=strval($addr->getOfficeAgeLimit());
                                $maxAge=strval($addr->getOfficeAgeLimitMax());

                                if(strlen($minAge)==1){
                                    $minAge='0'.$minAge;
                                }
                                $minAge=$minAge."Y";

                                if(strlen($maxAge)==1){
                                    $maxAge='0'.$maxAge;
                                }
                                $maxAge=$maxAge."Y";

                                $medicaid_percent="100%";
                                $org_rate=$em->getRepository('App\Entity\OrganizationRates')->findOneBy(array('organization'=>$organization->getId(),'rate'=>2));
                                if($org_rate!=null){
                                    $medicaid_percent=$org_rate->getPercent();
                                }
                                $medicaid_percent=$medicaid_percent."%";

                                $hasWeekend="N";
                                $hasEvening="N";

                                $exempt="";
                                if($addr->getSeePatientsAddr()==false){
                                    $exempt="DE";
                                }

                                $eClaimProviderType="GR";
                                $providerTypeOrder=['01','04','16','68','66','05','77','67','08','14','25','26','30','29','31','07','32','81','97'];
                                $groupNpi=$organization->getGroupNpi();
                                $proTypeArray=array();
                                $pmlsArray=array();
                                if($groupNpi!=""){
                                    $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$groupNpi,'colW'=>"A"));
                                    if($pmls!=null){
                                        foreach ($pmls as $pml) {
                                            $proTypeCode = $pml->getColD();
                                            if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                                $proTypeArray[]=$proTypeArray;
                                                $pmlsArray[]=$pml;
                                            }
                                        }
                                    }
                                }

                                if(count($proTypeArray)==0){
                                    if($providers!=null) {
                                        foreach ($providers as $provider) {
                                            $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>"A"));
                                            if($pmls!=null){
                                                foreach ($pmls as $pml) {
                                                    $proTypeCode = $pml->getColD();
                                                    if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                                        $proTypeArray[]=$proTypeArray;
                                                        $pmlsArray[]=$pml;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                                $pt_code="";
                                foreach ($pmlsArray as $pt){
                                    foreach ($providerTypeOrder as $order){
                                        if($pt->getColD()==$order){
                                            $pt_code=$pt->getColD();
                                            $specialty_code=$pt->getColE()."01";
                                            $medicaid_code=$pt->getColA();
                                            break 2;
                                        }
                                    }
                                }

                                $medicaid_length=strlen($medicaid_code);
                                if($medicaid_length<9){
                                    $rest=9-$medicaid_length;
                                    while ($rest>0){
                                        $medicaid_code="0".$medicaid_code;
                                        $rest--;
                                    }
                                }

                                if(strlen($pt_code)==1){
                                    $pt_code="00".$pt_code;
                                }
                                if(strlen($pt_code)==2){
                                    $pt_code="0".$pt_code;
                                }

                                if($providerType==""){
                                    $providerType=$pt_code;
                                }

                                if($specialty1==""){
                                    $specialty1=$specialty_code;
                                }

                                if($specialty1=="20001" or $specialty1=="20101" or $specialty1=="90101" or $specialty1=="30001" or $specialty1=="30101"){
                                    $eClaimProviderType="HS";
                                }

                                if($specialty2=="20001" or $specialty2=="20101" or $specialty2=="90101" or $specialty2=="30001" or $specialty2=="30101"){
                                    $eClaimProviderType="HS";
                                }

                                if($specialty3=="20001" or $specialty3=="20101" or $specialty3=="90101" or $specialty3=="30001" or $specialty3=="30101"){
                                    $eClaimProviderType="HS";
                                }

                                if($providerType!=""){
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, $key);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $providerType);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BO, $site_index);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $site_index);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, strtoupper($organization->getName()));
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, 'N'); //Switch Flag
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z, $organization->getGroupNpi());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AA, $medicaid_code);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $specialty1);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $eClaimProviderType);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_P, $specialty2);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_S, $specialty3);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_V, $specialty4);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_CF, 'O');
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_CD, 'B');
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_DU, 'MCD');
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_DV, $medicaid_percent);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_DY, $minAge);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_DZ, $maxAge);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, $exempt);
                                    //$spreadsheet->getActiveSheet(1)->setCellValue($cell_BP, $pareffdt);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_BP, '20210101');

                                    $providers_te=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organizationID));
                                    $languagesId=[];
                                    if($providers_te!=null){
                                        foreach ($providers_te as $provider){
                                            $languages=$provider->getLanguages();
                                            if($languages!=null){
                                                foreach ($languages as $lng){
                                                    if(!in_array($lng->getId(),$languagesId)){
                                                        $languagesId[]=$lng->getId();
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    $language1 = "";
                                    $language2 = "";
                                    $language3 = "";
                                    $language4 = "";
                                    $language5 = "";
                                    $language6 = "";
                                    $language7 = "";
                                    $language8 = "";
                                    $languagesArray = array();
                                    if ($languagesId != null) {
                                        foreach ($languagesId as $id) {
                                            $language = $em->getRepository('App\Entity\Languages')->find($id);
                                            if ($language != null) {
                                                $languagesArray[] = $language;
                                            }
                                        }
                                    }

                                    if ($languagesArray != null) {
                                        if (count($languagesArray) > 0) {
                                            $language1 = $languagesArray[0]->getLangCd();
                                        }
                                        if (count($languagesArray) > 1) {
                                            $language2 = $languagesArray[1]->getLangCd();
                                        }
                                        if (count($languagesArray) > 2) {
                                            $language3 = $languagesArray[2]->getLangCd();
                                        }
                                        if (count($languagesArray) > 3) {
                                            $language4 = $languagesArray[3]->getLangCd();
                                        }
                                        if (count($languagesArray) > 4) {
                                            $language5 = $languagesArray[4]->getLangCd();
                                        }
                                        if (count($languagesArray) > 5) {
                                            $language6 = $languagesArray[5]->getLangCd();
                                        }
                                        if (count($languagesArray) > 6) {
                                            $language7 = $languagesArray[6]->getLangCd();
                                        }
                                        if (count($languagesArray) > 7) {
                                            $language8 = $languagesArray[7]->getLangCd();
                                        }
                                    }

                                    if($language1==""){
                                        $language1="ENG";
                                    }

                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AE, $language1);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AF, $language2);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AG, $language3);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AH, $language4);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AI, $language5);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AJ, $language6);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AK, $language7);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_AL, $language8);
                                    $phone_number = $addr->getPhoneNumber();
                                    $phone_number = str_replace('-', "", $phone_number);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_BT, $addr->getStreet());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_BS, $addr->getSuiteNumber());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_BU, $addr->getCity());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_BV, $addr->getUsState());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_BW, $addr->getZipCode());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_BY, $phone_number);

                                    $ada = "N";
                                    if ($addr->getWheelchair() == true) {
                                        $ada = "Y";
                                    }

                                    if($taxonomyCode1==""){
                                        $org_taxonomies=$em->getRepository('App\Entity\OrganizationTaxonomy')->findBy(array('organization'=>$organization->getId()));
                                        if($org_taxonomies!=null){
                                            if(count($org_taxonomies)>=1){
                                                $taxonomyCode1=$org_taxonomies[0]->getTaxonomy()->getCode();
                                            }
                                            if(count($org_taxonomies)>=2){
                                                $taxonomyCode2=$org_taxonomies[1]->getTaxonomy()->getCode();
                                            }
                                            if(count($org_taxonomies)>=3){
                                                $taxonomyCode3=$org_taxonomies[2]->getTaxonomy()->getCode();
                                            }
                                        }
                                    }

                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_CB, $ada);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_CC, 'Y');
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_CG, $taxonomyCode1);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_CH, $taxonomyCode2);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_CI, $taxonomyCode3);

                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_CO, "N");

                                    $bussinesOurs = $addr->getBusinessHours();
                                    if($bussinesOurs!=""){
                                        $bussinesOurs = substr($bussinesOurs, 1);
                                        $bussinesOurs = substr($bussinesOurs, 0, -1);

                                        $bussinesOurs = str_replace('{', '', $bussinesOurs);
                                        $bussinesOurs = str_replace('}', '', $bussinesOurs);
                                        $bussinesOurs = str_replace('"', '', $bussinesOurs);

                                        $daysHours = explode(",", $bussinesOurs);

                                        //sunday
                                        $dayActive = explode(":", $daysHours[18])[1];
                                        $startHour = substr($daysHours[19], 9);
                                        $tillHour = substr($daysHours[20], 9);

                                        if ($dayActive == "true") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_DD, strtoupper($startHour));
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_DE, strtoupper($tillHour));

                                            $hasWeekend="Y";
                                            $hourTill=explode(':',$tillHour);
                                            $hourTill=intval($hourTill[0]);
                                            if($hourTill>6){
                                                $hasEvening="Y";
                                            }
                                        }

                                        //monday
                                        $dayActive = explode(":", $daysHours[0])[1];
                                        $startHour = substr($daysHours[1], 9);
                                        $tillHour = substr($daysHours[2], 9);
                                        if ($dayActive == "true") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CR, strtoupper($startHour));
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CS, strtoupper($tillHour));
                                            $hourTill=explode(':',$tillHour);
                                            $hourTill=intval($hourTill[0]);
                                            if($hourTill>6){
                                                $hasEvening="Y";
                                            }
                                        }

                                        //tuesday
                                        $dayActive = explode(":", $daysHours[3])[1];
                                        $startHour = substr($daysHours[4], 9);
                                        $tillHour = substr($daysHours[5], 9);
                                        if ($dayActive == "true") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CT, strtoupper($startHour));
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CU, strtoupper($tillHour));
                                            $hourTill=explode(':',$tillHour);
                                            $hourTill=intval($hourTill[0]);
                                            if($hourTill>6){
                                                $hasEvening="Y";
                                            }
                                        }

                                        //Wednesday
                                        $dayActive = explode(":", $daysHours[6])[1];
                                        $startHour = substr($daysHours[7], 9);
                                        $tillHour = substr($daysHours[8], 9);
                                        if ($dayActive == "true") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CV, strtoupper($startHour));
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CW, strtoupper($tillHour));
                                            $hourTill=explode(':',$tillHour);
                                            $hourTill=intval($hourTill[0]);
                                            if($hourTill>6){
                                                $hasEvening="Y";
                                            }
                                        }

                                        //Thurs
                                        $dayActive = explode(":", $daysHours[9])[1];
                                        $startHour = substr($daysHours[10], 9);
                                        $tillHour = substr($daysHours[11], 9);
                                        if ($dayActive == "true") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CX, strtoupper($startHour));
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CY, strtoupper($tillHour));
                                            $hourTill=explode(':',$tillHour);
                                            $hourTill=intval($hourTill[0]);
                                            if($hourTill>6){
                                                $hasEvening="Y";
                                            }
                                        }

                                        //Frid
                                        $dayActive = explode(":", $daysHours[12])[1];
                                        $startHour = substr($daysHours[13], 9);
                                        $tillHour = substr($daysHours[14], 9);
                                        if ($dayActive == "true") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CZ, strtoupper($startHour));
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_DA, strtoupper($tillHour));
                                            $hourTill=explode(':',$tillHour);
                                            $hourTill=intval($hourTill[0]);
                                            if($hourTill>6){
                                                $hasEvening="Y";
                                            }
                                        }

                                        //sat
                                        $dayActive = explode(":", $daysHours[15])[1];
                                        $startHour = substr($daysHours[16], 9);
                                        $tillHour = substr($daysHours[17], 9);
                                        if ($dayActive == "true") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_DB, strtoupper($startHour));
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_DC, strtoupper($tillHour));

                                            $hasWeekend="Y";
                                            $hourTill=explode(':',$tillHour);
                                            $hourTill=intval($hourTill[0]);
                                            if($hourTill>6){
                                                $hasEvening="Y";
                                            }
                                        }
                                    }

                                    //get billing Address
                                    $orgAddress = $em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $organization->getId()));

                                    if (count($orgAddress) == 1) {
                                        foreach ($orgAddress as $addrB) {
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DO, $addrB->getStreet());
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DP, $addrB->getCity());
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DQ, $addrB->getUsState());
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DR, $addrB->getZipCode());
                                            $phone_number_B = $addrB->getPhoneNumber();
                                            $phone_number_B = str_replace('-', "", $phone_number_B);
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DT,$phone_number_B);
                                        }
                                    }

                                    $street = "";
                                    $city = "";
                                    $usState = "";
                                    $zipCode = "";
                                    $phoneNumber = "";
                                    $hasBilling = false;

                                    if (count($orgAddress) > 1) {
                                        foreach ($orgAddress as $addrB) {
                                            if ($addrB->getBillingAddr() == true) {
                                                $hasBilling = true;
                                                $street = $addrB->getStreet();
                                                $city = $addrB->getCity();
                                                $usState = $addrB->getUsState();
                                                $zipCode = $addrB->getZipCode();
                                                $phone_number_B = $addrB->getPhoneNumber();
                                                $phone_number_B = str_replace('-', "", $phone_number_B);
                                            }
                                        }

                                        if ($hasBilling == true) {
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DO, $street);
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DP, $city);
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DQ, $usState);
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DR, $zipCode);
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DT, $phone_number_B);
                                        }
                                    }

                                    $tin = $organization->getTinNumber();

                                    if ($tin != "") {
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BQ, 'T');
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BR, $tin);
                                    }else{
                                        $provider=$em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number'=>$organization->getGroupNpi()));
                                        if($provider!=null){
                                            $tin=$encoder->decryptthis($provider->getSocial());
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BQ, 'S');
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BR, $tin);
                                        }
                                    }

                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_CP, $hasEvening);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_CQ, $hasWeekend);

                                    $cont++;
                                    $key_cont++;
                                }
                            }
                        }
                    }
                }
            }
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="FCC_BSN_Roster'.date('Ymd').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/provider-network-standards-table", name="provider_network_standards_table")
     */
    public function  ProviderNetworkStandardsTable(){
        set_time_limit(3600);

        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');
        $date=date('m/d/Y');

        $spreadsheet = $reader->load($url.'fcc_standards_table.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $em=$this->getDoctrine()->getManager();
        $cont=5;

        $specialties=['Board Certified or Board Eligible Adult Psychiatrists','Board Certified or Board Eligible Child Psychiatrists','Licensed Practitioners of the Healing Arts',
            'Licensed Community Substance Abuse Treatment Centers'];

        $customerCounties=$em->getRepository('App\Entity\PayerCustomerCounty')->findBy(array('payer'=>4));
        $count_regions=[0,0,0,0,0,0,0,0,0,0,0];
        if($customerCounties!=null){
            foreach ($customerCounties as $customerCounty){
                $region=intval($customerCounty->getCounty()->getRegion());
                $current=intval($count_regions[$region-1]);
                $customers=intval($customerCounty->getTotal());
                $new_customers=$current+$customers;

                $count_regions[$region-1]=$new_customers;
            }
        }
        $cont_specialty=1;
        foreach ($specialties as $specialty){
            $cell_A = 'A' . $cont;

            $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $specialty);
            $cont_region=1;

            foreach ($count_regions as $count_region){
                $cell_B = 'B' . $cont;$cell_C = 'C' . $cont;$cell_D = 'D' . $cont;
                $cell_E = 'E' . $cont;$cell_F = 'F' . $cont;$cell_G = 'G' . $cont;$cell_H = 'H' . $cont;
                $cell_I = 'I' . $cont;$cell_J = 'J' . $cont;

                $region_name="Region ".$cont_region;
                $provider_count=$this->providersSpecialtiesByRegion($cont_region,$cont_specialty);

                $result=$provider_count.":".$count_region;



                $spreadsheet->getActiveSheet(0)->setCellValue($cell_B,$region_name);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_C,strval($count_region));
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_D,strval($provider_count));
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_E,'30');
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_F,'20');
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_G,'60');
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_H,'45');

                if($cont_specialty==1){
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_I,'1:1,500 enrollees');
                }
                if($cont_specialty==2){
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_I,'1:7,100 enrollees');
                }
                if($cont_specialty==3){
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_I,'1:1,500 enrollees');
                }
                if($cont_specialty==4){
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_I,'1:1,500 enrollees');
                }

                $spreadsheet->getActiveSheet(0)->setCellValue($cell_J,strval($result));

                $cont++;
                $cont_region++;
            }

            $cont++;
            $cont_specialty++;
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="fcc_regional_provider_ratios'.date('mdY_his').'.xls"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    function providersSpecialtiesByRegion($region,$specialty_count){
        $em=$this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $em=$this->getDoctrine()->getManager();
        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));
        $total=0;

        //Board Certified or Board Eligible Adult Psychiatrists
        if($specialty_count==1){
            foreach ($providers as $provider){
                $is_valid=false;
                $specialties=$provider->getPrimarySpecialties();
                if($specialties!=null){
                    foreach ($specialties as $specialty){
                        if($specialty->getCode()=='042'){
                            $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>"A"));
                            if($pmls!=null){
                                $address=$provider->getBillingAddress();
                                if($address!=null){
                                    foreach ($address as $addr){
                                        if($addr->getRegion()==$region){
                                            $is_valid=true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if($is_valid==true){
                    $total++;
                }
            }
        }

        //Board Certified or Board Eligible Child Psychiatrists
        if($specialty_count==2){
            foreach ($providers as $provider){
                $is_valid=false;

                $specialties=$provider->getPrimarySpecialties();
                if($specialties!=null){
                    foreach ($specialties as $specialty){
                        if($specialty->getCode()=='043'){
                            $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>"A"));
                            if($pmls!=null){
                                $address=$provider->getBillingAddress();
                                if($address!=null){
                                    foreach ($address as $addr){
                                        if($addr->getRegion()==$region){
                                            $is_valid=true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if($is_valid==true){
                    $total++;
                }
            }
        }

        //Licensed Practitioners of the Healing Arts
        if($specialty_count==3){
            foreach ($providers as $provider){
                $is_valid=false;
                $specialties=$provider->getPrimarySpecialties();
                if($specialties!=null){
                    foreach ($specialties as $specialty){
                        if($specialty->getCode()=='066' or $specialty->getCode()=='067' or $specialty->getCode()=='907' or $specialty->getCode()=='124' or $specialty->getCode()=='929' or
                            $specialty->getCode()=='076' or $specialty->getCode()=='177' or $specialty->getCode()=='930' or $specialty->getCode()=='172' or $specialty->getCode()=='173' or
                            $specialty->getCode()=='176' or $specialty->getCode()=='174' or $specialty->getCode()=='175' or $specialty->getCode()=='932' or $specialty->getCode()=='390' or
                            $specialty->getCode()=='391' or $specialty->getCode()=='392' or $specialty->getCode()=='393' or $specialty->getCode()=='981' or $specialty->getCode()=='178' or
                            $specialty->getCode()=='991' or $specialty->getCode()=='803'){
                            $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>"A"));
                            if($pmls!=null){
                                $address=$provider->getBillingAddress();
                                if($address!=null){
                                    foreach ($address as $addr){
                                        if($addr->getRegion()==$region){
                                            $is_valid=true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if($is_valid==true){
                    $total++;
                }
            }
        }

        //Licensed Community Substance Abuse Treatment Centers
        if($specialty_count==4){
            foreach ($providers as $provider){
                $is_valid=false;
                $specialties=$provider->getPrimarySpecialties();
                if($specialties!=null){
                    foreach ($specialties as $specialty){
                        if($specialty->getCode()=='302' or $specialty->getCode()=='303' or $specialty->getCode()=='305' or $specialty->getCode()=='306'){
                            $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>"A"));
                            if($pmls!=null){
                                $address=$provider->getBillingAddress();
                                if($address!=null){
                                    foreach ($address as $addr){
                                        if($addr->getRegion()==$region){
                                            $is_valid=true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if($is_valid==false){
                    $organization_pro=$provider->getOrganization();
                    $org_id=$organization_pro->getId();

                    $sql="SELECT o.id, o.name,
                    GROUP_CONCAT(DISTINCT taxonomy_code.specialization ORDER BY taxonomy_code.id ASC SEPARATOR ', ') AS taxonomies_specialty
                    FROM  organization o 
                    LEFT JOIN provider ON provider.org_id = o.id
                    LEFT JOIN organization_taxonomy ON organization_taxonomy.organization_id = o.id
                    LEFT JOIN taxonomy_code ON taxonomy_code.id = organization_taxonomy.taxonomy_id
                    WHERE o.status_id=2 and o.disabled=0 and o.id=".$org_id."
                    GROUP BY o.id
                    ORDER BY o.name";


                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                    $organization= $stmt->fetchAll();

                    foreach ($organization as $org){
                        $specialties_str= $org['taxonomies_specialty'];

                        if(strpos($specialties_str,'Addiction')==true or strpos($specialties_str,'Substance')==true or strpos($specialties_str,'Abuse')==true){
                            $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>"A"));
                            if($pmls!=null){
                                $address=$provider->getBillingAddress();
                                if($address!=null){
                                    foreach ($address as $addr){
                                        if($addr->getRegion()==$region){
                                            $is_valid=true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if($is_valid==true){
                    $total++;
                }
            }
        }

        return $total;
    }

    /**
     * @Route("/FCC_update_data", name="fcc_update_data")
     */
    public function updateFCCData(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $sql="SELECT p.id
            FROM  provider p 
            WHERE p.disabled=0
            ORDER BY p.id";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $providers= $stmt->fetchAll();
            //m $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));

            return $this->render('fcc/index.html.twig',['providers'=>$providers]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/FCC_update_data_provider", name="fcc_update_data_process",methods={"POST"})
     */
    public function processData(Request $request){
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $id=$request->get('id');
        $type=$request->get('type'); //provider or org

        return new Response(
            json_encode(array('id' => $id)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/save-roster-log", name="fcc_roster_report_log")
     */
    public function saveRosterReportLog(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "fcc_roster_log/FCC_BSN_Roster20211114.xlsx";

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=0;

        //remove current logs
        $currentLogs=$em->getRepository('App:FCCRosterLog')->findAll();

        if($currentLogs){
            foreach ($currentLogs as $log){
                $em->remove($log);
            }
            $em->flush();
        }

        foreach ($sheetData as $sheet) {
            if ($cont >= 1) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    $key_id=$sheet['A'];
                    $last_name=$sheet['F'];
                    $first_name=$sheet['G'];
                    $name_switch_flg=$sheet['I'];
                    $prov_title=$sheet['H'];
                    $eclaims_provider_type=$sheet['J'];
                    $ahca_provider_type=$sheet['K'];
                    $spec1=$sheet['M'];
                    $spec2=$sheet['P'];
                    $spec3=$sheet['S'];
                    $spec4=$sheet['V'];
                    $npi=$sheet['Z'];
                    $medicaid_id=$sheet['AA'];
                    $state_license=$sheet['AB'];
                    $lng1=$sheet['AE'];
                    $lng2=$sheet['AF'];
                    $lng3=$sheet['AG'];
                    $lng4=$sheet['AH'];
                    $lng5=$sheet['AI'];
                    $lng6=$sheet['AJ'];
                    $lng7=$sheet['AK'];
                    $lng8=$sheet['AL'];
                    $prac_isr_type=$sheet['BQ'];
                    $prac_isr=$sheet['BR'];
                    $prac_street=$sheet['BT'];
                    $practice_suite=$sheet['BS'];
                    $prac_city=$sheet['BU'];
                    $prac_state=$sheet['BV'];
                    $prac_zipcode=$sheet['BW'];
                    $prac_phone=$sheet['BY'];
                    $ada_comp=$sheet['CB'];
                    $taxonomy1=$sheet['CG'];
                    $taxonomy2=$sheet['CH'];
                    $taxonomy3=$sheet['CI'];
                    $evening_hrs=$sheet['CP'];
                    $weekend_hrs=$sheet['CQ'];
                    $mon_start=$sheet['CR'];
                    $mon_end=$sheet['CS'];
                    $tues_start=$sheet['CT'];
                    $tues_end=$sheet['CU'];
                    $wed_start =$sheet['CV'];
                    $wed_end=$sheet['CW'];
                    $thurs_start =$sheet['CX'];
                    $thurs_end=$sheet['CY'];
                    $fri_start=$sheet['CZ'];
                    $fri_end=$sheet['DA'];
                    $sat_start=$sheet['DB'];
                    $sat_end=$sheet['DC'];
                    $sun_start  =$sheet['DD'];
                    $sun_end=$sheet['DE'];
                    $payto_street=$sheet['DO'];
                    $payto_city  =$sheet['DP'];
                    $payto_state  =$sheet['DQ'];
                    $payto_zipcode  =$sheet['DR'];
                    $payto_phone=$sheet['DT'];
                    $min_age=$sheet['DY'];
                    $max_age   =$sheet['DZ'];

                    $fccrl=new FCCRosterLog();
                    $fccrl->setIdKey($key_id);
                    $fccrl->setFirstName($first_name);
                    $fccrl->setLastName($last_name);
                    $fccrl->setProvTitle($prov_title);
                    $fccrl->setNameSwitchFlag($name_switch_flg);
                    $fccrl->setEclaimsProviderType($eclaims_provider_type);
                    $fccrl->setAhcaProviderType($ahca_provider_type);
                    $fccrl->setSpec1($spec1);
                    $fccrl->setSpec2($spec2);
                    $fccrl->setSpec3($spec3);
                    $fccrl->setSpec4($spec4);
                    $fccrl->setNpi($npi);
                    $fccrl->setMedicaidId($medicaid_id);
                    $fccrl->setStateLicense($state_license);
                    $fccrl->setLng1($lng1);
                    $fccrl->setLng2($lng2);
                    $fccrl->setLng3($lng3);
                    $fccrl->setLng4($lng4);
                    $fccrl->setLng5($lng5);
                    $fccrl->setLng6($lng6);
                    $fccrl->setLng7($lng7);
                    $fccrl->setLng8($lng8);
                    $fccrl->setPracIsrType($prac_isr_type);
                    $fccrl->setPracIsr($prac_isr);
                    $fccrl->setPracStreet($prac_street);
                    $fccrl->setPracticeSuite($practice_suite);
                    $fccrl->setPracCity($prac_city);
                    $fccrl->setPracState($prac_state);
                    $fccrl->setPracZipcode($prac_zipcode);
                    $fccrl->setPracPhone($prac_phone);
                    $fccrl->setAdaComp($ada_comp);
                    $fccrl->setTaxonomy1($taxonomy1);
                    $fccrl->setTaxonomy2($taxonomy2);
                    $fccrl->setTaxonomy3($taxonomy3);
                    $fccrl->setEveningHrs($evening_hrs);
                    $fccrl->setWeekendHrs($weekend_hrs);
                    $fccrl->setMonStart($mon_start);
                    $fccrl->setMonEnd($mon_end);
                    $fccrl->setTuesStart($tues_start);
                    $fccrl->setTuesEnd($tues_end);
                    $fccrl->setWedStart($wed_start);
                    $fccrl->setWedEnd($wed_end);
                    $fccrl->setThursStart($thurs_start);
                    $fccrl->setThursEnd($thurs_end);
                    $fccrl->setFriStart($fri_start);
                    $fccrl->setFriEnd($fri_end);
                    $fccrl->setSatStart($sat_start);
                    $fccrl->setSatEnd($sat_end);
                    $fccrl->setSunStart($sun_start);
                    $fccrl->setSunEnd($sun_end);
                    $fccrl->setPaytoStreet($payto_street);
                    $fccrl->setPaytoCity($payto_city);
                    $fccrl->setPaytoState($payto_state);
                    $fccrl->setPaytoZipcode($payto_zipcode);
                    $fccrl->setPaytoPhone($payto_phone);
                    $fccrl->setMinAge($min_age);
                    $fccrl->setMaxAge($max_age);

                    $em->persist($fccrl);
                }
            }

            $cont++;
        }
        $em->flush();

        return new Response('All OK');
    }

    /**
     * @Route("/disruptor-report-changes", name="fcc_disruptor_report_changes")
     */
    public function disruptorReportChanges(){
        set_time_limit(3600);

        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');;

        $spreadsheet = $reader->load($url.'fcc_roster_template.xlsx');
        $em=$this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $providers=$em->getRepository('App\Entity\ViewProvider')->findBy(array('disabled'=>0,'valid_npi_number'=>1));

        //cont for new
        $cont=2;

        //cont for changes
        $cont_changes=2;
        //cont for terminates
        $cont_terminated=2;
        $organizations_result=[];
        $pt_order=['25','26','30','29','31','07','32','81','97','01','04','16','68','66','05','77','67','08','14','25','26','30','29','31','07','32','81','97'];

        $exclude_npis=[];
        $exclude_ids=[]; //providers excludes on a organization
        $sql="SELECT p.provider_id, p.payer_id, provider.npi_number as npi
            FROM  provider_payer_exclude p
            LEFT JOIN provider ON provider.id = p.provider_id";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $excludes_p_ids= $stmt->fetchAll();

        foreach ($excludes_p_ids as $ids_excludes){
            $exclude_npis[]=$ids_excludes['npi'];
        }

        //get exclude organization
        $sql2="SELECT organization.id as organization_id
            FROM  organization_payer_exclude o
            LEFT JOIN organization ON organization.id = o.organization_id
            LEFT JOIN payer ON payer.id = o.payer_id
            WHERE payer.id=4";

        $stmt2 = $conn->prepare($sql2);
        $stmt2->execute();
        $excludes_o_ids= $stmt2->fetchAll();

        foreach ($excludes_o_ids as $org_id){
            $providers_org=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$org_id['organization_id']));
            if($providers_org!=null){
                foreach ($providers_org as $provider){
                    $exclude_ids[]=$provider->getId();
                }
            }
        }

        $providerNPIS=[];
        foreach ($providers as $provider){
            $npi_number=$provider->getNpiNumber();
            $organization_id=$provider->getOrganization()->getId();
            $id=$provider->getId();

            if(!in_array($id,$exclude_ids) and $npi_number!="N/A" and $npi_number!="" and $npi_number!="NOT AVAILABLE" and in_array($npi_number,$providerNPIS)==false and !in_array($npi_number,$exclude_npis) ){
                $providerNPIS[]=$npi_number;
                $pmls=$em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>'A'));
                $pareffdt="";

                $pro_org=$provider->getOrganization();
                $contCredF=0;
                if($pro_org!=null){
                    $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$pro_org->getId()));
                    if($address!=null){
                        foreach ($address as $addr){
                            $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId()));
                            if($credentialingsF!=null){
                                foreach ($credentialingsF as $cf){
                                    $contCredF++;
                                }
                            }
                        }
                    }
                }

                $is_credentialing=false;

                $credRecord=$em->getRepository('App\Entity\ProviderCredentialing')->findOneBy(array('credentialing_status'=>4, 'provider'=>$provider->getId()));
                if($credRecord!=null){
                    $effective_date=$credRecord->getCredentialingEffectiveDate();
                    if($effective_date!=""){
                        $effD=explode('/',$effective_date);
                        $pareffdt=$effD[2].$effD[0].'01';
                    }

                    $is_credentialing=true;
                }

                if($contCredF>0 and $credRecord==null){
                    $is_credentialing=true;
                    $pro_org=$provider->getOrganization();
                    if($pro_org!=null){
                        $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$pro_org->getId()));
                        if($address!=null){
                            foreach ($address as $addr){
                                $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId(),'credentialing_status'=>4));
                                if($credentialingsF!=null){
                                    foreach ($credentialingsF as $cf){
                                        $dateFacEffe=$cf->getCredentialingEffectiveDate();

                                        if($dateFacEffe!=""){
                                            $dateFacEffe=explode('/',$dateFacEffe);
                                            $year=$dateFacEffe[2];
                                            $moth=$dateFacEffe[0];

                                            $pareffdt=$year.$moth.'01';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $address=$provider->getBillingAddress();
                $private_key=$this->getParameter('private_key');
                $encoder=new My_Mcript($private_key);
                $hospitals=$provider->getHospitals();

                $ahca_id="";
                if($hospitals!=null){
                    foreach ($hospitals as $hospital){
                        $ahca_id=$hospital->getAhcaNumber();
                    }
                }

                $social=$encoder->decryptthis($provider->getSocial());
                $orga=$provider->getOrganization();
                if($orga->getTinNumber()!=""){
                    $irs=$orga->getTinNumber();
                    $irs_type="T";
                }else{
                    $irs=$social;
                    $irs_type="S";
                }

                $irs=str_replace("-","",$irs);
                if(strlen($irs)==8){
                    $irs="0".$irs;
                }

                $key_cont=1;

                $isDr="";
                $degrees=$provider->getDegree();
                if($degrees!=null){
                    foreach ($degrees as $degree){
                        $isDr=$degree->getName();
                    }
                }

                $genderCode=$provider->getGender();
                if($genderCode!=""){
                    $genderCode=substr($genderCode,0,1);
                    $genderCode=strtoupper($genderCode);
                }else{
                    $genderCode="";
                }

                $language1="";$language2="";$language3="";$language4="";$language5="";$language6="";$language7="";$language8="";
                $languages=$provider->getLanguages();
                if($languages!=null){
                    if(count($languages)>0){
                        $language1=$languages[0]->getLangCd();
                    }
                    if(count($languages)>1){
                        $language2=$languages[1]->getLangCd();
                    }
                    if(count($languages)>2){
                        $language3=$languages[2]->getLangCd();
                    }
                    if(count($languages)>3){
                        $language4=$languages[3]->getLangCd();
                    }
                    if(count($languages)>4){
                        $language5=$languages[4]->getLangCd();
                    }
                    if(count($languages)>5){
                        $language6=$languages[5]->getLangCd();
                    }
                    if(count($languages)>6){
                        $language7=$languages[6]->getLangCd();
                    }
                    if(count($languages)>7){
                        $language8=$languages[7]->getLangCd();
                    }
                }

                $is_valid=false;
                $provider_type="";
                $taxonomy1="";$taxonomy2="";$taxonomy3="";
                $taxonomies=array();
                $medicaids=array();
                $medicaid="";
                $ind_or_group="";
                $providerTypesArray=array();

                $specialty1 = "";
                $specialty2 = "";
                $specialty3 = "";
                $specialty4 = "";
                $specialtiesArray=array();
                $stateLic="";
                $code="";

                if($pmls!=null){
                    foreach ($pmls as $pml){
                        if($pml->getColW()=="A"){
                            $is_valid=true;
                            $pt=$pml->getColD();
                            if($pt!=""){
                                $providerTypesArray[]=$pt;
                                $stateLic=$pml->getColV();
                            }
                        }

                        $spe=$pml->getColE();
                        if($spe!=""){
                            if(!in_array($spe,$specialtiesArray)){
                                $specialtiesArray[]=$spe;
                            }
                        }
                    }

                    $match_pt=0;
                    foreach ($providerTypesArray as $spt){
                        foreach ($pt_order as $pt_o){
                            if($pt_o==$spt and $match_pt==0){
                                $code=$spt;
                                $match_pt=1;
                                break 2;
                            }
                        }
                    }

                    if(strlen($code)==1){
                        $code="00".$code;
                    }

                    if(strlen($code)==2){
                        $code="0".$code;
                    }
                    $provider_type=$code;

                    if(count($specialtiesArray)>=1){
                        $specialty1=$specialtiesArray[0]."01";
                    }
                    if(count($specialtiesArray)>=2){
                        $specialty2=$specialtiesArray[1]."01";
                    }
                    if(count($specialtiesArray)>=3){
                        $specialty3=$specialtiesArray[2]."01";
                    }
                    if(count($specialtiesArray)>=4){
                        $specialty4=$specialtiesArray[3]."01";
                    }

                    if(count($pmls)>=1){
                        $specialtiesCodes1= $pmls[0]->getColE();
                        if(strlen($specialtiesCodes1)==2){
                            $specialtiesCodes1="0".$specialtiesCodes1;
                        }
                        if($pmls[0]->getColF()!=""){
                            $taxonomies[]=$pmls[0]->getColF();
                        }

                        $medicaids[]=$pmls[0]->getColA();
                        $ind_or_group=$pmls[0]->getColU();
                    }

                    if(count($pmls)>=2){
                        $specialtiesCodes2= $pmls[1]->getColE();
                        if(strlen($specialtiesCodes2)==2){
                            $specialtiesCodes2="0".$specialtiesCodes2;
                        }
                        if($pmls[1]->getColF()!=""){
                            $taxonomies[]=$pmls[1]->getColF();
                        }
                        $medicaids[]=$pmls[1]->getColA();
                    }

                    if(count($pmls)>=3){
                        $specialtiesCodes3= $pmls[2]->getColE();
                        if(strlen($specialtiesCodes3)==2){
                            $specialtiesCodes3="0".$specialtiesCodes3;
                        }
                        if($pmls[2]->getColF()!=""){
                            $taxonomies[]=$pmls[2]->getColF();
                        }
                        $medicaids[]=$pmls[2]->getColA();
                    }

                    if(count($pmls)>=4){
                        $specialtiesCodes4= $pmls[3]->getColE();
                        if(strlen($specialtiesCodes4)==2){
                            $specialtiesCodes4="0".$specialtiesCodes4;
                        }
                    }

                    if(count($taxonomies)>=1){
                        $taxonomy1=$taxonomies[0];
                    }
                    if(count($taxonomies)>=2){
                        $taxonomy2=$taxonomies[1];
                    }
                    if(count($taxonomies)>=3){
                        $taxonomy3=$taxonomies[2];
                    }

                    if($taxonomy1==""){
                        $pro_taxonomies=$provider->getTaxonomyCodes();
                        if($pro_taxonomies!=null){
                            if(count($pro_taxonomies)>=1){
                                $taxonomy1=$pro_taxonomies[0]->getCode();
                            }
                            if(count($pro_taxonomies)>=2){
                                $taxonomy1=$pro_taxonomies[1]->getCode();
                            }
                            if(count($pro_taxonomies)>=3){
                                $taxonomy1=$pro_taxonomies[2]->getCode();
                            }
                        }
                    }

                    if(count($medicaids)>=1){
                        if($medicaids[0]!=""){
                            $medicaid=$medicaids[0];
                        }else{
                            if(count($medicaids)>=2){
                                if($medicaids[1]!=""){
                                    $medicaid=$medicaids[1];
                                }
                            }
                        }
                    }
                }

                if($pmls!=null and $is_valid==true and $is_credentialing==true){
                    if($address!=null){
                        foreach ($address as $addr){
                            $street_check=$addr->getStreet();
                            $addr_pass=true;
                            if(stristr($street_check, 'BOX') or stristr($street_check, 'Box') or stristr($street_check, 'box')) {
                                $addr_pass=false;
                            }
                            if($addr_pass==true){
                                $eClaimProviderType = "SP";
                                $pro_degrees = $provider->getDegree();
                                if ($pro_degrees != null) {
                                    foreach ($pro_degrees as $degree) {
                                        if ($degree->getId() == 1) {
                                            $eClaimProviderType = "NP";
                                            break;
                                        }
                                        if ($degree->getId() == 19) {
                                            if($specialty1!='92901' and $specialty2!='92901' and $specialty3!='92901' and $specialty4!='92901'){
                                                $eClaimProviderType = "PA";
                                                break;
                                            }
                                        }
                                    }
                                }

                                $site_index="";
                                if($key_cont<10){
                                    $key=$provider->getNpiNumber()."-".'00'.$key_cont;
                                    $site_index="00".$key_cont;
                                }else{
                                    $key=$provider->getNpiNumber()."-".'0'.$key_cont;
                                    $site_index="0".$key_cont;
                                }

                                $minAge=strval($addr->getOfficeAgeLimit());
                                $maxAge=strval($addr->getOfficeAgeLimitMax());

                                if(strlen($minAge)==1){
                                    $minAge='0'.$minAge;
                                }
                                $minAge=$minAge."Y";

                                if(strlen($maxAge)==1){
                                    $maxAge='0'.$maxAge;
                                }
                                $maxAge=$maxAge."Y";

                                $org_id=$provider->getOrganization()->getId();
                                $medicaid_percent="100";
                                $org_rate=$em->getRepository('App\Entity\OrganizationRates')->findOneBy(array('organization'=>$org_id,'rate'=>2));
                                if($org_rate!=null){
                                    $medicaid_percent=$org_rate->getPercent();
                                }
                                $medicaid_percent=$medicaid_percent."%";

                                $hasWeekend="N";
                                $hasEvening="N";
                                $exempt="";
                                if($addr->getSeePatientsAddr()==false){
                                    $exempt="DE";
                                }


                                $phone_number = $addr->getPhoneNumber();
                                $phone_number = str_replace('-', "", $phone_number);

                                $license=$provider->getStateLic();
                                if($license=="not found" or $license=="NOTFOUND"){ $license=""; }
                                $license=str_replace(" ","",$license);

                                if($license==""){
                                    if($provider_type=="032"){
                                        $license="99999";
                                    }
                                }

                                $license=strval($license);

                                $license_match=false;
                                $cont_license=0;
                                foreach ($pml as $pt) {
                                    if($pt->getColV()!="" and $pt->getColV()!=null){
                                        $license_pml=$pt->getColV();
                                        $cont_license++;
                                        if($license_pml==$license){
                                            $license_match=true;
                                        }
                                    }
                                }

                                if(stristr($license, 'ARNP')) {
                                    $license=  str_replace("ARNP","APRN",$license);
                                }

                                $has_rbt=false;
                                if(stristr($license, 'RBT')) {
                                    $has_rbt=true;
                                }

                                $first_latter_license=substr($license,0,1);
                                $not_match_pml=0;
                                if($license=="" and $cont_license>0){
                                    if($provider_type=="039"){
                                        $not_match_pml=1;
                                    }
                                }

                                if($license!="" and $license_match==false){
                                    if($provider_type=="039"){
                                        $not_match_pml=1;
                                    }
                                }

                                if($first_latter_license!="I" and $first_latter_license!="i" and $license!="n/a" and $license!="N/A" and $license!="notavailable" and $license!="NotAvailable"
                                    and $has_rbt==false and $not_match_pml==0 and $license!="" and $provider_type!=""){

                                    $degrees=$provider->getDegree();
                                    $degreePass=0;
                                    if($degrees!=null){
                                        foreach ($degrees as $degree){
                                            if($degree->getId()==20 or $degree->getId()==24 or $degree->getId()==22 or $degree->getId()==30 or $degree->getId()==32){
                                                $degreePass=1;
                                            }
                                        }
                                    }

                                    $organizationId=$provider->getOrganization();
                                    if(!in_array($organizationId,$organizations_result)){
                                        $organizations_result[]=$organizationId;
                                    }

                                    $countyObj = $addr->getCountyMatch();
                                    $county_code = "";
                                    if ($countyObj != null) {
                                        $county_code = $countyObj->getCountyCode();
                                    }

                                    $is_new=0;
                                    $is_changed=0;
                                    $is_terminated=0;

                                    $bussinesOurs=$addr->getBusinessHours();
                                    //sunday
                                    $dayActive_sun="";
                                    $startHour_sun="";
                                    $tillHour_sun="";
                                    //monday
                                    $dayActive_mon="";
                                    $startHour_mon="";
                                    $tillHour_mon="";
                                    //tuesday
                                    $dayActive_tue="";
                                    $startHour_tue="";
                                    $tillHour_tue="";
                                    //Wednesday
                                    $dayActive_wed="";
                                    $startHour_wed="";
                                    $tillHour_wed="";
                                    //Thurs
                                    $dayActive_thurs="";
                                    $startHour_thurs="";
                                    $tillHour_thurs="";
                                    //Frid
                                    $dayActive_fri="";
                                    $startHour_fri="";
                                    $tillHour_fri="";
                                    //sat
                                    $dayActive_sat="";
                                    $startHour_sat="";
                                    $tillHour_sat="";
                                    if($bussinesOurs!=""){
                                        $bussinesOurs=substr($bussinesOurs,1);
                                        $bussinesOurs=substr($bussinesOurs,0,-1);

                                        $bussinesOurs=str_replace('{','',$bussinesOurs);
                                        $bussinesOurs=str_replace('}','',$bussinesOurs);
                                        $bussinesOurs=str_replace('"','',$bussinesOurs);

                                        $daysHours = explode(",", $bussinesOurs);
                                        //sunday
                                        $dayActive_sun=explode(":",$daysHours[18])[1];
                                        $startHour_sun=substr($daysHours[19],9);
                                        $tillHour_sun=substr($daysHours[20],9);
                                        //monday
                                        $dayActive_mon=explode(":",$daysHours[0])[1];
                                        $startHour_mon=substr($daysHours[1],9);
                                        $tillHour_mon=substr($daysHours[2],9);
                                        //tuesday
                                        $dayActive_tue=explode(":",$daysHours[3])[1];
                                        $startHour_tue=substr($daysHours[4],9);
                                        $tillHour_tue=substr($daysHours[5],9);
                                        //Wednesday
                                        $dayActive_wed=explode(":",$daysHours[6])[1];
                                        $startHour_wed=substr($daysHours[7],9);
                                        $tillHour_wed=substr($daysHours[8],9);
                                        //Thurs
                                        $dayActive_thurs=explode(":",$daysHours[9])[1];
                                        $startHour_thurs=substr($daysHours[10],9);
                                        $tillHour_thurs=substr($daysHours[11],9);
                                        //Frid
                                        $dayActive_fri=explode(":",$daysHours[12])[1];
                                        $startHour_fri=substr($daysHours[13],9);
                                        $tillHour_fri=substr($daysHours[14],9);
                                        //sat
                                        $dayActive_sat=explode(":",$daysHours[15])[1];
                                        $startHour_sat=substr($daysHours[16],9);
                                        $tillHour_sat=substr($daysHours[17],9);
                                    }

                                    //check if the providers is a new
                                    $roster_log=$em->getRepository('App\Entity\FCCRosterLog')->findOneBy(array('id_key'=>$key));
                                    if($roster_log==null){
                                       $is_new=1;
                                    }else{
                                        $cont_chgn=0;
                                        $new_last_name=strtolower($provider->getLastName());
                                        $old_last_name=strtolower($roster_log->getLastName());

                                        if($new_last_name!=$old_last_name){ $cont_chgn++;}
                                        if($isDr!=$roster_log->getProvTitle()){ $cont_chgn++; }
                                        if($eClaimProviderType!=$roster_log->getEclaimsProviderType()){ $cont_chgn++; }
                                        if($provider_type!=$roster_log->getAhcaProviderType()){ $cont_chgn++; }
                                        if($specialty1!=$roster_log->getSpec1()){ $cont_chgn++; }
                                        if($specialty2!=$roster_log->getSpec2()){ $cont_chgn++; }
                                        if($specialty3!=$roster_log->getSpec3()){ $cont_chgn++; }
                                        if($taxonomy1!=$roster_log->getTaxonomy1()){ $cont_chgn++; }
                                        if($taxonomy2!=$roster_log->getTaxonomy2()){ $cont_chgn++; }
                                        if($taxonomy3!=$roster_log->getTaxonomy3()){ $cont_chgn++; }

                                        if($addr->getStreet()!=$roster_log->getPracStreet()){ $cont_chgn++; }
                                        if($addr->getSuiteNumber()!=$roster_log->getPracticeSuite()){ $cont_chgn++; }
                                        if($addr->getCity()!=$roster_log->getPracCity()){ $cont_chgn++; }
                                        if($addr->getUsState()!=$roster_log->getPracState()){ $cont_chgn++; }
                                        if($addr->getZipCode()!=$roster_log->getPracZipcode()){ $cont_chgn++; }
                                        if($phone_number!=$roster_log->getPracPhone()){ $cont_chgn++; }

                                        if(strtolower($startHour_sun)!=strtolower($roster_log->getSunStart())){
                                            if($startHour_sun=="" and $roster_log->getSunStart()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($tillHour_sun)!=strtolower($roster_log->getSunEnd())){
                                            if($tillHour_sun=="" and $roster_log->getSunEnd()!=null){
                                               $cont_chgn++;
                                            }
                                        }

                                        if(strtolower($startHour_mon)!=strtolower($roster_log->getMonStart())){
                                            if($startHour_mon=="" and $roster_log->getMonStart()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($tillHour_mon)!=strtolower($roster_log->getMonEnd())){
                                            if($tillHour_mon=="" and $roster_log->getMonEnd()!=null){
                                                $cont_chgn++;
                                            }
                                        }

                                        if(strtolower($startHour_tue)!=strtolower($roster_log->getTuesStart())){
                                            if($startHour_tue=="" and $roster_log->getTuesStart()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($tillHour_tue)!=strtolower($roster_log->getTuesEnd())){
                                            if($tillHour_tue=="" and $roster_log->getTuesEnd()!=null){
                                                $cont_chgn++;
                                            }
                                        }

                                        if(strtolower($startHour_wed)!=strtolower($roster_log->getWedStart())){
                                            if($startHour_wed=="" and $roster_log->getWedStart()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($tillHour_wed)!=strtolower($roster_log->getWedEnd())){
                                            if($tillHour_wed=="" and $roster_log->getWedEnd()!=null){
                                                $cont_chgn++;
                                            }
                                        }

                                        if(strtolower($startHour_thurs)!=strtolower($roster_log->getThursStart())){
                                            if($startHour_thurs=="" and $roster_log->getThursStart()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($tillHour_thurs)!=strtolower($roster_log->getThursEnd())){
                                            if($tillHour_thurs=="" and $roster_log->getThursEnd()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($startHour_fri)!=strtolower($roster_log->getFriStart())){
                                            if($startHour_fri=="" and $roster_log->getFriStart()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($tillHour_fri)!=strtolower($roster_log->getFriEnd())){
                                            if($tillHour_fri=="" and $roster_log->getFriEnd()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($startHour_sat)!=strtolower($roster_log->getSatStart())){
                                            if($startHour_sat=="" and $roster_log->getSatStart()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($tillHour_sat)!=strtolower($roster_log->getSatEnd())){
                                            if($tillHour_sat=="" and $roster_log->getSatEnd()!=null){
                                                $cont_chgn++;
                                            }
                                        }

                                        if($cont_chgn>0){
                                            $is_changed=1;
                                        }
                                    }

                                    if($is_changed==1){
                                        $spreadsheet->setActiveSheetIndex(2);
                                        $cell_A='A'.$cont_changes;$cell_C='C'.$cont_changes;$cell_D='D'.$cont_changes;$cell_E='E'.$cont_changes;$cell_F='F'.$cont_changes;$cell_G='G'.$cont_changes;$cell_H='H'.$cont_changes;
                                        $cell_J='J'.$cont_changes;$cell_L='L'.$cont_changes;$cell_N='N'.$cont_changes;$cell_P='P'.$cont_changes;$cell_U='U'.$cont_changes;$cell_V='V'.$cont_changes;$cell_W='W'.$cont_changes;
                                        $cell_X='X'.$cont_changes;$cell_Y='Y'.$cont_changes;$cell_Z='Z'.$cont_changes;
                                        $cell_AA='AA'.$cont_changes; $cell_AB='AB'.$cont_changes; $cell_AC='AC'.$cont_changes; $cell_AD='AD'.$cont_changes; $cell_AE='AE'.$cont_changes; $cell_AF='AF'.$cont_changes; $cell_AG='AG'.$cont_changes;
                                        $cell_AI='AI'.$cont_changes; $cell_AJ='AJ'.$cont_changes; $cell_AL='AL'.$cont_changes; $cell_AM='AM'.$cont_changes; $cell_AN='AN'.$cont_changes; $cell_AO='AO'.$cont_changes;$cell_AY='AY'.$cont_changes;
                                        $cell_AP='AP'.$cont_changes; $cell_AQ='AQ'.$cont_changes; $cell_AR='AR'.$cont_changes; $cell_AT='AT'.$cont_changes; $cell_AU='AU'.$cont_changes;$cell_AW='AW'.$cont_changes;$cell_AX='AX'.$cont_changes;
                                        $cell_BA='BA'.$cont_changes;$cell_BB='BB'.$cont_changes;$cell_BC='BC'.$cont_changes;$cell_BD='BD'.$cont_changes;$cell_BJ='BJ'.$cont_changes;$cell_BK='BK'.$cont_changes;$cell_BL='BL'.$cont_changes;
                                        $cell_BM='BM'.$cont_changes;$cell_BN='BN'.$cont_changes;$cell_BO='BO'.$cont_changes;$cell_BP='BP'.$cont_changes;$cell_BQ='BQ'.$cont_changes;$cell_BR='BR'.$cont_changes;$cell_BS='BS'.$cont_changes;
                                        $cell_BT='BT'.$cont_changes;$cell_BU='BU'.$cont_changes;$cell_BV='BV'.$cont_changes;$cell_BW='BW'.$cont_changes;$cell_BX='BX'.$cont_changes;
                                        $cell_DI='DI'.$cont_changes;$cell_DJ='DJ'.$cont_changes;$cell_DK='DK'.$cont_changes;$cell_DL='DL'.$cont_changes;$cell_DN='DN'.$cont_changes;
                                    }


                                    if($is_new==1){
                                        $cont++;
                                    }
                                    if($is_changed==1){
                                        $cont_changes++;
                                    }
                                    $key_cont++;

                                    if($degreePass==0 and $is_changed==1){

                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $key);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $provider->getLastName());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $provider->getFirstName());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $isDr);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, 'Y');
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $eClaimProviderType);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $provider_type);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $specialty1);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $specialty2);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $specialty3);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_P, $specialty4);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_U, $provider->getNpiNumber());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_V, $medicaid);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_W, $license);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_X, $ahca_id);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_Y, $genderCode);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_Z, $language1);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AA, $language2);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AB, $language3);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AC, $language4);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AD, $language5);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AE, $language6);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AF, $language7);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AG, $language8);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AI, $site_index);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AL, $irs_type);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AM, $irs);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AN, $addr->getStreet());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AO, $addr->getSuiteNumber());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AP, $addr->getCity());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AQ, $addr->getUsState());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AR, $addr->getZipCode());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AT, $phone_number);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AU, $county_code);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BA, $ind_or_group);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AJ, '20210101');

                                        $ada="N";
                                        if($addr->getWheelchair()==true){
                                            $ada="Y";
                                        }

                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AW, $ada);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AX, 'Y');
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AY, 'B');

                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BB, $taxonomy1);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BC, $taxonomy2);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BD, $taxonomy3);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BJ, "N");

                                        $bussinesOurs=$addr->getBusinessHours();
                                        if($bussinesOurs!=""){
                                            if($dayActive_sun=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BW,strtoupper($startHour_sun));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BX,strtoupper($tillHour_sun));
                                            }
                                            if($dayActive_mon=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BK,strtoupper($startHour_mon));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BL,strtoupper($tillHour_mon));
                                            }
                                            if($dayActive_tue=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BM,strtoupper($startHour_tue));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BN,strtoupper($tillHour_tue));
                                            }
                                            if($dayActive_wed=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BO,strtoupper($startHour_wed));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BP,strtoupper($tillHour_wed));
                                            }
                                            if($dayActive_thurs=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BQ,strtoupper($startHour_thurs));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BR,strtoupper($tillHour_thurs));
                                            }
                                            if($dayActive_fri=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BS,strtoupper($startHour_fri));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BT,strtoupper($tillHour_fri));
                                            }
                                            if($dayActive_sat=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BU,strtoupper($startHour_sat));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BV,strtoupper($tillHour_sat));
                                            }
                                        }

                                        //get billing Address
                                        $orgId=null;
                                        if($provider->getOrganization()!=null){
                                            $orgId=$provider->getOrganization()->getId();
                                        }
                                        $orgAddress=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$orgId));

                                        if(count($orgAddress)==1){
                                            foreach ($orgAddress as $addrB){

                                                $phone_number_B = $addrB->getPhoneNumber();
                                                $phone_number_B = str_replace('-', "", $phone_number_B);

                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DI, $addrB->getStreet());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DJ, $addrB->getCity());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DK, $addrB->getUsState());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DL, $addrB->getZipCode());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DN, $phone_number_B);
                                            }
                                        }

                                        $street="";
                                        $city="";
                                        $usState="";
                                        $zipCode="";
                                        $hasBilling=false;

                                        if(count($orgAddress)>1){
                                            foreach ($orgAddress as $addrB){
                                                if($addrB->getBillingAddr()==true){
                                                    $hasBilling=true;
                                                    $street=$addrB->getStreet();
                                                    $city=$addrB->getCity();
                                                    $usState=$addrB->getUsState();
                                                    $zipCode=$addrB->getZipCode();
                                                }
                                            }

                                            if($hasBilling==true){
                                                $phone_number_B = $addrB->getPhoneNumber();
                                                $phone_number_B = str_replace('-', "", $phone_number_B);

                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DI, $street);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DJ, $city);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DK, $usState);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DL, $zipCode);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DN, $phone_number_B);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        foreach ($organizations_result as $organizationID){
           $organization=$em->getRepository('App\Entity\Organization')->find($organizationID);
            $languagesId=[];
            if($organization!=null and $organization->getDisabled()==0 and $organization->getOrganizationStatus()->getId()==2){
                $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                $key_cont=1;

                if($address!=null){
                    foreach ($address as $addr){
                        $street_check=$addr->getStreet();
                        $pass=1;

                        if(stristr($street_check, 'BOX')==true or stristr($street_check, 'Box')==true or stristr($street_check, 'box')==true) {
                            $pass=0;
                        }

                        if($pass==1){
                            $pmls=array();
                            if($addr->getLocationNpi()!=""){
                                $pmls=$em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$organization->getGroupNpi(),'colW'=>'A'));
                            }else{
                                $npiR=$organization->getGroupNpi();
                                $pmls=$em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$npiR,'colW'=>'A'));
                            }

                            $site_index = "";
                            $ind_or_group = "";

                            if ($key_cont < 10) {
                                $key = $organization->getGroupNpi() . "-" . '00' . $key_cont;
                                $site_index = "00" . $key_cont;
                            } else {
                                $key = $organization->getGroupNpi() . "-" . '0' . $key_cont;
                                $site_index = "0" . $key_cont;
                            }

                            $providerType = "";
                            $providerTypesArray=array();
                            $specialty1 = "";
                            $specialty2 = "";
                            $specialty3 = "";
                            $specialty4 = "";

                            $taxonomyCode1="";
                            $taxonomyCode2="";
                            $taxonomyCode3="";

                            $types=array();
                            $specialtiesArray=array();
                            $taxonomiesCode=array();

                            if(count($pmls)>0){
                                foreach ($pmls as $pml){
                                    $type=$pml->getColD();
                                    if($type!="" and $type!=null){
                                        $providerTypesArray[]=$type;
                                    }

                                    $spe=$pml->getColE();
                                    $tax=$pml->getColF();

                                    if($type!=""){
                                        $types[]=$type;
                                    }
                                    if($spe!=""){
                                        if(!in_array($spe,$specialtiesArray)){
                                            $specialtiesArray[]=$spe;
                                        }
                                    }
                                    if($tax!=""){
                                        if(!in_array($tax,$taxonomiesCode)){
                                            $taxonomiesCode[]=$tax;
                                        }
                                    }
                                }

                                $code="";
                                $match_pt=0;
                                foreach ($providerTypesArray as $spt){
                                    foreach ($pt_order as $pt_o){
                                        if($pt_o==$spt and $match_pt==0){
                                            $code=$spt;
                                            $match_pt=1;
                                        }
                                    }
                                }

                                if(strlen($code)==1){
                                    $code="00".$code;
                                }
                                if(strlen($code)==2){
                                    $code="0".$code;
                                }

                                $providerType=$code;

                                if(count($specialtiesArray)>=1){
                                    $specialty1=$specialtiesArray[0]."01";
                                }
                                if(count($specialtiesArray)>=2){
                                    $specialty2=$specialtiesArray[1]."01";
                                }
                                if(count($specialtiesArray)>=3){
                                    $specialty3=$specialtiesArray[2]."01";
                                }
                                if(count($specialtiesArray)>=4){
                                    $specialty4=$specialtiesArray[3]."01";
                                }
                                if(count($taxonomiesCode)>=1){
                                    $taxonomyCode1=$taxonomiesCode[0];
                                }
                                if(count($taxonomiesCode)>=2){
                                    $taxonomyCode2=$taxonomiesCode[1];
                                }
                                if(count($taxonomiesCode)>=3){
                                    $taxonomyCode3=$taxonomiesCode[2];
                                }
                            }

                            $pareffdt="";
                            $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));

                            if($address!=null){
                                foreach ($address as $addr3){
                                    $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr3->getId(),'credentialing_status'=>4));
                                    if($credentialingsF!=null){
                                        foreach ($credentialingsF as $cf){
                                            $dateFac=$cf->getCredentialingAcceptedDate();
                                            $dateFacEffe=$cf->getCredentialingEffectiveDate();

                                            $year="";
                                            $moth='';
                                            if($dateFacEffe!=""){
                                                $effD=explode('/',$dateFacEffe);
                                                $year=$effD[2];
                                                $moth=$effD[0];
                                            }
                                            $pareffdt=$year.$moth.'01';
                                        }
                                    }else{
                                        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId()));
                                        if($providers!=null){
                                            foreach ($providers as $provider){
                                                $credentialings_Tem=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status'=>4,'provider'=>$provider->getId()));
                                                $datesCExist=[];
                                                if($credentialings_Tem!=null){
                                                    foreach ($credentialings_Tem as $credeTem){
                                                        $dateT=$credeTem->getCredentialingEffectiveDate();
                                                        if($dateT!=""){
                                                            if(!in_array($dateT,$datesCExist)){
                                                                $datesCExist[]=$dateT;
                                                            }
                                                        }
                                                    }
                                                }
                                                //total of different dates from credentialing providers
                                                $totalDates=count($datesCExist);
                                                if($totalDates==1){
                                                    $dateC1= explode('/',$datesCExist[0]);
                                                    $yearT=$dateC1[2];
                                                    $mothT=$dateC1[0];
                                                    $pareffdt=$yearT.$mothT."01";
                                                }
                                                if($totalDates>1){
                                                    $dateC1= explode('/',$datesCExist[0]);
                                                    $yearT=$dateC1[2];
                                                    $mothT=$dateC1[0];
                                                    $pareffdt=$yearT.$mothT."01";
                                                }

                                                if(count($specialtiesArray)==0){
                                                    $pmls=$em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>"A"));
                                                    if($pmls!=null){
                                                        foreach ($pmls as $pml) {
                                                            $spe=$pml->getColE();
                                                            if($spe!=""){
                                                                if(!in_array($spe,$specialtiesArray)){
                                                                    $specialtiesArray[]=$spe;
                                                                }
                                                            }
                                                            $type=$pml->getColD();
                                                            if($type!="" and $type!=null){
                                                                if(!in_array($type,$providerTypesArray)){
                                                                    $providerTypesArray[]=$type;
                                                                }
                                                            }
                                                        }

                                                        $code="";
                                                        $match_pt=0;
                                                        foreach ($providerTypesArray as $spt){
                                                            foreach ($pt_order as $pt_o){
                                                                if($pt_o==$spt and $match_pt==0){
                                                                    $code=$spt;
                                                                    $match_pt=1;
                                                                }
                                                            }
                                                        }

                                                        if(strlen($code)==1){
                                                            $code="00".$code;
                                                        }
                                                        if(strlen($code)==2){
                                                            $code="0".$code;
                                                        }
                                                        $providerType=$code;
                                                        if(count($specialtiesArray)>=1){
                                                            $specialty1=$specialtiesArray[0]."01";
                                                        }
                                                        if(count($specialtiesArray)>=2){
                                                            $specialty2=$specialtiesArray[1]."01";
                                                        }
                                                        if(count($specialtiesArray)>=3){
                                                            $specialty3=$specialtiesArray[2]."01";
                                                        }
                                                        if(count($specialtiesArray)>=4){
                                                            $specialty4=$specialtiesArray[3]."01";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            $street_check2=$addr->getStreet();
                            if(stristr($street_check2, 'BOX')==false and stristr($street_check2, 'Box')==false and stristr($street_check2, 'box')==false) {
                                $org_rate=$em->getRepository('App\Entity\OrganizationRates')->findOneBy(array('organization'=>$organization->getId(),'rate'=>2));

                                $eClaimProviderType="GR";
                                $providerTypeOrder=['01','04','16','68','66','05','77','67','08','14','25','26','30','29','31','07','32','81','97'];
                                $groupNpi=$organization->getGroupNpi();
                                $proTypeArray=array();
                                $pmlsArray=array();
                                if($groupNpi!=""){
                                    $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$groupNpi,'colW'=>"A"));
                                    if($pmls!=null){
                                        foreach ($pmls as $pml) {
                                            $proTypeCode = $pml->getColD();
                                            if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                                $proTypeArray[]=$proTypeArray;
                                                $pmlsArray[]=$pml;
                                            }
                                        }
                                    }
                                }

                                if(count($proTypeArray)==0){
                                    if($providers!=null) {
                                        foreach ($providers as $provider) {
                                            $languages=$provider->getLanguages();
                                            if($languages!=null){
                                                foreach ($languages as $lng){
                                                    if(!in_array($lng->getId(),$languagesId)){
                                                        $languagesId[]=$lng->getId();
                                                    }
                                                }
                                            }
                                            $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>"A"));
                                            if($pmls!=null){
                                                foreach ($pmls as $pml) {
                                                    $proTypeCode = $pml->getColD();
                                                    if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                                        $proTypeArray[]=$proTypeArray;
                                                        $pmlsArray[]=$pml;
                                                    }
                                                }
                                            }
                                        }

                                        $language1 = "";
                                        $language2 = "";
                                        $language3 = "";
                                        $language4 = "";
                                        $language5 = "";
                                        $language6 = "";
                                        $language7 = "";
                                        $language8 = "";
                                        $languagesArray = array();
                                        if ($languagesId != null) {
                                            foreach ($languagesId as $id) {
                                                $language = $em->getRepository('App\Entity\Languages')->find($id);
                                                if ($language != null) {
                                                    $languagesArray[] = $language;
                                                }
                                            }
                                        }

                                        if ($languagesArray != null) {
                                            if (count($languagesArray) > 0) {
                                                $language1 = $languagesArray[0]->getLangCd();
                                            }
                                            if (count($languagesArray) > 1) {
                                                $language2 = $languagesArray[1]->getLangCd();
                                            }
                                            if (count($languagesArray) > 2) {
                                                $language3 = $languagesArray[2]->getLangCd();
                                            }
                                            if (count($languagesArray) > 3) {
                                                $language4 = $languagesArray[3]->getLangCd();
                                            }
                                            if (count($languagesArray) > 4) {
                                                $language5 = $languagesArray[4]->getLangCd();
                                            }
                                            if (count($languagesArray) > 5) {
                                                $language6 = $languagesArray[5]->getLangCd();
                                            }
                                            if (count($languagesArray) > 6) {
                                                $language7 = $languagesArray[6]->getLangCd();
                                            }
                                            if (count($languagesArray) > 7) {
                                                $language8 = $languagesArray[7]->getLangCd();
                                            }
                                        }

                                        if($language1==""){
                                            $language1="ENG";
                                        }
                                    }
                                }

                                $pt_code="";
                                foreach ($pmlsArray as $pt){
                                    foreach ($providerTypeOrder as $order){
                                        if($pt->getColD()==$order){
                                            $pt_code=$pt->getColD();
                                            $specialty_code=$pt->getColE()."01";
                                            $medicaid_code=$pt->getColA();
                                            break 2;
                                        }
                                    }
                                }

                                $medicaid_length=strlen($medicaid_code);
                                if($medicaid_length<9){
                                    $rest=9-$medicaid_length;
                                    while ($rest>0){
                                        $medicaid_code="0".$medicaid_code;
                                        $rest--;
                                    }
                                }

                                if(strlen($pt_code)==1){
                                    $pt_code="00".$pt_code;
                                }
                                if(strlen($pt_code)==2){
                                    $pt_code="0".$pt_code;
                                }

                                if($providerType==""){
                                    $providerType=$pt_code;
                                }

                                if($specialty1==""){
                                    $specialty1=$specialty_code;
                                }

                                if($specialty1=="20001" or $specialty1=="20101" or $specialty1=="90101" or $specialty1=="30001" or $specialty1=="30101"){
                                    $eClaimProviderType="HS";
                                }

                                if($specialty2=="20001" or $specialty2=="20101" or $specialty2=="90101" or $specialty2=="30001" or $specialty2=="30101"){
                                    $eClaimProviderType="HS";
                                }

                                if($specialty3=="20001" or $specialty3=="20101" or $specialty3=="90101" or $specialty3=="30001" or $specialty3=="30101"){
                                    $eClaimProviderType="HS";
                                }

                                if($providerType!=""){
                                    $phone_number = $addr->getPhoneNumber();
                                    $phone_number = str_replace('-', "", $phone_number);
                                    $is_changed=0;

                                    //check if the providers is a new
                                    $roster_log=$em->getRepository('App\Entity\FCCRosterLog')->findOneBy(array('id_key'=>$key));

                                    if($roster_log==null){
                                        $is_new=1;
                                    }else{
                                        $cont_chgn=0;
                                        $new_last_name=strtolower($organization->getName());
                                        $old_last_name=strtolower($roster_log->getLastName());

                                        if($new_last_name!=$old_last_name){ $cont_chgn++;}
                                        if($eClaimProviderType!=$roster_log->getEclaimsProviderType()){ $cont_chgn++; }
                                        if($provider_type!=$roster_log->getAhcaProviderType()){ $cont_chgn++; }
                                        if($specialty1!=$roster_log->getSpec1()){ $cont_chgn++; }
                                        if($specialty2!=$roster_log->getSpec2()){ $cont_chgn++; }
                                        if($specialty3!=$roster_log->getSpec3()){ $cont_chgn++; }
                                        if($language1!=$roster_log->getLng1()){ $cont_chgn++; }
                                        if($language2!=$roster_log->getLng2()){ $cont_chgn++; }
                                        if($language3!=$roster_log->getLng3()){ $cont_chgn++; }
                                        if($language4!=$roster_log->getLng4()){ $cont_chgn++; }
                                        if($language5!=$roster_log->getLng5()){ $cont_chgn++; }
                                        if($language6!=$roster_log->getLng6()){ $cont_chgn++; }
                                        if($language7!=$roster_log->getLng7()){ $cont_chgn++; }
                                        if($language8!=$roster_log->getLng8()){ $cont_chgn++; }

                                        if($addr->getStreet()!=$roster_log->getPracStreet()){ $cont_chgn++; }
                                        if($addr->getSuiteNumber()!=$roster_log->getPracticeSuite()){ $cont_chgn++; }
                                        if($addr->getCity()!=$roster_log->getPracCity()){ $cont_chgn++; }
                                        if($addr->getUsState()!=$roster_log->getPracState()){ $cont_chgn++; }
                                        if($addr->getZipCode()!=$roster_log->getPracZipcode()){ $cont_chgn++; }
                                        if($phone_number!=$roster_log->getPracPhone()){ $cont_chgn++; }
                                        if($taxonomy1!=$roster_log->getTaxonomy1()){ $cont_chgn++; }
                                        if($taxonomy2!=$roster_log->getTaxonomy2()){ $cont_chgn++; }
                                        if($taxonomy3!=$roster_log->getTaxonomy3()){ $cont_chgn++; }

                                        if($cont_chgn>0){
                                            $is_changed=1;
                                        }
                                    }

                                    if($is_changed==1){
                                        $spreadsheet->setActiveSheetIndex(2);
                                        $cell_A = 'A' . $cont_changes;$cell_B = 'B' . $cont_changes;$cell_C = 'C' . $cont_changes;$cell_D = 'D' . $cont_changes;
                                        $cell_E = 'E' . $cont_changes;$cell_F = 'F' . $cont_changes;$cell_G = 'G' . $cont_changes;$cell_H = 'H' . $cont_changes;
                                        $cell_I = 'I' . $cont_changes;$cell_J = 'J' . $cont_changes;$cell_K = 'K' . $cont_changes;$cell_L = 'L' . $cont_changes;
                                        $cell_M = 'M' . $cont_changes;$cell_N = 'N' . $cont_changes;$cell_O = 'O' . $cont_changes;$cell_P = 'P' . $cont_changes;
                                        $cell_Q = 'Q' . $cont_changes;$cell_R = 'R' . $cont_changes;$cell_S = 'S' . $cont_changes;$cell_T = 'T' . $cont_changes;$cell_U = 'U' . $cont_changes;$cell_V = 'V' . $cont_changes;
                                        $cell_W = 'W' . $cont_changes;$cell_X = 'X' . $cont_changes;$cell_Y = 'Y' . $cont_changes;$cell_Z = 'Z' . $cont_changes;

                                        $cell_AA = 'AA' . $cont_changes;$cell_AB = 'AB' . $cont_changes;$cell_AC = 'AC' . $cont_changes;$cell_AD = 'AD' . $cont_changes;
                                        $cell_AE = 'AE' . $cont_changes;$cell_AF = 'AF' . $cont_changes;$cell_AG = 'AG' . $cont_changes;$cell_AH = 'AH' . $cont_changes;$cell_AI = 'AI' . $cont_changes;$cell_AJ = 'AJ' . $cont_changes;$cell_AK = 'AK' . $cont_changes;
                                        $cell_AL = 'AL' . $cont_changes;$cell_AM = 'AM' . $cont_changes;$cell_AN = 'AN' . $cont_changes;$cell_AO = 'AO' . $cont_changes;$cell_AP = 'AP' . $cont_changes;
                                        $cell_AQ = 'AQ' . $cont_changes;$cell_AR = 'AR' . $cont_changes;$cell_AS = 'AS' . $cont_changes;$cell_AT = 'AT' . $cont_changes;$cell_AU = 'AU' . $cont_changes;
                                        $cell_AV = 'AV' . $cont_changes;$cell_AW = 'AW' . $cont_changes;$cell_AX = 'AX' . $cont_changes;$cell_AY = 'AY' . $cont_changes;$cell_AZ = 'AZ' . $cont_changes;

                                        $cell_BA = 'BA' . $cont_changes;$cell_BB = 'BB' . $cont_changes;$cell_BC = 'BC' . $cont_changes;$cell_BD = 'BD' . $cont_changes;$cell_BE = 'BE' . $cont_changes;
                                        $cell_BF = 'BF' . $cont_changes;$cell_BG = 'BG' . $cont_changes;$cell_BH = 'BH' . $cont_changes;$cell_BI = 'BI' . $cont_changes;$cell_BJ = 'BJ' . $cont_changes;
                                        $cell_BK = 'BK' . $cont_changes;$cell_BL = 'BL' . $cont_changes;$cell_BM = 'BM' . $cont_changes;$cell_BN = 'BN' . $cont_changes;$cell_BO = 'BO' . $cont_changes;
                                        $cell_BP = 'BP' . $cont_changes;$cell_BQ = 'BQ' . $cont_changes;$cell_BR = 'BR' . $cont_changes;$cell_BS = 'BS' . $cont_changes;$cell_BT = 'BT' . $cont_changes;
                                        $cell_BU = 'BU' . $cont_changes;$cell_BV = 'BV' . $cont_changes;$cell_BW = 'BW' . $cont_changes;$cell_BX = 'BX' . $cont_changes;$cell_BY = 'BY' . $cont_changes;$cell_BZ = 'BZ' . $cont_changes;

                                        $cell_CA = 'CA' . $cont_changes;$cell_CB = 'CB' . $cont_changes;
                                        $cell_CC = 'CC' . $cont_changes;$cell_CD = 'CD' . $cont_changes;$cell_CE = 'CE' . $cont_changes;$cell_CF = 'CF' . $cont_changes;
                                        $cell_CG = 'CG' . $cont_changes;$cell_CH = 'CH' . $cont_changes;$cell_CI = 'CI' . $cont_changes;$cell_CJ = 'CJ' . $cont_changes;
                                        $cell_CK = 'CK' . $cont_changes;$cell_CL = 'CL' . $cont_changes;$cell_CM = 'CM' . $cont_changes;$cell_CN = 'CN' . $cont_changes;
                                        $cell_CO = 'CO' . $cont_changes;$cell_CP = 'CP' . $cont_changes;$cell_CQ = 'CQ' . $cont_changes;$cell_CR = 'CR' . $cont_changes;
                                        $cell_CS = 'CS' . $cont_changes;$cell_CT = 'CT' . $cont_changes;$cell_CU = 'CU' . $cont_changes;$cell_CV = 'CV' . $cont_changes;
                                        $cell_CW = 'CW' . $cont_changes;$cell_CX = 'CX' . $cont_changes;$cell_CY = 'CY' . $cont_changes;$cell_CZ = 'CZ' . $cont_changes;

                                        $cell_DA = 'DA' . $cont_changes;$cell_DB = 'DB' . $cont_changes;$cell_DC = 'DC' . $cont_changes;$cell_DD = 'DD' . $cont_changes;$cell_DE = 'DE' . $cont_changes;
                                        $cell_DF = 'DF' . $cont_changes;$cell_DG = 'DG' . $cont_changes;$cell_DH = 'DH' . $cont_changes;$cell_DI = 'DI' . $cont_changes;$cell_DJ = 'DJ' . $cont_changes;$cell_DK = 'DK' . $cont_changes;$cell_DL = 'DL' . $cont_changes;$cell_DM = 'DM' . $cont_changes;
                                        $cell_DN = 'DN' . $cont_changes;$cell_DO = 'DO' . $cont_changes;$cell_DP = 'DP' . $cont_changes;
                                        $cell_DQ = 'DQ' . $cont_changes;$cell_DR = 'DR' . $cont_changes;$cell_DT = 'DT' . $cont_changes;
                                        $cell_DU='DU'.$cont_changes;;$cell_DV='DV'.$cont_changes;$cell_DY='DY'.$cont_changes;$cell_DZ='DZ'.$cont_changes;

                                        $cont_changes++;
                                    }

                                    if($is_changed==1){
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, $key);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_H, $providerType);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI, $site_index);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, strtoupper($organization->getName()));
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, 'N'); //Switch Flag
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_U, $organization->getGroupNpi());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_V, $medicaid_code);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_G, $eClaimProviderType);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $specialty1);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $specialty2);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, $specialty3);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_P, $specialty4);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AY, 'B');
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AJ, '20210101');

                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_Z, $language1);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AA, $language2);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AB, $language3);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AC, $language4);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AD, $language5);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AE, $language6);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AF, $language7);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AG, $language8);

                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AN, $addr->getStreet());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AO, $addr->getSuiteNumber());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AP, $addr->getCity());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AQ, $addr->getUsState());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AR, $addr->getZipCode());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AT, $phone_number);

                                        $ada = "N";
                                        if ($addr->getWheelchair() == true) {
                                            $ada = "Y";
                                        }

                                        if($taxonomyCode1==""){
                                            $org_taxonomies=$em->getRepository('App\Entity\OrganizationTaxonomy')->findBy(array('organization'=>$organization->getId()));
                                            if($org_taxonomies!=null){
                                                if(count($org_taxonomies)>=1){
                                                    $taxonomyCode1=$org_taxonomies[0]->getTaxonomy()->getCode();
                                                }
                                                if(count($org_taxonomies)>=2){
                                                    $taxonomyCode2=$org_taxonomies[1]->getTaxonomy()->getCode();
                                                }
                                                if(count($org_taxonomies)>=3){
                                                    $taxonomyCode3=$org_taxonomies[2]->getTaxonomy()->getCode();
                                                }
                                            }
                                        }

                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AW, $ada);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AX, 'Y');
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BB, $taxonomyCode1);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BC, $taxonomyCode2);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BD, $taxonomyCode3);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BJ, "N");

                                        $bussinesOurs = $addr->getBusinessHours();
                                        if($bussinesOurs!=""){
                                            $bussinesOurs = substr($bussinesOurs, 1);
                                            $bussinesOurs = substr($bussinesOurs, 0, -1);

                                            $bussinesOurs = str_replace('{', '', $bussinesOurs);
                                            $bussinesOurs = str_replace('}', '', $bussinesOurs);
                                            $bussinesOurs = str_replace('"', '', $bussinesOurs);

                                            $daysHours = explode(",", $bussinesOurs);

                                            //sunday
                                            $dayActive = explode(":", $daysHours[18])[1];
                                            $startHour = substr($daysHours[19], 9);
                                            $tillHour = substr($daysHours[20], 9);

                                            if ($dayActive == "true") {
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BW, strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BX, strtoupper($tillHour));

                                                $hasWeekend="Y";
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //monday
                                            $dayActive = explode(":", $daysHours[0])[1];
                                            $startHour = substr($daysHours[1], 9);
                                            $tillHour = substr($daysHours[2], 9);
                                            if ($dayActive == "true") {
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BK, strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BL, strtoupper($tillHour));
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //tuesday
                                            $dayActive = explode(":", $daysHours[3])[1];
                                            $startHour = substr($daysHours[4], 9);
                                            $tillHour = substr($daysHours[5], 9);
                                            if ($dayActive == "true") {
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BM, strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BN, strtoupper($tillHour));
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //Wednesday
                                            $dayActive = explode(":", $daysHours[6])[1];
                                            $startHour = substr($daysHours[7], 9);
                                            $tillHour = substr($daysHours[8], 9);
                                            if ($dayActive == "true") {
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BO, strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BP, strtoupper($tillHour));
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //Thurs
                                            $dayActive = explode(":", $daysHours[9])[1];
                                            $startHour = substr($daysHours[10], 9);
                                            $tillHour = substr($daysHours[11], 9);
                                            if ($dayActive == "true") {
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BQ, strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BR, strtoupper($tillHour));
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //Frid
                                            $dayActive = explode(":", $daysHours[12])[1];
                                            $startHour = substr($daysHours[13], 9);
                                            $tillHour = substr($daysHours[14], 9);
                                            if ($dayActive == "true") {
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BS, strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BT, strtoupper($tillHour));
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //sat
                                            $dayActive = explode(":", $daysHours[15])[1];
                                            $startHour = substr($daysHours[16], 9);
                                            $tillHour = substr($daysHours[17], 9);
                                            if ($dayActive == "true") {
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BU, strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BV, strtoupper($tillHour));

                                                $hasWeekend="Y";
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }
                                        }

                                        //get billing Address
                                        $orgAddress = $em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $organization->getId()));

                                        if (count($orgAddress) == 1) {
                                            foreach ($orgAddress as $addrB) {
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DI, $addrB->getStreet());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DJ, $addrB->getCity());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DK, $addrB->getUsState());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DL, $addrB->getZipCode());
                                                $phone_number_B = $addrB->getPhoneNumber();
                                                $phone_number_B = str_replace('-', "", $phone_number_B);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DN,$phone_number_B);
                                            }
                                        }

                                        $street = "";
                                        $city = "";
                                        $usState = "";
                                        $zipCode = "";
                                        $phoneNumber = "";
                                        $hasBilling = false;

                                        if (count($orgAddress) > 1) {
                                            foreach ($orgAddress as $addrB) {
                                                if ($addrB->getBillingAddr() == true) {
                                                    $hasBilling = true;
                                                    $street = $addrB->getStreet();
                                                    $city = $addrB->getCity();
                                                    $usState = $addrB->getUsState();
                                                    $zipCode = $addrB->getZipCode();
                                                    $phone_number_B = $addrB->getPhoneNumber();
                                                    $phone_number_B = str_replace('-', "", $phone_number_B);
                                                }
                                            }

                                            if ($hasBilling == true) {
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DI, $street);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DJ, $city);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DK, $usState);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DL, $zipCode);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DN, $phone_number_B);
                                            }
                                        }

                                        $tin = $organization->getTinNumber();

                                        if ($tin != "") {
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AL, 'T');
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AM, $tin);
                                        }else{
                                            $provider=$em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number'=>$organization->getGroupNpi()));
                                            if($provider!=null){
                                                $tin=$encoder->decryptthis($provider->getSocial());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AL, 'S');
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AM, $tin);
                                            }
                                        }

                                        $key_cont++;
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="FCC_BSN_Roster'.date('mdY_his').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/disruptor-report-terminated", name="fcc_disruptor_report_terminated")
     */
    public function disruptorReportTerminated(){
        set_time_limit(3600);

        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');;

        $spreadsheet = $reader->load($url.'fcc_roster_template.xlsx');
        $em=$this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $providers=$em->getRepository('App\Entity\ViewProvider')->findBy(array('disabled'=>0,'valid_npi_number'=>1));

        //cont for new
        $cont=2;

        //cont for changes
        $cont_changes=2;
        //cont for terminates
        $cont_terminated=2;
        $organizations_result=[];
        $pt_order=['25','26','30','29','31','07','32','81','97','01','04','16','68','66','05','77','67','08','14','25','26','30','29','31','07','32','81','97'];

        $prividers_to_terminate=[756,1275,3579,2203];

        $exclude_npis=[];
        $exclude_ids=[]; //providers excludes on a organization
        $sql="SELECT p.provider_id, p.payer_id, provider.npi_number as npi
            FROM  provider_payer_exclude p
            LEFT JOIN provider ON provider.id = p.provider_id";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $excludes_p_ids= $stmt->fetchAll();

        foreach ($excludes_p_ids as $ids_excludes){
            $exclude_npis[]=$ids_excludes['npi'];
        }
        //get exclude organization
        $sql2="SELECT organization.id as organization_id
            FROM  organization_payer_exclude o
            LEFT JOIN organization ON organization.id = o.organization_id
            LEFT JOIN payer ON payer.id = o.payer_id
            WHERE payer.id=4";

        $stmt2 = $conn->prepare($sql2);
        $stmt2->execute();
        $excludes_o_ids= $stmt2->fetchAll();

        foreach ($excludes_o_ids as $org_id){
            $providers_org=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$org_id['organization_id']));
            if($providers_org!=null){
                foreach ($providers_org as $provider){
                    $exclude_ids[]=$provider->getId();
                }
            }
        }

        $providerNPIS=[];
        foreach ($providers as $provider){
            $npi_number=$provider->getNpiNumber();
            $id=$provider->getId();
            if(in_array($id,$prividers_to_terminate)){
                $providerNPIS[]=$npi_number;
                $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>'A'));
                $pareffdt="";

                $pro_org=$provider->getOrganization();
                $contCredF=0;
                if($pro_org!=null){
                    $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$pro_org->getId()));
                    if($address!=null){
                        foreach ($address as $addr){
                            $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId()));
                            if($credentialingsF!=null){
                                foreach ($credentialingsF as $cf){
                                    $contCredF++;
                                }
                            }
                        }
                    }
                }

                $is_credentialing=false;

                $credRecord=$em->getRepository('App\Entity\ProviderCredentialing')->findOneBy(array('credentialing_status'=>4, 'provider'=>$provider->getId()));
                if($credRecord!=null){
                    $effective_date=$credRecord->getCredentialingEffectiveDate();
                    if($effective_date!=""){
                        $effD=explode('/',$effective_date);
                        $pareffdt=$effD[2].$effD[0].'01';
                    }

                    $is_credentialing=true;
                }

                if($contCredF>0 and $credRecord==null){
                    $is_credentialing=true;
                    $pro_org=$provider->getOrganization();
                    if($pro_org!=null){
                        $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$pro_org->getId()));
                        if($address!=null){
                            foreach ($address as $addr){
                                $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId(),'credentialing_status'=>4));
                                if($credentialingsF!=null){
                                    foreach ($credentialingsF as $cf){
                                        $dateFacEffe=$cf->getCredentialingEffectiveDate();

                                        if($dateFacEffe!=""){
                                            $dateFacEffe=explode('/',$dateFacEffe);
                                            $year=$dateFacEffe[2];
                                            $moth=$dateFacEffe[0];

                                            $pareffdt=$year.$moth.'01';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $address=$provider->getBillingAddress();
                $private_key=$this->getParameter('private_key');
                $encoder=new My_Mcript($private_key);
                $hospitals=$provider->getHospitals();

                $ahca_id="";
                if($hospitals!=null){
                    foreach ($hospitals as $hospital){
                        $ahca_id=$hospital->getAhcaNumber();
                    }
                }

                $social=$encoder->decryptthis($provider->getSocial());
                $orga=$provider->getOrganization();
                if($orga->getTinNumber()!=""){
                    $irs=$orga->getTinNumber();
                    $irs_type="T";
                }else{
                    $irs=$social;
                    $irs_type="S";
                }

                $irs=str_replace("-","",$irs);
                if(strlen($irs)==8){
                    $irs="0".$irs;
                }

                $key_cont=1;

                $isDr="";
                $degrees=$provider->getDegree();
                if($degrees!=null){
                    foreach ($degrees as $degree){
                        $isDr=$degree->getName();
                    }
                }

                $genderCode=$provider->getGender();
                if($genderCode!=""){
                    $genderCode=substr($genderCode,0,1);
                    $genderCode=strtoupper($genderCode);
                }else{
                    $genderCode="";
                }

                $language1="";$language2="";$language3="";$language4="";$language5="";$language6="";$language7="";$language8="";
                $languages=$provider->getLanguages();
                if($languages!=null){
                    if(count($languages)>0){
                        $language1=$languages[0]->getLangCd();
                    }
                    if(count($languages)>1){
                        $language2=$languages[1]->getLangCd();
                    }
                    if(count($languages)>2){
                        $language3=$languages[2]->getLangCd();
                    }
                    if(count($languages)>3){
                        $language4=$languages[3]->getLangCd();
                    }
                    if(count($languages)>4){
                        $language5=$languages[4]->getLangCd();
                    }
                    if(count($languages)>5){
                        $language6=$languages[5]->getLangCd();
                    }
                    if(count($languages)>6){
                        $language7=$languages[6]->getLangCd();
                    }
                    if(count($languages)>7){
                        $language8=$languages[7]->getLangCd();
                    }
                }

                $is_valid=false;
                $provider_type="";
                $taxonomy1="";$taxonomy2="";$taxonomy3="";
                $taxonomies=array();
                $medicaids=array();
                $medicaid="";
                $ind_or_group="";
                $providerTypesArray=array();

                $specialty1 = "";
                $specialty2 = "";
                $specialty3 = "";
                $specialty4 = "";
                $specialtiesArray=array();
                $stateLic="";
                $code="";

                if($pmls!=null){
                    foreach ($pmls as $pml){
                        if($pml->getColW()=="A"){
                            $is_valid=true;
                            $pt=$pml->getColD();
                            if($pt!=""){
                                $providerTypesArray[]=$pt;
                                $stateLic=$pml->getColV();
                            }
                        }

                        $spe=$pml->getColE();
                        if($spe!=""){
                            if(!in_array($spe,$specialtiesArray)){
                                $specialtiesArray[]=$spe;
                            }
                        }
                    }

                    $match_pt=0;
                    foreach ($providerTypesArray as $spt){
                        foreach ($pt_order as $pt_o){
                            if($pt_o==$spt and $match_pt==0){
                                $code=$spt;
                                $match_pt=1;
                                break 2;
                            }
                        }
                    }

                    if(strlen($code)==1){
                        $code="00".$code;
                    }

                    if(strlen($code)==2){
                        $code="0".$code;
                    }
                    $provider_type=$code;

                    if(count($specialtiesArray)>=1){
                        $specialty1=$specialtiesArray[0]."01";
                    }
                    if(count($specialtiesArray)>=2){
                        $specialty2=$specialtiesArray[1]."01";
                    }
                    if(count($specialtiesArray)>=3){
                        $specialty3=$specialtiesArray[2]."01";
                    }
                    if(count($specialtiesArray)>=4){
                        $specialty4=$specialtiesArray[3]."01";
                    }

                    if(count($pmls)>=1){
                        $specialtiesCodes1= $pmls[0]->getColE();
                        if(strlen($specialtiesCodes1)==2){
                            $specialtiesCodes1="0".$specialtiesCodes1;
                        }
                        if($pmls[0]->getColF()!=""){
                            $taxonomies[]=$pmls[0]->getColF();
                        }

                        $medicaids[]=$pmls[0]->getColA();
                        $ind_or_group=$pmls[0]->getColU();
                    }

                    if(count($pmls)>=2){
                        $specialtiesCodes2= $pmls[1]->getColE();
                        if(strlen($specialtiesCodes2)==2){
                            $specialtiesCodes2="0".$specialtiesCodes2;
                        }
                        if($pmls[1]->getColF()!=""){
                            $taxonomies[]=$pmls[1]->getColF();
                        }
                        $medicaids[]=$pmls[1]->getColA();
                    }

                    if(count($pmls)>=3){
                        $specialtiesCodes3= $pmls[2]->getColE();
                        if(strlen($specialtiesCodes3)==2){
                            $specialtiesCodes3="0".$specialtiesCodes3;
                        }
                        if($pmls[2]->getColF()!=""){
                            $taxonomies[]=$pmls[2]->getColF();
                        }
                        $medicaids[]=$pmls[2]->getColA();
                    }

                    if(count($pmls)>=4){
                        $specialtiesCodes4= $pmls[3]->getColE();
                        if(strlen($specialtiesCodes4)==2){
                            $specialtiesCodes4="0".$specialtiesCodes4;
                        }
                    }

                    if(count($taxonomies)>=1){
                        $taxonomy1=$taxonomies[0];
                    }
                    if(count($taxonomies)>=2){
                        $taxonomy2=$taxonomies[1];
                    }
                    if(count($taxonomies)>=3){
                        $taxonomy3=$taxonomies[2];
                    }

                    if($taxonomy1==""){
                        $pro_taxonomies=$provider->getTaxonomyCodes();
                        if($pro_taxonomies!=null){
                            if(count($pro_taxonomies)>=1){
                                $taxonomy1=$pro_taxonomies[0]->getCode();
                            }
                            if(count($pro_taxonomies)>=2){
                                $taxonomy1=$pro_taxonomies[1]->getCode();
                            }
                            if(count($pro_taxonomies)>=3){
                                $taxonomy1=$pro_taxonomies[2]->getCode();
                            }
                        }
                    }

                    if(count($medicaids)>=1){
                        if($medicaids[0]!=""){
                            $medicaid=$medicaids[0];
                        }else{
                            if(count($medicaids)>=2){
                                if($medicaids[1]!=""){
                                    $medicaid=$medicaids[1];
                                }
                            }
                        }
                    }
                }

                if($pmls!=null and $is_valid==true and $is_credentialing==true){
                    if($address!=null){
                        foreach ($address as $addr){
                            $street_check=$addr->getStreet();
                            $addr_pass=true;
                            if(stristr($street_check, 'BOX') or stristr($street_check, 'Box') or stristr($street_check, 'box')) {
                                $addr_pass=false;
                            }
                            if($addr_pass==true){
                                $eClaimProviderType = "SP";
                                $pro_degrees = $provider->getDegree();
                                if ($pro_degrees != null) {
                                    foreach ($pro_degrees as $degree) {
                                        if ($degree->getId() == 1) {
                                            $eClaimProviderType = "NP";
                                            break;
                                        }
                                        if ($degree->getId() == 19) {
                                            if($specialty1!='92901' and $specialty2!='92901' and $specialty3!='92901' and $specialty4!='92901'){
                                                $eClaimProviderType = "PA";
                                                break;
                                            }
                                        }
                                    }
                                }

                                $site_index="";
                                if($key_cont<10){
                                    $key=$provider->getNpiNumber()."-".'00'.$key_cont;
                                    $site_index="00".$key_cont;
                                }else{
                                    $key=$provider->getNpiNumber()."-".'0'.$key_cont;
                                    $site_index="0".$key_cont;
                                }

                                $minAge=strval($addr->getOfficeAgeLimit());
                                $maxAge=strval($addr->getOfficeAgeLimitMax());

                                if(strlen($minAge)==1){
                                    $minAge='0'.$minAge;
                                }
                                $minAge=$minAge."Y";

                                if(strlen($maxAge)==1){
                                    $maxAge='0'.$maxAge;
                                }
                                $maxAge=$maxAge."Y";

                                $org_id=$provider->getOrganization()->getId();
                                $medicaid_percent="100";
                                $org_rate=$em->getRepository('App\Entity\OrganizationRates')->findOneBy(array('organization'=>$org_id,'rate'=>2));
                                if($org_rate!=null){
                                    $medicaid_percent=$org_rate->getPercent();
                                }
                                $medicaid_percent=$medicaid_percent."%";

                                $hasWeekend="N";
                                $hasEvening="N";
                                $exempt="";
                                if($addr->getSeePatientsAddr()==false){
                                    $exempt="DE";
                                }


                                $phone_number = $addr->getPhoneNumber();
                                $phone_number = str_replace('-', "", $phone_number);

                                $license=$provider->getStateLic();
                                if($license=="not found" or $license=="NOTFOUND"){ $license=""; }
                                $license=str_replace(" ","",$license);

                                if($license==""){
                                    if($provider_type=="032"){
                                        $license="99999";
                                    }
                                }

                                $license=strval($license);

                                $license_match=false;
                                $cont_license=0;
                                foreach ($pml as $pt) {
                                    if($pt->getColV()!="" and $pt->getColV()!=null){
                                        $license_pml=$pt->getColV();
                                        $cont_license++;
                                        if($license_pml==$license){
                                            $license_match=true;
                                        }
                                    }
                                }

                                if(stristr($license, 'ARNP')) {
                                    $license=  str_replace("ARNP","APRN",$license);
                                }

                                $has_rbt=false;
                                if(stristr($license, 'RBT')) {
                                    $has_rbt=true;
                                }

                                $first_latter_license=substr($license,0,1);
                                $not_match_pml=0;
                                if($license=="" and $cont_license>0){
                                    if($provider_type=="039"){
                                        $not_match_pml=1;
                                    }
                                }

                                if($license!="" and $license_match==false){
                                    if($provider_type=="039"){
                                        $not_match_pml=1;
                                    }
                                }

                                if($first_latter_license!="I" and $first_latter_license!="i" and $license!="n/a" and $license!="N/A" and $license!="notavailable" and $license!="NotAvailable"
                                    and $has_rbt==false and $not_match_pml==0 and $license!="" and $provider_type!=""){

                                    $degrees=$provider->getDegree();
                                    $degreePass=0;
                                    if($degrees!=null){
                                        foreach ($degrees as $degree){
                                            if($degree->getId()==20 or $degree->getId()==24 or $degree->getId()==22 or $degree->getId()==30 or $degree->getId()==32){
                                                $degreePass=1;
                                            }
                                        }
                                    }

                                    $organizationId=$provider->getOrganization();
                                    if(!in_array($organizationId,$organizations_result)){
                                        $organizations_result[]=$organizationId;
                                    }

                                    $countyObj = $addr->getCountyMatch();
                                    $county_code = "";
                                    if ($countyObj != null) {
                                        $county_code = $countyObj->getCountyCode();
                                    }

                                    $is_new=0;
                                    $is_changed=0;
                                    $is_terminated=1;

                                    $bussinesOurs=$addr->getBusinessHours();
                                    //sunday
                                    $dayActive_sun="";
                                    $startHour_sun="";
                                    $tillHour_sun="";
                                    //monday
                                    $dayActive_mon="";
                                    $startHour_mon="";
                                    $tillHour_mon="";
                                    //tuesday
                                    $dayActive_tue="";
                                    $startHour_tue="";
                                    $tillHour_tue="";
                                    //Wednesday
                                    $dayActive_wed="";
                                    $startHour_wed="";
                                    $tillHour_wed="";
                                    //Thurs
                                    $dayActive_thurs="";
                                    $startHour_thurs="";
                                    $tillHour_thurs="";
                                    //Frid
                                    $dayActive_fri="";
                                    $startHour_fri="";
                                    $tillHour_fri="";
                                    //sat
                                    $dayActive_sat="";
                                    $startHour_sat="";
                                    $tillHour_sat="";
                                    if($bussinesOurs!=""){
                                        $bussinesOurs=substr($bussinesOurs,1);
                                        $bussinesOurs=substr($bussinesOurs,0,-1);

                                        $bussinesOurs=str_replace('{','',$bussinesOurs);
                                        $bussinesOurs=str_replace('}','',$bussinesOurs);
                                        $bussinesOurs=str_replace('"','',$bussinesOurs);

                                        $daysHours = explode(",", $bussinesOurs);
                                        //sunday
                                        $dayActive_sun=explode(":",$daysHours[18])[1];
                                        $startHour_sun=substr($daysHours[19],9);
                                        $tillHour_sun=substr($daysHours[20],9);
                                        //monday
                                        $dayActive_mon=explode(":",$daysHours[0])[1];
                                        $startHour_mon=substr($daysHours[1],9);
                                        $tillHour_mon=substr($daysHours[2],9);
                                        //tuesday
                                        $dayActive_tue=explode(":",$daysHours[3])[1];
                                        $startHour_tue=substr($daysHours[4],9);
                                        $tillHour_tue=substr($daysHours[5],9);
                                        //Wednesday
                                        $dayActive_wed=explode(":",$daysHours[6])[1];
                                        $startHour_wed=substr($daysHours[7],9);
                                        $tillHour_wed=substr($daysHours[8],9);
                                        //Thurs
                                        $dayActive_thurs=explode(":",$daysHours[9])[1];
                                        $startHour_thurs=substr($daysHours[10],9);
                                        $tillHour_thurs=substr($daysHours[11],9);
                                        //Frid
                                        $dayActive_fri=explode(":",$daysHours[12])[1];
                                        $startHour_fri=substr($daysHours[13],9);
                                        $tillHour_fri=substr($daysHours[14],9);
                                        //sat
                                        $dayActive_sat=explode(":",$daysHours[15])[1];
                                        $startHour_sat=substr($daysHours[16],9);
                                        $tillHour_sat=substr($daysHours[17],9);
                                    }

                                    //check if the providers is a new
                                    $roster_log=$em->getRepository('App\Entity\FCCRosterLog')->findOneBy(array('id_key'=>$key));
                                    if($roster_log==null){
                                        $is_new=1;
                                    }else{
                                        $cont_chgn=0;
                                        $new_last_name=strtolower($provider->getLastName());
                                        $old_last_name=strtolower($roster_log->getLastName());

                                        $new_name=strtolower($provider->getFirstName());
                                        $old_name=strtolower($roster_log->getFirstName());

                                        if($new_name!=$old_name){ $cont_chgn++; }
                                        if($new_last_name!=$old_last_name){ $cont_chgn++;}
                                        if($isDr!=$roster_log->getProvTitle()){ $cont_chgn++; }
                                        if($eClaimProviderType!=$roster_log->getEclaimsProviderType()){ $cont_chgn++; }
                                        if($provider_type!=$roster_log->getAhcaProviderType()){ $cont_chgn++; }
                                        if($specialty1!=$roster_log->getSpec1()){ $cont_chgn++; }
                                        if($specialty2!=$roster_log->getSpec2()){ $cont_chgn++; }
                                        if($specialty3!=$roster_log->getSpec3()){ $cont_chgn++; }
                                        if($language1!=$roster_log->getLng1()){ $cont_chgn++; }
                                        if($language2!=$roster_log->getLng2()){ $cont_chgn++; }
                                        if($language3!=$roster_log->getLng3()){ $cont_chgn++; }
                                        if($language4!=$roster_log->getLng4()){ $cont_chgn++; }
                                        if($language5!=$roster_log->getLng5()){ $cont_chgn++; }
                                        if($language6!=$roster_log->getLng6()){ $cont_chgn++; }
                                        if($language7!=$roster_log->getLng7()){ $cont_chgn++; }
                                        if($language8!=$roster_log->getLng8()){ $cont_chgn++; }

                                        if($taxonomy1!=$roster_log->getTaxonomy1()){ $cont_chgn++; }
                                        if($taxonomy2!=$roster_log->getTaxonomy2()){ $cont_chgn++; }
                                        if($taxonomy3!=$roster_log->getTaxonomy3()){ $cont_chgn++; }

                                        if($addr->getStreet()!=$roster_log->getPracStreet()){ $cont_chgn++; }
                                        if($addr->getSuiteNumber()!=$roster_log->getPracticeSuite()){ $cont_chgn++; }
                                        if($addr->getCity()!=$roster_log->getPracCity()){ $cont_chgn++; }
                                        if($addr->getUsState()!=$roster_log->getPracState()){ $cont_chgn++; }
                                        if($addr->getZipCode()!=$roster_log->getPracZipcode()){ $cont_chgn++; }
                                        if($phone_number!=$roster_log->getPracPhone()){ $cont_chgn++; }

                                        if(strtolower($startHour_sun)!=strtolower($roster_log->getSunStart())){
                                            if($startHour_sun=="" and $roster_log->getSunStart()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($tillHour_sun)!=strtolower($roster_log->getSunEnd())){
                                            if($tillHour_sun=="" and $roster_log->getSunEnd()!=null){
                                                $cont_chgn++;
                                            }
                                        }

                                        if(strtolower($startHour_mon)!=strtolower($roster_log->getMonStart())){
                                            if($startHour_mon=="" and $roster_log->getMonStart()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($tillHour_mon)!=strtolower($roster_log->getMonEnd())){
                                            if($tillHour_mon=="" and $roster_log->getMonEnd()!=null){
                                                $cont_chgn++;
                                            }
                                        }

                                        if(strtolower($startHour_tue)!=strtolower($roster_log->getTuesStart())){
                                            if($startHour_tue=="" and $roster_log->getTuesStart()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($tillHour_tue)!=strtolower($roster_log->getTuesEnd())){
                                            if($tillHour_tue=="" and $roster_log->getTuesEnd()!=null){
                                                $cont_chgn++;
                                            }
                                        }

                                        if(strtolower($startHour_wed)!=strtolower($roster_log->getWedStart())){
                                            if($startHour_wed=="" and $roster_log->getWedStart()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($tillHour_wed)!=strtolower($roster_log->getWedEnd())){
                                            if($tillHour_wed=="" and $roster_log->getWedEnd()!=null){
                                                $cont_chgn++;
                                            }
                                        }

                                        if(strtolower($startHour_thurs)!=strtolower($roster_log->getThursStart())){
                                            if($startHour_thurs=="" and $roster_log->getThursStart()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($tillHour_thurs)!=strtolower($roster_log->getThursEnd())){
                                            if($tillHour_thurs=="" and $roster_log->getThursEnd()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($startHour_fri)!=strtolower($roster_log->getFriStart())){
                                            if($startHour_fri=="" and $roster_log->getFriStart()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($tillHour_fri)!=strtolower($roster_log->getFriEnd())){
                                            if($tillHour_fri=="" and $roster_log->getFriEnd()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($startHour_sat)!=strtolower($roster_log->getSatStart())){
                                            if($startHour_sat=="" and $roster_log->getSatStart()!=null){
                                                $cont_chgn++;
                                            }
                                        }
                                        if(strtolower($tillHour_sat)!=strtolower($roster_log->getSatEnd())){
                                            if($tillHour_sat=="" and $roster_log->getSatEnd()!=null){
                                                $cont_chgn++;
                                            }
                                        }

                                        if($cont_chgn>0){
                                            $is_changed=1;
                                        }
                                    }

                                    $spreadsheet->setActiveSheetIndex(3);
                                    $cell_A='A'.$cont_changes;$cell_C='C'.$cont_changes;$cell_D='D'.$cont_changes;$cell_E='E'.$cont_changes;$cell_F='F'.$cont_changes;$cell_G='G'.$cont_changes;$cell_H='H'.$cont_changes;
                                    $cell_J='J'.$cont_changes;$cell_L='L'.$cont_changes;$cell_N='N'.$cont_changes;$cell_P='P'.$cont_changes;$cell_U='U'.$cont_changes;$cell_V='V'.$cont_changes;$cell_W='W'.$cont_changes;
                                    $cell_X='X'.$cont_changes;$cell_Y='Y'.$cont_changes;$cell_Z='Z'.$cont_changes;$cell_AK='AK'.$cont_changes;
                                    $cell_AA='AA'.$cont_changes; $cell_AB='AB'.$cont_changes; $cell_AC='AC'.$cont_changes; $cell_AD='AD'.$cont_changes; $cell_AE='AE'.$cont_changes; $cell_AF='AF'.$cont_changes; $cell_AG='AG'.$cont_changes;
                                    $cell_AI='AI'.$cont_changes; $cell_AJ='AJ'.$cont_changes; $cell_AL='AL'.$cont_changes; $cell_AM='AM'.$cont_changes; $cell_AN='AN'.$cont_changes; $cell_AO='AO'.$cont_changes;$cell_AY='AY'.$cont_changes;
                                    $cell_AP='AP'.$cont_changes; $cell_AQ='AQ'.$cont_changes; $cell_AR='AR'.$cont_changes; $cell_AT='AT'.$cont_changes; $cell_AU='AU'.$cont_changes;$cell_AW='AW'.$cont_changes;$cell_AX='AX'.$cont_changes;
                                    $cell_BA='BA'.$cont_changes;$cell_BB='BB'.$cont_changes;$cell_BC='BC'.$cont_changes;$cell_BD='BD'.$cont_changes;$cell_BJ='BJ'.$cont_changes;$cell_BK='BK'.$cont_changes;$cell_BL='BL'.$cont_changes;
                                    $cell_BM='BM'.$cont_changes;$cell_BN='BN'.$cont_changes;$cell_BO='BO'.$cont_changes;$cell_BP='BP'.$cont_changes;$cell_BQ='BQ'.$cont_changes;$cell_BR='BR'.$cont_changes;$cell_BS='BS'.$cont_changes;
                                    $cell_BT='BT'.$cont_changes;$cell_BU='BU'.$cont_changes;$cell_BV='BV'.$cont_changes;$cell_BW='BW'.$cont_changes;$cell_BX='BX'.$cont_changes;
                                    $cell_DI='DI'.$cont_changes;$cell_DJ='DJ'.$cont_changes;$cell_DK='DK'.$cont_changes;$cell_DL='DL'.$cont_changes;$cell_DN='DN'.$cont_changes;

                                    $cont_changes++;

                                    $key_cont++;

                                    if($degreePass==0 and ($is_new==1 or $is_changed==1 or $is_terminated==1)){

                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $key);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $provider->getLastName());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $provider->getFirstName());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $isDr);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, 'Y');
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $eClaimProviderType);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $provider_type);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $specialty1);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $specialty2);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $specialty3);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_P, $specialty4);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_U, $provider->getNpiNumber());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_V, $medicaid);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_W, $license);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_X, $ahca_id);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_Y, $genderCode);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_Z, $language1);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AA, $language2);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AB, $language3);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AC, $language4);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AD, $language5);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AE, $language6);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AF, $language7);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AG, $language8);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AI, $site_index);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AL, $irs_type);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AM, $irs);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AK, '20210121');
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AN, $addr->getStreet());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AO, $addr->getSuiteNumber());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AP, $addr->getCity());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AQ, $addr->getUsState());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AR, $addr->getZipCode());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AT, $phone_number);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AU, $county_code);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BA, $ind_or_group);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AJ, '20210101');

                                        $ada="N";
                                        if($addr->getWheelchair()==true){
                                            $ada="Y";
                                        }

                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AW, $ada);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AX, 'Y');
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AY, 'B');

                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BB, $taxonomy1);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BC, $taxonomy2);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BD, $taxonomy3);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BJ, "N");

                                        $bussinesOurs=$addr->getBusinessHours();
                                        if($bussinesOurs!=""){
                                            if($dayActive_sun=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BW,strtoupper($startHour_sun));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BX,strtoupper($tillHour_sun));
                                            }
                                            if($dayActive_mon=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BK,strtoupper($startHour_mon));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BL,strtoupper($tillHour_mon));
                                            }
                                            if($dayActive_tue=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BM,strtoupper($startHour_tue));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BN,strtoupper($tillHour_tue));
                                            }
                                            if($dayActive_wed=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BO,strtoupper($startHour_wed));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BP,strtoupper($tillHour_wed));
                                            }
                                            if($dayActive_thurs=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BQ,strtoupper($startHour_thurs));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BR,strtoupper($tillHour_thurs));
                                            }
                                            if($dayActive_fri=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BS,strtoupper($startHour_fri));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BT,strtoupper($tillHour_fri));
                                            }
                                            if($dayActive_sat=="true"){
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BU,strtoupper($startHour_sat));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BV,strtoupper($tillHour_sat));
                                            }
                                        }

                                        //get billing Address
                                        $orgId=null;
                                        if($provider->getOrganization()!=null){
                                            $orgId=$provider->getOrganization()->getId();
                                        }
                                        $orgAddress=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$orgId));

                                        if(count($orgAddress)==1){
                                            foreach ($orgAddress as $addrB){

                                                $phone_number_B = $addrB->getPhoneNumber();
                                                $phone_number_B = str_replace('-', "", $phone_number_B);

                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DI, $addrB->getStreet());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DJ, $addrB->getCity());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DK, $addrB->getUsState());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DL, $addrB->getZipCode());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DN, $phone_number_B);
                                            }
                                        }

                                        $street="";
                                        $city="";
                                        $usState="";
                                        $zipCode="";
                                        $hasBilling=false;

                                        if(count($orgAddress)>1){
                                            foreach ($orgAddress as $addrB){
                                                if($addrB->getBillingAddr()==true){
                                                    $hasBilling=true;
                                                    $street=$addrB->getStreet();
                                                    $city=$addrB->getCity();
                                                    $usState=$addrB->getUsState();
                                                    $zipCode=$addrB->getZipCode();
                                                }
                                            }

                                            if($hasBilling==true){
                                                $phone_number_B = $addrB->getPhoneNumber();
                                                $phone_number_B = str_replace('-', "", $phone_number_B);

                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DI, $street);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DJ, $city);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DK, $usState);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DL, $zipCode);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DN, $phone_number_B);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        foreach ($organizations_result as $organizationID){
            $organization=$em->getRepository('App\Entity\Organization')->find($organizationID);
            $languagesId=[];
            if($organization!=null and $organization->getDisabled()==0){
                $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                $key_cont=1;

                if($address!=null){
                    foreach ($address as $addr){
                        $street_check=$addr->getStreet();
                        $pass=1;

                        if(stristr($street_check, 'BOX')==true or stristr($street_check, 'Box')==true or stristr($street_check, 'box')==true) {
                            $pass=0;
                        }

                        if($pass==1){
                            $pmls=array();
                            if($addr->getLocationNpi()!=""){
                                $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$organization->getGroupNpi(),'colW'=>'A'));
                            }else{
                                $npiR=$organization->getGroupNpi();
                                $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$npiR,'colW'=>'A'));
                            }

                            $site_index = "";
                            $ind_or_group = "";

                            if ($key_cont < 10) {
                                $key = $organization->getGroupNpi() . "-" . '00' . $key_cont;
                                $site_index = "00" . $key_cont;
                            } else {
                                $key = $organization->getGroupNpi() . "-" . '0' . $key_cont;
                                $site_index = "0" . $key_cont;
                            }

                            $providerType = "";
                            $providerTypesArray=array();
                            $specialty1 = "";
                            $specialty2 = "";
                            $specialty3 = "";
                            $specialty4 = "";

                            $taxonomyCode1="";
                            $taxonomyCode2="";
                            $taxonomyCode3="";

                            $types=array();
                            $specialtiesArray=array();
                            $taxonomiesCode=array();

                            if(count($pmls)>0){
                                foreach ($pmls as $pml){
                                    $type=$pml->getColD();
                                    if($type!="" and $type!=null){
                                        $providerTypesArray[]=$type;
                                    }

                                    $spe=$pml->getColE();
                                    $tax=$pml->getColF();

                                    if($type!=""){
                                        $types[]=$type;
                                    }
                                    if($spe!=""){
                                        if(!in_array($spe,$specialtiesArray)){
                                            $specialtiesArray[]=$spe;
                                        }
                                    }
                                    if($tax!=""){
                                        if(!in_array($tax,$taxonomiesCode)){
                                            $taxonomiesCode[]=$tax;
                                        }
                                    }
                                }

                                $code="";
                                $match_pt=0;
                                foreach ($providerTypesArray as $spt){
                                    foreach ($pt_order as $pt_o){
                                        if($pt_o==$spt and $match_pt==0){
                                            $code=$spt;
                                            $match_pt=1;
                                        }
                                    }
                                }

                                if(strlen($code)==1){
                                    $code="00".$code;
                                }
                                if(strlen($code)==2){
                                    $code="0".$code;
                                }

                                $providerType=$code;

                                if(count($specialtiesArray)>=1){
                                    $specialty1=$specialtiesArray[0]."01";
                                }
                                if(count($specialtiesArray)>=2){
                                    $specialty2=$specialtiesArray[1]."01";
                                }
                                if(count($specialtiesArray)>=3){
                                    $specialty3=$specialtiesArray[2]."01";
                                }
                                if(count($specialtiesArray)>=4){
                                    $specialty4=$specialtiesArray[3]."01";
                                }
                                if(count($taxonomiesCode)>=1){
                                    $taxonomyCode1=$taxonomiesCode[0];
                                }
                                if(count($taxonomiesCode)>=2){
                                    $taxonomyCode2=$taxonomiesCode[1];
                                }
                                if(count($taxonomiesCode)>=3){
                                    $taxonomyCode3=$taxonomiesCode[2];
                                }
                            }

                            $pareffdt="";
                            $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));

                            if($address!=null){
                                foreach ($address as $addr3){
                                    $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr3->getId(),'credentialing_status'=>4));
                                    if($credentialingsF!=null){
                                        foreach ($credentialingsF as $cf){
                                            $dateFac=$cf->getCredentialingAcceptedDate();
                                            $dateFacEffe=$cf->getCredentialingEffectiveDate();

                                            $year="";
                                            $moth='';
                                            if($dateFacEffe!=""){
                                                $effD=explode('/',$dateFacEffe);
                                                $year=$effD[2];
                                                $moth=$effD[0];
                                            }
                                            $pareffdt=$year.$moth.'01';
                                        }
                                    }else{
                                        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId()));
                                        if($providers!=null){
                                            foreach ($providers as $provider){
                                                $credentialings_Tem=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status'=>4,'provider'=>$provider->getId()));
                                                $datesCExist=[];
                                                if($credentialings_Tem!=null){
                                                    foreach ($credentialings_Tem as $credeTem){
                                                        $dateT=$credeTem->getCredentialingEffectiveDate();
                                                        if($dateT!=""){
                                                            if(!in_array($dateT,$datesCExist)){
                                                                $datesCExist[]=$dateT;
                                                            }
                                                        }
                                                    }
                                                }
                                                //total of different dates from credentialing providers
                                                $totalDates=count($datesCExist);
                                                if($totalDates==1){
                                                    $dateC1= explode('/',$datesCExist[0]);
                                                    $yearT=$dateC1[2];
                                                    $mothT=$dateC1[0];
                                                    $pareffdt=$yearT.$mothT."01";
                                                }
                                                if($totalDates>1){
                                                    $dateC1= explode('/',$datesCExist[0]);
                                                    $yearT=$dateC1[2];
                                                    $mothT=$dateC1[0];
                                                    $pareffdt=$yearT.$mothT."01";
                                                }

                                                if(count($specialtiesArray)==0){
                                                    $pmls=$em->getRepository('App\Entity\Pml')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>"A"));
                                                    if($pmls!=null){
                                                        foreach ($pmls as $pml) {
                                                            $spe=$pml->getColE();
                                                            if($spe!=""){
                                                                if(!in_array($spe,$specialtiesArray)){
                                                                    $specialtiesArray[]=$spe;
                                                                }
                                                            }
                                                            $type=$pml->getColD();
                                                            if($type!="" and $type!=null){
                                                                if(!in_array($type,$providerTypesArray)){
                                                                    $providerTypesArray[]=$type;
                                                                }
                                                            }
                                                        }

                                                        $code="";
                                                        $match_pt=0;
                                                        foreach ($providerTypesArray as $spt){
                                                            foreach ($pt_order as $pt_o){
                                                                if($pt_o==$spt and $match_pt==0){
                                                                    $code=$spt;
                                                                    $match_pt=1;
                                                                }
                                                            }
                                                        }

                                                        if(strlen($code)==1){
                                                            $code="00".$code;
                                                        }
                                                        if(strlen($code)==2){
                                                            $code="0".$code;
                                                        }
                                                        $providerType=$code;
                                                        if(count($specialtiesArray)>=1){
                                                            $specialty1=$specialtiesArray[0]."01";
                                                        }
                                                        if(count($specialtiesArray)>=2){
                                                            $specialty2=$specialtiesArray[1]."01";
                                                        }
                                                        if(count($specialtiesArray)>=3){
                                                            $specialty3=$specialtiesArray[2]."01";
                                                        }
                                                        if(count($specialtiesArray)>=4){
                                                            $specialty4=$specialtiesArray[3]."01";
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            $street_check2=$addr->getStreet();
                            if(stristr($street_check2, 'BOX')==false and stristr($street_check2, 'Box')==false and stristr($street_check2, 'box')==false) {
                                $org_rate=$em->getRepository('App\Entity\OrganizationRates')->findOneBy(array('organization'=>$organization->getId(),'rate'=>2));

                                $eClaimProviderType="GR";
                                $providerTypeOrder=['01','04','16','68','66','05','77','67','08','14','25','26','30','29','31','07','32','81','97'];
                                $groupNpi=$organization->getGroupNpi();
                                $proTypeArray=array();
                                $pmlsArray=array();
                                if($groupNpi!=""){
                                    $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$groupNpi,'colW'=>"A"));
                                    if($pmls!=null){
                                        foreach ($pmls as $pml) {
                                            $proTypeCode = $pml->getColD();
                                            if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                                $proTypeArray[]=$proTypeArray;
                                                $pmlsArray[]=$pml;
                                            }
                                        }
                                    }
                                }

                                if(count($proTypeArray)==0){
                                    if($providers!=null) {

                                        foreach ($providers as $provider) {
                                            $languages=$provider->getLanguages();
                                            if($languages!=null){
                                                foreach ($languages as $lng){
                                                    if(!in_array($lng->getId(),$languagesId)){
                                                        $languagesId[]=$lng->getId();
                                                    }
                                                }
                                            }
                                            $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>"A"));
                                            if($pmls!=null){
                                                foreach ($pmls as $pml) {
                                                    $proTypeCode = $pml->getColD();
                                                    if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                                        $proTypeArray[]=$proTypeArray;
                                                        $pmlsArray[]=$pml;
                                                    }
                                                }
                                            }
                                        }

                                        $language1 = "";
                                        $language2 = "";
                                        $language3 = "";
                                        $language4 = "";
                                        $language5 = "";
                                        $language6 = "";
                                        $language7 = "";
                                        $language8 = "";
                                        $languagesArray = array();
                                        if ($languagesId != null) {
                                            foreach ($languagesId as $id) {
                                                $language = $em->getRepository('App\Entity\Languages')->find($id);
                                                if ($language != null) {
                                                    $languagesArray[] = $language;
                                                }
                                            }
                                        }

                                        if ($languagesArray != null) {
                                            if (count($languagesArray) > 0) {
                                                $language1 = $languagesArray[0]->getLangCd();
                                            }
                                            if (count($languagesArray) > 1) {
                                                $language2 = $languagesArray[1]->getLangCd();
                                            }
                                            if (count($languagesArray) > 2) {
                                                $language3 = $languagesArray[2]->getLangCd();
                                            }
                                            if (count($languagesArray) > 3) {
                                                $language4 = $languagesArray[3]->getLangCd();
                                            }
                                            if (count($languagesArray) > 4) {
                                                $language5 = $languagesArray[4]->getLangCd();
                                            }
                                            if (count($languagesArray) > 5) {
                                                $language6 = $languagesArray[5]->getLangCd();
                                            }
                                            if (count($languagesArray) > 6) {
                                                $language7 = $languagesArray[6]->getLangCd();
                                            }
                                            if (count($languagesArray) > 7) {
                                                $language8 = $languagesArray[7]->getLangCd();
                                            }
                                        }

                                        if($language1==""){
                                            $language1="ENG";
                                        }
                                    }
                                }

                                $pt_code="";
                                foreach ($pmlsArray as $pt){
                                    foreach ($providerTypeOrder as $order){
                                        if($pt->getColD()==$order){
                                            $pt_code=$pt->getColD();
                                            $specialty_code=$pt->getColE()."01";
                                            $medicaid_code=$pt->getColA();
                                            break 2;
                                        }
                                    }
                                }

                                $medicaid_length=strlen($medicaid_code);
                                if($medicaid_length<9){
                                    $rest=9-$medicaid_length;
                                    while ($rest>0){
                                        $medicaid_code="0".$medicaid_code;
                                        $rest--;
                                    }
                                }

                                if(strlen($pt_code)==1){
                                    $pt_code="00".$pt_code;
                                }
                                if(strlen($pt_code)==2){
                                    $pt_code="0".$pt_code;
                                }

                                if($providerType==""){
                                    $providerType=$pt_code;
                                }

                                if($specialty1==""){
                                    $specialty1=$specialty_code;
                                }

                                if($specialty1=="20001" or $specialty1=="20101" or $specialty1=="90101" or $specialty1=="30001" or $specialty1=="30101"){
                                    $eClaimProviderType="HS";
                                }

                                if($specialty2=="20001" or $specialty2=="20101" or $specialty2=="90101" or $specialty2=="30001" or $specialty2=="30101"){
                                    $eClaimProviderType="HS";
                                }

                                if($specialty3=="20001" or $specialty3=="20101" or $specialty3=="90101" or $specialty3=="30001" or $specialty3=="30101"){
                                    $eClaimProviderType="HS";
                                }

                                if($providerType!=""){
                                    $phone_number = $addr->getPhoneNumber();
                                    $phone_number = str_replace('-', "", $phone_number);

                                    $is_new=0;
                                    $is_changed=0;
                                    $is_terminated=1;

                                    //check if the providers is a new
                                    $roster_log=$em->getRepository('App\Entity\FCCRosterLog')->findOneBy(array('id_key'=>$key));

                                    if($roster_log==null){
                                        $is_new=1;
                                    }else{
                                        $cont_chgn=0;
                                        $new_last_name=strtolower($organization->getName());
                                        $old_last_name=strtolower($roster_log->getLastName());

                                        if($new_last_name!=$old_last_name){ $cont_chgn++;}
                                        if($eClaimProviderType!=$roster_log->getEclaimsProviderType()){ $cont_chgn++; }
                                        if($provider_type!=$roster_log->getAhcaProviderType()){ $cont_chgn++; }
                                        if($specialty1!=$roster_log->getSpec1()){ $cont_chgn++; }
                                        if($specialty2!=$roster_log->getSpec2()){ $cont_chgn++; }
                                        if($specialty3!=$roster_log->getSpec3()){ $cont_chgn++; }
                                        if($language1!=$roster_log->getLng1()){ $cont_chgn++; }
                                        if($language2!=$roster_log->getLng2()){ $cont_chgn++; }
                                        if($language3!=$roster_log->getLng3()){ $cont_chgn++; }
                                        if($language4!=$roster_log->getLng4()){ $cont_chgn++; }
                                        if($language5!=$roster_log->getLng5()){ $cont_chgn++; }
                                        if($language6!=$roster_log->getLng6()){ $cont_chgn++; }
                                        if($language7!=$roster_log->getLng7()){ $cont_chgn++; }
                                        if($language8!=$roster_log->getLng8()){ $cont_chgn++; }

                                        if($addr->getStreet()!=$roster_log->getPracStreet()){ $cont_chgn++; }
                                        if($addr->getSuiteNumber()!=$roster_log->getPracticeSuite()){ $cont_chgn++; }
                                        if($addr->getCity()!=$roster_log->getPracCity()){ $cont_chgn++; }
                                        if($addr->getUsState()!=$roster_log->getPracState()){ $cont_chgn++; }
                                        if($addr->getZipCode()!=$roster_log->getPracZipcode()){ $cont_chgn++; }
                                        if($phone_number!=$roster_log->getPracPhone()){ $cont_chgn++; }
                                        if($taxonomy1!=$roster_log->getTaxonomy1()){ $cont_chgn++; }
                                        if($taxonomy2!=$roster_log->getTaxonomy2()){ $cont_chgn++; }
                                        if($taxonomy3!=$roster_log->getTaxonomy3()){ $cont_chgn++; }

                                        if($cont_chgn>0){
                                            $is_changed=1;
                                        }
                                    }

                                        $spreadsheet->setActiveSheetIndex(3);
                                        $cell_A = 'A' . $cont_changes;$cell_B = 'B' . $cont_changes;$cell_C = 'C' . $cont_changes;$cell_D = 'D' . $cont_changes;
                                        $cell_E = 'E' . $cont_changes;$cell_F = 'F' . $cont_changes;$cell_G = 'G' . $cont_changes;$cell_H = 'H' . $cont_changes;
                                        $cell_I = 'I' . $cont_changes;$cell_J = 'J' . $cont_changes;$cell_K = 'K' . $cont_changes;$cell_L = 'L' . $cont_changes;
                                        $cell_M = 'M' . $cont_changes;$cell_N = 'N' . $cont_changes;$cell_O = 'O' . $cont_changes;$cell_P = 'P' . $cont_changes;
                                        $cell_Q = 'Q' . $cont_changes;$cell_R = 'R' . $cont_changes;$cell_S = 'S' . $cont_changes;$cell_T = 'T' . $cont_changes;$cell_U = 'U' . $cont_changes;$cell_V = 'V' . $cont_changes;
                                        $cell_W = 'W' . $cont_changes;$cell_X = 'X' . $cont_changes;$cell_Y = 'Y' . $cont_changes;$cell_Z = 'Z' . $cont_changes;

                                        $cell_AA = 'AA' . $cont_changes;$cell_AB = 'AB' . $cont_changes;$cell_AC = 'AC' . $cont_changes;$cell_AD = 'AD' . $cont_changes;
                                        $cell_AE = 'AE' . $cont_changes;$cell_AF = 'AF' . $cont_changes;$cell_AG = 'AG' . $cont_changes;$cell_AH = 'AH' . $cont_changes;$cell_AI = 'AI' . $cont_changes;$cell_AJ = 'AJ' . $cont_changes;$cell_AK = 'AK' . $cont_changes;
                                        $cell_AL = 'AL' . $cont_changes;$cell_AM = 'AM' . $cont_changes;$cell_AN = 'AN' . $cont_changes;$cell_AO = 'AO' . $cont_changes;$cell_AP = 'AP' . $cont_changes;
                                        $cell_AQ = 'AQ' . $cont_changes;$cell_AR = 'AR' . $cont_changes;$cell_AS = 'AS' . $cont_changes;$cell_AT = 'AT' . $cont_changes;$cell_AU = 'AU' . $cont_changes;
                                        $cell_AV = 'AV' . $cont_changes;$cell_AW = 'AW' . $cont_changes;$cell_AX = 'AX' . $cont_changes;$cell_AY = 'AY' . $cont_changes;$cell_AZ = 'AZ' . $cont_changes;

                                        $cell_BA = 'BA' . $cont_changes;$cell_BB = 'BB' . $cont_changes;$cell_BC = 'BC' . $cont_changes;$cell_BD = 'BD' . $cont_changes;$cell_BE = 'BE' . $cont_changes;
                                        $cell_BF = 'BF' . $cont_changes;$cell_BG = 'BG' . $cont_changes;$cell_BH = 'BH' . $cont_changes;$cell_BI = 'BI' . $cont_changes;$cell_BJ = 'BJ' . $cont_changes;
                                        $cell_BK = 'BK' . $cont_changes;$cell_BL = 'BL' . $cont_changes;$cell_BM = 'BM' . $cont_changes;$cell_BN = 'BN' . $cont_changes;$cell_BO = 'BO' . $cont_changes;
                                        $cell_BP = 'BP' . $cont_changes;$cell_BQ = 'BQ' . $cont_changes;$cell_BR = 'BR' . $cont_changes;$cell_BS = 'BS' . $cont_changes;$cell_BT = 'BT' . $cont_changes;
                                        $cell_BU = 'BU' . $cont_changes;$cell_BV = 'BV' . $cont_changes;$cell_BW = 'BW' . $cont_changes;$cell_BX = 'BX' . $cont_changes;$cell_BY = 'BY' . $cont_changes;$cell_BZ = 'BZ' . $cont_changes;

                                        $cell_CA = 'CA' . $cont_changes;$cell_CB = 'CB' . $cont_changes;
                                        $cell_CC = 'CC' . $cont_changes;$cell_CD = 'CD' . $cont_changes;$cell_CE = 'CE' . $cont_changes;$cell_CF = 'CF' . $cont_changes;
                                        $cell_CG = 'CG' . $cont_changes;$cell_CH = 'CH' . $cont_changes;$cell_CI = 'CI' . $cont_changes;$cell_CJ = 'CJ' . $cont_changes;
                                        $cell_CK = 'CK' . $cont_changes;$cell_CL = 'CL' . $cont_changes;$cell_CM = 'CM' . $cont_changes;$cell_CN = 'CN' . $cont_changes;
                                        $cell_CO = 'CO' . $cont_changes;$cell_CP = 'CP' . $cont_changes;$cell_CQ = 'CQ' . $cont_changes;$cell_CR = 'CR' . $cont_changes;
                                        $cell_CS = 'CS' . $cont_changes;$cell_CT = 'CT' . $cont_changes;$cell_CU = 'CU' . $cont_changes;$cell_CV = 'CV' . $cont_changes;
                                        $cell_CW = 'CW' . $cont_changes;$cell_CX = 'CX' . $cont_changes;$cell_CY = 'CY' . $cont_changes;$cell_CZ = 'CZ' . $cont_changes;

                                        $cell_DA = 'DA' . $cont_changes;$cell_DB = 'DB' . $cont_changes;$cell_DC = 'DC' . $cont_changes;$cell_DD = 'DD' . $cont_changes;$cell_DE = 'DE' . $cont_changes;
                                        $cell_DF = 'DF' . $cont_changes;$cell_DG = 'DG' . $cont_changes;$cell_DH = 'DH' . $cont_changes;$cell_DI = 'DI' . $cont_changes;$cell_DJ = 'DJ' . $cont_changes;$cell_DK = 'DK' . $cont_changes;$cell_DL = 'DL' . $cont_changes;$cell_DM = 'DM' . $cont_changes;
                                        $cell_DN = 'DN' . $cont_changes;$cell_DO = 'DO' . $cont_changes;$cell_DP = 'DP' . $cont_changes;
                                        $cell_DQ = 'DQ' . $cont_changes;$cell_DR = 'DR' . $cont_changes;$cell_DT = 'DT' . $cont_changes;
                                        $cell_DU='DU'.$cont_changes;;$cell_DV='DV'.$cont_changes;$cell_DY='DY'.$cont_changes;$cell_DZ='DZ'.$cont_changes;

                                        $cont_changes++;


                                    if($is_terminated==1){
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, $key);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_H, $providerType);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI, $site_index);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, strtoupper($organization->getName()));
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, 'N'); //Switch Flag
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_U, $organization->getGroupNpi());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_V, $medicaid_code);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_G, $eClaimProviderType);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $specialty1);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $specialty2);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, $specialty3);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_P, $specialty4);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AY, 'B');
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AJ, '20210101');

                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_Z, $language1);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AA, $language2);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AB, $language3);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AC, $language4);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AD, $language5);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AE, $language6);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AF, $language7);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AG, $language8);

                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AN, $addr->getStreet());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AO, $addr->getSuiteNumber());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AP, $addr->getCity());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AQ, $addr->getUsState());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AR, $addr->getZipCode());
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AT, $phone_number);

                                        $ada = "N";
                                        if ($addr->getWheelchair() == true) {
                                            $ada = "Y";
                                        }

                                        if($taxonomyCode1==""){
                                            $org_taxonomies=$em->getRepository('App\Entity\OrganizationTaxonomy')->findBy(array('organization'=>$organization->getId()));
                                            if($org_taxonomies!=null){
                                                if(count($org_taxonomies)>=1){
                                                    $taxonomyCode1=$org_taxonomies[0]->getTaxonomy()->getCode();
                                                }
                                                if(count($org_taxonomies)>=2){
                                                    $taxonomyCode2=$org_taxonomies[1]->getTaxonomy()->getCode();
                                                }
                                                if(count($org_taxonomies)>=3){
                                                    $taxonomyCode3=$org_taxonomies[2]->getTaxonomy()->getCode();
                                                }
                                            }
                                        }

                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AW, $ada);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_AX, 'Y');
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BB, $taxonomyCode1);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BC, $taxonomyCode2);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BD, $taxonomyCode3);
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_BJ, "N");

                                        $bussinesOurs = $addr->getBusinessHours();
                                        if($bussinesOurs!=""){
                                            $bussinesOurs = substr($bussinesOurs, 1);
                                            $bussinesOurs = substr($bussinesOurs, 0, -1);

                                            $bussinesOurs = str_replace('{', '', $bussinesOurs);
                                            $bussinesOurs = str_replace('}', '', $bussinesOurs);
                                            $bussinesOurs = str_replace('"', '', $bussinesOurs);

                                            $daysHours = explode(",", $bussinesOurs);

                                            //sunday
                                            $dayActive = explode(":", $daysHours[18])[1];
                                            $startHour = substr($daysHours[19], 9);
                                            $tillHour = substr($daysHours[20], 9);

                                            if ($dayActive == "true") {
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BW, strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BX, strtoupper($tillHour));

                                                $hasWeekend="Y";
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //monday
                                            $dayActive = explode(":", $daysHours[0])[1];
                                            $startHour = substr($daysHours[1], 9);
                                            $tillHour = substr($daysHours[2], 9);
                                            if ($dayActive == "true") {
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BK, strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BL, strtoupper($tillHour));
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //tuesday
                                            $dayActive = explode(":", $daysHours[3])[1];
                                            $startHour = substr($daysHours[4], 9);
                                            $tillHour = substr($daysHours[5], 9);
                                            if ($dayActive == "true") {
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BM, strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BN, strtoupper($tillHour));
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //Wednesday
                                            $dayActive = explode(":", $daysHours[6])[1];
                                            $startHour = substr($daysHours[7], 9);
                                            $tillHour = substr($daysHours[8], 9);
                                            if ($dayActive == "true") {
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BO, strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BP, strtoupper($tillHour));
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //Thurs
                                            $dayActive = explode(":", $daysHours[9])[1];
                                            $startHour = substr($daysHours[10], 9);
                                            $tillHour = substr($daysHours[11], 9);
                                            if ($dayActive == "true") {
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BQ, strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BR, strtoupper($tillHour));
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //Frid
                                            $dayActive = explode(":", $daysHours[12])[1];
                                            $startHour = substr($daysHours[13], 9);
                                            $tillHour = substr($daysHours[14], 9);
                                            if ($dayActive == "true") {
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BS, strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BT, strtoupper($tillHour));
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //sat
                                            $dayActive = explode(":", $daysHours[15])[1];
                                            $startHour = substr($daysHours[16], 9);
                                            $tillHour = substr($daysHours[17], 9);
                                            if ($dayActive == "true") {
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BU, strtoupper($startHour));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BV, strtoupper($tillHour));

                                                $hasWeekend="Y";
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }
                                        }

                                        //get billing Address
                                        $orgAddress = $em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $organization->getId()));

                                        if (count($orgAddress) == 1) {
                                            foreach ($orgAddress as $addrB) {
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DI, $addrB->getStreet());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DJ, $addrB->getCity());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DK, $addrB->getUsState());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DL, $addrB->getZipCode());
                                                $phone_number_B = $addrB->getPhoneNumber();
                                                $phone_number_B = str_replace('-', "", $phone_number_B);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DN,$phone_number_B);
                                            }
                                        }

                                        $street = "";
                                        $city = "";
                                        $usState = "";
                                        $zipCode = "";
                                        $phoneNumber = "";
                                        $hasBilling = false;

                                        if (count($orgAddress) > 1) {
                                            foreach ($orgAddress as $addrB) {
                                                if ($addrB->getBillingAddr() == true) {
                                                    $hasBilling = true;
                                                    $street = $addrB->getStreet();
                                                    $city = $addrB->getCity();
                                                    $usState = $addrB->getUsState();
                                                    $zipCode = $addrB->getZipCode();
                                                    $phone_number_B = $addrB->getPhoneNumber();
                                                    $phone_number_B = str_replace('-', "", $phone_number_B);
                                                }
                                            }

                                            if ($hasBilling == true) {
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DI, $street);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DJ, $city);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DK, $usState);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DL, $zipCode);
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DN, $phone_number_B);
                                            }
                                        }

                                        $tin = $organization->getTinNumber();

                                        if ($tin != "") {
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AL, 'T');
                                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AM, $tin);
                                        }else{
                                            $provider=$em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number'=>$organization->getGroupNpi()));
                                            if($provider!=null){
                                                $tin=$encoder->decryptthis($provider->getSocial());
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AL, 'S');
                                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AM, $tin);
                                            }
                                        }

                                        $key_cont++;
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="FCC_BSN_Roster'.date('mdY_his').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/fcc-providers-medicare", name="fcc_providers_medicare")
     */
    public function reportProviderMedicare(){
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');

        $spreadsheet = $reader->load($url.'fcc_providers_medicare.xlsx');
        $spreadsheet->setActiveSheetIndex(0);

        $sql="SELECT p.id, p.medicare, p.disabled, p.npi_number,p.first_name, p.last_name, organization.name as organization_name,
            p.medicare, p.medicaid,p.gender,
            GROUP_CONCAT(DISTINCT `degree`.`name` ORDER BY `degree`.`name` ASC SEPARATOR ', ') AS degrees
            FROM  provider p
            LEFT JOIN organization ON organization.id = p.org_id
            LEFT JOIN provider_degree ON provider_degree.provider_id = p.id
            LEFT JOIN degree ON degree.id = provider_degree.degree_id
            WHERE p.disabled=0 and p.medicare!='-' and p.medicare!='' and p.medicare!='0' and p.medicare!='N/A' and p.medicare!='None' and p.medicare!='Pending'
            and p.medicare!='in process' and p.medicare!='NA' and p.medicare!='in proceses' and p.medicare!='55525 - opted out' and p.medicare!='submitting application' 
            and p.medicare!='In-process'
            GROUP BY p.id";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $providers= $stmt->fetchAll();

        $providersNPI=[];
        $cont=2;
         foreach ($providers as $provider){
             $npi=$provider['npi_number'];
             if(!in_array($npi,$providersNPI)){
                 $providersNPI[]=$npi;

                 $providerObj=$em->getRepository('App:Provider')->find($provider['id']);
                 $address=$providerObj->getBillingAddress();

                 if($address!=null){
                     foreach ($address as $addr){

                         $cell_A = 'A' . $cont;$cell_B = 'B' . $cont;
                         $cell_C = 'C' . $cont;$cell_D = 'D' . $cont;
                         $cell_E = 'E' . $cont;$cell_F = 'F' . $cont;
                         $cell_G = 'G' . $cont;
                         $cell_H = 'H' . $cont;
                         $cell_I = 'I' . $cont;
                         $cell_J = 'J' . $cont;
                         $cell_K = 'K' . $cont;
                         $cell_L = 'L' . $cont;
                         $cell_M = 'M' . $cont;
                         $cell_N = 'N' . $cont;
                         $cell_O = 'O' . $cont;

                         $gender="";
                         if($provider['gender']=="female" or $provider['gender']=="Female" or $provider['gender']=="Fenale"){
                             $gender="F";
                         }
                         if($provider['gender']=="male" or $provider['gender']=="Male"){
                             $gender="M";
                         }

                         $fcc_roster_ready="No";
                         $fcc_record=$em->getRepository('App:PNVLog')->findOneBy(array('npi_number'=>$npi));
                         if($fcc_record!=null){
                             $fcc_roster_ready="Yes";
                         }

                         $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $provider['npi_number']);
                         $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $provider['first_name']);
                         $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $provider['last_name']);
                         $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $provider['organization_name']);
                         $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $provider['medicare']);
                         $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $provider['medicaid']);
                         $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $provider['degrees']);
                         $spreadsheet->getActiveSheet(0)->setCellValue($cell_O, $gender);
                         $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $addr->getStreet());
                         $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $addr->getSuiteNumber());
                         $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $addr->getCity());
                         $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $addr->getCounty());
                         $spreadsheet->getActiveSheet(0)->setCellValue($cell_M, $addr->getUsState());
                         $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $addr->getPhoneNumber());
                         $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $fcc_roster_ready);

                         $cont++;
                     }
                 }
             }
         }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="FCC_Providers_Medicare_'.date('Ymd').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/fcc-providers-medicare-2", name="fcc_providers_medicare_2")
     */
    public function reportProviderMedicare2(){
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');

        $spreadsheet = $reader->load($url.'fcc_providers_medicare.xlsx');
        $spreadsheet->setActiveSheetIndex(0);

        $organizations=$em->getRepository('App:Organization')->findBy(array('disabled'=>0));
        $providersNPI=[];
        $cont=2;
        foreach ($organizations as $organization){
            $org_ready_medicare=false;
            $organization_medicare="";
            $address=$em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization->getId()));

            if($address!=null){
                foreach ($address as $addr){
                    $group_medicare=$addr->getGroupMedicare();
                    if($group_medicare!="" and  $group_medicare!="-" and $group_medicare!="N/A" and $group_medicare!="Pending"){
                        $org_ready_medicare=true;
                        $organization_medicare=$group_medicare;
                        break;
                    }

                }
            }

            if($org_ready_medicare==false){
                $providers=$em->getRepository('App:Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));
                if($providers!=null){
                    foreach ($providers as $provider){
                        $medicare=$provider->getMedicare();

                        if($medicare!="" and $medicare!="-" and $medicare!="0" and $medicare!="N/A" and $medicare!="None" and $medicare!="Pending" and $medicare!="in process"
                            and $medicare!="NA" and $medicare!="in proceses" and $medicare!="55525 - opted out" and $medicare!="submitting application" and $medicare!="In-process"){
                            $org_ready_medicare=true;
                            $organization_medicare=$medicare;
                            break;
                        }
                    }
                }
            }

            if($org_ready_medicare==true){
                $providers_reault=$em->getRepository('App:Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));
                foreach ($providers_reault as $pro){

                    $npi=$pro->getNpiNumber();
                        $address=$pro->getBillingAddress();
                        if($address!=null){
                            foreach ($address as $addr){

                                $cell_A = 'A' . $cont;$cell_B = 'B' . $cont;
                                $cell_C = 'C' . $cont;$cell_D = 'D' . $cont;
                                $cell_E = 'E' . $cont;$cell_F = 'F' . $cont;
                                $cell_G = 'G' . $cont;$cell_P = 'P' . $cont;
                                $cell_H = 'H' . $cont;
                                $cell_I = 'I' . $cont;
                                $cell_J = 'J' . $cont;
                                $cell_K = 'K' . $cont;
                                $cell_L = 'L' . $cont;
                                $cell_M = 'M' . $cont;
                                $cell_N = 'N' . $cont;
                                $cell_O = 'O' . $cont;

                                $gender="";
                                if($pro->getGender()=="female" or $pro->getGender()=="Female" or $pro->getGender()=="Fenale"){
                                    $gender="F";
                                }
                                if($pro->getGender()=="male" or $pro->getGender()=="Male"){
                                    $gender="M";
                                }

                                $fcc_roster_ready="No";
                                $fcc_record=$em->getRepository('App:PNVLog')->findOneBy(array('npi_number'=>$npi));
                                if($fcc_record!=null){
                                    $fcc_roster_ready="Yes";
                                }

                                $medicare="";
                                $pro_medicare=$pro->getMedicare();
                                if($pro_medicare!="" and $pro_medicare!="-" and $pro_medicare!="0" and $pro_medicare!="N/A" and $pro_medicare!="None" and $pro_medicare!="Pending" and $pro_medicare!="in process"
                                    and $pro_medicare!="NA" and $pro_medicare!="in proceses" and $pro_medicare!="55525 - opted out" and $pro_medicare!="submitting application" and $pro_medicare!="In-process"){
                                    $medicare=$pro->getMedicare();
                                }

                                if( $medicare==""){
                                    $medicare=$organization_medicare;
                                }

                                $id=$pro->getId();
                                $sql="SELECT p.id, 
                                GROUP_CONCAT(DISTINCT `degree`.`name` ORDER BY `degree`.`name` ASC SEPARATOR ', ') AS degrees
                                FROM  provider p
                                LEFT JOIN provider_degree ON provider_degree.provider_id = p.id
                                LEFT JOIN degree ON degree.id = provider_degree.degree_id
                                WHERE p.id=".$id."
                                GROUP BY p.id";

                                $stmt = $conn->prepare($sql);
                                $stmt->execute();
                                $provider_degree= $stmt->fetchAll();

                                $str_degree="";
                                foreach ($provider_degree as $pro_degree){
                                    $str_degree.=$pro_degree['degrees'];
                                }

                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $npi);
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $pro->getFirstName());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $pro->getLastName());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $organization->getName());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $medicare);
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $pro->getMedicaid());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $str_degree);
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_O, $gender);
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $addr->getStreet());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $addr->getSuiteNumber());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $addr->getCity());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $addr->getCounty());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_M, $addr->getUsState());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $addr->getPhoneNumber());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $fcc_roster_ready);

                                $status="";
                                if($organization->getOrganizationStatus()->getId()==1){
                                    $status="Penging";
                                }
                                if($organization->getOrganizationStatus()->getId()==2){
                                    $status="Active";
                                }

                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_P, $status);

                                $cont++;
                            }
                        }
                }
            }
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="FCC_Providers_Medicare_'.date('Ymd').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

}
