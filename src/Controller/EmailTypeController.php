<?php

namespace App\Controller;

use App\Entity\EmailType;
use App\Form\EmailTType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Service\EmailAttachFileUploader;

/**
 * @Route("/admin/email-types")
 */
class EmailTypeController extends AbstractController
{
    /**
     * @Route("/index", name="email_type_index")
     */
    public function index()
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();


            $sql="SELECT et.id, et.name, et.subject,
            GROUP_CONCAT(DISTINCT `contact_pbest_choice`.`name` ORDER BY `contact_pbest_choice`.`name` ASC SEPARATOR ', ') AS `best_choices`
            FROM  email_type et 
            LEFT JOIN email_type_best_option_choice ON email_type_best_option_choice.email_type_id = et.id
            LEFT JOIN contact_pbest_choice ON contact_pbest_choice.id = email_type_best_option_choice.best_choice_id
            GROUP BY et.id
            ORDER BY et.name";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $emailTypes= $stmt->fetchAll();

            $delete_form_ajax = $this->createCustomForm('EMAILTYPE_ID', 'DELETE', 'admin_delete_email_type');

            return $this->render('email_type/index.html.twig', array('emailTypes' => $emailTypes, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_email_type")
     */
    public function add(Request $request, EmailAttachFileUploader $fileUploader){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $emailType=new EmailType();
            $form = $this->createForm(EmailTType::class, $emailType);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $action_type=$form['action_type']->getData();
                $files = $form['attachments']->getData();

                $em->persist($emailType);
                $em->flush();

                if ($files != "") {
                    foreach ($files as $file){
                        if($file!="" and $file!=null){
                            $file_provider_agreement = $fileUploader->upload($file, $emailType->getId(), $file->getClientOriginalName());


                        }
                    }
                }

                if($action_type==1){
                    return  $this->redirectToRoute('email_type_index');
                }else{
                    return  $this->redirectToRoute('admin_new_email_type');
                }

                $this->addFlash(
                    "success",
                    "Email Template has been created successfully!"
                );
            }

            return $this->render('email_type/form.html.twig', ['form' => $form->createView(), 'action' => 'New']);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_email_type", defaults={"id": null})
     */
    public function edit(Request $request, $id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $mailType=$em->getRepository('App:EmailType')->find($id);

            $form = $this->createForm(EmailTType::class, $mailType);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $em->persist($mailType);
                $em->flush();

                $action_type=$form['action_type']->getData();

                if($action_type==1){
                    return  $this->redirectToRoute('email_type_index');
                }else{
                    return  $this->redirectToRoute('admin_new_email_type');
                }

                $this->addFlash(
                    "success",
                    "Email Template has been created successfully!"
                );
            }

            return $this->render('email_type/form.html.twig', ['form' => $form->createView(), 'action' => 'Edit']);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_email_type", defaults={"id": null})
     */
    public function view(Request $request, $id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $emailType=$em->getRepository('App:EmailType')->find($id);

            return $this->render('email_type/view.html.twig',['document'=>$emailType]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/delete/{id}", name="admin_delete_email_type",methods={"POST","DELETE"})
     */
    public function delete(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $object = $em->getRepository('App:EmailType')->find($id);
            $removed = 0;
            $message = "";

            if ($object) {
                try {
                    $em->remove($object);
                    $em->flush();
                    $removed = 1;
                    $message = "The Email template has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Email template can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_email_type",methods={"POST","DELETE"})
     */
    public function deleteMultiple(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $object = $em->getRepository('App:EmailType')->find($id);

                if ($object) {
                    try {
                        $em->remove($object);
                        $em->flush();
                        $removed = 1;
                        $message = "The Email Template has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Email Template can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }
}
