<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\Provider;

/**
 * @Route("/admin/report")
 */
class AdminReportsController extends AbstractController{

    /**
     * @Route("/providers", name="admin_provider_report")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            return new Response('OK');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }
}
