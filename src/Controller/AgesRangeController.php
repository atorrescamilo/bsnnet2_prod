<?php

namespace App\Controller;

use App\Entity\AgesRange;
use App\Form\AgesRangeType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/ages-range")
 */
class AgesRangeController extends AbstractController
{
    /**
     * @Route("/list", name="ages_range_index")
     */
    public function index()
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em=$this->getDoctrine()->getManager();

            $ages_ranges=$em->getRepository('App:AgesRange')->findAll();
            $delete_form_ajax = $this->createCustomForm('AGE_RANGE_ID', 'DELETE', 'admin_delete_ages_range');

        return $this->render('ages_range/index.html.twig', ['ages_ranges' => $ages_ranges,'delete_form_ajax' => $delete_form_ajax->createView()]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="ages_range_new")
    */
    public function new(Request $request): Response{
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em=$this->getDoctrine()->getManager();

            $ageRange=new AgesRange();
            $form=$this->createForm(AgesRangeType::class, $ageRange);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $em->persist($ageRange);
                $em->flush();

                return  $this->redirectToRoute('ages_range_index');
            }

            return $this->render('ages_range/form.html.twig', ['form'=>$form->createView(),'action'=>'New']);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="ages_range_edit",defaults={"id": null})
     */
    public function edit(Request $request,$id): Response{
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em=$this->getDoctrine()->getManager();

            $ageRange=$em->getRepository('App:AgesRange')->find($id);
            $form=$this->createForm(AgesRangeType::class, $ageRange);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $em->persist($ageRange);
                $em->flush();

                return  $this->redirectToRoute('ages_range_index');
            }

            return $this->render('ages_range/form.html.twig', ['form'=>$form->createView(),'action'=>'Edit']);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/delete/{id}", name="admin_delete_ages_range",methods={"POST","GET","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $ages_range = $em->getRepository('App:AgesRange')->find($id);
            $removed = 0;
            $message = "";

            if ($ages_range) {
                try {
                    $em->remove($ages_range);
                    $em->flush();
                    $removed = 1;
                    $message = "The Age Range has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Age Range can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_age_range",methods={"POST","DELETE"})
     */
    public function deleteMultiple(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $age_range =  $em->getRepository('App:AgesRange')->find($id);

                if ($age_range) {
                    try {
                        $em->remove($age_range);
                        $em->flush();
                        $removed = 1;
                        $message = "The Ages Range has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Ages Range can't be removed";
                    }
                }
            }

            $this->SaveLog(1,'Provider',implode($ids),'Delete multiples Age ranges');

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }
}
