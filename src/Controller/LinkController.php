<?php

namespace App\Controller;

use App\Entity\Link;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/links")
*/
class LinkController extends AbstractController
{
    /**
     * @Route("/index", name="link_index")
    */
    public function index()
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $links = $em->getRepository('App\Entity\Link')->findAll();
            $delete_form_ajax = $this->createCustomForm('LINK_ID', 'DELETE', 'admin_delete_link');

            return $this->render('link/index.html.twig', array('links' => $links, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/renderLinks", name="render_link_index")
     */
    public function renderLinks()
    {
        $em = $this->getDoctrine()->getManager();
        $links = $em->getRepository('App\Entity\Link')->findBy(array(),array('link_order'=>'ASC'));

        $total=count($links);

        return $this->render('link/render_links.html.twig', array('links' => $links,'total'=>$total));
    }


    /**
     * @Route("/new", name="admin_new_link")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            $em=$this->getDoctrine()->getManager();

            $links=$em->getRepository('App:Link')->findAll();

            $max_order=1;

            if($links){
                foreach ($links as $link){
                    $order=intval($link->getLinkOrder());
                    if($order>$max_order){
                        $max_order=$order;
                    }
                }
            }

            $current_order=$max_order+1;
            return $this->render('link/add.html.twig',['current_order'=>$current_order]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_link", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Link')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('link_index');
            }

            return $this->render('link/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }
    
    /**
     * @Route("/create", name="admin_create_link")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $url = $request->get('url');
            $new = $request->get('new');
            $order=$request->get('order');

            if($order==""){
                $order=0;
            }

            $link = new Link();
            $link->setName($name);
            $link->setLink($url);
            $link->setLinkOrder($order);

            $em->persist($link);
            $em->flush();

            $this->addFlash(
                'success',
                'Link has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_link');
            }

            return $this->redirectToRoute('link_index');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_link")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $url = $request->get('url');
            $new = $request->get('new');
            $id = $request->get('id');
            $order=$request->get('order');

            $link = $em->getRepository('App\Entity\Link')->find($id);

            if ($link == null) {
                $this->addFlash(
                    "danger",
                    "The Link can't been updated!"
                );

                return $this->redirectToRoute('link_index');
            }

            if ($link != null) {
                $link->setName($name);
                $link->setLink($url);
                $link->setLinkOrder($order);

                $em->persist($link);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "Link has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_link');
            }

            return $this->redirectToRoute('link_index');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_link",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $link = $em->getRepository('App\Entity\Link')->find($id);
            $removed = 0;
            $message = "";

            if ($link) {
                try {
                    $em->remove($link);
                    $em->flush();
                    $removed = 1;
                    $message = "The Link has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Link can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_link",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $link  = $em->getRepository('App\Entity\Link')->find($id);

                if ($link) {
                    try {
                        $em->remove($link);
                        $em->flush();
                        $removed = 1;
                        $message = "The Link has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Link can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }
}
