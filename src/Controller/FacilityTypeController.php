<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\FacilityType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/facilitytype")
 */
class FacilityTypeController extends AbstractController{

    /**
     * @Route("/index", name="admin_facilitytype")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $facilitytypes = $em->getRepository('App\Entity\FacilityType')->findAll();

            $delete_form_ajax = $this->createCustomForm('FACILITYTYPE_ID', 'DELETE', 'admin_delete_facilitytype');


            return $this->render('facilitytype/index.html.twig', array('facilitytypes' => $facilitytypes, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_facilitytype")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('facilitytype/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_facilitytype", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\FacilityType')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_facilitytype');
            }

            return $this->render('facilitytype/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_facilitytype", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\FacilityType')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_facilitytype');
            }

            return $this->render('facilitytype/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_facilitytype")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');

            $facilitytype = new FacilityType();
            $facilitytype->setName($name);
            $facilitytype->setDescription($description);

            $em->persist($facilitytype);
            $em->flush();

            $this->addFlash(
                'success',
                'Facility Type has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_facilitytype');
            }

            return $this->redirectToRoute('admin_facilitytype');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_facilitytype")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $facilitytype = $em->getRepository('App\Entity\FacilityType')->find($id);

            if ($facilitytype == null) {
                $this->addFlash(
                    "danger",
                    "The Facility Type can't been updated!"
                );

                return $this->redirectToRoute('admin_facilitytype');
            }

            if ($facilitytype != null) {
                $facilitytype->setName($name);
                $facilitytype->setDescription($description);

                $em->persist($facilitytype);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "The Facility Type has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_facilitytype');
            }

            return $this->redirectToRoute('admin_facilitytype');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_facilitytype",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $facilitytype = $facilitytype = $em->getRepository('App\Entity\FacilityType')->find($id);
            $removed = 0;
            $message = "";

            if ($facilitytype) {
                try {
                    $em->remove($facilitytype);
                    $em->flush();
                    $removed = 1;
                    $message = "The Facility Type has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Facility Type can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_facilitytype",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $facilitytype = $facilitytype = $em->getRepository('App\Entity\FacilityType')->find($id);

                if ($facilitytype) {
                    try {
                        $em->remove($facilitytype);
                        $em->flush();
                        $removed = 1;
                        $message = "The Facility Type has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Facility Type can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
