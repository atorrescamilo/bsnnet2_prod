<?php

namespace App\Controller;

use App\Entity\SpecialtyAreaProvider;
use App\Form\SpecialtyAreaProviderType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/provider-specialty-areas")
 */
class SpecialtyAreaProviderController extends AbstractController
{
    /**
     * @Route("/index", name="specialty_area_provider_index")
     */
    public function index()
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
        
            $em = $this->getDoctrine()->getManager();
            $specialty_areas = $em->getRepository('App:SpecialtyAreaProvider')->findAll();

            $delete_form_ajax = $this->createCustomForm('PSA_ID', 'DELETE', 'admin_delete_specialty_area_provider');

            return $this->render('specialty_area_provider/index.html.twig', array('specialty_areas' => $specialty_areas, 'delete_form_ajax' => $delete_form_ajax->createView()));

        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
    * @Route("/new", name="admin_new_specialty_area_provider")
    */
    public function add(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $specialty_area=new SpecialtyAreaProvider();    

            $form = $this->createForm(SpecialtyAreaProviderType::class, $specialty_area);    

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $em->persist($specialty_area);
                $em->flush();

                $action_type=$form['action_type']->getData();

                if($action_type==1){

                    $this->addFlash(
                        "success",
                        "Accreditation has been created successfully!"
                    );

                    return  $this->redirectToRoute('specialty_area_provider_index');

                }else{
                    return  $this->redirectToRoute('admin_new_specialty_area_provider');
                }
            }

            return $this->render('specialty_area_provider/form.html.twig', ['form' => $form->createView(), 'action' => 'New']);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
    * @Route("/edit/{id}", name="admin_edit_specialty_area_provider", defaults={"id": null})
    */
    public function edit(Request $request, $id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $specialty_area=$em->getRepository('App:SpecialtyAreaProvider')->find($id); 

            $form = $this->createForm(SpecialtyAreaProviderType::class, $specialty_area);    

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $em->persist($specialty_area);
                $em->flush();

                $action_type=$form['action_type']->getData();

                if($action_type==1){

                    $this->addFlash(
                        "success",
                        "Accreditation has been created successfully!"
                    );

                    return  $this->redirectToRoute('specialty_area_provider_index');

                }else{
                    return  $this->redirectToRoute('admin_new_specialty_area_provider');
                }
            }

            return $this->render('specialty_area_provider/form.html.twig', ['form' => $form->createView(), 'action' => 'Edit']);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


        /**
    * @Route("/view/{id}", name="admin_view_specialty_area_provider", defaults={"id": null})
    */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $specialty_area=$em->getRepository('App:SpecialtyAreaProvider')->find($id); 

            return $this->render('specialty_area_provider/view.html.twig', ['document' => $specialty_area]);

        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_specialty_area_provider",methods={"POST","DELETE"})
     */
    public function delete(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $specialty_area = $em->getRepository('App:SpecialtyAreaProvider')->find($id);
            $removed = 0;
            $message = "";

            if ($specialty_area) {
                try {
                    $em->remove($specialty_area);
                    $em->flush();
                    $removed = 1;
                    $message = "The Specialty Area has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Specialty Area can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

        /**
     * @Route("/delete_multiple", name="admin_delete_multiple_specialty_area_provider",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $specialty_area= $em->getRepository('App:SpecialtyAreaProvider')->find($id);

                if ($specialty_area) {
                    try {
                        $em->remove($specialty_area);
                        $em->flush();
                        $removed = 1;
                        $message = "The Provider Specialty Area has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Provider Specialty Area can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }
}
