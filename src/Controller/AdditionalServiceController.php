<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Cvo;

/**
 * @Route("/admin/additional-service")
 */
class AdditionalServiceController extends AbstractController{

    /**
     * @Route("/index", name="admin_additional_service")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $additionalservices = $em->getRepository('App\Entity\AdditionalService')->findAll();

            $delete_form_ajax = $this->createCustomForm('ADDITIONALSERVICE_ID', 'DELETE', 'admin_delete_additional_service');


            return $this->render('additional_service/index.html.twig', array('additionalservices' => $additionalservices, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_additional_service")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('additional_service/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_additional_service", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\AdditionalService')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_additional_service');
            }

            return $this->render('additional_service/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_additional_service", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\AdditionalService')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_additional_service');
            }

            return $this->render('additional_service/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_additional_service")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');

            $additionalservice = new Cvo();
            $additionalservice->setName($name);
            $additionalservice->setDescription($description);

            $em->persist($additionalservice);
            $em->flush();

            $this->addFlash(
                'success',
                'The Additional Service has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_additional_service');
            }

            return $this->redirectToRoute('admin_additional_service');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_additional_service")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $additionalservice = $em->getRepository('App\Entity\AdditionalService')->find($id);

            if ($additionalservice == null) {
                $this->addFlash(
                    "danger",
                    "The Additional Service can't been updated!"
                );

                return $this->redirectToRoute('admin_additional_service');
            }

            if ($additionalservice != null) {
                $additionalservice->setName($name);
                $additionalservice->setDescription($description);

                $em->persist($additionalservice);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "The Additional Service has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_additional_service');
            }

            return $this->redirectToRoute('admin_additional_service');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_additional_service",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $additionalservice = $additionalservice = $em->getRepository('App\Entity\AdditionalService')->find($id);
            $removed = 0;
            $message = "";

            if ($additionalservice) {
                try {
                    $em->remove($additionalservice);
                    $em->flush();
                    $removed = 1;
                    $message = "The Additional Service has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Additional Service can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_additional_service",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $additionalservice = $additionalservice = $em->getRepository('App\Entity\Cvo')->find($id);

                if ($additionalservice) {
                    try {
                        $em->remove($additionalservice);
                        $em->flush();
                        $removed = 1;
                        $message = "The Cvo has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Cvo can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
