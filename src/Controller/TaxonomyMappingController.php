<?php

namespace App\Controller;

use App\Entity\TaxonomyPM;
use App\Form\AccreditationType;
use App\Form\TaxonomyPMType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\Reader;

/**
 * @Route("/admin/taxonomy-mapping")
 */
class TaxonomyMappingController extends AbstractController
{

    /**
     * @Route("/index", name="admin_taxonomy_mapping_index")
     */
    public function index(): Response
    {
        $em=$this->getDoctrine()->getManager();

        $taxonomies=$em->getRepository('App:TaxonomyPM')->findAll();
        $delete_form_ajax = $this->createCustomForm('TAXONOMYPM_ID', 'DELETE', 'admin_delete_taxonomy_mapping');

        return $this->render('taxonomy_mapping/index.html.twig', [
            'taxonomies' => $taxonomies,'delete_form_ajax' => $delete_form_ajax->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_taxonomy_mapping",methods={"POST","DELETE"})
     */
    public function delete(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $accreditation = $em->getRepository('App:TaxonomyPM')->find($id);
            $removed = 0;
            $message = "";

            if ($accreditation) {
                try {
                    $em->remove($accreditation);
                    $em->flush();
                    $removed = 1;
                    $message = "The Accreditation has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The accreditation can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_taxonomy_mapping")
     */
    public function add(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $taxonomy=new TaxonomyPM();
            $form = $this->createForm(TaxonomyPMType::class, $taxonomy);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $em->persist($taxonomy);
                $em->flush();

                $action_type=$form['action_type']->getData();

                if($action_type==1){
                    return  $this->redirectToRoute('admin_taxonomy_mapping_index');
                }else{
                    return  $this->redirectToRoute('admin_new_taxonomy_mapping');
                }

                $this->addFlash(
                    "success",
                    "Taxonomy Provider Map has been created successfully!"
                );
            }


            return $this->render('taxonomy_mapping/form.html.twig', ['form' => $form->createView(), 'action' => 'New']);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_taxonomy_mapping",defaults={"id": null})
     */
    public function edit(Request $request, $id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $taxonomypm=$em->getRepository('App:TaxonomyPM')->find($id);
            $form = $this->createForm(TaxonomyPMType::class, $taxonomypm);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $em->persist($taxonomypm);
                $em->flush();

                $action_type=$form['action_type']->getData();

                if($action_type==1){
                    return  $this->redirectToRoute('admin_taxonomy_mapping_index');
                }else{
                    return  $this->redirectToRoute('admin_new_taxonomy_mapping');
                }

                $this->addFlash(
                    "success",
                    "Taxonomy provider mapping has been updated successfully!"
                );
            }


            return $this->render('taxonomy_mapping/form.html.twig', ['form' => $form->createView(), 'action' => 'Edit']);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/load-report", name="admin_taxonomy_mapping_load_report")
     */
    public function loadReport(): Response
    {
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "docs/FHKC_Taxonomy_provider_mapping.xlsx";
        $reader = new Reader\Xlsx();

        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        $cont=1;
        foreach ($sheetData as $sheet) {
            if ($cont >= 2) {
                if ($sheet['A'] != null or $sheet['A'] != "") {

                    $specialty1=$sheet['A'];
                    $specialty2=$sheet['B'];
                    $specialty3=$sheet['C'];
                    $specialty4=$sheet['D'];
                    $code=$sheet['E'];
                    $grouping=$sheet['F'];
                    $classification=$sheet['G'];
                    $specialization=$sheet['H'];
                    $definition=$sheet['I'];
                    $notes=$sheet['J'];

                    $taxonomy=new TaxonomyPM();
                    $taxonomy->setSpecialty1($specialty1);
                    $taxonomy->setSpecialty2($specialty2);
                    $taxonomy->setSpecialty3($specialty3);
                    $taxonomy->setSpecialty4($specialty4);
                    $taxonomy->setCode($code);
                    $taxonomy->setGrouping($grouping);
                    $taxonomy->setClassification($classification);
                    $taxonomy->setSpecialization($specialization);
                    $taxonomy->setDefinition($definition);
                    $taxonomy->setNotes($notes);

                    $em->persist($taxonomy);
                }
            }
            $cont++;
        }

        $em->flush();

        return new Response('Loaded all records');
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_taxonomy-mapping",methods={"POST","DELETE"})
     *
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $organization = $em->getRepository('App\Entity\TaxonomyPM')->find($id);

                if ($organization) {
                    try {
                        $em->remove($organization);
                        $em->flush();
                        $removed = 1;
                        $message = "The taxonomies mapping has been successfully removed";

                        //$this->SaveLog(4,'TaxonomyPM',$ids,'Deleting many taxonomies mapping','');
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The taxonomies mapping can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_taxonomy_mapping",defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\TaxonomyPM')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_accreditation');
            }

            return $this->render('taxonomy_mapping/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

}
