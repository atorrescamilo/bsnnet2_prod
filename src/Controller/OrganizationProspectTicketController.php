<?php

namespace App\Controller;

use App\Entity\ProspectTicket;
use App\Form\ProspectTicketType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\OrganizationProspectTicket;
use App\Entity\OrganizationProspect;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/organization-prospect-ticket")
 */
class OrganizationProspectTicketController extends AbstractController{

    /**
     * @Route("/new/{id}", name="admin_new_organizationprospectticket")
     */
    public function add($id, Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $organizationProspect = $em->getRepository('App\Entity\Prospect')->find($id);
            $user=$this->getUser();

            $tickect=new ProspectTicket();
            $form = $this->createForm(ProspectTicketType::class, $tickect);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $tickect->setProspect($organizationProspect);
                $tickect->setUser($user);
                $em->persist($tickect);
                $em->flush();

                $action_type=$form['action_type']->getData();

                if($action_type==1){
                    return  $this->redirectToRoute('admin_view_organizationprospect',['id'=>$organizationProspect->getId(),'tab'=>2]);
                }else{
                    return  $this->redirectToRoute('admin_new_organizationprospectticket',['id'=>$organizationProspect->getId()]);
                }

                $this->addFlash(
                    "success",
                    "The Tickect has been created successfully!"
                );
            }

            $date=date("m/d/Y");

            return $this->render('organizationprospectticket/form.html.twig', ['form' => $form->createView(),'date'=>$date, 'action' => 'New','prospect'=>$organizationProspect->getId()]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_organizationprospectticket", defaults={"id": null, "prospect": null} )
     */
    public function edit($id, Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $tickect=$em->getRepository('App:ProspectTicket')->find($id);
            $organizationProspect = $tickect->getProspect();

            $form = $this->createForm(ProspectTicketType::class, $tickect);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $em->persist($tickect);
                $em->flush();

                $action_type=$form['action_type']->getData();

                if($action_type==1){
                    return  $this->redirectToRoute('admin_view_organizationprospect',['id'=>$organizationProspect->getId(),'tab'=>2]);
                }else{
                    return  $this->redirectToRoute('admin_edit_organizationprospectticket',['id'=>$id]);
                }

                $this->addFlash(
                    "success",
                    "The Tickect has been Updated successfully!"
                );
            }

            return $this->render('organizationprospectticket/form.html.twig', ['form' => $form->createView(), 'action' => 'Edit','prospect'=>$organizationProspect->getId()]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_organizationprospectticket", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ProspectTicket')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_organizationprospectticket');
            }

            $prospect=$document->getProspect();

            return $this->render('organizationprospectticket/view.html.twig', array('document' => $document, 'prospect' => $prospect));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_organizationprospectticket",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $prospect_ticket = $em->getRepository('App\Entity\ProspectTicket')->find($id);
            $removed = 0;
            $message = "";

            if ($prospect_ticket) {
                try {
                    $em->remove($prospect_ticket);
                    $em->flush();
                    $removed = 1;
                    $message = "The Ticket has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Ticket can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_organizationprospectticket",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $prospect_ticket =  $em->getRepository('App:ProspectTicket')->find($id);

                if ($prospect_ticket) {
                    try {
                        $em->remove($prospect_ticket);
                        $em->flush();
                        $removed = 1;
                        $message = "The Ticket has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Ticket can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
