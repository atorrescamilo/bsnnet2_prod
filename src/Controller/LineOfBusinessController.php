<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\LineOfBusiness;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/lineofbusiness")
 */
class LineOfBusinessController extends AbstractController{

    /**
     * @Route("/index", name="admin_lineofbusiness")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $lineofbusinesss = $em->getRepository('App\Entity\LineOfBusiness')->findAll();

            $delete_form_ajax = $this->createCustomForm('LINEOFBUSINESS_ID', 'DELETE', 'admin_delete_lineofbusiness');

            return $this->render('lineofbusiness/index.html.twig', array('lineofbusinesss' => $lineofbusinesss, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_lineofbusiness")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('lineofbusiness/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_lineofbusiness", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\LineOfBusiness')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_lineofbusiness');
            }

            return $this->render('lineofbusiness/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_lineofbusiness", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\LineOfBusiness')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_lineofbusiness');
            }

            return $this->render('lineofbusiness/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_lineofbusiness")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');

            $lineofbusiness = new LineOfBusiness();
            $lineofbusiness->setName($name);
            $lineofbusiness->setDescription($description);

            $em->persist($lineofbusiness);
            $em->flush();

            $this->addFlash(
                'success',
                'Line Of Business has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_lineofbusiness');
            }

            return $this->redirectToRoute('admin_lineofbusiness');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_lineofbusiness")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $lineofbusiness = $em->getRepository('App\Entity\LineOfBusiness')->find($id);

            if ($lineofbusiness == null) {
                $this->addFlash(
                    "danger",
                    "The Line Of Business can't been updated!"
                );

                return $this->redirectToRoute('admin_lineofbusiness');
            }

            if ($lineofbusiness != null) {
                $lineofbusiness->setName($name);
                $lineofbusiness->setDescription($description);

                $em->persist($lineofbusiness);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "Line Of Business has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_lineofbusiness');
            }

            return $this->redirectToRoute('admin_lineofbusiness');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_lineofbusiness",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $lineofbusiness = $lineofbusiness = $em->getRepository('App\Entity\LineOfBusiness')->find($id);
            $removed = 0;
            $message = "";

            if ($lineofbusiness) {
                try {
                    $em->remove($lineofbusiness);
                    $em->flush();
                    $removed = 1;
                    $message = "The Line Of Business has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Line Of Business can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_lineofbusiness",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $lineofbusiness = $lineofbusiness = $em->getRepository('App\Entity\LineOfBusiness')->find($id);

                if ($lineofbusiness) {
                    try {
                        $em->remove($lineofbusiness);
                        $em->flush();
                        $removed = 1;
                        $message = "The Line Of Business has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Line Of Business can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
