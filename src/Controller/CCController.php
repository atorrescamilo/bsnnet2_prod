<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/contrating-credentialing")
 */
class CCController extends AbstractController
{
    /**
     * @Route("/dashboard", name="admin_contrating_credentialing_dashboard")
     */
    public function index(){
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $payers = $em->getRepository('App\Entity\Payer')->findAll();

        $current_year=date('Y');
        $mmm_indicators=$em->getRepository('App\Entity\MMMIndicator')->findBy(array('year'=>$current_year,'payer'=>3),array('id'=>'ASC'));

        $data=[];
        foreach ($mmm_indicators as $indicator){
            $c_data=explode(',',$indicator->getData());
            $data[$indicator->getTopic()][$indicator->getIndicator()]=$c_data;
        }

        return $this->render('contrating_credentialing/index.html.twig', [
            'mmm_indicators'=>$mmm_indicators,'data'=>$data,'payers'=>$payers
        ]);
    }
}
