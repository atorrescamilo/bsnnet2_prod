<?php

namespace App\Controller;

use App\Entity\FloridaHospital;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader;

/**
 * @Route("/admin/florida-hospitals")
 */
class FloridaHospitalController extends AbstractController
{
    /**
     * @Route("/index", name="florida_hospital_index")
     */
    public function index(){
        $em=$this->getDoctrine()->getManager();

        $hospitals=$em->getRepository('App\Entity\FloridaHospital')->findAll();
        $delete_form_ajax = $this->createCustomForm('HOSPITAL_ID', 'DELETE', 'admin_delete_florida_hospital');

        return $this->render('florida_hospital/index.html.twig', [
            'hospitals' => $hospitals,'delete_form_ajax' => $delete_form_ajax->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_florida_hospital",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $cvo = $cvo = $em->getRepository('App\Entity\HospitalAHCA')->find($id);
            $removed = 0;
            $message = "";

            if ($cvo) {
                try {
                    $em->remove($cvo);
                    $em->flush();
                    $removed = 1;
                    $message = "The Hospital has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Hospital can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/load-info", name="load_florida_hospital",methods={"GET"})
     */
    public function loadInfo(Request $request) {
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "template_xls/FloridaHealthFinderFacilitiesExport.xls";

        $reader = new Reader\Xls();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=0;

        foreach ($sheetData as $sheet) {
            if ($cont > 0) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    $hospital=new FloridaHospital();
                    $hospital->setAhcaNumber($sheet['A']);
                    $hospital->setLicenseNumber($sheet['B']);
                    $hospital->setLicenseEffectiveDate($sheet['C']);
                    $hospital->setLicenseExpirationDate($sheet['D']);
                    $hospital->setName($sheet['F']);
                    $hospital->setStreet($sheet['G']);
                    $hospital->setSuiteNumber($sheet['H']);
                    $hospital->setCity($sheet['I']);
                    $hospital->setState($sheet['J']);
                    $hospital->setZipCode($sheet['K']);
                    $hospital->setCounty($sheet['L']);
                    $hospital->setPhone($sheet['M']);
                    $hospital->setMailingStreet($sheet['N']);
                    $hospital->setMailingSuiteNumber($sheet['O']);
                    $hospital->setMailingCity($sheet['P']);
                    $hospital->setMailingState($sheet['Q']);
                    $hospital->setMailingZipCode($sheet['R']);
                    $hospital->setMailingCounty($sheet['S']);
                    $hospital->setLicenseStatus($sheet['T']);
                    $hospital->setOwner($sheet['U']);
                    $hospital->setOwnerSince($sheet['V']);
                    $hospital->setBeds($sheet['X']);
                    $hospital->setProfitStatus($sheet['Y']);
                    $hospital->setWeb($sheet['Z']);

                    $em->persist($hospital);
                    $em->flush();
                }
            }
            $cont++;
        }

        return new Response('OK');
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }
}
