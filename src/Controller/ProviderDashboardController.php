<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/provider")
 */
class ProviderDashboardController extends AbstractController
{
    /**
     * @Route("/provider_dashboard", name="dashboard_provider")
     *
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            return $this->render('home/provider_dashboard.html.twig');
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }


    /**
     * @Route("/loaddata", name="search_data")
     *
     */
    public function search(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $roles = $em->getRepository('App\Entity\Role')->findAll();
            //  $users=$em->getRepository('App\Entity\User')->findAll();

            return new Response(
                json_encode(array('roles' => $roles)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }



}
