<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/report")
 */
class LogController extends AbstractController
{
    /**
     * @Route("/index", name="admin_log")
     */
    public function index(){

        $em=$this->getDoctrine()->getManager();
        $logs=$em->getRepository('App\Entity\ChangeLog')->findBy(array(),array('date'=>'DESC'));

        return $this->render('log/index.html.twig', [
            'logs' => $logs,
        ]);
    }
}
