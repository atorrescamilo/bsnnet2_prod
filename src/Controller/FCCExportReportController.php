<?php

namespace App\Controller;

use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\Reader;

/**
 * @Route("/admin/report")
 */
class FCCExportReportController extends AbstractController
{
    /**
     * @Route("/pnv-export", name="fcc_pnv_report_export")
     */
    public function fcc_pnv_export()
    {
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');
        $date=date('m/d/Y');

        $spreadsheet = $reader->load($url.'PNV_Template.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $em=$this->getDoctrine()->getManager();
        $cont=2;

        //get all records from FCCData for providers for PG
        $providers_data=$em->getRepository('App\Entity\FCCData')->findAll();

        foreach ($providers_data as $datum){
            $cell_A = 'A' . $cont;$cell_B = 'B' . $cont;
            $cell_C = 'C' . $cont;$cell_D = 'D' . $cont;
            $cell_E = 'E' . $cont;$cell_F = 'F' . $cont;
            $cell_G = 'G' . $cont;$cell_H = 'H' . $cont;
            $cell_I = 'I' . $cont;$cell_J = 'J' . $cont;
            $cell_K = 'K' . $cont;$cell_M = 'M' . $cont;
            $cell_N = 'N' . $cont;$cell_O = 'O' . $cont;

            $data=$datum->getData();
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, strval($data['record_tracking_number']));
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $data['provider_id']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, strval($data['first_name']));
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $data['last_name']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $data['license_number']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $data['ssn_fein']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $data['npi_number']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $data['start_date']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $data['end_date']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $data['provider_type']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $data['primary_specialty']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_M, $data['gender']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $data['provider_application_receipt_date']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_O, $data['credentialed_date']);

            $cont++;
        }

        //get all records from FCCData for organizations for PG
        $organizations_data=$em->getRepository('App\Entity\FCCDataOrg')->findAll();
        foreach ($organizations_data as $datum){
            $cell_A = 'A' . $cont;$cell_B = 'B' . $cont;
            $cell_C = 'C' . $cont;$cell_D = 'D' . $cont;
            $cell_E = 'E' . $cont;$cell_F = 'F' . $cont;
            $cell_G = 'G' . $cont;$cell_H = 'H' . $cont;
            $cell_I = 'I' . $cont;$cell_J = 'J' . $cont;
            $cell_K = 'K' . $cont;$cell_M = 'M' . $cont;
            $cell_N = 'N' . $cont;$cell_O = 'O' . $cont;
            $cell_L = 'L' . $cont;

            $data=$datum->getData();

            $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $data['record_tracking_number']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $data['provider_id']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $data['first_name']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $data['last_name']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $data['license_number']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $data['ssn_fein']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $data['npi_number']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $data['start_date']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $data['end_date']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $data['provider_type']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $data['primary_specialty']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_M, $data['gender']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $data['provider_application_receipt_date']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_O, $data['credentialed_date']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $data['ahca_id']);

            $cont++;
        }

        $spreadsheet->setActiveSheetIndex(1);

        $cont2=2;
        $all_sl_records=$em->getRepository('App\Entity\FCCDataSL')->findAll();
        $plan_medicaid="100053001~100053002~100053003~100053004~100053005~100053006~100053007~100053008~100053009~100053010~100053011";
        if($all_sl_records){
            foreach ($all_sl_records as $item){
                $cell_A = 'A' . $cont2;$cell_B = 'B' . $cont2;$cell_C = 'C' . $cont2;$cell_H = 'H' . $cont2;
                $cell_G = 'G' . $cont2;$cell_I = 'I' . $cont2;$cell_J = 'J' . $cont2;$cell_K = 'K' . $cont2;
                $cell_L = 'L' . $cont2;$cell_M = 'M' . $cont2;$cell_O = 'O' . $cont2;$cell_Q = 'Q' . $cont2;
                $cell_R = 'R' . $cont2;$cell_S = 'S' . $cont2;$cell_T = 'T' . $cont2;$cell_U = 'U' . $cont2;
                $cell_V = 'V' . $cont2;$cell_AA = 'AA' . $cont2;$cell_E = 'E' . $cont2;$cell_Y = 'Y' . $cont2;
                $cell_N = 'N' . $cont2;$cell_W = 'W' . $cont2;$cell_X = 'X' . $cont2;$cell_Z = 'Z' . $cont2;
                $cell_AF = 'AF' . $cont2;$cell_F = 'F' . $cont2;$cell_AG = 'AG' . $cont2;$cell_D='D'.$cont2;

                $data=$item->getData();

                $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, $data['col_a']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_B, $plan_medicaid);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, strval($data['col_c']));
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $data['col_d']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, $data['col_e']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $data['col_f']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_G, $data['col_g']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_H, $data['col_h']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $data['col_i']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $data['col_j']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $data['col_k']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $data['col_l']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $data['col_m']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, $data['col_n']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, $data['col_o']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Q, $data['col_q']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_R, $data['col_r']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_S, $data['col_s']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_T, $data['col_t']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_U, $data['col_u']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_V, $data['col_v']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_W, $data['col_w']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $data['col_x']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $data['col_y']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z, $data['col_z']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AF, $data['col_af']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AA, strval($data['col_aa']));
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AG, $data['col_ag']);

                $cont2++;
            }
        }


        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="FCC_BSN_PNV_'.date('Ymd').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/roster-export", name="fcc_roster_report_export")
     */
    public function fcc_roster_export()
    {
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');;

        $spreadsheet = $reader->load($url.'fcc_roster_template.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $em=$this->getDoctrine()->getManager();
        $providers=$em->getRepository('App\Entity\FCCRosterData')->findAll();
        $cont=2;

        foreach ($providers as $record){
            if($record->getStatus()!="to_remove"){

                $cell_A='A'.$cont;$cell_F='F'.$cont;$cell_E='E'.$cont;$cell_C='C'.$cont;
                $cell_G='G'.$cont;$cell_H='H'.$cont;$cell_I='I'.$cont;$cell_J='J'.$cont;$cell_K='K'.$cont;$cell_L='L'.$cont;$cell_M='M'.$cont;
                $cell_N='N'.$cont;$cell_O='O'.$cont;$cell_P='P'.$cont;$cell_Q='Q'.$cont;$cell_R='R'.$cont;$cell_S='S'.$cont;$cell_T='T'.$cont;
                $cell_U='U'.$cont;$cell_V='V'.$cont;$cell_W='W'.$cont;$cell_X='X'.$cont;$cell_Y='Y'.$cont;$cell_Z='Z'.$cont;

                $cell_AA='AA'.$cont;$cell_AB='AB'.$cont;$cell_AC='AC'.$cont;$cell_AD='AD'.$cont;$cell_AE='AE'.$cont;
                $cell_AF='AF'.$cont;$cell_AG='AG'.$cont;$cell_AH='AH'.$cont;$cell_AI='AI'.$cont;$cell_AJ='AJ'.$cont;
                $cell_AK='AK'.$cont;$cell_AL='AL'.$cont;$cell_AM='AM'.$cont;$cell_AN='AN'.$cont;$cell_AO='AO'.$cont;
                $cell_AP='AP'.$cont;$cell_AQ='AQ'.$cont;$cell_AR='AR'.$cont;$cell_AS='AS'.$cont;$cell_AT='AT'.$cont;
                $cell_AU='AU'.$cont;$cell_AV='AV'.$cont;$cell_AW='AW'.$cont;$cell_AX='AX'.$cont;$cell_AY='AY'.$cont;$cell_AZ='AZ'.$cont;

                $cell_BA='BA'.$cont;$cell_BB='BB'.$cont;$cell_BC='BC'.$cont;$cell_BD='BD'.$cont;$cell_BE='BE'.$cont;$cell_BF='BF'.$cont;
                $cell_BG='BG'.$cont;$cell_BH='BH'.$cont;$cell_BI='BI'.$cont;$cell_BJ='BJ'.$cont;$cell_BK='BK'.$cont;$cell_BL='BL'.$cont;
                $cell_BM='BM'.$cont;$cell_BN='BN'.$cont;$cell_BO='BO'.$cont;$cell_BP='BP'.$cont;$cell_BQ='BQ'.$cont;$cell_BR='BR'.$cont;
                $cell_BS='BS'.$cont;$cell_BT='BT'.$cont;$cell_BU='BU'.$cont;$cell_BV='BV'.$cont;$cell_BW='BW'.$cont;$cell_BX='BX'.$cont;
                $cell_BY='BY'.$cont;$cell_BZ='BZ'.$cont;

                $cell_CA='CA'.$cont;$cell_CB='CB'.$cont;$cell_CC='CC'.$cont;$cell_CD='CD'.$cont;$cell_CE='CE'.$cont;$cell_CF='CF'.$cont;
                $cell_CG='CG'.$cont;$cell_CH='CH'.$cont;$cell_CI='CI'.$cont;$cell_CJ='CJ'.$cont;$cell_CK='CK'.$cont;$cell_CL='CL'.$cont;
                $cell_CM='CM'.$cont;$cell_CN='CN'.$cont;$cell_CO='CO'.$cont;$cell_CP='CP'.$cont;$cell_CQ='CQ'.$cont;$cell_CR='CR'.$cont;
                $cell_CS='CS'.$cont;$cell_CT='CT'.$cont;$cell_CU='CU'.$cont;$cell_CV='CV'.$cont;$cell_CW='CW'.$cont;$cell_CX='CX'.$cont;
                $cell_CY='CY'.$cont;$cell_CZ='CZ'.$cont;

                $cell_DA='DA'.$cont;$cell_DB='DB'.$cont;$cell_DC='DC'.$cont;$cell_DD='DD'.$cont;$cell_DE='DE'.$cont;$cell_DF='DF'.$cont;
                $cell_DG='DG'.$cont;$cell_DH='DH'.$cont;$cell_DI='DI'.$cont;$cell_DJ='DJ'.$cont;$cell_DK='DK'.$cont;$cell_DL='DL'.$cont;
                $cell_DM='DM'.$cont;$cell_DN='DN'.$cont;$cell_DO='DO'.$cont;$cell_DP='DP'.$cont;$cell_DQ='DQ'.$cont;$cell_DR='DR'.$cont;
                $cell_DT='DT'.$cont;$cell_DU='DU'.$cont;;$cell_DV='DV'.$cont;$cell_DY='DY'.$cont;$cell_DZ='DZ'.$cont;

                $data=$record->getData();
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $data['col_a']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $data['col_f']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $data['col_g']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $data['col_h']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $data['col_i']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $data['col_j']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $data['col_k']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_M, $data['col_m']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_P, $data['col_p']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_S, $data['col_s']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_V, $data['col_v']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_Z, $data['col_z']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AA, $data['col_aa']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AB, $data['col_ab']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AC, $data['col_ac']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AD, $data['col_ad']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AE, $data['col_ae']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AF, $data['col_af']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AG, $data['col_ag']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AH, $data['col_ah']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AI, $data['col_ai']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AJ, $data['col_aj']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AK, $data['col_ak']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AL, $data['col_al']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_BO, $data['col_bo']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $data['col_c']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_BQ, $data['col_bq']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_BR, $data['col_br']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_BT, $data['col_bt']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_BS, $data['col_bs']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_BU, $data['col_bu']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_BV, $data['col_bv']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_BW, $data['col_bw']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_BY, $data['col_by']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_CF, $data['col_cf']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DU, $data['col_du']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DV, $data['col_dv']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DY, $data['col_dy']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DZ, $data['col_dz']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $data['col_e']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_BP, $data['col_bp']);

                $spreadsheet->getActiveSheet(0)->setCellValue($cell_CB, $data['col_cb']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_CC, $data['col_cc']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_CD, $data['col_cd']);

                $spreadsheet->getActiveSheet(0)->setCellValue($cell_CG, $data['col_cg']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_CH, $data['col_ch']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_CI, $data['col_ci']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_CO, $data['col_co']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DD, $data['col_dd']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DE, $data['col_de']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CR, $data['col_cr']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CS, $data['col_cs']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CT, $data['col_ct']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CU, $data['col_cu']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CV, $data['col_cv']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CW, $data['col_cw']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CX, $data['col_cx']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CY, $data['col_cy']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CZ, $data['col_cz']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DA, $data['col_da']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DB, $data['col_db']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DC, $data['col_dc']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_CP, $data['col_cp']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_CQ, $data['col_cq']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DO, $data['col_do']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DP, $data['col_dp']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DQ, $data['col_dq']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DR, $data['col_dr']);
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_DT, $data['col_dt']);

                $cont++;
            }
        }

        //save new providers
        $cont2=2;
        $spreadsheet->setActiveSheetIndex(1);
        $new_records=$em->getRepository('App\Entity\FCCRosterData')->findBy(array('status'=>'new'));

        foreach ($new_records as $record){
            $cell_A='A'.$cont2;$cell_F='F'.$cont2;$cell_E='E'.$cont2;$cell_C='C'.$cont2;$cell_D='D'.$cont2;
            $cell_G='G'.$cont2;$cell_H='H'.$cont2;$cell_I='I'.$cont2;$cell_J='J'.$cont2;$cell_K='K'.$cont2;$cell_L='L'.$cont2;$cell_M='M'.$cont2;
            $cell_N='N'.$cont2;$cell_O='O'.$cont2;$cell_P='P'.$cont2;$cell_Q='Q'.$cont2;$cell_R='R'.$cont2;$cell_S='S'.$cont2;$cell_T='T'.$cont2;
            $cell_U='U'.$cont2;$cell_V='V'.$cont2;$cell_W='W'.$cont2;$cell_X='X'.$cont2;$cell_Y='Y'.$cont2;$cell_Z='Z'.$cont2;

            $cell_AA='AA'.$cont2;$cell_AB='AB'.$cont2;$cell_AC='AC'.$cont2;$cell_AD='AD'.$cont2;$cell_AE='AE'.$cont2;
            $cell_AF='AF'.$cont2;$cell_AG='AG'.$cont2;$cell_AH='AH'.$cont2;$cell_AI='AI'.$cont2;$cell_AJ='AJ'.$cont2;
            $cell_AK='AK'.$cont2;$cell_AL='AL'.$cont2;$cell_AM='AM'.$cont2;$cell_AN='AN'.$cont2;$cell_AO='AO'.$cont2;
            $cell_AP='AP'.$cont2;$cell_AQ='AQ'.$cont2;$cell_AR='AR'.$cont2;$cell_AS='AS'.$cont2;$cell_AT='AT'.$cont2;
            $cell_AU='AU'.$cont2;$cell_AV='AV'.$cont2;$cell_AW='AW'.$cont2;$cell_AX='AX'.$cont2;$cell_AY='AY'.$cont2;$cell_AZ='AZ'.$cont2;

            $cell_BA='BA'.$cont2;$cell_BB='BB'.$cont2;$cell_BC='BC'.$cont2;$cell_BD='BD'.$cont2;$cell_BE='BE'.$cont2;$cell_BF='BF'.$cont2;
            $cell_BG='BG'.$cont2;$cell_BH='BH'.$cont2;$cell_BI='BI'.$cont2;$cell_BJ='BJ'.$cont2;$cell_BK='BK'.$cont2;$cell_BL='BL'.$cont2;
            $cell_BM='BM'.$cont2;$cell_BN='BN'.$cont2;$cell_BO='BO'.$cont2;$cell_BP='BP'.$cont2;$cell_BQ='BQ'.$cont2;$cell_BR='BR'.$cont2;
            $cell_BS='BS'.$cont2;$cell_BT='BT'.$cont2;$cell_BU='BU'.$cont2;$cell_BV='BV'.$cont2;$cell_BW='BW'.$cont2;$cell_BX='BX'.$cont2;
            $cell_BY='BY'.$cont2;$cell_BZ='BZ'.$cont2;

            $cell_CA='CA'.$cont2;$cell_CB='CB'.$cont2;$cell_CC='CC'.$cont2;$cell_CD='CD'.$cont2;$cell_CE='CE'.$cont2;$cell_CF='CF'.$cont2;
            $cell_CG='CG'.$cont2;$cell_CH='CH'.$cont2;$cell_CI='CI'.$cont2;$cell_CJ='CJ'.$cont2;$cell_CK='CK'.$cont2;$cell_CL='CL'.$cont2;
            $cell_CM='CM'.$cont2;$cell_CN='CN'.$cont2;$cell_CO='CO'.$cont2;$cell_CP='CP'.$cont2;$cell_CQ='CQ'.$cont2;$cell_CR='CR'.$cont2;
            $cell_CS='CS'.$cont2;$cell_CT='CT'.$cont2;$cell_CU='CU'.$cont2;$cell_CV='CV'.$cont2;$cell_CW='CW'.$cont2;$cell_CX='CX'.$cont2;
            $cell_CY='CY'.$cont2;$cell_CZ='CZ'.$cont2;

            $cell_DA='DA'.$cont2;$cell_DB='DB'.$cont2;$cell_DC='DC'.$cont2;$cell_DD='DD'.$cont2;$cell_DE='DE'.$cont2;$cell_DF='DF'.$cont2;
            $cell_DG='DG'.$cont2;$cell_DH='DH'.$cont2;$cell_DI='DI'.$cont2;$cell_DJ='DJ'.$cont2;$cell_DK='DK'.$cont2;$cell_DL='DL'.$cont2;
            $cell_DM='DM'.$cont2;$cell_DN='DN'.$cont2;$cell_DO='DO'.$cont2;$cell_DP='DP'.$cont2;$cell_DQ='DQ'.$cont2;$cell_DR='DR'.$cont2;
            $cell_DT='DT'.$cont2;$cell_DU='DU'.$cont2;;$cell_DV='DV'.$cont2;$cell_DY='DY'.$cont2;$cell_DZ='DZ'.$cont2;

            $data=$record->getData();
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $data['col_a']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $data['col_f']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $data['col_g']);

            $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $data['col_h']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $data['col_i']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $data['col_j']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $data['col_k']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $data['col_m']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $data['col_p']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $data['col_s']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_P, $data['col_v']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_U, $data['col_z']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_V, $data['col_aa']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_W, $data['col_ab']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_Y, $data['col_ad']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_Z, $data['col_ae']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AA, $data['col_af']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AB, $data['col_ag']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AC, $data['col_ah']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AD, $data['col_ai']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AE, $data['col_aj']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AF, $data['col_ak']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AG, $data['col_al']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AI, $data['col_bo']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AJ, $data['col_bp']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AL, $data['col_bq']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AM, $data['col_br']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AN, $data['col_bt']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AO, $data['col_bs']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AP, $data['col_bu']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AQ, $data['col_bv']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AR, $data['col_bw']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AT, $data['col_by']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AU, "");
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AW, $data['col_cb']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AX, $data['col_cc']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AY, $data['col_cd']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BA, $data['col_cf']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BB, $data['col_cg']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BC, $data['col_ch']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BD, $data['col_ci']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BJ, "N");
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BK, $data['col_cr']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BL, $data['col_cs']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BM, $data['col_ct']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BN, $data['col_cu']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BO, $data['col_cv']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BP, $data['col_cw']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BQ, $data['col_cx']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BR, $data['col_cy']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BS, $data['col_cz']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BT, $data['col_da']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BU, $data['col_db']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BV, $data['col_dc']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BW, $data['col_dd']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BX, $data['col_de']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DI, $data['col_do']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DJ, $data['col_dp']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DK, $data['col_dq']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DL, $data['col_dr']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DN, $data['col_dt']);

            $cont2++;
        }

        //save changed records
        $cont3=2;
        $spreadsheet->setActiveSheetIndex(2);
        $chenged_records=$em->getRepository('App\Entity\FCCRosterData')->findBy(array('status'=>'changed'));

        foreach ($chenged_records as $record){
            $cell_A='A'.$cont3;$cell_F='F'.$cont3;$cell_E='E'.$cont3;$cell_C='C'.$cont3;$cell_D='D'.$cont3;
            $cell_G='G'.$cont3;$cell_H='H'.$cont3;$cell_I='I'.$cont3;$cell_J='J'.$cont3;$cell_K='K'.$cont3;$cell_L='L'.$cont3;$cell_M='M'.$cont3;
            $cell_N='N'.$cont3;$cell_O='O'.$cont3;$cell_P='P'.$cont3;$cell_Q='Q'.$cont3;$cell_R='R'.$cont3;$cell_S='S'.$cont3;$cell_T='T'.$cont3;
            $cell_U='U'.$cont3;$cell_V='V'.$cont3;$cell_W='W'.$cont3;$cell_X='X'.$cont3;$cell_Y='Y'.$cont3;$cell_Z='Z'.$cont3;

            $cell_AA='AA'.$cont3;$cell_AB='AB'.$cont3;$cell_AC='AC'.$cont3;$cell_AD='AD'.$cont3;$cell_AE='AE'.$cont3;
            $cell_AF='AF'.$cont3;$cell_AG='AG'.$cont3;$cell_AH='AH'.$cont3;$cell_AI='AI'.$cont3;$cell_AJ='AJ'.$cont3;
            $cell_AK='AK'.$cont3;$cell_AL='AL'.$cont3;$cell_AM='AM'.$cont3;$cell_AN='AN'.$cont3;$cell_AO='AO'.$cont3;
            $cell_AP='AP'.$cont3;$cell_AQ='AQ'.$cont3;$cell_AR='AR'.$cont3;$cell_AS='AS'.$cont3;$cell_AT='AT'.$cont3;
            $cell_AU='AU'.$cont3;$cell_AV='AV'.$cont3;$cell_AW='AW'.$cont3;$cell_AX='AX'.$cont3;$cell_AY='AY'.$cont3;$cell_AZ='AZ'.$cont3;

            $cell_BA='BA'.$cont3;$cell_BB='BB'.$cont3;$cell_BC='BC'.$cont3;$cell_BD='BD'.$cont3;$cell_BE='BE'.$cont3;$cell_BF='BF'.$cont3;
            $cell_BG='BG'.$cont3;$cell_BH='BH'.$cont3;$cell_BI='BI'.$cont3;$cell_BJ='BJ'.$cont3;$cell_BK='BK'.$cont3;$cell_BL='BL'.$cont3;
            $cell_BM='BM'.$cont3;$cell_BN='BN'.$cont3;$cell_BO='BO'.$cont3;$cell_BP='BP'.$cont3;$cell_BQ='BQ'.$cont3;$cell_BR='BR'.$cont3;
            $cell_BS='BS'.$cont3;$cell_BT='BT'.$cont3;$cell_BU='BU'.$cont3;$cell_BV='BV'.$cont3;$cell_BW='BW'.$cont3;$cell_BX='BX'.$cont3;
            $cell_BY='BY'.$cont3;$cell_BZ='BZ'.$cont3;

            $cell_CA='CA'.$cont3;$cell_CB='CB'.$cont3;$cell_CC='CC'.$cont3;$cell_CD='CD'.$cont3;$cell_CE='CE'.$cont3;$cell_CF='CF'.$cont3;
            $cell_CG='CG'.$cont3;$cell_CH='CH'.$cont3;$cell_CI='CI'.$cont3;$cell_CJ='CJ'.$cont3;$cell_CK='CK'.$cont3;$cell_CL='CL'.$cont3;
            $cell_CM='CM'.$cont3;$cell_CN='CN'.$cont3;$cell_CO='CO'.$cont3;$cell_CP='CP'.$cont3;$cell_CQ='CQ'.$cont3;$cell_CR='CR'.$cont3;
            $cell_CS='CS'.$cont3;$cell_CT='CT'.$cont3;$cell_CU='CU'.$cont3;$cell_CV='CV'.$cont3;$cell_CW='CW'.$cont3;$cell_CX='CX'.$cont3;
            $cell_CY='CY'.$cont3;$cell_CZ='CZ'.$cont3;

            $cell_DA='DA'.$cont3;$cell_DB='DB'.$cont3;$cell_DC='DC'.$cont3;$cell_DD='DD'.$cont3;$cell_DE='DE'.$cont3;$cell_DF='DF'.$cont3;
            $cell_DG='DG'.$cont3;$cell_DH='DH'.$cont3;$cell_DI='DI'.$cont3;$cell_DJ='DJ'.$cont3;$cell_DK='DK'.$cont3;$cell_DL='DL'.$cont3;
            $cell_DM='DM'.$cont3;$cell_DN='DN'.$cont3;$cell_DO='DO'.$cont3;$cell_DP='DP'.$cont3;$cell_DQ='DQ'.$cont3;$cell_DR='DR'.$cont3;
            $cell_DT='DT'.$cont3;$cell_DU='DU'.$cont3;;$cell_DV='DV'.$cont3;$cell_DY='DY'.$cont3;$cell_DZ='DZ'.$cont3;

            $data=$record->getData();
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $data['col_a']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $data['col_f']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $data['col_g']);

            $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $data['col_h']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $data['col_i']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $data['col_j']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $data['col_k']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $data['col_m']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $data['col_p']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $data['col_s']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_P, $data['col_v']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_U, $data['col_z']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_V, $data['col_aa']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_W, $data['col_ab']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_Y, $data['col_ad']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_Z, $data['col_ae']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AA, $data['col_af']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AB, $data['col_ag']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AC, $data['col_ah']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AD, $data['col_ai']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AE, $data['col_aj']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AF, $data['col_ak']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AG, $data['col_al']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AI, $data['col_bo']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AJ, $data['col_bp']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AL, $data['col_bq']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AM, $data['col_br']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AN, $data['col_bt']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AO, $data['col_bs']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AP, $data['col_bu']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AQ, $data['col_bv']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AR, $data['col_bw']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AT, $data['col_by']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AU, "");
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AW, $data['col_cb']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AX, $data['col_cc']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AY, $data['col_cd']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BA, $data['col_cf']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BB, $data['col_cg']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BC, $data['col_ch']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BD, $data['col_ci']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BJ, "N");
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BK, $data['col_cr']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BL, $data['col_cs']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BM, $data['col_ct']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BN, $data['col_cu']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BO, $data['col_cv']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BP, $data['col_cw']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BQ, $data['col_cx']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BR, $data['col_cy']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BS, $data['col_cz']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BT, $data['col_da']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BU, $data['col_db']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BV, $data['col_dc']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BW, $data['col_dd']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BX, $data['col_de']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DI, $data['col_do']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DJ, $data['col_dp']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DK, $data['col_dq']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DL, $data['col_dr']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DN, $data['col_dt']);

            $cont3++;

        }


        $spreadsheet->setActiveSheetIndex(3);
        $cont4=2;

        $remove_records=$em->getRepository('App\Entity\FCCRosterData')->findBy(array('status'=>'to_remove'));
        foreach ($remove_records as $record){


            $cell_A='A'.$cont4;$cell_F='F'.$cont4;$cell_E='E'.$cont4;$cell_C='C'.$cont4;$cell_D='D'.$cont4;
            $cell_G='G'.$cont4;$cell_H='H'.$cont4;$cell_I='I'.$cont4;$cell_J='J'.$cont4;$cell_K='K'.$cont4;$cell_L='L'.$cont4;$cell_M='M'.$cont4;
            $cell_N='N'.$cont4;$cell_O='O'.$cont4;$cell_P='P'.$cont4;$cell_Q='Q'.$cont4;$cell_R='R'.$cont4;$cell_S='S'.$cont4;$cell_T='T'.$cont4;
            $cell_U='U'.$cont4;$cell_V='V'.$cont4;$cell_W='W'.$cont4;$cell_X='X'.$cont4;$cell_Y='Y'.$cont4;$cell_Z='Z'.$cont4;

            $cell_AA='AA'.$cont4;$cell_AB='AB'.$cont4;$cell_AC='AC'.$cont4;$cell_AD='AD'.$cont4;$cell_AE='AE'.$cont4;
            $cell_AF='AF'.$cont4;$cell_AG='AG'.$cont4;$cell_AH='AH'.$cont4;$cell_AI='AI'.$cont4;$cell_AJ='AJ'.$cont4;
            $cell_AK='AK'.$cont4;$cell_AL='AL'.$cont4;$cell_AM='AM'.$cont4;$cell_AN='AN'.$cont4;$cell_AO='AO'.$cont4;
            $cell_AP='AP'.$cont4;$cell_AQ='AQ'.$cont4;$cell_AR='AR'.$cont4;$cell_AS='AS'.$cont4;$cell_AT='AT'.$cont4;
            $cell_AU='AU'.$cont4;$cell_AV='AV'.$cont4;$cell_AW='AW'.$cont4;$cell_AX='AX'.$cont4;$cell_AY='AY'.$cont4;$cell_AZ='AZ'.$cont4;

            $cell_BA='BA'.$cont4;$cell_BB='BB'.$cont4;$cell_BC='BC'.$cont4;$cell_BD='BD'.$cont4;$cell_BE='BE'.$cont4;$cell_BF='BF'.$cont4;
            $cell_BG='BG'.$cont4;$cell_BH='BH'.$cont4;$cell_BI='BI'.$cont4;$cell_BJ='BJ'.$cont4;$cell_BK='BK'.$cont4;$cell_BL='BL'.$cont4;
            $cell_BM='BM'.$cont4;$cell_BN='BN'.$cont4;$cell_BO='BO'.$cont4;$cell_BP='BP'.$cont4;$cell_BQ='BQ'.$cont4;$cell_BR='BR'.$cont4;
            $cell_BS='BS'.$cont4;$cell_BT='BT'.$cont4;$cell_BU='BU'.$cont4;$cell_BV='BV'.$cont4;$cell_BW='BW'.$cont4;$cell_BX='BX'.$cont4;
            $cell_BY='BY'.$cont4;$cell_BZ='BZ'.$cont4;

            $cell_CA='CA'.$cont4;$cell_CB='CB'.$cont4;$cell_CC='CC'.$cont4;$cell_CD='CD'.$cont4;$cell_CE='CE'.$cont4;$cell_CF='CF'.$cont4;
            $cell_CG='CG'.$cont4;$cell_CH='CH'.$cont4;$cell_CI='CI'.$cont4;$cell_CJ='CJ'.$cont4;$cell_CK='CK'.$cont4;$cell_CL='CL'.$cont4;
            $cell_CM='CM'.$cont4;$cell_CN='CN'.$cont4;$cell_CO='CO'.$cont4;$cell_CP='CP'.$cont4;$cell_CQ='CQ'.$cont4;$cell_CR='CR'.$cont4;
            $cell_CS='CS'.$cont4;$cell_CT='CT'.$cont4;$cell_CU='CU'.$cont4;$cell_CV='CV'.$cont4;$cell_CW='CW'.$cont4;$cell_CX='CX'.$cont4;
            $cell_CY='CY'.$cont4;$cell_CZ='CZ'.$cont4;

            $cell_DA='DA'.$cont4;$cell_DB='DB'.$cont4;$cell_DC='DC'.$cont4;$cell_DD='DD'.$cont4;$cell_DE='DE'.$cont4;$cell_DF='DF'.$cont4;
            $cell_DG='DG'.$cont4;$cell_DH='DH'.$cont4;$cell_DI='DI'.$cont4;$cell_DJ='DJ'.$cont4;$cell_DK='DK'.$cont4;$cell_DL='DL'.$cont4;
            $cell_DM='DM'.$cont4;$cell_DN='DN'.$cont4;$cell_DO='DO'.$cont4;$cell_DP='DP'.$cont4;$cell_DQ='DQ'.$cont4;$cell_DR='DR'.$cont4;
            $cell_DT='DT'.$cont4;$cell_DU='DU'.$cont4;;$cell_DV='DV'.$cont4;$cell_DY='DY'.$cont4;$cell_DZ='DZ'.$cont4;

            $data=$record->getData();

            $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $data['col_a']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $data['col_f']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $data['col_g']);

            $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $data['col_h']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $data['col_i']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $data['col_j']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $data['col_k']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $data['col_m']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $data['col_p']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $data['col_s']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_P, $data['col_v']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_U, $data['col_z']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_V, $data['col_aa']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_W, $data['col_ab']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_Y, $data['col_ad']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_Z, $data['col_ae']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AA, $data['col_af']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AB, $data['col_ag']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AC, $data['col_ah']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AD, $data['col_ai']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AE, $data['col_aj']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AF, $data['col_ak']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AG, $data['col_al']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AI, $data['col_bo']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AJ, $data['col_bp']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AL, $data['col_bq']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AM, $data['col_br']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AN, $data['col_bt']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AO, $data['col_bs']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AP, $data['col_bu']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AQ, $data['col_bv']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AR, $data['col_bw']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AT, $data['col_by']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AU, "");
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AW, $data['col_cb']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AX, $data['col_cc']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AY, $data['col_cd']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BA, $data['col_cf']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BB, $data['col_cg']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BC, $data['col_ch']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BD, $data['col_ci']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BJ, "N");
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BK, $data['col_cr']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BL, $data['col_cs']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BM, $data['col_ct']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BN, $data['col_cu']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BO, $data['col_cv']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BP, $data['col_cw']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BQ, $data['col_cx']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BR, $data['col_cy']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BS, $data['col_cz']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BT, $data['col_da']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BU, $data['col_db']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BV, $data['col_dc']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BW, $data['col_dd']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BX, $data['col_de']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DI, $data['col_do']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DJ, $data['col_dp']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DK, $data['col_dq']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DL, $data['col_dr']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DN, $data['col_dt']);

            $cont4++;
        }


        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="FCC_BSN_Roster'.date('Ymd').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");

    }

    /**
     * @Route("/roster-medicare-export", name="fcc_roster_medicare_report_export")
     */
    public function fcc_roster_medicare_export(){
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');;

        $spreadsheet = $reader->load($url.'fcc_roster_medicare_template.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $em=$this->getDoctrine()->getManager();
        $records=$em->getRepository('App:FCCRosterDataMedicare')->findAll();
        $cont=2;

        foreach ($records as $record){
            $cell_A='A'.$cont;$cell_F='F'.$cont;$cell_E='E'.$cont;$cell_C='C'.$cont;
            $cell_G='G'.$cont;$cell_H='H'.$cont;$cell_I='I'.$cont;$cell_J='J'.$cont;$cell_K='K'.$cont;$cell_L='L'.$cont;$cell_M='M'.$cont;
            $cell_N='N'.$cont;$cell_O='O'.$cont;$cell_P='P'.$cont;$cell_Q='Q'.$cont;$cell_R='R'.$cont;$cell_S='S'.$cont;$cell_T='T'.$cont;
            $cell_U='U'.$cont;$cell_V='V'.$cont;$cell_W='W'.$cont;$cell_X='X'.$cont;$cell_Y='Y'.$cont;$cell_Z='Z'.$cont;

            $cell_AA='AA'.$cont;$cell_AB='AB'.$cont;$cell_AC='AC'.$cont;$cell_AD='AD'.$cont;$cell_AE='AE'.$cont;
            $cell_AF='AF'.$cont;$cell_AG='AG'.$cont;$cell_AH='AH'.$cont;$cell_AI='AI'.$cont;$cell_AJ='AJ'.$cont;
            $cell_AK='AK'.$cont;$cell_AL='AL'.$cont;$cell_AM='AM'.$cont;$cell_AN='AN'.$cont;$cell_AO='AO'.$cont;
            $cell_AP='AP'.$cont;$cell_AQ='AQ'.$cont;$cell_AR='AR'.$cont;$cell_AS='AS'.$cont;$cell_AT='AT'.$cont;
            $cell_AU='AU'.$cont;$cell_AV='AV'.$cont;$cell_AW='AW'.$cont;$cell_AX='AX'.$cont;$cell_AY='AY'.$cont;$cell_AZ='AZ'.$cont;

            $cell_BA='BA'.$cont;$cell_BB='BB'.$cont;$cell_BC='BC'.$cont;$cell_BD='BD'.$cont;$cell_BE='BE'.$cont;$cell_BF='BF'.$cont;
            $cell_BG='BG'.$cont;$cell_BH='BH'.$cont;$cell_BI='BI'.$cont;$cell_BJ='BJ'.$cont;$cell_BK='BK'.$cont;$cell_BL='BL'.$cont;
            $cell_BM='BM'.$cont;$cell_BN='BN'.$cont;$cell_BO='BO'.$cont;$cell_BP='BP'.$cont;$cell_BQ='BQ'.$cont;$cell_BR='BR'.$cont;
            $cell_BS='BS'.$cont;$cell_BT='BT'.$cont;$cell_BU='BU'.$cont;$cell_BV='BV'.$cont;$cell_BW='BW'.$cont;$cell_BX='BX'.$cont;
            $cell_BY='BY'.$cont;$cell_BZ='BZ'.$cont;

            $cell_CA='CA'.$cont;$cell_CB='CB'.$cont;$cell_CC='CC'.$cont;$cell_CD='CD'.$cont;$cell_CE='CE'.$cont;$cell_CF='CF'.$cont;
            $cell_CG='CG'.$cont;$cell_CH='CH'.$cont;$cell_CI='CI'.$cont;$cell_CJ='CJ'.$cont;$cell_CK='CK'.$cont;$cell_CL='CL'.$cont;
            $cell_CM='CM'.$cont;$cell_CN='CN'.$cont;$cell_CO='CO'.$cont;$cell_CP='CP'.$cont;$cell_CQ='CQ'.$cont;$cell_CR='CR'.$cont;
            $cell_CS='CS'.$cont;$cell_CT='CT'.$cont;$cell_CU='CU'.$cont;$cell_CV='CV'.$cont;$cell_CW='CW'.$cont;$cell_CX='CX'.$cont;
            $cell_CY='CY'.$cont;$cell_CZ='CZ'.$cont;

            $cell_DA='DA'.$cont;$cell_DB='DB'.$cont;$cell_DC='DC'.$cont;$cell_DD='DD'.$cont;$cell_DE='DE'.$cont;$cell_DF='DF'.$cont;
            $cell_DG='DG'.$cont;$cell_DH='DH'.$cont;$cell_DI='DI'.$cont;$cell_DJ='DJ'.$cont;$cell_DK='DK'.$cont;$cell_DL='DL'.$cont;
            $cell_DM='DM'.$cont;$cell_DN='DN'.$cont;$cell_DO='DO'.$cont;$cell_DP='DP'.$cont;$cell_DQ='DQ'.$cont;$cell_DR='DR'.$cont;
            $cell_DT='DT'.$cont;$cell_DU='DU'.$cont;;$cell_DV='DV'.$cont;$cell_DY='DY'.$cont;$cell_DZ='DZ'.$cont;

            $data=$record->getData();
            $data=$record->getData();
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $data['col_a']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $data['col_c']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $data['col_f']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $data['col_g']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $data['col_h']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $data['col_l']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $data['col_i']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $data['col_j']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_M, $data['col_m']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_P, $data['col_p']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_S, $data['col_s']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_V, $data['col_v']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_Z, $data['col_z']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AA, $data['col_aa']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AB, $data['col_ab']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AC, $data['col_ac']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AD, $data['col_ad']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AE, $data['col_ae']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AF, $data['col_af']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AG, $data['col_ag']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AH, $data['col_ah']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AI, $data['col_ai']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AJ, $data['col_aj']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AK, $data['col_ak']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AL, $data['col_al']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BO, $data['col_bo']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BQ, $data['col_bq']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BR, $data['col_br']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BT, $data['col_bt']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BS, $data['col_bs']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BU, $data['col_bu']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BV, $data['col_bv']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BW, $data['col_bw']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BY, $data['col_by']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_CF, $data['col_cf']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DU, $data['col_du']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DV, $data['col_dv']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DY, $data['col_dy']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DZ, $data['col_dz']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $data['col_e']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BP, $data['col_bp']);

            $spreadsheet->getActiveSheet(0)->setCellValue($cell_CB, $data['col_cb']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_CC, $data['col_cc']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_CD, $data['col_cd']);

            $spreadsheet->getActiveSheet(0)->setCellValue($cell_CG, $data['col_cg']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_CH, $data['col_ch']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_CI, $data['col_ci']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_CO, $data['col_co']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_DD, $data['col_dd']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_DE, $data['col_de']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CR, $data['col_cr']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CS, $data['col_cs']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CT, $data['col_ct']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CU, $data['col_cu']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CV, $data['col_cv']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CW, $data['col_cw']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CX, $data['col_cx']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CY, $data['col_cy']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CZ, $data['col_cz']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_DA, $data['col_da']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_DB, $data['col_db']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_DC, $data['col_dc']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_CP, $data['col_cp']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_CQ, $data['col_cq']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DO, $data['col_do']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DP, $data['col_dp']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DQ, $data['col_dq']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DR, $data['col_dr']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_DT, $data['col_dt']);

            $cont++;
        }

        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="FCC_BSN_Medicare_Roster'.date('Ymd').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }
}
