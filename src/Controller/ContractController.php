<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/contract")
 */
class ContractController extends AbstractController
{
    /**
     * @Route("/index", name="admin_contract_index")
     */
    public function index()
    {
        $em=$this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $email_templates=$em->getRepository('App\Entity\EmailType')->findBy(array(),array('name'=>'ASC'));

        $sql="SELECT o.id, o.name, o.bda_name, o.provider_type, o.group_npi,
            GROUP_CONCAT(DISTINCT `org_specialty`.`name` ORDER BY `org_specialty`.`name` ASC SEPARATOR ', ') AS `org_clasification`,
            organization_status.name AS status,
            o.created_on,
            GROUP_CONCAT(DISTINCT taxonomy_code.specialization ORDER BY taxonomy_code.id ASC SEPARATOR ', ') AS taxonomies_specialty,
            IF(SUM(IF(ba.is_facility, 1, 0)) > 0, 'Yes', 'No') AS `is_facility`
            FROM  organization o 
            LEFT JOIN billing_address ba on o.id = ba.organization_id
            LEFT JOIN organization_taxonomy ON organization_taxonomy.organization_id = o.id
            LEFT JOIN organization_status ON organization_status.id = o.status_id
            LEFT JOIN organization_org_specialties ON organization_org_specialties.organization_id =o.id    
            LEFT JOIN org_specialty ON org_specialty.id =organization_org_specialties.org_specialty_id    
            LEFT JOIN taxonomy_code ON taxonomy_code.id = organization_taxonomy.taxonomy_id
            WHERE organization_status.id=1   
            GROUP BY o.id
            ORDER BY o.name";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $organizations= $stmt->fetchAll();

        return $this->render('contract/index.html.twig', [
            'email_templates' => $email_templates, 'organizations' => $organizations,
            ]);
    }
}
