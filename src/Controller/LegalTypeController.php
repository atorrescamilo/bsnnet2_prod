<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\LegalType;

/**
 * @Route("/admin/legal_type")
 */
class LegalTypeController extends AbstractController{

    /**
     * @Route("/index", name="admin_legal_type")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $legaltypes = $em->getRepository('App\Entity\LegalType')->findAll();

            $delete_form_ajax = $this->createCustomForm('LEGALTYPE_ID', 'DELETE', 'admin_delete_legal_type');


            return $this->render('legal_type/index.html.twig', array('legaltypes' => $legaltypes, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_legal_type")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('legal_type/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_legal_type", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\LegalType')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_legal_type');
            }

            return $this->render('legal_type/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_legal_type", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\LegalType')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_legal_type');
            }

            return $this->render('legal_type/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_legal_type")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');

            $legaltype = new LegalType();
            $legaltype->setName($name);
            $legaltype->setDescription($description);

            $em->persist($legaltype);
            $em->flush();

            $this->addFlash(
                'success',
                'Legal Type has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_legal_type');
            }

            return $this->redirectToRoute('admin_legal_type');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_legal_type")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $legaltype = $em->getRepository('App\Entity\LegalType')->find($id);

            if ($legaltype == null) {
                $this->addFlash(
                    "danger",
                    "The Legal Type can't been updated!"
                );

                return $this->redirectToRoute('admin_legal_type');
            }

            if ($legaltype != null) {
                $legaltype->setName($name);
                $legaltype->setDescription($description);

                $em->persist($legaltype);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "The Legal Type has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_legal_type');
            }

            return $this->redirectToRoute('admin_legal_type');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_legal_type",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $legaltype = $legaltype = $em->getRepository('App\Entity\LegalType')->find($id);
            $removed = 0;
            $message = "";

            if ($legaltype) {
                try {
                    $em->remove($legaltype);
                    $em->flush();
                    $removed = 1;
                    $message = "The Legal Type has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Legal Type can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_legal_type",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $legaltype = $legaltype = $em->getRepository('App\Entity\LegalType')->find($id);

                if ($legaltype) {
                    try {
                        $em->remove($legaltype);
                        $em->flush();
                        $removed = 1;
                        $message = "The Legal Type has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Legal Type can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
