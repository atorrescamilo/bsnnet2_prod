<?php

namespace App\Controller;

use App\Entity\ChangeLog;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Accreditation;
use App\Form\AccreditationType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/accreditation")
 */
class AccreditationController extends AbstractController{

    /**
     * @Route("/index", name="admin_accreditation")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $accreditations = $em->getRepository('App\Entity\Accreditation')->findAll();

            $delete_form_ajax = $this->createCustomForm('ACCREDITATION_ID', 'DELETE', 'admin_delete_accreditation');


            return $this->render('accreditation/index.html.twig', array('accreditations' => $accreditations, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_accreditation")
     */
    public function add(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $accreditation=new Accreditation();
            $form = $this->createForm(AccreditationType::class, $accreditation);    

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $em->persist($accreditation);
                $em->flush();

                $action_type=$form['action_type']->getData();

                if($action_type==1){
                    return  $this->redirectToRoute('admin_accreditation');
                }else{
                    return  $this->redirectToRoute('admin_new_accreditation');
                }

                $this->addFlash(
                    "success",
                    "Accreditation has been created successfully!"
                );
            }


            return $this->render('accreditation/form.html.twig', ['form' => $form->createView(), 'action' => 'New']);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_accreditation",defaults={"id": null})
     */
    public function edit(Request $request, $id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $accreditation=$em->getRepository('App:Accreditation')->find($id);
            $form = $this->createForm(AccreditationType::class, $accreditation);    

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $em->persist($accreditation);
                $em->flush();

                $action_type=$form['action_type']->getData();

                if($action_type==1){
                    return  $this->redirectToRoute('admin_accreditation');
                }else{
                    return  $this->redirectToRoute('admin_new_accreditation');
                }

                $this->addFlash(
                    "success",
                    "Accreditation has been updated successfully!"
                );
            }

            return $this->render('accreditation/form.html.twig', ['form' => $form->createView(), 'action' => 'Edit']);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_accreditation",defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Accreditation')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_accreditation');
            }

            return $this->render('accreditation/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_accreditation",methods={"POST","DELETE"})
     */
    public function delete(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $accreditation = $em->getRepository('App\Entity\Accreditation')->find($id);
            $removed = 0;
            $message = "";

            if ($accreditation) {
                try {
                    $em->remove($accreditation);
                    $em->flush();
                    $removed = 1;
                    $message = "The Accreditation has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The accreditation can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/isUnique", name="admin_isunique_accreditation",methods={"POST"})
     */
    public function isUnique(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $code = $request->get('code');

            $accreditation = $em->getRepository('App\Entity\Accreditation')->findOneBy(array('code' => $code));

            $isUnique = 0;
            if ($accreditation != null) {
                $isUnique = 1;
            }

            return new Response(
                json_encode(array('isUnique' => $isUnique)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_accreditation",methods={"POST","DELETE"})
     */
    public function deleteMultiple(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $accreditation =  $em->getRepository('App\Entity\Accreditation')->find($id);

                if ($accreditation) {
                    try {
                        $em->remove($accreditation);
                        $em->flush();
                        $removed = 1;
                        $message = "The Accreditation has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Accreditation can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
