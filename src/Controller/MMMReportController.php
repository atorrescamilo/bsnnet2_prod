<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\BillingAddressCvo;
use App\Entity\MMMReportLog;
use App\Entity\Organization;
use App\Entity\OrganizationContact;
use App\Entity\Provider;
use App\Entity\Specialty;
use App\Entity\TaxonomyCode;
use App\Entity\ViewProvider;
use Monolog\Processor\ProcessIdProcessor;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Utils\My_Mcript;
use PhpOffice\PhpSpreadsheet\Reader;


/**
 * @Route("/admin/report")
*/
class MMMReportController extends AbstractController
{
    /**
     * @Route("/mmm/report_individual", name="mmm_report_individual")
    */
    public function individual(){
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');
        $spreadsheet = $reader->load($url.'template-mmm.xlsx');

        $existNPIArray=array(1053347955, 1053504845, 1053504845, 1093910499, 1144266206, 1184821928, 1184821928, 1245249218, 1275522013, 1295978260,
            1326128323, 1326128323, 1386632883, 1528151404, 1598103491, 1780726836, 1811177249, 1831281237, 1881732857, 1922096437, 1962510073, 1962510073,
            1962510073, 1962510073, 1700270113, 1063417483, 1649340415, 1366514705, 1518966126, 1538102397, 1528099561, 1275800104, 1881933703, 1316992019,
            1205883493, 1164769998, 1164769998, 1164769998, 1174707863, 1558409789, 1558409789, 1558409789, 1063600633, 1225105778, 1306148754, 1316116122,
            1336122464, 1346581758, 1588620470, 1497707574, 1558419572, 1568583979, 1962554337, 1972733442, 1114915717, 1578514865, 1144614660, 1689618647,
            1245487388, 1780670539, 1255400602, 1972979631, 1972979631, 1972979631, 1972979631, 1972979631, 1972979631, 1205219003, 1275897886, 1902310295, 1841573409, 1225248628,
            1619966538, 1477627602, 1639629694, 1932197548, 1376822981, 1356338784, 1295946002, 1700380649, 1700380649, 1700380649, 1710394499, 1215204433,
            1669872248, 1548327828, 1548327828, 1518964162, 1134117658, 1144653163, 1144653163, 1821086323, 1326438227, 1811048788, 1699763789,
            1184799546, 1154443380, 1437341914, 1609063544, 1609063544, 1609063544, 1225050107, 1427114255, 1679689699, 1679689699, 1881661684, 1881661684,
            1104083773, 1104185024, 1295895589, 1518182443, 1598961476, 1083888432, 1750403127, 1538408513, 1558552844, 1144384124, 1346238805, 1306944855, 1356774061,
            1962640003, 1144252982, 1174957914, 1255538641, 1235347956, 1821138819, 1821138819, 1821138819, 1821138819, 1003048422, 1972556124, 1659542462, 1720026628, 1598833832, 1861663973, 1952360620,
            1952524258, 1578790887, 1033578109, 1568614972, 1073960464, 1427055417, 1104250356, 1184830044, 1275758476, 1437460672, 1477615193, 1487864187, 1568810844,
            1679080501, 1689070922, 1730164914, 1821024704, 1366528705, 1861680936, 1104908508, 1962533828, 1942395850, 1447364070, 1184739567, 1184739567, 1902811490, 1942350236,
            1942350236, 1942350236, 1942350236, 1710321443, 1598846008, 1053759050, 1043625999, 1497014765, 1336248202, 1154426955, 1649336660, 1821341686, 1477747236, 1295067817,
            1861458226, 1598207508, 1881737732, 1265824833, 1174027023, 1528098050, 1275969099, 1366839607, 1942691720, 1942691720, 1326460080, 1912014804,
            1912014804, 1912014804, 1386095198, 1386946374, 1477656098, 1225143472, 1275045296, 1275045296, 1275886236, 1033651963, 1114154481, 1487799136, 1083871784,
            1083871784, 1295111623, 1295825263, 1134210271, 1326323718, 1306989454, 1003331646, 1548467095, 1568717296, 1679982375, 1720143910, 1780071274, 1780071274,
            1780071274, 1811985708, 1831270958, 1871552570, 1871765768, 1902944085, 1902967821, 1871741991, 1750450094, 1578654497, 1659842151, 1740752484, 1982162822,
            1316077514, 1336795509, 1346406840, 1417358078, 1548291834, 1649224320, 1568490506, 1598846008, 1225248628);

        $em=$this->getDoctrine()->getManager();

        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));
        $providersNPI=array();

        foreach ($providers as $provider) {
            $providersNPI[]=$provider->getNpiNumber();
        }

        $providersResult=array();
        $cont=1;
        foreach ($providers as $provider){
            $isMMM=false;
            if($provider->getIsFacility()==false){
                $payers=$provider->getPayers();
                if($payers!=null){

                    foreach ($payers as $payer){
                        if($payer->getId()==1){
                            $isMMM=true;
                            $cont++;
                        }
                    }
                }
            }
            if($isMMM==true){
                $providersResult[]=$provider;
            }
        }

        $date=date('m/d/Y');

        $company=$em->getRepository('App\Entity\Company')->find(1);
        $company_tin=$company->getTinNumber();
        $company_address=$company->getAddress();
        $company_name=$company->getName();

        $cont=2;
        foreach ($providersResult as $pro){
            $provider=$em->getRepository('App\Entity\Provider')->find($pro->getId());
            $providerV=$em->getRepository('App\Entity\ViewProvider')->find($pro->getId());

            $npi=$provider->getNpiNumber();
            $pdata=$em->getRepository('App\Entity\PDataMedicare')->findOneBy(array('npi_number'=>$npi));
            $specialty="";
            if($pdata!=null) {
                $json = $pdata->getData();
                $datos=json_decode($json,true);
                $taxonomies=$datos['results'][0]['taxonomies'];
                foreach ($taxonomies as $taxonomy){
                    if($taxonomy['primary']==1){
                        $specialty=$taxonomy['desc'];
                    }
                }
            }

            $cell_A='A'.$cont;$cell_C='C'.$cont;$cell_D='D'.$cont;$cell_E='E'.$cont;$cell_F='F'.$cont;
            $cell_I='I'.$cont;$cell_J='J'.$cont;$cell_K='K'.$cont;$cell_L='L'.$cont;$cell_N='N'.$cont;$cell_O='O'.$cont;$cell_P='P'.$cont;
            $cell_Q='Q'.$cont;$cell_V='V'.$cont;$cell_W='W'.$cont;$cell_X='X'.$cont;$cell_Y='Y'.$cont;$cell_Z='Z'.$cont;$cell_AA='AA'.$cont;$cell_AZ='AZ'.$cont;
            $cell_BB='BB'.$cont;$cell_BC='BC'.$cont;$cell_BV='BV'.$cont;$cell_BW='BW'.$cont;$cell_BX='BX'.$cont;$cell_BY='BY'.$cont;$cell_BZ='BZ'.$cont;$cell_AH='AH'.$cont;
            $cell_AW='AW'.$cont;$cell_AX='AX'.$cont;$cell_M='M'.$cont;

            if(in_array($provider->getNpiNumber(), $existNPIArray)){
                $range="A".$cont.":"."CQ".$cont;
                $spreadsheet->getActiveSheet(1)->getStyle($range)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('d457cd');
            }

            $organization=$pro->getOrganization();
            $billing_npi=$organization->getGroupNpi();

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, 'New');
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $company_name);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $date);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, 'PAR');
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $provider->getNpiNumber());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $provider->getLastName());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $provider->getInitial());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $provider->getFirstName());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $specialty);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, "Yes");
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, "No");

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Q, "Yes");
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_V, $providerV->getDegrees());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_W, $provider->getOrganization()->getName());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $billing_npi);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AW, $provider->getHopitalPrivilegesType());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AX, $provider->getHopitalPrivileges());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $this->ProviderEmail($provider));

            //get TIN or Social
            $tins=$providerV->getTinNumbers();

            if($tins==""){
                $private_key=$this->getParameter('private_key');
                $encoder=new My_Mcript($private_key);
                if($provider){
                    $tins=$encoder->decryptthis($provider->getSocial());
                }
            }

            if($tins==0)
                $tins="";

            $boardCertified="No";
            if($provider->getBoardCertified()==1){
                $boardCertified="Yes";
            }

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $tins);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z, $specialty);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AA, $providerV->getOrganizationsstr());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AZ, "No");
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BB, "Yes");
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BC, "85%");
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BV, $company_name);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BW, $company_tin);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BX, $company_address);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BY, $boardCertified);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BZ, $provider->getBoardCertifiedSpecialty());

            $cell_CA='CA'.$cont;
            $cell_CB='CB'.$cont;

            $HospitalBase="No";

            $organization=$provider->getOrganization();
            $OrgatizationClasification=$organization->getOrgSpecialties();
            if($OrgatizationClasification){
                foreach ($OrgatizationClasification as $orgClas){
                    if($orgClas->getName()=="General Hospital" or $orgClas->getName()=="Hospital"){
                        $HospitalBase="Yes";
                    }
                }
            }

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CA, $HospitalBase);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CB, $providerV->getLanguagesStr());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AH, $this->getFaxNumber($provider));

            //get address for provider
            $prov_addrs=$provider->getBillingAddress();


            $billing_street="";
            $billing_city="";
            $billing_suite="";
            $billing_zipcode="";
            $billing_state="";
            $billing_phone="";
            $billing_fax="";

            $cont_billing=0;
            if($prov_addrs){
                $contTotalAddrs=count($prov_addrs);
                foreach ($prov_addrs as $prov_addr){
                    $address=$em->getRepository('App\Entity\BillingAddress')->find($prov_addr);
                    if($address->getBillingAddr()==true or $contTotalAddrs==1){
                        $billing_street= $address->getStreet();
                        $billing_city=$address->getCity();
                        $billing_suite=$address->getSuiteNumber();
                        $billing_zipcode=$address->getZipCode();
                        $billing_state=$address->getUsState();
                        $billing_phone=$address->getPhoneNumber();
                        $billing_fax=$this->AddressFax($address);

                        $cont_billing++;
                    }
                }
            }

            if($cont_billing==0){
                if($prov_addrs){
                    foreach ($prov_addrs as $prov_addr){
                        $address=$em->getRepository('App\Entity\BillingAddress')->find($prov_addr);
                        if($address->getPrimaryAddr()==true){
                            $billing_street= $address->getStreet();
                            $billing_city=$address->getCity();
                            $billing_suite=$address->getSuiteNumber();
                            $billing_zipcode=$address->getZipCode();
                            $billing_state=$address->getUsState();
                            $billing_phone=$address->getPhoneNumber();
                            $billing_fax=$this->AddressFax($address);
                        }else{
                            $billing_street= $address->getStreet();
                            $billing_city=$address->getCity();
                            $billing_suite=$address->getSuiteNumber();
                            $billing_zipcode=$address->getZipCode();
                            $billing_state=$address->getUsState();
                            $billing_phone=$address->getPhoneNumber();
                            $billing_fax=$this->AddressFax($address);
                        }
                    }
                }
            }

            if($prov_addrs){
                $contTotalAddrs=count($prov_addrs);
                foreach ($prov_addrs as $prov_addr){
                    $cell_AB='AB'.$cont;
                    $cell_AC='AC'.$cont;
                    $cell_AD='AD'.$cont;
                    $cell_AE='AE'.$cont;
                    $cell_AF='AF'.$cont;
                    $cell_AG='AG'.$cont;

                    $cell_AI='AI'.$cont;
                    $cell_AJ='AJ'.$cont;
                    $cell_AK='AK'.$cont;
                    $cell_AL='AL'.$cont;
                    $cell_AM='AM'.$cont;
                    $cell_AN='AN'.$cont;
                    $cell_AO='AO'.$cont;
                    $cell_AH='AH'.$cont;

                    $address=$em->getRepository('App\Entity\BillingAddress')->find($prov_addr);

                    $cell_AP='AP'.$cont;$cell_AQ='AQ'.$cont;$cell_AR='AR'.$cont;$cell_AS='AS'.$cont;$cell_AT='AT'.$cont;
                    $cell_AU='AU'.$cont;$cell_AV='AV'.$cont;

                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $billing_street);
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $billing_city);
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $billing_suite);
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $billing_zipcode);
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $billing_state);
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $billing_phone);
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $billing_fax);

                    $contMailing=0;

                    if($address->getMailingAddr()==1 and $address->getPrimaryAddr()==0){
                        $contMailing++;
                    }

                    if($contTotalAddrs==1){
                        $contMailing=0;
                    }

                    if($contMailing==0 and $address and $address->getIsFacility()==0 and ($address->getRegion()==9 or $address->getRegion()==10 or $address->getRegion()==11)){

                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AB, $address->getStreet());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AC, $address->getSuiteNumber());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AD, $address->getCity());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AE, $address->getUsState());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AF, $address->getZipCode());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AG, $address->getPhoneNumber());

                        $cell_BA='BA'.$cont;

                        $serviceSettings=$address->getServiceSettings();
                        $serviceSettingStr="";

                        if(count($serviceSettings)>0){
                            foreach ($serviceSettings as $serviceSetting){
                                $serviceSettingStr.=$serviceSetting->getName().",";
                            }
                            $serviceSettingStr=substr($serviceSettingStr,0,-1);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, $serviceSettingStr);
                        }else{
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, "");
                        }

                        $bussinesOurs=$address->getBusinessHours();
                        $bussinesOurs=substr($bussinesOurs,1);
                        $bussinesOurs=substr($bussinesOurs,0,-1);

                        $bussinesOurs=str_replace('{','',$bussinesOurs);
                        $bussinesOurs=str_replace('}','',$bussinesOurs);
                        $bussinesOurs=str_replace('"','',$bussinesOurs);

                        $daysHours = explode(",", $bussinesOurs);

                        $daysHours[0];
                        $daysHours[3];
                        $daysHours[6];
                        $daysHours[9];
                        $daysHours[12];
                        $daysHours[15];

                        //sunday
                        $dayActive=explode(":",$daysHours[18])[1];
                        $dayStart=explode(":",$daysHours[19]);
                        $dayTill=explode(":",$daysHours[20]);

                        if($dayActive=="false"){
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI,'');
                        }else{
                            $dateStart=$dayStart[1].$dayStart[2];
                            $date24Start=$this->convertTo24Hours($dateStart);
                            $dateTill=$dayTill[1].$dayTill[2];
                            $date24Till=$this->convertTo24Hours($dateTill);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI,$date24Start."-".$date24Till);
                        }

                        //monday
                        $dayActive=explode(":",$daysHours[0])[1];
                        $dayStart=explode(":",$daysHours[1]);
                        $dayTill=explode(":",$daysHours[2]);

                        if($dayActive=="false"){
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ,'');
                        }else{
                            $dateStart=$dayStart[1].$dayStart[2];
                            $date24Start=$this->convertTo24Hours($dateStart);
                            $dateTill=$dayTill[1].$dayTill[2];
                            $date24Till=$this->convertTo24Hours($dateTill);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ,$date24Start."-".$date24Till);
                        }

                        //tuesday
                        $dayActive=explode(":",$daysHours[3])[1];
                        $dayStart=explode(":",$daysHours[4]);
                        $dayTill=explode(":",$daysHours[5]);

                        if($dayActive=="false"){
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AK,'');
                        }else{
                            $dateStart=$dayStart[1].$dayStart[2];
                            $date24Start=$this->convertTo24Hours($dateStart);
                            $dateTill=$dayTill[1].$dayTill[2];
                            $date24Till=$this->convertTo24Hours($dateTill);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AK,$date24Start."-".$date24Till);
                        }

                        //wednesday
                        $dayActive=explode(":",$daysHours[6])[1];
                        $dayStart=explode(":",$daysHours[7]);
                        $dayTill=explode(":",$daysHours[8]);

                        if($dayActive=="false"){
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AL,'');
                        }else{
                            $dateStart=$dayStart[1].$dayStart[2];
                            $date24Start=$this->convertTo24Hours($dateStart);
                            $dateTill=$dayTill[1].$dayTill[2];
                            $date24Till=$this->convertTo24Hours($dateTill);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AL,$date24Start."-".$date24Till);
                        }

                        //thuerday
                        $dayActive=explode(":",$daysHours[9])[1];
                        $dayStart=explode(":",$daysHours[10]);
                        $dayTill=explode(":",$daysHours[11]);

                        if($dayActive=="false"){
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AM,'');
                        }else{
                            $dateStart=$dayStart[1].$dayStart[2];
                            $date24Start=$this->convertTo24Hours($dateStart);
                            $dateTill=$dayTill[1].$dayTill[2];
                            $date24Till=$this->convertTo24Hours($dateTill);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AM,$date24Start."-".$date24Till);
                        }

                        //friday
                        $dayActive=explode(":",$daysHours[12])[1];
                        $dayStart=explode(":",$daysHours[13]);
                        $dayTill=explode(":",$daysHours[14]);

                        if($dayActive=="false"){
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN,'');
                        }else{
                            $dateStart=$dayStart[1].$dayStart[2];
                            $date24Start=$this->convertTo24Hours($dateStart);
                            $dateTill=$dayTill[1].$dayTill[2];
                            $date24Till=$this->convertTo24Hours($dateTill);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN,$date24Start."-".$date24Till);
                        }

                        //saturday
                        $dayActive=explode(":",$daysHours[15])[1];
                        $dayStart=explode(":",$daysHours[16]);
                        $dayTill=explode(":",$daysHours[17]);

                        if($dayActive=="false"){
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO,'');
                        }else{
                            $dateStart=$dayStart[1].$dayStart[2];
                            $date24Start=$this->convertTo24Hours($dateStart);
                            $dateTill=$dayTill[1].$dayTill[2];
                            $date24Till=$this->convertTo24Hours($dateTill);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO,$date24Start."-".$date24Till);
                        }
                        $cont++;

                        $cell_A='A'.$cont;$cell_C='C'.$cont;$cell_D='D'.$cont;$cell_E='E'.$cont;$cell_F='F'.$cont;
                        $cell_I='I'.$cont;$cell_J='J'.$cont;$cell_K='K'.$cont;$cell_L='L'.$cont;$cell_N='N'.$cont;$cell_O='O'.$cont;$cell_P='P'.$cont;
                        $cell_Q='Q'.$cont;$cell_V='V'.$cont;$cell_W='W'.$cont;$cell_X='X'.$cont;$cell_Y='Y'.$cont;$cell_Z='Z'.$cont;$cell_AA='AA'.$cont;$cell_AZ='AZ'.$cont;
                        $cell_BB='BB'.$cont;$cell_BC='BC'.$cont;$cell_BV='BV'.$cont;$cell_BW='BW'.$cont;$cell_BX='BX'.$cont;$cell_BY='BY'.$cont;$cell_BZ='BZ'.$cont;
                        $cell_CA='CA'.$cont;$cell_AW='AW'.$cont;$cell_AX='AX'.$cont;
                        $cell_CB='CB'.$cont; $cell_M='M'.$cont;

                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, 'New');
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $company_name);
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $date);
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, 'PAR');
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $provider->getNpiNumber());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $provider->getLastName());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $provider->getInitial());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $provider->getFirstName());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $specialty);
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, "Yes");
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, "No");
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_P, "Yes");
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_Q, "Yes");
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_V, $providerV->getDegrees());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_W, $providerV->getOrganizationsstr());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $billing_npi);
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AW, $provider->getHopitalPrivilegesType());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AX, $provider->getHopitalPrivileges());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $this->ProviderEmail($provider));

                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $tins);
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z, $specialty);
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AA, $providerV->getOrganizationsstr());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AZ, "No");
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_BB, "Yes");
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_BC, "85%");
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_BV, $company_name);
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_BW, $company_tin);
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_BX, $company_address);
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_BY, $boardCertified);
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_BZ, $provider->getBoardCertifiedSpecialty());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_CA, $HospitalBase);
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_CB, $providerV->getLanguagesStr());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_CB, $providerV->getLanguagesStr());

                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AH, $this->getFaxNumber($provider));
                    }

                }
            }
        }

        // Redirect output to a client’s web browser (XlS)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="mmm-report-simple.xls"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/mmm/report_facility", name="mmm_report_facility")
    */
    public function facility(){
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');

        $spreadsheet = $reader->load($url.'template-mmm.xlsx');

        $em=$this->getDoctrine()->getManager();
        $organizations=$em->getRepository('App\Entity\Organization')->findAll();

        $addressResult=array();


        foreach ($organizations as $organization) {
            $address = $this->OrganizationOnMMM($organization);
            if($address!=null){
                foreach ($address as $addr) {
                    $addressResult[] = $addr;
                }
            }
        }

        $date=date('m/d/Y');
        $cont=2;

        $company=$em->getRepository('App\Entity\Company')->find(1);
        $company_tin=$company->getTinNumber();
        $company_address=$company->getAddress();
        $company_name=$company->getName();

        foreach ($addressResult as $address){

            $cell_A='A'.$cont;$cell_C='C'.$cont;$cell_D='D'.$cont;$cell_E='E'.$cont;$cell_F='F'.$cont;$cell_J='J'.$cont;$cell_K='K'.$cont;
            $cell_I='I'.$cont;$cell_L='L'.$cont;$cell_N='N'.$cont;$cell_O='O'.$cont;$cell_P='P'.$cont;
            $cell_Q='Q'.$cont;$cell_V='V'.$cont;$cell_W='W'.$cont;$cell_X='X'.$cont;$cell_Y='Y'.$cont;$cell_Z='Z'.$cont;$cell_AA='AA'.$cont;$cell_AZ='AZ'.$cont;
            $cell_BB='BB'.$cont;$cell_BC='BC'.$cont;$cell_BV='BV'.$cont;$cell_BW='BW'.$cont;$cell_BX='BX'.$cont;$cell_BY='BY'.$cont;$cell_CB='CB'.$cont;$cell_AH='AH'.$cont;
            $cell_CQ='CQ'.$cont;$cell_M='M'.$cont;

            $existNPIArray=array(1053347955, 1053504845, 1053504845, 1093910499, 1144266206, 1184821928, 1184821928, 1245249218, 1275522013, 1295978260,
                1326128323, 1326128323, 1386632883, 1528151404, 1598103491, 1780726836, 1811177249, 1831281237, 1881732857, 1922096437, 1962510073, 1962510073,
                1962510073, 1962510073, 1700270113, 1063417483, 1649340415, 1366514705, 1518966126, 1538102397, 1528099561, 1275800104, 1881933703, 1316992019,
                1205883493, 1164769998, 1164769998, 1164769998, 1174707863, 1558409789, 1558409789, 1558409789, 1063600633, 1225105778, 1306148754, 1316116122,
                1336122464, 1346581758, 1588620470, 1497707574, 1558419572, 1568583979, 1962554337, 1972733442, 1114915717, 1578514865, 1144614660, 1689618647,
                1245487388, 1780670539, 1255400602, 1972979631, 1972979631, 1972979631, 1972979631, 1972979631, 1972979631, 1205219003, 1275897886, 1902310295, 1841573409, 1225248628,
                1619966538, 1477627602, 1639629694, 1932197548, 1376822981, 1356338784, 1295946002, 1700380649, 1700380649, 1700380649, 1710394499, 1215204433,
                1669872248, 1548327828, 1548327828, 1518964162, 1134117658, 1144653163, 1144653163, 1821086323, 1326438227, 1811048788, 1699763789,
                1184799546, 1154443380, 1437341914, 1609063544, 1609063544, 1609063544, 1225050107, 1427114255, 1679689699, 1679689699, 1881661684, 1881661684,
                1104083773, 1104185024, 1295895589, 1518182443, 1598961476, 1083888432, 1750403127, 1538408513, 1558552844, 1144384124, 1346238805, 1306944855, 1356774061,
                1962640003, 1144252982, 1174957914, 1255538641, 1235347956, 1821138819, 1821138819, 1821138819, 1821138819, 1003048422, 1972556124, 1659542462, 1720026628, 1598833832, 1861663973, 1952360620,
                1952524258, 1578790887, 1033578109, 1568614972, 1073960464, 1427055417, 1104250356, 1184830044, 1275758476, 1437460672, 1477615193, 1487864187, 1568810844,
                1679080501, 1689070922, 1730164914, 1821024704, 1366528705, 1861680936, 1104908508, 1962533828, 1942395850, 1447364070, 1184739567, 1184739567, 1902811490, 1942350236,
                1942350236, 1942350236, 1942350236, 1710321443, 1598846008, 1053759050, 1043625999, 1497014765, 1336248202, 1154426955, 1649336660, 1821341686, 1477747236, 1295067817,
                1861458226, 1598207508, 1881737732, 1265824833, 1174027023, 1528098050, 1275969099, 1366839607, 1942691720, 1942691720, 1326460080, 1912014804,
                1912014804, 1912014804, 1386095198, 1386946374, 1477656098, 1225143472, 1275045296, 1275045296, 1275886236, 1033651963, 1114154481, 1487799136, 1083871784,
                1083871784, 1295111623, 1295825263, 1134210271, 1326323718, 1306989454, 1003331646, 1548467095, 1568717296, 1679982375, 1720143910, 1780071274, 1780071274,
                1780071274, 1811985708, 1831270958, 1871552570, 1871765768, 1902944085, 1902967821, 1871741991, 1750450094, 1578654497, 1659842151, 1740752484, 1982162822,
                1316077514, 1336795509, 1346406840, 1417358078, 1548291834, 1649224320, 1568490506, 1598846008, 1225248628
            );

            if(in_array($address->getOrganization()->getGroupNpi(), $existNPIArray)){
                $range="A".$cont.":"."CQ".$cont;
                $spreadsheet->getActiveSheet(1)->getStyle($range)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('d457cd');
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CQ, 'Yes');
            }else{
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CQ, 'No');
            }

            $locationNpi="";
            if($address->getLocationNpi()!="" and $address->getLocationNpi()!=null){
                $locationNpi=$address->getLocationNpi();
            }else{
                $locationNpi=$address->getOrganization()->getGroupNpi();
            }

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, 'New');
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $company_name);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $date);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, 'PAR');
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $locationNpi);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $address->getOrganization()->getName());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, "Yes");
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $this->AddressEmail($address));
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, "No");
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_P, "Yes");
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Q, "Yes");

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_W, $address->getOrganization()->getName());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $address->getOrganization()->getBillingNpi());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $address->getOrganization()->getTinNumber());

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AA, $address->getOrganization()->getName());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AZ, "No");
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BB, "Yes");
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BC, "85%");


            $facilitiesTypes=$address->getFacilityType();
            $facilitiesTypesStr="";
            if($facilitiesTypes){
                foreach ($facilitiesTypes as $ft){
                    $facilitiesTypesStr.=$ft->getName().",";
                }
                $facilitiesTypesStr=substr($facilitiesTypesStr,0,-1);
            }

            $taxonomies=$address->getTaxonomyCodes();
            $taxonomySpecialties="";
            if(count($taxonomies)>0){
                  foreach ($taxonomies as $taxonomy){
                      $taxonomySpecialties.=$taxonomy->getSpecialization();
                  }
                $taxonomySpecialties=substr($taxonomySpecialties,0,-1);
            }

            if($taxonomySpecialties==""){
                $taxonomySpecialties="-";
            }

            $npi=$address->getOrganization()->getGroupNpi();
            $pdata=$em->getRepository('App\Entity\PDataMedicare')->findOneBy(array('npi_number'=>$npi));
            $taxonomySpecialties="";
            if($pdata!=null) {
                $json = $pdata->getData();
                $datos=json_decode($json,true);
                $taxonomies=$datos['results'][0]['taxonomies'];
                foreach ($taxonomies as $taxonomy){
                    if($taxonomy['primary']==1){
                        $taxonomySpecialties=$taxonomy['desc'];
                    }
                }
            }

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $taxonomySpecialties);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AH, $this->AddressFax($address));
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BV, $company_name);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BW, $company_tin);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BX, $company_address);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BY, "No");
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CB, $this->AddressLanguages($address));
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z,$taxonomySpecialties);

            $cell_AB='AB'.$cont;
            $cell_AC='AC'.$cont;
            $cell_AD='AD'.$cont;
            $cell_AE='AE'.$cont;
            $cell_AF='AF'.$cont;
            $cell_AG='AG'.$cont;

            $cell_AI='AI'.$cont;
            $cell_AJ='AJ'.$cont;
            $cell_AK='AK'.$cont;
            $cell_AL='AL'.$cont;
            $cell_AM='AM'.$cont;
            $cell_AN='AN'.$cont;
            $cell_AO='AO'.$cont;
            $cell_AP='AP'.$cont;

            $cell_AP='AP'.$cont;$cell_AQ='AQ'.$cont;$cell_AR='AR'.$cont;$cell_AS='AS'.$cont;$cell_AT='AT'.$cont;
            $cell_AU='AU'.$cont;$cell_AV='AV'.$cont;

            if($address->getBillingAddr()==true){
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
            }else{
                if($address->getMailingAddr()==true){
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                }else{
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                }
            }

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AB, $address->getStreet());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AC, $address->getSuiteNumber());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AD, $address->getCity());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AE, $address->getUsState());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AF, $address->getZipCode());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AG, $address->getPhoneNumber());

            $cell_BA='BA'.$cont;

            $serviceSettings=$address->getServiceSettings();
            $serviceSettingStr="";

            if(count($serviceSettings)>0){
                foreach ($serviceSettings as $serviceSetting){
                    $serviceSettingStr.=$serviceSetting->getName().",";
                }
                $serviceSettingStr=substr($serviceSettingStr,0,-1);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, $serviceSettingStr);
            }else{
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, "");
            }

            $bussinesOurs=$address->getBusinessHours();
            $bussinesOurs=substr($bussinesOurs,1);
            $bussinesOurs=substr($bussinesOurs,0,-1);

            $bussinesOurs=str_replace('{','',$bussinesOurs);
            $bussinesOurs=str_replace('}','',$bussinesOurs);
            $bussinesOurs=str_replace('"','',$bussinesOurs);

            $daysHours = explode(",", $bussinesOurs);

            //sunday
            $dayActive=explode(":",$daysHours[18])[1];
            $dayStart=explode(":",$daysHours[19]);
            $dayTill=explode(":",$daysHours[20]);

            if($dayActive=="false"){
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI,'');
            }else{
                $dateStart=$dayStart[1].$dayStart[2];
                $date24Start=$this->convertTo24Hours($dateStart);
                $dateTill=$dayTill[1].$dayTill[2];
                $date24Till=$this->convertTo24Hours($dateTill);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI,$date24Start."-".$date24Till);
            }

            //monday
            $dayActive=explode(":",$daysHours[0])[1];
            $dayStart=explode(":",$daysHours[1]);
            $dayTill=explode(":",$daysHours[2]);

            if($dayActive=="false"){
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ,'');
            }else{
                $dateStart=$dayStart[1].$dayStart[2];
                $date24Start=$this->convertTo24Hours($dateStart);
                $dateTill=$dayTill[1].$dayTill[2];
                $date24Till=$this->convertTo24Hours($dateTill);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ,$date24Start."-".$date24Till);
            }

            //tuesday
            $dayActive=explode(":",$daysHours[3])[1];
            $dayStart=explode(":",$daysHours[4]);
            $dayTill=explode(":",$daysHours[5]);

            if($dayActive=="false"){
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AK,'');
            }else{
                $dateStart=$dayStart[1].$dayStart[2];
                $date24Start=$this->convertTo24Hours($dateStart);
                $dateTill=$dayTill[1].$dayTill[2];
                $date24Till=$this->convertTo24Hours($dateTill);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AK,$date24Start."-".$date24Till);
            }

            //wednesday
            $dayActive=explode(":",$daysHours[6])[1];
            $dayStart=explode(":",$daysHours[7]);
            $dayTill=explode(":",$daysHours[8]);

            if($dayActive=="false"){
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AL,'');
            }else{
                $dateStart=$dayStart[1].$dayStart[2];
                $date24Start=$this->convertTo24Hours($dateStart);
                $dateTill=$dayTill[1].$dayTill[2];
                $date24Till=$this->convertTo24Hours($dateTill);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AL,$date24Start."-".$date24Till);
            }

            //thuerday
            $dayActive=explode(":",$daysHours[9])[1];
            $dayStart=explode(":",$daysHours[10]);
            $dayTill=explode(":",$daysHours[11]);

            if($dayActive=="false"){
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AM,'');
            }else{
                $dateStart=$dayStart[1].$dayStart[2];
                $date24Start=$this->convertTo24Hours($dateStart);
                $dateTill=$dayTill[1].$dayTill[2];
                $date24Till=$this->convertTo24Hours($dateTill);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AM,$date24Start."-".$date24Till);
            }

            //friday
            $dayActive=explode(":",$daysHours[12])[1];
            $dayStart=explode(":",$daysHours[13]);
            $dayTill=explode(":",$daysHours[14]);

            if($dayActive=="false"){
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN,'');
            }else{
                $dateStart=$dayStart[1].$dayStart[2];
                $date24Start=$this->convertTo24Hours($dateStart);
                $dateTill=$dayTill[1].$dayTill[2];
                $date24Till=$this->convertTo24Hours($dateTill);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN,$date24Start."-".$date24Till);
            }

            //saturday
            $dayActive=explode(":",$daysHours[15])[1];
            $dayStart=explode(":",$daysHours[16]);
            $dayTill=explode(":",$daysHours[17]);

            if($dayActive=="false"){
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO,'');
            }else{
                $dateStart=$dayStart[1].$dayStart[2];
                $date24Start=$this->convertTo24Hours($dateStart);
                $dateTill=$dayTill[1].$dayTill[2];
                $date24Till=$this->convertTo24Hours($dateTill);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO,$date24Start."-".$date24Till);
            }

            $cont++;

            //get the all providers assigned to MMM on this address
            $db = $em->getConnection();
            $addressId=$address->getId();

            $query = "SELECT  * FROM provider_billing_address WHERE  provider_billing_address.billing_address_id=$addressId;";
            $stmt = $db->prepare($query);
            $stmt->execute();

            $addreees=$stmt->fetchAll();
            $providers=array();

            if(count($addreees)>0){
                foreach ($addreees as $addr){
                    if($addr!=null){
                       $provider=$em->getRepository('App\Entity\ViewProvider')->find($addr['provider_id']);
                       $providers[]=$provider;
                    }
                }
            }
        }


        //get all organizations with facility credentialing Asign to Payer MMM
        $facilityCredentialing=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('credentialing_status'=>7));
        $organizationsFacility=array();
        $addressFacilityResult=array();
        foreach ($facilityCredentialing as $fc){
            $orgId=$fc->getBillingAddress()->getOrganization()->getId();
            if (!in_array($orgId, $organizationsFacility)) {
                $organizationsFacility[]=$orgId;
            }
        }

        foreach ($organizationsFacility as $orgF){
           $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$orgF));

           foreach ($address as $addr){
                  $exist=false;
                  foreach ($addressResult as $addrR){
                      if($addrR->getId()==$addr->getId()){
                          $exist=true;
                      }
                  }

                  if($exist==false){
                      $addressFacilityResult[]=$addr;
                  }
           }
        }

        foreach ($addressFacilityResult as $address){
            $cell_A='A'.$cont;$cell_C='C'.$cont;$cell_D='D'.$cont;$cell_E='E'.$cont;$cell_F='F'.$cont;$cell_J='J'.$cont;$cell_K='K'.$cont;
            $cell_I='I'.$cont;$cell_L='L'.$cont;$cell_N='N'.$cont;$cell_O='O'.$cont;$cell_P='P'.$cont;
            $cell_Q='Q'.$cont;$cell_V='V'.$cont;$cell_W='W'.$cont;$cell_X='X'.$cont;$cell_Y='Y'.$cont;$cell_Z='Z'.$cont;$cell_AA='AA'.$cont;$cell_AZ='AZ'.$cont;
            $cell_BB='BB'.$cont;$cell_BC='BC'.$cont;$cell_BV='BV'.$cont;$cell_BW='BW'.$cont;$cell_BX='BX'.$cont;$cell_BY='BY'.$cont;$cell_CB='CB'.$cont;$cell_AH='AH'.$cont;

            $locationNpi="";
            if($address->getLocationNpi()!="" and $address->getLocationNpi()!=null){
                $locationNpi=$address->getLocationNpi();
            }else{
                $locationNpi=$address->getOrganization()->getGroupNpi();
            }

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, 'New');
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $company_name);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $date);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, 'PAR');
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $locationNpi);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $address->getOrganization()->getName());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, "Yes");
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, "No");
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_P, "Yes");
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Q, "Yes");

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_W, $address->getOrganization()->getName());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $address->getOrganization()->getBillingNpi());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $address->getOrganization()->getTinNumber());

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AA, $address->getOrganization()->getName());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AZ, "No");
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BB, "Yes");
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BC, "85%");

            $facilitiesTypes=$address->getFacilityType();
            $facilitiesTypesStr="";
            if($facilitiesTypes){
                foreach ($facilitiesTypes as $ft){
                    $facilitiesTypesStr.=$ft->getName().",";
                }
                $facilitiesTypesStr=substr($facilitiesTypesStr,0,-1);
            }

            $npi=$address->getOrganization()->getGroupNpi();
            $pdata=$em->getRepository('App\Entity\PDataMedicare')->findOneBy(array('npi_number'=>$npi));
            $taxonomySpecialties="";
            if($pdata!=null) {
                $json = $pdata->getData();
                $datos=json_decode($json,true);
                $taxonomies=$datos['results'][0]['taxonomies'];
                foreach ($taxonomies as $taxonomy){
                    if($taxonomy['primary']==1){
                        $taxonomySpecialties=$taxonomy['desc'];
                    }
                }
            }

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $taxonomySpecialties);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AH, $this->AddressFax($address));
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BV, $company_name);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BW, $company_tin);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BX, $company_address);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BY, "No");
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CB, $this->AddressLanguages($address));
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z,$taxonomySpecialties);

            $cell_AB='AB'.$cont;
            $cell_AC='AC'.$cont;
            $cell_AD='AD'.$cont;
            $cell_AE='AE'.$cont;
            $cell_AF='AF'.$cont;
            $cell_AG='AG'.$cont;

            $cell_AI='AI'.$cont;
            $cell_AJ='AJ'.$cont;
            $cell_AK='AK'.$cont;
            $cell_AL='AL'.$cont;
            $cell_AM='AM'.$cont;
            $cell_AN='AN'.$cont;
            $cell_AO='AO'.$cont;

            $cell_AP='AP'.$cont;$cell_AQ='AQ'.$cont;$cell_AR='AR'.$cont;$cell_AS='AS'.$cont;$cell_AT='AT'.$cont;
            $cell_AU='AU'.$cont;$cell_AV='AV'.$cont;
            if($address->getBillingAddr()==true){
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                if($address->getBillingAddr()==true){
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                }else{
                    if($address->getMailingAddr()==true){
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                    }else{
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                    }
                }
            }

            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AB, $address->getStreet());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AC, $address->getSuiteNumber());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AD, $address->getCity());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AE, $address->getUsState());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AF, $address->getZipCode());
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AG, $address->getPhoneNumber());

            $cell_BA='BA'.$cont;

            $serviceSettings=$address->getServiceSettings();
            $serviceSettingStr="";

            if(count($serviceSettings)>0){
                foreach ($serviceSettings as $serviceSetting){
                    $serviceSettingStr.=$serviceSetting->getName().",";
                }
                $serviceSettingStr=substr($serviceSettingStr,0,-1);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, $serviceSettingStr);
            }else{
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, "");
            }

            $bussinesOurs=$address->getBusinessHours();

            $bussinesOurs=substr($bussinesOurs,1);
            $bussinesOurs=substr($bussinesOurs,0,-1);

            $bussinesOurs=str_replace('{','',$bussinesOurs);
            $bussinesOurs=str_replace('}','',$bussinesOurs);
            $bussinesOurs=str_replace('"','',$bussinesOurs);

            $daysHours = explode(",", $bussinesOurs);

            //sunday
            $dayActive=explode(":",$daysHours[18])[1];
            $dayStart=explode(":",$daysHours[19]);
            $dayTill=explode(":",$daysHours[20]);

            if($dayActive=="false"){
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI,'');
            }else{
                $dateStart=$dayStart[1].$dayStart[2];
                $date24Start=$this->convertTo24Hours($dateStart);
                $dateTill=$dayTill[1].$dayTill[2];
                $date24Till=$this->convertTo24Hours($dateTill);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI,$date24Start."-".$date24Till);
            }

            //monday
            $dayActive=explode(":",$daysHours[0])[1];
            $dayStart=explode(":",$daysHours[1]);
            $dayTill=explode(":",$daysHours[2]);

            if($dayActive=="false"){
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ,'');
            }else{
                $dateStart=$dayStart[1].$dayStart[2];
                $date24Start=$this->convertTo24Hours($dateStart);
                $dateTill=$dayTill[1].$dayTill[2];
                $date24Till=$this->convertTo24Hours($dateTill);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ,$date24Start."-".$date24Till);
            }

            //tuesday
            $dayActive=explode(":",$daysHours[3])[1];
            $dayStart=explode(":",$daysHours[4]);
            $dayTill=explode(":",$daysHours[5]);

            if($dayActive=="false"){
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AK,'');
            }else{
                $dateStart=$dayStart[1].$dayStart[2];
                $date24Start=$this->convertTo24Hours($dateStart);
                $dateTill=$dayTill[1].$dayTill[2];
                $date24Till=$this->convertTo24Hours($dateTill);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AK,$date24Start."-".$date24Till);
            }

            //wednesday
            $dayActive=explode(":",$daysHours[6])[1];
            $dayStart=explode(":",$daysHours[7]);
            $dayTill=explode(":",$daysHours[8]);

            if($dayActive=="false"){
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AL,'');
            }else{
                $dateStart=$dayStart[1].$dayStart[2];
                $date24Start=$this->convertTo24Hours($dateStart);
                $dateTill=$dayTill[1].$dayTill[2];
                $date24Till=$this->convertTo24Hours($dateTill);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AL,$date24Start."-".$date24Till);
            }

            //thuerday
            $dayActive=explode(":",$daysHours[9])[1];
            $dayStart=explode(":",$daysHours[10]);
            $dayTill=explode(":",$daysHours[11]);

            if($dayActive=="false"){
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AM,'');
            }else{
                $dateStart=$dayStart[1].$dayStart[2];
                $date24Start=$this->convertTo24Hours($dateStart);
                $dateTill=$dayTill[1].$dayTill[2];
                $date24Till=$this->convertTo24Hours($dateTill);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AM,$date24Start."-".$date24Till);
            }

            //friday
            $dayActive=explode(":",$daysHours[12])[1];
            $dayStart=explode(":",$daysHours[13]);
            $dayTill=explode(":",$daysHours[14]);

            if($dayActive=="false"){
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN,'');
            }else{
                $dateStart=$dayStart[1].$dayStart[2];
                $date24Start=$this->convertTo24Hours($dateStart);
                $dateTill=$dayTill[1].$dayTill[2];
                $date24Till=$this->convertTo24Hours($dateTill);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN,$date24Start."-".$date24Till);
            }

            //saturday
            $dayActive=explode(":",$daysHours[15])[1];
            $dayStart=explode(":",$daysHours[16]);
            $dayTill=explode(":",$daysHours[17]);

            if($dayActive=="false"){
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO,'');
            }else{
                $dateStart=$dayStart[1].$dayStart[2];
                $date24Start=$this->convertTo24Hours($dateStart);
                $dateTill=$dayTill[1].$dayTill[2];
                $date24Till=$this->convertTo24Hours($dateTill);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO,$date24Start."-".$date24Till);
            }

            //get the all providers assigned to MMM on this address
            $db = $em->getConnection();
            $addressId=$address->getId();

            $query = "SELECT  * FROM provider_billing_address WHERE  provider_billing_address.billing_address_id=$addressId;";
            $stmt = $db->prepare($query);
            $stmt->execute();

            $addreees=$stmt->fetchAll();
            $providers=array();

            if(count($addreees)>0){
                foreach ($addreees as $addr){
                    if($addr!=null){
                        $provider=$em->getRepository('App\Entity\ViewProvider')->find($addr['provider_id']);
                        $providers[]=$provider;
                    }
                }
            }

            $cont++;
        }


        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="mmm-report-facility.xls"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');
        // Return a text response to the browser saying that the excel was succesfully created

        return new Response("Report Export Succefully");

    }

    private function OrganizationOnMMM($organization){
        $em=$this->getDoctrine()->getManager();
        $addressResult=array();
        $contAssignedMMM=0;
        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));

        //verify if organization have a credentialing
        $isMMMAsigned=false;

        $payers=$organization->getPayers();
        if($payers!=null){
            foreach ($payers as $payer){
                if($payer->getId()==1){
                    $isMMMAsigned=true;
                }
            }
        }

        if($isMMMAsigned==true){
            foreach ($providers as $provider){
                if($provider->getIsFacility()==true){
                    $payers=$provider->getPayers();
                    if($payers!=null){
                        foreach ($payers as $payer){
                            if($payer->getId()==1){
                                $contAssignedMMM++;
                            }
                        }
                    }
                }
            }

            if($contAssignedMMM>0){
                $addresses=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                if(count($addresses)>0) {
                    foreach ($addresses as $address) {
                        $addressResult[]=$address;
                    }
                }
            }

            return $addressResult;
        }

        return false;
    }

    private function convertTo24Hours($date){
        $newDate="";
        if (strpos($date, 'am') == true) {
            $newDate=substr($date,0,-2);

            if(strlen($newDate)<4){
                $newDate='0'.$newDate;
            }

        }else{
            $newDate=substr($date,0,-2);
            if($newDate=="1200"){$newDate="1200";}if($newDate=="1230"){$newDate="1230";}
            if($newDate=="100"){$newDate="1300";}if($newDate=="130"){$newDate="1330";}
            if($newDate=="200"){$newDate="1400";}if($newDate=="230"){$newDate="1430";}
            if($newDate=="300"){$newDate="1500";}if($newDate=="330"){$newDate="1530";}
            if($newDate=="400"){$newDate="1600";}if($newDate=="430"){$newDate="1630";}
            if($newDate=="500"){$newDate="1700";}if($newDate=="530"){$newDate="1730";}
            if($newDate=="600"){$newDate="1800";}if($newDate=="630"){$newDate="1830";}
            if($newDate=="700"){$newDate="1900";}if($newDate=="730"){$newDate="1930";}
            if($newDate=="800"){$newDate="2000";}if($newDate=="830"){$newDate="2030";}
            if($newDate=="900"){$newDate="2100";}if($newDate=="930"){$newDate="2130";}
            if($newDate=="1000"){$newDate="2200";}if($newDate=="1030"){$newDate="2230";}
            if($newDate=="1100"){$newDate="2300";}if($newDate=="1130"){$newDate="2330";}
        }

        return $newDate;
    }

    private function getFacilityNpi($provider){
        $npi="";
        $addrs=$provider->getBillingAddress();

        if(count($addrs)>0){
            foreach ($addrs as $addr){
                if($addr->getIsFacility()==1 and $addr->getLocationNpi()!="" and $addr->getLocationNpi()!=null){
                    $npi.=$addr->getLocationNpi().',';
                }
            }
            $npi=substr($npi,0,-1);
        }else{
            $npi="";
        }

        return $npi;
    }

    private function  getFacilityTypes($provider){
        $facilities="";

        $addrs=$provider->getBillingAddress();

        if(count($addrs)>0){
            foreach ($addrs as $addr){
                if($addr->getIsFacility()==1){
                    $ft=$addr->getFacilityType();
                    if($ft){
                        foreach ($ft as $facilityType){
                            $facilities.=$facilityType->getName().",";
                        }
                    }
                }
            }
            $facilities=substr($facilities,0,-1);
        }else{
            $facilities="";
        }

        return $facilities;
    }

    private function getNpiGroup($provider){
        $organization=$provider->getOrganization();
        $npiGroup=$organization->getGroupNpi();
        return $npiGroup;
    }

    private function getTinOrganizationNumber($provider){
        $tin="";
        $organizations=$provider->getOrganizations();
        if(count($organizations)>0){
            foreach ($organizations as $organization){
                $tin.=$organization->getTinNumber().",";
            }
            $tin=substr($tin,0,-1);
        }

        return $tin;
    }

    private  function getFaxNumber($provider){
        $em=$this->getDoctrine()->getManager();
        $fax="";

        $organization=$provider->getOrganization();
        $fax=$organization->getFaxno();

        if($fax==""){
            $faxs=array();
            $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));
            if($contacts){
                foreach ($contacts as $contact){
                    if($contact->getFax()!=""){
                        $faxs[]=$fax;
                    }
                }
            }
            if(count($faxs)>0){
                $fax=$faxs[0];
            }
        }

        return $fax;
    }

    private function AddressLanguages($address){
        $em=$this->getDoctrine()->getManager();
        $organization=$address->getOrganization();
        $viewOrganization=$em->getRepository('App\Entity\ViewOrganization')->find($organization->getId());
        $languagesStr=$viewOrganization->getLanguages();
        return $languagesStr;
    }

    private function ProviderEmail($provider){
        $em=$this->getDoctrine()->getManager();
        $email="";

        $organization=$provider->getOrganization();

            $emails=array();
            $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));
            if($contacts){
                foreach ($contacts as $contact){
                    if($contact->getEmail()!=""){
                        $emails[]=$contact->getEmail();
                    }
                }
            }
            if(count($emails)>0){
                $email=$emails[0];
            }

        return $email;
    }

    private function AddressFax($address){
        $em=$this->getDoctrine()->getManager();
        $fax="";

        $organization=$address->getOrganization();
        $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));
        $faxs=array();
        if($contacts){
            foreach ($contacts as $contact){
                if($contact->getFax()!=""){
                    $faxs[]=$contact->getFax();
                }
            }
        }
        if(count($faxs)>0){
            $fax=$faxs[0];
        }

        return $fax;
    }


    private function OrganizationEmail($organization){
        $em=$this->getDoctrine()->getManager();
        $email="";

        $emails=array();
        $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));
        if($contacts){
            foreach ($contacts as $contact){
                if($contact->getEmail()!=""){
                    $emails[]=$contact->getEmail();
                }
            }
        }
        if(count($emails)>0){
            $email=$emails[0];
        }

        return $email;
    }

    private function AddressEmail($address){
        $em=$this->getDoctrine()->getManager();
        $email="";

        $organization=$address->getOrganization();
        $emails=array();
        $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));
        if($contacts){
            foreach ($contacts as $contact){
                if($contact->getEmail()!=""){
                    $emails[]=$contact->getEmail();
                }
            }
        }
        if(count($emails)>0){
            $email=$emails[0];
        }

        return $email;
    }

    /**
     * @Route("/mmm/report_facility_2", name="mmm_report_facility_2")
     */
    public function facility2(){
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');

        $spreadsheet = $reader->load($url.'template-mmm.xlsx');
        $em=$this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App\Entity\Organization')->findAll();
        $cont=2;
        $date=date('m/d/Y');

        $existNPIArray=array(1053347955, 1053504845, 1053504845, 1093910499, 1144266206, 1184821928, 1184821928, 1245249218, 1275522013, 1295978260,
            1326128323, 1326128323, 1386632883, 1528151404, 1598103491, 1780726836, 1811177249, 1831281237, 1881732857, 1922096437, 1962510073, 1962510073,
            1962510073, 1962510073, 1700270113, 1063417483, 1649340415, 1366514705, 1518966126, 1538102397, 1528099561, 1275800104, 1881933703, 1316992019,
            1205883493, 1164769998, 1164769998, 1164769998, 1174707863, 1558409789, 1558409789, 1558409789, 1063600633, 1225105778, 1306148754, 1316116122,
            1336122464, 1346581758, 1588620470, 1497707574, 1558419572, 1568583979, 1962554337, 1972733442, 1114915717, 1578514865, 1144614660, 1689618647,
            1245487388, 1780670539, 1255400602, 1972979631, 1972979631, 1972979631, 1972979631, 1972979631, 1972979631, 1205219003, 1275897886, 1902310295, 1841573409, 1225248628,
            1619966538, 1477627602, 1639629694, 1932197548, 1376822981, 1356338784, 1295946002, 1700380649, 1700380649, 1700380649, 1710394499, 1215204433,
            1669872248, 1548327828, 1548327828, 1518964162, 1134117658, 1144653163, 1144653163, 1821086323, 1326438227, 1811048788, 1699763789,
            1184799546, 1154443380, 1437341914, 1609063544, 1609063544, 1609063544, 1225050107, 1427114255, 1679689699, 1679689699, 1881661684, 1881661684,
            1104083773, 1104185024, 1295895589, 1518182443, 1598961476, 1083888432, 1750403127, 1538408513, 1558552844, 1144384124, 1346238805, 1306944855, 1356774061,
            1962640003, 1144252982, 1174957914, 1255538641, 1235347956, 1821138819, 1821138819, 1821138819, 1821138819, 1003048422, 1972556124, 1659542462, 1720026628, 1598833832, 1861663973, 1952360620,
            1952524258, 1578790887, 1033578109, 1568614972, 1073960464, 1427055417, 1104250356, 1184830044, 1275758476, 1437460672, 1477615193, 1487864187, 1568810844,
            1679080501, 1689070922, 1730164914, 1821024704, 1366528705, 1861680936, 1104908508, 1962533828, 1942395850, 1447364070, 1184739567, 1184739567, 1902811490, 1942350236,
            1942350236, 1942350236, 1942350236, 1710321443, 1598846008, 1053759050, 1043625999, 1497014765, 1336248202, 1154426955, 1649336660, 1821341686, 1477747236, 1295067817,
            1861458226, 1598207508, 1881737732, 1265824833, 1174027023, 1528098050, 1275969099, 1366839607, 1942691720, 1942691720, 1326460080, 1912014804,
            1912014804, 1912014804, 1386095198, 1386946374, 1477656098, 1225143472, 1275045296, 1275045296, 1275886236, 1033651963, 1114154481, 1487799136, 1083871784,
            1083871784, 1295111623, 1295825263, 1134210271, 1326323718, 1306989454, 1003331646, 1548467095, 1568717296, 1679982375, 1720143910, 1780071274, 1780071274,
            1780071274, 1811985708, 1831270958, 1871552570, 1871765768, 1902944085, 1902967821, 1871741991, 1750450094, 1578654497, 1659842151, 1740752484, 1982162822,
            1316077514, 1336795509, 1346406840, 1417358078, 1548291834, 1649224320, 1568490506, 1598846008, 1225248628
        );

        foreach ($organizations as $organization) {

            $company=$em->getRepository('App\Entity\Company')->find(1);
            $company_tin=$company->getTinNumber();
            $company_address=$company->getAddress();
            $company_name=$company->getName();

            $org_have_mmm=false;
            $org_payers=$organization->getPayers();
            if($org_payers!=null){
                foreach ($org_payers as $orgP){
                    if($orgP->getId()==1){
                        $org_have_mmm=true;
                    }
                }
            }

            $addresses = $em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
            $contF=0;

            if($addresses!=null or $org_have_mmm==true){
                foreach ($addresses as $addr) {
                    if($addr->getIsFacility()==true){
                        $contF++;
                    }
                }
                if($contF>0){
                    $providers = $em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId()));
                    $contAssignedMMM=0;

                    if($providers!=null){
                        foreach ($providers as $provider){
                            $payers=$provider->getPayers();
                            if($payers!=null){
                                foreach ($payers as $payer){
                                    if($payer->getId()==1){
                                        $contAssignedMMM++;
                                    }
                                }
                            }


                        }
                    }

                    if($contAssignedMMM>0){
                        foreach ($addresses as $address){

                            $cell_A='A'.$cont;$cell_C='C'.$cont;$cell_D='D'.$cont;$cell_E='E'.$cont;$cell_F='F'.$cont;$cell_J='J'.$cont;$cell_K='K'.$cont;
                            $cell_I='I'.$cont;$cell_L='L'.$cont;$cell_N='N'.$cont;$cell_O='O'.$cont;$cell_P='P'.$cont;
                            $cell_Q='Q'.$cont;$cell_V='V'.$cont;$cell_W='W'.$cont;$cell_X='X'.$cont;$cell_Y='Y'.$cont;$cell_Z='Z'.$cont;$cell_AA='AA'.$cont;$cell_AZ='AZ'.$cont;
                            $cell_BB='BB'.$cont;$cell_BC='BC'.$cont;$cell_BV='BV'.$cont;$cell_BW='BW'.$cont;$cell_BX='BX'.$cont;$cell_BY='BY'.$cont;$cell_CB='CB'.$cont;$cell_AH='AH'.$cont;

                            $locationNpi="";
                            if($address->getLocationNpi()!="" and $address->getLocationNpi()!=null){
                                $locationNpi=$address->getLocationNpi();
                            }else{
                                $locationNpi=$address->getOrganization()->getGroupNpi();
                            }

                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, 'New');
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $company_name);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $date);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, 'PAR');
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $locationNpi);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $address->getOrganization()->getName());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, "Yes");
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, "No");
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_P, "Yes");
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Q, "Yes");

                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_W, $address->getOrganization()->getName());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $address->getOrganization()->getBillingNpi());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $address->getOrganization()->getTinNumber());

                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AA, $address->getOrganization()->getName());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AZ, "No");
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BB, "Yes");
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BC, "85%");

                            $facilitiesTypes=$address->getFacilityType();
                            $facilitiesTypesStr="";
                            if($facilitiesTypes){
                                foreach ($facilitiesTypes as $ft){
                                    $facilitiesTypesStr.=$ft->getName().",";
                                }
                                $facilitiesTypesStr=substr($facilitiesTypesStr,0,-1);
                            }

                            $npi=$address->getOrganization()->getGroupNpi();
                            $pdata=$em->getRepository('App\Entity\PDataMedicare')->findOneBy(array('npi_number'=>$npi));
                            $taxonomySpecialties="";
                            if($pdata!=null) {
                                $json = $pdata->getData();
                                $datos=json_decode($json,true);
                                $taxonomies=$datos['results'][0]['taxonomies'];
                                foreach ($taxonomies as $taxonomy){
                                    if($taxonomy['primary']==1){
                                        $taxonomySpecialties=$taxonomy['desc'];
                                    }
                                }
                            }

                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $taxonomySpecialties);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AH, $this->AddressFax($address));
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BV, $company_name);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BW, $company_tin);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BX, $company_address);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BY, "No");
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CB, $this->AddressLanguages($address));
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z,$taxonomySpecialties);

                            $cell_AB='AB'.$cont;
                            $cell_AC='AC'.$cont;
                            $cell_AD='AD'.$cont;
                            $cell_AE='AE'.$cont;
                            $cell_AF='AF'.$cont;
                            $cell_AG='AG'.$cont;

                            $cell_AI='AI'.$cont;
                            $cell_AJ='AJ'.$cont;
                            $cell_AK='AK'.$cont;
                            $cell_AL='AL'.$cont;
                            $cell_AM='AM'.$cont;
                            $cell_AN='AN'.$cont;
                            $cell_AO='AO'.$cont;
                            $cell_CQ='CQ'.$cont;

                            $cell_AP='AP'.$cont;$cell_AQ='AQ'.$cont;$cell_AR='AR'.$cont;$cell_AS='AS'.$cont;$cell_AT='AT'.$cont;
                            $cell_AU='AU'.$cont;$cell_AV='AV'.$cont;
                            if($address->getBillingAddr()==true){
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                if($address->getBillingAddr()==true){
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                                }else{
                                    if($address->getMailingAddr()==true){
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                                    }else{
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                                    }
                                }
                            }

                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AB, $address->getStreet());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AC, $address->getSuiteNumber());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AD, $address->getCity());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AE, $address->getUsState());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AF, $address->getZipCode());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AG, $address->getPhoneNumber());

                            $cell_BA='BA'.$cont;

                            $serviceSettings=$address->getServiceSettings();
                            $serviceSettingStr="";

                            if(count($serviceSettings)>0){
                                foreach ($serviceSettings as $serviceSetting){
                                    $serviceSettingStr.=$serviceSetting->getName().",";
                                }
                                $serviceSettingStr=substr($serviceSettingStr,0,-1);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, $serviceSettingStr);
                            }else{
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, "");
                            }

                            $bussinesOurs=$address->getBusinessHours();
                            $bussinesOurs=substr($bussinesOurs,1);
                            $bussinesOurs=substr($bussinesOurs,0,-1);

                            $bussinesOurs=str_replace('{','',$bussinesOurs);
                            $bussinesOurs=str_replace('}','',$bussinesOurs);
                            $bussinesOurs=str_replace('"','',$bussinesOurs);

                            $daysHours = explode(",", $bussinesOurs);

                            //sunday
                            $dayActive=explode(":",$daysHours[18])[1];
                            $dayStart=explode(":",$daysHours[19]);
                            $dayTill=explode(":",$daysHours[20]);

                            if($dayActive=="false"){
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI,'');
                            }else{
                                $dateStart=$dayStart[1].$dayStart[2];
                                $date24Start=$this->convertTo24Hours($dateStart);
                                $dateTill=$dayTill[1].$dayTill[2];
                                $date24Till=$this->convertTo24Hours($dateTill);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI,$date24Start."-".$date24Till);
                            }

                            //monday
                            $dayActive=explode(":",$daysHours[0])[1];
                            $dayStart=explode(":",$daysHours[1]);
                            $dayTill=explode(":",$daysHours[2]);

                            if($dayActive=="false"){
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ,'');
                            }else{
                                $dateStart=$dayStart[1].$dayStart[2];
                                $date24Start=$this->convertTo24Hours($dateStart);
                                $dateTill=$dayTill[1].$dayTill[2];
                                $date24Till=$this->convertTo24Hours($dateTill);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ,$date24Start."-".$date24Till);
                            }

                            //tuesday
                            $dayActive=explode(":",$daysHours[3])[1];
                            $dayStart=explode(":",$daysHours[4]);
                            $dayTill=explode(":",$daysHours[5]);

                            if($dayActive=="false"){
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AK,'');
                            }else{
                                $dateStart=$dayStart[1].$dayStart[2];
                                $date24Start=$this->convertTo24Hours($dateStart);
                                $dateTill=$dayTill[1].$dayTill[2];
                                $date24Till=$this->convertTo24Hours($dateTill);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AK,$date24Start."-".$date24Till);
                            }

                            //wednesday
                            $dayActive=explode(":",$daysHours[6])[1];
                            $dayStart=explode(":",$daysHours[7]);
                            $dayTill=explode(":",$daysHours[8]);

                            if($dayActive=="false"){
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AL,'');
                            }else{
                                $dateStart=$dayStart[1].$dayStart[2];
                                $date24Start=$this->convertTo24Hours($dateStart);
                                $dateTill=$dayTill[1].$dayTill[2];
                                $date24Till=$this->convertTo24Hours($dateTill);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AL,$date24Start."-".$date24Till);
                            }

                            //thuerday
                            $dayActive=explode(":",$daysHours[9])[1];
                            $dayStart=explode(":",$daysHours[10]);
                            $dayTill=explode(":",$daysHours[11]);

                            if($dayActive=="false"){
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AM,'');
                            }else{
                                $dateStart=$dayStart[1].$dayStart[2];
                                $date24Start=$this->convertTo24Hours($dateStart);
                                $dateTill=$dayTill[1].$dayTill[2];
                                $date24Till=$this->convertTo24Hours($dateTill);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AM,$date24Start."-".$date24Till);
                            }

                            //friday
                            $dayActive=explode(":",$daysHours[12])[1];
                            $dayStart=explode(":",$daysHours[13]);
                            $dayTill=explode(":",$daysHours[14]);

                            if($dayActive=="false"){
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN,'');
                            }else{
                                $dateStart=$dayStart[1].$dayStart[2];
                                $date24Start=$this->convertTo24Hours($dateStart);
                                $dateTill=$dayTill[1].$dayTill[2];
                                $date24Till=$this->convertTo24Hours($dateTill);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN,$date24Start."-".$date24Till);
                            }

                            //saturday
                            $dayActive=explode(":",$daysHours[15])[1];
                            $dayStart=explode(":",$daysHours[16]);
                            $dayTill=explode(":",$daysHours[17]);

                            if($dayActive=="false"){
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO,'');
                            }else{
                                $dateStart=$dayStart[1].$dayStart[2];
                                $date24Start=$this->convertTo24Hours($dateStart);
                                $dateTill=$dayTill[1].$dayTill[2];
                                $date24Till=$this->convertTo24Hours($dateTill);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO,$date24Start."-".$date24Till);
                            }

                            if(in_array($address->getOrganization()->getGroupNpi(), $existNPIArray)){
                                $range="A".$cont.":"."CQ".$cont;
                                $spreadsheet->getActiveSheet(1)->getStyle($range)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('d457cd');
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CQ, 'Yes');
                            }else{
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CQ, 'No');
                            }

                            $cont++;
                        }
                    }
                }
            }
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="mmm-report-facility.xls"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');
        // Return a text response to the browser saying that the excel was succesfully created

        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/mmm/report-individual-changes", name="mmm_report_individual_changes")
     */
    public function individual_changes(){
        $projectDir = $this->getParameter('kernel.project_dir');
        $url = $projectDir . "/public/template_xls/";
        $reader = IOFactory::createReader('Xlsx');
        $spreadsheet = $reader->load($url . 'template-mmm-new.xlsx');

        $em = $this->getDoctrine()->getManager();

        $providers = $em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));
        $providersNPI = array();
        $providersResult = array();
        $providersResult_all = array();

        $cont = 1;
        foreach ($providers as $provider) {
            $isMMM = false;
            if ($provider->getIsFacility() == false) {
                $payers = $provider->getPayers();
                if ($payers != null) {

                    foreach ($payers as $payer) {
                        if ($payer->getId() == 1) {
                            $isMMM = true;
                            $cont++;
                        }
                    }
                }
            }
            if ($isMMM == true) {
                $providersResult_all[] = $provider;
                $providersNPI[] = $provider->getNpiNumber();
            }
        }

        $date = date('m/d/Y');

        $company = $em->getRepository('App\Entity\Company')->find(1);
        $company_tin = $company->getTinNumber();
        $company_address = $company->getAddress();
        $company_name = $company->getName();
        $mmm_log = $em->getRepository('App\Entity\MMMReportLog')->findBy(array('register_type' => 'individual'));

        $providersTerminated = [];
        foreach ($mmm_log as $mmm_l) {
            $npi = $mmm_l->getNpi();

            if (!in_array($npi, $providersNPI) and !in_array($npi,$providersTerminated) ) {
                $provider = $em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number' => $npi));
                $providersTerminated[] = $npi;
                $providersResult[] = $provider;
            }
        }

        $providersNew = [];
        foreach ($providersResult_all as $pro_new) {
            $npi = $pro_new->getNpiNumber();
            $providerLog = $em->getRepository('App\Entity\MMMReportLog')->findBy(array('npi' => $npi));
            if($providerLog==null){
                $providersNew[]=$npi;
                $providersResult[]=$pro_new;
            }
        }

        $cont = 2;
        foreach ($providersResult as $pro) {
            $provider = $em->getRepository('App\Entity\Provider')->findOneBy(array('id'=>$pro->getId()));
            if($provider!=null){
                $providerV = $em->getRepository('App\Entity\ViewProvider')->find($pro->getId());
                $npi = $provider->getNpiNumber();
                $pdata = $em->getRepository('App\Entity\PDataMedicare')->findOneBy(array('npi_number' => $npi));
                $specialty = "";
                if ($pdata != null) {
                    $json = $pdata->getData();
                    $datos = json_decode($json, true);

                    if(count($datos)>1){
                        $taxonomies = $datos['results'][0]['taxonomies'];
                        foreach ($taxonomies as $taxonomy) {
                            if ($taxonomy['primary'] == 1) {
                                $specialty = $taxonomy['desc'];
                            }
                        }
                    }
                }

                $organization = $pro->getOrganization();
                $billing_npi = $organization->getGroupNpi();

                $status = "";
                //get the status
                if (in_array($npi, $providersTerminated)) {
                    $status = "TERMINATED";
                }
                if (in_array($npi, $providersNew)) {
                    $status = "NEW";
                }

                //get TIN or Social
                $tins = $providerV->getTinNumbers();
                if ($tins == "") {
                    $private_key = $this->getParameter('private_key');
                    $encoder = new My_Mcript($private_key);
                    if ($provider) {
                        $tins = $encoder->decryptthis($provider->getSocial());
                    }
                }

                if ($tins == 0)
                    $tins = "";

                $boardCertified = "No";
                if ($provider->getBoardCertified() == 1) {
                    $boardCertified = "Yes";
                }

                $HospitalBase = "No";
                $organization=$provider->getOrganization();
                $OrgatizationClasification = $organization->getOrgSpecialties();
                if ($OrgatizationClasification) {
                    foreach ($OrgatizationClasification as $orgClas) {
                        if ($orgClas->getName() == "General Hospital" or $orgClas->getName() == "Hospital") {
                            $HospitalBase = "Yes";
                        }
                    }
                }

                //get address for provider
                $prov_addrs = $provider->getBillingAddress();
                if ($prov_addrs) {
                    $contTotalAddrs = count($prov_addrs);
                    foreach ($prov_addrs as $prov_addr) {
                        $cell_A = 'A' . $cont;
                        $cell_C = 'C' . $cont;
                        $cell_D = 'D' . $cont;
                        $cell_E = 'E' . $cont;
                        $cell_F = 'F' . $cont;
                        $cell_G = 'G' . $cont;
                        $cell_I = 'I' . $cont;
                        $cell_J = 'J' . $cont;
                        $cell_K = 'K' . $cont;
                        $cell_L = 'L' . $cont;
                        $cell_N = 'N' . $cont;
                        $cell_O = 'O' . $cont;
                        $cell_P = 'P' . $cont;
                        $cell_Q = 'Q' . $cont;
                        $cell_V = 'V' . $cont;
                        $cell_W = 'W' . $cont;
                        $cell_X = 'X' . $cont;
                        $cell_Y = 'Y' . $cont;
                        $cell_Z = 'Z' . $cont;
                        $cell_AA = 'AA' . $cont;
                        $cell_AZ = 'AZ' . $cont;
                        $cell_BB = 'BB' . $cont;
                        $cell_BC = 'BC' . $cont;
                        $cell_BV = 'BV' . $cont;
                        $cell_BW = 'BW' . $cont;
                        $cell_BX = 'BX' . $cont;
                        $cell_BY = 'BY' . $cont;
                        $cell_BZ = 'BZ' . $cont;
                        $cell_CA = 'CA' . $cont;
                        $cell_AW = 'AW' . $cont;
                        $cell_AX = 'AX' . $cont;
                        $cell_CB = 'CB' . $cont;
                        $cell_M = 'M' . $cont;
                        $cell_AB = 'AB' . $cont;
                        $cell_AC = 'AC' . $cont;
                        $cell_AD = 'AD' . $cont;
                        $cell_AE = 'AE' . $cont;
                        $cell_AF = 'AF' . $cont;
                        $cell_AG = 'AG' . $cont;
                        $cell_AI = 'AI' . $cont;
                        $cell_AJ = 'AJ' . $cont;
                        $cell_AK = 'AK' . $cont;
                        $cell_AL = 'AL' . $cont;
                        $cell_AM = 'AM' . $cont;
                        $cell_AN = 'AN' . $cont;
                        $cell_AO = 'AO' . $cont;
                        $cell_AH = 'AH' . $cont;

                        $address = $em->getRepository('App\Entity\BillingAddress')->find($prov_addr);

                        $cell_AP = 'AP' . $cont;
                        $cell_AQ = 'AQ' . $cont;
                        $cell_AR = 'AR' . $cont;
                        $cell_AS = 'AS' . $cont;
                        $cell_AT = 'AT' . $cont;
                        $cell_AU = 'AU' . $cont;
                        $cell_AV = 'AV' . $cont;
                        $cell_CA = 'CA' . $cont;
                        $cell_CB = 'CB' . $cont;

                        if ($address->getBillingAddr() == true) {
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                        } else {
                            if ($address->getMailingAddr() == true) {
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                            } else {
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                            }
                        }

                        if (true) {
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AB, $address->getStreet());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AC, $address->getSuiteNumber());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AD, $address->getCity());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AE, $address->getUsState());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AF, $address->getZipCode());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AG, $address->getPhoneNumber());

                            $cell_BA = 'BA' . $cont;

                            $serviceSettings = $address->getServiceSettings();
                            $serviceSettingStr = "";

                            if (count($serviceSettings) > 0) {
                                foreach ($serviceSettings as $serviceSetting) {
                                    $serviceSettingStr .= $serviceSetting->getName() . ",";
                                }
                                $serviceSettingStr = substr($serviceSettingStr, 0, -1);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, $serviceSettingStr);
                            } else {
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, "");
                            }

                            $bussinesOurs = $address->getBusinessHours();
                            if($bussinesOurs!=""){
                                $bussinesOurs = substr($bussinesOurs, 1);
                                $bussinesOurs = substr($bussinesOurs, 0, -1);

                                $bussinesOurs = str_replace('{', '', $bussinesOurs);
                                $bussinesOurs = str_replace('}', '', $bussinesOurs);
                                $bussinesOurs = str_replace('"', '', $bussinesOurs);

                                $daysHours = explode(",", $bussinesOurs);

                                $daysHours[0];
                                $daysHours[3];
                                $daysHours[6];
                                $daysHours[9];
                                $daysHours[12];
                                $daysHours[15];

                                //sunday
                                $dayActive = explode(":", $daysHours[18])[1];
                                $dayStart = explode(":", $daysHours[19]);
                                $dayTill = explode(":", $daysHours[20]);

                                if ($dayActive == "false") {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI, '');
                                } else {
                                    $dateStart = $dayStart[1] . $dayStart[2];
                                    $date24Start = $this->convertTo24Hours($dateStart);
                                    $dateTill = $dayTill[1] . $dayTill[2];
                                    $date24Till = $this->convertTo24Hours($dateTill);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI, $date24Start . "-" . $date24Till);
                                }

                                //monday
                                $dayActive = explode(":", $daysHours[0])[1];
                                $dayStart = explode(":", $daysHours[1]);
                                $dayTill = explode(":", $daysHours[2]);

                                if ($dayActive == "false") {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ, '');
                                } else {
                                    $dateStart = $dayStart[1] . $dayStart[2];
                                    $date24Start = $this->convertTo24Hours($dateStart);
                                    $dateTill = $dayTill[1] . $dayTill[2];
                                    $date24Till = $this->convertTo24Hours($dateTill);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ, $date24Start . "-" . $date24Till);
                                }

                                //tuesday
                                $dayActive = explode(":", $daysHours[3])[1];
                                $dayStart = explode(":", $daysHours[4]);
                                $dayTill = explode(":", $daysHours[5]);

                                if ($dayActive == "false") {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AK, '');
                                } else {
                                    $dateStart = $dayStart[1] . $dayStart[2];
                                    $date24Start = $this->convertTo24Hours($dateStart);
                                    $dateTill = $dayTill[1] . $dayTill[2];
                                    $date24Till = $this->convertTo24Hours($dateTill);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AK, $date24Start . "-" . $date24Till);
                                }

                                //wednesday
                                $dayActive = explode(":", $daysHours[6])[1];
                                $dayStart = explode(":", $daysHours[7]);
                                $dayTill = explode(":", $daysHours[8]);

                                if ($dayActive == "false") {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AL, '');
                                } else {
                                    $dateStart = $dayStart[1] . $dayStart[2];
                                    $date24Start = $this->convertTo24Hours($dateStart);
                                    $dateTill = $dayTill[1] . $dayTill[2];
                                    $date24Till = $this->convertTo24Hours($dateTill);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AL, $date24Start . "-" . $date24Till);
                                }

                                //thuerday
                                $dayActive = explode(":", $daysHours[9])[1];
                                $dayStart = explode(":", $daysHours[10]);
                                $dayTill = explode(":", $daysHours[11]);

                                if ($dayActive == "false") {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AM, '');
                                } else {
                                    $dateStart = $dayStart[1] . $dayStart[2];
                                    $date24Start = $this->convertTo24Hours($dateStart);
                                    $dateTill = $dayTill[1] . $dayTill[2];
                                    $date24Till = $this->convertTo24Hours($dateTill);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AM, $date24Start . "-" . $date24Till);
                                }

                                //friday
                                $dayActive = explode(":", $daysHours[12])[1];
                                $dayStart = explode(":", $daysHours[13]);
                                $dayTill = explode(":", $daysHours[14]);

                                if ($dayActive == "false") {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN, '');
                                } else {
                                    $dateStart = $dayStart[1] . $dayStart[2];
                                    $date24Start = $this->convertTo24Hours($dateStart);
                                    $dateTill = $dayTill[1] . $dayTill[2];
                                    $date24Till = $this->convertTo24Hours($dateTill);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN, $date24Start . "-" . $date24Till);
                                }

                                //saturday
                                $dayActive = explode(":", $daysHours[15])[1];
                                $dayStart = explode(":", $daysHours[16]);
                                $dayTill = explode(":", $daysHours[17]);

                                if ($dayActive == "false") {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO, '');
                                } else {
                                    $dateStart = $dayStart[1] . $dayStart[2];
                                    $date24Start = $this->convertTo24Hours($dateStart);
                                    $dateTill = $dayTill[1] . $dayTill[2];
                                    $date24Till = $this->convertTo24Hours($dateTill);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO, $date24Start . "-" . $date24Till);
                                }
                            }


                            $effectiveDate="";
                            $credentialings=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('npi_number'=>$provider->getNpiNumber()));
                            if($credentialings!=null){
                                foreach ($credentialings as $credentialing){
                                    if($credentialing->getCredentialingEffectiveDate()!=""){
                                        $effectiveDate=$credentialing->getCredentialingEffectiveDate();
                                    }
                                }
                            }

                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, $status);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $company_name);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $date);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, 'PAR');
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $provider->getNpiNumber());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_G, $effectiveDate);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $provider->getLastName());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $provider->getInitial());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $provider->getFirstName());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $specialty);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, "Yes");
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, "No");
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_P, "Yes");
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Q, "Yes");
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_V, $providerV->getDegrees());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_W, $providerV->getOrganizationsstr());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $billing_npi);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AW, $provider->getHopitalPrivilegesType());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AX, $provider->getHopitalPrivileges());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $this->ProviderEmail($provider));

                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $tins);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z, $specialty);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AA, $providerV->getOrganizationsstr());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AZ, "No");
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BB, "Yes");
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BC, "85%");
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BV, $company_name);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BW, $company_tin);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BX, $company_address);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BY, $boardCertified);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_BZ, $provider->getBoardCertifiedSpecialty());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CA, $HospitalBase);
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CB, $providerV->getLanguagesStr());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_CB, $providerV->getLanguagesStr());
                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AH, $this->getFaxNumber($provider));
                        }
                        $cont++;
                    }
                }
            }
        }

        //run for get the changes

        foreach ($providersResult_all as $pro) {
            $npi = $pro->getNpiNumber();
            if (!in_array($npi, $providersNew)) {
                $provider = $pro;
                $providerV = $em->getRepository('App\Entity\ViewProvider')->find($pro->getId());
                $pdata = $em->getRepository('App\Entity\PDataMedicare')->findOneBy(array('npi_number' => $npi));
                $specialty = "";
                if ($pdata != null) {
                    $json = $pdata->getData();
                    $datos = json_decode($json, true);
                    if(!isset($datos['errors'])){
                        $taxonomies = $datos['results'][0]['taxonomies'];
                        foreach ($taxonomies as $taxonomy) {
                            if ($taxonomy['primary'] == 1) {
                                $specialty = $taxonomy['desc'];
                            }
                        }
                    }
                }

                $organization = $pro->getOrganization();
                $billing_npi = $organization->getGroupNpi();

                //get TIN or Social
                $tins = $providerV->getTinNumbers();

                if ($tins == "") {
                    $private_key = $this->getParameter('private_key');
                    $encoder = new My_Mcript($private_key);
                    if ($provider) {
                        $tins = $encoder->decryptthis($provider->getSocial());
                    }
                }

                if ($tins == 0)
                    $tins = "";

                $boardCertified = "No";
                if ($provider->getBoardCertified() == 1) {
                    $boardCertified = "Yes";
                }

                $HospitalBase = "No";

                $organization=$provider->getOrganization();
                $OrgatizationClasification = $organization->getOrgSpecialties();
                if ($OrgatizationClasification) {
                    foreach ($OrgatizationClasification as $orgClas) {
                        if ($orgClas->getName() == "General Hospital" or $orgClas->getName() == "Hospital") {
                            $HospitalBase = "Yes";
                        }
                    }
                }

                //get address for provider
                $prov_addrs = $provider->getBillingAddress();
                if ($prov_addrs) {
                    foreach ($prov_addrs as $prov_addr) {
                        $npi=$pro->getNpiNumber();
                        $is_changed=false;
                        $cause_change="";
                        $last_name=$pro->getLastName();
                        $phone=$prov_addr->getPhoneNumber();
                        $phone = str_replace("-", "", $phone);
                        $email=$this->ProviderEmail($provider);

                        $mmm_log = $em->getRepository('App\Entity\MMMReportLog')->findBy(array('register_type' => 'individual','npi'=>$npi));
                        if($mmm_log!=null){
                            foreach ($mmm_log as $mmm_l) {
                                $log_last_name=$mmm_l->getLastName();

                                if(strtolower($log_last_name)!=strtolower($last_name)){
                                    $is_changed=true;
                                    $cause_change="Last Name changed, ";
                                }

                                $log_phone=$mmm_l->getPhoneNumber();
                                if(strtolower($log_phone)!=strtolower($phone)){

                                    $is_changed=true;
                                    $cause_change.="Phone Number changed, ";
                                }

                                $log_email=$mmm_l->getEmail();
                                if(strtolower($log_email)!=strtolower($email)){
                                    $is_changed=true;
                                    $cause_change.="Email changed, ";
                                }
                            }
                        }

                        if($is_changed==true) {
                            $cell_A = 'A' . $cont;
                            $cell_B = 'B' . $cont;
                            $cell_C = 'C' . $cont;
                            $cell_D = 'D' . $cont;
                            $cell_E = 'E' . $cont;
                            $cell_F = 'F' . $cont;
                            $cell_G = 'G' . $cont;
                            $cell_I = 'I' . $cont;
                            $cell_J = 'J' . $cont;
                            $cell_K = 'K' . $cont;
                            $cell_L = 'L' . $cont;
                            $cell_N = 'N' . $cont;
                            $cell_O = 'O' . $cont;
                            $cell_P = 'P' . $cont;
                            $cell_Q = 'Q' . $cont;
                            $cell_V = 'V' . $cont;
                            $cell_W = 'W' . $cont;
                            $cell_X = 'X' . $cont;
                            $cell_Y = 'Y' . $cont;
                            $cell_Z = 'Z' . $cont;
                            $cell_AA = 'AA' . $cont;
                            $cell_AZ = 'AZ' . $cont;
                            $cell_BB = 'BB' . $cont;
                            $cell_BC = 'BC' . $cont;
                            $cell_BV = 'BV' . $cont;
                            $cell_BW = 'BW' . $cont;
                            $cell_BX = 'BX' . $cont;
                            $cell_BY = 'BY' . $cont;
                            $cell_BZ = 'BZ' . $cont;
                            $cell_AW = 'AW' . $cont;
                            $cell_AX = 'AX' . $cont;
                            $cell_M = 'M' . $cont;

                            $cell_AB = 'AB' . $cont;
                            $cell_AC = 'AC' . $cont;
                            $cell_AD = 'AD' . $cont;
                            $cell_AE = 'AE' . $cont;
                            $cell_AF = 'AF' . $cont;
                            $cell_AG = 'AG' . $cont;

                            $cell_AI = 'AI' . $cont;
                            $cell_AJ = 'AJ' . $cont;
                            $cell_AK = 'AK' . $cont;
                            $cell_AL = 'AL' . $cont;
                            $cell_AM = 'AM' . $cont;
                            $cell_AN = 'AN' . $cont;
                            $cell_AO = 'AO' . $cont;
                            $cell_AH = 'AH' . $cont;
                            $cell_AP = 'AP' . $cont;
                            $cell_AQ = 'AQ' . $cont;
                            $cell_AR = 'AR' . $cont;
                            $cell_AS = 'AS' . $cont;
                            $cell_AT = 'AT' . $cont;
                            $cell_AU = 'AU' . $cont;
                            $cell_AV = 'AV' . $cont;
                            $cell_CA = 'CA' . $cont;
                            $cell_CB = 'CB' . $cont;

                            $address = $prov_addr;

                            if ($address->getBillingAddr() == true) {
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                            } else {
                                if ($address->getMailingAddr() == true) {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                                } else {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                                }
                            }

                            if (true) {
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AB, $address->getStreet());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AC, $address->getSuiteNumber());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AD, $address->getCity());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AE, $address->getUsState());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AF, $address->getZipCode());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AG, $address->getPhoneNumber());

                                $cell_BA = 'BA' . $cont;

                                $serviceSettings = $address->getServiceSettings();
                                $serviceSettingStr = "";

                                if (count($serviceSettings) > 0) {
                                    foreach ($serviceSettings as $serviceSetting) {
                                        $serviceSettingStr .= $serviceSetting->getName() . ",";
                                    }
                                    $serviceSettingStr = substr($serviceSettingStr, 0, -1);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, $serviceSettingStr);
                                } else {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, "");
                                }

                                $bussinesOurs = $address->getBusinessHours();
                                if($bussinesOurs!=""){
                                    $bussinesOurs = substr($bussinesOurs, 1);
                                    $bussinesOurs = substr($bussinesOurs, 0, -1);

                                    $bussinesOurs = str_replace('{', '', $bussinesOurs);
                                    $bussinesOurs = str_replace('}', '', $bussinesOurs);
                                    $bussinesOurs = str_replace('"', '', $bussinesOurs);

                                    $daysHours = explode(",", $bussinesOurs);

                                    $daysHours[0];
                                    $daysHours[3];
                                    $daysHours[6];
                                    $daysHours[9];
                                    $daysHours[12];
                                    $daysHours[15];

                                    //sunday
                                    $dayActive = explode(":", $daysHours[18])[1];
                                    $dayStart = explode(":", $daysHours[19]);
                                    $dayTill = explode(":", $daysHours[20]);

                                    if ($dayActive == "false") {
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI, '');
                                    } else {
                                        $dateStart = $dayStart[1] . $dayStart[2];
                                        $date24Start = $this->convertTo24Hours($dateStart);
                                        $dateTill = $dayTill[1] . $dayTill[2];
                                        $date24Till = $this->convertTo24Hours($dateTill);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI, $date24Start . "-" . $date24Till);
                                    }

                                    //monday
                                    $dayActive = explode(":", $daysHours[0])[1];
                                    $dayStart = explode(":", $daysHours[1]);
                                    $dayTill = explode(":", $daysHours[2]);

                                    if ($dayActive == "false") {
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ, '');
                                    } else {
                                        $dateStart = $dayStart[1] . $dayStart[2];
                                        $date24Start = $this->convertTo24Hours($dateStart);
                                        $dateTill = $dayTill[1] . $dayTill[2];
                                        $date24Till = $this->convertTo24Hours($dateTill);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ, $date24Start . "-" . $date24Till);
                                    }

                                    //tuesday
                                    $dayActive = explode(":", $daysHours[3])[1];
                                    $dayStart = explode(":", $daysHours[4]);
                                    $dayTill = explode(":", $daysHours[5]);

                                    if ($dayActive == "false") {
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AK, '');
                                    } else {
                                        $dateStart = $dayStart[1] . $dayStart[2];
                                        $date24Start = $this->convertTo24Hours($dateStart);
                                        $dateTill = $dayTill[1] . $dayTill[2];
                                        $date24Till = $this->convertTo24Hours($dateTill);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AK, $date24Start . "-" . $date24Till);
                                    }

                                    //wednesday
                                    $dayActive = explode(":", $daysHours[6])[1];
                                    $dayStart = explode(":", $daysHours[7]);
                                    $dayTill = explode(":", $daysHours[8]);

                                    if ($dayActive == "false") {
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AL, '');
                                    } else {
                                        $dateStart = $dayStart[1] . $dayStart[2];
                                        $date24Start = $this->convertTo24Hours($dateStart);
                                        $dateTill = $dayTill[1] . $dayTill[2];
                                        $date24Till = $this->convertTo24Hours($dateTill);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AL, $date24Start . "-" . $date24Till);
                                    }

                                    //thuerday
                                    $dayActive = explode(":", $daysHours[9])[1];
                                    $dayStart = explode(":", $daysHours[10]);
                                    $dayTill = explode(":", $daysHours[11]);

                                    if ($dayActive == "false") {
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AM, '');
                                    } else {
                                        $dateStart = $dayStart[1] . $dayStart[2];
                                        $date24Start = $this->convertTo24Hours($dateStart);
                                        $dateTill = $dayTill[1] . $dayTill[2];
                                        $date24Till = $this->convertTo24Hours($dateTill);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AM, $date24Start . "-" . $date24Till);
                                    }

                                    //friday
                                    $dayActive = explode(":", $daysHours[12])[1];
                                    $dayStart = explode(":", $daysHours[13]);
                                    $dayTill = explode(":", $daysHours[14]);

                                    if ($dayActive == "false") {
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN, '');
                                    } else {
                                        $dateStart = $dayStart[1] . $dayStart[2];
                                        $date24Start = $this->convertTo24Hours($dateStart);
                                        $dateTill = $dayTill[1] . $dayTill[2];
                                        $date24Till = $this->convertTo24Hours($dateTill);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN, $date24Start . "-" . $date24Till);
                                    }

                                    //saturday
                                    $dayActive = explode(":", $daysHours[15])[1];
                                    $dayStart = explode(":", $daysHours[16]);
                                    $dayTill = explode(":", $daysHours[17]);

                                    if ($dayActive == "false") {
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO, '');
                                    } else {
                                        $dateStart = $dayStart[1] . $dayStart[2];
                                        $date24Start = $this->convertTo24Hours($dateStart);
                                        $dateTill = $dayTill[1] . $dayTill[2];
                                        $date24Till = $this->convertTo24Hours($dateTill);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO, $date24Start . "-" . $date24Till);
                                    }
                                }

                                $status="CHANGED";

                                $cause_change=substr($cause_change,0,-1);

                                $effectiveDate="";
                                $credentialings=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('npi_number'=>$provider->getNpiNumber()));
                                if($credentialings!=null){
                                    foreach ($credentialings as $credentialing){
                                        if($credentialing->getCredentialingEffectiveDate()!=""){
                                            $effectiveDate=$credentialing->getCredentialingEffectiveDate();
                                        }
                                    }
                                }

                                $emails=array();

                                $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));
                                if($contacts){
                                    foreach ($contacts as $contact){
                                        if($contact->getEmail()!=""){
                                            $emails[]=$contact->getEmail();
                                        }
                                    }
                                }
                                if(count($emails)>0){
                                    $email=$emails[0];
                                }

                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, $status);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_B, $cause_change);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $company_name);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $date);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, 'PAR');
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $provider->getNpiNumber());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_G, $effectiveDate);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $provider->getLastName());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $provider->getInitial());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $provider->getFirstName());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $specialty);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, "Yes");
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, "No");
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_P, "Yes");
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Q, "Yes");
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_V, $providerV->getDegrees());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_W, $providerV->getOrganizationsstr());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $billing_npi);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AW, $provider->getHopitalPrivilegesType());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AX, $provider->getHopitalPrivileges());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $this->ProviderEmail($provider));
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $tins);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z, $specialty);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AA, $providerV->getOrganizationsstr());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AZ, "No");
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BB, "Yes");
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BC, "85%");
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BV, $company_name);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BW, $company_tin);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BX, $company_address);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BY, $boardCertified);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BZ, $provider->getBoardCertifiedSpecialty());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CA, $HospitalBase);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CB, $providerV->getLanguagesStr());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CB, $providerV->getLanguagesStr());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AH, $this->getFaxNumber($provider));
                            }
                            $cont++;
                        }
                    }
                }
            }
        }

        // Redirect output to a client’s web browser (XlS)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="mmm-report-simple.xls"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/mmm/save-mmm-report-log-facility", name="mmm_save_report_log_facility")
     */
    public function savereportLogFacility(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "mmm-reports/bsn-mmm-report-facility.xlsx";

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(1);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=0;

        foreach ($sheetData as $sheet) {
            if ($cont >= 1) {
                if ($sheet['A'] != null or $sheet['A'] != "") {

                    $status=$sheet['A'];
                    $npi=$sheet['F'];
                    $reporting_date=$sheet['D'];
                    $first_name=$sheet['K'];
                    $specialties=$sheet['L'];
                    $email=$sheet['M'];
                    $street=$sheet['AB'];
                    $suite=$sheet['AC'];
                    $phone_number=$sheet['AG'];
                    $billing_address=$sheet['AP'];
                    $practice_name=$sheet['AA'];
                    $zip_code=$sheet['AF'];
                    $state_code=$sheet['AE'];
                    $county_name="";
                    $city=$sheet['AD'];

                    $mmm_log=new MMMReportLog();
                    $mmm_log->setStatus($status);
                    $mmm_log->setRegisterType('facility');
                    $mmm_log->setNpi($npi);
                    $mmm_log->setFirstName($first_name);
                    $mmm_log->setReportingDate($reporting_date);
                    $mmm_log->setSpecialties($specialties);
                    $mmm_log->setDegrees('');
                    $mmm_log->setStreet($street);
                    $mmm_log->setSuite($suite);
                    $mmm_log->setPhoneNumber($phone_number);
                    $mmm_log->setEmail($email);
                    $mmm_log->setBillingAddress($billing_address);
                    $mmm_log->setPracticeName($practice_name);
                    $mmm_log->setZipCode($zip_code);
                    $mmm_log->setStateCode($state_code);
                    $mmm_log->setCountyName($county_name);
                    $mmm_log->setCity($city);
                    $em->persist($mmm_log);
                }
            }
            $cont++;
        }

        $em->flush();

    die();
    }

    /**
     * @Route("/mmm/save-mmm-report-log-individual", name="mmm_save_report_log_individual")
     */
    public function savereportLogIndividual(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "mmm-reports/bsn-mmm-report-simple.xlsx";

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(1);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=0;

        foreach ($sheetData as $sheet) {
            if ($cont >= 1) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    $status=$sheet['A'];
                    $npi=$sheet['F'];
                    $reporting_date=$sheet['D'];
                    $first_name=$sheet['K'];
                    $last_name=$sheet['I'];
                    $specialties=$sheet['L'];
                    $email=$sheet['M'];
                    $street=$sheet['AB'];
                    $suite=$sheet['AC'];
                    $phone_number=$sheet['AG'];
                    $billing_address=$sheet['AP'];
                    $practice_name=$sheet['AA'];
                    $zip_code=$sheet['AF'];
                    $state_code=$sheet['AE'];
                    $county_name="";
                    $city=$sheet['AD'];

                    $provider=$em->getRepository('App\Entity\Provider')->findBy(array('npi_number'=>$npi));

                    $mmm_log=new MMMReportLog();
                    $mmm_log->setStatus($status);
                    $mmm_log->setRegisterType('individual');
                    $mmm_log->setNpi($npi);
                    $mmm_log->setFirstName($first_name);
                    $mmm_log->setLastName($last_name);
                    $mmm_log->setReportingDate($reporting_date);


                    $mmm_log->setSpecialties($specialties);
                    $mmm_log->setDegrees('');
                    $mmm_log->setStreet($street);
                    $mmm_log->setSuite($suite);
                    $mmm_log->setPhoneNumber($phone_number);
                    $mmm_log->setEmail('');
                    $mmm_log->setBillingAddress($billing_address);
                    $mmm_log->setPracticeName($practice_name);
                    $mmm_log->setZipCode($zip_code);
                    $mmm_log->setStateCode($state_code);
                    $mmm_log->setCountyName($county_name);
                    $mmm_log->setCity($city);
                    $mmm_log->setEmail($email);

                    if(count($provider)==1){
                        if($provider!=null){
                            foreach ($provider as $pro){
                                $mmm_log->setIdLog($pro->getId());
                            }
                        }

                    }

                    $em->persist($mmm_log);
                }
            }
            $cont++;
        }

        $em->flush();

        die();
    }


    /**
     * @Route("/mmm/report_facility_change", name="mmm_report_facility_change")
     */
    public function facility_change(){
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');

        $spreadsheet = $reader->load($url.'template-mmm.xlsx');
        $em=$this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App\Entity\Organization')->findBy(array('disabled'=>0,'organization_status'=>2));
        $cont=2;
        $date=date('m/d/Y');

        foreach ($organizations as $organization) {
            $company=$em->getRepository('App\Entity\Company')->find(1);
            $company_tin=$company->getTinNumber();
            $company_address=$company->getAddress();
            $company_name=$company->getName();

            $addresses = $em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
            $contF=0;

            if($addresses!=null){
                foreach ($addresses as $addr) {
                    if($addr->getIsFacility()==true){
                        $contF++;
                    }
                }
                if($contF>0){
                    $providers = $em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));
                    $contAssignedMMM=0;

                    if($providers!=null){
                        foreach ($providers as $provider){
                            $payers=$provider->getPayers();
                            if($payers!=null){
                                foreach ($payers as $payer){
                                    if($payer->getId()==1){
                                        $contAssignedMMM++;
                                    }
                                }
                            }
                        }
                    }
                    $org_have_mmm=false;
                    $org_payers=$organization->getPayers();
                    if($org_payers!=null){
                        foreach ($org_payers as $orgP){
                            if($orgP->getId()==1){
                                $org_have_mmm=true;
                            }
                        }
                    }

                    if($contAssignedMMM>0 or $org_have_mmm==true){
                        $npi=$organization->getGroupNpi();
                        $mmm_log = $em->getRepository('App\Entity\MMMReportLog')->findBy(array('register_type' => 'facility','npi'=>$npi));

                        if($mmm_log==null){
                            foreach ($addresses as $address){
                                $cell_A='A'.$cont;$cell_C='C'.$cont;$cell_D='D'.$cont;$cell_E='E'.$cont;$cell_F='F'.$cont;$cell_M='M'.$cont;
                                $cell_I='I'.$cont;$cell_L='L'.$cont;$cell_N='N'.$cont;$cell_O='O'.$cont;$cell_P='P'.$cont;
                                $cell_Q='Q'.$cont;$cell_W='W'.$cont;$cell_X='X'.$cont;$cell_Y='Y'.$cont;$cell_Z='Z'.$cont;$cell_AA='AA'.$cont;$cell_AZ='AZ'.$cont;
                                $cell_BB='BB'.$cont;$cell_BC='BC'.$cont;$cell_BV='BV'.$cont;$cell_BW='BW'.$cont;$cell_BX='BX'.$cont;$cell_BY='BY'.$cont;$cell_CB='CB'.$cont;$cell_AH='AH'.$cont;

                                if($address->getLocationNpi()!="" and $address->getLocationNpi()!=null){
                                    $locationNpi=$address->getLocationNpi();
                                }else{
                                    $locationNpi=$address->getOrganization()->getGroupNpi();
                                }

                                $email="";
                                $emails=array();
                                $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));
                                if($contacts){
                                    foreach ($contacts as $contact){
                                        if($contact->getEmail()!=""){
                                            $emails[]=$contact->getEmail();
                                        }
                                    }
                                }
                                if(count($emails)>0){
                                    $email=$emails[0];
                                }


                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, 'New');
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $company_name);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $date);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, 'PAR');
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $locationNpi);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $address->getOrganization()->getName());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, "Yes");
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, "No");
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_P, "Yes");
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Q, "Yes");
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $email);

                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_W, $address->getOrganization()->getName());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $address->getOrganization()->getBillingNpi());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $address->getOrganization()->getTinNumber());

                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AA, $address->getOrganization()->getName());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AZ, "No");
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BB, "Yes");
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BC, "85%");

                                $facilitiesTypes=$address->getFacilityType();
                                $facilitiesTypesStr="";
                                if($facilitiesTypes){
                                    foreach ($facilitiesTypes as $ft){
                                        $facilitiesTypesStr.=$ft->getName().",";
                                    }
                                    $facilitiesTypesStr=substr($facilitiesTypesStr,0,-1);
                                }

                                $npi=$address->getOrganization()->getGroupNpi();
                                $pdata=$em->getRepository('App\Entity\PDataMedicare')->findOneBy(array('npi_number'=>$npi));
                                $taxonomySpecialties="";
                                if($pdata!=null) {
                                    $json = $pdata->getData();
                                    $datos=json_decode($json,true);
                                    $taxonomies=$datos['results'][0]['taxonomies'];
                                    foreach ($taxonomies as $taxonomy){
                                        if($taxonomy['primary']==1){
                                            $taxonomySpecialties=$taxonomy['desc'];
                                        }
                                    }
                                }

                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $taxonomySpecialties);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AH, $this->AddressFax($address));
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BV, $company_name);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BW, $company_tin);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BX, $company_address);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BY, "No");
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CB, $this->AddressLanguages($address));
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z,$taxonomySpecialties);

                                $cell_AB='AB'.$cont;
                                $cell_AC='AC'.$cont;
                                $cell_AD='AD'.$cont;
                                $cell_AE='AE'.$cont;
                                $cell_AF='AF'.$cont;
                                $cell_AG='AG'.$cont;

                                $cell_AI='AI'.$cont;
                                $cell_AJ='AJ'.$cont;
                                $cell_AK='AK'.$cont;
                                $cell_AL='AL'.$cont;
                                $cell_AM='AM'.$cont;
                                $cell_AN='AN'.$cont;
                                $cell_AO='AO'.$cont;

                                $cell_AP='AP'.$cont;$cell_AQ='AQ'.$cont;$cell_AR='AR'.$cont;$cell_AS='AS'.$cont;$cell_AT='AT'.$cont;
                                $cell_AU='AU'.$cont;$cell_AV='AV'.$cont;
                                if($address->getBillingAddr()==true){
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                    if($address->getBillingAddr()==true){
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                                    }else{
                                        if($address->getMailingAddr()==true){
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                                        }else{
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                                        }
                                    }
                                }

                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AB, $address->getStreet());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AC, $address->getSuiteNumber());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AD, $address->getCity());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AE, $address->getUsState());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AF, $address->getZipCode());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AG, $address->getPhoneNumber());
                                $cell_BA='BA'.$cont;
                                $serviceSettings=$address->getServiceSettings();
                                $serviceSettingStr="";

                                if(count($serviceSettings)>0){
                                    foreach ($serviceSettings as $serviceSetting){
                                        $serviceSettingStr.=$serviceSetting->getName().",";
                                    }
                                    $serviceSettingStr=substr($serviceSettingStr,0,-1);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, $serviceSettingStr);
                                }else{
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, "");
                                }

                                $bussinesOurs=$address->getBusinessHours();
                                if($bussinesOurs!=""){
                                    $bussinesOurs=substr($bussinesOurs,1);
                                    $bussinesOurs=substr($bussinesOurs,0,-1);

                                    $bussinesOurs=str_replace('{','',$bussinesOurs);
                                    $bussinesOurs=str_replace('}','',$bussinesOurs);
                                    $bussinesOurs=str_replace('"','',$bussinesOurs);

                                    $daysHours = explode(",", $bussinesOurs);

                                    //sunday
                                    $dayActive=explode(":",$daysHours[18])[1];
                                    $dayStart=explode(":",$daysHours[19]);
                                    $dayTill=explode(":",$daysHours[20]);

                                    if($dayActive=="false"){
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI,'');
                                    }else{
                                        $dateStart=$dayStart[1].$dayStart[2];
                                        $date24Start=$this->convertTo24Hours($dateStart);
                                        $dateTill=$dayTill[1].$dayTill[2];
                                        $date24Till=$this->convertTo24Hours($dateTill);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI,$date24Start."-".$date24Till);
                                    }

                                    //monday
                                    $dayActive=explode(":",$daysHours[0])[1];
                                    $dayStart=explode(":",$daysHours[1]);
                                    $dayTill=explode(":",$daysHours[2]);

                                    if($dayActive=="false"){
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ,'');
                                    }else{
                                        $dateStart=$dayStart[1].$dayStart[2];
                                        $date24Start=$this->convertTo24Hours($dateStart);
                                        $dateTill=$dayTill[1].$dayTill[2];
                                        $date24Till=$this->convertTo24Hours($dateTill);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ,$date24Start."-".$date24Till);
                                    }

                                    //tuesday
                                    $dayActive=explode(":",$daysHours[3])[1];
                                    $dayStart=explode(":",$daysHours[4]);
                                    $dayTill=explode(":",$daysHours[5]);

                                    if($dayActive=="false"){
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AK,'');
                                    }else{
                                        $dateStart=$dayStart[1].$dayStart[2];
                                        $date24Start=$this->convertTo24Hours($dateStart);
                                        $dateTill=$dayTill[1].$dayTill[2];
                                        $date24Till=$this->convertTo24Hours($dateTill);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AK,$date24Start."-".$date24Till);
                                    }

                                    //wednesday
                                    $dayActive=explode(":",$daysHours[6])[1];
                                    $dayStart=explode(":",$daysHours[7]);
                                    $dayTill=explode(":",$daysHours[8]);

                                    if($dayActive=="false"){
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AL,'');
                                    }else{
                                        $dateStart=$dayStart[1].$dayStart[2];
                                        $date24Start=$this->convertTo24Hours($dateStart);
                                        $dateTill=$dayTill[1].$dayTill[2];
                                        $date24Till=$this->convertTo24Hours($dateTill);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AL,$date24Start."-".$date24Till);
                                    }

                                    //thuerday
                                    $dayActive=explode(":",$daysHours[9])[1];
                                    $dayStart=explode(":",$daysHours[10]);
                                    $dayTill=explode(":",$daysHours[11]);

                                    if($dayActive=="false"){
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AM,'');
                                    }else{
                                        $dateStart=$dayStart[1].$dayStart[2];
                                        $date24Start=$this->convertTo24Hours($dateStart);
                                        $dateTill=$dayTill[1].$dayTill[2];
                                        $date24Till=$this->convertTo24Hours($dateTill);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AM,$date24Start."-".$date24Till);
                                    }

                                    //friday
                                    $dayActive=explode(":",$daysHours[12])[1];
                                    $dayStart=explode(":",$daysHours[13]);
                                    $dayTill=explode(":",$daysHours[14]);

                                    if($dayActive=="false"){
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN,'');
                                    }else{
                                        $dateStart=$dayStart[1].$dayStart[2];
                                        $date24Start=$this->convertTo24Hours($dateStart);
                                        $dateTill=$dayTill[1].$dayTill[2];
                                        $date24Till=$this->convertTo24Hours($dateTill);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN,$date24Start."-".$date24Till);
                                    }

                                    //saturday
                                    $dayActive=explode(":",$daysHours[15])[1];
                                    $dayStart=explode(":",$daysHours[16]);
                                    $dayTill=explode(":",$daysHours[17]);

                                    if($dayActive=="false"){
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO,'');
                                    }else{
                                        $dateStart=$dayStart[1].$dayStart[2];
                                        $date24Start=$this->convertTo24Hours($dateStart);
                                        $dateTill=$dayTill[1].$dayTill[2];
                                        $date24Till=$this->convertTo24Hours($dateTill);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO,$date24Start."-".$date24Till);
                                    }
                                }

                                $cont++;
                            }
                        }
                    }
                }
            }
        }

        $organizations=$em->getRepository('App\Entity\Organization')->findBy(array('disabled'=>0,'organization_status'=>2));
        foreach ($organizations as $organization) {
            $company=$em->getRepository('App\Entity\Company')->find(1);
            $company_tin=$company->getTinNumber();
            $company_address=$company->getAddress();
            $company_name=$company->getName();

            $addresses = $em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
            $contF=0;

            if($addresses!=null){
                foreach ($addresses as $addr) {
                    if($addr->getIsFacility()==true){
                        $contF++;
                    }
                }
                if($contF>0){
                    $providers = $em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));
                    $contAssignedMMM=0;

                    if($providers!=null){
                        foreach ($providers as $provider){
                            $payers=$provider->getPayers();
                            if($payers!=null){
                                foreach ($payers as $payer){
                                    if($payer->getId()==1){
                                        $contAssignedMMM++;
                                    }
                                }
                            }
                        }
                    }

                    $org_have_mmm=false;
                    $org_payers=$organization->getPayers();
                    if($org_payers!=null){
                        foreach ($org_payers as $orgP){
                            if($orgP->getId()==1){
                                $org_have_mmm=true;
                            }
                        }
                    }

                    if($contAssignedMMM>0 or $org_have_mmm==true){
                            foreach ($addresses as $address){
                                $npi=$organization->getGroupNpi();
                                $mmm_log = $em->getRepository('App\Entity\MMMReportLog')->findBy(array('register_type' => 'facility','npi'=>$npi,'phone_number'=>$address->getPhoneNumber()));
                                $cause_change="";
                                $is_changed=false;

                                if($mmm_log!=null){
                                    foreach ($mmm_log as $mmm_l) {
                                        $last_name=$organization->getName();
                                        $street=$address->getStreet();
                                        $suite=$address->getSuiteNumber();
                                        $phone=$address->getPhoneNumber();

                                        $log_last_name=$mmm_l->getFirstName();
                                        if($log_last_name!=$last_name){
                                            $is_changed=true;
                                            $cause_change.="Organization Name changed, ";
                                        }

                                        $log_street=$mmm_l->getStreet();
                                        if($log_street!=$street){
                                            $is_changed=true;
                                            $cause_change.="Street changed, ";
                                        }

                                        $log_suite=$mmm_l->getSuite();
                                        if($log_suite!=$suite){
                                            $is_changed=true;
                                            $cause_change.="Suite Number changed, ";
                                        }

                                        $log_phone=$mmm_l->getPhoneNumber();
                                        if($log_phone!=$phone){
                                            $is_changed=true;
                                            $cause_change.="Phone Number changed, ";
                                        }
                                    }
                                }

                                if($is_changed==true) {
                                    $cell_A = 'A' . $cont;$cell_B = 'B' . $cont;$cell_C = 'C' . $cont;$cell_D = 'D' . $cont;$cell_M = 'M' . $cont;
                                    $cell_E = 'E' . $cont;$cell_F = 'F' . $cont;$cell_I = 'I' . $cont;$cell_L = 'L' . $cont;
                                    $cell_N = 'N' . $cont;$cell_O = 'O' . $cont;$cell_P = 'P' . $cont;$cell_Q = 'Q' . $cont;
                                    $cell_W = 'W' . $cont;$cell_X = 'X' . $cont;$cell_Y = 'Y' . $cont;$cell_Z = 'Z' . $cont;
                                    $cell_AA = 'AA' . $cont;$cell_AZ = 'AZ' . $cont;$cell_BB = 'BB' . $cont;$cell_BC = 'BC' . $cont;
                                    $cell_BV = 'BV' . $cont;$cell_BW = 'BW' . $cont;$cell_BX = 'BX' . $cont;$cell_BY = 'BY' . $cont;
                                    $cell_CB = 'CB' . $cont;$cell_AH = 'AH' . $cont;

                                    if ($address->getLocationNpi() != "" and $address->getLocationNpi() != null) {
                                        $locationNpi = $address->getLocationNpi();
                                    } else {
                                        $locationNpi = $address->getOrganization()->getGroupNpi();
                                    }
                                    $cause_change=substr($cause_change,0,-2);

                                    $email="";
                                    $emails=array();
                                    $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));
                                    if($contacts){
                                        foreach ($contacts as $contact){
                                            if($contact->getEmail()!=""){
                                                $emails[]=$contact->getEmail();
                                            }
                                        }
                                    }
                                    if(count($emails)>0){
                                        $email=$emails[0];
                                    }

                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, 'CHANGED');
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_B, $cause_change);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $company_name);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $date);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, 'PAR');
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $locationNpi);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $address->getOrganization()->getName());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, "Yes");
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, "No");
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_P, "Yes");
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_Q, "Yes");
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $email);

                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_W, $address->getOrganization()->getName());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $address->getOrganization()->getBillingNpi());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $address->getOrganization()->getTinNumber());

                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AA, $address->getOrganization()->getName());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AZ, "No");
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BB, "Yes");
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BC, "85%");

                                    $facilitiesTypes = $address->getFacilityType();
                                    $facilitiesTypesStr = "";
                                    if ($facilitiesTypes) {
                                        foreach ($facilitiesTypes as $ft) {
                                            $facilitiesTypesStr .= $ft->getName() . ",";
                                        }
                                        $facilitiesTypesStr = substr($facilitiesTypesStr, 0, -1);
                                    }

                                    $npi = $address->getOrganization()->getGroupNpi();
                                    $pdata = $em->getRepository('App\Entity\PDataMedicare')->findOneBy(array('npi_number' => $npi));
                                    $taxonomySpecialties = "";
                                    if ($pdata != null) {
                                        $json = $pdata->getData();
                                        $datos = json_decode($json, true);
                                        $taxonomies = $datos['results'][0]['taxonomies'];
                                        foreach ($taxonomies as $taxonomy) {
                                            if ($taxonomy['primary'] == 1) {
                                                $taxonomySpecialties = $taxonomy['desc'];
                                            }
                                        }
                                    }

                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $taxonomySpecialties);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AH, $this->AddressFax($address));
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BV, $company_name);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BW, $company_tin);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BX, $company_address);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_BY, "No");
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_CB, $this->AddressLanguages($address));
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z, $taxonomySpecialties);

                                    $cell_AB = 'AB' . $cont;
                                    $cell_AC = 'AC' . $cont;
                                    $cell_AD = 'AD' . $cont;
                                    $cell_AE = 'AE' . $cont;
                                    $cell_AF = 'AF' . $cont;
                                    $cell_AG = 'AG' . $cont;

                                    $cell_AI = 'AI' . $cont;
                                    $cell_AJ = 'AJ' . $cont;
                                    $cell_AK = 'AK' . $cont;
                                    $cell_AL = 'AL' . $cont;
                                    $cell_AM = 'AM' . $cont;
                                    $cell_AN = 'AN' . $cont;
                                    $cell_AO = 'AO' . $cont;
                                    $cell_CQ = 'CQ' . $cont;

                                    $cell_AP = 'AP' . $cont;
                                    $cell_AQ = 'AQ' . $cont;
                                    $cell_AR = 'AR' . $cont;
                                    $cell_AS = 'AS' . $cont;
                                    $cell_AT = 'AT' . $cont;
                                    $cell_AU = 'AU' . $cont;
                                    $cell_AV = 'AV' . $cont;

                                    if ($address->getBillingAddr() == true) {
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                        if ($address->getBillingAddr() == true) {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                                        } else {
                                            if ($address->getMailingAddr() == true) {
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                                            } else {
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $address->getStreet());
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $address->getCity());
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $address->getSuiteNumber());
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $address->getZipCode());
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $address->getUsState());
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AV, $this->AddressFax($address));
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $address->getPhoneNumber());
                                            }
                                        }
                                    }

                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AB, $address->getStreet());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AC, $address->getSuiteNumber());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AD, $address->getCity());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AE, $address->getUsState());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AF, $address->getZipCode());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AG, $address->getPhoneNumber());

                                    $cell_BA = 'BA' . $cont;

                                    $serviceSettings = $address->getServiceSettings();
                                    $serviceSettingStr = "";

                                    if (count($serviceSettings) > 0) {
                                        foreach ($serviceSettings as $serviceSetting) {
                                            $serviceSettingStr .= $serviceSetting->getName() . ",";
                                        }
                                        $serviceSettingStr = substr($serviceSettingStr, 0, -1);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, $serviceSettingStr);
                                    } else {
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, "");
                                    }

                                    $bussinesOurs = $address->getBusinessHours();
                                    if($bussinesOurs!=""){
                                        $bussinesOurs = substr($bussinesOurs, 1);
                                        $bussinesOurs = substr($bussinesOurs, 0, -1);

                                        $bussinesOurs = str_replace('{', '', $bussinesOurs);
                                        $bussinesOurs = str_replace('}', '', $bussinesOurs);
                                        $bussinesOurs = str_replace('"', '', $bussinesOurs);

                                        $daysHours = explode(",", $bussinesOurs);

                                        $dayActive = explode(":", $daysHours[18])[1];
                                        $dayStart = explode(":", $daysHours[19]);
                                        $dayTill = explode(":", $daysHours[20]);

                                        if ($dayActive == "false") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI, '');
                                        } else {
                                            $dateStart = $dayStart[1] . $dayStart[2];
                                            $date24Start = $this->convertTo24Hours($dateStart);
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI, $date24Start . "-" . $date24Till);
                                        }


                                        //monday
                                        $dayActive = explode(":", $daysHours[0])[1];
                                        $dayStart = explode(":", $daysHours[1]);
                                        $dayTill = explode(":", $daysHours[2]);

                                        if ($dayActive == "false") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ, '');
                                        } else {
                                            $dateStart = $dayStart[1] . $dayStart[2];
                                            $date24Start = $this->convertTo24Hours($dateStart);
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ, $date24Start . "-" . $date24Till);
                                        }

                                        //tuesday
                                        $dayActive = explode(":", $daysHours[3])[1];
                                        $dayStart = explode(":", $daysHours[4]);
                                        $dayTill = explode(":", $daysHours[5]);

                                        if ($dayActive == "false") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AK, '');
                                        } else {
                                            $dateStart = $dayStart[1] . $dayStart[2];
                                            $date24Start = $this->convertTo24Hours($dateStart);
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AK, $date24Start . "-" . $date24Till);
                                        }

                                        //wednesday
                                        $dayActive = explode(":", $daysHours[6])[1];
                                        $dayStart = explode(":", $daysHours[7]);
                                        $dayTill = explode(":", $daysHours[8]);

                                        if ($dayActive == "false") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AL, '');
                                        } else {
                                            $dateStart = $dayStart[1] . $dayStart[2];
                                            $date24Start = $this->convertTo24Hours($dateStart);
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AL, $date24Start . "-" . $date24Till);
                                        }

                                        //thuerday
                                        $dayActive = explode(":", $daysHours[9])[1];
                                        $dayStart = explode(":", $daysHours[10]);
                                        $dayTill = explode(":", $daysHours[11]);

                                        if ($dayActive == "false") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AM, '');
                                        } else {
                                            $dateStart = $dayStart[1] . $dayStart[2];
                                            $date24Start = $this->convertTo24Hours($dateStart);
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AM, $date24Start . "-" . $date24Till);
                                        }

                                        //friday
                                        $dayActive = explode(":", $daysHours[12])[1];
                                        $dayStart = explode(":", $daysHours[13]);
                                        $dayTill = explode(":", $daysHours[14]);

                                        if ($dayActive == "false") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN, '');
                                        } else {
                                            $dateStart = $dayStart[1] . $dayStart[2];
                                            $date24Start = $this->convertTo24Hours($dateStart);
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN, $date24Start . "-" . $date24Till);
                                        }

                                        //saturday
                                        $dayActive = explode(":", $daysHours[15])[1];
                                        $dayStart = explode(":", $daysHours[16]);
                                        $dayTill = explode(":", $daysHours[17]);

                                        if ($dayActive == "false") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO, '');
                                        } else {
                                            $dateStart = $dayStart[1] . $dayStart[2];
                                            $date24Start = $this->convertTo24Hours($dateStart);
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO, $date24Start . "-" . $date24Till);
                                        }
                                    }

                                    $cont++;
                                }
                            }
                    }
                }
            }
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="mmm-report-facility.xls"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');
        // Return a text response to the browser saying that the excel was succesfully created

        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/mmm/report-alf-ltc", name="mmm_report_alf_ltc")
     */
    public function reportALF_LTC(){
        set_time_limit(3600);
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');
        $date=date('m/d/Y');

        $spreadsheet = $reader->load($url.'mmm-ALF-LTC-template.xlsx');

        $spreadsheet->setActiveSheetIndex(0);
        $em=$this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        //line to start to write
        $cont=5;

        $sql="SELECT p.id, p.first_name, p.last_name,p.npi_number,
        GROUP_CONCAT(DISTINCT degree.`name` ORDER BY degree.id ASC SEPARATOR ', ') AS degrees,
        GROUP_CONCAT(DISTINCT specialty.`name` ORDER BY specialty.id ASC SEPARATOR ', ') AS specialties,
        organization.name as organization,
        GROUP_CONCAT(DISTINCT organization_contact.phone ORDER BY organization_contact.id ASC SEPARATOR ', ') AS phones
        FROM  provider p
        LEFT JOIN organization ON p.org_id = organization.id
        LEFT JOIN organization_contact ON organization_contact.organization_id = organization.id
        LEFT JOIN provider_degree ON p.id = provider_degree.provider_id
        LEFT JOIN degree ON degree.id = provider_degree.degree_id
        LEFT JOIN provider_primary_specialties ON p.id = provider_primary_specialties.provider_id
        LEFT JOIN specialty ON specialty.id = provider_primary_specialties.primary_specialty_id
        LEFT JOIN provider_billing_address ON p.id = provider_billing_address.provider_id
        LEFT JOIN billing_address ON billing_address.id = provider_billing_address.billing_address_id
        LEFT JOIN provider_payer ON p.id = provider_payer.provider_id
        LEFT JOIN payer ON payer.id = provider_payer.payer_id
        WHERE p.disabled=0 and (billing_address.county_match_id=80 or billing_address.county_match_id=73 or  billing_address.county_match_id=117) and payer.id=1 and
        p.interested_in_residential_services=1
        GROUP BY p.id";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $providers= $stmt->fetchAll();

        foreach ($providers as $provider){
            $providerObj=$em->getRepository('App\Entity\Provider')->find($provider['id']);

            $address=$providerObj->getBillingAddress();
            if($address!=null){
                foreach ($address as $addr){

                    if($addr->getCountyMatch()->getId()==80 or $addr->getCountyMatch()->getId()==73 or $addr->getCountyMatch()->getId()==117){
                        $cell_B = 'B' . $cont;
                        $cell_C = 'C' . $cont;
                        $cell_D = 'D' . $cont;
                        $cell_E = 'E' . $cont;
                        $cell_F = 'F' . $cont;
                        $cell_G = 'G' . $cont;
                        $cell_H = 'H' . $cont;
                        $cell_I = 'I' . $cont;
                        $cell_J = 'J' . $cont;
                        $cell_K = 'K' . $cont;
                        $cell_L = 'L' . $cont;
                        $cell_M = 'M' . $cont;

                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $provider['first_name']);
                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $provider['last_name']);
                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $provider['npi_number']);
                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $provider['degrees']);
                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $provider['specialties']);
                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $provider['organization']);
                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $provider['phones']);

                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $addr->getStreet());
                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $addr->getSuiteNumber());
                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $addr->getCity());
                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $addr->getCountyMatch()->getName());
                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_M, $addr->getZipCode());
                        $cont++;
                    }
                }
            }
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="MMM_ALF_LTC.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");

    }


}


