<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\ProviderUser;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\Bridge\Google\Smtp\GmailTransport;
use Symfony\Component\Mailer\Mailer;

/**
 * @Route("/admin/provideruser")
 */
class ProviderUserController extends AbstractController{
    /**
     * @Route("/index", name="admin_provideruser")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $providerusers = $em->getRepository('App\Entity\ProviderUser')->findAll();
            $delete_form_ajax = $this->createCustomForm('ROLE_ID', 'DELETE', 'admin_delete_provideruser');

            return $this->render('provideruser/index.html.twig', array('providerusers' => $providerusers, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_provideruser")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $roles = $em->getRepository('App\Entity\ProviderRole')->findAll();

            return $this->render('provideruser/add.html.twig', array('roles' => $roles));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_provideruser",defaults={"id": null} )
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ProviderUser')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_provideruser');
            }
            $roles = $em->getRepository('App\Entity\ProviderRole')->findAll();

            return $this->render('provideruser/edit.html.twig', array('document' => $document, 'roles' => $roles));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_provideruser",defaults={"id": null} )
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ProviderUser')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_provideruser');
            }

            return $this->render('provideruser/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_provideruser")
     */
    public function create(Request $request, UserPasswordEncoderInterface $encoder){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $organization_name=$request->get('organization_name');
            $lastname = $request->get('last_name');
            $provideruser_name = $request->get('provideruser_name');
            $email = $request->get('email');
            $account_non_expired = $request->get('account_non_expired');
            $credentials_non_expired = $request->get('credentials_non_expired');
            $account_non_locked = $request->get('account_non_locked');
            $enabled = $request->get('enabled');
            $password = $request->get('password');
            $roles = $request->get('roles');

            $new = $request->get('new');

            $provideruser = new ProviderUser();
            $provideruser->setFirstName($name);
            $provideruser->setOrganizationName($organization_name);
            $provideruser->setLastName($lastname);
            $provideruser->setEmail($email);
            $provideruser->setUsername($provideruser_name);

            $encode = $encoder->encodePassword($provideruser, $password);
            $provideruser->setPassword($encode);
            $provideruser->setEnabled($enabled);
            $provideruser->setAccountNonExpired($account_non_expired);
            $provideruser->setAccountNonLocked($account_non_locked);
            $provideruser->setCredentialsNonExpired($credentials_non_expired);

            if ($roles !== null or $roles !== "") {

                $rol = $em->getRepository('App\Entity\ProviderRole')->find($roles);
                if ($rol !== null) {
                    $provideruser->addRole($rol);
                }

            }

            $em->persist($provideruser);
            $em->flush();

            $this->addFlash(
                'success',
                'ProviderUser has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_provideruser');
            }

            return $this->redirectToRoute('admin_provideruser');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/registered", name="admin_registered_provideruser")
     */
    public function registered(Request $request, UserPasswordEncoderInterface $encoder, MailerInterface $mailer)
    {
        $em = $this->getDoctrine()->getManager();

        $name=$request->get('name');
        $lastname=$request->get('lastname');
        $password=$request->get('password');
        $email=$request->get('email');
        $role=$request->get('role');
        $organization_name=$request->get('organization_name');


        $user=new ProviderUser();
        $user->setFirstName($name);
        $user->setLastName($lastname);
        $user->setUsername($email);
        $user->setEmail($email);
        $user->setOrganizationName($organization_name);

        $user->setAccountNonLocked(true);
        $user->setCredentialsNonExpired(true);
        $user->setEnabled(true);
        $user->setAccountNonExpired(true);

        $encode= $encoder->encodePassword($user, $password);
        $user->setPassword($encode);

        if($role!=""){
            $rol=$em->getRepository('App\Entity\ProviderRole')->findOneBy(array('id'=>$role));

            if($rol!==null){
                $user->addRole($rol);
            }
        }

        $em->persist($user);
        $em->flush();

        //send notification email

        $html="";
        $email_to=$email;

        $html.='<h3>Congratulations!  You have taken the first step to join the BSN network.</h3>';
        $html.='<p>Click <a href="http://providers.bsnnet.com/provider/login">here</a> to go to the provider portal where you will complete the following:</p>';
        $html.='<ul>';
        $html.='<li>Contracting - complete the BSN agreement and download for signing. </li>';
        $html.='<li>Credentialing- follow the prompts to enter and or upload applicable documents.</li>';
        $html.='<li>Once complete, the provider portal is available to you to load rosters, update demographics and otherwise maintain your organizations account per CMS guidelines. </li>';
        $html.='</ul>';

        $html.='<p>For additional information contact BSN at 305-907-7470 or email us at <a href="mailto:info@bsnnet.com">info@bsnnet.com</a></p>';

        return $this->redirectToRoute('check_email_registration');
    }


    /**
     * @Route("/user-registered", name="admin_registered_provideruser_new")
     */
    public function registered2(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $em = $this->getDoctrine()->getManager();

        $name=$request->get('name');
        $lastname=$request->get('lastname');
        $password=$request->get('password');
        $email=$request->get('email');
        $organization=$request->get('organization');
        $orgObj=$em->getRepository('App\Entity\Organization')->find($organization);

        $user=new User();

        $user->setName($name);
        $user->setLastName($lastname);
        $user->setUsername($email);
        $user->setEmail($email);
        $user->setOrganization($orgObj);

        $user->setAccountNonLocked(true);
        $user->setCredentialsNonExpired(true);
        $user->setEnabled(true);
        $user->setAccountNonExpired(true);

        $encode= $encoder->encodePassword($user, $password);
        $user->setPassword($encode);

        $rol=$em->getRepository('App\Entity\Role')->find(3);

        if($rol!==null){
           $user->addRole($rol);
        }

        $em->persist($user);
        $em->flush();

        //send notification email



        return $this->redirectToRoute('check_email_registration_new');
    }


    /**
     * @Route("/update_che", name="check_email_registration")
     */
    public function check_email_registration(){

        return $this->render('provideruser/register_completed.html.twig');
    }

    /**
     * @Route("/user-register-completed", name="check_email_registration_new")
     */
    public function check_email_registration_new (){

        return $this->render('provideruser/register_completed2.html.twig');
    }

    /**
     * @Route("/update", name="admin_update_provideruser")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $db = $em->getConnection();

            $name = $request->get('name');
            $lastname = $request->get('last_name');
            $user_name = $request->get('provideruser_name');
            $email = $request->get('email');
            $account_non_expired = $request->get('account_non_expired');
            $credentials_non_expired = $request->get('credentials_non_expired');
            $account_non_locked = $request->get('account_non_locked');
            $enabled = $request->get('enabled');
            $role = $request->get('role');
            $id = $request->get('id');

            $new = $request->get('new');

            $provideruser = $em->getRepository('App\Entity\ProviderUser')->find($id);

            if ($provideruser == null) {
                $this->addFlash(
                    "danger",
                    "The Provider User can't been updated!"
                );

                return $this->redirectToRoute('admin_provideruser');
            }

            if ($provideruser != null) {
                $provideruser->setFirstName($name);
                $provideruser->setUsername($user_name);
                $provideruser->setLastName($lastname);
                $provideruser->setEmail($email);
                $provideruser->setEnabled($enabled);
                $provideruser->setAccountNonExpired($account_non_expired);
                $provideruser->setAccountNonLocked($account_non_locked);
                $provideruser->setCredentialsNonExpired($credentials_non_expired);

                $sqlDelete = "DELETE FROM `provideruser_roles` WHERE `provideruser_roles`.`user_id` = $id";
                $stmt = $db->prepare($sqlDelete);
                $stmt->execute();

                $rol = $em->getRepository('App\Entity\ProviderRole')->find($role);
                if ($rol !== null) {
                    $provideruser->addRole($rol);
                }

                $em->persist($provideruser);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "Provider User has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_provideruser');
            }

            return $this->redirectToRoute('admin_provideruser');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_provideruser",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $provideruser = $provideruser = $em->getRepository('App\Entity\ProviderUser')->find($id);
            $removed = 0;
            $message = "";

            if ($provideruser) {
                try {
                    $em->remove($provideruser);
                    $em->flush();
                    $removed = 1;
                    $message = "The Provider User has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Provider User can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_provideruser",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $provideruser = $provideruser = $em->getRepository('App\Entity\ProviderUser')->find($id);

                if ($provideruser) {
                    try {
                        $em->remove($provideruser);
                        $em->flush();
                        $removed = 1;
                        $message = "The Provider User has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Provider User can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }



}
