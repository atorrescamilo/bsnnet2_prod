<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\ProviderType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/providertype")
 */
class ProviderTypeController extends AbstractController{
    /**
     * @Route("/index", name="admin_providertype")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $providertypes = $em->getRepository('App\Entity\ProviderType')->findBy(array(),array('name'=>'ASC'));

            $delete_form_ajax = $this->createCustomForm('PROVIDERTYPE_ID', 'DELETE', 'admin_delete_providertype');


            return $this->render('providertype/index.html.twig', array('providertypes' => $providertypes, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_providertype")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $specialties=$em->getRepository('App\Entity\Specialty')->findBy(array(),array('name'=>'ASC'));

            return $this->render('providertype/add.html.twig',['specialties'=>$specialties]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_providertype", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ProviderType')->find($id);

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('admin_providertype');
            }

            return $this->render('providertype/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_providertype", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ProviderType')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_providertype');
            }

            return $this->render('providertype/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_providertype")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $code = $request->get('code');
            $description = $request->get('description');
            $new = $request->get('new');

            try {
                $providertype = new ProviderType();
                $providertype->setName($name);
                $providertype->setCode($code);
                $providertype->setDescription($description);
                $em->persist($providertype);
                $em->flush();
                $this->addFlash(
                    'success',
                    'Provider Type has been saved successfully!'
                );

            } catch (PDOException $ex) {
                $this->addFlash(
                    'danger',
                    "ProviderType can't be added"
                );
                return $this->redirectToRoute('admin_new_providertype');
            }

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_providertype');
            }

            return $this->redirectToRoute('admin_providertype');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }



    /**
     * @Route("/update", name="admin_update_providertype")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $code = $request->get('code');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $providertype = $em->getRepository('App\Entity\ProviderType')->find($id);

            if ($providertype == null) {
                $this->addFlash(
                    "danger",
                    "The Provider Type can't been updated!"
                );

                return $this->redirectToRoute('admin_providertype');
            }

            if ($providertype != null) {
                $providertype->setName($name);
                $providertype->setCode($code);
                $providertype->setDescription($description);

                $em->persist($providertype);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "The Provider Type has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_providertype');
            }

            return $this->redirectToRoute('admin_providertype');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_providertype",methods={"POST","DELETE"})
     */
    public function delete(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $providertype = $providertype = $em->getRepository('App\Entity\ProviderType')->find($id);
            $removed = 0;
            $message = "";

            if ($providertype) {
                try {
                    $em->remove($providertype);
                    $em->flush();
                    $removed = 1;
                    $message = "The Provider Type has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Provider Type can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/isUnique", name="admin_isunique_providertype",methods={"POST"})
     */
    public function isUnique(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $code = $request->get('code');

            $providertype = $em->getRepository('App\Entity\ProviderType')->findOneBy(array('code' => $code));

            $isUnique = 0;
            if ($providertype != null) {
                $isUnique = 1;
            }

            return new Response(
                json_encode(array('isUnique' => $isUnique)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_providertype",methods={"POST","DELETE"})
     */
    public function deleteMultiple(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $providertype = $providertype = $em->getRepository('App\Entity\ProviderType')->find($id);

                if ($providertype) {
                    try {
                        $em->remove($providertype);
                        $em->flush();
                        $removed = 1;
                        $message = "The Provider Type has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Provider Type can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
