<?php

namespace App\Controller;

use App\Entity\AETFHKCMedicaidData;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/report")
 */
class AETNAExportReportController extends AbstractController
{

    /**
     * @Route("/aetna-pnv-export", name="aetna_pnv_report_export")
     */
    public function pnv_export(){
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');
        $date=date('m/d/Y');

        $spreadsheet = $reader->load($url.'PNV_Template.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $em=$this->getDoctrine()->getManager();
        $cont=2;

        //get all records from FCCData for providers for PG
        $providers_data=$em->getRepository('App:AETData')->findAll();

        foreach ($providers_data as $datum){
            $cell_A = 'A' . $cont;$cell_B = 'B' . $cont;
            $cell_C = 'C' . $cont;$cell_D = 'D' . $cont;
            $cell_E = 'E' . $cont;$cell_F = 'F' . $cont;
            $cell_G = 'G' . $cont;$cell_H = 'H' . $cont;
            $cell_I = 'I' . $cont;$cell_J = 'J' . $cont;
            $cell_K = 'K' . $cont;$cell_M = 'M' . $cont;
            $cell_N = 'N' . $cont;$cell_O = 'O' . $cont;

            $data=$datum->getData();
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, strval($data['record_tracking_number']));
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $data['provider_id']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, strval($data['first_name']));
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $data['last_name']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $data['license_number']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $data['ssn_fein']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $data['npi_number']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $data['start_date']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $data['end_date']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $data['provider_type']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $data['primary_specialty']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_M, $data['gender']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $data['provider_application_receipt_date']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_O, $data['credentialed_date']);

            $cont++;
        }

        //get all records from FCCData for organizations for PG
        $organizations_data=$em->getRepository('App:AETDataOrg')->findAll();
        foreach ($organizations_data as $datum){
            $cell_A = 'A' . $cont;$cell_B = 'B' . $cont;
            $cell_C = 'C' . $cont;$cell_D = 'D' . $cont;
            $cell_E = 'E' . $cont;$cell_F = 'F' . $cont;
            $cell_G = 'G' . $cont;$cell_H = 'H' . $cont;
            $cell_I = 'I' . $cont;$cell_J = 'J' . $cont;
            $cell_K = 'K' . $cont;$cell_M = 'M' . $cont;
            $cell_N = 'N' . $cont;$cell_O = 'O' . $cont;
            $cell_L = 'L' . $cont;

            $data=$datum->getData();

            $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $data['record_tracking_number']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $data['provider_id']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $data['first_name']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $data['last_name']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $data['license_number']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $data['ssn_fein']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $data['npi_number']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $data['start_date']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $data['end_date']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $data['provider_type']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $data['primary_specialty']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_M, $data['gender']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $data['provider_application_receipt_date']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_O, $data['credentialed_date']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $data['ahca_id']);

            $cont++;
        }

        $spreadsheet->setActiveSheetIndex(1);

        $cont2=2;
        $all_sl_records=$em->getRepository('App:AETDataSL')->findAll();
        $plan_medicaid="100120306~100120307~100120311";

        if($all_sl_records){
            foreach ($all_sl_records as $item){
                $cell_A = 'A' . $cont2;$cell_B = 'B' . $cont2;$cell_C = 'C' . $cont2;$cell_H = 'H' . $cont2;
                $cell_G = 'G' . $cont2;$cell_I = 'I' . $cont2;$cell_J = 'J' . $cont2;$cell_K = 'K' . $cont2;
                $cell_L = 'L' . $cont2;$cell_M = 'M' . $cont2;$cell_O = 'O' . $cont2;$cell_Q = 'Q' . $cont2;
                $cell_R = 'R' . $cont2;$cell_S = 'S' . $cont2;$cell_T = 'T' . $cont2;$cell_U = 'U' . $cont2;
                $cell_V = 'V' . $cont2;$cell_AA = 'AA' . $cont2;$cell_E = 'E' . $cont2;$cell_Y = 'Y' . $cont2;
                $cell_N = 'N' . $cont2;$cell_W = 'W' . $cont2;$cell_X = 'X' . $cont2;$cell_Z = 'Z' . $cont2;
                $cell_AF = 'AF' . $cont2;$cell_F = 'F' . $cont2;$cell_AG = 'AG' . $cont2;$cell_D='D'.$cont2;

                $data=$item->getData();

                $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, $data['col_a']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_B, $plan_medicaid);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, strval($data['col_c']));
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $data['col_d']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, $data['col_e']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $data['col_f']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_G, $data['col_g']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_H, $data['col_h']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $data['col_i']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $data['col_j']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $data['col_k']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $data['col_l']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $data['col_m']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, $data['col_n']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, $data['col_o']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Q, $data['col_q']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_R, $data['col_r']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_S, $data['col_s']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_T, $data['col_t']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_U, $data['col_u']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_V, $data['col_v']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_W, $data['col_w']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $data['col_x']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $data['col_y']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z, $data['col_z']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AF, $data['col_af']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AA, strval($data['col_aa']));
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AG, $data['col_ag']);

                $cont2++;
            }
        }

        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="AETNA_BSN_PNV_'.date('Ymd').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/aetna-fhkc-export", name="aetna_fhkc_report_export")
     */
    public function fhkc_export(){
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');
        $date=date('m/d/Y');

        $spreadsheet = $reader->load($url.'fhkc_template.xlsx');
        $spreadsheet->setActiveSheetIndex(1);
        $em=$this->getDoctrine()->getManager();
        $cont=2;

        $providers_data=$em->getRepository('App:AETFHKCMedicaidData')->findAll();

        foreach ($providers_data as $item){
            $cell_A = 'A' . $cont;$cell_B = 'B' . $cont;$cell_C = 'C' . $cont;
            $cell_D = 'D' . $cont;$cell_E = 'E' . $cont;$cell_F = 'F' . $cont;
            $cell_G = 'G' . $cont;$cell_H = 'H' . $cont;$cell_I = 'I' . $cont;
            $cell_J = 'J' . $cont;$cell_K = 'K' . $cont;$cell_L = 'L' . $cont;
            $cell_M = 'M' . $cont;$cell_N = 'N' . $cont;$cell_O = 'O' . $cont;
            $cell_P = 'P' . $cont;$cell_Q = 'Q' . $cont;$cell_R = 'R' . $cont;
            $cell_S = 'S' . $cont;$cell_T = 'T' . $cont;$cell_U = 'U' . $cont;
            $cell_V = 'V' . $cont;$cell_W = 'W' . $cont;$cell_X = 'X' . $cont;
            $cell_Y = 'Y' . $cont;$cell_Z = 'Z' . $cont;$cell_AA= 'AA' . $cont;
            $cell_AB = 'AB' . $cont;$cell_AC = 'AC' . $cont;$cell_AD = 'AD' . $cont;
            $cell_AE = 'AE' . $cont;$cell_AF = 'AF' . $cont;$cell_AG = 'AG' . $cont;
            $cell_AH = 'AH' . $cont;$cell_AI = 'AI' . $cont;$cell_AJ = 'AJ' . $cont;
            $cell_AK = 'AK' . $cont;$cell_AL = 'AL' . $cont;$cell_AM = 'AM' . $cont;
            $cell_AN = 'AN' . $cont;$cell_AO = 'AO' . $cont;$cell_AP = 'AP' . $cont;$cell_AQ = 'AQ' . $cont;

            $data=$item->getData();
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, $data['col_a']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_B, $data['col_b']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $data['col_c']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $data['col_d']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, $data['col_e']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $data['col_f']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_G, $data['col_g']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_H, $data['col_h']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $data['col_i']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $data['col_j']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $data['col_k']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $data['col_l']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $data['col_m']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, $data['col_n']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, $data['col_o']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_P, $data['col_p']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Q, $data['col_q']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_R, $data['col_r']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_S, $data['col_s']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_T, $data['col_t']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_U, $data['col_u']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_V, $data['col_v']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_W, $data['col_w']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $data['col_x']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $data['col_y']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z, $data['col_z']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AA, $data['col_aa']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AB, $data['col_ab']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AC, $data['col_ac']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AD, $data['col_ad']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AE, $data['col_ae']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AF, $data['col_af']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AG, $data['col_ag']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AH, $data['col_ah']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AI, $data['col_ai']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AJ, $data['col_aj']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AK, $data['col_ak']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AL, $data['col_al']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AM, $data['col_am']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN, $data['col_an']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO, $data['col_ao']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $data['col_ap']);
            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $data['col_aq']);

            $cont++;
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="FHKC_Centralized_Provider_Directory_Layout_Interoperability_API_(002)_'.date('Ymd').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/aetna-pay-export", name="aetna_pay_report_export")
     */
    public function pay_export(){
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');

        $spreadsheet = $reader->load($url.'aetan_payreport_template.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $em=$this->getDoctrine()->getManager();

        $records=$em->getRepository('App:AETPayData')->findAll();

        $cont=3;
        if($records){
            foreach ($records as $record){

                $cell_A = 'A' . $cont;$cell_B = 'B' . $cont;$cell_C = 'C' . $cont;$cell_D = 'D' . $cont;$cell_E = 'E' . $cont;
                $cell_F = 'F' . $cont;$cell_G = 'G' . $cont;$cell_H = 'H' . $cont;$cell_I = 'I' . $cont;$cell_J = 'J' . $cont;
                $cell_L = 'L' . $cont;$cell_M = 'M' . $cont;$cell_R = 'R' . $cont;$cell_S = 'S' . $cont;$cell_X = 'X' . $cont;
                $cell_Y = 'Y' . $cont;$cell_AD = 'AD' . $cont;$cell_AE = 'AE' . $cont;$cell_AO = 'AO' . $cont;$cell_AN = 'AN' . $cont;
                $cell_AP = 'AP' . $cont;$cell_AR = 'AR' . $cont;$cell_AS = 'AS' . $cont;$cell_AT = 'AT' . $cont;$cell_AU = 'AU' . $cont;
                $cell_AX = 'AX' . $cont;$cell_AY = 'AY' . $cont;$cell_AZ = 'AZ' . $cont;$cell_BA = 'BA' . $cont;$cell_BB = 'BB' . $cont;
                $cell_BC = 'BC' . $cont;$cell_BD = 'BD' . $cont;$cell_BE = 'BE' . $cont;$cell_BF = 'BF' . $cont;$cell_BG = 'BG' . $cont;
                $cell_BH = 'BH' . $cont;$cell_BI = 'BI' . $cont;$cell_BJ = 'BJ' . $cont;$cell_BK = 'BK' . $cont;$cell_BL = 'BL' . $cont;
                $cell_BM = 'BM' . $cont;$cell_BN = 'BN' . $cont;$cell_BO = 'BO' . $cont;$cell_BP = 'BP' . $cont;$cell_BR = 'BR' . $cont;
                $cell_BS = 'BS' . $cont;$cell_BT = 'BT' . $cont;$cell_BU = 'BU' . $cont;$cell_BW = 'BW' . $cont;$cell_BX = 'BX' . $cont;
                $cell_BY = 'BY' . $cont;$cell_BZ = 'BZ' . $cont;$cell_CA = 'CA' . $cont;$cell_CB = 'CB' . $cont;$cell_CC = 'CC' . $cont;
                $cell_CN = 'CN' . $cont;$cell_CO = 'CO' . $cont;$cell_CP = 'CP' . $cont;$cell_CQ = 'CQ' . $cont;$cell_CR = 'CR' . $cont;
                $cell_CS = 'CS' . $cont;$cell_CT = 'CT' . $cont;$cell_CU = 'CU' . $cont;$cell_CV = 'CV' . $cont;$cell_CW = 'CW' . $cont;
                $cell_CX = 'CX' . $cont;$cell_CY = 'CY' . $cont;$cell_CZ = 'CZ' . $cont;$cell_DA = 'DA' . $cont;$cell_DB = 'DB' . $cont;
                $cell_DC = 'DC' . $cont;$cell_DD = 'DD' . $cont;$cell_DE = 'DE' . $cont;

                $cell_DO = 'DO' . $cont;
                $cell_DP = 'DP' . $cont;
                $cell_DQ = 'DQ' . $cont;
                $cell_DR = 'DR' . $cont;
                $cell_DS = 'DS' . $cont;
                $cell_DT = 'DT' . $cont;
                $cell_DU = 'DU' . $cont;
                $cell_DN = 'DN' . $cont;
                $cell_BQ = 'BQ' . $cont;
                $cell_AQ = 'AQ' . $cont;


                $data=$record->getData();
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, $data['col_a']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_B, $data['col_b']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $data['col_c']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $data['col_d']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, $data['col_e']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $data['col_f']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_G, $data['col_g']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_H, $data['col_h']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $data['col_i']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $data['col_j']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $data['col_l']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $data['col_m']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_R, $data['col_r']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_S, $data['col_s']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $data['col_x']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $data['col_y']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AD, $data['col_ad']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AE, $data['col_ae']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AN, $data['col_an']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AP, $data['col_ap']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AQ, $data['col_aq']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AR, $data['col_ar']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AS, $data['col_as']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AT, $data['col_at']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AU, $data['col_au']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AX, $data['col_ax']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AY, $data['col_ay']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AZ, $data['col_az']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AO, $data['col_ao']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BA, $data['col_ba']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BB, $data['col_bb']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BC, $data['col_bc']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BD, $data['col_bd']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BE, $data['col_be']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BF, $data['col_bf']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BG, $data['col_bg']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BH, $data['col_bh']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BI, $data['col_bi']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BJ, $data['col_bj']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BK, $data['col_bk']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BL, $data['col_bl']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BM, $data['col_bm']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BN, $data['col_bn']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BO, $data['col_bo']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BP, $data['col_bp']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BR, $data['col_br']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BS, $data['col_bs']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BT, $data['col_bt']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BU, $data['col_bu']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BW, $data['col_bw']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BX, $data['col_bx']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BY, $data['col_by']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BZ, $data['col_bz']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CA, $data['col_ca']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CB, $data['col_cb']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CC, $data['col_cc']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CN, $data['col_cn']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CO, $data['col_co']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CP, $data['col_cp']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CQ, $data['col_cq']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CR, $data['col_cr']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CS, $data['col_cs']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CT, $data['col_ct']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CU, $data['col_cu']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CV, $data['col_cv']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CW, $data['col_cw']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CX, $data['col_cx']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CY, $data['col_cy']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_CZ, $data['col_cz']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DA, $data['col_da']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DB, $data['col_db']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DC, $data['col_dc']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DD, $data['col_dd']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DE, $data['col_de']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DO, $data['col_do']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DP, $data['col_dp']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DQ, $data['col_dq']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DS, $data['col_ds']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DT, $data['col_dt']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DU, $data['col_du']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_DN, $data['col_dn']);
                $spreadsheet->getActiveSheet(1)->setCellValue($cell_BQ, $data['col_bq']);


                $cont++;
            }
        }


        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Aetna_Pay_Report_'.date('Ymd').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }


}
