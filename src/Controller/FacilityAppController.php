<?php

namespace App\Controller;

use App\Entity\FacilityApp;
use App\Entity\FacilityAppLocation;
use App\Repository\FacilityAppRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * @Route("/provider/facility_app")
 */
class FacilityAppController extends AbstractController{


    /**
     * @Route("/data", name="provider_new_facility_app")
     */
    public function formData(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $user=$this->getUser();
            $document=$em->getRepository('App\Entity\FacilityApp')->findOneBy(array('provider_user'=>$user));

            $facilityTypes = $em->getRepository('App\Entity\FacilityType')->findAll(array('name' => 'ASC'));
            $additionalServices = $em->getRepository('App\Entity\AdditionalService')->findAll(array('name' => 'ASC'));
            $legalTypes = $em->getRepository('App\Entity\LegalType')->findAll(array('name' => 'ASC'));
            $ustates = $em->getRepository('App\Entity\UsState')->findAll(array('name' => 'ASC'));

            $facilityLocations=$em->getRepository('App\Entity\facilityAppLocation')->findBy(array('user'=>$user->getId()));

            return $this->render('facility_app/add.html.twig', array('facilityTypes' => $facilityTypes, 'additionalServices' => $additionalServices, 'legalTypes' => $legalTypes,
                'ustates' => $ustates,'document'=>$document,'facilityLocations'=>$facilityLocations));
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/view_ajax_facility_location", name="view_ajax_facility_location")
     */
    public function viewAjax(Request $request){

            $em = $this->getDoctrine()->getManager();
            $id=$request->get('id');

            $facilityLocationObj=$em->getRepository('App\Entity\FacilityAppLocation')->find($id);

            $facilityLocationResult=array();
            if($facilityLocationObj){
                $facilityLocationResult['street']=$facilityLocationObj->getStreet1();
                $facilityLocationResult['street2']=$facilityLocationObj->getStreet2();
                $facilityLocationResult['us_state']="";

                if($facilityLocationObj->getUsState()){
                    $facilityLocationResult['us_state']=$facilityLocationObj->getUsState()->getName();
                    $facilityLocationResult['us_state_id']=$facilityLocationObj->getUsState()->getId();
                }

                $facilityLocationResult['city']=$facilityLocationObj->getCity();
                $facilityLocationResult['zip_code']=$facilityLocationObj->getZipCode();
                $facilityLocationResult['separate']=$facilityLocationObj->getIsSeparate();
            }

            return new Response(
                json_encode(array('facilityLocationResult'=>$facilityLocationResult)), 200, array('Content-Type' => 'application/json')
            );

    }


    /**
     * @Route("/update_facility_location", name="update_facility_location")
     */
    public function updateFacilityLocation(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $user=$this->getUser();

            $id=$request->get('id');
            $street=$request->get('street');
            $street2=$request->get('street2');
            $us_state=$request->get('us_state');
            $city=$request->get('city');
            $zip_code=$request->get('zip_code');
            $separate_billing_address=$request->get('separate_billing_address');

            $facilityAppLocation=$em->getRepository('App\Entity\FacilityAppLocation')->find($id);

            $facilityAppLocation->setStreet1($street);
            $facilityAppLocation->setStreet2($street2);
            $facilityAppLocation->setCity($city);
            $facilityAppLocation->setZipCode($zip_code);
            $facilityAppLocation->setIsSeparate($separate_billing_address);

            if($us_state!=""){
                $usStateObj=$em->getRepository('App\Entity\UsState')->find($us_state);
                if($usStateObj){
                    $facilityAppLocation->setUsState($usStateObj);
                }
            }

            $facilityAppLocation->setUser($user);

            $em->persist($facilityAppLocation);
            $em->flush();

            $facilityApp=array();

            $facilityApp['street']=$street;
            $facilityApp['street2']=$street2;
            $facilityApp['us_state']="";
            if($us_state!=""){
                $facilityApp['us_state']=$usStateObj->getName();
            }

            $facilityApp['city']=$city;
            $facilityApp['zip_code']=$zip_code;
            $facilityApp['separate']=$separate_billing_address;

            return new Response(
                json_encode(array('facilityApp'=>$facilityApp,'id'=>$facilityAppLocation->getId())), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/save_facility_location", name="save_facility_location")
    */
    public function saveFacilityLocation(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $user=$this->getUser();

            $inserted=true;

            $street=$request->get('street');
            $street2=$request->get('street2');
            $us_state=$request->get('us_state');
            $city=$request->get('city');
            $zip_code=$request->get('zip_code');
            $separate_billing_address=$request->get('separate_billing_address');

            $facilityAppLocation=new FacilityAppLocation();

            $facilityAppLocation->setStreet1($street);
            $facilityAppLocation->setStreet2($street2);
            $facilityAppLocation->setCity($city);
            $facilityAppLocation->setZipCode($zip_code);
            $facilityAppLocation->setIsSeparate($separate_billing_address);

            if($us_state!=""){
              $usStateObj=$em->getRepository('App\Entity\UsState')->find($us_state);
              if($usStateObj){
                  $facilityAppLocation->setUsState($usStateObj);
              }
            }

            $facilityAppLocation->setUser($user);

            $em->persist($facilityAppLocation);
            $em->flush();

            $facilityApp=array();

            $facilityApp['street']=$street;
            $facilityApp['street2']=$street2;
            $facilityApp['us_state']="";

            if($us_state!=0){
                $facilityApp['us_state']=$usStateObj->getName();
            }

            $facilityApp['city']=$city;
            $facilityApp['zip_code']=$zip_code;
            $facilityApp['separate']=$separate_billing_address;

            return new Response(
                json_encode(array('facilityApp'=>$facilityApp,'inserted' => $inserted,'id'=>$facilityAppLocation->getId())), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/activeFacilityAppLink", name="provider_active_facilityapp_link")
     *
    */
    public function isActiveLinkFacilityAoo(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $user=$this->getUser();

            //get a facility address for this user
            $address=$em->getRepository('App\Entity\BillingAddressP')->findBy(array('user'=>$user->getId(),'is_facility'=>1));

            $cont_address=count($address);

            return $this->render('facility_app/link_facility_app.html.twig',array('cont_address'=>$cont_address));
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/activeFacilityAppButton", name="provider_active_facilityapp_Button")
     *
     */
    public function isActiveButtonFacilityAoo(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $user=$this->getUser();

            //get a facility address for this user
            $address=$em->getRepository('App\Entity\BillingAddressP')->findBy(array('user'=>$user->getId(),'is_facility'=>1));

            $cont_address=count($address);

            return $this->render('facility_app/link_facility_app.html.twig',array('cont_address'=>$cont_address));
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }


    /**
     * @Route("/save_data", name="provider_facillity_app_save_data")
     *
     */
    public function saveData(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $user=$this->getUser();

            $facility_name = $request->get('facility_name');
            $facility_dba = $request->get('facility_dba');
            $facility_npi = $request->get('facility_npi');
            $tax_id_number = $request->get('tax_id_number');
            $facility_medicare = $request->get('facility_medicare');
            $facility_medicaid = $request->get('facility_medicaid');
            $contact_name = $request->get('contact_name');
            $contact_last_name = $request->get('contact_last_name');
            $contact_email = $request->get('contact_email');
            $contact_phone = $request->get('contact_phone');
            $contact_fax = $request->get('contact_fax');
            $facility_type = $request->get('facility_type'); //id
            $other_additional_service = $request->get('other_additional_service');
            $facility_owner = $request->get('facility_owner');
            $legal_type = $request->get('legal_type'); //id
            $licensed_by_state = $request->get('licensed_by_state');
            $licensing_us_state = $request->get('licensing_us_state'); //id
            $state_license_number = $request->get('state_license_number');
            $state_license_expiration_date = $request->get('state_license_expiration_date');
            $licensed_by_city_county = $request->get('licensed_by_city_county');
            $licensing_city_county = $request->get('licensing_city_county');
            $licensing_city_county_number = $request->get('licensing_city_county_number');
            $license_expiration_city_date = $request->get('license_expiration_city_date');
            $facility_accredited = $request->get('facility_accredited');
            $accrediting_body = $request->get('accrediting_body');
            $date_last_accreditation = $request->get('date_last_accreditation');
            $accreditation_expiration_date = $request->get('accreditation_expiration_date');
            $facility_site_survey = $request->get('facility_site_survey');
            $site_survey_conducted = $request->get('site_survey_conducted');
            $medicare_medicaid_sanctions = $request->get('medicare_medicaid_sanctions');
            $medicare_medicaid_sanctions_more_context = $request->get('medicare_medicaid_sanctions_more_context');
            $issuing_insurance_agency = $request->get('issuing_insurance_agency');
            $policy_number = $request->get('policy_number');
            $single_occurrence_amount = $request->get('single_occurrence_amount');
            $aggregate_amount = $request->get('aggregate_amount');
            $issued_date = $request->get('issued_date');
            $expiration_date = $request->get('expiration_date');
            $convictions_under_federal = $request->get('convictions_under_federal');
            $context_convictions_under_federal = $request->get('context_convictions_under_federal');
            $federal_sanstions_limitations = $request->get('federal_sanstions_limitations');
            $contect_federal_sanstions_limitations = $request->get('contect_federal_sanstions_limitations');
            $additionalServicesSelected=$request->get('additionalServicesSelected');
            $authorized_agent_first_name=$request->get('authorized_agent_first_name');
            $authorized_agent_last_name=$request->get('authorized_agent_last_name');
            $date_of_signature=$request->get('date_of_signature');

            $facillityApp=$em->getRepository('App\Entity\FacilityApp')->findOneBy(array('provider_user'=>$user->getId()));

            if($facillityApp==null){
                $facillityApp = new FacilityApp();
            }else{
                $updatedDate = date('m/d/Y H:i:s');
                $facillityApp->setUpdatedOn($updatedDate);
            }

            $facillityApp->setAuthorizedAgentFirstName($authorized_agent_first_name);
            $facillityApp->setAuthorizedAgentLastName($authorized_agent_last_name);
            $facillityApp->setDateSignature($date_of_signature);
            $facillityApp->setProviderUser($user);
            $facillityApp->setName($facility_name);
            $facillityApp->setFacilityDba($facility_dba);
            $facillityApp->setNpiNumber($facility_npi);
            $facillityApp->setTaxIdentification($tax_id_number);
            $facillityApp->setMedicareNumber($facility_medicare);
            $facillityApp->setMedicaidNumber($facility_medicaid);

            $facillityApp->setContactName($contact_name);
            $facillityApp->setContactLastName($contact_last_name);
            $facillityApp->setContactEmail($contact_email);
            $facillityApp->setContactPhoneNumber($contact_phone);
            $facillityApp->setContactFax($contact_fax);

            $facilityTypeObj=$em->getRepository('App\Entity\FacilityType')->find($facility_type);
            if($facilityTypeObj){
                $facillityApp->setFacilityType($facilityTypeObj);
            }

            $facillityApp->setOtherAdditionalService($other_additional_service);
            $facillityApp->setFacilityOwner($facility_owner);

            $legalTypeObj=$em->getRepository('App\Entity\LegalType')->find($legal_type);

            if($legalTypeObj){
                $facillityApp->setLegalType($legalTypeObj);
            }

            $facillityApp->setLicensedByState($licensed_by_state);

            $stateUsStateObj=$em->getRepository('App\Entity\UsState')->find($licensing_us_state);

            if($stateUsStateObj){
                $facillityApp->setLicensingState($stateUsStateObj);
            }

            $facillityApp->setStateLicenseNumber($state_license_number);
            $facillityApp->setStateLicenseExpirationDate($state_license_expiration_date);
            $facillityApp->setLicenseByCity($licensed_by_city_county);
            $facillityApp->setCityLicenseNumber($licensing_city_county_number);
            $facillityApp->setLicenseCityCounty($licensing_city_county);
            $facillityApp->setCityLicenseExpirationDate($license_expiration_city_date);
            $facillityApp->setIsAccredited($facility_accredited);
            $facillityApp->setAccreditationBody($accrediting_body);
            $facillityApp->setDateLastAccreditation($date_last_accreditation);
            $facillityApp->setAccreditationExpirationDate($accreditation_expiration_date);
            $facillityApp->setHaveSiteSurvey($facility_site_survey);
            $facillityApp->setDateRecentSurveyConducted($site_survey_conducted);
            $facillityApp->setSactionLast3Year($medicare_medicaid_sanctions);
            $facillityApp->setAccreditationMoreContext($medicare_medicaid_sanctions_more_context);
            $facillityApp->setInsuranceAgency($issuing_insurance_agency);
            $facillityApp->setPolicyNumber($policy_number);
            $facillityApp->setSingleOccurrenceAmount($single_occurrence_amount);
            $facillityApp->setAggredateAmount($aggregate_amount);
            $facillityApp->setIssuesDate($issued_date);
            $facillityApp->setExpirationDate($expiration_date);
            $facillityApp->setConvictionsUnderFederal($convictions_under_federal);
            $facillityApp->setContextConvictionsUnderFederal($context_convictions_under_federal);
            $facillityApp->setFederalSanstionsLimitations($federal_sanstions_limitations);
            $facillityApp->setContectFederalSanstionsLimitations($contect_federal_sanstions_limitations);

            //remove all additional services
            $allAdditionalServices=$facillityApp->getAdditionalServices();

            if($allAdditionalServices){
                foreach ($allAdditionalServices as  $additionalService){
                    $facillityApp->removeAdditionalService($additionalService);
                }
            }

            $additionalServices = substr($additionalServicesSelected, 0, -1);
            $lbs = explode(",", $additionalServices);

            foreach ($lbs as $additionalService){
                $additionalServiceObj=$em->getRepository('App\Entity\AdditionalService')->find($additionalService);
                if($additionalServiceObj!=null){
                    $facillityApp->addAdditionalService($additionalServiceObj);
                }

            }

            $em->persist($facillityApp);
            $em->flush();

            return $this->redirectToRoute('provider_dashboard');
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/export_pdf/{id}", name="facility_app_pdf",methods={"GET"})
     */
    public function export(FacilityAppRepository $repository, $id) {

        $facilityApp=$repository->find($id);


        $pdfOptions=new Options();
        $pdfOptions->set('defaultFont', 'Roboto');
        $pdfOptions->set('isHtml5ParserEnabled',true);


        $pdf=new Dompdf($pdfOptions);



  //     return $this->render('facility_app/facilityapppdf.html.twig',['fApp'=>$facilityApp]);

//       die();
        $html = $this-> renderView('facility_app/facilityapppdf.html.twig',['fApp'=>$facilityApp]);


        $pdf->loadHtml($html);
        $pdf->setPaper('A4','portrait');
        $pdf->render();

        $pdf->stream('facility_credentialing_app.pdf',['Attachment'=>true]);

        $this->returnPDFResponseFromHTML($html);

        die();
    }


    /**
     * @Route("/delete", name="provider_delete_facility_location",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $cvo = $em->getRepository('App\Entity\FacilityAppLocation')->find($id);
            $removed = 0;
            $message = "";

            if ($cvo) {
                try {
                    $em->remove($cvo);
                    $em->flush();
                    $removed = 1;
                    $message = "The Facility App Location has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Facility App Location can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message,'id'=>$id)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }

}
