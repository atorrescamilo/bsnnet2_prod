<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\Prospect;
use App\Entity\ProspectAddress;
use App\Entity\ProspectContact;
use App\Form\AddressPType;
use App\Form\ContactType;
use App\Form\ProspectContactType;
use App\Form\ProspectProviderType;
use App\Form\ProspectType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\OrganizationProspect;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use PhpOffice\PhpSpreadsheet\Reader;


/**
 * @Route("/admin/organizationprospect")
 */
class OrganizationProspectController extends AbstractController{

    /**
     * @Route("/index", name="admin_organizationprospect")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $sql="SELECT p.id, p.provider_type,p.name,p.email,p.phone,p.contact_name,p.group_npi,p.phone_ext,
            prospect_status.name as status,
            GROUP_CONCAT(DISTINCT prospect_address.county ORDER BY prospect_address.id ASC SEPARATOR ', ') AS counties,
            GROUP_CONCAT(DISTINCT prospect_address.region ORDER BY prospect_address.id ASC SEPARATOR ', ') AS regions,
            GROUP_CONCAT(DISTINCT prospect_address.street ORDER BY prospect_address.id ASC SEPARATOR ', ') AS street,
            GROUP_CONCAT(DISTINCT prospect_address.city ORDER BY prospect_address.id ASC SEPARATOR ', ') AS city,
            GROUP_CONCAT(DISTINCT DATE_FORMAT(email_log.created_date, '%m/%d/%Y') ORDER BY email_log.id DESC SEPARATOR ', ')  AS created_date,
            GROUP_CONCAT(DISTINCT email_type.name  SEPARATOR ', ')  AS template 
            FROM  prospect p 
            LEFT JOIN prospect_address ON prospect_address.prospect_id = p.id
            LEFT JOIN prospect_status ON p.status_id=prospect_status.id
            LEFT JOIN email_log ON email_log.sent_to=p.email
            LEFT JOIN email_type ON email_type.id=email_log.email_type_id
            WHERE p.source_type='organization'
            GROUP BY p.id
            ORDER BY p.name";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $prospects= $stmt->fetchAll();


            $email_templates=$em->getRepository('App\Entity\EmailType')->findBy(array(),array('name'=>'ASC'));
            $delete_form_ajax = $this->createCustomForm('PROSPECT_ID', 'DELETE', 'admin_delete_organizationprospect');

            return $this->render('organizationprospect/index.html.twig', array('organizationprospects' => $prospects,
                'email_templates'=>$email_templates,'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/providers-list", name="admin_organizationprospect2")
     */
    public function index2(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $sql="SELECT p.id, p.provider_type,p.name,p.email,p.phone,p.contact_name,p.group_npi,
            prospect_status.name as status,p.provider_name, p.provider_npi, p.last_name as lastname,
            GROUP_CONCAT(DISTINCT prospect_address.county ORDER BY prospect_address.id ASC SEPARATOR ', ') AS counties,
            GROUP_CONCAT(DISTINCT prospect_address.region ORDER BY prospect_address.id ASC SEPARATOR ', ') AS regions,
            GROUP_CONCAT(DISTINCT prospect_address.street ORDER BY prospect_address.id ASC SEPARATOR ', ') AS street,
            GROUP_CONCAT(DISTINCT prospect_address.city ORDER BY prospect_address.id ASC SEPARATOR ', ') AS city,
            GROUP_CONCAT(DISTINCT DATE_FORMAT(email_log.created_date, '%m/%d/%Y') ORDER BY email_log.id DESC SEPARATOR ', ')  AS created_date,
            GROUP_CONCAT(DISTINCT email_type.name  SEPARATOR ', ')  AS template 
            FROM  prospect p 
            LEFT JOIN prospect_address ON prospect_address.prospect_id = p.id
            LEFT JOIN prospect_status ON p.status_id=prospect_status.id
            LEFT JOIN email_log ON email_log.sent_to=p.email
            LEFT JOIN email_type ON email_type.id=email_log.email_type_id
            WHERE p.source_type='provider'
            GROUP BY p.id
            ORDER BY p.name";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $prospects= $stmt->fetchAll();

            //$prospects = $em->getRepository('App\Entity\Prospect')->findAll();
            $email_templates=$em->getRepository('App\Entity\EmailType')->findBy(array(),array('name'=>'ASC'));

            $delete_form_ajax = $this->createCustomForm('PROSPECT_ID', 'DELETE', 'admin_delete_organizationprospect');

            return $this->render('organizationprospect/index2.html.twig', array('organizationprospects' => $prospects,
                'email_templates'=>$email_templates,'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new-address/{id}", name="admin_new_organizationprospect_address")
     */
    public function addAddressProspect($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $google_apikey = $this->getParameter('google_apikey');
            $us_states=$em->getRepository('App\Entity\UsState')->findAll();
            $prospect=$em->getRepository('App\Entity\Prospect')->find($id);

            return $this->render('organizationprospect/add_address.html.twig',['google_apikey'=>$google_apikey,'us_states'=>$us_states,
                'prospect'=>$prospect]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new-address-2/{id}", name="admin_new_organizationprospect_address2")
     */
    public function addAddressProspect2($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $google_apikey = $this->getParameter('google_apikey');
            $us_states=$em->getRepository('App\Entity\UsState')->findAll();
            $prospect=$em->getRepository('App\Entity\Prospect')->find($id);

            return $this->render('organizationprospect/add_address2.html.twig',['google_apikey'=>$google_apikey,'us_states'=>$us_states,
                'prospect'=>$prospect]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new-contact/{id}", name="admin_new_prospect_contact")
     */
    public function newProspectContact($id, Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $organizationProspect = $em->getRepository('App\Entity\Prospect')->find($id);

            $contact=new ProspectContact();
            $form = $this->createForm(ProspectContactType::class, $contact);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $contact->setProspect($organizationProspect);
                $em->persist($contact);
                $em->flush();

                $action_type=$form['action_type']->getData();

                if($action_type==1){
                    return  $this->redirectToRoute('admin_view_organizationprospect',['id'=>$organizationProspect->getId(),'tab'=>6]);
                }else{
                    return  $this->redirectToRoute('admin_new_prospect_contact',['id'=>$organizationProspect->getId()]);
                }
            }

            return $this->render('organizationprospect/prospect_contact_form.html.twig', ['form' => $form->createView(), 'action' => 'New','prospect'=>$organizationProspect->getId()]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit-address/{id}/{prospect}", name="admin_edit_organizationprospect_address",defaults={"id": null, "prospect": null})
     */
    public function editAddressProspect($id,$prospect){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $google_apikey = $this->getParameter('google_apikey');
            $us_states=$em->getRepository('App\Entity\UsState')->findAll();
            $prospect=$em->getRepository('App\Entity\Prospect')->find($prospect);
            $document=$em->getRepository('App\Entity\ProspectAddress')->find($id);

            return $this->render('organizationprospect/edit_address.html.twig',['google_apikey'=>$google_apikey,'us_states'=>$us_states,
                'prospect'=>$prospect,'document'=>$document]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit_prospect_contact/{id_pc}/{id}", name="edit_prospect_contact", defaults={"id_pc": null,"id": null})
     *
     */
    public function edit_prospect_contact(Request $request, $id_pc, $id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $organizationProspect = $em->getRepository('App\Entity\Prospect')->find($id);

            $contact=$em->getRepository('App:ProspectContact')->find($id_pc);
            $form=$this->createForm(ProspectContactType::class, $contact);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $contact->setUpdatedAt(new \DateTime());
                $em->persist($contact);
                $em->flush();

                $action_type=$form['action_type']->getData();

                if($action_type==1){
                    return  $this->redirectToRoute('admin_view_organizationprospect',['id'=>$organizationProspect->getId(),'tab'=>6]);
                }else{
                    return  $this->redirectToRoute('admin_new_prospect_contact',['id'=>$organizationProspect->getId()]);
                }
            }

            return $this->render('organizationprospect/prospect_contact_form.html.twig', ['form' => $form->createView(), 'action' => 'Edit','prospect'=>$organizationProspect->getId()]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view-address/{id}/{prospect}", name="admin_view_organizationprospect_address",defaults={"id": null, "prospect": null})
     */
    public function viewAddressProspect($id,$prospect){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $google_apikey = $this->getParameter('google_apikey');
            $us_states=$em->getRepository('App\Entity\UsState')->findAll();
            $prospect=$em->getRepository('App\Entity\Prospect')->find($prospect);
            $document=$em->getRepository('App\Entity\ProspectAddress')->find($id);

            return $this->render('organizationprospect/view_address.html.twig',['google_apikey'=>$google_apikey,'us_states'=>$us_states,
                'prospect'=>$prospect,'document'=>$document]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view-contact_prospect/{id}/{prospect}", name="admin_view_organizationprospect_contact",defaults={"id": null, "prospect": null})
     */
    public function viewContactProspect($id,$prospect){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $prospect=$em->getRepository('App\Entity\Prospect')->find($prospect);
            $contact=$em->getRepository('App\Entity\ProspectContact')->find($id);

            return $this->render('organizationprospect/view_contact.html.twig',['prospect'=>$prospect,'contact'=>$contact]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create-proscpect-adrr", name="admin_create_organizationprospect_address", methods={"POST"})
     */
    public function createAddressProspect(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $street=$request->get('street');
            $suite_number=$request->get('suite_number');
            $county=$request->get('county');
            $state=$request->get('state');
            $zip_code=$request->get('zip_code');
            $region=$request->get('region');
            $phone_number=$request->get('phone_number');
            $phone_number=$request->get('phone_number');
            $fax=$request->get('fax');
            $prospect=$request->get('prospect');
            $city=$request->get('city');

            $addr=new ProspectAddress();
            $addr->setStreet($street);
            $addr->setSuite($suite_number);
            $addr->setCounty($county);
            $addr->setState($state);
            $addr->setZipCode($zip_code);
            $addr->setRegion($region);
            $addr->setPhone($phone_number);
            $addr->setFax($fax);
            $addr->setCity($city);

            $prospectObj=$em->getRepository('App\Entity\Prospect')->find($prospect);
            if($prospectObj){
                $addr->setProspect($prospectObj);
            }

            $em->persist($addr);
            $em->flush();

            return $this->redirectToRoute('admin_view_organizationprospect',array('id'=>$prospect,'tab'=>3));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/updated-proscpect-adrr", name="admin_updated_organizationprospect_address", methods={"POST"})
     */
    public function updatedAddressProspect(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $prospect=$request->get('prospect');
            $address_id=$request->get('address_id');
            $street=$request->get('street');
            $suite_number=$request->get('suite_number');
            $county=$request->get('county');
            $state=$request->get('state');
            $zip_code=$request->get('zip_code');
            $region=$request->get('region');
            $phone_number=$request->get('phone_number');
            $fax=$request->get('fax');
            $city=$request->get('city');

            $addr=$em->getRepository('App\Entity\ProspectAddress')->find($address_id);
            if($addr==null){
                $addr=new ProspectAddress();
            }

            $addr->setStreet($street);
            $addr->setSuite($suite_number);
            $addr->setCounty($county);
            $addr->setState($state);
            $addr->setZipCode($zip_code);
            $addr->setRegion($region);
            $addr->setPhone($phone_number);
            $addr->setFax($fax);
            $addr->setCity($city);

            $prospectObj=$em->getRepository('App\Entity\Prospect')->find($prospect);
            if($prospectObj){
                $addr->setProspect($prospectObj);
            }

            $em->persist($addr);
            $em->flush();

            return $this->redirectToRoute('admin_view_organizationprospect',array('id'=>$prospect,'tab'=>3));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/provider-edit/{id}", name="admin_edit_organizationprospect_provider", defaults={"id": null})
     */
    public function edit2($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Prospect')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_organizationprospect');
            }

            $status=$em->getRepository('App\Entity\ProspectStatus')->findAll();

            return $this->render('organizationprospect/edit2.html.twig', array('document' => $document,'status'=>$status));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}/{tab}", name="admin_view_organizationprospect",defaults={"id": null,"tab": 1})
     */
    public function view($id,$tab){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Prospect')->find($id);

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('admin_organizationprospect');
            }

            $tickets = $em->getRepository('App:ProspectTicket')->findBy(array('prospect' => $id));
            $tickets_delete_form_ajax = $this->createCustomForm('TICKET_ID', 'DELETE', 'admin_delete_organizationprospectticket');
            $address_delete_form_ajax = $this->createCustomForm('PROSPECT_ADDRESS_ID', 'DELETE', 'admin_delete_organization_prospect_address');
            $contact_delete_form_ajax = $this->createCustomForm('PROSPECT_CONTACT_ID', 'DELETE', 'admin_delete_organization_prospect_contact');

            $npi=$document->getGroupNpi();

            $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$npi));
            $emailLogs=$em->getRepository('App\Entity\EmailLog')->findBy(array('sent_to'=>$document->getEmail()));

            $address=$em->getRepository('App:ProspectAddress')->findBy(array('prospect'=>$id));
            $contacts=$em->getRepository('App:ProspectContact')->findAll();

            return $this->render('organizationprospect/view.html.twig', array('document' => $document,
                'tab' => $tab,'pmls'=>$pmls, 'emailLogs'=>$emailLogs,
                'tickets' => $tickets,'address'=>$address,'contacts'=>$contacts,
                'tickets_delete_form_ajax' => $tickets_delete_form_ajax->createView(),
                'address_delete_form_ajax' => $address_delete_form_ajax->createView(),
                'contact_delete_form_ajax' => $contact_delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/provider-view/{id}/{tab}", name="admin_view_organizationprospect_provider",defaults={"id": null,"tab": 1})
     */
    public function view2($id,$tab){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Prospect')->find($id);

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('admin_organizationprospect2');
            }

            $tickets = $em->getRepository('App\Entity\ProspectTicket')->findBy(array('prospect' => $id));
            $delete_form_ajax = $this->createCustomForm('TICKET_ID', 'DELETE', 'admin_delete_organizationprospectticket');

            $npi=$document->getGroupNpi();

            $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$npi));
            $emailLogs=$em->getRepository('App\Entity\EmailLog')->findBy(array('sent_to'=>$document->getEmail()));

            $address=$em->getRepository('App\Entity\ProspectAddress')->findBy(array('prospect'=>$id));


            return $this->render('organizationprospect/view2.html.twig', array('document' => $document,
                'tab' => $tab,'pmls'=>$pmls, 'emailLogs'=>$emailLogs,
                'tickets' => $tickets,'address'=>$address,
                'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_organizationprospect",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $organizationprospect = $em->getRepository('App\Entity\Prospect')->find($id);
            $removed = 0;
            $message = "";

            if ($organizationprospect) {
                try {
                    $em->remove($organizationprospect);
                    $em->flush();
                    $removed = 1;
                    $message = "The Organization Prospect has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Organization Prospect can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/prospect-address-delete/{id}", name="admin_delete_organization_prospect_address",methods={"POST","DELETE"})
     */
    public function deleteProspectAddressAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $address_prospect = $em->getRepository('App:ProspectAddress')->find($id);
            $removed = 0;
            $message = "";

            if ($address_prospect) {
                try {
                    $em->remove($address_prospect);
                    $em->flush();
                    $removed = 1;
                    $message = "The Prospect Address has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Prospect Address can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/prospect-contact-delete/{id}", name="admin_delete_organization_prospect_contact",methods={"POST","DELETE"})
     */
    public function deleteProspectContactAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $contact_prospect = $em->getRepository('App:ProspectContact')->find($id);
            $removed = 0;
            $message = "";

            if ($contact_prospect) {
                try {
                    $em->remove($contact_prospect);
                    $em->flush();
                    $removed = 1;
                    $message = "The Prospect Contact has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Prospect Contact can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_organizationprospect",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $organizationprospect  = $em->getRepository('App\Entity\Prospect')->find($id);

                if ($organizationprospect) {
                    try {
                        $em->remove($organizationprospect);
                        $em->flush();
                        $removed = 1;
                        $message = "The Organization Prospect has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Organization Prospect can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


    /**
     * @Route("/providers-prospect-upload", name="admin_organizationprospect_upload")
     */
    public function upload(){
        $em = $this->getDoctrine()->getManager();
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls";

        $fileName="FCC_provider_recruit_list.xlsx";

        $fileToRead = $url."/" . $fileName;

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=1;

        foreach ($sheetData as $sheet) {
            if ($cont > 1) {
                if ($sheet['A'] != null or $sheet['A'] != "") {

                    $prospect=new Prospect();
                    $prospect->setProviderName($sheet['A']);
                    $prospect->setProviderNpi($sheet['B']);
                    $prospect->setProviderSpecialty($sheet['C']);
                    $prospect->setPhone($sheet['K']);
                    $prospect->setName($sheet['L']);
                    $prospect->setDbaName($sheet['L']);
                    $prospect->setSourceType('provider');

                    $em->persist($prospect);
                    $em->flush();

                    $prospectaddr=new ProspectAddress();
                    $prospectaddr->setStreet($sheet['F']);
                    $prospectaddr->setSuite($sheet['G']);
                    $prospectaddr->setProspect($prospect);
                    $prospectaddr->setState($sheet['J']);
                    $prospectaddr->setZipCode($sheet['I']);
                    $prospectaddr->setPhone($sheet['K']);

                    $em->persist($prospectaddr);
                    $em->flush();
                }
            }

            $cont++;
        }
        die();
    }

    /**
     * @Route("/new", name="admin_new_organizationprospect")
     *
     */
    public function add(Request  $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $prospect = new Prospect();
            $referer = $request->headers->get('referer');
            $refererPathInfo = Request::create($referer)->getPathInfo();

            $origin="";
            if(strpos($refererPathInfo, 'provider-view')){
                $origin='view';
            }
            if(strpos($refererPathInfo, 'providers-list')){
                $origin='index';
            }
            $form = $this->createForm(ProspectType::class, $prospect);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $action_type=$form['action_type']->getData();

                $prospect->setSourceType('organization');
                $em->persist($prospect);
                $em->flush();

                $address=$form['address']->getData();
                $address_array=explode(",",$address);
                foreach ($address_array as $addr_array){
                    $address_p=explode("~",$addr_array);

                    $prospect_addr=new ProspectAddress();
                    $prospect_addr->setStreet($address_p[0]);
                    $prospect_addr->setSuite($address_p[1]);
                    $prospect_addr->setCity($address_p[2]);
                    $prospect_addr->setProspect($prospect);
                    $prospect_addr->setZipCode(substr($address_p[3],0,5));
                    $prospect_addr->setState($address_p[4]);
                    $prospect_addr->setPhone($address_p[5]);
                    $prospect_addr->setFax($address_p[6]);

                    $em->persist($prospect_addr);
                    $em->flush();
                }

                if($action_type==1){
                    return $this->redirectToRoute('admin_organizationprospect');
                }else{
                    return $this->redirectToRoute('admin_new_organizationprospect');
                }
            }
            return $this->render('organizationprospect/form.html.twig', array('form' => $form->createView(), 'action' => 'New','origin'=>$origin));
        }else{
            return $this->redirectToRoute('admin_login');
        }

    }

    /**
     * @Route("/edit/{id}", name="admin_edit_organizationprospect",defaults={"id": null, "prospect": null})
     *
     */
    public function edit(Request  $request, $id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            $em = $this->getDoctrine()->getManager();
            $referer = $request->headers->get('referer');
            $refererPathInfo = Request::create($referer)->getPathInfo();

            $origin="";
            if(strpos($refererPathInfo, 'view')){
                $origin='view';
            }
            if(strpos($refererPathInfo, 'index')){
                $origin='index';
            }

            $prospect = $em->getRepository('App:Prospect')->find($id);

            $form = $this->createForm(ProspectType::class, $prospect);


            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $action_type=$form['action_type']->getData();

                $em->persist($prospect);
                $em->flush();

                if($action_type==1){
                    if($origin=="index"){
                        return $this->redirectToRoute('admin_organizationprospect');
                    }else{
                        return $this->redirectToRoute('admin_view_organizationprospect',['id'=>$id]);
                    }
                }else{
                    return $this->redirectToRoute('admin_new_organizationprospect');
                }
            }
            return $this->render('organizationprospect/form.html.twig', array('form' => $form->createView(), 'action' => 'Edit','origin'=>$origin,'id'=>$id));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new-provider", name="admin_new_organizationprospect2")
     *
     */
    public function add2(Request  $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $prospect = new Prospect();

            $prospect->setSourceType('provider');

            $form = $this->createForm(ProspectProviderType::class, $prospect);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $action_type=$form['action_type']->getData();
                $prospect->setSourceType('provider');
                $em->persist($prospect);
                $em->flush();

                $address=$form['address']->getData();
                $address_array=explode(",",$address);
                foreach ($address_array as $addr_array){
                    $address_p=explode("~",$addr_array);

                    $prospect_addr=new ProspectAddress();
                    $prospect_addr->setStreet($address_p[0]);
                    $prospect_addr->setSuite($address_p[1]);
                    $prospect_addr->setCity($address_p[2]);
                    $prospect_addr->setProspect($prospect);
                    $prospect_addr->setZipCode(substr($address_p[3],0,5));
                    $prospect_addr->setState($address_p[4]);
                    $prospect_addr->setPhone($address_p[5]);
                    $prospect_addr->setFax($address_p[6]);

                    $em->persist($prospect_addr);
                    $em->flush();
                }

                if($action_type==1){
                    return $this->redirectToRoute('admin_organizationprospect2');
                }else{
                    return $this->redirectToRoute('admin_new_organizationprospect2');
                }
            }
            return $this->render('organizationprospect/form_prospect_provider.html.twig', array('form' => $form->createView(), 'action' => 'New'));
        }else{
            return $this->redirectToRoute('admin_login');
        }

    }

    /**
     * * @Route("/edit-provider/{id}", name="admin_update_organizationprospect2",defaults={"id": null, "prospect": null})
     *
     */
    public function update2(Request  $request, $id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            $em = $this->getDoctrine()->getManager();
            $referer = $request->headers->get('referer');
            $refererPathInfo = Request::create($referer)->getPathInfo();

            $origin="";
            if(strpos($refererPathInfo, 'provider-view')){
                $origin='view';
            }
            if(strpos($refererPathInfo, 'providers-list')){
                $origin='index';
            }

            $prospect = $em->getRepository('App:Prospect')->find($id);
            $form = $this->createForm(ProspectProviderType::class, $prospect);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $action_type=$form['action_type']->getData();

                $em->persist($prospect);
                $em->flush();

                if($action_type==1){
                    if($origin=="index"){
                        return $this->redirectToRoute('admin_organizationprospect2');
                    }else{
                        return $this->redirectToRoute('admin_view_organizationprospect_provider',['id'=>$id]);
                    }
                }else{
                    return $this->redirectToRoute('admin_new_organizationprospect2');
                }
            }
            return $this->render('organizationprospect/form_prospect_provider.html.twig', array('form' => $form->createView(), 'action' => 'Edit','origin'=>$origin,
                'id'=>$id));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple_addresses", name="admin_delete_multiple_organizationprospectaddress",methods={"POST","DELETE"})
     */
    public function deleteMultipleAddressAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $prospect_address =  $em->getRepository('App:ProspectAddress')->find($id);

                if ($prospect_address) {
                    try {
                        $em->remove($prospect_address);
                        $em->flush();
                        $removed = 1;
                        $message = "The address has been successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The address can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple_contacts", name="admin_delete_multiple_organizationprospectcontact",methods={"POST","DELETE"})
     */
    public function deleteMultipleContactAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $prospect_address =  $em->getRepository('App:ProspectContact')->find($id);

                if ($prospect_address) {
                    try {
                        $em->remove($prospect_address);
                        $em->flush();
                        $removed = 1;
                        $message = "The address has been successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The address can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }
}
