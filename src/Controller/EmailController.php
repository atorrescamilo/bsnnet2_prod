<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\ChangeLog;
use App\Entity\EmailLog;
use App\Entity\EmailQueued;
use App\Entity\EmailType;
use App\Entity\Organization;
use App\Entity\OrganizationContact;
use App\Entity\ProviderCredentialing;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/email")
 */
class EmailController extends AbstractController
{

    private $path;
    /**
     * EmailController constructor.
     */
    public function __construct()
    {
      //  $this->path=  $this->getParameter('kernel.project_dir');
    }

    /**
     * @Route("/email-queue", name="admin-email-queue-list")
     */
    public function index()
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $emailqueued = $em->getRepository('App\Entity\EmailQueued')->findAll();
            $delete_form_ajax = $this->createCustomForm('EMAIL_ID', 'DELETE', 'admin_delete_email');

            return $this->render('email/index.html.twig', [
                'emailqueued' => $emailqueued,
                'delete_form_ajax' => $delete_form_ajax->createView()
            ]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/add_email_queued", name="admin_add_email_queued")
     */
    public function addEmailQueued(Request $request){
        $em=$this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App\Entity\Organization')->findAll();
        $organizationR=array();

        foreach ($organizations as $organization){
            if($organization!=null){
                //$organization=new Organization();
                //check if the organization have a addr credentialing
                $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                $hassCred=false;
                if($address!=null){
                    foreach ($address as $addr){
                        $addrCred=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId()));

                        if($addrCred!=null  and count($addrCred)>0){
                            $hassCred=true;
                        }
                    }
                }
                if($hassCred==true){
                    $organizationR[]=$organization;
                }else{
                    //search if the organization  have one or more providers assigned to MMM
                    $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId()));
                    if($providers!=null){

                    }
                }
            }
        }

        return new Response('OK');
    }

    /**
     * @Route("/add_email_queued_ajax", name="admin_add_email_queued_ajax")
     */
    public function addEmailQueuedAjax(Request $request){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();
        $ids = $request->get('ids');
        $template=$request->get('template');
        $datetosend=$request->get('datetosend');
        $id_organization=$request->get('id_organization');
        $plan=$request->get('plan');
        $cont_global=0;

        if($ids!=null){
            foreach ($ids as $id) {
                $organization  = $em->getRepository('App\Entity\Organization')->find($id);

                if ($organization) {
                    $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));

                    $providerResults="";
                    $contMMM=0;

                    if($template==24){
                        foreach ($contacts as $contact) {
                            $cont = 0;
                            $email = $contact->getEmail();

                            if($email!="" and $email!=null) {
                                $emailqueed_exits = $em->getRepository('App:EmailQueued')->findBy(array('sent_to' => $email, 'email_type' => $template));
                                if ($emailqueed_exits == null or count($emailqueed_exits) == 0) {

                                    $email_template=$em->getRepository('App:EmailType')->find($template);
                                    $best_option_template=$email_template->getBestChoices();
                                    $best_option_template_array=[];
                                    if($best_option_template){
                                        foreach ($best_option_template as $bot){
                                            $best_option_template_array[]=$bot->getId();
                                        }
                                    }

                                    $best_options_contact=$contact->getBestChoices();

                                    if($best_options_contact){
                                        foreach ($best_options_contact as $best_option_contact){
                                            $best_id_1=$best_option_contact->getId();
                                            if(in_array($best_id_1,$best_option_template_array)){
                                                $cont++;
                                                $cont_global++;
                                            }
                                        }

                                        $date_today=date('m/d/Y');

                                        if($cont>0){
                                            $emailQueue=new EmailQueued();
                                            $status=$em->getRepository('App:EmailStatus')->find(1);
                                            $emailQueue->setStatus($status);
                                            $emailQueue->setOrganization($organization);
                                            $emailQueue->setOrganizationName($organization->getName());
                                            $emailQueue->setEmailType($email_template);
                                            $emailQueue->setDateToSend(new \DateTime($date_today));
                                            $emailQueue->setSentTo($email);
                                            $emailQueue->setContact($contact);
                                            $emailQueue->setHealthPlan(null);

                                            $em->persist($emailQueue);
                                            $em->flush();
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if($template==5){
                        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));
                        if($providers!=null){
                            foreach ($providers as $provider){
                                $payers=$provider->getPayers();
                                if($payers!=null){
                                    foreach ($payers as $payer){
                                        if($payer->getId()==1){
                                            $contMMM++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if($template==4){
                        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));
                        if($this->OrgIsFacility($organization)==true){
                            if($providers!=null){
                                foreach ($providers as $provider){
                                    if($provider!=null){
                                        $providerResults.=$provider->getId().",";
                                    }
                                }
                            }
                        }else{
                            $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));
                            if($providers!=null){
                                foreach ($providers as $provider){
                                    $providerCredentialing=$em->getRepository('App:ProviderCredentialing')->findOneBy(array('npi_number'=>$provider->getNpiNumber()));
                                    if($providerCredentialing->getCredentialingStatus()->getId()==4){
                                        $providerResults.=$provider->getId().",";
                                    }
                                }
                            }
                        }
                    }else{
                        if($this->OrgIsFacility($organization)==true){
                            $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));
                            if($providers!=null){
                                foreach ($providers as $provider){
                                    if($provider!=null){
                                        $providerResults.=$provider->getId().",";
                                    }
                                }
                            }
                        }else{
                            $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));

                            if($providers!=null){
                                foreach ($providers as $provider){
                                    $payers=$provider->getPayers();
                                    if($payers!=null){
                                        foreach ($payers as $payer){
                                            if($payer->getId()==1){
                                                $providerResults.=$provider->getId().",";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $providerResults=substr($providerResults,0,-1);

                    if($template==6 or $template==2 or $template==3 or $template==11 or $template==12 or $template==13 or $template==14 or $template==15 or $template==16
                        or $template==17 or $template==21 or $template==20  or $template==22 or $template==23){
                        $has_cred=false;

                        $playerObJ=$em->getRepository('App:Payer')->find($plan);
                        if($contacts!=null){

                            foreach ($contacts as $contact){
                                $cont=0;
                                $email=$contact->getEmail();
                                if($email!="" and $email!=null){
                                    $emailqueed_exits=$em->getRepository('App\Entity\EmailQueued')->findBy(array('sent_to'=>$email,'email_type'=>$template));

                                    if($emailqueed_exits==null or count($emailqueed_exits)==0){
                                        if($template==22 or $template==20 or $template==23){

                                            $email_template=$em->getRepository('App\Entity\EmailType')->find($template);
                                            $best_option_template=$email_template->getBestChoices();
                                            $best_option_template_array=[];
                                            if($best_option_template){
                                                foreach ($best_option_template as $bot){
                                                    $best_option_template_array[]=$bot->getId();
                                                }
                                            }

                                            $best_options_contact=$contact->getBestChoices();

                                            if($best_options_contact){
                                                foreach ($best_options_contact as $best_option_contact){
                                                    $best_id_1=$best_option_contact->getId();
                                                    if(in_array($best_id_1,$best_option_template_array)){
                                                        $cont++;
                                                        $cont_global++;
                                                    }
                                                }

                                                $date_today=date('m/d/Y');

                                                if($cont>0){
                                                    $emailQueue=new EmailQueued();
                                                    $status=$em->getRepository('App\Entity\EmailStatus')->find(1);
                                                    $emailQueue->setStatus($status);
                                                    $emailQueue->setOrganization($organization);
                                                    $emailQueue->setOrganizationName($organization->getName());
                                                    $emailQueue->setEmailType($email_template);
                                                    $emailQueue->setDateToSend(new \DateTime($date_today));
                                                    $emailQueue->setSentTo($email);
                                                    $emailQueue->setContact($contact);
                                                    if($template==22){
                                                        $emailQueue->setHealthPlan($playerObJ->getName());
                                                    }

                                                    $em->persist($emailQueue);
                                                    $em->flush();
                                                }
                                            }
                                        }else{
                                            $emailQueue=new EmailQueued();
                                            $status=$em->getRepository('App\Entity\EmailStatus')->find(1);
                                            $email_type=$em->getRepository('App\Entity\EmailType')->find($template);

                                            $emailQueue->setStatus($status);
                                            $emailQueue->setOrganization($organization);
                                            $emailQueue->setEmailType($email_type);
                                            $emailQueue->setDateToSend(new \DateTime($datetosend));
                                            $emailQueue->setSentTo($contact->getEmail());
                                            $emailQueue->setContact($contact);
                                            $emailQueue->setProviders($providerResults);

                                            $em->persist($emailQueue);
                                            $em->flush();
                                        }
                                    }
                                }
                            }

                            if($cont_global==0){
                                if($template==20 or $template==22 or $template==23){
                                    foreach ($contacts as $contact){
                                        $email=$contact->getEmail();
                                        if($email!=""){
                                            $email_template=$em->getRepository('App\Entity\EmailType')->find($template);
                                            $emailQueue=new EmailQueued();
                                            $status=$em->getRepository('App\Entity\EmailStatus')->find(1);
                                            $emailQueue->setStatus($status);
                                            $emailQueue->setOrganization($organization);
                                            $emailQueue->setOrganizationName($organization->getName());
                                            $emailQueue->setEmailType($email_template);
                                            $emailQueue->setDateToSend(new \DateTime($date_today));
                                            $emailQueue->setSentTo($email);
                                            $emailQueue->setContact($contact);
                                            if($template==22){
                                                $emailQueue->setHealthPlan($playerObJ->getName());
                                            }

                                            $em->persist($emailQueue);
                                            $em->flush();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if($contacts!=null and $providerResults!=""){
                        foreach ($contacts as $contact){
                            $email=$contact->getEmail();

                            if($template==4 and $contMMM>0){
                                if($email!="" and $email!=null){
                                    $emailqueed_exits=$em->getRepository('App\Entity\EmailQueued')->findBy(array('sent_to'=>$email,'email_type'=>$template));

                                    if($emailqueed_exits==null or count($emailqueed_exits)==0){
                                        $emailQueue=new EmailQueued();
                                        $status=$em->getRepository('App\Entity\EmailStatus')->find(1);
                                        $email_type=$em->getRepository('App\Entity\EmailType')->find($template);

                                        $emailQueue->setStatus($status);
                                        $emailQueue->setOrganization($organization);
                                        $emailQueue->setEmailType($email_type);
                                        $emailQueue->setDateToSend(new \DateTime($datetosend));
                                        $emailQueue->setSentTo($contact->getEmail());
                                        $emailQueue->setContact($contact);
                                        $emailQueue->setProviders($providerResults);

                                        $em->persist($emailQueue);
                                        $em->flush();
                                    }
                                }
                            }else{
                                if($email!="" and $email!=null){
                                    $emailqueed_exits=$em->getRepository('App\Entity\EmailQueued')->findBy(array('sent_to'=>$email,'email_type'=>$template));

                                    if($emailqueed_exits==null or count($emailqueed_exits)==0){
                                        if($template!=20 and $template!=22 and $template!=23 and $template!=24){
                                            $emailQueue=new EmailQueued();
                                            $status=$em->getRepository('App\Entity\EmailStatus')->find(1);
                                            $email_type=$em->getRepository('App\Entity\EmailType')->find($template);

                                            $emailQueue->setStatus($status);
                                            $emailQueue->setOrganization($organization);
                                            $emailQueue->setEmailType($email_type);
                                            $emailQueue->setDateToSend(new \DateTime($datetosend));
                                            $emailQueue->setSentTo($contact->getEmail());
                                            $emailQueue->setContact($contact);
                                            $emailQueue->setProviders($providerResults);

                                            $em->persist($emailQueue);
                                            $em->flush();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if($id_organization!="") {
            $organization = $em->getRepository('App\Entity\Organization')->find($id_organization);

            if ($organization) {
                $contacts = $em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization' => $organization->getId()));

                $providerResults = "";
                if ($this->OrgIsFacility($organization) == true) {
                    $providers = $em->getRepository('App\Entity\Provider')->findBy(array('organization' => $organization->getId()));
                    if ($providers != null) {
                        foreach ($providers as $provider) {
                            if ($provider != null) {
                                $providerResults .= $provider->getId() . ",";
                            }
                        }
                    }
                } else {
                    $providers = $em->getRepository('App\Entity\Provider')->findBy(array('organization' => $organization->getId()));

                    if ($providers != null) {
                        foreach ($providers as $provider) {
                            $payers = $provider->getPayers();
                            if ($payers != null or $payers != "") {
                                $payersArray = explode(',', $payers);
                                foreach ($payersArray as $payer) {
                                    if ($payer->getId() == 1) {
                                        $providerResults .= $provider->getId() . ",";
                                    }
                                }
                            }

                        }
                    }
                }

                $providerResults = substr($providerResults, 0, -1);
                if ($contacts != null) {
                    foreach ($contacts as $contact) {
                        $email = $contact->getEmail();

                        if ($email != "" and $email != null) {
                            $emailqueed_exits = $em->getRepository('App\Entity\EmailQueued')->findBy(array('sent_to' => $email, 'email_type' => $template));

                            if ($emailqueed_exits == null or count($emailqueed_exits) == 0) {

                                if($template!=20 and $template!=22 and $template!=23 and $template!=24){
                                    $emailQueue = new EmailQueued();
                                    $status = $em->getRepository('App\Entity\EmailStatus')->find(1);
                                    $email_type = $em->getRepository('App\Entity\EmailType')->find($template);

                                    $emailQueue->setStatus($status);
                                    $emailQueue->setOrganization($organization);
                                    $emailQueue->setEmailType($email_type);
                                    $emailQueue->setDateToSend(new \DateTime($datetosend));
                                    $emailQueue->setSentTo($contact->getEmail());
                                    $emailQueue->setContact($contact);
                                    $emailQueue->setProviders($providerResults);

                                    $em->persist($emailQueue);
                                    $em->flush();
                                }
                            }
                        }
                    }
                }

            }
        }

        return new Response(
            json_encode(array('id' => $cont_global)), 200, array('Content-Type' => 'application/json')
        );
    }

    public function OrgIsFacility($org){
        $em=$this->getDoctrine()->getManager();

        $isFacility=false;
        $cont=0;
        $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$org->getId()));
        if($address!=null){
            foreach ($address as $addr){
                if($addr!=null){
                    if($addr->getIsFacility()==true){
                        $cont++;
                    }
                }
            }
        }

        if($cont>0){
            $isFacility=true;
        }

        return $isFacility;
    }

    /**
     * @Route("/email_log", name="admin_email_log")
     */
    public function getEmailLog(){
        $em=$this->getDoctrine()->getManager();
        $emaillogs=$em->getRepository('App\Entity\EmailLog')->findAll();

        return $this->render('email/email-log.html.twig', [
            'emaillogs' => $emaillogs]);
    }

    /**
     * @Route("/email_log_view/{id}", name="admin_email_log_view",defaults={"id": null})
     */
    public function EmailLogView($id){
        $em=$this->getDoctrine()->getManager();
        $document=$em->getRepository('App\Entity\EmailLog')->find($id);
        $providersIDs=$document->getProviders();

        $providers=explode(',',$providersIDs);
        $providersResult=[];
        foreach ($providers as $provider){
            $proObj=$em->getRepository('App\Entity\Provider')->find($provider);
            if($proObj!=null){
                $providersResult[]=$proObj;
            }
        }

        return $this->render('email/email-log-view.html.twig', [
            'document' => $document,'providers'=>$providersResult]);
    }

    /**
     * @Route("/send_email", name="admin_send_email")
     */
    public function sendEmail(Request $request, \Swift_Mailer $mailer){
        $em=$this->getDoctrine()->getManager();
        $id=$request->get('id');
        $emailLog=$em->getRepository('App\Entity\EmailLog')->find($id);

        if($emailLog!=null){
            $email_type=$emailLog->getEmailType();
            $organization=$emailLog->getOrganization();
            $contact=$emailLog->getContact();
            $providerResults=$emailLog->getProviders();
            $approved_date=$emailLog->getApprovedDate();

            $emailQueue = new EmailQueued();
            $status = $em->getRepository('App\Entity\EmailStatus')->find(1);

            $emailQueue->setStatus($status);
            $emailQueue->setOrganization($organization);
            $emailQueue->setEmailType($email_type);
            $emailQueue->setSentTo($contact->getEmail());
            $emailQueue->setContact($contact);
            $emailQueue->setProviders($providerResults);
            $emailQueue->setApprovedDate($approved_date);

            $em->persist($emailQueue);
            $em->flush();
        }

        return new Response(
            json_encode(array('id' => $id)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/verification-email-readed/{code}", name="admin_verification-email-readed", methods={"GET"})
     */
    public function verificationEmailReaded(Request $request,$code){
        $em=$this->getDoctrine()->getManager();

        $emailLog=$em->getRepository('App\Entity\EmailLog')->findOneBy(array('verification_hass'=>$code));

        if($emailLog!=null){
            $status=$em->getRepository('App\Entity\EmailStatus')->find(3); //status readed
            $emailLog->setStatus($status);
            $em->persist($emailLog);
            $em->flush();
        }
        return $this->render('email/read-notification.html.twig');
    }

    /**
     * @Route("/view/{id}", name="admin_view_email_queued", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\EmailQueued')->find($id);

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('admin-email-queue-list');
            }

            $providersResult=array();
            $providersId=$document->getProviders();
            $idArray=explode(',', $providersId);
            if($idArray!=null){
                foreach ($idArray as $id){
                    if($id!=null){
                        $provider=$em->getRepository('App\Entity\Provider')->find($id);
                        if($provider!=null){
                            $providersResult[]=$provider;
                        }
                    }
                }

            }

            return $this->render('email/view.html.twig', array('document' => $document,'providers'=>$providersResult));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_email_queued", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $emailQueud=$em->getRepository('App\Entity\EmailQueued')->find($id);

            return $this->render('email/edit.html.twig', array('document' => $emailQueud));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/set-org-credentialing-emails", name="admin_setorg_credentialing_emails")
     */
    public function setOrgCred(){
        set_time_limit(88200);
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $credentialings=$em->getRepository('App\Entity\ProviderCredentialing')->findAll();
            foreach ($credentialings as $credentialing){
                if($credentialing->getProvider()!=null){
                    $organization=$credentialing->getProvider()->getOrganization();
                    $credentialing->setOrganization($organization);
                    $em->persist($credentialing);
                    $em->flush();
                }
            }

            die();
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new-credentialing-emails", name="admin_new_credentialing_emails")
     */
    public function newEmailsCred(){
        set_time_limit(88200);
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $organizations=$em->getRepository('App\Entity\Organization')->findAll();
            $cont=1;
            foreach ($organizations as $organization){
                $credentialings=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('organization'=>$organization->getId()));

                if($credentialings!=null){

                    echo $cont." ".$organization->getName()."<br/>";
                    $cont++;
                }

            }

            die();
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_email",methods={"POST","DELETE"})
     */
    public function delete(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $email = $em->getRepository('App\Entity\EmailQueued')->find($id);
            $removed = 0;
            $message = "";

            if ($email) {
                try {
                    $em->remove($email);
                    $em->flush();
                    $removed = 1;
                    $message = "The Email Queued has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Email Queued can't be removed";
                }
            }

            $this->SaveLog(3,'Email Queued',$id,'Delete email Queued');

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_email",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $cvo= $em->getRepository('App\Entity\EmailQueued')->find($id);

                if ($cvo) {
                    try {
                        $em->remove($cvo);
                        $em->flush();
                        $removed = 1;
                        $message = "The Email queue has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Email queue can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/add_email_queued_ajax2", name="admin_add_email_queued_ajax2")
     */
    public function addEmailQueuedAjax2(Request $request){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();
        $ids = $request->get('ids');
        $template=$request->get('template');
        $datetosend=$request->get('datetosend');
        $id_organization=$request->get('organization');

        if($ids!=null){
            foreach ($ids as $id) {
                $contact  = $em->getRepository('App\Entity\OrganizationContact')->find($id);
                $organization=$contact->getOrganization();

                if ($contact) {
                    $providerResults="";
                    $contMMM=0;
                    if($template==5){
                        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));
                        if($providers!=null){
                            foreach ($providers as $provider){
                                $payers=$provider->getPayers();
                                if($payers!=null){
                                    foreach ($payers as $payer){
                                        if($payer->getId()==1){
                                            $contMMM++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if($template==4){
                        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));
                        if($this->OrgIsFacility($organization)==true){
                            if($providers!=null){
                                foreach ($providers as $provider){
                                    if($provider!=null){
                                        $providerResults.=$provider->getId().",";
                                    }
                                }
                            }
                        }else{
                            $providers=$em->getRepository('App:Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));
                            if($providers!=null){
                                foreach ($providers as $provider){
                                    if($provider->getStatusCred()=="Approved"){
                                        $providerResults.=$provider->getId().",";
                                    }
                                }
                            }
                        }
                    }else{
                        if($this->OrgIsFacility($organization)==true){
                            $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));
                            if($providers!=null){
                                foreach ($providers as $provider){
                                    if($provider!=null){
                                        $providerResults.=$provider->getId().",";
                                    }
                                }
                            }
                        }else{
                            $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));

                            if($providers!=null){
                                foreach ($providers as $provider){
                                    $payers=$provider->getPayers();
                                    if($payers!=null){
                                        foreach ($payers as $payer){
                                            if($payer->getId()==1){
                                                $providerResults.=$provider->getId().",";
                                            }
                                        }
                                    }

                                }
                            }
                        }
                    }

                    $providerResults=substr($providerResults,0,-1);

                    if($template==6 or $template==2 or $template==3){
                        if($contact!=null){
                            $email=$contact->getEmail();
                            if($email!="" and $email!=null){
                                $emailqueed_exits=$em->getRepository('App\Entity\EmailQueued')->findBy(array('sent_to'=>$email,'email_type'=>$template));

                                if($emailqueed_exits==null or count($emailqueed_exits)==0){
                                    $emailQueue=new EmailQueued();
                                    $status=$em->getRepository('App\Entity\EmailStatus')->find(1);
                                    $email_type=$em->getRepository('App\Entity\EmailType')->find($template);

                                    $emailQueue->setStatus($status);
                                    $emailQueue->setOrganization($organization);
                                    $emailQueue->setEmailType($email_type);
                                    $emailQueue->setDateToSend(new \DateTime($datetosend));
                                    $emailQueue->setSentTo($contact->getEmail());
                                    $emailQueue->setContact($contact);
                                    $emailQueue->setProviders($providerResults);

                                    $em->persist($emailQueue);
                                    $em->flush();
                                }
                            }
                        }
                    }
                    if($contact!=null and $providerResults!=""){
                        $email=$contact->getEmail();

                        if($template==4 and $contMMM>0){
                            if($email!="" and $email!=null){
                                $emailqueed_exits=$em->getRepository('App\Entity\EmailQueued')->findBy(array('sent_to'=>$email,'email_type'=>$template));

                                if($emailqueed_exits==null or count($emailqueed_exits)==0){
                                    $emailQueue=new EmailQueued();
                                    $status=$em->getRepository('App\Entity\EmailStatus')->find(1);
                                    $email_type=$em->getRepository('App\Entity\EmailType')->find($template);

                                    $emailQueue->setStatus($status);
                                    $emailQueue->setOrganization($organization);
                                    $emailQueue->setEmailType($email_type);
                                    $emailQueue->setDateToSend(new \DateTime($datetosend));
                                    $emailQueue->setSentTo($contact->getEmail());
                                    $emailQueue->setContact($contact);
                                    $emailQueue->setProviders($providerResults);

                                    $em->persist($emailQueue);
                                    $em->flush();
                                }
                            }
                        }else{
                            if($email!="" and $email!=null){
                                $emailqueed_exits=$em->getRepository('App\Entity\EmailQueued')->findBy(array('sent_to'=>$email,'email_type'=>$template));

                                if($emailqueed_exits==null or count($emailqueed_exits)==0){
                                    $emailQueue=new EmailQueued();
                                    $status=$em->getRepository('App\Entity\EmailStatus')->find(1);
                                    $email_type=$em->getRepository('App\Entity\EmailType')->find($template);

                                    $emailQueue->setStatus($status);
                                    $emailQueue->setOrganization($organization);
                                    $emailQueue->setEmailType($email_type);
                                    $emailQueue->setDateToSend(new \DateTime($datetosend));
                                    $emailQueue->setSentTo($contact->getEmail());
                                    $emailQueue->setContact($contact);
                                    $emailQueue->setProviders($providerResults);

                                    $em->persist($emailQueue);
                                    $em->flush();
                                }
                            }
                        }
                    }
                }
            }
        }

        if($id_organization!="") {
            $organization = $em->getRepository('App\Entity\Organization')->find($id_organization);

            if ($organization) {
                $providerResults = "";
                if ($this->OrgIsFacility($organization) == true) {
                    $providers = $em->getRepository('App\Entity\Provider')->findBy(array('organization' => $organization->getId()));
                    if ($providers != null) {
                        foreach ($providers as $provider) {
                            if ($provider != null) {
                                $providerResults .= $provider->getId() . ",";
                            }
                        }
                    }
                } else {
                    $providers = $em->getRepository('App\Entity\Provider')->findBy(array('organization' => $organization->getId()));

                    if ($providers != null) {
                        foreach ($providers as $provider) {
                            $payers = $provider->getPayers();
                            if ($payers != null) {
                                foreach ($payers as $payer){
                                    if ($payer->getId() == 1) {
                                        $providerResults .= $provider->getId() . ",";
                                    }
                                }
                            }
                        }
                    }
                }

                $providerResults = substr($providerResults, 0, -1);

                if ($ids != null) {
                    foreach ($ids as $id) {
                        $contact = $em->getRepository('App\Entity\OrganizationContact')->find($id);
                        $email = $contact->getEmail();

                        if ($email != "" and $email != null) {
                            $emailqueed_exits = $em->getRepository('App\Entity\EmailQueued')->findBy(array('sent_to' => $email, 'email_type' => $template));

                            if ($emailqueed_exits == null or count($emailqueed_exits) == 0) {
                                $emailQueue = new EmailQueued();
                                $status = $em->getRepository('App\Entity\EmailStatus')->find(1);
                                $email_type = $em->getRepository('App\Entity\EmailType')->find($template);

                                $emailQueue->setStatus($status);
                                $emailQueue->setOrganization($organization);
                                $emailQueue->setEmailType($email_type);
                                $emailQueue->setDateToSend(new \DateTime($datetosend));
                                $emailQueue->setSentTo($email);
                                $emailQueue->setContact($contact);
                                $emailQueue->setProviders($providerResults);

                                $em->persist($emailQueue);
                                $em->flush();
                            }
                        }
                    }
                }

            }
        }

        return new Response(
            json_encode(array('id' => 1)), 200, array('Content-Type' => 'application/json')
        );
    }

    private function SaveLog($action_id,$entity,$entity_id,$note){
        $em=$this->getDoctrine()->getManager();
        $user=$this->getUser();

        $log=new ChangeLog();
        $log->setUser($user);
        $action=$em->getRepository('App\Entity\ActionLog')->find($action_id);
        $log->setAction($action);
        $log->setEntityId($entity_id);
        $log->setEntityClass($entity);
        $log->setNote($note);

        $em->persist($log);
        $em->flush();
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }

    /**
     * @Route("/add_email_queued_prospect_ajax", name="admin_add_email_queued_prospect_ajax",methods={"POST","DELETE"})
     */
    public function addEmailQueuedProspectAjax(Request $request){

        $em=$this->getDoctrine()->getManager();
        $ids = $request->get('ids');
        $template=$request->get('template');
        $datetosend=$request->get('datetosend');

        if($ids!=null){
            foreach ($ids as $id) {
                $prospect=$em->getRepository('App\Entity\Prospect')->find($id);

                if($prospect!=null){
                    $email=$prospect->getEmail();
                    if($email!="" and $email!=null){
                        $emailQueue=new EmailQueued();
                        $status=$em->getRepository('App\Entity\EmailStatus')->find(1);
                        $email_type=$em->getRepository('App\Entity\EmailType')->find($template);

                        $emailQueue->setStatus($status);
                        $emailQueue->setOrganization(null);
                        $emailQueue->setOrganizationName($prospect->getName());
                        $emailQueue->setEmailType($email_type);
                        $emailQueue->setDateToSend(new \DateTime($datetosend));
                        $emailQueue->setSentTo($email);
                        $emailQueue->setContact(null);
                        $emailQueue->setProviders(null);
                        $emailQueue->setProspect($prospect);

                        $em->persist($emailQueue);
                    }
                }
            }
            $em->flush();
        }

        return new Response(
            json_encode(array('id' => $template,'datetosend'=>$datetosend)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/send_email_queued_ajax", name="admin_send_email_queued_ajax")
     */
    public function sendEmailQueuedAjax(Request $request,\Swift_Mailer $mailer){

        $em=$this->getDoctrine()->getManager();
        $id=$request->get('id');
        $queued=$em->getRepository('App\Entity\EmailQueued')->find($id);
        $errors=0;

        $email_to=$queued->getSentTo();
        $template=$queued->getEmailType()->getId();
        $templateObj=$queued->getEmailType();
        $date=date('F d,Y');

        //email for Credentialing MMM Notification
        if($template==1){
            $providersId=$queued->getProviders();
            $providersIdArray=explode(',',$providersId);

            $pathFile=$this->path."/public/attachment/BSN-MMM-Plan-Addendum-Provider.pdf";

            $providersArray=array();
            foreach ($providersIdArray as $id){
                $provider= $em->getRepository('App\Entity\Provider')->find($id);
                if($provider!=null){
                    $providersArray[]=$provider;
                }
            }

            try {
                $message = (new \Swift_Message('Re: MMM of Florida, Inc.'))
                    ->setFrom('info@bsnnet.com')
                    ->setTo($email_to)
                    ->setBody(
                        $this->renderView(
                            'email/templates/mmm-notification.html.twig',
                            array('date' =>$date,'queued'=>$queued,'providers'=>$providersArray)
                        ),
                        'text/html'
                    )
                    ->attach(\Swift_Attachment::fromPath($pathFile)->setFilename('BSN-MMM-Plan-Addendum-Provider.pdf'));
                $mailer->send($message);
            } catch (Exception $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }
        }

        //email for Executed Contract
        if($template==2){
            $folderId=$queued->getOrganization()->getId();
            $pathFile=$this->path."/public/uploads/organizations/".$folderId."/provider_agreement_file_".$folderId.".pdf";

            //get the organization is facility or not
            $address= $em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$folderId));
            $isFacility=false;
            if($address!=null){
                foreach ($address as $addr){
                    if($addr->getIsFacility()==true){
                        $isFacility=true;
                    }
                }
            }

            $path_amendment="";
            if($isFacility==true){
                $path_amendment=$this->path."/public/uploads/organizations/amendment/BSN_FACC/BSN_FACC_".$folderId.".pdf";
            }else{
                $path_amendment=$this->path."/public/uploads/organizations/amendment/BSN_PACC/BSN_PACC_".$folderId.".pdf";
            }

            if(file_exists($pathFile)){
                $message = (new \Swift_Message('Behavioral Services Network Executed Contract'))
                    ->setFrom('info@bsnnet.com')
                    ->setTo($email_to)
                    ->setBody(
                        $this->renderView(
                            'email/templates/excecuted-contract.html.twig',array('queued'=>$queued)
                        ),
                        'text/html'
                    )
                    ->attach(\Swift_Attachment::fromPath($pathFile)->setFilename('Provider_agreement_file.pdf'));
                $mailer->send($message);
            }else{
                $message = (new \Swift_Message('Behavioral Services Network Executed Contract'))
                    ->setFrom('info@bsnnet.com')
                    ->setTo($email_to)
                    ->setBody(
                        $this->renderView(
                            'email/templates/excecuted-contract.html.twig',array('queued'=>$queued)
                        ),
                        'text/html'
                    );
                $mailer->send($message);
            }
        }

        // Send email for Provider Portal Login Notification
        if($template==3){
            $message = (new \Swift_Message('Re: Registration for BSN Provider Portal for ('.$queued->getOrganization()->getName().")"))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->renderView(
                        'email/templates/provider-login-notification.html.twig',
                        array('date' =>$date,'queued'=>$queued)
                    ),
                    'text/html'
                );
            $mailer->send($message);
        }

        // Send email for Credentialing Notification
        if($template==4){
            $pathFile=$this->path."/public/attachment/BSN-PROVIDER-ROSTER-TEMPLATE-MULTIPLE.xlsx";
            $providersId=$queued->getProviders();
            $providersIdArray=explode(',',$providersId);

            $providersArray=array();
            foreach ($providersIdArray as $id){
                $provider= $em->getRepository('App\Entity\Provider')->find($id);
                if($provider!=null){
                    $providersArray[]=$provider;
                }
            }

            $effective_date="";
            $approved_date=$queued->getApprovedDate();
            $decision_date_array=explode('/',$approved_date);
            if($approved_date!=null and $approved_date!=""){
                $year=$decision_date_array[2];
                $moth=$decision_date_array[0];
                if($moth=='01'){
                    $effective_date="02/01/".$year;
                }
                if($moth=='02'){
                    $effective_date="03/01/".$year;
                }
                if($moth=='03'){
                    $effective_date="04/01/".$year;
                }
                if($moth=='04'){
                    $effective_date="05/01/".$year;
                }
                if($moth=='05'){
                    $effective_date="06/01/".$year;
                }
                if($moth=='06'){
                    $effective_date="07/01/".$year;
                }
                if($moth=='07'){
                    $effective_date="08/01/".$year;
                }
                if($moth=='08'){
                    $effective_date="09/01/".$year;
                }
                if($moth=='09'){
                    $effective_date="10/01/".$year;
                }
                if($moth=='10'){
                    $effective_date="11/01/".$year;
                }
                if($moth=='11'){
                    $effective_date="12/01/".$year;
                }
                if($moth=='12'){
                    $effective_date="01/01/".$year+1;
                }
            }else{
                $effective_date="";
            }

            $org_id=$queued->getOrganization()->getId();
            $conn =  $em->getConnection();

            $sql="SELECT IF(SUM(IF(ba.is_facility, 1, 0)) > 0, 'Yes', 'No') AS `is_facility`
            FROM  organization o 
            LEFT JOIN billing_address ba on o.id = ba.organization_id
            WHERE o.disabled=0 and o.id= ".$org_id."
            GROUP BY o.id
            ORDER BY o.name";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $organizations= $stmt->fetchAll();

            $isFacility=0;
            foreach ($organizations as $org){
                if($org['is_facility']=='Yes'){
                    $isFacility=1;
                }
            }

            try {
                $message = (new \Swift_Message('Re: Behavioral Services Network Credentialing Decision'))
                    ->setFrom('info@bsnnet.com')
                    ->setTo($email_to)
                    ->setBody(
                        $this->renderView(
                            'email/templates/credentialing-notification.html.twig',
                            array('date' =>$date,'queued'=>$queued,'is_facility'=>$isFacility,'providers'=>$providersArray,'effective_date'=>$effective_date,'approved_date'=>$approved_date)
                        ),
                        'text/html'
                    );
                $mailer->send($message);
            } catch (Exception $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }
        }

        // Send email for MMM Provider Survey
        if($template==5){
            $pathFile=$this->path."/public/attachment/MMMFL-Provider-Survey-2020-fillable.pdf";

            $message = (new \Swift_Message('Re: Provider Satisfaction Survey - 2020'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->renderView(
                        'email/templates/mmm-survey.html.twig',
                        array('date' =>$date,'queued'=>$queued)
                    ),
                    'text/html'
                )
                ->attach(\Swift_Attachment::fromPath($pathFile)->setFilename('MMMFL-Provider-Survey-2020-fillable.pdf'));
            $mailer->send($message);
        }

        // Send email for network announcement
        if($template==6){
            $message = (new \Swift_Message('Things to come.'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->renderView(
                        'email/templates/network-announcement.html.twig',
                        array('date' =>$date,'queued'=>$queued)
                    ),
                    'text/html'
                );
            $mailer->send($message);
        }

        // Send email for network announcement
        if($template==7){
            $message = (new \Swift_Message('Re: Florida Community Care.'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->renderView(
                        'email/templates/fcc-participation-notification.html.twig',
                        array('date' =>$date,'queued'=>$queued)
                    ),
                    'text/html'
                );
            $mailer->send($message);
        }

        // Send email for denied credentialing
        if($template==8){
            $providersId=$queued->getProviders();
            $providersIdArray=explode(',',$providersId);

            $providersArray=array();
            foreach ($providersIdArray as $id){
                $provider= $em->getRepository('App\Entity\Provider')->find($id);
                if($provider!=null){
                    $credentialing=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('npi_number'=>$provider->getNpiNumber(),'credentialing_status'=>6 ));
                    if($credentialing!=null){
                        $providersArray[]=$provider;
                    }
                }
            }

            $message = (new \Swift_Message('Re: Credentialing decision.'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->renderView(
                        'email/templates/credentialing_denied_notification.html.twig',
                        array('date' =>$date,'queued'=>$queued,'providers'=>$providersArray)
                    ),
                    'text/html'
                );
            $mailer->send($message);
        }

        // Send email for FCC Welcome go live letter template
        if($template==9){

            $date=date('F d, Y');
            $pathFile1=$this->path."/public/attachment/BSN-FCC-Plan-Addendum.pdf";
            $pathFile2=$this->path."/public/attachment/FCC-Quick-Reference-Guide-BSN-FINAL.pdf";

            $message = (new \Swift_Message('Re: Credentialing decision.'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->renderView(
                        'email/templates/fcc_welcome_go_live.html.twig',
                        array('date' =>$date,'queued'=>$queued,'date'=>$date)
                    ),
                    'text/html'
                )
                ->attach(\Swift_Attachment::fromPath($pathFile1)->setFilename('BSN FCC Plan Addendum.pdf'))
                ->attach(\Swift_Attachment::fromPath($pathFile2)->setFilename('FCC Quick Reference Guide BSN.pdf'));
            $mailer->send($message);
        }

        // Send email for Holiday message
        if($template==10){
            $message = (new \Swift_Message('BSNnet: Holiday message.'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->renderView(
                        'email/templates/holiday_message.html.twig'
                    ),
                    'text/html'
                );
            $mailer->send($message);
        }

        // Send email for Missing Information on Provider portal
        if($template==11){
            $message = (new \Swift_Message('BSNnet: Missing information in Provider Portal.'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->renderView(
                        'email/templates/missing_information_provider_portal.html.twig', array('queued'=>$queued)
                    ),
                    'text/html'
                );
            $mailer->send($message);
        }

        // Send email for AHCANotification
        if($template==12){
            $message = (new \Swift_Message('BSNnet: AHCA Announcment.'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->renderView(
                        'email/templates/ahca_bsn_notification.html.twig', array('queued'=>$queued)
                    ),
                    'text/html'
                );
            $mailer->send($message);
        }

        // Send email forDecline Request to join to network
        if($template==13){
            $message = (new \Swift_Message('BSNnet: Decline request to join network.'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->renderView(
                        'email/templates/decline_request_participation.html.twig', array('queued'=>$queued)
                    ),
                    'text/html'
                );
            $mailer->send($message);
        }

        // Send email for Network Opportunity
        if($template==14){
            $organization_name=$queued->getOrganizationName();
            if($organization_name==""){
                $contact=$queued->getContact();
                if($contact){
                    $organization_name=$queued->getContact()->getName();
                }
            }

            $message = (new \Swift_Message('BSNNet: Network Opportunity.'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->renderView(
                        'email/templates/provider_marketing_letter.html.twig',array('queued'=>$queued,'organization_name'=>$organization_name)
                    ),
                    'text/html'
                );
            $mailer->send($message);
        }

        // Year Start refresh FCC
        if($template==15){
            $pathFile=$this->path."/public/attachment/FCC_Quick_Reference_Guide_BSN_FINAL.pdf";



            $message = (new \Swift_Message('Network Refresh - FCC'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->renderView(
                        'email/templates/year_start_fcc.html.twig'
                    ),
                    'text/html'
                )
                ->attach(\Swift_Attachment::fromPath($pathFile)->setFilename('FCC_Quick_Reference_Guide_BSN_FINAL.pdf'));
            $mailer->send($message);

        }

        // Year Start refresh FCC
        if($template==16){
            $pathFile=$this->path."/public/attachment/MMM_FL_WAN_Quick_Referen_Guide_BSN.pdf";
            $message = (new \Swift_Message('Network Refresh - MMM'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->renderView(
                        'email/templates/year_start_mmm.html.twig'
                    ),
                    'text/html'
                )
                ->attach(\Swift_Attachment::fromPath($pathFile)->setFilename('MMM_FL_WAN_Quick_Referen_Guide_BSN.pdf'));
            $mailer->send($message);

        }
        // Year Start refresh FCC
        if($template==17){
            $date=date('m/d/Y');
            $message = (new \Swift_Message('Important Notice Regarding MMM of Florida Claims Submission'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->renderView(
                        'email/templates/mmm_update_provider_manual.html.twig',array('date'=>$date)
                    ),
                    'text/html'
                );
            $mailer->send($message);

        }

        // Notice of Termination Due to Adverse Action
        if($template==18){
            $date=date('m/d/Y');
            $message = (new \Swift_Message('Notice of Termination Due to Adverse Action'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->renderView(
                        'email/templates/termination_due_to_adverse_action.html.twig',array('date'=>$date,'queued'=>$queued)
                    ),
                    'text/html'
                );
            $mailer->send($message);

        }

        // To all MMM Providers including Behavioral Health
        if($template==19){
            $date=date('m/d/Y');
            $message = (new \Swift_Message('To all MMM Providers including Behavioral Health'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->renderView(
                        'email/templates/provider_notification_psr_tcm.html.twig',array('date'=>$date)
                    ),
                    'text/html'
                );
            $mailer->send($message);
        }

        // Directory Update
        if($template==20){
            $message = (new \Swift_Message('Re: Urgent Reminder: Make Sure Your Provider Directory Information Is Accurate and Up-to-Date'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->renderView(
                        'email/templates/urgent_reminder.html.twig'),
                    'text/html'
                );
            $mailer->send($message);
        }

        //email for FCC Plan Addendum Letter
        if($template==21){
            $providersId=$queued->getProviders();
            $providersIdArray=explode(',',$providersId);

            $pathFile=$this->path."/public/attachment/BSN-FCC-Plan-Addendum.pdf";

            $providersArray=array();
            foreach ($providersIdArray as $id){
                $provider= $em->getRepository('App\Entity\Provider')->find($id);
                if($provider!=null){
                    $providersArray[]=$provider;
                }
            }

            try {
                $message = (new \Swift_Message('Re: Florida Community Care.'))
                    ->setFrom('info@bsnnet.com')
                    ->setTo($email_to)
                    ->setBody(
                        $this->renderView(
                            'email/templates/fcc-plan-addendum-letter.html.twig',
                            array('date' =>$date,'queued'=>$queued,'providers'=>$providersArray)
                        ),
                        'text/html'
                    )
                    ->attach(\Swift_Attachment::fromPath($pathFile)->setFilename('BSN-FCC-Plan-Addendum.pdf'));
                $mailer->send($message);
            } catch (Exception $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }

        }

        //email for BSN Termination without Reason
        if($template==22){
            $subject=$templateObj->getSubject();
            $cc=$templateObj->getCc();
            $bcc=$templateObj->getBcc();
            $organization=$queued->getOrganization();
            $plan=$em->getRepository('App:Payer')->findOneBy(array('name'=>$queued->getHealthPlan()));
            $termination_date="";
            $date=date('F d, Y');

            if($organization){
                $payerExcluded=$em->getRepository('App:OrganizationPayerExcluded')->findOneBy(array('organization'=>$organization->getId(),'payer'=>$plan->getId()));
                if($payerExcluded){
                    $termination_date=$payerExcluded->getCreateAt()->format('m/d/Y');
                }
            }

            try {
                $message = (new \Swift_Message($subject))
                    ->setFrom('info@bsnnet.com')
                    ->setTo($email_to)
                    ->setBody(
                        $this->renderView(
                            'email/templates/bsn_termination_without_reason.html.twig',
                            array('date' =>$date,'queued'=>$queued,'termination_date'=>$termination_date)
                        ),
                        'text/html'
                    );

                if($cc!=""){
                    $message->addCc($cc);
                }
                if($bcc!=""){
                    $message->addBc($bcc);
                }
                $mailer->send($message);


            } catch (Exception $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }
        }

        //email for FCC- TCM policy communication
        if($template==23){
            $subject=$templateObj->getSubject();
            $cc=$templateObj->getCc();
            $bcc=$templateObj->getBcc();

            $pathFile=$this->path."/public/attachment/Targeted_Case_Management_AHCA_HCBS_MMA.pdf";

            try {
                $message = (new \Swift_Message($subject))
                    ->setFrom('info@bsnnet.com')
                    ->setTo($email_to)
                    ->setBody(
                        $this->renderView(
                            'email/templates/fcc_tcm_policy_communication.html.twig',
                            array('queued' =>$queued)
                        ),
                        'text/html'
                    )
                    ->attach(\Swift_Attachment::fromPath($pathFile)->setFilename('Targeted_Case_Management_AHCA_HCBS_MMA.pdf'));

                if($cc!=""){
                    $message->addCc($cc);
                }
                if($bcc!=""){
                    $message->addBc($bcc);
                }

                $mailer->send($message);


            } catch (Exception $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }
        }

        //email for BSN Deem Letter
        if($template==24){
            $subject=$templateObj->getSubject();
            $cc=$templateObj->getCc();
            $bcc=$templateObj->getBcc();
            $organization=$queued->getOrganization();

            $date=date('F d, Y');

            $pathFile= $this->getParameter('kernel.project_dir')."/public/docs/organizations/organizations-amendment/".$organization->getId()."/BSN_Provider_Amendment_Contract_Changes_090821.pdf";

            try {
                $message = (new \Swift_Message($subject))
                    ->setFrom('info@bsnnet.com')
                    ->setTo($email_to)
                    ->setBody(
                        $this->renderView(
                            'email/templates/bcn-deem-letter.html.twig',
                            array('queued'=>$queued,'date'=>$date)
                        ),
                        'text/html'
                    );

                if($cc!=""){
                    $message->addCc($cc);
                }
                if($bcc!=""){
                    $message->addBc($bcc);
                }

                $message->attach(\Swift_Attachment::fromPath($pathFile)->setFilename('BSN_Provider_Amendment_Contract_Changes_090821.pdf'));

                $mailer->send($message);
            } catch (Exception $e) {
                echo 'Excepción capturada: ',  $e->getMessage(), "\n";
            }

        }

        if($errors==0){
            $status=$em->getRepository('App\Entity\EmailStatus')->find(2);
            $emailLog=new EmailLog();
            $emailLog->setSentTo($queued->getSentTo());
            $emailLog->setEmailType($queued->getEmailType());
            $emailLog->setOrganization($queued->getOrganization());
            $emailLog->setStatus($status);
            $emailLog->setVerificationHass($queued->getVerificationHass());
            $emailLog->setContact($queued->getContact());
            $emailLog->setProviders($queued->getProviders());
            $emailLog->setApprovedDate($queued->getApprovedDate());
            $em->persist($emailLog);

            //remove email queued
            $em->remove($queued);
            $em->flush();
        }

        return new Response(
            json_encode(array('id' => $id,'template'=>$template)), 200, array('Content-Type' => 'application/json')
        );
    }
}
