<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\PaymentTier;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/paymenttier")
 */
class PaymentTierController extends AbstractController{

    /**
     * @Route("/index/{id}", name="admin_paymenttier", defaults={"id": null})
     */
    public function index($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $paymenttiers = $em->getRepository('App\Entity\PaymentTier')->findBy(array('payer' => $id));
            $payer = $em->getRepository('App\Entity\Payer')->find($id);

            $delete_form_ajax = $this->createCustomForm('PAYMENTTIER_ID', 'DELETE', 'admin_delete_paymenttier');

            return $this->render('paymenttier/index.html.twig', array('paymenttiers' => $paymenttiers, 'payer' => $payer, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new/{id}", name="admin_new_paymenttier", defaults={"id": null})
     */
    public function add($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $payer = $em->getRepository('App\Entity\Payer')->find($id);

            return $this->render('paymenttier/add.html.twig', array('payer' => $payer));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_paymenttier", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\PaymentTier')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_paymenttier');
            }

            return $this->render('paymenttier/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_paymenttier", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\PaymentTier')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_paymenttier');
            }

            return $this->render('paymenttier/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_paymenttier")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $value = $request->get('value');
            $payer = $request->get('payer');
            $description = $request->get('description');
            $new = $request->get('new');

            $paymenttier = new PaymentTier();
            $paymenttier->setName($name);
            $paymenttier->setValue($value);
            $paymenttier->setPayer($em->getRepository('App\Entity\Payer')->find($payer));
            $paymenttier->setDescription($description);

            $em->persist($paymenttier);
            $em->flush();

            $this->addFlash(
                'success',
                'The Payment Tier has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_payer', ['id' => $payer]);
            }

            return $this->redirectToRoute('admin_paymenttier', ['id' => $payer]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_paymenttier")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $value = $request->get('value');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');
            $payer = $request->get('payer');

            $paymenttier = $em->getRepository('App\Entity\PaymentTier')->find($id);

            if ($paymenttier == null) {
                $this->addFlash(
                    "danger",
                    "The Payment Tier can't been updated!"
                );

                return $this->redirectToRoute('admin_paymenttier');
            }

            if ($paymenttier != null) {
                $paymenttier->setName($name);
                $paymenttier->setValue($value);
                $paymenttier->setDescription($description);

                $em->persist($paymenttier);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "The Payment Tier has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_paymenttier', ['id' => $payer]);
            }

            return $this->redirectToRoute('admin_paymenttier', ['id' => $payer]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_paymenttier",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $paymenttier = $paymenttier = $em->getRepository('App\Entity\PaymentTier')->find($id);
            $removed = 0;
            $message = "";

            if ($paymenttier) {
                try {
                    $em->remove($paymenttier);
                    $em->flush();
                    $removed = 1;
                    $message = "The Payment Tier has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Payment Tier can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_paymenttier",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $paymenttier = $paymenttier = $em->getRepository('App\Entity\PaymentTier')->find($id);

                if ($paymenttier) {
                    try {
                        $em->remove($paymenttier);
                        $em->flush();
                        $removed = 1;
                        $message = "The Payment Tier has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The PaymentTier can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
