<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\TaxonomyCode;

/**
 * @Route("/admin/taxonomy")
 */
class TaxonomyCodeController extends AbstractController{

    /**
     * @Route("/index", name="admin_taxonomy")
    */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $taxonomys = $em->getRepository('App\Entity\TaxonomyCode')->findAll();

            $delete_form_ajax = $this->createCustomForm('CVO_ID', 'DELETE', 'admin_delete_taxonomy');


            return $this->render('taxonomy_code/index.html.twig', array('taxonomys' => $taxonomys, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_taxonomy")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('taxonomy_code/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_taxonomy", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\TaxonomyCode')->find($id);

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('admin_taxonomy');
            }

            return $this->render('taxonomy_code/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_taxonomy", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\TaxonomyCode')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_taxonomy');
            }

            return $this->render('taxonomy_code/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_taxonomy")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $code = $request->get('code');
            $classification = $request->get('classification');
            $specialization = $request->get('specialization');
            $grouping = $request->get('grouping');

            $new = $request->get('new');

            $taxonomy = new TaxonomyCode();
            $taxonomy->setCode($code);
            $taxonomy->setClassification($classification);
            $taxonomy->setSpecialization($specialization);
            $taxonomy->setGrouping($grouping);


            $em->persist($taxonomy);
            $em->flush();

            $this->addFlash(
                'success',
                'TaxonomyCode has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_taxonomy');
            }

            return $this->redirectToRoute('admin_taxonomy');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_taxonomy")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $code = $request->get('code');
            $classification = $request->get('classification');
            $specialization = $request->get('specialization');
            $grouping = $request->get('grouping');

            $new = $request->get('new');
            $id = $request->get('id');

            $taxonomy = $em->getRepository('App\Entity\TaxonomyCode')->find($id);

            if ($taxonomy == null) {
                $this->addFlash(
                    "danger",
                    "The TaxonomyCode can't been updated!"
                );

                return $this->redirectToRoute('admin_taxonomy');
            }

            if ($taxonomy != null) {
                $taxonomy->setCode($code);
                $taxonomy->setClassification($classification);
                $taxonomy->setSpecialization($specialization);
                $taxonomy->setGrouping($grouping);

                $em->persist($taxonomy);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "TaxonomyCode has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_taxonomy');
            }

            return $this->redirectToRoute('admin_taxonomy');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_taxonomy",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $taxonomy = $taxonomy = $em->getRepository('App\Entity\TaxonomyCode')->find($id);
            $removed = 0;
            $message = "";

            if ($taxonomy) {
                try {
                    $em->remove($taxonomy);
                    $em->flush();
                    $removed = 1;
                    $message = "The TaxonomyCode has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The taxonomy can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_taxonomy",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $taxonomy= $em->getRepository('App\Entity\TaxonomyCode')->find($id);

                if ($taxonomy) {
                    try {
                        $em->remove($taxonomy);
                        $em->flush();
                        $removed = 1;
                        $message = "The TaxonomyCode has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The TaxonomyCode can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }

    /**
     * @Route("/remove-duplicates", name="admin_remove_duplicates_taxonomy")
     */
    public function removeDuplicates(Request $request){
        $em = $this->getDoctrine()->getManager();

        $taxonomies= $em->getRepository('App\Entity\TaxonomyCode')->findAll();

        foreach ($taxonomies as $taxonomy){
            $current_code=$taxonomy->getCode();
            $current_specialization=$taxonomy->getSpecialization();

            $cont_match=0;
            foreach ($taxonomies as $taxonomy2){
                if($taxonomy2->getCode()==$current_code and $taxonomy2->getSpecialization()==$current_specialization){
                    $cont_match++;
                }
            }

            if($cont_match>1){
                $taxonomies_toremove= $em->getRepository('App\Entity\TaxonomyCode')->findBy(array('code'=>$current_code,'Specialization'=>$current_specialization));

                $totalTax=count($taxonomies_toremove);


               foreach ($taxonomies_toremove as $item){
                   if($totalTax>1){
                       $em->remove($item);
                       $em->flush();
                       $totalTax--;
                   }
               }
            }
        }

        die();
    }
}
