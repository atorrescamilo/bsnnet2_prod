<?php

namespace App\Controller;


use App\Entity\AddressFacilityLicense;
use App\Entity\ChangeLog;
use App\Entity\EmailQueued;
use App\Entity\OrganizationLineofBusiness;
use App\Entity\OrganizationPayerExcluded;
use App\Entity\OrganizationRates;
use App\Entity\OrganizationTaxonomy;
use App\Entity\OrganizationValidateRoster;
use App\Form\OrganizationPType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Organization;
use App\Entity\Provider;
use App\Service\OrganizationFileUploader;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use PhpOffice\PhpSpreadsheet\Reader;


/**
 * @Route("/admin/organization")
 */
class OrganizationController extends AbstractController{

    /**
     * @Route("/index", name="admin_organization")
     *
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
           return $this->redirectToRoute('admin_organization_list');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/list", name="admin_organization_list")
     *
     */
    public function index2(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $delete_form_ajax = $this->createCustomForm('ORGANIZATION_ID', 'DELETE', 'admin_delete_organization');

            $organizationsF=array();
            $email_templates=$em->getRepository('App\Entity\EmailType')->findBy(array(),array('name'=>'ASC'));

            $sql="SELECT o.id, o.name, o.bda_name, o.provider_type, o.group_npi,o.disabled,      
            COUNT(DISTINCT provider.id) AS provider_count,
            COUNT(DISTINCT user.id) AS user_count,
            GROUP_CONCAT(DISTINCT `org_specialty`.`name` ORDER BY `org_specialty`.`name` ASC SEPARATOR ', ') AS `org_clasification`,
            organization_status.name AS status,
            o.created_on,
            GROUP_CONCAT(DISTINCT taxonomy_code.specialization ORDER BY taxonomy_code.id ASC SEPARATOR ', ') AS taxonomies_specialty,
            GROUP_CONCAT(DISTINCT `payer`.`name` ORDER BY `payer`.`name` ASC SEPARATOR ', ') AS `payer_str`,
            IF(SUM(IF(ba.is_facility, 1, 0)) > 0, 'Yes', 'No') AS `is_facility`,
            o.provider_agreement_file, o.roster_file, o.accreditation_certificate_file,
            GROUP_CONCAT(DISTINCT address_facility_license.facility_type ORDER BY address_facility_license.id ASC SEPARATOR ', ') AS facility_type
            FROM  organization o 
            LEFT JOIN provider ON provider.org_id = o.id
            LEFT JOIN `user` ON `user`.organization_id = o.id
            LEFT JOIN billing_address ba on o.id = ba.organization_id
            LEFT JOIN address_facility_license on address_facility_license.address_id= ba.id      
            LEFT JOIN organization_taxonomy ON organization_taxonomy.organization_id = o.id
            LEFT JOIN organization_status ON organization_status.id = o.status_id
            LEFT JOIN organization_org_specialties ON organization_org_specialties.organization_id =o.id    
            LEFT JOIN org_specialty ON org_specialty.id =organization_org_specialties.org_specialty_id    
            LEFT JOIN taxonomy_code ON taxonomy_code.id = organization_taxonomy.taxonomy_id
            LEFT JOIN provider_payer ON provider_payer.provider_id = provider.id
            LEFT JOIN payer ON payer.id = provider_payer.payer_id
            GROUP BY o.id
            ORDER BY o.name";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $organizations= $stmt->fetchAll();

            $terminationreasons=$em->getRepository('App\Entity\OrgTerminationReason')->findAll();

            $payers=$em->getRepository('App\Entity\Payer')->findAll();

            return $this->render('organization/list.html.twig',
                array('organizationsF'=>$organizationsF,'terminationreasons'=>$terminationreasons,
                    'organizations' => $organizations,
                    'payers'=>$payers,
                    'delete_form_ajax' => $delete_form_ajax->createView(),
                    'email_templates'=>$email_templates));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/ag_report", name="admin_organization_agreport")
    */
    public function aggrid(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $organizations = $em->getRepository('App\Entity\Organization')->findBy(array(),array('name'=>'ASC'));
            $organizationsF=array();

            foreach ($organizations as $organization){
                $isFacility=$this->isFacilityOrganization($organization);
                $organizationsF[$organization->getId()]=$isFacility;
            }

            return $this->render('organization/aggrid.html.twig', array('organizationsF'=>$organizationsF,'organizations' => $organizations));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_organization")
     *
    */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $specialties = $em->getRepository('App\Entity\SpecialtyAreas')->findBy(array(),array('name' => 'ASC'));
            $clasifications = $em->getRepository('App\Entity\OrgSpecialty')->findBy(array(),array('name' => 'ASC'));
            $accreditations = $em->getRepository('App\Entity\Accreditation')->findAll();
            $rates=$em->getRepository('App\Entity\Rate')->findAll();

            $totalSpecialties = count($specialties);
            $sa_first_column_elements = ($totalSpecialties / 3);
            if (is_float($sa_first_column_elements)) {
                $sa_first_column_elements = intval($sa_first_column_elements + 1);
            }
            $sa_rest_columns_element = intval(($totalSpecialties - $sa_first_column_elements) / 2);

            $totalOrgClasification = count($clasifications);
            $first_column_elements = ($totalOrgClasification / 3);
            if (is_float($first_column_elements)) {
                $first_column_elements = intval($first_column_elements + 1);
            }
            $rest_columns_element = intval(($totalOrgClasification - $first_column_elements) / 2);

            $terminationReason=$em->getRepository('App\Entity\OrgTerminationReason')->findAll();

            return $this->render('organization/add.html.twig',
                ['specialties' => $specialties, 'clasifications' => $clasifications, 'accreditations' => $accreditations,'rates'=>$rates,'terminationReason'=>$terminationReason,
                    'sa_first_column_elements'=>$sa_first_column_elements,'sa_rest_columns_element'=>$sa_rest_columns_element,'totalSpecialties'=>$totalSpecialties,
                    'totalOrgClasification'=>$totalOrgClasification,'first_column_elements'=>$first_column_elements,'rest_columns_element'=>$rest_columns_element]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_organization", defaults={"id": null})
     *
    */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Organization')->find($id);

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('admin_organization');
            }

            $specialties = $em->getRepository('App\Entity\SpecialtyAreas')->findBy(array(),array('name' => 'ASC'));
            $clasifications = $em->getRepository('App\Entity\OrgSpecialty')->findBy(array(),array('name' => 'ASC'));
            $accreditations = $em->getRepository('App\Entity\Accreditation')->findAll();

            $specialiesAreas = $document->getSpecialtyAreas();
            $orgClasification = $document->getOrgSpecialties();

            $totalSpecialties = count($specialties);
            $sa_first_column_elements = ($totalSpecialties / 3);
            if (is_float($sa_first_column_elements)) {
                $sa_first_column_elements = intval($sa_first_column_elements + 1);
            }
            $sa_rest_columns_element = intval(($totalSpecialties - $sa_first_column_elements) / 2);

            $totalOrgClasification = count($clasifications);
            $first_column_elements = ($totalOrgClasification / 3);
            if (is_float($first_column_elements)) {
                $first_column_elements = intval($first_column_elements + 1);
            }
            $rest_columns_element = intval(($totalOrgClasification - $first_column_elements) / 2);

            $terminationReason=$em->getRepository('App\Entity\OrgTerminationReason')->findAll();
            $rates=$em->getRepository('App\Entity\Rate')->findAll();

            $orgrates=$em->getRepository('App\Entity\OrganizationRates')->findBy(array('organization'=>$id));
            $organization_status=$em->getRepository('App\Entity\OrganizationStatus')->findAll();

            return $this->render('organization/edit.html.twig', ['accreditations' => $accreditations,'document' => $document, 'specialties' => $specialties, 'clasifications' => $clasifications, 'specialiesAreas' => $specialiesAreas,
                'orgClasification' => $orgClasification,'terminationReason'=>$terminationReason,'rates'=>$rates,'orgrates'=>$orgrates,'organization_status'=>$organization_status,
                'sa_first_column_elements'=>$sa_first_column_elements,'sa_rest_columns_element'=>$sa_rest_columns_element,'totalSpecialties'=>$totalSpecialties,
                'totalOrgClasification'=>$totalOrgClasification,'first_column_elements'=>$first_column_elements,'rest_columns_element'=>$rest_columns_element]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit-organization-data", name="admin_edit2_organization")
     */
    public function edit2(Request $request, ValidatorInterface $validator){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            $em = $this->getDoctrine()->getManager();
            $organization=$this->getUser()->getOrganization();

            $provider_type['Individual'] = 'Individual';
            $provider_type['Organization ( Licensed as a facility by AHCA )'] = 'Organization';
            $provider_type['Group'] = 'Group';

            $providerType=$organization->getProviderType();

            $formOptions = array('provider_type' => $provider_type);

            //get all specialty area by Type
            $specialties_mh=$em->getRepository('App:SpecialtyAreas')->findBy(array('specialty_area_type'=>1));
            $specialties_sa=$em->getRepository('App:SpecialtyAreas')->findBy(array('specialty_area_type'=>2));
            $specialties_se=$em->getRepository('App:SpecialtyAreas')->findBy(array('specialty_area_type'=>3));

            $form = $this->createForm(OrganizationPType::class, $organization, $formOptions);

            $form->handleRequest($request);
            $validation_required=$form['validation_required']->getData();

            $errors = $validator->validate($organization);

            if($validation_required==1){
                if($form->isSubmitted() && $form->isValid()){

                    $specialties_areas_ids=$form['specialty_areas']->getData();

                    $specialties_areas_ids = substr($specialties_areas_ids, 0, -1);
                    $lbs = explode(",", $specialties_areas_ids);

                    $current_sa=$organization->getSpecialtyAreas();
                    if($current_sa){
                        foreach ($current_sa as $item){
                            $organization->removeSpecialtyArea($item);
                        }

                        $em->flush();
                    }

                    if ($lbs != "") {
                        foreach ($lbs as $lb) {
                            $specialties_area = $em->getRepository('App:SpecialtyAreas')->find($lb);
                            if ($specialties_area) {
                                $organization->addSpecialtyArea($specialties_area);
                            }
                        }
                    }

                    $em->persist($organization);
                    $em->flush();
                    $this->SaveLog(2,$organization->getId(),'Editing Organization information','provider_portal');

                    return $this->redirectToRoute('admin_view_organization_2');
                }
            }

            if($validation_required==0){
                if($form->isSubmitted()){

                    $specialties_areas_ids=$form['specialties_area']->getData();

                    $specialties_areas_ids = substr($specialties_areas_ids, 0, -1);
                    $lbs = explode(",", $specialties_areas_ids);

                    $current_sa=$organization->getSpecialtyAreas();
                    if($current_sa){
                        foreach ($current_sa as $item){
                            $organization->removeSpecialtyArea($item);
                        }

                        $em->flush();
                    }

                    if ($lbs != "") {
                        foreach ($lbs as $lb) {
                            $specialties_area = $em->getRepository('App:SpecialtyArea')->find($lb);
                            if ($specialties_area) {
                                $organization=new Organization();
                                $organization->addSpecialtyArea($specialties_area);
                            }
                        }
                    }

                    $em->persist($organization);
                    $em->flush();
                    $this->SaveLog(2,$organization->getId(),'Editing Organization information','provider_portal');

                    return $this->redirectToRoute('admin_view_organization_2');
                }
            }

            $speciaty_areas=$organization->getSpecialtyAreas();

            return $this->render('organization/form_p.html.twig', ['form' => $form->createView(), 'action' => 'Edit','providerType'=>$providerType,
                'errors'=>$errors,'specialties_mh'=>$specialties_mh, 'specialties_sa'=>$specialties_sa,'specialties_se'=>$specialties_se,'speciaty_areas'=>$speciaty_areas]);

        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}/{tab}", name="admin_view_organization",defaults={"id": null, "tab": 1 })
    */
    public function view($id,$tab){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Organization')->find($id);
            $organizationContacts = $em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization' => $id));
            $organizationAddress = $em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $id));
            $allProviders = $em->getRepository('App:Provider')->findBy(array('organization'=>$id));
            $contactVerify=[];

            foreach ($organizationContacts as $contact){
                $email=$contact->getEmail();
                if($email!=""){
                    $user=$em->getRepository('App\Entity\User')->findBy(array('email'=>$email));
                    if($user!=null){
                        $contactVerify[$contact->getId()]="Yes";
                    }else{
                        $contactVerify[$contact->getId()]="No";
                    }
                }else{
                    $contactVerify[$contact->getId()]="No";
                }
            }

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('admin_organization');
            }

            $delete_form_ajax = $this->createCustomForm('ORGANIZATIONCONTACT_ID', 'DELETE', 'admin_delete_organizationcontact');
            $delete_form_provider_ajax = $this->createCustomForm('PROVIDER_ID', 'DELETE', 'admin_delete_provider');
            $delete_form_billing_ajax = $this->createCustomForm('BILLINGADDRESS_ID', 'DELETE', 'admin_delete_billing');

            $cvos=$em->getRepository('App\Entity\Cvo')->findAll();
            $credentialingStatus=$em->getRepository('App\Entity\CredentialingStatus')->findAll();
            $payers=$em->getRepository('App\Entity\Payer')->findAll();
            $specialiesAreas = $document->getSpecialtyAreaArray();
            $orgClasification = $document->getOrgSpecialtiesArray();

            $npi=$document->getGroupNpi();
            $pmls=array();
            if($npi!='' and $npi!=null and $npi!="-"){
                $pmlsT=$em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$npi));
                if($pmlsT!=null){
                    foreach ($pmlsT as $pml){
                        if($pml!=null and $pml->getColN()!=""){
                            $pmls[]=$pml;
                        }
                    }
                }
            }

            //load all address for this organuiz
            $medicaids=array();
            $is_facility=false;
            if($organizationAddress!=null){
                foreach ($organizationAddress as $orgAddr){
                    if($orgAddr->getIsFacility()==true){
                        $is_facility=true;
                    }
                    $medicaid=$orgAddr->getGroupMedicaid();
                    if($medicaid!=""){
                        $medicaids[]=$medicaid;
                    }
                    if($orgAddr->getLocationNpi()!=""){
                        $addr_npi=$orgAddr->getLocationNpi();
                        if($addr_npi!=$npi){
                            $pmlsT=$em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$addr_npi));
                            if($pmlsT!=null){
                                foreach ($pmlsT as $pml){
                                    if($pml!=null and $pml->getColN()!=""){
                                        $pmls[]=$pml;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $medicares=array();
            if($organizationAddress!=null){
                foreach ($organizationAddress as $orgAddr){
                    $medicare=$orgAddr->getGroupMedicare();
                    if($medicare!=""){
                        $medicares[]=$medicare;
                    }
                }
            }

            $totalAddress=count($medicaids);
            if($totalAddress>0){

            }

            //get all credentialing process for each address on this org
            $credentialingProcess=$em->getRepository('App\Entity\OrganizationCredentialing')->findBy(array('organization'=>$id));
            $emailLog=$em->getRepository('App\Entity\EmailLog')->findBy(array('organization'=>$id));
            $email_templates=$em->getRepository('App\Entity\EmailType')->findBy(array(),array('name'=>'ASC'));

            $medicaid_eligible=false;
            $medicare_eligible=false;
            //$document=new Organization();
            if($medicaids!=null and count($medicaids)>0){
                $medicaid_eligible=true;
            }

            if($medicares!=null and count($medicares)>0){
                $medicare_eligible=true;
            }

            //get all languages for organizations
            $languages=array();
            foreach ($allProviders as $provider){
                //$provider=new Provider();
                $langSTr=$provider->getLanguages();
                if($langSTr){
                    foreach ($langSTr as $lg){
                       if(!in_array($lg->getName(),$languages)){
                           $languages[]=$lg->getName();
                       }
                    }
                }

                $medicaid=$provider->getMedicaid();
                if($medicaid!="-" and $medicaid!='LCSW' and $medicaid!="" and $medicaid!="n/a" and $medicaid!="N/A" and $medicaid!="blank" and $medicaid!="00PENDING" and $medicaid!="PENDING"
                    and $medicaid!="submitting application" and $medicaid!="credentialing in process" and $medicaid!="In process"){
                    $medicaid_eligible=true;
                }

                $medicare=$provider->getMedicare();
                if($medicare!="-" and $medicare!="" and $medicare!="0"){
                    $medicare_eligible=true;
                }
            }

            //get Rates
            $rates=$em->getRepository('App\Entity\OrganizationRates')->findBy(array('organization'=>$id));

            //load info from NPPES NPI Registry
            $npi=$document->getGroupNpi();

            $pdata=$em->getRepository('App\Entity\PDataMedicare')->findOneBy(array('npi_number'=>$npi));
            $json=null;
            $pdataArray=array();
            $addressPdata=array();
            $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=1&skip=&pretty=on&version=2.1";

            $json="";
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );

            if($pdata!=null){
                $json=$pdata->getData();
            }else{
                $json=file_get_contents($url_result,false,stream_context_create($arrContextOptions));
            }

            $datos=json_decode($json,true);

            if(count($datos)>=1){
                $addressPdata=$datos['results'][0]['addresses'];
                $pdataArray['npi']=$datos['results'][0]['number'];
                if($document->getProviderType()!="Individual"){
                    $pdataArray['name']=$datos['results'][0]['basic']['organization_name'];
                }

                $pdataArray['status']=$datos['results'][0]['basic']['status'];
                $pdataArray['npi_type']=$datos['results'][0]['enumeration_type'];

                $address=$datos['results'][0]['addresses'];

                foreach ($address as $addr){
                    if($addr['address_purpose']=="LOCATION"){
                        $addr_str="";
                        $addr_str=$addr['address_1'].",".$addr['address_2']." \n".$addr['city']." , ".$addr['state']." ";
                        $codeP=$addr['postal_code'];
                        $codeP=substr($codeP,0,5)."-".substr($codeP,5,4);
                        $addr_str.=$codeP;
                        $pdataArray['prymary_location']=$addr_str;
                        $pdataArray['phone']=$addr['telephone_number'];
                    }
                }

                $taxonomies=$datos['results'][0]['taxonomies'];

                foreach ($taxonomies as $taxonomy){
                    if($taxonomy['primary']==true){
                        $pdataArray['taxonomy']=$taxonomy['desc']." - (".$taxonomy['code'].")";
                    }
                }
            }

            $taxonomies_org=$em->getRepository('App\Entity\OrganizationTaxonomy')->findBy(array('organization'=>$id));
            $trainings=$em->getRepository('App\Entity\OrganizationTrainingAttestation')->findBy(array('organization'=>$id));
            $organizations=$em->getRepository('App\Entity\Organization')->findBy(array('disabled'=>0));

            $individualCred=[];
            foreach ($allProviders as $provider){
                $cred=$em->getRepository('App\Entity\CredSimpleRecord')->findOneBy(array('npi'=>$provider->getNpiNumber()));
                if($cred!=null){
                    $individualCred[]=$cred;
                }
            }

            $facility_licenses=[];
            foreach ($organizationAddress as $addr){
                $licenses=$em->getRepository('App:AddressFacilityLicense')->findBy(array('address'=>$addr->getId()));
                if($licenses){
                    foreach ($licenses as $license){
                        $facility_licenses[]=$license;
                    }
                }
            }

            return $this->render('organization/view.html.twig', array('document' => $document,
                'organizationContacts' => $organizationContacts, 'organizationProviders' => $allProviders, 'facility_licenses'=>$facility_licenses,
                'organizationAddress' => $organizationAddress, 'credentialingProcess'=>$credentialingProcess, 'tab' => $tab,'organizations'=>$organizations,
                'cvos'=>$cvos,'contactVerify'=>$contactVerify, 'languages'=>$languages, 'credentialingStatus'=>$credentialingStatus, 'payers'=>$payers,
                'specialiesAreas' => $specialiesAreas,
                'orgClasification' => $orgClasification,
                'delete_form_ajax' => $delete_form_ajax->createView(),
                'delete_form_provider_ajax'=>$delete_form_provider_ajax->createView(),
                'delete_form_billing_ajax'=>$delete_form_billing_ajax->createView(),
                'pmls'=>$pmls,'medicaids'=>$medicaids,'medicares'=>$medicares,
                'emailLog'=>$emailLog,
                'email_templates'=>$email_templates,
                'medicaid_eligible'=>$medicaid_eligible,'medicare_eligible'=>$medicare_eligible,
                'rates'=>$rates,'is_facility'=>$is_facility,
                'pdata'=>$pdataArray, 'addressPdata'=>$addressPdata,'taxonomies'=>$taxonomies,'taxonomies_org'=>$taxonomies_org,
                'trainings'=>$trainings,'individualCred'=>$individualCred
                ));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_organization")
     *
     */
    public function create(Request $request, OrganizationFileUploader $fileUploader){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $db = $em->getConnection();

            $save_status = $request->get('save_status');
            $page = $request->get('page');
            $id = 0;

            //save fisrt part for organization
            if ($page == 0) {
                $name = $request->get('name');
                $dba_name = $request->get('dba_name');
                $organization_type = $request->get('organization_type');
                $docusign_link = $request->get('docusign_link');
                $npi = $request->get('npi');
                $npi2 = $request->get('npi2');
                $billing_npi = $request->get('billing_npi');
                $tin_number = $request->get('tin_number');
                $name_ehr_emr = $request->get('name_ehr_emr');
                $billing_phone = $request->get('billing_phone');
                $phone = $request->get('phone');
                $faxno = $request->get('faxno');
                $website = $request->get('website');
                $accreditationSelected = $request->get('accreditationSelected');
                $taxonomy_code = $request->get('taxonomy_code');
                $dateagmt=$request->get('dateagmt');
                $application_receipt_date=$request->get('application_receipt_date');

                $organization = null;
                if ($save_status == 0) {
                    $organization = new Organization();
                } else {
                    $organization = $em->getRepository('App\Entity\Organization')->find($save_status);
                }

                $status=$em->getRepository('App\Entity\OrganizationStatus')->find(1);

                $organization->setName($name);
                $organization->setBdaName($dba_name);
                $organization->setProviderType($organization_type);
                $organization->setDocusignLink($docusign_link);
                $organization->setGroupNpi($npi);
                $organization->setGroupNpi2($npi2);
                $organization->setBillingNpi($billing_npi);
                $organization->setTinNumber($tin_number);
                $organization->setNameOfEhrEmr($name_ehr_emr);
                $organization->setBillingPhone($billing_phone);
                $organization->setPhone($phone);
                $organization->setFaxno($faxno);
                $organization->setWebsite($website);
                $organization->setTaxonomyCode($taxonomy_code);
                $organization->setOrganizationStatus($em->getRepository('App\Entity\OrganizationStatus')->find(2));
                $organization->setReceiveDateAgmt($dateagmt);
                $organization->setOrganizationStatus($status);
                $organization->setDisabled(0);
                $organization->setProviderApplicationReceiptDate($application_receipt_date);

                $em->persist($organization);
                $em->flush();

                $id = $organization->getId();

                $sqlDelete = "DELETE FROM `organization_accreditation` WHERE `organization_accreditation`.`organization_id` = $id";
                $stmt = $db->prepare($sqlDelete);
                $stmt->execute();

                $accreditations = substr($accreditationSelected, 0, -1);
                $lbs = explode(",", $accreditations);

                foreach ($lbs as $accreditation) {
                    $accreditationObj = $em->getRepository('App\Entity\Accreditation')->find($accreditation);

                    if ($accreditationObj) {
                        $organization->addAccreditation($accreditationObj);
                    }
                }

                //save log actions
                if ($save_status == 0) {
                    $this->SaveLog(1,$organization->getId(),'Creating a new  organization','');
                }else{
                    $this->SaveLog(2,$organization->getId(),'Editing the organization','');
                }
            }
            //save data for Specialty Areas
            if ($page == 1) {
                $id = $request->get('id');
                $specialtiesSelected = $request->get('specialtiesSelected');

                $sqlDelete = "DELETE FROM `organization_specialty_areas` WHERE `organization_specialty_areas`.`organization_id` = $id";
                $stmt = $db->prepare($sqlDelete);
                $stmt->execute();

                $specialties = substr($specialtiesSelected, 0, -1);
                $lbs = explode(",", $specialties);

                foreach ($lbs as $specialty) {
                    $sql = "INSERT INTO `organization_specialty_areas` (`organization_id`, `specialtyarea_id`) VALUES ($id, $specialty);";
                    $stmt1 = $db->prepare($sql);
                    $stmt1->execute();
                }
            }

            //save data for Organization Clasification
            if ($page == 2) {
                $id = $request->get('id');
                $clasificationSelected = $request->get('clasificationSelected');


                $sqlDelete = "DELETE FROM `organization_org_specialties` WHERE `organization_org_specialties`.`organization_id` = $id";
                $stmt = $db->prepare($sqlDelete);
                $stmt->execute();

                $clasifications = substr($clasificationSelected, 0, -1);
                $lbs = explode(",", $clasifications);

                foreach ($lbs as $clasification) {
                    $sql = "INSERT INTO `organization_org_specialties` (`organization_id`, `org_specialty_id`) VALUES ($id, $clasification);";
                    $stmt1 = $db->prepare($sql);
                    $stmt1->execute();
                }
            }

            //save data for Hospital Afiliation
            if ($page == 3) {
                $id = $request->get('id');
                $notes = $request->get('notes');
                $non_standard_rates = $request->get('non_standard_rates');
                $standard_rates = $request->get('standard_rates');
                $ratesSelected=$request->get('ratesSelected');
                $ratesValueSelected=$request->get('ratesValueSelected');

                $ratesSelected = substr($ratesSelected, 0, -1);
                $rates = explode(",", $ratesSelected);

                $ratesValueSelected = substr($ratesValueSelected, 0, -1);
                $ratesValue = explode(",", $ratesValueSelected);

                $organization = $em->getRepository('App\Entity\Organization')->find($id);

                if ($organization != null) {

                    //check if Organization Rates exist
                    $orgRates=$em->getRepository('App\Entity\OrganizationRates')->findBy(array('organization'=>$id));
                    if($orgRates!=null){
                        foreach ($orgRates as $or){
                            if($or!=null){
                                $em->remove($or);
                            }
                        }
                        $em->flush();
                    }
                    $contrates=0;
                    foreach ($rates as $rate){
                        if($rate!=0){
                            $rateObj=$em->getRepository('App\Entity\Rate')->find($rate);
                            if($rateObj!=null){
                                $orgRate=new OrganizationRates();
                                $orgRate->setOrganization($organization);
                                $orgRate->setRate($rateObj);
                                $orgRate->setPercent($ratesValue[$contrates]);

                                $em->persist($orgRate);
                            }
                        }
                        $em->flush();
                        $contrates++;
                    }

                    $organization->setNotes($notes);
                    $organization->setNonStandardRates($non_standard_rates);
                    $organization->setStandardRates($standard_rates);
                    $em->persist($organization);
                    $em->flush();
                }
            }

            //upload files
            $uploadError = array();
            if ($page == 4) {
                $id = $request->get('save_status');
                $organization = $em->getRepository('App\Entity\Organization')->find($id);

                $provider_agreement_file = $request->files->get('provider_agreement_file');
                $state_licence_file = $request->files->get('state_licence_file');
                $provider_data_form_file = $request->files->get('provider_data_form_file');
                $w9_file = $request->files->get('w9_file');
                $roster_file = $request->files->get('roster_file');
                $general_liability_coverage_file = $request->files->get('general_liability_coverage_file');
                $site_visit_survery_file = $request->files->get('site_visit_survery_file');
                $accreditation_certificate_file = $request->files->get('accreditation_certificate_file');
                $loa_loi_file = $request->files->get('loa_loi_file');

                if ($provider_agreement_file != "") {
                    $file = $fileUploader->upload($provider_agreement_file, $id, "provider_agreement_file");
                    if ($file != "error") {
                        $organization->setProviderAgreementFile($file);
                        $uploadError[0] = 0;
                    } else {
                        $uploadError[0] = 1;
                    }
                }
                if ($state_licence_file != "") {
                    $file = $fileUploader->upload($state_licence_file, $id, "state_licence_file");
                    if ($file != "error") {
                        $organization->setStateLicenseFile($file);
                        $uploadError[1] = 0;
                    } else {
                        $uploadError[1] = 1;
                    }
                }
                if ($provider_data_form_file != "") {
                    $file = $fileUploader->upload($provider_data_form_file, $id, "provider_data_form_file");
                    if ($file != "error") {
                        $organization->setProviderDataFormFile($file);
                        $uploadError[2] = 0;
                    } else {
                        $uploadError[2] = 1;
                    }
                }
                if ($w9_file != "") {
                    $file = $fileUploader->upload($w9_file, $id, "w9_file");
                    if ($file != "error") {
                        $organization->setW9File($file);
                        $uploadError[3] = 0;
                    } else {
                        $uploadError[3] = 1;
                    }
                }
                if ($roster_file != "") {
                    $file = $fileUploader->upload($roster_file, $id, "roster_file");
                    if ($file != "error") {
                        $organization->setRosterFile($file);
                        $uploadError[4] = 0;
                    } else {
                        $uploadError[4] = 1;
                    }
                }
                if ($general_liability_coverage_file != "") {
                    $file = $fileUploader->upload($general_liability_coverage_file, $id, "general_liability_coverage_file");
                    if ($file != "error") {
                        $organization->setGeneralLiabilityCoverageFile($file);
                        $uploadError[5] = 0;
                    } else {
                        $uploadError[5] = 1;
                    }
                }
                if ($site_visit_survery_file != "") {
                    $file = $fileUploader->upload($site_visit_survery_file, $id, "site_visit_survery_file");
                    if ($file != "error") {
                        $organization->setSiteVisitSurveyFile($file);
                        $uploadError[6] = 0;
                    } else {
                        $uploadError[6] = 1;
                    }
                }
                if ($accreditation_certificate_file != "") {
                    $file = $fileUploader->upload($accreditation_certificate_file, $id, "accreditation_certificate_file");
                    if ($file != "error") {
                        $organization->setAccreditationCertificateFile($file);
                        $uploadError[7] = 0;
                    } else {
                        $uploadError[7] = 1;
                    }
                }
                if ($loa_loi_file != "") {
                    $file = $fileUploader->upload($loa_loi_file, $id, "loa_loi_file");
                    if ($file != "error") {
                        $organization->setLoaLoiFile($file);
                        $uploadError[8] = 0;
                    } else {
                        $uploadError[8] = 1;
                    }
                }

                $em->persist($organization);
                $em->flush();
            }

            return new Response(
                json_encode(array('id' => $id, 'page' => $page, 'uploadError' => $uploadError)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_organization",methods={"POST","DELETE"})
     *
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $organization = $organization = $em->getRepository('App\Entity\Organization')->find($id);
            $removed = 0;
            $message = "";

            if ($organization) {
                try {
                    $em->remove($organization);
                    $em->flush();
                    $removed = 1;
                    $message = "The Organization has been Successfully removed";

                    $this->SaveLog(4,$id,'Deleting an organization','');
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The organization can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_organization",methods={"POST","DELETE"})
     *
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $organization = $em->getRepository('App\Entity\Organization')->find($id);

                if ($organization) {
                    try {
                        $em->remove($organization);
                        $em->flush();
                        $removed = 1;
                        $message = "The Organization has been Successfully removed";

                        $this->SaveLog(4,$ids,'Deleting many organizations','');
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Organization can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }

    private function isFacilityOrganization($organization){
        $em=$this->getDoctrine()->getManager();
        $db = $em->getConnection();
        $id=$organization->getId();

        $query1 = "SELECT  * FROM billing_address WHERE  billing_address.organization_id=$id;";
        $stmt1 = $db->prepare($query1);
        $stmt1->execute();

        $adresses=$stmt1->fetchAll();
        $isFacility=0;

        foreach ($adresses as $adress){
            $isF= $adress['is_facility'];

            if($isF==1){
                $isFacility=1;
                return $isFacility;
            }
        }

        return $isFacility;
    }

    private function OrgPayers($org){
        $em=$this->getDoctrine()->getManager();
        $providers=$org->getProviders();
        $payers="";

        if($providers!=null){
              foreach ($providers as $provider){
                  $provider=new Provider();

                 // $proObj=$em->getRepository('App\Entity\ViewProvider')->find($id);
                 // $payers=$proObj->getPayers();
              }
        }

        return $payers;
    }


    private function SaveLog($action_id,$entity_id,$note,$source){
        $em=$this->getDoctrine()->getManager();
        $user=$this->getUser();

        $log=new ChangeLog();
        $log->setUser($user);
        $action=$em->getRepository('App\Entity\ActionLog')->find($action_id);
        $log->setAction($action);
        $log->setEntityId($entity_id);
        $log->setSource($source);
        $log->setEntityClass('Organization');
        $log->setNote($note);

        $em->persist($log);
        $em->flush();
    }

    /**
     * @Route("/update", name="admin_update_organization", methods={"POST"})
     *
    */
    public function update(Request $request, OrganizationFileUploader $fileUploader){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            ini_set('max_execution_time', 0);
            $em = $this->getDoctrine()->getManager();
            $db = $em->getConnection();
            $organization_id = $request->get('organization_id');
            $organization=$em->getRepository('App\Entity\Organization')->find($organization_id);

            $name = $request->get('name');
            $dba_name = $request->get('dba_name');
            $organization_type = $request->get('organization_type');
            $docusign_link=$request->get('docusign_link');
            $npi = $request->get('npi');
            $npi2 = $request->get('npi2');
            $billing_npi = $request->get('billing_npi');
            $tin_number = $request->get('tin_number');
            $name_ehr_emr = $request->get('name_ehr_emr');
            $billing_phone = $request->get('billing_phone');
            $phone = $request->get('phone');
            $faxno = $request->get('faxno');
            $website = $request->get('website');
            $taxonomy_code = $request->get('taxonomy_code');
            $notes = $request->get('notes');
            $non_standard_rates = $request->get('non_standard_rates');
            $standard_rates = $request->get('standard_rates');
            $enabled = $request->get('enabled');
            $dateagmt=$request->get('dateagmt');
            $organization_status=$request->get('organization_status');
            $accreditationSelected = $request->get('accreditationSelected');
            $specialtiesSelected = $request->get('specialtiesSelected');
            $clasificationSelected = $request->get('clasificationSelected');
            $ratesSelected=$request->get('ratesSelected');
            $ratesValueSelected=$request->get('ratesValueSelected');
            $application_receipt_date=$request->get('application_receipt_date');

            $status=$em->getRepository('App\Entity\OrganizationStatus')->find($organization_status);

            $organization->setName($name);
            $organization->setBdaName($dba_name);
            $organization->setProviderType($organization_type);
            $organization->setDocusignLink($docusign_link);
            $organization->setGroupNpi($npi);
            $organization->setGroupNpi2($npi2);
            $organization->setBillingNpi($billing_npi);
            $organization->setTinNumber($tin_number);
            $organization->setNameOfEhrEmr($name_ehr_emr);
            $organization->setBillingPhone($billing_phone);
            $organization->setProviderApplicationReceiptDate($application_receipt_date);
            $organization->setPhone($phone);
            $organization->setFaxno($faxno);
            $organization->setWebsite($website);
            $organization->setTaxonomyCode($taxonomy_code);
            $organization->setOrganizationStatus($em->getRepository('App\Entity\OrganizationStatus')->find(2));
            $organization->setNotes($notes);
            $organization->setNonStandardRates($non_standard_rates);
            $organization->setStandardRates($standard_rates);
            $organization->setReceiveDateAgmt($dateagmt);
            $organization->setOrganizationStatus($status);

            if ($organization != null) {
                $ratesSelected = substr($ratesSelected, 0, -1);
                $rates = explode(",", $ratesSelected);

                $ratesValueSelected = substr($ratesValueSelected, 0, -1);
                $ratesValue = explode(",", $ratesValueSelected);

                //check if Organization Rates exist
                $orgRates=$em->getRepository('App\Entity\OrganizationRates')->findBy(array('organization'=>$organization_id));

                if($orgRates!=null){
                    foreach ($orgRates as $or){
                        if($or!=null){
                            $em->remove($or);
                        }
                    }
                    $em->flush();
                }

                foreach ($rates as $rate){
                    if($rate!=0){
                        $rateObj=$em->getRepository('App\Entity\Rate')->find($rate);
                        if($rateObj!=null){
                            $orgRate=new OrganizationRates();
                            $orgRate->setOrganization($organization);
                            $orgRate->setRate($rateObj);
                            $orgRate->setPercent($ratesValue[$rate-1]);
                            $em->persist($orgRate);
                            $em->flush();
                        }
                    }
                }

                $em->persist($organization);
                $em->flush();
            }

            if(isset($enabled)){
                $organization->setDisabled(true);
            }else{
                $organization->setDisabled(false);
            }

            $organization->setUpdatedOn( new \DateTime());

            $sqlDelete = "DELETE FROM `organization_accreditation` WHERE `organization_accreditation`.`organization_id` = $organization_id";
            $stmt = $db->prepare($sqlDelete);
            $stmt->execute();

            $accreditations = substr($accreditationSelected, 0, -1);
            $lbs = explode(",", $accreditations);

            foreach ($lbs as $accreditation) {
                $accreditationObj = $em->getRepository('App\Entity\Accreditation')->find($accreditation);

                if ($accreditationObj) {
                    $organization->addAccreditation($accreditationObj);
                }
            }

            $sqlDelete = "DELETE FROM `organization_specialty_areas` WHERE `organization_specialty_areas`.`organization_id` = $organization_id";
            $stmt = $db->prepare($sqlDelete);
            $stmt->execute();

            $specialties = substr($specialtiesSelected, 0, -1);
            $lbs = explode(",", $specialties);

            foreach ($lbs as $specialty) {
                if($specialty!="" and $specialty!=null){
                    $specialtyObj=$em->getRepository('App\Entity\SpecialtyAreas')->find($specialty);
                    $organization->addSpecialtyArea($specialtyObj);
                }
            }

            $sqlDelete = "DELETE FROM `organization_org_specialties` WHERE `organization_org_specialties`.`organization_id` = $organization_id";
            $stmt = $db->prepare($sqlDelete);
            $stmt->execute();

            $clasifications = substr($clasificationSelected, 0, -1);
            $lbs = explode(",", $clasifications);

            foreach ($lbs as $clasification) {
                if($clasification!="" and $clasification!=null){
                    $clasificationObj=$em->getRepository('App\Entity\OrgSpecialty')->find($clasification);
                    $organization->addOrgSpecialty($clasificationObj);
                }
            }

            $this->SaveLog(2,'Organization',$organization->getId(),'Editing the organization','');

            $provider_agreement_file = $request->files->get('provider_agreement_file');
            $state_licence_file = $request->files->get('state_licence_file');
            $provider_data_form_file = $request->files->get('provider_data_form_file');
            $w9_file = $request->files->get('w9_file');
            $roster_file = $request->files->get('roster_file');
            $general_liability_coverage_file = $request->files->get('general_liability_coverage_file');
            $site_visit_survery_file = $request->files->get('site_visit_survery_file');
            $accreditation_certificate_file = $request->files->get('accreditation_certificate_file');
            $loa_loi_file = $request->files->get('loa_loi_file');

            $uploadError = array();
            $errorTest="";
            if ($provider_agreement_file != "") {
                $file = $fileUploader->upload($provider_agreement_file, $organization_id, "provider_agreement_file");
                if ($file != "error") {
                    $organization->setProviderAgreementFile($file);
                    $uploadError[1] = 0;
                } else {
                    $uploadError[1] = 1;
                }
            }

            if ($state_licence_file != "") {
                $file = $fileUploader->upload($state_licence_file, $organization_id, "state_licence_file");
                if ($file != "error") {
                    $organization->setStateLicenseFile($file);
                    $uploadError[1] = 0;
                } else {
                    $uploadError[1] = 1;
                }
            }
            if ($provider_data_form_file != "") {
                $file = $fileUploader->upload($provider_data_form_file, $organization_id, "provider_data_form_file");
                if ($file != "error") {
                    $organization->setProviderDataFormFile($file);
                    $uploadError[2] = 0;
                } else {
                    $uploadError[2] = 1;
                }
            }
            if ($w9_file != "") {
                $file = $fileUploader->upload($w9_file, $organization_id, "w9_file");
                if ($file != "error") {
                    $organization->setW9File($file);
                    $uploadError[3] = 0;
                } else {
                    $uploadError[3] = 1;
                }
            }
            if ($roster_file != "") {
                $file = $fileUploader->upload($roster_file, $organization_id, "roster_file");
                if ($file != "error") {
                    $organization->setRosterFile($file);
                    $uploadError[4] = 0;
                } else {
                    $uploadError[4] = 1;
                }
            }
            if ($general_liability_coverage_file != "") {
                $file = $fileUploader->upload($general_liability_coverage_file, $organization_id, "general_liability_coverage_file");
                if ($file != "error") {
                    $organization->setGeneralLiabilityCoverageFile($file);
                    $uploadError[5] = 0;
                } else {
                    $uploadError[5] = 1;
                }
            }
            if ($site_visit_survery_file != "") {
                $file = $fileUploader->upload($site_visit_survery_file, $organization_id, "site_visit_survery_file");
                if ($file != "error") {
                    $organization->setSiteVisitSurveyFile($file);
                    $uploadError[6] = 0;
                } else {
                    $uploadError[6] = 1;
                }
            }
            if ($accreditation_certificate_file != "") {
                $file = $fileUploader->upload($accreditation_certificate_file, $organization_id, "accreditation_certificate_file");
                if ($file != "error") {
                    $organization->setAccreditationCertificateFile($file);
                    $uploadError[7] = 0;
                } else {
                    $uploadError[7] = 1;
                }
            }
            if ($loa_loi_file != "") {
                $file = $fileUploader->upload($loa_loi_file, $organization_id, "loa_loi_file");
                if ($file != "error") {
                    $organization->setLoaLoiFile($file);
                    $uploadError[8] = 0;
                } else {
                    $uploadError[8] = 1;
                }
            }

            $em->persist($organization);
            $em->flush();

            return  $this->redirectToRoute('admin_organization_list');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update2", name="admin_update2_organization")
     *
     */
    public function update2(Request $request){
            $em = $this->getDoctrine()->getManager();
            $db = $em->getConnection();
            $organization_id = $request->get('organization_id');
            $organization=$em->getRepository('App\Entity\Organization')->find($organization_id);

            $name = $request->get('name');
            $organization_type = $request->get('organization_type');
            $npi = $request->get('npi');
            $npi2 = $request->get('npi2');
            $billing_npi = $request->get('billing_npi');
            $billing_npi2 = $request->get('billing_npi2');
            $tin_number = $request->get('tin_number');
            $name_ehr_emr = $request->get('name_ehr_emr');
            $billing_phone = $request->get('billing_phone');
            $phone = $request->get('phone');
            $lega_name_tin_owner=$request->get('lega_name_tin_owner');
            $faxno = $request->get('faxno');
            $website = $request->get('website');
            $taxonomies=$request->get('taxonomies');
            $notes = $request->get('notes');
            $non_standard_rates = $request->get('non_standard_rates');
            $standard_rates = $request->get('standard_rates');
            $accreditationSelected = $request->get('accreditationSelected');
            $specialtiesSelected = $request->get('specialtiesSelected');
            $clasificationSelected = $request->get('clasificationSelected');
            $american_sign_language=$request->get('american_sign_language');


            $organization->setName($name);
            $organization->setProviderType($organization_type);
            $organization->setGroupNpi($npi);
            $organization->setGroupNpi2($npi2);
            $organization->setBillingNpi($billing_npi);
            $organization->setBillingNpi2($billing_npi2);
            $organization->setTinNumber($tin_number);
            $organization->setNameOfEhrEmr($name_ehr_emr);
            $organization->setBillingPhone($billing_phone);
            $organization->setPhone($phone);
            $organization->setFaxno($faxno);
            $organization->setWebsite($website);
            $organization->setOrganizationStatus($em->getRepository('App\Entity\OrganizationStatus')->find(2));
            $organization->setNotes($notes);
            $organization->setNonStandardRates($non_standard_rates);
            $organization->setStandardRates($standard_rates);
            $organization->setLegalNameTinOwner($lega_name_tin_owner);
            $organization->setAmericanSignLanguage($american_sign_language);

            $organization->setUpdatedOn( new \DateTime());

            $sqlDelete = "DELETE FROM `organization_accreditation` WHERE `organization_accreditation`.`organization_id` = $organization_id";
            $stmt = $db->prepare($sqlDelete);
            $stmt->execute();

            $accreditations = substr($accreditationSelected, 0, -1);
            $lbs = explode(",", $accreditations);

            foreach ($lbs as $accreditation) {
                $accreditationObj = $em->getRepository('App\Entity\Accreditation')->find($accreditation);

                if ($accreditationObj) {
                    $organization->addAccreditation($accreditationObj);
                    $em->flush();
                }
            }



            $sqlDelete = "DELETE FROM `organization_specialty_areas` WHERE `organization_specialty_areas`.`organization_id` = $organization_id";
            $stmt = $db->prepare($sqlDelete);
            $stmt->execute();

            $specialties = substr($specialtiesSelected, 0, -1);
            $lbs = explode(",", $specialties);

            foreach ($lbs as $specialty) {
                if($specialty!="" and $specialty!=null){
                    $specialtyObj=$em->getRepository('App\Entity\SpecialtyAreas')->find($specialty);
                    $organization->addSpecialtyArea($specialtyObj);
                }
            }

            $em->flush();

            $sqlDelete = "DELETE FROM `organization_org_specialties` WHERE `organization_org_specialties`.`organization_id` = $organization_id";
            $stmt = $db->prepare($sqlDelete);
            $stmt->execute();

            $clasifications = substr($clasificationSelected, 0, -1);
            $lbs = explode(",", $clasifications);

            foreach ($lbs as $clasification) {
                if($clasification!="" and $clasification!=null){
                    $clasificationObj=$em->getRepository('App\Entity\OrgSpecialty')->find($clasification);
                    $organization->addOrgSpecialty($clasificationObj);
                }
            }

            $em->flush();

            $organization_taxonomies=$em->getRepository('App\Entity\OrganizationTaxonomy')->findBy(array('organization'=>$organization_id));

            if($organization_taxonomies!=null){
                foreach ($organization_taxonomies as $org_taxonomy){
                    $em->remove($org_taxonomy);
                    $em->flush();
                }
            }

        if($taxonomies!="") {
            if (count($taxonomies) > 0) {
                foreach ($taxonomies as $taxonomy_id) {
                    $taxonomyObj = $em->getRepository('App\Entity\TaxonomyCode')->find($taxonomy_id);
                    if ($taxonomyObj != null) {
                        $org_tax = new OrganizationTaxonomy();
                        $org_tax->setOrganization($organization);
                        $org_tax->setTaxonomy($taxonomyObj);
                        $org_tax->setIsPrimary(false);
                        $em->persist($org_tax);
                    }
                }
                $em->flush();
            }
        }

            $this->SaveLog(2,'Organization',$organization->getId(),'Editing the organization','provider_portal');

            $em->persist($organization);
            $em->flush();

         return $this->redirectToRoute('home');
    }

    /**
     * @Route("/update_billing_address_type", name="admin_update_billing_address_type")
     *
    */
    public function update_billing_address_type(){
        $em=$this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App\Entity\Organization')->findAll();
        foreach ($organizations as $organization){

            $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
            if($address!=null){
                if(count($address)==1){
                    foreach ($address as $addr){
                      $addr->setMailingAddr(true);
                      $addr->setBillingAddr(true);
                      $addr->setPrimaryAddr(true);

                      $em->persist($addr);
                    }
                    $em->flush();
                }
            }
        }

        die();
    }

    /**
     * @Route("/add_payer_organization", name="admin_add_organization_payer")
     */
    public function addPayer(Request $request){
        $em=$this->getDoctrine()->getManager();

        $org= $request->get('org_id');
        $payer=$request->get('payer');
        $select_payment=$request->get('select_payment');

        $payerObj=$em->getRepository('App\Entity\Payer')->find($payer);
        $tierObj=$em->getRepository('App\Entity\PaymentTier')->find($select_payment);
        $organization=$em->getRepository('App\Entity\Organization')->find($org);



        if($organization!=null){
            $organization->removePayer($payerObj);
            $organization->addPayer($payerObj);
            $em->persist($organization);
            $em->flush();

            $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$org));
            if($providers!=null){
                foreach ($providers as $provider){
                    $existPayer=false;
                    $existTier=false;
                    $payers=$provider->getPayers();
                    $tiers=$provider->getPaymentTiers();
                    if($payers!=null){
                        foreach ($payers as $p){
                            if($p!=null){
                                if($p->getId()==$payer){
                                    $existPayer=true;
                                }
                            }
                        }
                    }

                    if($tiers!=null){
                        foreach ($tiers as $tier){
                            if($tier!=null){
                                if($tier->getId()==$select_payment){
                                    $existTier=true;
                                }
                            }
                        }
                    }

                    if($existPayer==false){
                        $provider->addPayer($payerObj);
                    }
                    if($existTier==false){
                        $provider->addPaymentTier($tierObj);
                    }

                    $em->persist($provider);
                }
                $em->flush();
            }
        }

        return new Response(
            json_encode(array('payer' =>$payer)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/setmaincontact", name="admin_organization_contact_main")
     */
    public function setContactMain(){
        set_time_limit(88200);
        $em=$this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App\Entity\Organization')->findAll();
        $bests=$em->getRepository('App\Entity\ContactPBestChoice')->findAll();

        foreach ($organizations as $organization){
            $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));

            if($contacts!=null){
                if(count($contacts)==1){
                   foreach ($contacts as $contact){
                       $contact->setMain(true);
                       $em->persist($contact);
                       $em->flush();

                       $existContacts=$contact->getBestChoices();
                       if($existContacts!=null){
                           foreach ($existContacts as $existContact){
                               $contact->removeBestChoice($existContact);
                               $em->persist($contact);
                               $em->flush();
                           }
                       }

                       foreach ($bests as $best){
                           $contact->addBestChoice($best);
                           $em->persist($contact);
                           $em->flush();
                       }
                   }
                }
            }
        }
        die();
    }


    /**
     * @Route("/updateinfo_nppes", name="admin_organization_update_info_NPPES")
     */
    public function updateInfoFromNPPES(){
        set_time_limit(88200);
        $em = $this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App\Entity\Organization')->findAll();
        foreach ($organizations as $org){
            $npi=$org->getGroupNpi();
            if($npi!="" and $npi!="NOT AVAILABLE"){

                $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=200&skip=&pretty=on&version=2.1";

                $json=file_get_contents($url_result);
                $data=json_decode($json,true);

                $name=$data['results'][0]['basic']['name'];
                $type=$data['results'][0]['enumeration_type'];

                if($type=='NPI-2'){
                    $org->setName($name);
                    $em->persist($org);
                    $em->flush();
                }
            }
        }

        die();
    }

    /**
     * @Route("/update_address_nppes", name="admin_organization_update_address_NPPES")
     */
    public function updateAddressFromNPPES(){
        set_time_limit(88200);
        $em = $this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App\Entity\Organization')->findAll();

        $orgR=array();
        $cont=1;

        foreach ($organizations as $org){
            if($cont<2){
                $orgR[]=$org;
            }
            $cont++;
        }

        foreach ($orgR as $org){
            $npi=$org->getGroupNpi();
            $addressOrg=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$org->getId()));
            if($npi!="" and $npi!="NOT AVAILABLE"){
                $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=200&skip=&pretty=on&version=2.1";

                $json=file_get_contents($url_result);
                $data=json_decode($json,true);

                if($data['result_count']>0){
                    $type=$data['results'][0]['enumeration_type'];
                if($type=='NPI-2'){
                    $address=$data['results'][0]['addresses'];

                    foreach ($address as $addr){
                        $streetData = explode(" ",$addr['address_1']);

                        foreach ($addressOrg as $addrO){
                           $streetO=explode(" ",$addrO->getStreet());
                           echo $streetO[0]."<br/>";
                        }
                    }
                }

                }

            }
        }

        die();
    }

    /**
     * @Route("/add_taxonomies_nppes", name="admin_organization_add_taxonomies_NPPES")
    */
    public function setTaxonomiesFromNPPES(){
        set_time_limit(88200);
        $em = $this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App\Entity\Organization')->findBy(array('disabled'=>0));
        foreach ($organizations as $organization){

            $currentTaxonomies=$em->getRepository('App:OrganizationTaxonomy')->findBy(array('organization'=>$organization->getId()));
            if($currentTaxonomies!=null){
                foreach ($currentTaxonomies as $currentTaxonomy){
                    $em->remove($currentTaxonomy);
                }
                $em->flush();
            }

            $npi=$organization->getGroupNpi();

            if($npi!=""){
                $data=$em->getRepository('App\Entity\PDataMedicare')->findOneBy(array('npi_number'=>$npi));
                if($data==null){
                    $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=200&skip=&pretty=on&version=2.1";
                }

                $json="";
                if($data!=null){
                    $json=$data->getData();
                }else{
                    $json=file_get_contents($url_result);
                }

                $datos=json_decode($json,true);
                if(count($datos)>1){
                    if($datos['result_count']>0){
                        $taxonomies=$datos['results'][0]['taxonomies'];

                        if($taxonomies!=null and count($taxonomies)>0){
                            foreach ($taxonomies as $taxonomy){
                                $code=$taxonomy['code'];
                                $state=$taxonomy['state'];
                                $license=$taxonomy['license'];
                                $is_primary=$taxonomy['primary'];

                                $taxonomyObj=$em->getRepository('App\Entity\TaxonomyCode')->findOneBy(array('code'=>$code));
                                if($taxonomyObj!=null){
                                    $org_tax=new OrganizationTaxonomy();
                                    $org_tax->setOrganization($organization);
                                    $org_tax->setTaxonomy($taxonomyObj);
                                    $org_tax->setState($state);
                                    $org_tax->setLicence($license);
                                    $org_tax->setIsPrimary($is_primary);

                                    $em->persist($org_tax);
                                    $em->flush();
                                }
                            }
                        }
                    }
                }
            }
        }

        die();
    }

    /**
     * @Route("/validate-roster", name="admin_validate_roster_organization",methods={"POST"})
     *
     */
    public function validateRosterSet(Request $request) {
        $em=$this->getDoctrine()->getManager();
        $id=$request->get('id');
        $validateO=$request->get('validate');

        $date=date('m-Y');
        $organization=$em->getRepository('App\Entity\Organization')->find($id);

        $validate=$em->getRepository('App\Entity\OrganizationvalidateRoster')->findOneBy(array('organization'=>$id,'date'=>$date));

        if($validate==null){
            $validate=new OrganizationValidateRoster();
        }
        $validate->setDate($date);
        $validate->setOrganization($organization);
        $validate->setValidate($validateO);

        $em->persist($validate);
        $em->flush();

        return new Response(
            json_encode(array('id' =>$validateO)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/disable_organization", name="admin_disable_organization",methods={"POST"})
     */
    public function disableOrganization(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $id=$request->get('id');
        $termination_reason=$request->get('termination_reason');
        $termination_reason_notes=$request->get('termination_reason_notes');
        $date_of_disabled=date('m/d/Y H:i:s');
        $organization=$em->getRepository('App\Entity\Organization')->find($id);

        if($organization){
            $organization->setDisabled(1);
            $organization->setTerminationReason($em->getRepository('App\Entity\OrgTerminationReason')->find($termination_reason));
            $organization->setDisabledDate($date_of_disabled);
            $organization->setTerminationReasonNote($termination_reason_notes);

            $em->persist($organization);
            $em->flush();
        }

        $this->SaveLog(6,'Organization',$organization->getId(),'Disable organization','');

        return new Response(
            json_encode(array('id' => 2)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/view-org-data", name="admin_view_organization_2",defaults={"tab": 1 })
     */
    public function view2(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $id=$this->getUser()->getOrganization()->getId();
            $document = $em->getRepository('App\Entity\Organization')->find($id);
            $organizationContacts = $em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization' => $id));
            $organizationAddress = $em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $id));
            $allProviders = $em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$id));

            $sql="SELECT p.*,
            GROUP_CONCAT(DISTINCT `credentialing_status`.`name` ORDER BY `credentialing_status`.`name` ASC SEPARATOR ', ') AS `status_cred`,
            GROUP_CONCAT(DISTINCT `provider_credentialing`.`credentialing_effective_date` ORDER BY `provider_credentialing`.`id` ASC SEPARATOR ' ') AS `effective_date`,
            GROUP_CONCAT(DISTINCT `degree`.`name` ORDER BY `degree`.`name` ASC SEPARATOR ', ') AS `degrees`
            FROM  provider p 
            LEFT JOIN organization ON organization.id = p.org_id
            LEFT JOIN provider_credentialing ON provider_credentialing.provider_id = p.id
            LEFT JOIN credentialing_status ON provider_credentialing.credentialing_status_id = credentialing_status.id
            LEFT JOIN provider_degree ON provider_degree.provider_id = p.id    
            LEFT JOIN degree ON degree.id = provider_degree.degree_id
        
            WHERE organization.id=$id
            GROUP BY p.id
            ORDER BY p.first_name";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $providers= $stmt->fetchAll();

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('admin_organization');
            }

            $delete_form_ajax = $this->createCustomForm('ORGANIZATIONCONTACT_ID', 'DELETE', 'admin_delete_organizationcontact');
            $delete_form_provider_ajax = $this->createCustomForm('PROVIDER_ID', 'DELETE', 'admin_delete_provider');
            $delete_form_billing_ajax = $this->createCustomForm('BILLINGADDRESS_ID', 'DELETE', 'admin_delete_billing');

            $cvos=$em->getRepository('App\Entity\Cvo')->findAll();
            $credentialingStatus=$em->getRepository('App\Entity\CredentialingStatus')->findAll();
            $payers=$em->getRepository('App\Entity\Payer')->findAll();
            $specialiesAreas = $document->getSpecialtyAreaArray();
            $orgClasification = $document->getOrgSpecialtiesArray();

            $npi=$document->getGroupNpi();
            $pmls=array();
            if($npi!='' and $npi!=null and $npi!="-"){
                $pmlsT=$em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$npi));
                if($pmlsT!=null){
                    foreach ($pmlsT as $pml){
                        if($pml!=null and $pml->getColN()!=""){
                            $pmls[]=$pml;
                        }
                    }
                }
            }

            //load all address for this organuiz
            $medicaids=array();
            $is_facility=false;
            if($organizationAddress!=null){
                foreach ($organizationAddress as $orgAddr){
                    if($orgAddr->getIsFacility()==true){
                        $is_facility=true;
                    }
                    $medicaid=$orgAddr->getGroupMedicaid();
                    if($medicaid!=""){
                        $medicaids[]=$medicaid;
                    }
                    if($orgAddr->getLocationNpi()!=""){
                        $addr_npi=$orgAddr->getLocationNpi();
                        if($addr_npi!=$npi){
                            $pmlsT=$em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$addr_npi));
                            if($pmlsT!=null){
                                foreach ($pmlsT as $pml){
                                    if($pml!=null and $pml->getColN()!=""){
                                        $pmls[]=$pml;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $medicares=array();
            if($organizationAddress!=null){
                foreach ($organizationAddress as $orgAddr){
                    $medicare=$orgAddr->getGroupMedicare();
                    if($medicare!=""){
                        $medicares[]=$medicare;
                    }
                }
            }

            $totalAddress=count($medicaids);
            if($totalAddress>0){

            }

            //get all credentialing process for each address on this org
            $credentialingProcess=$em->getRepository('App\Entity\OrganizationCredentialing')->findBy(array('organization'=>$id));

            $emailLog=$em->getRepository('App\Entity\EmailLog')->findBy(array('organization'=>$id));
            $email_templates=$em->getRepository('App\Entity\EmailType')->findBy(array(),array('name'=>'ASC'));

            $medicaid_eligible=false;
            $medicare_eligible=false;
            if($medicaids!=null and count($medicaids)>0){
                $medicaid_eligible=true;
            }

            if($medicares!=null and count($medicares)>0){
                $medicare_eligible=true;
            }

            //get all languages for organizations
            $languages=array();
            foreach ($allProviders as $provider){
                $langSTr=$provider->getLanguages();
                if($langSTr){
                    foreach ($langSTr as $lg){
                        if(!in_array($lg->getName(),$languages)){
                            $languages[]=$lg->getName();
                        }
                    }
                }

                $medicaid=$provider->getMedicaid();
                if($medicaid!="-" and $medicaid!='LCSW' and $medicaid!="" and $medicaid!="n/a" and $medicaid!="N/A" and $medicaid!="blank" and $medicaid!="00PENDING" and $medicaid!="PENDING"
                    and $medicaid!="submitting application" and $medicaid!="credentialing in process" and $medicaid!="In process"){
                    $medicaid_eligible=true;
                }

                $medicare=$provider->getMedicare();
                if($medicare!="-" and $medicare!="" and $medicare!="0"){
                    $medicare_eligible=true;
                }
            }

            //get Rates
            $rates=$em->getRepository('App\Entity\OrganizationRates')->findBy(array('organization'=>$id));

            //load info from NPPES NPI Registry
            $npi=$document->getGroupNpi();

            $pdata=$em->getRepository('App\Entity\PDataMedicare')->findOneBy(array('npi_number'=>$npi));
            $json=null;
            $pdataArray=array();
            $addressPdata=array();
            $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=1&skip=&pretty=on&version=2.1";

            $json="";
            $arrContextOptions=array(
                "ssl"=>array(
                    "verify_peer"=>false,
                    "verify_peer_name"=>false,
                ),
            );

            if($pdata!=null){
                $json=$pdata->getData();
            }else{
                $json=file_get_contents($url_result,false,stream_context_create($arrContextOptions));
            }

            $datos=json_decode($json,true);

            if(count($datos)>=1){
                $addressPdata=$datos['results'][0]['addresses'];
                $pdataArray['npi']=$datos['results'][0]['number'];
                $pdataArray['name']=$datos['results'][0]['basic']['organization_name'];
                $pdataArray['status']=$datos['results'][0]['basic']['status'];
                $pdataArray['npi_type']=$datos['results'][0]['enumeration_type'];

                $address=$datos['results'][0]['addresses'];

                foreach ($address as $addr){
                    if($addr['address_purpose']=="LOCATION"){
                        $addr_str="";
                        $addr_str=$addr['address_1'].",".$addr['address_2']." \n".$addr['city']." , ".$addr['state']." ";
                        $codeP=$addr['postal_code'];
                        $codeP=substr($codeP,0,5)."-".substr($codeP,5,4);
                        $addr_str.=$codeP;
                        $pdataArray['prymary_location']=$addr_str;
                        $pdataArray['phone']=$addr['telephone_number'];
                    }
                }

                $taxonomies=$datos['results'][0]['taxonomies'];

                foreach ($taxonomies as $taxonomy){
                    if($taxonomy['primary']==true){
                        $pdataArray['taxonomy']=$taxonomy['desc']." - (".$taxonomy['code'].")";
                    }
                }
            }

            $taxonomies_org=$document->getTaxonomies();
            $trainings=$em->getRepository('App\Entity\OrganizationTrainingAttestation')->findBy(array('organization'=>$id));

            $organizationsResult=[];
            /*
            $organizations=$em->getRepository('App\Entity\Organization')->findAll();
            $organizationsResult=[];

            $currentOrganizations= [];
            if($this->getUser()->getOrganizations()!=null){
                foreach ($this->getUser()->getOrganizations() as $currentOrg){
                    $currentOrganizations[]=$currentOrg->getId();
                }
            }
            foreach ($organizations as $org){
                if(!in_array($org->getId(),$currentOrganizations)){
                    $organizationsResult[]=$org;
                }
            }*/

            //check if the org are assigned to a Payer
            $has_mmm=false;
            $has_fcc=false;
            $has_payer=false;
            if($providers!=null){
               foreach ($providers as $provider){
                   $pObj=$em->getRepository('App\Entity\Provider')->find($provider['id']);
                   $payers=$pObj->getPayers();
                   if($payers!=null){
                       foreach ($payers as $payer){
                           if($payer->getId()==1){
                               $has_mmm=true;
                               $has_payer=true;
                           }
                           if($payer->getId()==4){
                               $has_fcc=true;
                               $has_payer=true;
                           }
                       }
                   }
               }
            }

            return $this->render('organization/view_organization.html.twig', array('document' => $document,
                'organizationContacts' => $organizationContacts,
                'organizationProviders' => $providers,'organizations'=>$organizationsResult,
                'organizationAddress' => $organizationAddress,
                'credentialingProcess'=>$credentialingProcess,
                'cvos'=>$cvos,'has_mmm'=>$has_mmm,'has_fcc'=>$has_fcc,'has_payer'=>$has_payer,
                'tab'=>1,
                'languages'=>$languages,
                'credentialingStatus'=>$credentialingStatus,
                'payers'=>$payers,
                'specialiesAreas' => $specialiesAreas,
                'orgClasification' => $orgClasification,
                'delete_form_ajax' => $delete_form_ajax->createView(),
                'delete_form_provider_ajax'=>$delete_form_provider_ajax->createView(),
                'delete_form_billing_ajax'=>$delete_form_billing_ajax->createView(),
                'pmls'=>$pmls,'medicaids'=>$medicaids,'medicares'=>$medicares,
                'emailLog'=>$emailLog,
                'email_templates'=>$email_templates,
                'medicaid_eligible'=>$medicaid_eligible,'medicare_eligible'=>$medicare_eligible,
                'rates'=>$rates,'is_facility'=>$is_facility,
                'pdata'=>$pdataArray, 'addressPdata'=>$addressPdata,'taxonomies'=>$taxonomies,'taxonomies_org'=>$taxonomies_org,
                'trainings'=>$trainings
            ));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/organization-set-rates", name="admin_organization_set_rates")
     */
    public function setRates(){
        $em=$this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App\Entity\Organization')->findAll();
        $rate1=$em->getRepository('App\Entity\Rate')->find(1);
        $rate2=$em->getRepository('App\Entity\Rate')->find(2);
        $rate3=$em->getRepository('App\Entity\Rate')->find(3);

        foreach ($organizations as $organization){
            $org_rate1=new OrganizationRates();
            $org_rate1->setOrganization($organization);
            $org_rate1->setPercent(85);
            $org_rate1->setRate($rate1);

            $org_rate2=new OrganizationRates();
            $org_rate2->setOrganization($organization);
            $org_rate2->setPercent(100);
            $org_rate2->setRate($rate2);

            $org_rate3=new OrganizationRates();
            $org_rate3->setOrganization($organization);
            $org_rate3->setPercent(95);
            $org_rate3->setRate($rate3);

            $em->persist($org_rate1);
            $em->persist($org_rate2);
            $em->persist($org_rate3);
            $em->flush();
        }

        echo "All OK";
        die();
    }

    /**
     * @Route("/organization-set-addrs-support-emr", name="admin_organization_set_emr")
     */
    public function setAddrEMR(){
        $em=$this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App\Entity\Organization')->findAll();

        foreach ($organizations as $organization){
            if($organization->getNameOfEhrEmr()!=""){
                $addresses=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                if($addresses!=null and count($addresses)==1){
                 foreach ($addresses as $addr){
                     $addr->setSupportEHR(1);
                     $em->persist($addr);
                     $em->flush();
                 }
                }
            }

        }
        echo "All OK";
        die();
    }

    /**
     * @Route("/organization-set-tin", name="admin_organization_set_tin")
     */
    public function setTIN(){
        $em=$this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App\Entity\Organization')->findAll();

        foreach ($organizations as $organization){
            $tin=$organization->getTinNumber();

            if(strrpos($tin, "-")==true) {
             $newtin=str_replace("-","",$tin);
             $organization->setTinNumber($newtin);
             $em->persist($organization);
            }
        }

        $em->flush();
        die();
    }

    /**
     * @Route("/save-lob", name="admin_organization_save_lob",methods={"POST"})
     *
     */
    public function saveLOB(Request $request) {
        $em=$this->getDoctrine()->getManager();
        $organization=$this->getUser()->getOrganization();

        if($organization!=null){
            $lineofb1=$request->get('line_of_business_1');
            $lineofb2=$request->get('line_of_business_2');
            $lineofb3=$request->get('line_of_business_3');
            $lineofb4=$request->get('line_of_business_4');


            if(isset($lineofb1)){
               $org_lob1=$em->getRepository('App\Entity\OrganizationLineofBusiness')->findOneBy(array('organization'=>$organization->getId(),'line_of_business'=>1));

               if($org_lob1==null){
                   $org_lob1=new OrganizationLineofBusiness();
               }

               $lineofb1_obj=$em->getRepository('App\Entity\LineOfBusiness')->find(1);

               $org_lob1->setOrganization($organization);
               $org_lob1->setLineOfBusiness($lineofb1_obj);
               $org_lob1->setValue($lineofb1);

               $em->persist($org_lob1);
               $em->flush();
            }

            if(isset($lineofb2)){
                $org_lob2=$em->getRepository('App\Entity\OrganizationLineofBusiness')->findOneBy(array('organization'=>$organization->getId(),'line_of_business'=>2));

                if($org_lob2==null){
                    $org_lob2=new OrganizationLineofBusiness();
                }
                $lineofb2_obj=$em->getRepository('App\Entity\LineOfBusiness')->find(2);
                $org_lob2->setOrganization($organization);
                $org_lob2->setLineOfBusiness($lineofb2_obj);
                $org_lob2->setValue($lineofb2);

                $em->persist($org_lob2);
                $em->flush();
            }

            if(isset($lineofb3)){
                $org_lob3=$em->getRepository('App\Entity\OrganizationLineofBusiness')->findOneBy(array('organization'=>$organization->getId(),'line_of_business'=>3));

                if($org_lob3==null){
                    $org_lob3=new OrganizationLineofBusiness();
                }
                $lineofb3_obj=$em->getRepository('App\Entity\LineOfBusiness')->find(3);
                $org_lob3->setOrganization($organization);
                $org_lob3->setLineOfBusiness($lineofb3_obj);
                $org_lob3->setValue($lineofb3);

                $em->persist($org_lob3);
                $em->flush();
            }

            if(isset($lineofb4)){
                $org_lob4=$em->getRepository('App\Entity\OrganizationLineofBusiness')->findOneBy(array('organization'=>$organization->getId(),'line_of_business'=>4));

                if($org_lob4==null){
                    $org_lob4=new OrganizationLineofBusiness();
                }
                $lineofb4_obj=$em->getRepository('App\Entity\LineOfBusiness')->find(4);
                $org_lob4->setOrganization($organization);
                $org_lob4->setLineOfBusiness($lineofb4_obj);
                $org_lob4->setValue($lineofb4);

                $em->persist($org_lob4);
                $em->flush();
            }
        }

        return new Response(
            json_encode(array('id' => $lineofb1)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/disable-organizations-multiple", name="admin_disable_organizations_multiple",methods={"POST"})
     */
    public function disableMultiplesOrganization(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $ids=$request->get('ids');

        $termination_reason=$request->get('termination_reason');
        $termination_reason_notes=$request->get('termination_reason_notes');
        $date_of_disabled=date('m/d/Y H:i:s');

        foreach ($ids as $id) {
            $organization = $em->getRepository('App\Entity\Organization')->find($id);
            if($organization){
                $organization->setDisabled(1);
                $organization->setTerminationReason($em->getRepository('App\Entity\OrgTerminationReason')->find($termination_reason));
                $organization->setDisabledDate($date_of_disabled);
                $organization->setTerminationReasonNote($termination_reason_notes);

                $em->persist($organization);
                $em->flush();

                $this->SaveLog(6,'Organization',$organization->getId(),'Disable organization','');
            }
        }

        return new Response(
            json_encode(array('id' => 2)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/exclude_payer_organization", name="admin_exclude_organization_payer")
     */
    public function excludePayer(Request $request){
        $em=$this->getDoctrine()->getManager();

        $ids=$request->get('ids');
        $payer_id=$request->get('payer');
        $date=$request->get('date');
        $date_today=date('m/d/Y');
        $send_email_notification=$request->get('send_email_notification');

        $dateArray=explode('/',$date);

        $newDate=$dateArray[2]."-".$dateArray[0]."-".$dateArray[1];

        $payer=$em->getRepository('App\Entity\Payer')->find($payer_id);
        foreach ($ids as $id) {
            $organization = $em->getRepository('App:Organization')->find($id);
            if($organization!=null){

                $excluded=$em->getRepository('App:OrganizationPayerExcluded')->findOneBy(array('payer'=>$payer_id,'organization'=>$id));
                if($excluded==null){
                    $excluded=new OrganizationPayerExcluded();
                    $excluded->setOrganization($organization);
                    $excluded->setPayer($payer);
                    $excluded->setCreateAt(new \DateTime($newDate));

                    $em->persist($excluded);

                    //send email
                    if($send_email_notification==1){
                        $contacts=$em->getRepository('App:OrganizationContact')->findBy(array('organization'=>$id));
                        $email_template=$em->getRepository('App\Entity\EmailType')->find(22);

                        $best_option_template=$email_template->getBestChoices();
                        $best_option_template_array=[];
                        if($best_option_template){
                            foreach ($best_option_template as $bot){
                                $best_option_template_array[]=$bot->getId();
                            }
                        }
                        if($contacts){
                            $cont_global=0;
                            foreach ($contacts as $contact){
                                $best_options_contact=$contact->getBestChoices();
                                if($best_options_contact){
                                    $cont=0;
                                    foreach ($best_options_contact as $best_option_contact){
                                        $best_id_1=$best_option_contact->getId();
                                        if(in_array($best_id_1,$best_option_template_array)){
                                            $cont++;
                                            $cont_global++;
                                        }
                                    }
                                    $email=$contact->getEmail();
                                    if($cont>0 and $email!=""){

                                        $emailQueue=new EmailQueued();
                                        $status=$em->getRepository('App\Entity\EmailStatus')->find(1);
                                        $emailQueue->setStatus($status);
                                        $emailQueue->setOrganization($organization);
                                        $emailQueue->setOrganizationName($organization->getName());
                                        $emailQueue->setEmailType($email_template);
                                        $emailQueue->setDateToSend(new \DateTime($date_today));
                                        $emailQueue->setSentTo($email);
                                        $emailQueue->setContact($contact);
                                        $emailQueue->setHealthPlan($payer->getName());

                                        $em->persist($emailQueue);
                                    }
                                }
                            }

                            if($cont_global==0){
                                foreach ($contacts as $contact){
                                    $email=$contact->getEmail();
                                    if($email!=""){
                                        $emailQueue=new EmailQueued();
                                        $status=$em->getRepository('App\Entity\EmailStatus')->find(1);
                                        $emailQueue->setStatus($status);
                                        $emailQueue->setOrganization($organization);
                                        $emailQueue->setOrganizationName($organization->getName());
                                        $emailQueue->setEmailType($email_template);
                                        $emailQueue->setDateToSend("");
                                        $emailQueue->setSentTo($email);
                                        $emailQueue->setContact(null);

                                        $em->persist($emailQueue);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $em->flush();

        return new Response(
            json_encode(array('id' => $send_email_notification)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/report-addrs-org-missing-type", name="report_addrs_org_missing_typ")
     *
     */
    public function report_billing_address_type(){
        $em=$this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App\Entity\Organization')->findAll();
        $cont=1;
        foreach ($organizations as $organization){

            $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
            if($address!=null){
                if(count($address)>1){

                    $cont_primary=0;
                    $cont_billing=0;
                    $con_mailing=0;

                    foreach ($address as $addr){
                        if($addr->getPrimaryAddr()==true){
                            $cont_primary++;
                        }
                        if($addr->getBillingAddr()==true){
                            $cont_billing++;
                        }
                        if($addr->getMailingAddr()==true){
                            $con_mailing++;
                        }
                    }

                    if($cont_primary==0 or $cont_billing==0 or $con_mailing==0){
                        echo $cont." ".$organization->getGroupNpi()." ".$organization->getName()." Primary:".$cont_primary." Billing:".$cont_billing." Mailing: ".$con_mailing."<br>";
                        $cont++;
                    }
                }
            }else{

            }
        }

        die();
    }

    /**
     * @Route("/move-taxonomies", name="admin_org_move_taxonomies")
     *
     */
    public function orgMoveTaxonomies(){

        $em=$this->getDoctrine()->getManager();
        $currents=$em->getRepository('App:OrganizationTaxonomy')->findAll();

        foreach ($currents as $current){
            $organization=$current->getOrganization();
            $taxonomy=$current->getTaxonomy();


            $current_taxonomies=$organization->getTaxonomies();
            $validT=true;
            if($current_taxonomies){
                foreach ($current_taxonomies as $ct){
                    if($ct->getId()==$taxonomy->getId()){
                        $validT=false;
                    }
                }
            }

            if($validT){
                $organization->addTaxonomy($taxonomy);
                $em->persist($organization);
                $em->flush();
            }
        }

      die();
    }

    /**
     * @Route("/missing-organization-information-list", name="missing_organization_information_list")
     *
     */
    public function missing_organization_information_list(ValidatorInterface $validator){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            set_time_limit(500);
            $em = $this->getDoctrine()->getManager();
            $email_templates=$em->getRepository('App\Entity\EmailType')->findBy(array(),array('name'=>'ASC'));
            $missing_results = array();

            $organizations=$em->getRepository('App\Entity\Organization')->findBy(array('disabled'=>0));

            $payers = $em->getRepository('App\Entity\Payer')->findAll();

            foreach ($organizations as $organization){
                $emerging_result = array();
                $emerging_result = $this->verify_missing_information($validator, $organization);
                array_push($missing_results, $emerging_result);
            }

            return $this->render('organization/missing-organization-information-list.html.twig',
                array('missing_results'=>$missing_results,
                      'email_templates'=>$email_templates,
                      'payers'=>$payers
                    ));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function verify_missing_information(ValidatorInterface $validator, Organization $organization){

        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            $missing_result = array(
                'org_id'=>$organization->getId(),
                'org_name'=>$organization->getName(),
                'group_npi'=>$organization->getGroupNpi(),
                'training_attestation'=>'No',
                'organization_check'=>'No',
                'providers_check'=>'No',
                'address_check'=>'No',
                'contacts_check'=>'No',
                'missing_global'=>'No'
            );

            $em = $this->getDoctrine()->getManager();

            $providersResult=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));

            $contactResult2=array();
            $contact_check=array();
            if($organization!=null){
                $contactResult2=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));
                $currentYear=date('Y');
                $training=$em->getRepository('App\Entity\OrganizationTrainingAttestation')->findOneBy(array('organization'=>$organization->getId(),'year'=>$currentYear));

                if($training==false){
                    $replace = array('training_attestation' => 'Yes');
                    $missing_result = array_replace($missing_result, $replace);
                }

                $errors_org = $validator->validate($organization);
                if(count($errors_org)>0){
                    $replace = array('organization_check' => 'Yes');
                    $missing_result = array_replace($missing_result, $replace);
                }
            }

            if(count($contactResult2)==0){
                $replace = array('contacts_check' => 'Yes');
                $missing_result = array_replace($missing_result, $replace);
            }else{
                foreach ($contactResult2 as $contact2){
                    if($contact2->getName()==""){
                        $contact_check[]=$contact2;

                        $replace = array('contacts_check' => 'Yes');
                        $missing_result = array_replace($missing_result, $replace);
                        break;
                    }
                    if($contact2->getPhone()==""){
                        $contact_check[]=$contact2;
                        $replace = array('contacts_check' => 'Yes');
                        $missing_result = array_replace($missing_result, $replace);
                        break;
                    }
                    if(sizeof($contact2->getBestChoices())==0){
                        $contact_check[]=$contact2;
                        $replace = array('contacts_check' => 'Yes');
                        $missing_result = array_replace($missing_result, $replace);
                        break;
                    }
                    if($contact2->getEmail()==""){
                        $contact_check[]=$contact2;
                        $replace = array('contacts_check' => 'Yes');
                        $missing_result = array_replace($missing_result, $replace);
                        break;
                    }
                }
            }

            //get address for check status
            $addresses_check=[];
            $columns_addr=[];
            $columns_addr['phone']=false;

            $primary_addr=false;
            $billing_addr=false;
            $mailing_addr=false;

            if($organization!=null){
                $addresses=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));

                if(count($addresses)==0){
                    $replace = array('address_check' => 'Yes');
                    $missing_result = array_replace($missing_result, $replace);
                }else{
                    foreach ($addresses as $addr){
                        if($addr->getPhoneNumber()=="" or $addr->getPhoneNumber()==null){
                            $columns_addr['phone']=true;
                            $addresses_check[]=$addr;
                            $replace = array('address_check' => 'Yes');
                            $missing_result = array_replace($missing_result, $replace);
                        }

                        if($addr->getPrimaryAddr()==true){
                            $primary_addr=true;
                        }
                        if($addr->getBillingAddr()==true){
                            $billing_addr=true;
                        }
                        if($addr->getMailingAddr()==true){
                            $mailing_addr=true;
                        }
                    }
                }
            }

            if($primary_addr==false or $billing_addr==false or $mailing_addr==false){
                $replace = array('address_check' => 'Yes');
                $missing_result = array_replace($missing_result, $replace);
            }

            //get providers for check status on provider portal
            foreach ($providersResult as $provider){

                $errors = $validator->validate($provider);

                if(count($errors)>0){
                    $replace = array('providers_check' => 'Yes');
                    $missing_result = array_replace($missing_result, $replace);
                    break;
                }
            }


            if($missing_result['training_attestation']=='Yes' || $missing_result['organization_check']=='Yes' || $missing_result['providers_check']=='Yes' || $missing_result['address_check']=='Yes' || $missing_result['contacts_check']=='Yes'){
                $replace = array('missing_global' => 'Yes');
                $missing_result = array_replace($missing_result, $replace);
            }

            return $missing_result;

        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view-missing-organization-information/{id}", name="view_missing_organization_information", defaults={"id": null})
     */
    public function viewMissingOrganizationInformation(ValidatorInterface $validator, $id)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $roles = $em->getRepository('App\Entity\Role')->findAll();

            $organizationsT = $em->getRepository('App\Entity\Organization')->findBy(array('disabled'=>0));
            $providers = $em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));
            $payers = $em->getRepository('App\Entity\Payer')->findAll();
            $prospects = $em->getRepository('App\Entity\Prospect')->findAll();

            $providersResult=array();
            $addressResult=array();
            $contactResult=array();
            $organization= $em->getRepository('App\Entity\Organization')->find($id);

            $hasFcc=0;
            $hasMMM=0;
            $credentialingProcess=[];
            $org_facility=false;
            if($organization!=null){
                $org_id=$organization->getId();
                $providersResult=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));
                $addressResult=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                $contactResult=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));


                $sql="SELECT p.id,GROUP_CONCAT(DISTINCT `payer`.`id` ORDER BY `payer`.`id` ASC SEPARATOR ',') AS `payers`
                FROM  provider p
                LEFT JOIN organization ON organization.id = p.org_id
                LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
                LEFT JOIN payer ON payer.id = provider_payer.payer_id
                WHERE p.disabled=0 and p.org_id=$org_id
                GROUP BY p.id
                ORDER BY p.id";

                $stmt = $conn->prepare($sql);
                $stmt->execute();
                $prdrs= $stmt->fetchAll();


                foreach ($prdrs as $pro){
                    if(stristr($pro['payers'], '1')){
                        $hasMMM=1;
                    }
                    if(stristr($pro['payers'], '4')){
                        $hasFcc=1;
                    }
                }

                $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                if($address!=null){
                    foreach ($address as $addr){
                        if($addr->getIsFacility()==true){
                            $org_facility=true;
                            break;
                        }
                    }
                }

                $credentialingProcess=$em->getRepository('App\Entity\OrganizationCredentialing')->findBy(array('organization'=>$organization->getId()));
            }

            $users=$em->getRepository('App\Entity\User')->findBy(array(),array('id'=>'DESC'));
            $usersP=array();

            foreach ($users as $user){
                if($user->getOrganization()!=null and $user->getEnabled()==true){
                    $usersP[]=$user;
                }
            }
            $totaluserP=count($usersP);

            //section for check the items to be check
            $organization_check=false;
            $providers_check=false;
            $contacts_check=false;
            $training_attestation=false;

            //validation for information
            $check_org_accreditation=false;
            $check_org_groupnpi=false;
            $check_org_billingnpi=false;
            $check_org_tin=false;
            $check_org_legal_name_tin_owner=false;
            $check_org_billing_phone=false;
            $check_org_phone=false;

            $contactResult2=array();
            $contact_check=array();
            if($organization!=null){
                $contactResult2=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));
                $currentYear=date('Y');
                $training=$em->getRepository('App\Entity\OrganizationTrainingAttestation')->findOneBy(array('organization'=>$organization->getId(),'year'=>$currentYear));

                if($training==false){
                    $training_attestation=true;
                }

                $accreditations=$organization->getAccreditations();

                if($accreditations==null or count($accreditations)==0 ){
                    $organization_check=true;
                    $check_org_accreditation=true;
                }

                $org_npi=$organization->getGroupNpi();
                if($org_npi==null or $org_npi==""){
                    $organization_check=true;
                    $check_org_groupnpi=true;
                }

                $org_billingnpi=$organization->getBillingNpi();
                if($org_billingnpi=="" or $org_billingnpi==null){
                    $organization_check=true;
                    $check_org_billingnpi=true;
                }

                $org_tin=$organization->getTinNumber();
                if($org_tin=="" or $org_tin==null){
                    $organization_check=true;
                    $check_org_tin=true;
                }

                $org_legal_name_tin_owner=$organization->getLegalNameTinOwner();
                if($org_legal_name_tin_owner=="" or $org_legal_name_tin_owner==null){
                    $organization_check=true;
                    $check_org_legal_name_tin_owner=true;
                }

                $org_billing_phone=$organization->getBillingPhone();
                if($org_billing_phone=="" or $org_billing_phone==null) {
                    $organization_check=true;
                    $check_org_billing_phone=true;
                }

                $org_phone=$organization->getPhone();
                if($org_phone==null or $org_phone==""){
                    $organization_check=true;
                    $check_org_phone=true;
                }
            }

            if(count($contactResult2)==0){
                $contacts_check=true;
            }else{
                foreach ($contactResult2 as $contact2){
                    if($contact2->getName()==""){
                        $contact_check[]=$contact2;
                        $contacts_check=true;
                    }
                    if($contact2->getPhone()==""){
                        $contact_check[]=$contact2;
                        $contacts_check=true;
                    }
                    if(sizeof($contact2->getBestChoices())==0){
                        $contact_check[]=$contact2;
                        $contacts_check=true;
                    }
                    if($contact2->getEmail()==""){
                        $contact_check[]=$contact2;
                        $contacts_check=true;
                    }
                }
            }

            //get address for check status
            $addresses_check=[];
            $addr_check=false;
            $address_check=false;
            $addr_type_check=false;
            $columns_addr=[];
            $columns_addr['phone']=false;

            $primary_addr=false;
            $billing_addr=false;
            $mailing_addr=false;
            $is_facility_addr=false;

            if($organization!=null){
                $addresses=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));

                if(count($addresses)==0){
                    $address_check=true;
                    $addr_check=true;
                }else{
                    foreach ($addresses as $addr){
                        if($addr->getPhoneNumber()=="" or $addr->getPhoneNumber()==null){
                            $columns_addr['phone']=true;
                            $addresses_check[]=$addr;
                            $address_check=true;
                            $addr_check=true;
                        }

                        if($addr->getPrimaryAddr()==true){
                            $primary_addr=true;
                        }
                        if($addr->getBillingAddr()==true){
                            $billing_addr=true;
                        }
                        if($addr->getMailingAddr()==true){
                            $mailing_addr=true;
                        }
                        if($addr->getIsFacility()==true){
                            $is_facility_addr=true;
                        }
                    }
                }
            }

            if($primary_addr==false or $billing_addr==false or $mailing_addr==false){
                $addr_type_check=true;
                $address_check=true;
            }

            $provider_check=[];
            $providers_id=[];
            //get providers for check status on provider portal
            foreach ($providersResult as $provider){

                $id=$provider->getId();
                $errors = $validator->validate($provider);

                if(count($errors)>0){
                    $providers_check=true;
                    $provider_check[]=$provider;
                    $providers_id[]=$id;
                }
            }

            $userValdiate=$em->getRepository('App\Entity\ProviderToVerify')->findAll();
            $currentYear=date('Y');
            $cmsTrainings=$em->getRepository('App\Entity\OrganizationTrainingAttestation')->findBy(array('year'=>$currentYear));

            $billing=$em->getRepository('App\Entity\BillingAddress')->findAll();

            $totalBilling=count($billing);

            //validate Roster
            $validateRoster=null;
            $currentDateF=date('m-Y');

            if($organization!=null){
                $validateRoster=$em->getRepository('App\Entity\OrganizationValidateRoster')->findOneBy(array('date'=>$currentDateF,'organization'=>$organization));
            }

            //get requests to access to organization
            $requestsOrg=$em->getRepository('App\Entity\UserOrgRequest')->findAll();

            //load all line of business
            $lineofBusiness=$em->getRepository('App\Entity\LineOfBusiness')->findAll();
            $organization_lob=[];
            $organization_lob_completed=false;

            if($organization!=null){
                $organization_lob=$em->getRepository('App\Entity\OrganizationLineofBusiness')->findBy(array('organization'=>$organization->getId()));
            }

            if(count($organization_lob)==4){
                $organization_lob_completed=true;
            }

            $conn = $em->getConnection();
            //organization statistics
            $sql_disabled="SELECT o.id FROM  organization o WHERE o.disabled=1";
            $sql_active="SELECT o.id FROM  organization o WHERE o.status_id=2";
            $sql_pending="SELECT o.id FROM  organization o WHERE o.status_id=1";

            $stmt = $conn->prepare($sql_disabled);
            $stmt->execute();
            $organizations_disabled= $stmt->fetchAll();

            $stmt1 = $conn->prepare($sql_active);
            $stmt1->execute();
            $organizations_active= $stmt1->fetchAll();

            $stmt2 = $conn->prepare($sql_pending);
            $stmt2->execute();
            $organizations_pending= $stmt2->fetchAll();

            $organizations_status=[];

            $organizations_status[0]=count($organizations_disabled);
            $organizations_status[1]=count($organizations_pending);
            $organizations_status[2]=count($organizations_active);

            $organization_types=[];
            $sql_pending="SELECT o.id FROM  organization o WHERE o.provider_type='Individual'";
            $stmt3 = $conn->prepare($sql_pending);
            $stmt3->execute();
            $organizations_individual= $stmt3->fetchAll();

            $sql_pending="SELECT o.id FROM  organization o WHERE o.provider_type='Group'";
            $stmt4 = $conn->prepare($sql_pending);
            $stmt4->execute();
            $organizations_group= $stmt4->fetchAll();

            $sql_pending="SELECT o.id FROM  organization o WHERE o.provider_type='Organization'";
            $stmt5 = $conn->prepare($sql_pending);
            $stmt5->execute();
            $organizations_facilities= $stmt5->fetchAll();

            $organization_types[0]=count($organizations_individual);
            $organization_types[1]=count($organizations_group);
            $organization_types[2]=count($organizations_facilities);

            $providersCred=[];
            $prov=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization));
            if($prov){
                foreach ($prov as $item){
                    $cred=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('npi_number'=>$item->getNpiNumber()));
                    if($cred!=null and count($cred)>0){
                        $providersCred[]=$item;
                    }
                }
            }

            //check if the organization have ready and valid Directory update
            $total_Records=0;
            $show_alert_directory_update=true;
            if($organization!=null){
                $directory_update_records=$em->getRepository('App:DirectoryUpdate')->findBy(array('organization'=>$organization),array('id'=>'ASC'));
                if($directory_update_records){
                    $total_Records=count($directory_update_records);
                    $record=$directory_update_records[intval($total_Records-1)];

                    $last_date=$record->getDate();
                    $last_date_array=explode('/',$last_date);

                    $begin= $last_date_array[2]."-".$last_date_array[0]."-".$last_date_array[1]." 00:00:00";

                    $current_date=date('Y-m-d H:i:s');

                    $datetime1=new \DateTime($begin);
                    $datetime2=new \DateTime($current_date);
                    $interval=$datetime2->diff($datetime1);
                    $intervalMeses=$interval->format("%m");
                    $intervalAnos = $interval->format("%y")*12;
                    $moths=$intervalMeses+$intervalAnos;

                    if($total_Records>0 and $moths<3){
                        $show_alert_directory_update=false;
                    }
                }
            }

            $organizations=$em->getRepository('App:Organization')->findBy(array('disabled'=>0));

            return $this->render('organization/view-missing-organization-information.html.twig', [
                'users' => $users, 'roles' => $roles, 'organizations' => $organizationsT,'organizations'=>$organizations,
                'providers' => $providers,'organization'=>$organization,'organization_lob_completed'=>$organization_lob_completed,
                'payers' => $payers,'cmsTrainings'=>$cmsTrainings,'hasMMM'=>$hasMMM,'hasFcc'=>$hasFcc,
                'prospect' => $prospects,'requestsOrg'=>$requestsOrg,
                'providersResult'=>$providersResult,'organization_types'=>$organization_types,
                'addressResult'=>$addressResult,'organization_lob'=>$organization_lob,
                'contactResult'=>$contactResult,'totaluserP'=>$totaluserP,'userValdiate'=>$userValdiate,'usersP'=>$usersP,
                'organization_check'=>$organization_check,'providers_check'=>$providers_check,'address_check'=>$address_check,
                'contacts_check'=>$contacts_check,'training_attestation'=>$training_attestation,'contactResult2'=>$contactResult2,
                'contact_check'=>$contact_check,'addresses_check'=>$addresses_check,'lineofBusiness'=>$lineofBusiness,
                'columns_addr'=>$columns_addr,'provider_check'=>$provider_check, 'totalBilling'=>$totalBilling,'validateRoster'=>$validateRoster,
                'addr_type_check'=>$addr_type_check,'addr_check'=>$addr_check,'organizations_status'=>$organizations_status,
                'primary_addr'=>$primary_addr,'billing_addr'=>$billing_addr,'mailing_addr'=>$mailing_addr,
                'credentialingProcess'=>$credentialingProcess,'providersCred'=>$providersCred,'org_facility'=>$org_facility,
                'check_org_accreditation'=>$check_org_accreditation,'check_org_groupnpi'=>$check_org_groupnpi,'check_org_billingnpi'=>$check_org_billingnpi,
                'check_org_tin'=>$check_org_tin,'check_org_legal_name_tin_owner'=>$check_org_legal_name_tin_owner,'check_org_billing_phone'=>$check_org_billing_phone,
                'check_org_phone'=>$check_org_phone,'show_alert_directory_update'=>$show_alert_directory_update
            ]);

        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/load-facility-licenses/{id}", name="organization_load_facility_licenses",defaults={"id":null})
     */
    public function uploadFileFacilityLcienses($id)
    {
        $em=$this->getDoctrine()->getManager();
        $organization=$em->getRepository('App:Organization')->find($id);
        return $this->render('organization/upload_facility_licenses.html.twig',['organization'=>$organization]);
    }


    /**
     * @Route("/facility-licenses-upload-proccess", name="admin_facility_licenses_upload_proccess")
     */
    function uploadProccess(Request $request){
        set_time_limit(8600);
        $em=$this->getDoctrine()->getManager();
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $file = $request->files->get('credentialing_load');
            $organization_id=$request->get('org');

            $organization=$em->getRepository('App:Organization')->find($organization_id);

            $fileName = "";
            $fileExtension="";
            if ($file != "") {
                $nombreoriginal = $file->getClientOriginalName();
                $fileName = $nombreoriginal;
                $adjuntosDir = $this->getparameter('facility_specialties_loaded');
                $fileExtension=$file->guessExtension();
                $file->move($adjuntosDir, $fileName);
            }

            //step for read the excel and read de information
            $fileToRead = $this->getparameter('facility_specialties_loaded') . "/" . $fileName;

            $fileType="";

            if($fileExtension=="xlsx"){
                $reader = new Reader\Xlsx();
                $reader->setReadDataOnly(true);
                $spreadsheet = $reader->load($fileToRead);
                $spreadsheet->setActiveSheetIndex(0);
                $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
                $fileType="xlsx";
            }

            if($fileExtension=="xls"){
                $reader = new Reader\Xls();
                $reader->setReadDataOnly(true);
                $spreadsheet = $reader->load($fileToRead);
                $spreadsheet->setActiveSheetIndex(0);
                $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
                $fileType="xls";
            }

            if($fileExtension=="csv" or $fileExtension=="txt"){
                $reader = new Reader\Csv();
                $spreadsheet = $reader->load($fileToRead);
                $reader->setDelimiter(',');
                $reader->setEnclosure('');
                $reader->setSheetIndex(0);
                $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
                $fileType="csv";
            }

            $records=$em->getRepository('App\Entity\CredSimpleRecord')->findAll();
            if($records!=null){
                foreach ($records as $record){
                    $em->remove($record);
                }
                $em->flush();
            }

            $cont=1;

            $data=[];

            // address on Organization
            $addresses=$em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization_id));
            if($addresses){

                foreach ($sheetData as $sheet) {
                    if($cont>1){
                        if ($sheet['A'] != null or $sheet['A'] != "") {

                            $ahca_number=$sheet['A'];
                            $license_number=$sheet['B'];
                            $effective_date=$sheet['C'];
                            $expiration_date=$sheet['D'];
                            $facility_type=$sheet['E'];
                            $street_address=$sheet['G'];
                            $beds=$sheet['X'];

                            //check if street record match with address on organization
                            $match='No';
                            $exist=false;
                            $addr_id=null;
                            foreach ($addresses as $addr){
                                $street_current=$addr->getStreet();
                                $fl_street_current=explode(" ",$street_current)[0];
                                $fl_street_address=explode(" ",$street_address)[0];

                               if($fl_street_current==$fl_street_address){
                                   $match='Yes';
                                   $addr_id=$addr->getId();

                                   //check if exit a current facility_license on database
                                   $current_license=$em->getRepository('App:AddressFacilityLicense')->findOneBy(array('address'=>$addr->getId(),'ahca_number'=>$ahca_number,'license_number'=>$license_number));

                                   if($current_license){
                                       $exist=true;
                                   }

                                   break;
                               }
                            }

                            $license_data=[
                                'match'=>$match,
                                'exist'=>$exist,
                                'addr_id'=>$addr_id
                            ];

                            $facility_license=null;
                            if($license_data['match']=='Yes'){
                                if($license_data['exist']==true){
                                    $facility_license=$em->getRepository('App:AddressFacilityLicense')->findOneBy(array('ahca_number'=>$ahca_number,'license_number'=>$license_number));
                                }else{
                                    $facility_license=new AddressFacilityLicense();
                                }

                                $new_effective_date="";
                                $new_expiration_date="";
                                //format for effective date
                                if($effective_date!=""){
                                    $effective_date_array=explode("/",$effective_date);

                                    $mont=$effective_date_array[0];
                                    if(strlen($mont)==1){
                                        $mont="0".$mont;
                                    }

                                    $day=$effective_date_array[1];
                                    if(strlen($day)==1){
                                        $day="0".$day;
                                    }

                                    $new_effective_date=$mont."/".$day."/".$effective_date_array[2];
                                }

                                //format for expiration date
                                if($expiration_date!=""){
                                    $expiration_date_array=explode("/",$expiration_date);
                                    $mont2=$expiration_date_array[0];
                                    if(strlen($mont2)==1){
                                        $mont2="0".$mont2;
                                    }

                                    $day2=$expiration_date_array[1];
                                    if(strlen($day2)==1){
                                        $day2="0".$day2;
                                    }

                                    $new_expiration_date=$mont2."/".$day2."/".$expiration_date_array[2];
                                }

                                $address_obj=$em->getRepository('App:BillingAddress')->find($license_data['addr_id']);

                                $facility_license->setAhcaNumber($ahca_number);
                                $facility_license->setLicenseNumber($license_number);
                                $facility_license->setEffectiveDate($new_effective_date);
                                $facility_license->setExpirationDate($new_expiration_date);
                                $facility_license->setFacilityType($facility_type);
                                $facility_license->setBeds($beds);
                                $facility_license->setAddress($address_obj);

                                $em->persist($facility_license);
                                $em->flush();
                            }
                        }
                    }
                    $cont++;
                }
            }

            return $this->redirectToRoute('admin_view_organization',['id'=>$organization_id]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }
}
