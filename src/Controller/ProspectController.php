<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\Prospect;
use App\Entity\ProspectAddress;
use App\Entity\Vivida;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader;

/**
 * @Route("/admin/prospects")
 */
class ProspectController extends AbstractController
{
    /**
     * @Route("/prospect", name="prospect")
     */
    public function index()
    {
        return $this->render('prospect/index.html.twig', [
            'controller_name' => 'ProspectController',
        ]);
    }

    /**
     * @Route("/load-from-fcc", name="prospect-load-from-fcc", methods={"GET"})
     */
    public function load_from_fcc(){
        set_time_limit(7200);
        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "template_xls/prospect-nominations.xlsx";

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=0;

        foreach ($sheetData as $sheet) {
            if ($cont > 0) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    if($sheet['E']=="No"){
                        $name=$sheet['A'];
                        $npi=$sheet['B'];
                        $npi2=$sheet['C'];
                        $address=$sheet['D'];
                        $is_facility=$sheet['F'];
                        $notes=$sheet['K'];

                        $prospect=$em->getRepository('App\Entity\Prospect')->findOneBy(array('group_npi'=>$npi));

                        if($prospect==null){
                            $prospect=new Prospect();
                            $prospect->setName($name);
                            $prospect->setGroupNpi($npi);
                            $prospect->setGroupNpi2($npi2);
                            $prospect->setAddress($address);
                            $prospect->setIsFacility($is_facility);
                            $prospect->setNotes($notes);

                            $em->persist($prospect);
                            $em->flush();
                        }
                    }
                }
            }
            $cont++;
        }

        return new Response('Process Finished');
    }

    /**
     * @Route("/match-from-fcc", name="prospect-match-from-fcc", methods={"GET"})
     */
    public function match_from_fcc()
    {
        set_time_limit(7200);
        $em=$this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App\Entity\Organization')->findAll();
        $prospects= $em->getRepository('App\Entity\Prospect')->findAll();

        $contF=1;
        foreach ($organizations as $organization){
            $cont=0;

            $phone=$organization->getPhone();
            $phone=str_replace("-","",$phone);
            $billing_phone=$organization->getBillingPhone();
            $billing_phone=str_replace("-","",$billing_phone);
            $fax=$organization->getFaxno();
            $fax=str_replace("-","",$fax);

            $tin=$organization->getTinNumber();

            $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));

            foreach ($prospects as $prospect){
                if($prospect->getPhone()!="" and $prospect->getPhone()==$phone){
                    $cont++;
                }
                if($prospect->getPhone()!="" and $prospect->getPhone()==$billing_phone){
                    $cont++;
                }

                if($prospect->getFax()!="" and $prospect->getFax()==$fax){
                    $cont++;
                }
                if($prospect->getTin()!="" and $prospect->getTin()==$tin){
                    $cont++;
                }

                if($cont==0){
                    if($contacts!=null){
                        foreach ($contacts as $contact){
                            $contact_phone=$contact->getPhone();
                            $contact_fax=$contact->getFax();
                            $contact_email=$contact->getEmail();

                            $contact_phone=str_replace("-","",$contact_phone);
                            $contact_fax=str_replace("-","",$contact_fax);
                            $contact_email=str_replace("-","",$contact_email);

                            if($prospect->getPhone()!="" and $prospect->getPhone()==$contact_email){
                                $cont++;
                            }

                            if($prospect->getPhone()!="" and $prospect->getPhone()==$contact_fax){
                                $cont++;
                            }

                            if($prospect->getPhone()!="" and $prospect->getPhone()==$contact_phone){
                                $cont++;
                            }
                        }
                    }
                }

                if($cont>0){
                   $em->remove($prospect);
                   $em->flush();
                }

                $cont=0;
            }
        }

        return new Response('Process Finished');
    }


    /**
     * @Route("/load-data", name="prospect-load", methods={"GET"})
     */
    public function load(){
        set_time_limit(7200);
        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "template_xls/vivida_providers.xlsx";

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=1;

        $status=null;
        foreach ($sheetData as $sheet) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                   $org_name=$sheet['A'];
                   $addr=$sheet['B'];
                   $city=$sheet['C'];
                   $state=$sheet['D'];
                   $zipcode=$sheet['E'];
                   $phone=$sheet['F'];
                   $email=$sheet['G'];

                   $prospect=new Prospect();
                   $prospect->setAddress($addr);
                   $prospect->setEmail($email);
                   $prospect->setPhone($phone);
                   $prospect->setName($org_name);
                   $prospect->setStatus($status);
                   $em->persist($prospect);

                    $prospect_address=new ProspectAddress();
                    $prospect_address->setProspect($prospect);
                    $prospect_address->setStreet($addr);
                    $prospect_address->setCity($city);
                    $prospect_address->setState($state);
                    $prospect_address->setZipCode($zipcode);

                    $em->persist($prospect_address);
                    $em->flush();
                }
        }

        echo "All is processed";
        die();
    }

    /**
     * @Route("/vivida-compare-report", name="vivida_compare_report", methods={"GET"})
     */
    public function vivida_compare_report(){
        set_time_limit(7200);
        $em=$this->getDoctrine()->getManager();

        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');;

        $spreadsheet = $reader->load($url.'vivida_providers2.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $em=$this->getDoctrine()->getManager();

        $cont=1;
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $providers_check=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));

        foreach ($sheetData as $sheet) {
            if ($sheet['A'] != null or $sheet['A'] != "") {
                $name=$sheet['A'];
                $name_array=explode(' ',$name);

                if(count($name_array)==3){
                    $new_name=$name_array[0]." ".$name_array[1];
                    $degree=$name_array[2];
                    $match=0;
                    foreach ($providers_check as $pro){
                        $current_full_name=$pro->getFirstName()." ".$pro->getLastName();
                        if (strcmp($current_full_name, $new_name) === 0){
                            $match++;
                        }
                    }

                    if($match>0){
                        $range="A".$cont.":"."I".$cont;
                        $spreadsheet->getActiveSheet()->getStyle($range)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('3fc738');
                    }

                    $cell_L = 'L' . $cont;
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $degree);
                }
                $cont++;
            }
        }

        $cont_t=1;
        //get the region for the addr
        foreach ($sheetData as $sheet) {
            if ($sheet['A'] != null or $sheet['A'] != "") {
                $zip_code=$sheet['H'];
                $addrs=$em->getRepository('App\Entity\BillingAddress')->findBy(array('zipCode'=>$zip_code));
                $cell_K = 'K' . $cont_t;
                $region="";
                if($addrs){
                    foreach ($addrs as $addr){
                        if($addr->getRegion()!="" and $addr->getRegion()!=null){
                            $region=$addr->getRegion();
                        }
                    }
                }
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $region);
            }
            $cont_t++;
        }

        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0,'is_facility'=>0));

        $cont=3615;
        $providers_npis=[];
        foreach ($providers as $provider){
            $npi=$provider->getNpiNumber();
            $id=$provider->getId();
            if($npi!="" and $npi!="NOT AVAILABLE" and $npi!="N/A" and $npi!="n/a"){
                if(!in_array($npi,$providers_npis)){
                    $providers_npis[]=$npi;

                    $providers_npi=$em->getRepository('App\Entity\Provider')->findBy(array('npi_number'=>$npi,'disabled'=>0));
                    if(count($providers_npi)>1){
                        foreach ($providers_npi as $item){
                            $address=$item->getBillingAddress();
                            if($address!=null) {
                                foreach ($address as $addr2) {
                                    $cell_A = 'A' . $cont;
                                    $cell_B = 'B' . $cont;
                                    $cell_D = 'D' . $cont;
                                    $cell_E = 'E' . $cont;
                                    $cell_F = 'F' . $cont;
                                    $cell_G = 'G' . $cont;
                                    $cell_H = 'H' . $cont;
                                    $cell_J = 'J' . $cont;
                                    $cell_K = 'K' . $cont;
                                    $cell_L = 'L' . $cont;

                                    $full_name = $item->getFirstName() . " " . $item->getLastName();
                                    $organization_name = $item->getOrganization()->getName();
                                    $phone = $addr2->getPhoneNumber();
                                    $street = $addr2->getStreet();
                                    $city = $addr2->getCity();
                                    $state = $addr2->getUsState();
                                    $zip_code = $addr2->getZipCode();
                                    $region=$addr2->getRegion();

                                    $degrees=$item->getDegree();
                                    $degree_str="";
                                    if($degrees){
                                        foreach ($degrees as $degree){
                                            if($degree->getName()!=""){
                                                $degree_str=$degree->getName();
                                            }
                                        }
                                    }

                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $full_name);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $phone);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $organization_name);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $street);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $city);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $state);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $zip_code);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, 'Yes');
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $region);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $degree_str);

                                    $cont++;
                                }
                            }
                        }
                    }else{
                       $address=$provider->getBillingAddress();
                       if($address!=null){
                           foreach ($address as $addr){
                               $cell_A='A'.$cont;
                               $cell_B='B'.$cont;
                               $cell_D='D'.$cont;
                               $cell_E='E'.$cont;
                               $cell_F='F'.$cont;
                               $cell_G='G'.$cont;
                               $cell_H='H'.$cont;
                               $cell_J='J'.$cont;
                               $cell_K = 'K' . $cont;
                               $cell_L = 'L' . $cont;

                               $full_name=$provider->getFirstName()." ".$provider->getLastName();
                               $organization_name=$provider->getOrganization()->getName();
                               $phone=$addr->getPhoneNumber();
                               $street=$addr->getStreet();
                               $city=$addr->getCity();
                               $state=$addr->getUsState();
                               $zip_code=$addr->getZipCode();
                               $region=$addr->getRegion();

                               $degrees=$provider->getDegree();
                               $degree_str="";
                               if($degrees){
                                   foreach ($degrees as $degree){
                                       if($degree->getName()!=""){
                                           $degree_str=$degree->getName();
                                       }
                                   }
                               }

                               $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $full_name);
                               $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $phone);
                               $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $organization_name);
                               $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $street);
                               $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $city);
                               $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $state);
                               $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $zip_code);
                               $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, 'Yes');
                               $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $region);
                               $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $degree_str);
                               
                               $cont++;
                           }
                       }
                    }

                }

            }
        }


        $organizations=$em->getRepository('App\Entity\Organization')->findBy(array('disabled'=>0));
        foreach ($organizations as $organization){
            $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));

            $is_facility=false;
            if($address!=null){
                foreach ($address as $addr){
                    if($addr->getIsFacility()==true){
                        $is_facility=true;
                    }
                }
            }

            if($is_facility){
                foreach ($address as $addr3){
                    $cell_A='A'.$cont;
                    $cell_B='B'.$cont;
                    $cell_D='D'.$cont;
                    $cell_E='E'.$cont;
                    $cell_F='F'.$cont;
                    $cell_G='G'.$cont;
                    $cell_H='H'.$cont;
                    $cell_J='J'.$cont;
                    $cell_K='K'.$cont;

                    $full_name=$organization->getName();
                    $phone=$addr3->getPhoneNumber();
                    $street=$addr3->getStreet();
                    $city=$addr3->getCity();
                    $state=$addr3->getUsState();
                    $zip_code=$addr3->getZipCode();
                    $region=$addr3->getRegion();

                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $full_name);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $phone);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $full_name);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $street);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $city);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $state);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $zip_code);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, 'Yes');
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $region);

                    $cont++;
                }
            }
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Vivida_compare'.date('Ymd').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created

        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/vivida-compare-upload", name="vivida_compare_upload", methods={"GET"})
     */
    public function vivida_upload(){
        set_time_limit(7200);

        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');;

        $spreadsheet = $reader->load($url.'vivida_upload.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $em=$this->getDoctrine()->getManager();
        $cont=0;
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        foreach ($sheetData as $sheet) {
            if($cont>0){
                if ($sheet['A'] != null or $sheet['A'] != "") {

                    $vivida=new Vivida();
                    $vivida->setName($sheet['A']);
                    $vivida->setDegree($sheet['B']);
                    $vivida->setPracticeName($sheet['C']);
                    $vivida->setNetwork($sheet['I']);
                    $vivida->setRegion($sheet['J']);
                    $em->persist($vivida);
                }
            }
            $cont++;
        }

        $em->flush();

        return new Response("Report Imported Succefully");
    }

    /**
     * @Route("/vivida-compare-export", name="vivida_compare_export", methods={"GET"})
     */
    public function vivida_export(){
        set_time_limit(7200);

        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";

        $reader=IOFactory::createReader('Xlsx');;

        $spreadsheet = $reader->load($url.'vivida_template.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $em=$this->getDoctrine()->getManager();
        $providers=$em->getRepository('App\Entity\Vivida')->findAll();
        $cont=2;

        foreach ($providers as $record){
            $cell_A='A'.$cont;
            $cell_B='B'.$cont;
            $cell_C='C'.$cont;
            $cell_D='D'.$cont;
            $cell_E='E'.$cont;
            $cell_F='F'.$cont;

            if($record->getNewStatus()=="BSN"){
                $range="A".$cont.":"."F".$cont;
                $spreadsheet->getActiveSheet()->getStyle($range)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('14cfe6');
            }

            if($record->getNewStatus()=="BOTH"){
                $range="A".$cont.":"."F".$cont;
                $spreadsheet->getActiveSheet()->getStyle($range)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('20cc43');
            }

            if($record->getNewStatus()==""){
                $range="A".$cont.":"."F".$cont;
                $spreadsheet->getActiveSheet()->getStyle($range)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('e72c2c');
            }

            $spreadsheet->getActiveSheet(0)->setCellValue($cell_A,$record->getName());
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_B,$record->getDegree());
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_C,$record->getPracticeName());
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_D,$record->getNewStatus());
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_E,$record->getRegion());
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_F,$record->getNetwork());
            $cont++;
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="vivida'.date('Ymd').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

}
