<?php

namespace App\Controller;

use App\Entity\EmailQueued;
use App\Entity\UserOptions;
use App\Entity\UserOrgRequest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\User;
use Symfony\Component\Mailer\Transport;

/**
 * @Route("/admin/user")
 */
class UserController extends AbstractController{

    /**
     * @Route("/index", name="admin_user")
    */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $users = $em->getRepository('App\Entity\User')->findAll();
            $delete_form_ajax = $this->createCustomForm('ROLE_ID', 'DELETE', 'admin_delete_user');

            return $this->render('user/index.html.twig', array('users' => $users, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/provider-users", name="admin_puser_list")
     */
    public function index2(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $users = $em->getRepository('App\Entity\User')->findAll();
            $delete_form_ajax = $this->createCustomForm('USER_ID', 'DELETE', 'admin_delete_user');

            $usersP=array();

            foreach ($users as $user){
                if($user->getOrganization()!=null){
                    $usersP[]=$user;
                }
            }

            return $this->render('user/user-providers.html.twig', array('users' => $usersP, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/providers-to-verify", name="admin_provider_to_verify_list")
     */
    public function index3(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $providers = $em->getRepository('App\Entity\ProviderToVerify')->findAll();

            return $this->render('user/providers-to-verify.html.twig', array('providers' => $providers));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_user")
     * 
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $roles = $em->getRepository('App\Entity\Role')->findAll();
            $organizations=$em->getRepository('App\Entity\Organization')->findAll();

            return $this->render('user/add.html.twig', array('roles' => $roles,'organizations'=>$organizations));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_user",defaults={"id": null} )
     * 
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\User')->find($id);
            $organizations=$em->getRepository('App\Entity\Organization')->findAll();

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_user');
            }
            $roles = $em->getRepository('App\Entity\Role')->findAll();

            return $this->render('user/edit.html.twig', array('document' => $document, 'roles' => $roles,'organizations'=>$organizations));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_user",defaults={"id": null} )
     *
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\User')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_user');
            }

            return $this->render('user/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/profile", name="admin_profile_user")
     *
     */
    public function profile(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();

            $roles=$em->getRepository('App\Entity\Role')->findAll();

            $is_admin=false;

            $current_roles=$user->getRoles();
            foreach ($current_roles as $role){
                if($role=="ROLE_ADMIN"){
                    $is_admin=true;
                }
            }



            return $this->render('user/profile.html.twig', array('user' => $user,'roles'=>$roles,'is_admin'=>$is_admin));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_user")
     *
     */
    public function create(Request $request, UserPasswordEncoderInterface $encoder){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $last_name = $request->get('last_name');
            $user_name = $request->get('user_name');
            $email = $request->get('email');
            $account_non_expired = $request->get('account_non_expired');
            $credentials_non_expired = $request->get('credentials_non_expired');
            $account_non_locked = $request->get('account_non_locked');
            $enabled = $request->get('enabled');
            $password = $request->get('password');
            $roles = $request->get('roles');
            $organizations = $request->get('organizations');

            $new = $request->get('new');

            $user = new User();
            $user->setName($name);
            $user->setLastName($last_name);
            $user->setEmail($email);
            $user->setUsername($user_name);

            $encode = $encoder->encodePassword($user, $password);
            $user->setPassword($encode);
            $user->setEnabled($enabled);
            $user->setAccountNonExpired($account_non_expired);
            $user->setAccountNonLocked($account_non_locked);
            $user->setCredentialsNonExpired($credentials_non_expired);

            if($organizations!=""){
                foreach ($organizations as $organization){
                    $objOrg=$em->getRepository('App\Entity\Organization')->find($organization);
                    if($objOrg!==null){
                        $user->addOrganization($objOrg);
                    }
                }
            }

            if ($roles !== null or $roles !== "") {
                foreach ($roles as $role) {
                    $rol = $em->getRepository('App\Entity\Role')->find($role);
                    if ($rol !== null) {
                        $user->addRole($rol);
                    }
                }
            }

            $em->persist($user);
            $em->flush();

            $this->addFlash(
                'success',
                'User has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_user');
            }

            return $this->redirectToRoute('admin_user');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_user")
     * 
     */
    public function update(Request $request,UserPasswordEncoderInterface $encoder){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $last_name = $request->get('last_name');
            $user_name = $request->get('user_name');
            $email = $request->get('email');
            $account_non_expired = $request->get('account_non_expired');
            $credentials_non_expired = $request->get('credentials_non_expired');
            $account_non_locked = $request->get('account_non_locked');
            $enabled = $request->get('enabled');
            $roles = $request->get('roles');
            $id = $request->get('id');
            $password = $request->get('password');
            $new = $request->get('new');
            $organization = $request->get('organization');
            $organizations = $request->get('organizations');
            $user = $em->getRepository('App\Entity\User')->find($id);

            if ($user == null) {
                $this->addFlash(
                    "danger",
                    "The User can't been updated!"
                );

                return $this->redirectToRoute('admin_user');
            }

            if ($user != null) {
                $user->setName($name);
                $user->setLastName($last_name);
                $user->setEmail($email);
                $user->setUsername($user_name);
                $user->setEnabled($enabled);
                $user->setAccountNonExpired($account_non_expired);
                $user->setAccountNonLocked($account_non_locked);
                $user->setCredentialsNonExpired($credentials_non_expired);
                $user->setUpdatedAt();

                $organizationObj=$em->getRepository('App\Entity\Organization')->find($organization);

                if($organizationObj!=null){
                    $user->setOrganization($organizationObj);
                }

                if($organizations!=""){
                    $currentOrgs=$user->getOrganizations();
                    if($currentOrgs!=null){
                        foreach ($currentOrgs as $currentOrg){
                            $user->removeOrganization($currentOrg);
                            $em->persist($user);
                            $em->flush();
                        }
                    }
                    foreach ($organizations as $organization){
                        $objOrg=$em->getRepository('App\Entity\Organization')->find($organization);
                        if($objOrg!=null){
                            $user->addOrganization($objOrg);
                        }

                    }
                }

                if($password!=""){
                    $encode = $encoder->encodePassword($user, $password);
                    $user->setPassword($encode);
                }

                //check for previos roles
                $rolExist = $user->getUserRoles();
                if ($rolExist != null) {
                    foreach ($rolExist as $re) {
                        if ($re != null)
                            $user->removeRole($re);
                    }
                }

                if ($roles !== null or $roles !== "") {
                    foreach ($roles as $role) {
                        $rol = $em->getRepository('App\Entity\Role')->find($role);
                        if ($rol !== null) {
                            $user->addRole($rol);
                        }
                    }
                }

                $em->persist($user);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "User has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_user');
            }

            return $this->redirectToRoute('admin_user');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_user",methods={"POST","DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $user = $user = $em->getRepository('App\Entity\User')->find($id);
            $removed = 0;
            $message = "";

            if ($user) {
                try {
                    $em->remove($user);
                    $em->flush();
                    $removed = 1;
                    $message = "The User has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The user can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_user",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $user = $user = $em->getRepository('App\Entity\User')->find($id);

                if ($user) {
                    try {
                        $em->remove($user);
                        $em->flush();
                        $removed = 1;
                        $message = "The User has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The User can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{

        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }

    /**
     * @Route("/send-test", name="admin_sent-test-email")
     */
    public function testSendEmail(\Swift_Mailer $mailer) {

        $message = (new \Swift_Message('Hello Email'))
            ->setFrom('info@bsnnet.com')
            ->setTo('asdrubaltc2014@gmail.com')
            ->setBody('You should see me from the profiler!')
        ;

        $mailer->send($message);

      return new Response('OK');
    }

    /**
     * @Route("/update-profile", name="admin_update_user_profile")
     *
     */
    public function update_profile(Request $request,UserPasswordEncoderInterface $encoder,\Swift_Mailer $mailer){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $user=$this->getUser();
            $old_name=$user->getName();
            $old_last_name=$user->getLastName();
            $old_email=$user->getEmail();

            $first_name=$request->get('firstname');
            $last_name=$request->get('last_name');
            $email=$request->get('email');
            $new_password=$request->get('new_password');

            $user->setName($first_name);
            $user->setLastName($last_name);
            $user->setEmail($email);

            if($new_password!=""){
                $encode = $encoder->encodePassword($user, $new_password);
                $user->setPassword($encode);
            }

            $em->persist($user);
            $em->flush();

            $date_final=date('F d, Y')." at ".date('H:i a');

            $name_change=false;
            $last_name_change=false;
            $email_change=false;
            $password_change=false;

            if($old_name!=$first_name){
                $name_change=true;
            }
            if($old_last_name!=$last_name){
                $last_name_change=true;
            }
            if($old_email!=$email){
                $email_change=true;
            }

            if($new_password!=""){
                $password_change=true;
            }

            if($old_email==$email){
                $message = (new \Swift_Message('Your Account Update'))
                    ->setFrom('info@bsnnet.com')
                    ->setTo($email)
                    ->setBody($this->renderView(
                        'email/templates/account-updated.html.twig',array('date'=>$date_final,'user'=>$user,'name_change'=>$name_change,'last_name_change'=>$last_name_change,
                            'email_change'=>$email_change,'password_change'=>$password_change )
                    ), 'text/html');

                $mailer->send($message);
            }else{
                $message2 = (new \Swift_Message('Your Account Update'))
                    ->setFrom('info@bsnnet.com')
                    ->setTo($old_email)
                    ->setBody($this->renderView(
                        'email/templates/account-updated.html.twig',array('date'=>$date_final,'user'=>$user,'name_change'=>$name_change,'last_name_change'=>$last_name_change,
                            'email_change'=>$email_change,'password_change'=>$password_change )
                    ), 'text/html');

                $mailer->send($message2);

                $message3 = (new \Swift_Message('Your Account Update'))
                    ->setFrom('info@bsnnet.com')
                    ->setTo($email)
                    ->setBody($this->renderView(
                        'email/templates/account-updated.html.twig',array('date'=>$date_final,'user'=>$user,'name_change'=>$name_change,'last_name_change'=>$last_name_change,
                            'email_change'=>$email_change,'password_change'=>$password_change )
                    ), 'text/html');

                $mailer->send($message3);
            }




            return $this->redirectToRoute('home');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/add-options", name="admin_add_options")
     *
     */
    public function addOptions(){
        $em = $this->getDoctrine()->getManager();

        $users=$em->getRepository('App\Entity\User')->findAll();
        foreach ($users as $user){
            $userOptions=new UserOptions();
            $userOptions->setUser($user);
            $userOptions->setEmailNotification(true);

            $em->persist($userOptions);
        }
        $em->flush();
    }

    /**
     * @Route("/recovery-password", name="admin_user_recovery")
     *
     */
    public function admin_user_recovery(){

        return $this->render('user/password_recovery_email.html.twig');
    }

    /**
     * @Route("/check-user-email", name="admin_check_user_email", methods={"POST"})
     *
     */
    public function admin_check_user_email(Request $request){

        $em = $this->getDoctrine()->getManager();
        $email=$request->get('email');
        $valid_email=false;

        $user=$em->getRepository('App:User')->findOneBy(array('email'=>$email));
        if($user){
            $valid_email=true;
        }

        return new Response(
            json_encode(array('valid_email' => $valid_email)), 200, array('Content-Type' => 'application/json')
        );
    }


    /**
     * @Route("/send-email-recovery-password", name="send_admin_user_recovery", methods={"POST"})
     *
     */
    public function admin_user_recovery_email(Request $request,\Swift_Mailer $mailer){
        $em=$this->getDoctrine()->getManager();

        $email=$request->get('email');
        $code_hass=uniqid("ppr_",true);
        $user=$em->getRepository('App\Entity\User')->findOneBy(array('email'=>$email));

        if($user!=null){
            $user->setResetCodeHass($code_hass);
            $em->persist($user);
            $em->flush();

            $name=$user->getName();

            $link="";
            $server= $_SERVER['SERVER_NAME'];

            if($server=="127.0.0.1"){
                $link="http://127.0.0.1:8000/admin/user/set-new-password/".$code_hass;
            }else{
                $link="http://admin.bsnnet.com/admin/user/set-new-password/".$code_hass;
            }

            $message = (new \Swift_Message('Get a new Password'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email)
                ->setBody($this->renderView(
                    'email/templates/admin_recovery_password.html.twig',array('name'=>$name,'code_hass'=>$code_hass,'link'=>$link)
                )
                    , 'text/html');

            $mailer->send($message);
        }

        return $this->redirectToRoute('admin_check_email');
    }

    /**
     * @Route("/set-new-password/{code}", name="send_admin_user_set_password",methods={"GET"})
     *
     */
    public function admin_user_set_password($code){
        $em=$this->getDoctrine()->getManager();
        $user=$em->getRepository('App\Entity\User')->findOneBy(array('reset_code_hass'=>$code));

        return $this->render('user/reset_password_form.html.twig',array('user'=>$user,'code'=>$code));
    }

    /**
     * @Route("/set-new-pasword", name="admin_user_save_new_password", methods={"POST"})
     *
     */
    public function admin_user_save_new_password(Request $request,UserPasswordEncoderInterface $encoder){

        $em=$this->getDoctrine()->getManager();
        $code=$request->get('code');
        $password=$request->get('password');

        $user=$em->getRepository('App\Entity\User')->findOneBy(array('reset_code_hass'=>$code));

        if($user!=null) {
            $encode = $encoder->encodePassword($user, $password);
            $user->setPassword($encode);

            $em->persist($user);
            $em->flush();
        }

        return  $this->redirectToRoute('admin_login');
    }

    /**
     * @Route("/check-email", name="admin_check_email")
     *
     */
    public function payer_check_email(){
        return $this->render('user/check_email.html.twig');
    }

    /**
     * @Route("/set-organization", name="admin_user_setorganization",methods={"POST"})
     *
     */
    public function setOrganization(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em=$this->getDoctrine()->getManager();
            $id=$request->get('set_organization');

            $user=$this->getUser();
            $organization=$em->getRepository('App\Entity\Organization')->find($id);
            if($organization!=null){
                $user->setOrganization($organization);
                $em->persist($user);
                $em->flush();
            }
            return $this->redirectToRoute('home');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/add-request-org", name="admin_user_get_org_request",methods={"POST"})
     *
    */
    public function addRequestOrganization(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em=$this->getDoctrine()->getManager();
            $organization=$request->get('organization_selected');
            $contactsID=$request->get('contacts_selected');

            $user=$this->getUser();

            $contacts=explode(',',$contactsID);

            foreach ($contacts as $contact){
                $contactObj=$em->getRepository('App\Entity\OrganizationContact')->find($contact);

                if($contactObj!=null){
                    $orgObj=$em->getRepository('App\Entity\Organization')->find($organization);

                    $orgRequest=new UserOrgRequest();
                    $orgRequest->setOrganization($organization);
                    $orgRequest->setUser($user);
                    $orgRequest->setContact($contactObj);
                    $orgRequest->setOrganization($orgObj);

                    $em->persist($orgRequest);
                    $em->flush();
                }
            }

            return $this->redirectToRoute('new_contact_p_index');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/set-request-org", name="admin_user_set_org_request",methods={"POST"})
     *
     */
    public function setRequestOrganization(Request $request) {
        $em=$this->getDoctrine()->getManager();

        $request_id=$request->get('request_id');
        $request_status=$request->get('request_status');
        $userOrgRequest=$em->getRepository('App\Entity\UserOrgRequest')->find($request_id);

        if($userOrgRequest!=null){
            if($request_status==0){
                $em->remove($userOrgRequest);
            }
            if($request_status==1){
               //check if the contact have a user already exist!

                $contact=$userOrgRequest->getContact();

               if($contact!=null){
                   $email=$contact->getEmail();

                   if($email!="" or $email!=null){
                       $user=$em->getRepository('App\Entity\User')->findOneBy(array('email'=>$email));
                       if($user!=null){
                           $org=$userOrgRequest->getOrganization();
                           $currentOrganizations= $user->getOrganizations();

                           $existOrg=0;
                           foreach ($currentOrganizations as $currentOrganization){
                               if($org->getId()==$currentOrganization->getId()){
                                   $existOrg++;
                               }
                           }

                           if($existOrg==0){
                               $user->addOrganization($org);
                               $em->persist($user);

                               $em->remove($userOrgRequest);

                               $em->flush();
                           }

                       }else{
                           //will send an email to create a user
                           $emailQueue=new EmailQueued();

                           $org=$userOrgRequest->getOrganization();
                           $contact=$userOrgRequest->getContact();
                           $email=$contact->getEmail();
                           $date=date('m/d/Y');
                           $emailType=$em->getRepository('App\Entity\EmailType')->find(3);
                           $emailStatus=$em->getRepository('App\Entity\EmailStatus')->find(1);
                           $emailQueue->setOrganization($org);
                           $emailQueue->setContact($contact);
                           $emailQueue->setDateToSend(new \DateTime($date));
                           $emailQueue->setEmailType($emailType);
                           $emailQueue->setSentTo($email);
                           $emailQueue->setProviders(null);
                           $emailQueue->setStatus($emailStatus);

                           $em->persist($emailQueue);
                           $em->remove($userOrgRequest);
                           $em->flush();
                       }
                   }
               }
            }

            $em->flush();
        }
        return $this->redirectToRoute('home');
    }
}
