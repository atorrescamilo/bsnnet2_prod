<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Cvo;

/**
 * @Route("/admin/cvo")
 */
class CvoController extends AbstractController{

    /**
     * @Route("/index", name="admin_cvo")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $cvos = $em->getRepository('App\Entity\Cvo')->findAll();

            $delete_form_ajax = $this->createCustomForm('CVO_ID', 'DELETE', 'admin_delete_cvo');


            return $this->render('cvo/index.html.twig', array('cvos' => $cvos, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_cvo")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('cvo/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_cvo", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Cvo')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_cvo');
            }

            return $this->render('cvo/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_cvo", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Cvo')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_cvo');
            }

            return $this->render('cvo/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_cvo")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');

            $cvo = new Cvo();
            $cvo->setName($name);
            $cvo->setDescription($description);

            $em->persist($cvo);
            $em->flush();

            $this->addFlash(
                'success',
                'Cvo has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_cvo');
            }

            return $this->redirectToRoute('admin_cvo');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_cvo")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $cvo = $em->getRepository('App\Entity\Cvo')->find($id);

            if ($cvo == null) {
                $this->addFlash(
                    "danger",
                    "The Cvo can't been updated!"
                );

                return $this->redirectToRoute('admin_cvo');
            }

            if ($cvo != null) {
                $cvo->setName($name);
                $cvo->setDescription($description);

                $em->persist($cvo);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "The Cvo has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_cvo');
            }

            return $this->redirectToRoute('admin_cvo');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_cvo",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $cvo = $em->getRepository('App\Entity\Cvo')->find($id);
            $removed = 0;
            $message = "";

            if ($cvo) {
                try {
                    $em->remove($cvo);
                    $em->flush();
                    $removed = 1;
                    $message = "The CVO has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Cvo can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_cvo",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $cvo= $em->getRepository('App\Entity\Cvo')->find($id);

                if ($cvo) {
                    try {
                        $em->remove($cvo);
                        $em->flush();
                        $removed = 1;
                        $message = "The CVO has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The CVO can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
