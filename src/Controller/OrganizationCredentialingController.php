<?php

namespace App\Controller;

use App\Form\FacilityCredentialingType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\OrganizationCredentialing;

/**
 * @Route("/admin/facilities_credentialing")
 */
class OrganizationCredentialingController extends AbstractController
{
    /**
     * @Route("/list", name="admin_facility_credentialing_list")
     */
    public function list(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $sql="SELECT o.id, o.name, o.disabled,
            IF(SUM(IF(ba.is_facility, 1, 0)) > 0, 'Yes', 'No') AS is_facility,
            organization_status.name AS status, o.group_npi as npi_number,
            GROUP_CONCAT(DISTINCT credentialing_status.name ORDER BY credentialing_status.id ASC SEPARATOR ', ') AS credentialing_status,
            GROUP_CONCAT(DISTINCT organization_credentialing.credentialing_accepted_date ORDER BY organization_credentialing.id ASC SEPARATOR ', ') AS credentialing_accepted_date,
            GROUP_CONCAT(DISTINCT organization_credentialing.credentialing_effective_date ORDER BY organization_credentialing.id ASC SEPARATOR ', ') AS credentialing_effective_date,
            GROUP_CONCAT(DISTINCT organization_credentialing.credentialing_denied_date ORDER BY organization_credentialing.id ASC SEPARATOR ', ') AS credentialing_denied_date
            FROM  organization o
            LEFT JOIN billing_address ba on o.id = ba.organization_id
            LEFT JOIN organization_status ON organization_status.id = o.status_id
            LEFT JOIN organization_credentialing ON organization_credentialing.organization_id = o.id
            LEFT JOIN credentialing_status ON credentialing_status.id = organization_credentialing.credentialing_status_id
            WHERE o.disabled=0 and ba.is_facility=1
            GROUP BY o.id
            ORDER BY o.id";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $organizations= $stmt->fetchAll();

            $credentialingstatus=$em->getRepository('App\Entity\CredentialingStatus')->findAll();
            $delete_form_ajax = $this->createCustomForm('ORGANIZATION_ID', 'DELETE', 'admin_delete_organization_credentialing');

            return $this->render('organization_credentialing/index.html.twig', array('organizations' => $organizations,'credentialingstatus'=>$credentialingstatus,
                'delete_form_ajax' => $delete_form_ajax->createView()));

        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_organization_credentialing",defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\OrganizationCredentialing')->findOneBy(array('organization'=>$id));

            return $this->render('organization_credentialing/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_organization_credentialing",defaults={"id": null})
     */
    public function edit($id, Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $credentialing = $em->getRepository('App:OrganizationCredentialing')->findOneBy(array('organization'=>$id));
            $organization=$em->getRepository('App:Organization')->find($id);
            $form = $this->createForm(FacilityCredentialingType::class, $credentialing);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $em->persist($credentialing);
                $em->flush();

                $this->addFlash(
                    "success",
                    "Facility Credentialing has been updated successfully!"
                );

                return  $this->redirectToRoute('admin_facility_credentialing_list');
            }

            return $this->render('organization_credentialing/form.html.twig', ['form' => $form->createView(), 'action' => 'Edit','organization'=>$organization]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/add-ajax", name="admin_add_organization_credentialing",methods={"POST"})
     */
    public function addCredentialing(Request $request){
        $em = $this->getDoctrine()->getManager();
        $ids=$request->get('ids');
        foreach ($ids as $id) {
            $organization_credentialing = $em->getRepository('App\Entity\OrganizationCredentialing')->findOneBy(array('organization' => $id));
            $organization = $em->getRepository('App\Entity\Organization')->find($id);

            if ($organization_credentialing == null) {
                $organization_credentialing = new OrganizationCredentialing();
            }

            $sumitted_to_cvo = $request->get('sumitted_to_cvo');
            $complete_date = $request->get('complete_date');
            $app_received_date = $request->get('app_received_date');
            $credentialing_accepted_date = $request->get('credentialing_accepted_date');
            $saction_date = $request->get('saction_date');
            $denied_date = $request->get('denied_date');
            $credentialing_status = $request->get('credentialing_status');

            $statusObj = $em->getRepository('App\Entity\CredentialingStatus')->find($credentialing_status);

            $organization_credentialing->setOrganization($organization);
            $organization_credentialing->setCredentialingStatus($statusObj);
            $organization_credentialing->setCredentialingDate($sumitted_to_cvo);
            $organization_credentialing->setCredentialingCompleteDate($complete_date);
            $organization_credentialing->setCredentialingReceivedDate($app_received_date);
            $organization_credentialing->setCredentialingAcceptedDate($credentialing_accepted_date);
            $organization_credentialing->setCredentialingDeniedDate($denied_date);
            $organization_credentialing->setSanctionDate($saction_date);

            if ($credentialing_accepted_date != "") {
                $date_array = explode('/', $credentialing_accepted_date);
                $moth = $date_array[0];
                $year = $date_array[2];

                if ($moth == 12) {
                    $year = intval($year + 1);
                    $effecDate = "01/01" . $year;
                } else {
                    $effecDate = intval($moth + 1) . "/01/" . $year;
                }

                $organization_credentialing->setCredentialingEffectiveDate($effecDate);
            }

            $em->persist($organization_credentialing);
            $em->flush();

        }

        return new Response(
            json_encode(array('total' => 1)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/remove-organizations-credentialing-multiple", name="admin_remove_organizations_credentialing_multiple",methods={"POST"})
     */
    public function disableMultiplesOrganizationCredentialing(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $ids=$request->get('ids');

        foreach ($ids as $id) {
            $cred= $em->getRepository('App\Entity\OrganizationCredentialing')->findOneBy(array('organization'=>$id));
            if($cred){
                $em->remove($cred);
                $em->flush();
            }
        }

        return new Response(
            json_encode(array('id' => 2)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_organization_credentialing",methods={"POST","DELETE"})
     *
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $organization = $em->getRepository('App\Entity\OrganizationCredentialing')->findOneBy(array('organization'=>$id));
            $removed = 0;
            if ($organization) {
                try {
                    $em->remove($organization);
                    $em->flush();
                    $removed = 1;
                } catch (Exception $ex) {
                    $removed = 2;
                    $message = "The organization can't be removed";
                }
            }

            return new Response(
                json_encode(array('id' => $id,'removed'=>$removed)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }
}
