<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\BillingAddressCvo;
use App\Entity\ChangeLog;
use App\Entity\CredSimpleRecord;
use App\Entity\EmailQueued;
use App\Entity\Organization;
use App\Entity\OrganizationContact;
use App\Entity\Provider;
use App\Entity\ProviderCredentialing;
use App\Entity\ProviderCredFile;
use App\Form\ProviderCredentialingType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Role;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use PhpOffice\PhpSpreadsheet\Reader;
use Smalot\PdfParser;

/**
 * @Route("/admin/provider_credentialing")
 */
class ProviderCredentialingController extends AbstractController{
    /**
     * @Route("/credentialing_list", name="admin_list_credentialing")
     */
    public function list(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $credentilliang=$em->getRepository('App:ProviderCredentialing')->findAll();

            $delete_form_ajax = $this->createCustomForm('PROVIDERCREDENTIALING_ID', 'DELETE', 'admin_delete_provider_credentialing');
            $cvos=$em->getRepository('App\Entity\Cvo')->findAll();
            $payers=$em->getRepository('App\Entity\Payer')->findAll();

            return $this->render('provider_credentialing/list.html.twig', array('cvos'=>$cvos, 'payers'=>$payers, 'credentilliang'=>$credentilliang, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/credentialing-ready-list", name="admin_togo_credentialing")
     */
    public function readylist(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $sql="SELECT p.id, p.first_name , p.last_name, organization.name as organization_name, p.npi_number,
            p.medicare , p.medicaid , p.caqh, p.state_lic as license,
            GROUP_CONCAT(DISTINCT `degree`.`name` ORDER BY `degree`.`name` ASC SEPARATOR ', ') AS `degrees`
            FROM  provider p 
            LEFT JOIN organization ON p.org_id = organization.id
            LEFT JOIN provider_degree ON provider_degree.provider_id = p.id
            LEFT JOIN degree ON degree.id = provider_degree.degree_id
            WHERE (p.disabled=0 or p.disabled IS NULL ) and p.is_facility=0 and p.npi_number!='' and p.npi_number!='NOT AVAILABLE' and 
            p.caqh!='' and p.caqh!='-' and p.caqh!='N/A' and p.caqh!='Not Available' and p.caqh!='Pending' and p.caqh!='NA' and
            p.state_lic!='' and p.state_lic!='-' and p.state_lic!='n/a' and p.state_lic!='not available'
            and degree.name IS NOT NULL and degree.valid_cred=1 or p.allow_cred=1
            GROUP BY p.id
            ORDER BY p.id ASC";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $providers= $stmt->fetchAll();

            $providers_result=[];
            $providers_npis=[];

            $cont=1;
            
            foreach ($providers as $provider){
                $cred_simple=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('npi_number'=>$provider['npi_number']));
                $cred_s=$em->getRepository('App\Entity\CredSimpleRecord')->findBy(array('npi'=>$provider['npi_number']));

                if($cred_simple==null and $cred_s==null){
                    if(!in_array($provider['npi_number'],$providers_npis)){
                        $providers_npis[]=$provider['npi_number'];
                        $providers_result[]=$provider;
                    }
                }

                $cont++;
            }

            $sql2="SELECT p.id, p.first_name , p.last_name, organization.name as organization_name, p.npi_number,
            p.medicare , p.medicaid , p.caqh, p.state_lic as license,
            GROUP_CONCAT(DISTINCT `degree`.`name` ORDER BY `degree`.`name` ASC SEPARATOR ', ') AS `degrees`
            FROM  provider p 
            LEFT JOIN organization ON p.org_id = organization.id
            LEFT JOIN provider_degree ON provider_degree.provider_id = p.id
            LEFT JOIN degree ON degree.id = provider_degree.degree_id
            LEFT JOIN provider_credentialing ON provider_credentialing.provider_id = p.id
            WHERE provider_credentialing.credentialing_status_id=2
            GROUP BY p.id
            ORDER BY p.first_name";

            $stmt2 = $conn->prepare($sql2);
            $stmt2->execute();
            $providers_2= $stmt2->fetchAll();

            if($providers_2){
                foreach ($providers_2 as $provider2){
                    if(!in_array($provider2['npi_number'],$providers_npis)){
                        $providers_npis[]=$provider2['npi_number'];
                        $providers_result[]=$provider2;
                    }

                    $cont++;
                }
            }

            return $this->render('provider_credentialing/ready.html.twig', ['providers'=>$providers_result]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/sent-credsimple_list", name="admin_sent-credsimple-list")
     */
    public function listSentCredSimple(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $sentCredSimples=$em->getRepository('App\Entity\CredSimpleRecord')->findAll();

            return $this->render('provider_credentialing/sent-credsimple-list.html.twig', array('sentCredSimples'=>$sentCredSimples));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/export_to_cvo", name="admin_export_cvo")
     */
    public function exportToCvo(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $providersResult=array();
            $proArray=array();

            $credentialing_process=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('cvo'=>2,'credentialing_status'=>2));
            $readytoSend_total=0;
            $check_total=0;
            if($credentialing_process!=null){
                foreach ($credentialing_process as $cp){
                    $provider=$em->getRepository('App\Entity\Provider')->find($cp->getProvider()->getId());

                    if($provider!=null){
                        $proArray['id']=$provider->getId();
                        $proArray['npi']=$provider->getNpiNumber();
                        $proArray['first_name']=$provider->getFirstName();
                        $proArray['last_name']=$provider->getLastName();

                        $degrees=$provider->getDegree();
                        $degreeSTR="";
                        if($degrees!=null){
                            $cont=1;
                            foreach ($degrees as $degree){
                                if($cont<2){
                                    if($degree!=null){
                                        $degreeSTR=$degree->getName();
                                        if($degreeSTR=="ARNP"){
                                            $degreeSTR="APRN";
                                        }
                                        if($degreeSTR=="BCBA"){
                                            $degreeSTR="ABA";
                                        }
                                        if($degreeSTR=="LMHC"){
                                            $degreeSTR="MHC";
                                        }
                                        if($degreeSTR=="LMFT"){
                                            $degreeSTR="MFT";
                                        }
                                    }
                                }
                                $cont++;
                            }
                        }

                        $contactEmail="";
                        $phone="";

                        $organization=$provider->getOrganization();
                        $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));

                        if($contacts!=null){
                           foreach ($contacts as $contact){
                               $contactEmail=$contact->getEmail();
                               $phone=$contact->getPhone();
                               if($contactEmail!="" or $phone!=""){
                                   break 1;
                               }
                           }
                        }

                        $proArray['degree']=$degreeSTR;
                        $proArray['practice_state']=$provider->getLicState();
                        $proArray['caqh']=$provider->getCaqh();
                        $proArray['emails']=$contactEmail;
                        $proArray['phone']=$phone;

                        $npi=$provider->getNpiNumber();
                        $caqh=$provider->getCaqh();
                        $status=1;

                        if($npi=="" or $npi=="-" or $npi=="NOT AVAILABLE" or $npi=="Not Available"){
                            $status=0;
                        }
                        if($caqh=="" or $caqh=="-" or $caqh=="NOT AVAILABLE" or $caqh=="Not Available"){
                            $status=0;
                        }

                        if($status==0){
                            $proArray['status']=0;
                            $check_total++;
                        }else{
                            $proArray['status']=1;
                            $readytoSend_total++;
                        }

                        $providersResult[]=$proArray;
                    }
                }
            }


            return $this->render('provider_credentialing/export_to_cvo.html.twig',['providers'=>$providersResult,'check_total'=>$check_total,
                'readytoSend_total'=>$readytoSend_total]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create_excel_to_cvo", name="admin_create_excel_to_cvo")
    */
    public function CreateExcelToCvo(){
        $em = $this->getDoctrine()->getManager();
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xls');

        $spreadsheet = $reader->load($url.'template-send-to-cvo_v2.xls');
        $credentialing_process=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('cvo'=>2,'credentialing_status'=>2));

        $providers=array();
        if($credentialing_process!=null){
            foreach ($credentialing_process as $cp){

                $provider=$em->getRepository('App\Entity\Provider')->find($cp->getProvider());
                if($provider!=null){
                    $providers[]=$provider;
                    $cp->setCredentialingStatus($em->getRepository('App\Entity\CredentialingStatus')->find(3));
                    $date=date('m/d/Y');
                    $cp->setCredentialingDate($date);
                    $em->persist($cp);
                    $em->flush();
                }
            }

            $this->SaveLog(1,'Credentialing Proccess',null,'Export Providers to send to CVO');

            if(count($providers)>0){
                $cont=2;
                foreach ($providers as $pro){
                    $cell_A='A'.$cont;$cell_B='B'.$cont;$cell_C='C'.$cont;$cell_D='D'.$cont;$cell_E='E'.$cont;$cell_F='F'.$cont;$cell_G='G'.$cont;$cell_H='H'.$cont;$cell_I='I'.$cont;
                    $cell_J='J'.$cont;

                    $contactEmail="";
                    $phone="";

                    $organization=$pro->getOrganization();
                    $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));

                    if($contacts!=null){
                        foreach ($contacts as $contact){
                            $contactEmail=$contact->getEmail();
                            $phone=$contact->getPhone();
                            if($contactEmail!="" or $phone!=""){
                                break 1;
                            }
                        }
                    }

                    $degrees=$provider->getDegree();
                    $degreeSTR="";
                    if($degrees!=null){
                        $cont_2=1;
                        foreach ($degrees as $degree){
                            if($cont_2<2){
                                if($degree!=null){
                                    $degreeSTR=$degree->getName();
                                    if($degreeSTR=="ARNP"){
                                        $degreeSTR="APRN";
                                    }
                                    if($degreeSTR=="BCBA"){
                                        $degreeSTR="ABA";
                                    }
                                    if($degreeSTR=="LMHC"){
                                        $degreeSTR="MHC";
                                    }
                                    if($degreeSTR=="LMFT"){
                                        $degreeSTR="MFT";
                                    }
                                }
                            }
                            $cont_2++;
                        }
                    }

                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $pro->getNpiNumber());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $pro->getFirstName());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $pro->getLastName());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $degreeSTR);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, "FL");
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, "Initial");
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $pro->getCaqh());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $phone);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $contactEmail);

                    $cont++;
                }
            }
        }

        $date=date('m')."-".date('d')."-".date('Y');
        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="providers-send-to-cvo-'.$date.'.xls"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return $this->redirectToRoute('admin_list_credentialing');
    }


    /**
     * @Route("/new/{id}", name="admin_new_provider_credentialing")
    */
    public function add($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $cvos = $em->getRepository('App\Entity\Cvo')->findAll();
            $credStatus = $em->getRepository('App\Entity\CredentialingStatus')->findAll();
            $payers = $em->getRepository('App\Entity\Payer')->findAll(array('name' => 'ASC'));

            return $this->render('provider_credentialing/add.html.twig', array('id' => $id, 'cvos' => $cvos, 'credStatus' => $credStatus, 'payers' => $payers));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit_proccess/{id}", name="admin_edit2_provider_credentialing", defaults={"id": null})
    */
    public function edit($id, Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $providerCreedentialing = $em->getRepository('App\Entity\ProviderCredentialing')->find($id);

            $form = $this->createForm(ProviderCredentialingType::class, $providerCreedentialing);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()) {
                $em->persist($providerCreedentialing);
                $em->flush();

                $this->addFlash(
                    "success",
                    "Provider Credentialing has been updated successfully!"
                );

                return $this->redirectToRoute('admin_list_credentialing');
            }


            return $this->render('provider_credentialing/form.html.twig', ['form' => $form->createView(), 'action' => 'Edit', 'origin'=>'list']);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_provider_credentialing", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ProviderCredentialing')->find($id);

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('admin_view_provider', ['id' => $id, 'tab' => 2]);
            }

            return $this->render('provider_credentialing/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/details/{id}", name="admin_view2_provider_credentialing", defaults={"id": null})
     */
    public function view2($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ProviderCredentialing')->find($id);

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('admin_view_provider', ['id' => $id, 'tab' => 2]);
            }

            return $this->render('provider_credentialing/view2.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_provider_credentialing")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $cvo = $request->get('cvo');
            $provider_id = $request->get('provider_id');
            $submitted_to_cvo = $request->get('submitted_to_cvo');
            $effective_date = $request->get('effective_date');
            $sanction_date = $request->get('sanction_date');
            $accepted_date = $request->get('accepted_date');
            $complete_date=$request->get('complete_date');
            $denied_date = $request->get('denied_date');
            $appeal_date = $request->get('appeal_date');
            $credentialing_status = $request->get('credentialing_status');

            $new = $request->get('new');

            $provider_credentialing = new ProviderCredentialing();

            $cvoObj = $em->getRepository('App\Entity\Cvo')->find($cvo);
            $credStatusObj = $em->getRepository('App\Entity\CredentialingStatus')->find($credentialing_status);
            $providerObj = $em->getRepository('App\Entity\Provider')->find($provider_id);

            if ($cvoObj != null) {
                $provider_credentialing->setCvo($cvoObj);
            }

            if ($credStatusObj != null) {
                $provider_credentialing->setCredentialingStatus($credStatusObj);
            }


            if ($providerObj != null) {
                $provider_credentialing->setProvider($providerObj);
            }

            $provider_credentialing->setCredentialingDate($submitted_to_cvo);
            $provider_credentialing->setCredentialingEffectiveDate($effective_date);
            $provider_credentialing->setSanctionDate($sanction_date);
            $provider_credentialing->setCredentialingCompleteDate($complete_date);
            $provider_credentialing->setCredentialingAccepted($accepted_date);
            $provider_credentialing->setCredentialingDenied($denied_date);
            $provider_credentialing->setCredentialingAppeal($appeal_date);

            $em->persist($provider_credentialing);
            $em->flush();

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_provider_credentialing', ['id' => $provider_id]);
            }

            return $this->redirectToRoute('admin_view_provider', ['id' => $provider_id, 'tab' => 2]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_provider_credentialing")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $id = $request->get('id');
            $cvo = $request->get('cvo');
            $target=$request->get('target');
            $provider_id = $request->get('provider_id');
            $submitted_to_cvo = $request->get('submitted_to_cvo');
            $effective_date = $request->get('effective_date');
            $sanction_date = $request->get('sanction_date');
            $complete_date=$request->get('complete_date');
            $accepted_date = $request->get('accepted_date');
            $denied_date = $request->get('denied_date');
            $appeal_date = $request->get('appeal_date');
            $credentialing_status = $request->get('credentialing_status');

            $provider_credentialing = $em->getRepository('App\Entity\ProviderCredentialing')->find($id);
            if ($provider_credentialing != null) {

                $cvoObj = $em->getRepository('App\Entity\Cvo')->find($cvo);
                $credStatusObj = $em->getRepository('App\Entity\CredentialingStatus')->find($credentialing_status);
                $providerObj = $em->getRepository('App\Entity\Provider')->find($provider_id);

                if ($cvoObj != null) {
                    $provider_credentialing->setCvo($cvoObj);
                }

                if ($credStatusObj != null) {
                    $provider_credentialing->setCredentialingStatus($credStatusObj);
                }


                if ($providerObj != null) {
                    $provider_credentialing->setProvider($providerObj);
                }

                $provider_credentialing->setCredentialingDate($submitted_to_cvo);
                $provider_credentialing->setCredentialingEffectiveDate($effective_date);
                $provider_credentialing->setSanctionDate($sanction_date);
                $provider_credentialing->setCredentialingAccepted($accepted_date);
                $provider_credentialing->setCredentialingCompleteDate($complete_date);
                $provider_credentialing->setCredentialingDenied($denied_date);
                $provider_credentialing->setCredentialingAppeal($appeal_date);

                $em->persist($provider_credentialing);
                $em->flush();

            }

            $new = $request->get('new');

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_provider_credentialing', ['id' => $provider_id]);
            }

            if($target==1){
                return $this->redirectToRoute('admin_view_provider', ['id' => $provider_id, 'tab' => 2]);
            }else{
                return $this->redirectToRoute('admin_list_credentialing');
            }

        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_provider_credentialing",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $provider_credentialing = $provider_credentialing = $em->getRepository('App\Entity\ProviderCredentialing')->find($id);
            $removed = 0;
            $message = "";

            if ($provider_credentialing) {
                try {
                    $em->remove($provider_credentialing);
                    $em->flush();
                    $removed = 1;
                    $message = "The Credentialing Proccess has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Credentialing Proccess can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_cvo",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $cvo  = $em->getRepository('App\Entity\ProviderCredentialing')->find($id);

                if ($cvo) {
                    try {
                        $em->remove($cvo);
                        $em->flush();
                        $removed = 1;
                        $message = "The Credentialing Proccess has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Credentialing Proccess can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/setcvo", name="admin_setcvo_credentialing",methods={"POST"})
    */
    public function setCvo(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $ids = $request->get('ids');
            $cvo=$request->get('cvo');

            foreach ($ids as $id) {
                $providerCredentialing = $em->getRepository('App\Entity\ProviderCredentialing')->find($id);

                if($providerCredentialing!=null){
                    $cvo=$em->getRepository('App\Entity\Cvo')->find($cvo);
                    $status=$em->getRepository('App\Entity\CredentialingStatus')->find(2);
                    $providerCredentialing->setCvo($cvo);
                    $providerCredentialing->setCredentialingStatus($status);
                    $em->persist($providerCredentialing);
                    $em->flush();
                }
            }

            return new Response(
                json_encode(array('set' => 1)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/setpayer", name="admin_setpayer_credentialing",methods={"POST"})
     */
    public function setPayer(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $ids = $request->get('ids');

            $tier=$request->get('tier');

            foreach ($ids as $id) {
                $providerCredentialing = $em->getRepository('App\Entity\ProviderCredentialing')->find($id);

                if($providerCredentialing!=null){
                    $status=$em->getRepository('App\Entity\CredentialingStatus')->find(7);
                    $provider=$providerCredentialing->getProvider();
                    $tierObj=$em->getRepository('App\Entity\PaymentTier')->find($tier);

                    if($provider!=null and $tierObj!=null){
                        $provider->setPaymenttier($tierObj);
                        $em->persist($provider);
                        $em->flush();
                    }

                    $providerCredentialing->setCredentialingStatus($status);

                    $em->persist($providerCredentialing);
                    $em->flush();
                }
            }

            return new Response(
                json_encode(array('set' => 1)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new-multiple", name="admin_new_multiple_credentialing",methods={"POST"})
     */
    public function new_multiple(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $ids = $request->get('ids');

            $submitted_to_cvo=$request->get('submitted_to_cvo');

            $cvoObj=$em->getRepository('App\Entity\Cvo')->find(2);

            foreach ($ids as $id) {
                $provider = $em->getRepository('App\Entity\Provider')->find($id);

                //check if the provider have a current record
                $credProcedure=$em->getRepository('App:ProviderCredentialing')->findOneBy(array('provider'=>$id));

                if($credProcedure==null){
                    $credProcedure=new ProviderCredentialing();
                }

                if($cvoObj!=null){
                    $credProcedure->setCvo($cvoObj);
                }

                $credProcedure->setProvider($provider);
                $credProcedure->setCredentialingDate($submitted_to_cvo);
                $statusObj=$em->getRepository('App\Entity\CredentialingStatus')->find(2);
                $credProcedure->setNpiNumber($provider->getNpiNumber());
                $credProcedure->setOrganization($provider->getOrganization());

                if($statusObj!=null){
                    $credProcedure->setCredentialingStatus($statusObj);
                }

                $em->persist($credProcedure);
                $em->flush();
            }

            return new Response(
                json_encode(array('set' => 1)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }

    /**
     * @Route("/update-cred-process", name="admin_update_data")
     */
    public function updateData() {
        $em = $this->getDoctrine()->getManager();
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls";
        $fileName="full_roster_extract.xls";

        $fileToRead = $url."/" . $fileName;

        $reader = new Reader\Xls();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cvo=$em->getRepository('App\Entity\Cvo')->find(2);
        $cont=1;
        foreach ($sheetData as $sheet) {
            if($cont>1){
                $credentialing_status=$sheet['E'];
                if($credentialing_status=="approved"){
                    if ($sheet['A'] != null or $sheet['A'] != "") {
                        $provider_npi= $sheet['T'];
                        $provider=$em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number'=>$provider_npi));
                        $newPro=0;
                        if($provider!=null){
                            $credProcess=$em->getRepository('App\Entity\ProviderCredentialing')->findOneBy(array('provider'=>$provider->getId()));

                            if($credProcess==null){
                                $credProcess=new ProviderCredentialing();
                                $newPro=1;
                            }

                            if($newPro==1){
                                $credProcess->setProvider($provider);
                            }

                            if($credProcess!=null){
                                $dueDate=$sheet['B'];
                                $acceptedDate=$sheet['F'];
                                $completeDate=$sheet['C'];

                                $dateS=explode("-",$dueDate);
                                $dateCredDue=$dateS[1]."/".$dateS[2]."/".$dateS[0];
                                $credProcess->setCredentialingDate($dateCredDue);

                                if($acceptedDate!=""){
                                    $dateAcceptedS=explode('-',$acceptedDate);
                                    $dateCredAccepted=$dateAcceptedS[1]."/".$dateAcceptedS[2]."/".$dateAcceptedS[0];
                                    $credProcess->setCredentialingAccepted($dateCredAccepted);
                                }

                                if($completeDate!=""){
                                    $dateCompleteS=explode('-',$completeDate);
                                    $dateComplete=$dateCompleteS[1]."/".$dateCompleteS[2]."/".$dateCompleteS[0];
                                    $credProcess->setCredentialingCompleteDate($dateComplete);
                                }

                                //get the effective date
                                $yearAccepted=$dateAcceptedS[0];
                                $mothAccepted=$dateAcceptedS[1];
                                $newDateEffectiveDate="";

                                if($mothAccepted=='01'){
                                    $newDateEffectiveDate="02/01/".$yearAccepted;
                                }
                                if($mothAccepted=='02'){
                                    $newDateEffectiveDate="03/01/".$yearAccepted;
                                }
                                if($mothAccepted=='03'){
                                    $newDateEffectiveDate="04/01/".$yearAccepted;
                                }
                                if($mothAccepted=='04'){
                                    $newDateEffectiveDate="05/01/".$yearAccepted;
                                }
                                if($mothAccepted=='05'){
                                    $newDateEffectiveDate="06/01/".$yearAccepted;
                                }
                                if($mothAccepted=='06'){
                                    $newDateEffectiveDate="07/01/".$yearAccepted;
                                }
                                if($mothAccepted=='07'){
                                    $newDateEffectiveDate="08/01/".$yearAccepted;
                                }
                                if($mothAccepted=='08'){
                                    $newDateEffectiveDate="09/01/".$yearAccepted;
                                }
                                if($mothAccepted=='09'){
                                    $newDateEffectiveDate="10/01/".$yearAccepted;
                                }
                                if($mothAccepted=='10'){
                                    $newDateEffectiveDate="11/01/".$yearAccepted;
                                }
                                if($mothAccepted=='11'){
                                    $newDateEffectiveDate="12/01/".$yearAccepted;
                                }
                                if($mothAccepted=='12'){
                                    $newDateEffectiveDate="01/01/".$yearAccepted+1;
                                }

                                $credProcess->setCredentialingEffectiveDate($newDateEffectiveDate);
                                $credProcess->setCvo($cvo);

                                $em->persist($credProcess);
                                $em->flush();
                            }
                        }
                    }
                }

                if($credentialing_status=="complete"){
                    if ($sheet['A'] != null or $sheet['A'] != "") {
                        $provider_npi= $sheet['T'];
                        $provider=$em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number'=>$provider_npi));
                        $newPro=0;
                        if($provider!=null){
                            $credProcess=$em->getRepository('App\Entity\ProviderCredentialing')->findOneBy(array('provider'=>$provider->getId()));

                            if($credProcess==null){
                                $credProcess=new ProviderCredentialing();
                                $newPro=1;
                            }

                            if($newPro==1){
                                $credProcess->setProvider($provider);
                            }

                            $dueDate=$sheet['B'];
                            $completeDate=$sheet['C'];

                            $dateS=explode("-",$dueDate);
                            $dateCredDue=$dateS[1]."/".$dateS[2]."/".$dateS[0];

                            $dateCompleteS=explode('-',$completeDate);
                            $dateComplete=$dateCompleteS[1]."/".$dateCompleteS[2]."/".$dateCompleteS[0];

                            $credProcess->setCredentialingDate($dateCredDue);
                            $credProcess->setCredentialingCompleteDate($dateComplete);
                            $credProcess->setCvo($cvo);

                            $status=$em->getRepository('App\Entity\CredentialingStatus')->find(9);
                            $credProcess->setCredentialingStatus($status);

                            $em->persist($credProcess);
                            $em->flush();
                        }
                    }
                }

                if($credentialing_status=="canceled"){
                    if ($sheet['A'] != null or $sheet['A'] != "") {
                        $provider_npi= $sheet['T'];
                        $provider=$em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number'=>$provider_npi));
                        if($provider!=null){

                            $credProcess=new ProviderCredentialing();
                            $dueDate=$sheet['B'];

                            $dateS=explode("-",$dueDate);
                            $dateCredDue=$dateS[1]."/".$dateS[2]."/".$dateS[0];

                            $credProcess->setCredentialingDate($dateCredDue);
                            $credProcess->setCvo($cvo);
                            $credProcess->setProvider($provider);
                            $status=$em->getRepository('App\Entity\CredentialingStatus')->find(8);
                            $credProcess->setCredentialingStatus($status);

                            $em->persist($credProcess);
                            $em->flush();
                        }
                    }
                }

            }
            $cont++;
        }

     return new Response('Finish');
    }



    private function SaveLog($action_id,$entity,$entity_id,$note){
        $em=$this->getDoctrine()->getManager();
        $user=$this->getUser();

        $log=new ChangeLog();
        $log->setUser($user);
        $action=$em->getRepository('App\Entity\ActionLog')->find($action_id);
        $log->setAction($action);
        $log->setEntityId($entity_id);
        $log->setEntityClass($entity);
        $log->setNote($note);

        $em->persist($log);
        $em->flush();
    }

    /**
     * @Route("/check-sent-credsimple", name="admin_sent_credsimple")
     */
    public function checkSentCredSimple() {
        set_time_limit(3600);
        $em = $this->getDoctrine()->getManager();

        $fileToRead =  "template_xls/credsimple-allfilesreport.xlsx";

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        $cont=0;
        foreach ($sheetData as $sheet) {
            if($cont>0)
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    $npi = $sheet['A'];
                    $providers=$em->getRepository('App\Entity\Provider')->findBy(array('npi_number'=>$npi));

                    if($providers!=null){
                        foreach ($providers as $provider){
                            if($provider!=null){
                                $provider->setSentCredsimple(true);
                                $em->persist($provider);
                                $em->flush();
                            }
                        }
                    }
                }
            $cont++;
        }

        return new Response('All OK');
    }

    /**
     * @Route("/save-sent-credsimple", name="admin_sent_credsimple_save")
     */
    public function saveSentCredSimple() {
        set_time_limit(3600);
        $em = $this->getDoctrine()->getManager();

        $fileToRead =  "template_xls/credsimple-allfilesreport.xlsx";

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        $cont=0;
        foreach ($sheetData as $sheet) {
            if($cont>0)
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    $npi = $sheet['A'];
                    $first_name = $sheet['B'];
                    $last_name = $sheet['C'];
                    $provider_type = $sheet['D'];
                    $credential_event = $sheet['E'];
                    $due_date = $sheet['F'];
                    $completed_date = $sheet['G'];
                    $flagged = $sheet['H'];
                    $status = $sheet['I'];

                    $sentCredSimple=new CredSimpleRecord();
                    $sentCredSimple->setNpi($npi);
                    $sentCredSimple->setFirstName($first_name);
                    $sentCredSimple->setLastName($last_name);
                    $sentCredSimple->setProviderType($provider_type);
                    $sentCredSimple->setCredentialEvent($credential_event);
                    $sentCredSimple->setDueDate($due_date);
                    $sentCredSimple->setCompletedDate($completed_date);
                    $sentCredSimple->setFlagged($flagged);
                    $sentCredSimple->setStatus($status);

                    $em->persist($sentCredSimple);
                    $em->flush();
                }
            $cont++;
        }

        return new Response('All OK');
    }

    /**
     * @Route("/read_pdf", name="admin_read_pdf")
    */
    public function readPDFCredSimpleApplication(){
        $parser = new PdfParser\Parser();
        $em = $this->getDoctrine()->getManager();

        $directory = opendir('pdfs/'); //ruta actual
        while ($file = readdir($directory)) //obtenemos un archivo y luego otro sucesivamente
        {
            if (!is_dir($file)) {
                $nameF=substr($file,0,13);
                if($nameF!="credentialing"){
                    echo $file."<br/>";
                    $fileToRead =  "pdfs/".$file;
                    $pdf    = $parser->parseFile($fileToRead);
                    $text = $pdf->getText();

                    $pos = strpos($text, 'NPI:');
                    $posNPI=$pos+4;

                    $npi=intval(substr($text,$posNPI,11));

                    $provider=$em->getRepository('App\Entity\Provider')->findBy(array('npi_number'=>$npi));
                    if($provider!=null){
                        foreach ($provider as $pro){
                            //credentialing_application_file-180
                            $providerId=$pro->getId();
                            $newName="pdfs/credentialing_application_file-".$providerId.".pdf";
                            $oldName="pdfs/".$file;

                            //echo $pro->getId()."<br/>";
                            echo $newName."<br/>";
                            rename($oldName,$newName);

                            if(!file_exists("uploads/providers/".$providerId)){
                                mkdir("uploads/providers/".$providerId, 0777,true);
                                chmod("uploads/providers/".$providerId, 0777);
                            }

                            $newCopy="uploads/providers/".$providerId."/"."credentialing_application_file-".$providerId.".pdf";
                            if (!copy($newName, $newCopy)) {
                                echo "Error al copiar $newName...\n";
                            }else{
                                $pro->setCredentialingApplicationFile("credentialing_application_file-".$providerId.".pdf");
                                $em->persist($pro);
                                $em->flush();

                                echo "Provider updated<br/>";
                            }
                        }
                    }else{

                    }
                }
            }
        }
        echo "Finish the process";
        die();
    }

    /**
     * @Route("/read_credentialing_files", name="admin_read_credentialing_files")
     */
    public function checkProviderCredentialingFiles(){
        set_time_limit(3600);
        $em = $this->getDoctrine()->getManager();
        $directory = opendir('pdfs/');
        while ($file = readdir($directory)){
            if($file!="." and $file!=".."){
                $sub_folder = opendir('pdfs/'.$file);
                $cont=0;
                while ($doc=readdir($sub_folder)){
                    if (!is_dir($doc)) {
                       $cont++;
                    }
                }
                if($cont>3){

                }else{
                    $npi=$file;
                    $providers=$em->getRepository('App\Entity\Provider')->findBy(array('npi_number'=>$npi));
                    if($providers!=null){
                        foreach ($providers as $provider){
                           if($provider!=null){
                               if(!file_exists("uploads/providers_cred/".$file)){
                                   mkdir("uploads/providers_cred/".$file, 0777,true);
                                   chmod("uploads/providers_cred/".$file, 0777);
                               }

                               $sub_folder = opendir('pdfs/'.$file);
                           }
                        }
                    }

                    if($providers!=null) {
                        foreach ($providers as $provider) {
                            if($provider!=null){
                                while ($doc=readdir($sub_folder)){
                                    if (!is_dir($doc)) {
                                        $origin_name="pdfs/".$file."/".$doc;
                                        $copy="uploads/providers_cred/".$file."/".$doc;
                                        if (copy($origin_name, $copy)) {
                                            $provider_cred_file=new ProviderCredFile();
                                            $provider_cred_file->setProvider($provider);
                                            $provider_cred_file->setFileName($doc);
                                            $em->persist($provider_cred_file);
                                        }else{
                                            echo "Error for: ".$file;
                                        }
                                        echo "<br>";
                                    }
                                }
                                $em->flush();
                            }
                        }
                    }
                }

            }
        }

        die();

    }


    /**
     * @Route("/update_all_credentialing_proccess", name="admin_update_all_credentialing_process")
    */
    public function updateAllCredentialingProcess(){
        $em = $this->getDoctrine()->getManager();

        $credentialingProccess=$em->getRepository('App\Entity\ProviderCredentialing')->findAll();
        $cont=1;
        foreach ($credentialingProccess as $cp){
            //$cp=new ProviderCredentialing();
            if($cp->getCredentialingStatus()->getId()==3){
                $provider=$cp->getProvider();
                if($provider!=null){
                    $providerNPI=$provider->getNpiNumber();
                    $sentCvoRecords=$em->getRepository('App\Entity\CredSimpleRecord')->findBy(array('npi'=>$providerNPI));
                    if($sentCvoRecords!=null){
                        foreach ($sentCvoRecords as $sentRecord){
                            $status=$sentRecord->getStatus();
                            if($status=='Approved'){
                                $newStatus=$em->getRepository('App\Entity\CredentialingStatus')->find(4);
                                $cp->setCredentialingStatus($newStatus);
                                $em->persist($cp);
                            }
                            if($status=='Canceled'){
                                $newStatus=$em->getRepository('App\Entity\CredentialingStatus')->find(8);
                                $cp->setCredentialingStatus($newStatus);
                                $em->persist($cp);
                            }
                            if($status=='Outreach'){
                                $newStatus=$em->getRepository('App\Entity\CredentialingStatus')->find(12);
                                $cp->setCredentialingStatus($newStatus);
                                $em->persist($cp);
                            }
                            if($status=='In Progress'){
                                $newStatus=$em->getRepository('App\Entity\CredentialingStatus')->find(12);
                                $cp->setCredentialingStatus($newStatus);
                                $em->persist($cp);
                            }
                            $em->flush();
                        }

                    }

                }

            }
        }

        die();
    }

    /**
     * @Route("/get_provider_set_mmm_from_facility", name="admin_get_provider_set_mmm_from_facility")
     */
    public function updateAllCredentialingProcessFacility(){
        $em = $this->getDoctrine()->getManager();
        $process=$em->getRepository('App\Entity\BillingAddressCvo')->findAll();

        $isMMM=false;
        $isMedicare=false;
        $haveMedicare=false;

        $providersResult=array();

        $cont=1;
        foreach ($process as $p) {
            if ($p->getCredentialingStatus()->getId() == 7) {
            if ($p->getPayers() != null) {
                foreach ($p->getPayers() as $payer) {
                    if ($payer != null and $payer->getId() == 1) {
                        $isMMM = true;
                    }
                }
            }

            $organization = null;
            if ($p->getBillingAddress() != null) {
                $organization = $p->getBillingAddress()->getOrganization();
            }

            if ($organization != null) {

                $address = $em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $organization->getId()));
                $providers = $em->getRepository('App\Entity\Provider')->findBy(array('organization' => $organization->getId()));

                if ($address != null) {
                    foreach ($address as $addr) {
                        if ($addr != null) {
                            if ($addr->getGroupMedicare() != "" and $addr->getGroupMedicare() != "-" and $addr->getGroupMedicare() != "n/a" and $addr->getGroupMedicare() != "N/A") {
                                $haveMedicare = true;
                            }
                        }
                    }
                }

                if ($providers != null) {
                    foreach ($providers as $provider) {
                        if ($provider != null) {
                            if ($provider->getMedicare() != "" and $provider->getMedicare() != "-" and $provider->getMedicare() != "n/a" and $provider->getMedicare() != "N/A") {
                                $haveMedicare = true;
                            }
                        }
                    }
                }


            }

            if ($isMMM == true and $haveMedicare == true) {
                $providers = $em->getRepository('App\Entity\Provider')->findBy(array('organization' => $organization->getId()));

                foreach ($providers as $provider) {

                    $credentialingProccess=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('provider'=>$provider->getId()));
                    $address=$provider->getBillingAddress();
                    $isOnRegion=false;
                    $isOnMMM=false;
                    if($address!=null){
                        foreach ($address as $addr){
                            if($addr->getRegion()==9 or $addr->getRegion()==10 or $addr->getRegion()==11){
                                $isOnRegion=true;
                            }
                        }
                    }

                    foreach ($credentialingProccess as $cp){
                        if($cp->getCredentialingStatus()->getId()==7){
                            $isOnMMM=true;
                        }
                    }

                    if($isOnRegion==true and $isOnMMM==false){
                        echo $provider->getId()."<br/>";
                        $cont++;
                    }

                }

            }
        }
        }

        die();
    }

    /**
     * @Route("/load-from-cred-simple", name="admin_load_from_credsimple")
     */
    public function laodFromCredSimple(){
        return $this->render('provider_credentialing/load_from_credsimple.html.twig');
    }

    /**
     * @Route("/credentialing-upload-proccess", name="admin_credentialing_upload_proccess")
     */
    function uploadProccess(Request $request){
        set_time_limit(8600);
        $em=$this->getDoctrine()->getManager();
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $file = $request->files->get('credentialing_load');

            $fileName = "";
            $fileExtension="";
            if ($file != "") {
                $nombreoriginal = $file->getClientOriginalName();
                $fileName = $nombreoriginal;
                $adjuntosDir = $this->getparameter('credentialing_loaded');
                $fileExtension=$file->guessExtension();
                $file->move($adjuntosDir, $fileName);
            }

            //step for read the excel and read de information
            $fileToRead = $this->getparameter('credentialing_loaded') . "/" . $fileName;

            if($fileExtension=="xlsx"){
                $reader = new Reader\Xlsx();
                $reader->setReadDataOnly(true);
                $spreadsheet = $reader->load($fileToRead);
                $spreadsheet->setActiveSheetIndex(0);
                $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            }

            if($fileExtension=="xls"){
                $reader = new Reader\Xls();
                $reader->setReadDataOnly(true);
                $spreadsheet = $reader->load($fileToRead);
                $spreadsheet->setActiveSheetIndex(0);
                $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            }

            if($fileExtension=="csv" or $fileExtension=="txt"){
                $reader = new Reader\Csv();
                $spreadsheet = $reader->load($fileToRead);
                $reader->setDelimiter(',');
                $reader->setEnclosure('');
                $reader->setSheetIndex(0);

                $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            }

            $records=$em->getRepository('App\Entity\CredSimpleRecord')->findAll();
            if($records!=null){
                foreach ($records as $record){
                    $em->remove($record);
                }
                $em->flush();
            }

            $cont=1;

            foreach ($sheetData as $sheet) {
                if($cont>1){
                    if ($sheet['A'] != null or $sheet['A'] != "") {
                        $npi=$sheet['T'];
                        $first_name=$sheet['U'];
                        $last_name=$sheet['W'];
                        $provider_type=$sheet['X'];
                        $credential_event=$sheet['D'];

                        try{
                            $due_date=trim($sheet['B']);
                            if($due_date!=""){
                                $due_date=$this->coverDateFromExcel($sheet['B']);
                                $formated_due_date=$this->formatDate($due_date);
                            }

                        }catch (\Exception $exception){
                            $due_date=trim($sheet['B']);
                            if($due_date!=""){
                                $formated_due_date=$this->formatDate($due_date);
                            }
                        }

                        try{
                            $completed_date=trim($sheet['C']);
                            if($completed_date!=""){
                                $completed_date=$this->coverDateFromExcel($sheet['C']);
                                $formated_completed_date=$this->formatDate($completed_date);
                            }
                        }catch (\Exception $exception){
                            $completed_date=trim($sheet['C']);
                            if($completed_date!=""){
                                $formated_completed_date=$this->formatDate($completed_date);
                            }
                        }

                        try{
                            $decision_date=trim($sheet['F']);
                            if($decision_date!=""){
                                $decision_date=$this->coverDateFromExcel($sheet['F']);
                                $formated_decision_date=$this->formatDate($decision_date);
                            }
                        }catch (\Exception $exception){
                            $decision_date=trim($sheet['F']);
                            if($decision_date!=""){
                                $formated_decision_date=$this->formatDate($decision_date);
                            }
                        }

                        try{
                            $license_expires_on=trim($sheet['AT']);
                            if($license_expires_on!=""){
                                $license_expires_on=$this->coverDateFromExcel($sheet['AT']);
                                $formated_license_expires_on=$this->formatDate($license_expires_on);
                            }
                        }catch (\Exception $exception){
                            $license_expires_on=trim($sheet['AT']);
                            if($license_expires_on!=""){
                                $formated_license_expires_on=$this->formatDate($license_expires_on);
                            }
                        }

                        $flagged=$sheet['H'];
                        $status=$sheet['E'];

                        $report_pdf=$sheet['I'];
                        $license=$sheet['AY'];

                        $license_status=$sheet['AZ'];

                        if($credential_event=="initial" or $credential_event=="Initial"){
                            $objRecord=new CredSimpleRecord();
                            $objRecord->setNpi($npi);
                            $objRecord->setFirstName($first_name);
                            $objRecord->setLastName($last_name);
                            $objRecord->setProviderType($provider_type);
                            $objRecord->setCredentialEvent($credential_event);
                            $objRecord->setDueDate($formated_due_date);
                            $objRecord->setCompletedDate($formated_completed_date);
                            $objRecord->setFlagged($flagged);
                            $objRecord->setStatus($status);
                            $objRecord->setDecisionDate($formated_decision_date);
                            $objRecord->setReportPdf($report_pdf);
                            $objRecord->setLicense($license);
                            $objRecord->setLicenseExpiresOn($formated_license_expires_on);
                            $objRecord->setLicenseStatus($license_status);

                            $em->persist($objRecord);
                        }
                    }
                }
                $cont++;
            }
            $em->flush();

            //update the Individuals Credentialing Proccess List
           $sentCredPros=$em->getRepository('App\Entity\CredSimpleRecord')->findAll();

            foreach ($sentCredPros as $sp){
                $npi=$sp->getNpi();
                $status=$sp->getStatus();
                $report_pdf=$sp->getReportPdf();
                $decision_date=$sp->getDecisionDate();

                $providers=$em->getRepository('App\Entity\Provider')->findBy(array('npi_number'=>$npi));

                $cvo=$em->getRepository('App\Entity\Cvo')->find(2);
                $credStatusObj = $em->getRepository('App\Entity\CredentialingStatus')->findOneBy(array('name'=>$status));
                $newDateEffectiveDate="";

                if($decision_date!=""){
                    if(stristr($decision_date, '/')) {
                        $decision_date_array=explode('/',$decision_date);
                        $year=$decision_date_array[2];
                        $moth=$decision_date_array[0];
                    }

                    if(stristr($decision_date, '-')) {
                        $decision_date_array=explode('-',$decision_date);
                        $year=$decision_date_array[0];
                        $moth=$decision_date_array[1];
                    }

                    if($moth=='01' or $moth=='1'){
                        $newDateEffectiveDate="02/01/".$year;
                    }
                    if($moth=='02' or $moth=='2'){
                        $newDateEffectiveDate="03/01/".$year;
                    }
                    if($moth=='03' or $moth=='3'){
                        $newDateEffectiveDate="04/01/".$year;
                    }
                    if($moth=='04' or $moth=='4'){
                        $newDateEffectiveDate="05/01/".$year;
                    }
                    if($moth=='05' or $moth=='5'){
                        $newDateEffectiveDate="06/01/".$year;
                    }
                    if($moth=='06' or $moth=='6'){
                        $newDateEffectiveDate="07/01/".$year;
                    }
                    if($moth=='07'  or $moth=='7'){
                        $newDateEffectiveDate="08/01/".$year;
                    }
                    if($moth=='08'  or $moth=='8'){
                        $newDateEffectiveDate="09/01/".$year;
                    }
                    if($moth=='09' or $moth=='9'){
                        $newDateEffectiveDate="10/01/".$year;
                    }
                    if($moth=='10' or $moth=='10'){
                        $newDateEffectiveDate="11/01/".$year;
                    }
                    if($moth=='11'  or $moth=='11'){
                        $newDateEffectiveDate="12/01/".$year;
                    }
                    if($moth=='12' or $moth=='12'){
                        $newDateEffectiveDate="01/01/2021";
                    }
                }

                if($decision_date!=""){
                    if(stristr($decision_date, '/')) {
                        $decision_date_array=explode('/',$decision_date);
                        $d=$decision_date_array[1];
                        $m=$decision_date_array[0];
                        $y=$decision_date_array[2];
                        $decision_date=$m."/".$d."/".$y;
                    }

                    if(stristr($decision_date, '-')) {
                        $decision_date_array=explode('-',$decision_date);
                        $d=$decision_date_array[2];
                        $m=$decision_date_array[1];
                        $y=$decision_date_array[0];
                        $decision_date=$m."/".$d."/".$y;
                    }
                }

                if($providers!=null){
                    foreach ($providers as $provider){
                        $credentialing=$em->getRepository('App\Entity\ProviderCredentialing')->findOneBy(array('npi_number'=>$npi));
                        if($credentialing==null){
                            $credentialing=new ProviderCredentialing();
                            $credentialing->setProvider($provider);
                            $credentialing->setCvo($cvo);
                            $credentialing->setCredentialingStatus($credStatusObj);
                            $credentialing->setNpiNumber($npi);
                            $credentialing->setCredentialingEffectiveDate($newDateEffectiveDate);
                            $credentialing->setReportPdf($report_pdf);
                            $credentialing->setOrganization($provider->getOrganization());

                            if($status=='approved'){
                                $credentialing->setCredentialingAccepted($decision_date);
                            }

                            if($status=='denied'){
                                $credentialing->setCredentialingDenied($decision_date);
                            }

                            $credentialing->setReportPdf($report_pdf);
                        }else{
                            $credentialing->setCredentialingStatus($credStatusObj);
                            if($status=='approved'){
                                $credentialing->setCredentialingAccepted($decision_date);
                            }

                            if($status=='denied'){
                                $credentialing->setCredentialingDenied($decision_date);
                            }
                            $credentialing->setReportPdf($report_pdf);
                            $credentialing->setCredentialingEffectiveDate($newDateEffectiveDate);
                            $credentialing->setOrganization($provider->getOrganization());
                        }

                        $em->persist($credentialing);
                    }
                }
            }
            $em->flush();
            return $this->redirectToRoute('admin_credentialing_email_queue');

        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    function formatDate($date){
        $format_date="";
        if($date!=""){
            if(stristr($date, '/')){
                $date_array=explode("/",$date);
            }else{
                $date_array=explode("-",$date);
            }

            $moth=$date_array[0];
            $day=$date_array[1];
            $year=$date_array[2];

            if(strlen($moth)==1){
                $moth="0".$moth;
            }
            if(strlen($day)==1){
                $day="0".$day;
            }

            if(stristr($date, '/')){
                $format_date=$moth."/".$day."/".$year;
            }
            if(stristr($date, '-')){
                $format_date=$moth."-".$day."-".$year;
            }
        }

        return $format_date;
    }

    /**
     * @Route("/credentialing-verify-icpl", name="admin_credentialing_verify_icpl")
     */
    function verifyICPL(){
        $em=$this->getDoctrine()->getManager();
        $credentialings=$em->getRepository('App\Entity\ProviderCredentialing')->findAll();
        $credSimpleRecords=$em->getRepository('App\Entity\CredSimpleRecord')->findAll();

        $credSimpleRecords_NPI=[];
        $credentialings_NPI=[];

        foreach ($credSimpleRecords as $record){
            $credSimpleRecords_NPI[]=$record->getNpi();
        }

        foreach ($credentialings as $credentialing){
            $credentialings_NPI[]=$credentialing->getNpiNumber();
        }


        foreach ($credSimpleRecords_NPI as $credSimpleRecords){
            if(!in_array($credSimpleRecords,$credentialings_NPI)){
                echo $credSimpleRecords."<br/>";
            }
        }
/*
        foreach ($credSimpleRecords_NPI as $credSimpleRecords){
            if(!in_array($credSimpleRecords,$credentialings_NPI)){

                $credSR=$em->getRepository('App\Entity\CredSimpleRecord')->findOneBy(array('npi'=>$credSimpleRecords));
                $provider=$em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number'=>$credSimpleRecords));

                $status=$credSR->getStatus();
                $report_pdf=$credSR->getReportPdf();
                $decision_date=$credSR->getDecisionDate();
                $credStatusObj = $em->getRepository('App\Entity\CredentialingStatus')->findOneBy(array('name'=>$status));

                $newDateEffectiveDate="";
                if($decision_date!=""){

                    if(stristr($decision_date, '/')) {
                        $decision_date_array=explode('/',$decision_date);
                        $year=$decision_date_array[2];
                        $moth=$decision_date_array[0];
                    }

                    if(stristr($decision_date, '-')) {
                        $decision_date_array=explode('-',$decision_date);
                        $year=$decision_date_array[0];
                        $moth=$decision_date_array[1];
                    }

                    if(stristr($decision_date, '/')) {
                        $decision_date_array=explode('/',$decision_date);
                        $d=$decision_date_array[1];
                        $m=$decision_date_array[0];
                        $y=$decision_date_array[2];
                        $decision_date=$m."/".$d."/".$y;
                    }

                    if(stristr($decision_date, '-')) {
                        $decision_date_array=explode('-',$decision_date);
                        $d=$decision_date_array[2];
                        $m=$decision_date_array[1];
                        $y=$decision_date_array[0];
                        $decision_date=$m."/".$d."/".$y;
                    }

                    if($moth=='01' or $moth=='1'){
                        $newDateEffectiveDate="02/01/".$year;
                    }
                    if($moth=='02' or $moth=='2'){
                        $newDateEffectiveDate="03/01/".$year;
                    }
                    if($moth=='03' or $moth=='3'){
                        $newDateEffectiveDate="04/01/".$year;
                    }
                    if($moth=='04' or $moth=='4'){
                        $newDateEffectiveDate="05/01/".$year;
                    }
                    if($moth=='05' or $moth=='5'){
                        $newDateEffectiveDate="06/01/".$year;
                    }
                    if($moth=='06' or $moth=='6'){
                        $newDateEffectiveDate="07/01/".$year;
                    }
                    if($moth=='07'  or $moth=='7'){
                        $newDateEffectiveDate="08/01/".$year;
                    }
                    if($moth=='08'  or $moth=='8'){
                        $newDateEffectiveDate="09/01/".$year;
                    }
                    if($moth=='09' or $moth=='9'){
                        $newDateEffectiveDate="10/01/".$year;
                    }
                    if($moth=='10' or $moth=='10'){
                        $newDateEffectiveDate="11/01/".$year;
                    }
                    if($moth=='11'  or $moth=='11'){
                        $newDateEffectiveDate="12/01/".$year;
                    }
                    if($moth=='12' or $moth=='12'){
                        $newDateEffectiveDate="01/01/2021";
                    }
                }

                $newCred=new ProviderCredentialing();
                $newCred->setNpiNumber($credSimpleRecords);
                $newCred->setProvider($provider);
                $newCred->setReportPdf($report_pdf);
                $newCred->setCredentialingStatus($credStatusObj);

                if($status=='approved'){
                    $newCred->setCredentialingAccepted($decision_date);
                }

                if($status=='denied'){
                    $newCred->setCredentialingDenied($decision_date);
                }

                $newCred->setCredentialingEffectiveDate($newDateEffectiveDate);

                $em->persist($newCred);
                $em->flush();
            }
        }*/


        /*
        foreach ($credentialings_NPI as $cred_npi ){
            if(!in_array($cred_npi,$credSimpleRecords_NPI)){

                if($cred_npi!=""){
                    $credSimpleRecord=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('npi_number'=>$cred_npi));

                    if($credSimpleRecord!=null){
                        foreach ($credSimpleRecord as $csr){
                          $em->remove($csr);
                        }
                    }
                }
            }
        }

        $em->flush();*/

        die();
    }

    /**
     * @Route("/credentialing-create-email-queue", name="admin_credentialing_email_queue")
     */
    function createEmailQueueOrg(){
        $em=$this->getDoctrine()->getManager();
        $organizations=$em->getRepository('App\Entity\Organization')->findBy(array('disabled'=>0));

        $credentialings=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(
            array('credentialing_status'=>'4'));
        $dates=[];

        foreach($credentialings as $credentialing){
            $approvedDate=$credentialing->getCredentialingAccepted();
            if($approvedDate!=null and $approvedDate!=""){
                if(!in_array($approvedDate,$dates)){
                    $dates[]=$approvedDate;
                }
            }
        }

        return $this->render('provider_credentialing/addEmailqueue.html.twig', array('organizations' => $organizations,'dates'=>$dates));
    }

    /**
     * @Route("/credentialing-create-email-queue-ajax", name="admin_credentialing_email_queue_ajax", methods={"POST"}))
     */
    function createEmailQueueAjax(Request $request){
        $em=$this->getDoctrine()->getManager();

        $id=$request->get('organization');
        $organization=$em->getRepository('App\Entity\Organization')->find($id);
        $emailType=$em->getRepository('App\Entity\EmailType')->find(4);
        $date=$request->get('date');
        $total_contacts=0;

        $credentialings=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(
            array('organization'=>$id,'credentialing_status'=>'4','credentialing_accepted'=>$date));
        $providersIds="";

        if($credentialings!=null) {
            foreach ($credentialings as $credentialing) {
                $providers=$em->getRepository('App\Entity\Provider')->findBy(array('npi_number'=>$credentialing->getNpiNumber(),'organization'=>$id));
                if($providers!=null){
                    foreach ($providers as $provider){
                        $npi=$provider->getNpiNumber();
                        if($npi!="" and $npi!="NOT AVAILABLE"){
                            $providersIds.=$provider->getId().",";
                        }
                    }
                }
            }
        }

        $providersIds=substr($providersIds,0,-1);

        if($credentialings!=null){
            foreach ($credentialings as $credentialing){
                $approvedDate=$credentialing->getCredentialingAccepted();

                $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$id));
                $datetosend=date('m/d/Y');
                $status=$em->getRepository('App\Entity\EmailStatus')->find(1);

                if($contacts!=null){
                    foreach ($contacts as $contact){
                        $email=$contact->getEmail();
                        if($email!=""){
                            $email=$contact->getEmail();
                            $emailqueed_exits=$em->getRepository('App\Entity\EmailQueued')->findBy(array('sent_to'=>$email,'email_type'=>4));

                            if($emailqueed_exits==null or count($emailqueed_exits)==0){
                                $emailQueue=new EmailQueued();
                                $emailQueue->setOrganization($organization);
                                $emailQueue->setProviders($providersIds);
                                $emailQueue->setContact($contact);
                                $emailQueue->setEmailType($emailType);
                                $emailQueue->setSentTo($email);
                                $emailQueue->setDateToSend(new \DateTime($datetosend));
                                $emailQueue->setStatus($status);
                                $emailQueue->setApprovedDate($approvedDate);

                                $em->persist($emailQueue);
                                $em->flush();

                                $total_contacts++;
                            }
                        }
                    }
                }
            }
        }

        return new Response(
            json_encode(array('total'=>$total_contacts,'providers'=>$providersIds)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/add-payer-providers-facilities", name="admin_add_payer_providers_facility"))
     */
    function addPayerFacility(){
        $em=$this->getDoctrine()->getManager();

        $conn = $em->getConnection();
        $sql="SELECT o.id
            FROM  organization o 
            LEFT JOIN organization_payer ON organization_payer.organization_id = o.id
            LEFT JOIN payer ON payer.id = organization_payer.payer_id
            WHERE o.status_id=2
            GROUP BY o.id
            ORDER BY o.name";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $organizations= $stmt->fetchAll();

        foreach ($organizations as $organization){
            $id=$organization['id'];
            $organization=$em->getRepository('App\Entity\Organization')->find($id);
            $payers=$organization->getPayers();

            if($payers!=null){
                foreach ($payers as $payer){
                    if($payer->getId()==1){
                        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$id));
                        if($providers!=null){
                            foreach ($providers as $provider){
                               $haveMMM=false;
                               $currentPayers=$provider->getPayers();

                               if($currentPayers!=null){
                                   foreach ($currentPayers as $payerP){
                                       if($payerP->getId()==1){
                                           $haveMMM=true;
                                       }
                                   }
                               }

                               if($haveMMM==false){
                                   echo $provider->getId()."<br/>";
                                   $payerNew=$em->getRepository('App\Entity\Payer')->find(1);
                                   $provider->addPayer($payerNew);
                                   $em->persist($provider);
                                   $em->flush();

                                   echo $provider->getId()."<br/>";
                               }

                            }
                        }
                    }
                }
            }

        }

        die();
    }

    private function coverDateFromExcel($date){
        if($date==""){
            return "";
        }else{
            $unix_date = ($date - 25569) * 86400;
            $date = 25569 + ($unix_date / 86400);
            $unix_date = ($date - 25569) * 86400;

            return gmdate("m/d/Y", $unix_date);
        }
    }


}
