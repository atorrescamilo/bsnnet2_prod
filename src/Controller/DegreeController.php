<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Degree;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/degree")
 */
class DegreeController extends AbstractController{

    /**
     * @Route("/index", name="admin_degree")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $degrees = $em->getRepository('App\Entity\Degree')->findBy(array(),array('name'=>'ASC'));
            $delete_form_ajax = $this->createCustomForm('DEGREE_ID', 'DELETE', 'admin_delete_degree');


            return $this->render('degree/index.html.twig', array('degrees' => $degrees, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_degree")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('degree/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_degree", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Degree')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_degree');
            }

            return $this->render('degree/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_degree", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Degree')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_degree');
            }

            return $this->render('degree/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_degree")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $code = $request->get('code');
            $description = $request->get('description');
            $valid_cred=$request->get('valid_cred');
            $new = $request->get('new');

            $degree = new Degree();
            $degree->setName($name);
            $degree->setCode($code);
            $degree->setValidCred($valid_cred);
            $degree->setDescription($description);

            $em->persist($degree);
            $em->flush();

            $this->addFlash(
                'success',
                'Degree has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_degree');
            }

            return $this->redirectToRoute('admin_degree');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_degree")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $code = $request->get('code');
            $valid_cred=$request->get('valid_cred');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $degree = $em->getRepository('App\Entity\Degree')->find($id);

            if ($degree == null) {
                $this->addFlash(
                    "danger",
                    "The Degree can't been updated!"
                );

                return $this->redirectToRoute('admin_degree');
            }

            if ($degree != null) {
                $degree->setName($name);
                $degree->setCode($code);
                $degree->setDescription($description);
                $degree->setValidCred($valid_cred);

                $em->persist($degree);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "Degree has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_degree');
            }

            return $this->redirectToRoute('admin_degree');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_degree",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $degree = $degree = $em->getRepository('App\Entity\Degree')->find($id);
            $removed = 0;
            $message = "";

            if ($degree) {
                try {
                    $em->remove($degree);
                    $em->flush();
                    $removed = 1;
                    $message = "The Degree has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The degree can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_degree",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $degree = $degree = $em->getRepository('App\Entity\Degree')->find($id);

                if ($degree) {
                    try {
                        $em->remove($degree);
                        $em->flush();
                        $removed = 1;
                        $message = "The Degree has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Degree can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
