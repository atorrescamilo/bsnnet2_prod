<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\ContactTitle;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/contacttitle")
 */
class ContactTitleController extends AbstractController{

    /**
     * @Route("/index", name="admin_contacttitle")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $contacttitles = $em->getRepository('App\Entity\ContactTitle')->findAll();

            $delete_form_ajax = $this->createCustomForm('CONTACTTITLE_ID', 'DELETE', 'admin_delete_contacttitle');


            return $this->render('contacttitle/index.html.twig', array('contacttitles' => $contacttitles, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_contacttitle")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('contacttitle/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_contacttitle",defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ContactTitle')->find($id);

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('admin_contacttitle');
            }

            return $this->render('contacttitle/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_contacttitle",defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ContactTitle')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_contacttitle');
            }

            return $this->render('contacttitle/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_contacttitle")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $code = $request->get('code');
            $description = $request->get('description');
            $new = $request->get('new');

            try {
                $contacttitle = new ContactTitle();
                $contacttitle->setName($name);
                $contacttitle->setCode($code);
                $contacttitle->setDescription($description);
                $em->persist($contacttitle);
                $em->flush();
                $this->addFlash(
                    'success',
                    'Contact Title has been saved successfully!'
                );

            } catch (PDOException $ex) {
                $this->addFlash(
                    'danger',
                    "ContactTitle can't be added"
                );
                return $this->redirectToRoute('admin_new_contacttitle');
            }

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_contacttitle');
            }

            return $this->redirectToRoute('admin_contacttitle');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }



    /**
     * @Route("/update", name="admin_update_contacttitle")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $code = $request->get('code');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $contacttitle = $em->getRepository('App\Entity\ContactTitle')->find($id);

            if ($contacttitle == null) {
                $this->addFlash(
                    "danger",
                    "The Contact Title can't been updated!"
                );

                return $this->redirectToRoute('admin_contacttitle');
            }

            if ($contacttitle != null) {
                $contacttitle->setName($name);
                $contacttitle->setCode($code);
                $contacttitle->setDescription($description);

                $em->persist($contacttitle);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "The Contact Title has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_contacttitle');
            }

            return $this->redirectToRoute('admin_contacttitle');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_contacttitle",methods={"POST","DELETE"})
     */
    public function delete(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $contacttitle = $contacttitle = $em->getRepository('App\Entity\ContactTitle')->find($id);
            $removed = 0;
            $message = "";

            if ($contacttitle) {
                try {
                    $em->remove($contacttitle);
                    $em->flush();
                    $removed = 1;
                    $message = "The Contact Title has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Contact Title can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/isUnique", name="admin_isunique_contacttitle",methods={"POST"})
     */
    public function isUnique(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $code = $request->get('code');

            $contacttitle = $em->getRepository('App\Entity\ContactTitle')->findOneBy(array('code' => $code));

            $isUnique = 0;
            if ($contacttitle != null) {
                $isUnique = 1;
            }

            return new Response(
                json_encode(array('isUnique' => $isUnique)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_contacttitle",methods={"POST","DELETE"})
     */
    public function deleteMultiple(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $contacttitle = $contacttitle = $em->getRepository('App\Entity\ContactTitle')->find($id);

                if ($contacttitle) {
                    try {
                        $em->remove($contacttitle);
                        $em->flush();
                        $removed = 1;
                        $message = "The Contact Title has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Contact Title can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}

