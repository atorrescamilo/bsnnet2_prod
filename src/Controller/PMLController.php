<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\ErrorPMLCheck;
use App\Entity\Organization;
use App\Entity\OrganizationContact;
use App\Entity\PML;
use App\Entity\Provider;
use App\Repository\PMLRepository;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use PhpOffice\PhpSpreadsheet\Shared\JAMA\QRDecomposition;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Utils\My_Mcript;
use PhpOffice\PhpSpreadsheet\Reader;


/**
 * @Route("/admin/pml")
*/
class PMLController extends AbstractController
{
    /**
     * @Route("/aggrid", name="pml_aggrid")
     */
    public function aggrid(PMLRepository $repository){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $pmls = $repository->findAll();

            return $this->render('pml/aggrid.html.twig', ['pmls' => $pmls]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/check", name="pml_check", methods={"GET"})
    */
    public function index(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();

        //get all PML
        $pmls=$em->getRepository('App\Entity\PML')->findAll();

        $cont=1;
        $idsPMLErrors="";
        $provider2Ids="";
        $t=1;
        foreach ($pmls as $pml){
            //compare with
            if($pml->getColM()==1 and $pml->getColW()=="A" ){
                 $npi=$pml->getColN();
                 $medicaidID=$pml->getColA();
                 $provider=$em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number'=>$npi));

                 if($provider!=null){
                     $address=$provider->getBillingAddress();
                     if($address!=null){
                         foreach ($address as $addr){
                             //$addr=new BillingAddress();
                             $street=$addr->getStreet();
                             $addrArray=explode(" ",$street);
                             $numberStreet=$addrArray[0];

                             $addr_pml=$pml->getColG();
                             $addr_pml_Array=explode(" ",$addr_pml);
                             $street_pml=$addr_pml_Array[0];

                             $applytoChange="No";
                             if($numberStreet==$street_pml){
                                 $applytoChange="Yes";
                             }
                              if($applytoChange=="Yes"){
                                  echo $t.": ".$provider->getId()."-".$npi."-".$numberStreet." | ".$street_pml." -> ".$applytoChange." ".$pml->getColA()."<br/>";
                                  $t++;
                              }

                         }
                     }
                 }

                 //$provider=new Provider();
                 if($provider==null){
                     $provider2=$em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number'=>$npi));
                     if($provider2!=null){
                         if($medicaidID!=$provider2->getMedicaid()){
                             $idError=$pml->getId();
                             $idsPMLErrors.=$idError.",";
                             $provider2Ids.=$provider2->getMEdicaid()."<br/>";
                         }
                     }
                 }
                 /*
                 if($provider!=null){
                     $providerTypeCode=$pml->getColD();
                     $providerPrimarySpecialtyCode=$pml->getColE();

                     $taxonomyCode=$pml->getColF();
                     $licenceState=$pml->getColV();

                     $taxonomyCodeObj=$em->getRepository('App\Entity\TaxonomyCode')->findOneBy(array('code'=>$taxonomyCode));
                     if($taxonomyCodeObj!=null){
                         $provider->addTaxonomyCode($taxonomyCodeObj);
                     }
                     $provider->setTaxonomyCode($taxonomyCode);
                     
                     $provider->setStateLic($licenceState);

                     //get providers Type
                     $providersType=$provider->getProviderType();

                     $needAddProviderType=0;
                     if($providersType!=null){
                        foreach ($providersType as $providerType){
                            if($providerType->getCode()==$providerTypeCode){
                                $needAddProviderType=1;
                            }
                        }
                     }

                     //get providers Type
                     $primarySpecialties=$provider->getPrimarySpecialties();
                     $needPrimarySpecialty=0;
                     if($primarySpecialties!=null){
                         foreach ($primarySpecialties as $primarySpecialty){
                             if($primarySpecialty->getCode()==$providerPrimarySpecialtyCode){
                                 $needPrimarySpecialty=1;
                             }
                         }
                     }

                     //add new provider Type
                     if($needAddProviderType==0){
                         $proType=$em->getRepository('App\Entity\ProviderType')->findOneBy(array('code'=>$providerTypeCode));
                         if($proType!=null){
                             $provider->addProviderType($proType);
                         }
                     }

                     //add new Priamry Specialty
                     if($needPrimarySpecialty==0){
                         $specialty=$em->getRepository('App\Entity\Specialty')->findOneBy(array('code'=>$providerPrimarySpecialtyCode));
                         if($specialty!=null){
                             $provider->addPrimarySpecialty($specialty);
                         }
                     }
                     $em->persist($provider);
                 } */

                 $cont++;
            }

        }
        $em->flush();
        //echo $idsPMLErrors."<br>";
        echo $provider2Ids;

        return new Response('Process Finished');
    }

    /**
     * @Route("/check2", name="pml_check_pro2")
     */
    public function check_prov2(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "template_xls/pml-addr-1.xlsx";

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=0;

        foreach ($sheetData as $sheet) {
            if ($cont >= 0) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                   $npi=$sheet['E'];
                   $provider=$em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number'=>$npi));
                   if($provider!=null){
                      // echo $provider->getId()."-".$provider->getNpiNumber()." | ".$provider->getMedicaid()."<br/>";
                       $medicaidNumber=$sheet['A'];
                       $providerTypeCode=$sheet['B'];
                       $providerPrimarySpecialtyCode=$sheet['C'];
                       $taxonomyCode=$sheet['D'];

                       $provider->setMedicaid($medicaidNumber);
                       $provider->setTaxonomyCode($taxonomyCode);

                       if($providerTypeCode=='7'){
                           $providerTypeCode="07";
                       }
                       if($providerTypeCode=='5'){
                           $providerTypeCode="05";
                       }

                       if($providerPrimarySpecialtyCode=='42'){
                           $providerPrimarySpecialtyCode='042';
                       }
                       if($providerPrimarySpecialtyCode=='43'){
                           $providerPrimarySpecialtyCode='043';
                       }
                       if($providerPrimarySpecialtyCode=='66'){
                           $providerPrimarySpecialtyCode='066';
                       }
                       if($providerPrimarySpecialtyCode=='76'){
                           $providerPrimarySpecialtyCode='076';
                       }

                       $providersType=$provider->getProviderType();

                       $needAddProviderType=0;
                       if($providersType!=null){
                           foreach ($providersType as $providerType){
                               if($providerType->getCode()==$providerTypeCode){
                                   $needAddProviderType=1;
                               }
                           }
                       }

                       //get providers Type
                       $primarySpecialties=$provider->getPrimarySpecialties();
                       $needPrimarySpecialty=0;
                       if($primarySpecialties!=null){
                           foreach ($primarySpecialties as $primarySpecialty){
                               if($primarySpecialty->getCode()==$providerPrimarySpecialtyCode){
                                   $needPrimarySpecialty=1;
                               }
                           }
                       }


                       //add new provider Type
                       if($needAddProviderType==0){
                           $proType=$em->getRepository('App\Entity\ProviderType')->findOneBy(array('code'=>$providerTypeCode));
                           if($proType!=null){
                               $provider->addProviderType($proType);
                           }
                       }

                       //add new Priamry Specialty
                       if($needPrimarySpecialty==0){
                           $specialty=$em->getRepository('App\Entity\Specialty')->findOneBy(array('code'=>$providerPrimarySpecialtyCode));
                           if($specialty!=null){
                               $provider->addPrimarySpecialty($specialty);
                           }
                       }


                       $em->persist($provider);
                       $em->flush();

                       echo $provider->getId()."->".$needAddProviderType. " - ".$needPrimarySpecialty."<br/>";

                   }
                }
            }
            $cont++;
        }

        die();

    }


    /**
     * @Route("/check_org", name="pml_check_org")
     */
    public function check_org(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();

        //get all PML
        $pmls=$em->getRepository('App\Entity\PML')->findAll();

        $cont=1;
        $idsPMLErrors="";
        foreach ($pmls as $pml) {
            //compare with
            if ($pml->getColM() == 2 and $pml->getColW() == "A") {

                 $groupNPI=$pml->getColN();
                 $taxonomyCode=$pml->getColF();

                 $organization =$em->getRepository('App\Entity\Organization')->findOneBy(array('group_npi'=>$groupNPI));
                 $organization=new Organization();
                 if($organization!=null){
                     echo $organization->getName()." - ".$organization->getGroupNpi()."<br/>";

                     //$organization->setTaxonomyCode($taxonomyCode);

                     $em->persist($organization);
                     $em->flush();
                 }
            }
        }

        return new Response('Finish process Org');
    }

    /**
     * @Route("/insert_pml", name="insert_pml", methods={"GET"})
    */
    public function insertpml(){
        set_time_limit(7200);
        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "template_xls/prw19000.xlsx";

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=0;

        foreach ($sheetData as $sheet) {
            if($cont>0 and $cont<1000){
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    $npi=$sheet['N'];
                    if($npi!=""){
                        $pml=new PML();
                        $pml->setColA($sheet['A']);
                        $pml->setColB($sheet['B']);
                        $pml->setColC($sheet['C']);
                        $pml->setColD($sheet['D']);
                        $pml->setColE($sheet['E']);
                        $pml->setColF($sheet['F']);
                        $pml->setColG($sheet['G']);
                        $pml->setColH($sheet['H']);
                        $pml->setColI($sheet['I']);
                        $pml->setColJ($sheet['J']);
                        $pml->setColK($sheet['K']);
                        $pml->setColL($sheet['L']);
                        $pml->setColM($sheet['M']);
                        $pml->setColN($sheet['N']);
                        $pml->setColO($sheet['O']);
                        $pml->setColP($sheet['P']);
                        $pml->setColQ($sheet['Q']);
                        $pml->setColR($sheet['R']);
                        $pml->setColS($sheet['S']);
                        $pml->setColT($sheet['T']);
                        $pml->setColU($sheet['U']);
                        $pml->setColV($sheet['V']);
                        $pml->setColW($sheet['W']);
                        $pml->setColX($sheet['X']);
                        $pml->setColY($sheet['Y']);

                        $em->persist($pml);
                    }
                }
            }
            $cont++;
            $em->flush();
        }
    }

    /**
     * @Route("/check_addrs", name="pml_check_addrs")
     */
    public function check_addrs(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();

        $fileToRead =  "template_xls/pml-sheet3.xlsx";

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        $cont=1;
        foreach ($sheetData as $sheet) {
          $npi=$sheet['A'];
          $provider=$em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number'=>$npi));

          if($provider!=null){
              $address=$provider->getBillingAddress();

              if(count($address)>0){
                  $addrs="";
                  foreach ($address as $addr){
                      /*
                      if($addr->getSuiteNumber()!=null or $addr->getSuiteNumber()!=""){
                          $addrs.=$addr->getStreet().' @ '.$addr->getSuiteNumber()." | ";
                      }else{
                          $addrs.=$addr->getStreet()." | ";
                      }*/
                      $addrs.=$addr->getStreet()." | ";
                  }

                  $addrs=substr($addrs,0,-1);
                  $addrs=substr($addrs,0,-1);
                  echo $addrs.'<br>';
              }else{
                  echo "null <br/>";
              }
          }
          $cont++;
        }

        return new Response('Finish process Org');
    }

    /**
     * @Route("/check3", name="pml_check_pro3")
     */
    public function check_prov3(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "template_xls/pml-check-addrs.xlsx";

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=0;

        foreach ($sheetData as $sheet) {
            if ($cont >= 0) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    $npi=$sheet['E'];
                    $provider=$em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number'=>$npi));
                    if($provider!=null){
                        // echo $provider->getId()."-".$provider->getNpiNumber()." | ".$provider->getMedicaid()."<br/>";
                        $medicaidNumber=$sheet['A'];
                        $providerTypeCode=$sheet['B'];
                        $providerPrimarySpecialtyCode=$sheet['C'];
                        $taxonomyCode=$sheet['D'];

                        //$provider->setMedicaid($medicaidNumber);
                        //$provider->setTaxonomyCode($taxonomyCode);

                        if($providerTypeCode=='7'){
                            $providerTypeCode="07";
                        }
                        if($providerTypeCode=='5'){
                            $providerTypeCode="05";
                        }

                        if($providerPrimarySpecialtyCode=='42'){
                            $providerPrimarySpecialtyCode='042';
                        }
                        if($providerPrimarySpecialtyCode=='43'){
                            $providerPrimarySpecialtyCode='043';
                        }
                        if($providerPrimarySpecialtyCode=='66'){
                            $providerPrimarySpecialtyCode='066';
                        }
                        if($providerPrimarySpecialtyCode=='76'){
                            $providerPrimarySpecialtyCode='076';
                        }

                        $providersType=$provider->getProviderType();

                        $needAddProviderType=0;
                        if($providersType!=null){
                            foreach ($providersType as $providerType){
                                if($providerType->getCode()==$providerTypeCode){
                                    $needAddProviderType=1;
                                }
                            }
                        }

                        //get providers Type
                        $primarySpecialties=$provider->getPrimarySpecialties();
                        $needPrimarySpecialty=0;
                        if($primarySpecialties!=null){
                            foreach ($primarySpecialties as $primarySpecialty){
                                if($primarySpecialty->getCode()==$providerPrimarySpecialtyCode){
                                    $needPrimarySpecialty=1;
                                }
                            }
                        }


                        //add new provider Type
                        if($needAddProviderType==0){
                            $proType=$em->getRepository('App\Entity\ProviderType')->findOneBy(array('code'=>$providerTypeCode));
                            if($proType!=null){
                                $provider->addProviderType($proType);
                            }
                        }

                        //add new Priamry Specialty
                        if($needPrimarySpecialty==0){
                            $specialty=$em->getRepository('App\Entity\Specialty')->findOneBy(array('code'=>$providerPrimarySpecialtyCode));
                            if($specialty!=null){
                                $provider->addPrimarySpecialty($specialty);
                            }
                        }


                        $em->persist($provider);
                        $em->flush();

                        echo $provider->getId()."->".$needAddProviderType. " - ".$needPrimarySpecialty."<br/>";

                    }
                }
            }
            $cont++;
        }

        die();

    }


}


