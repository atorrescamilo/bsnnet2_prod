<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\EapOnly;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/eaponly")
 */
class EapOnlyController extends AbstractController{

    /**
     * @Route("/index", name="admin_eaponly")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $eaponlys = $em->getRepository('App\Entity\EapOnly')->findAll();

            $delete_form_ajax = $this->createCustomForm('EAPONLY_ID', 'DELETE', 'admin_delete_eaponly');


            return $this->render('eaponly/index.html.twig', array('eaponlys' => $eaponlys, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_eaponly")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('eaponly/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_eaponly", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\EapOnly')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_eaponly');
            }

            return $this->render('eaponly/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_eaponly", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\EapOnly')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_eaponly');
            }

            return $this->render('eaponly/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_eaponly")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');

            $eaponly = new EapOnly();
            $eaponly->setName($name);
            $eaponly->setDescription($description);

            $em->persist($eaponly);
            $em->flush();

            $this->addFlash(
                'success',
                'EapOnly has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_eaponly');
            }

            return $this->redirectToRoute('admin_eaponly');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_eaponly")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
        $em=$this->getDoctrine()->getManager();

        $name=$request->get('name');
        $description=$request->get('description');
        $new=$request->get('new');
        $id=$request->get('id');

        $eaponly=$em->getRepository('App\Entity\EapOnly')->find($id);

        if($eaponly==null){
            $this->addFlash(
                "danger",
                "The EapOnly can't been updated!"
            );

            return $this->redirectToRoute('admin_eaponly');
        }

        if($eaponly!=null){
            $eaponly->setName($name);
            $eaponly->setDescription($description);

            $em->persist($eaponly);
            $em->flush();
        }

        $this->addFlash(
            "success",
            "EapOnly has been updated successfully!"
        );

        if($new==1){
            return $this->redirectToRoute('admin_new_eaponly');
        }

        return $this->redirectToRoute('admin_eaponly');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_eaponly",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $eaponly = $eaponly = $em->getRepository('App\Entity\EapOnly')->find($id);
            $removed = 0;
            $message = "";

            if ($eaponly) {
                try {
                    $em->remove($eaponly);
                    $em->flush();
                    $removed = 1;
                    $message = "The EapOnly has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The eaponly can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_eaponly",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $eaponly = $eaponly = $em->getRepository('App\Entity\EapOnly')->find($id);

                if ($eaponly) {
                    try {
                        $em->remove($eaponly);
                        $em->flush();
                        $removed = 1;
                        $message = "The EapOnly has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The EapOnly can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
