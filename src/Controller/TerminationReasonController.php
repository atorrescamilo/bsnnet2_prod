<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\TerminationReason;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Dompdf\Dompdf;
use Dompdf\Options;

/**
 * @Route("/admin/terminationreason")
 */
class TerminationReasonController extends AbstractController{
    /**
     * @Route("/index", name="admin_terminationreason")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $terminationreasons = $em->getRepository('App\Entity\TerminationReason')->findAll();
            $delete_form_ajax = $this->createCustomForm('TERMINATIONREASON_ID', 'DELETE', 'admin_delete_terminationreason');


            return $this->render('terminationreason/index.html.twig', array('terminationreasons' => $terminationreasons, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_terminationreason")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('terminationreason/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_terminationreason", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\TerminationReason')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_terminationreason');
            }

            return $this->render('terminationreason/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_terminationreason", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\TerminationReason')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_terminationreason');
            }

            return $this->render('terminationreason/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_terminationreason")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');

            $terminationreason = new TerminationReason();
            $terminationreason->setName($name);
            $terminationreason->setDescription($description);

            $em->persist($terminationreason);
            $em->flush();

            $this->addFlash(
                'success',
                'Termination Reason has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_terminationreason');
            }

            return $this->redirectToRoute('admin_terminationreason');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_terminationreason")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $terminationreason = $em->getRepository('App\Entity\TerminationReason')->find($id);

            if ($terminationreason == null) {
                $this->addFlash(
                    "danger",
                    "The Termination Reason can't been updated!"
                );

                return $this->redirectToRoute('admin_terminationreason');
            }

            if ($terminationreason != null) {
                $terminationreason->setName($name);
                $terminationreason->setDescription($description);

                $em->persist($terminationreason);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "Termination Reason has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_terminationreason');
            }

            return $this->redirectToRoute('admin_terminationreason');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_terminationreason",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $terminationreason = $terminationreason = $em->getRepository('App\Entity\TerminationReason')->find($id);
            $removed = 0;
            $message = "";

            if ($terminationreason) {
                try {
                    $em->remove($terminationreason);
                    $em->flush();
                    $removed = 1;
                    $message = "The Termination Reason has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Termination Reason can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_terminationreason",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $terminationreason = $terminationreason = $em->getRepository('App\Entity\TerminationReason')->find($id);

                if ($terminationreason) {
                    try {
                        $em->remove($terminationreason);
                        $em->flush();
                        $removed = 1;
                        $message = "The Termination Reason has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Termination Reason can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
