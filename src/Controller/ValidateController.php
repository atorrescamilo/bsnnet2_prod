<?php

namespace App\Controller;

use App\Entity\OrganizationContact;
use App\Entity\Provider;
use App\Entity\ProviderToVerify;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/validate")
*/
class ValidateController extends AbstractController{
    /**
     * @Route("/email/{vh}", name="validate",methods={"GET"})
     */
    public function index($vh){
        $em=$this->getDoctrine()->getManager();
        $emailLog=$em->getRepository('App\Entity\EmailLog')->findOneBy(array('verification_hass'=>$vh));

        $organization=null;
        $contact=$emailLog->getContact();

        if($emailLog!=null){
            $organization=$emailLog->getOrganization();
        }

        return $this->render('validate/provider-email.html.twig',['vh'=>$vh,'organization'=>$organization,'contact'=>$contact]);
    }

    /**
     * @Route("/verify-email", name="verify-email-ajax")
     */
    public function verifyEmail(Request $request, UserPasswordEncoderInterface $encoder){
        $em=$this->getDoctrine()->getManager();
        $first_name=$request->get('first_name');
        $last_name=$request->get('last_name');
        $email=$request->get('email');
        $phone=$request->get('phone');
        $vh=$request->get('vh');
        $password=$request->get('password');
        $organization=$request->get('organization');
        $organizationObj=$em->getRepository('App\Entity\Organization')->find($organization);
        $exist=true;

        if($email!=''){
            $contact=$em->getRepository('App\Entity\OrganizationContact')->findOneBy(array('email'=>$email));

            //check if exist one user with this email
            $user=$em->getRepository('App\Entity\User')->findOneBy(array('email'=>$email));

            if($user!=null){
                $organizations=$user->getOrganizations();
                if($organizations!=null){
                    $exist=0;
                    foreach ($organizations as $org){
                        if($org->getId()==$organizationObj->getId()){
                            $exist=1;
                        }
                    }

                    if($exist==0){
                       $user->addOrganization($organizationObj);
                       $em->persist($user);
                       $em->flush();
                    }
                }
            }

            if($contact==null and $user==null){
                $exist=false;

                $user=$em->getRepository('App\Entity\User')->findOneBy(array('username'=>$email));

                if($user==null){
                    $user=new User();
                    $role=$em->getRepository('App\Entity\Role')->find(3);
                    $user->addRole($role);
                }

                $user->setName($first_name);
                $user->setLastName($last_name);
                $user->setUsername($email);
                $user->setEmail($email);
                $user->setEnabled(false);
                $user->setOrganization($organizationObj);
                $user->setAccountNonLocked(true);
                $user->setAccountNonExpired(true);
                $user->setCredentialsNonExpired(true);
                $encode = $encoder->encodePassword($user, $password);
                $user->setPassword($encode);

                $em->persist($user);
                $em->flush();

                //send the notification and save it on database
                $provider=new ProviderToVerify();
                $provider->setFirstName($first_name);
                $provider->setLastName($last_name);
                $provider->setEmail($email);
                $provider->setPhone($phone);
                $provider->setExist(false);
                $provider->setOrganization($organizationObj);
                $provider->setHc($vh);
                $provider->setPassword($password);
                $provider->setUser($user);

                $em->persist($provider);
                $em->flush();
            }else{
                if($user==null){
                    //Create a new User
                    $organization=$contact->getOrganization();
                    $user=$em->getRepository('App\Entity\User')->findOneBy(array('username'=>$email));

                    if($user==null){
                        $user=new User();
                        $role=$em->getRepository('App\Entity\Role')->find(3);
                        $user->addRole($role);
                    }

                    $user->setName($first_name);
                    $user->setLastName($last_name);
                    $user->setUsername($email);
                    $user->setEmail($email);
                    $user->setEnabled(true);
                    $user->setOrganization($organization);
                    $user->setAccountNonLocked(true);
                    $user->setAccountNonExpired(true);
                    $user->setCredentialsNonExpired(true);
                    $encode = $encoder->encodePassword($user, $password);
                    $user->setPassword($encode);

                    $em->persist($user);
                    $em->flush();
                }
            }
        }

        return new Response(
            json_encode(array('exist'=>$exist)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/verify-provider-user-ajax", name="admin_validate_user_ajax")
     */
    public function validatePRoviderAjax(Request $request, \Swift_Mailer $mailer){
        $em=$this->getDoctrine()->getManager();
        $id=$request->get('user');

        $providerV=$em->getRepository('App\Entity\ProviderToVerify')->find($id);
        if($providerV!=null){
            $user=$providerV->getUser();
            if($user!=null){
                $user->setEnabled(true);
                $em->persist($user);
                $em->flush();
            }
            $em->remove($providerV);
            $em->flush();
            $email=$user->getEmail();

            //send email for notification
            $email=$user->getEmail();
            $name=$user->getName()." ".$user->getLastName();
            $message = (new \Swift_Message('BSN Portal Login'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email)
                ->setBody($this->renderView(
                    'email/templates/request_accepted.html.twig',array('name'=>$name)
                )
                    , 'text/html');

            $mailer->send($message);
        }

        return new Response(
            json_encode(array('id'=>$id,'email'=>$email)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/remove-provider-user-ajax", name="admin_remove_user_ajax")
     */
    public function removeProviderAjax(Request $request){

        $em=$this->getDoctrine()->getManager();
        $id=$request->get('user');
        $providerV=$em->getRepository('App\Entity\ProviderToVerify')->find($id);

        if($providerV!=null){
            $user=$providerV->getUser();
            if($user!=null){
                $em->remove($user);
                $em->flush();
            }
            $em->remove($providerV);
            $em->flush();
        }

        return new Response(
            json_encode(array('id'=>$id)), 200, array('Content-Type' => 'application/json')
        );
    }

}
