<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\BillingAddressCvo;
use App\Entity\FCCData;
use App\Entity\HospitalAHCA;
use App\Entity\Languages;
use App\Entity\Organization;
use App\Entity\OrganizationContact;
use App\Entity\PNVLog;
use App\Entity\PNVSLLog;
use App\Entity\AETNAPNVLog;
use App\Entity\AETNAPNVSLLog;
use App\Entity\Provider;
use App\Entity\ProviderAddrFCCTN;
use App\Entity\ProviderType;
use App\Entity\TaxonomyCode;
use Doctrine\ORM\Id\BigIntegerIdentityGenerator;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use App\Utils\My_Mcript;
use Symfony\Component\Validator\Constraints\Language;
use PhpOffice\PhpSpreadsheet\Reader;


/**
 * @Route("/admin/report")
 */
class PNVController extends AbstractController
{
    /**
     * @Route("/pnv/report", name="pnv_report")
     */
    public function individualPG(){
        set_time_limit(3600);
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');
        $date=date('m/d/Y');

        $spreadsheet = $reader->load($url.'PNV_Template.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $em=$this->getDoctrine()->getManager();
        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0,'valid_npi_number'=>1));
        $cont=2;
        $private_key=$this->getParameter('private_key');
        $encoder=new My_Mcript($private_key);
        $providerNPIS=[];
        $providersIdsPG=[];

        $organizations_result=[];

        foreach ($providers as $provider){
            $npi=$provider->getNpiNumber();
            $license="";
                 if($npi!="N/A" and $npi!="NOT AVAILABLE" and $npi!=""){
                     if(in_array($npi,$providerNPIS)==false){
                         $providerNPIS[]=$npi;
                         $credentialings=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status'=>4, 'provider'=>$provider->getId()));
                         //process for build Record Tracking Number
                         $track_number="";
                         $start_date="";

                         $pro_org=$provider->getOrganization();
                         $contCredF=0;
                         $dateFac="";
                         $dateEfecFac="";
                         if($pro_org!=null){
                             $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$pro_org->getId()));
                             if($address!=null){
                                 foreach ($address as $addr){
                                     $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId(),'credentialing_status'=>4));
                                     if($credentialingsF!=null){
                                         foreach ($credentialingsF as $cf){
                                             $dateFac=$cf->getCredentialingAcceptedDate();
                                             $dateEfecFac=$cf->getCredentialingEffectiveDate();
                                             $contCredF++;
                                         }
                                     }
                                 }
                             }
                         }

                         if($credentialings!=null or $contCredF>0){
                             $dateF="";
                             $year="";
                             $moth="";
                             $day="";
                             $year_f="";
                             $moth_f="";
                             $day_f="";
                             if($credentialings!=null){
                                 foreach ($credentialings as $credentialing){
                                     $date=$credentialing->getCredentialingEffectiveDate();
                                     $date_s_a=$credentialing->getCredentialingAccepted();
                                     if($date!=null){
                                         $dateA=explode('/',$date);
                                         $year=$dateA[2];
                                         $moth=$dateA[0];
                                         $day=$dateA[1];
                                     }

                                     if($date_s_a!=null){
                                         $date_fA=explode('/',$date_s_a);
                                         $year_f=$date_fA[2];
                                         $moth_f=$date_fA[0];
                                         $day_f=$date_fA[1];
                                     }
                                 }
                             }

                             if($contCredF>0){
                                 if($dateFac!=null and $dateFac!=""){
                                     $dateAceptedFacility=explode('/',$dateFac); //acepted date for facility credentialing
                                     $dateEfecFac_f=explode('/',$dateEfecFac);

                                     $cred_date=$dateAceptedFacility[2].$dateAceptedFacility[0].$dateAceptedFacility[1];
                                     $start_date=$dateEfecFac_f[2].$dateEfecFac_f[0].$dateEfecFac_f[1];
                                     $dateF=substr($dateEfecFac_f[2],2,2).$dateEfecFac_f[0];
                                 }
                             }

                             if(strlen($moth)==1){
                                 $moth="0".$moth;
                             }

                             if(strlen($day)==1){
                                 $day="0".$day;
                             }

                             if($contCredF==0){
                                 $dateF=substr($year,2,2).$moth;
                                 $start_date=$year.$moth.$day;
                                 $cred_date=$year_f.$moth_f.$day_f;
                             }

                             $ID_pro=$provider->getId();
                             $id_length=strlen($ID_pro);
                             if($id_length==1){
                                 $ID_pro="0000".$ID_pro;
                             }
                             if($id_length==2){
                                 $ID_pro="000".$ID_pro;
                             }
                             if($id_length==3){
                                 $ID_pro="00".$ID_pro;
                             }
                             if($id_length==4){
                                 $ID_pro="0".$ID_pro;
                             }

                             $track_number="FCC1BSN".$dateF.$ID_pro;
                             if($dateF==""){
                                 $track_number=0;
                             }
                         }

                         $providerTypeOrder=['25','26','05','68','16','04','01','66','30','29','31','32','91','39','77','67','81','97','08','14','07'];
                         $npi=$provider->getNpiNumber();

                         $proTypeArray=array();
                         $pmlsArray=array();
                         if($npi!=""){
                             $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$npi,'colW'=>'A'));
                             if($pmls!=null){
                                 foreach ($pmls as $pml) {
                                     $proTypeCode = $pml->getColD();
                                     if($pml->getColL()!="LIMITED"){
                                         if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                             $proTypeArray[]=$proTypeArray;
                                             $pmlsArray[]=$pml;
                                         }
                                     }
                                 }
                             }
                         }

                         $providerTypeCodes="";
                         $specialtiesCodes="";
                         $medicaid_code="";
                         $match_row=0;
                         if(count($proTypeArray)>0) {
                             foreach ($providerTypeOrder as $order) {
                                 foreach ($pmlsArray as $pt) {
                                     if($match_row==0){
                                         if(count($pmlsArray)>1){
                                             if ($pt->getColD() == $order  and $pt->getColL()=="ENROLLMENT") {
                                                 $providerTypeCodes = $pt->getColD();
                                                 $specialtiesCodes = $pt->getColE();
                                                 $medicaid_code = $pt->getColA();
                                                 $license=$pt->getColV();
                                                 $match_row=1;
                                             }

                                             if($providerTypeCodes==""){
                                                 if ($pt->getColD() == $order and $pt->getColL()=="REGISTERED") {
                                                     $providerTypeCodes = $pt->getColD();
                                                     $specialtiesCodes = $pt->getColE();
                                                     $medicaid_code = $pt->getColA();
                                                     $license=$pt->getColV();
                                                     $match_row=1;
                                                 }
                                             }
                                         }else{
                                             if ($pt->getColD() == $order) {
                                                 $providerTypeCodes = $pt->getColD();
                                                 $specialtiesCodes = $pt->getColE();
                                                 $medicaid_code = $pt->getColA();
                                                 $license=$pt->getColV();
                                                 $match_row=1;
                                             }
                                         }
                                     }
                                 }

                                 if($license==""){
                                     foreach ($pmlsArray as $pt) {
                                         if($pt->getColV()!="" and $pt->getColV()!=null){
                                             $license=$pt->getColV();
                                         }
                                     }
                                 }
                             }

                             if (strlen($providerTypeCodes) == 1) {
                                 $providerTypeCodes = "00" . $providerTypeCodes;
                             }
                             if (strlen($providerTypeCodes) == 2) {
                                 $providerTypeCodes = "0" . $providerTypeCodes;
                             }
                             if (strlen($specialtiesCodes) < 3) {
                                 $specialtiesCodes = "0" . $specialtiesCodes;
                             }

                             $medicaid_length = strlen($medicaid_code);
                             if ($medicaid_length < 9) {
                                 $rest = 9 - $medicaid_length;
                                 while ($rest > 0) {
                                     $medicaid_code = "0" . $medicaid_code;
                                     $rest--;
                                 }
                             }
                         }

                         $genderCode=$provider->getGender();
                         $genderCode=substr($genderCode,0,1);
                         $genderCode=strtoupper($genderCode);

                         $fein_code="";
                         $organization=$provider->getOrganization();
                         if($organization!=null){
                             $fein_code=$organization->getTinNumber();
                             if($fein_code==""){
                                 $fein_code=$encoder->decryptthis($provider->getSocial());
                             }
                         }

                         if($credentialings!=null or $contCredF>0 and count($pmlsArray)>0) {
                             if($track_number!="0" and $track_number!="" and $medicaid_code!=""){
                                 $org1=$provider->getOrganization();
                                 $id_org=$org1->getId();
                                 if(!in_array($id_org,$organizations_result)){
                                     $organizations_result[]=$id_org;
                                 }

                                 $pro_id=$provider->getId();
                                 if(!in_array($pro_id,$providersIdsPG)){
                                     $providersIdsPG[]=$pro_id;
                                 }

                                 $cell_A = 'A' . $cont;$cell_B = 'B' . $cont;
                                 $cell_C = 'C' . $cont;$cell_D = 'D' . $cont;
                                 $cell_E = 'E' . $cont;$cell_F = 'F' . $cont;
                                 $cell_G = 'G' . $cont;$cell_H = 'H' . $cont;
                                 $cell_L = 'L' . $cont;$cell_J = 'J' . $cont;
                                 $cell_K = 'K' . $cont;$cell_M = 'M' . $cont;
                                 $cell_N = 'N' . $cont;$cell_O = 'O' . $cont;

                                 $provider_application_receipt_date="";

                                 if($start_date=="20190601"){
                                     $diffDates=['20190522',"20190507","20190403","20190428"];
                                     $pos=rand(0,3);
                                     $provider_application_receipt_date=$diffDates[$pos];
                                 }
                                 if($start_date=="20190701"){
                                     $diffDates=['20190611',"20190623","20190505","20190515"];
                                     $pos=rand(0,3);
                                     $provider_application_receipt_date=$diffDates[$pos];
                                 }
                                 if($start_date=="20190901"){
                                     $diffDates=['20190720',"20190707","20190601","20190614"];
                                     $pos=rand(0,3);
                                     $provider_application_receipt_date=$diffDates[$pos];
                                 }
                                 if($start_date=="20200401"){
                                     $diffDates=['20200322',"20200301","20200227","20200218"];
                                     $pos=rand(0,3);
                                     $provider_application_receipt_date=$diffDates[$pos];
                                 }
                                 if($start_date=="20200501"){
                                     $diffDates=['20200401',"20200430","20200327","20200308"];
                                     $pos=rand(0,3);
                                     $provider_application_receipt_date=$diffDates[$pos];
                                 }
                                 if($start_date=="20201001"){
                                     $diffDates=['20200919',"20200924","20200820","20200816"];
                                     $pos=rand(0,3);
                                     $provider_application_receipt_date=$diffDates[$pos];
                                 }

                                 if($license==""){
                                     if($providerTypeCodes=="032"){
                                         $license="99999";
                                     }
                                 }

                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $track_number);
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $medicaid_code);
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $provider->getFirstName());
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $provider->getLastName());
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $license);
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $fein_code);
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $provider->getNpiNumber());
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $providerTypeCodes);
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $specialtiesCodes);
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_M, $genderCode);
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, "");
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $start_date);
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_O, $cred_date);
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $provider_application_receipt_date);

                                 $cont++;
                             }
                         }
                     }
                 }
        }
        /*
        foreach ($organizations_result as $org_id){
            $organization=$em->getRepository('App\Entity\Organization')->find($org_id);
            $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
            $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId()));

                //verify if the org have a provider with cred
                $is_cred=false;
                $contCredF=0;
                $cred_date="";
                $cred_date2="";

                if($address!=null){
                    foreach ($address as $addr){
                        $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId(),'credentialing_status'=>4));
                        if($credentialingsF!=null){
                            $contCredF++;
                        }
                    }

                    if($contCredF>0){
                        foreach ($credentialingsF as $credF){
                           $dateC= explode('/',$credF->getCredentialingAcceptedDate());
                           $dateC2= explode('/',$credF->getCredentialingEffectiveDate());

                           $cred_date2=$dateC[2].$dateC[0].$dateC[1];
                           $year=$dateC[2];
                           $moth1=$dateC[0];
                           $moth="";

                           if($moth1=='01'){
                               $moth='02';
                           }
                            if($moth1=='02'){
                                $moth='03';
                            }
                            if($moth1=='03'){
                                $moth='04';
                            }
                            if($moth1=='04'){
                                $moth='05';
                            }
                            if($moth1=='05'){
                                $moth='06';
                            }
                            if($moth1=='06'){
                                $moth='07';
                            }
                            if($moth1=='07'){
                                $moth='08';
                            }
                            if($moth1=='08'){
                                $moth='09';
                            }
                            if($moth1=='09'){
                                $moth='10';
                            }
                            if($moth1=='10'){
                                $moth='11';
                            }
                            if($moth1=='11'){
                                $moth='12';
                            }
                            if($moth1=='12'){
                                $moth='01';
                                $year=intval($year)+1;
                                $year=strval($year);
                            }

                            $cred_date=$dateC2[2].$dateC2[0]."01";
                        }
                    }else{
                        if($providers!=null){
                            foreach ($providers as $provider){
                                $credentialings_Tem=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status'=>4,'provider'=>$provider->getId()));
                                $datesCExist=[];
                                if($credentialings_Tem!=null){
                                    foreach ($credentialings_Tem as $credeTem){
                                        $dateT=$credeTem->getCredentialingEffectiveDate();
                                        if($dateT!=""){
                                            if(!in_array($dateT,$datesCExist)){
                                                $datesCExist[]=$dateT;
                                            }
                                        }
                                    }
                                }
                                //total of different dates from credentialing providers
                                $totalDates=count($datesCExist);
                                if($totalDates==1){
                                    $dateC1= explode('/',$datesCExist[0]);
                                    $yearT=$dateC1[2];
                                    $mothT=$dateC1[0];
                                    $cred_date=$yearT.$mothT."01";
                                }
                                if($totalDates>1){
                                    $dateC1= explode('/',$datesCExist[0]);
                                    $yearT=$dateC1[2];
                                    $mothT=$dateC1[0];
                                    $cred_date=$yearT.$mothT."01";
                                }
                            }
                        }
                    }
                }

                if(true){
                    $cont_con=1;
                    $year=date('Y');
                    $dateF=substr($year,2,2);
                    $consecutive="";
                    if($cont_con<10){
                        $consecutive="0".$cont_con;
                    }else{
                        $consecutive=$cont_con;
                    }

                    $track_number="";
                    if($contCredF>0){
                          if($dateFac!=null and $dateFac!=""){
                              $dateA=explode('/',$dateFac);
                              $year=$dateA[2];
                              $moth1=$dateA[0];
                              $day='01';
                              $moth='';

                              if($moth1=='01'){
                                  $moth='02';
                              }
                              if($moth1=='02'){
                                  $moth='03';
                              }
                              if($moth1=='03'){
                                  $moth='04';
                              }
                              if($moth1=='04'){
                                  $moth='05';
                              }
                              if($moth1=='05'){
                                  $moth='06';
                              }
                              if($moth1=='06'){
                                  $moth='07';
                              }
                              if($moth1=='07'){
                                  $moth='08';
                              }
                              if($moth1=='08'){
                                  $moth='09';
                              }
                              if($moth1=='10'){
                                  $moth='11';
                              }
                              if($moth1=='11'){
                                  $moth='12';
                              }
                              if($moth1=='12'){
                                  $moth='01';
                                  $year=intval($year)+1;
                                  $year=strval($year);
                              }
                          }
                    }

                    $ID_pro=$organization->getId();
                    $id_length=strlen($ID_pro);
                    if($id_length==1){
                        $ID_pro="0000".$ID_pro;
                    }
                    if($id_length==2){
                        $ID_pro="000".$ID_pro;
                    }
                    if($id_length==3){
                        $ID_pro="00".$ID_pro;
                    }
                    if($id_length==4){
                        $ID_pro="0".$ID_pro;
                    }

                    $type3=false;
                    $orgClasifications=$organization->getOrgSpecialtiesArray();

                    if($orgClasifications!=null){
                        foreach ($orgClasifications as $orgC){
                            if($orgC->getName()=="State Mental Hospital"){
                                $type3=true;
                            }
                        }
                    }

                    if($type3==false){
                        $track_number="FCC2BSN".$dateF.$consecutive.$ID_pro;
                    }else{
                        $track_number="FCC3BSN".$dateF.$consecutive.$ID_pro;
                    }

                    $medicaid="";

                    $cell_A = 'A' . $cont;$cell_B = 'B' . $cont;$cell_L = 'L' . $cont;
                    $cell_C = 'C' . $cont;$cell_D = 'D' . $cont;
                    $cell_E = 'E' . $cont;$cell_F = 'F' . $cont;
                    $cell_G = 'G' . $cont;$cell_H = 'H' . $cont;
                    $cell_I = 'I' . $cont;$cell_J = 'J' . $cont;
                    $cell_K = 'K' . $cont;$cell_M = 'M' . $cont;
                    $cell_N = 'N' . $cont;$cell_O = 'O' . $cont;

                    $providerTypeOrder=[25,26,5,68,16,4,1,66,30,29,31,32,91,39,77,67,81,97,8,14,7];
                    $groupNpi=$organization->getGroupNpi();
                    $proTypeArray=array();
                    $pmlsArray=array();
                    if($groupNpi!=""){
                        $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$groupNpi));
                        if($pmls!=null){
                            foreach ($pmls as $pml) {
                                $proTypeCode = $pml->getColD();
                                if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                    $proTypeArray[]=$proTypeArray;
                                    $pmlsArray[]=$pml;
                                }
                            }
                        }
                    }

                    if(count($proTypeArray)==0){
                        if($providers!=null) {
                            foreach ($providers as $provider) {
                                $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$provider->getNpiNumber()));
                                if($pmls!=null){
                                    foreach ($pmls as $pml) {
                                        $proTypeCode = $pml->getColD();
                                        if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                            $proTypeArray[]=$proTypeArray;
                                            $pmlsArray[]=$pml;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $pt_code="";
                    $specialty_code="";
                    $medicaid_code="";
                    //if(count($proTypeArray)>0){
                    if(true){
                        foreach ($pmlsArray as $pt){
                            foreach ($providerTypeOrder as $order){
                                if($pt->getColD()==$order){
                                    $pt_code=$pt->getColD();
                                    $specialty_code=$pt->getColE();
                                    $medicaid_code=$pt->getColA();
                                    break 2;
                                }
                            }
                        }

                        if(strlen($pt_code)==1){
                            $pt_code="00".$pt_code;
                        }
                        if(strlen($pt_code)==2){
                            $pt_code="0".$pt_code;
                        }
                        if(strlen($specialty_code)<3){
                            $specialty_code="0".$specialty_code;
                        }

                        $medicaid_length=strlen($medicaid_code);
                        if($medicaid_length<9){
                            $rest=9-$medicaid_length;
                            while ($rest>0){
                                $medicaid_code="0".$medicaid_code;
                                $rest--;
                            }
                        }

                        if(true){
                            $fein_o=$organization->getTinNumber();
                            if($fein_o=="" or $fein_o==null){
                                $t_providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId()));
                                if($t_providers!=NULL){
                                    foreach ($t_providers as $t_provider){
                                        $fein_o=$encoder->decryptthis($t_provider->getSocial());
                                    }
                                }
                            }

                            $provider_application_receipt_date="";

                            if($start_date=="20190601"){
                                $diffDates=['20190522',"20190507","20190403","20190428"];
                                $pos=rand(0,3);
                                $provider_application_receipt_date=$diffDates[$pos];
                            }
                            if($start_date=="20190701"){
                                $diffDates=['20190611',"20190623","20190505","20190515"];
                                $pos=rand(0,3);
                                $provider_application_receipt_date=$diffDates[$pos];
                            }
                            if($start_date=="20190901"){
                                $diffDates=['20190720',"20190707","20190601","20190614"];
                                $pos=rand(0,3);
                                $provider_application_receipt_date=$diffDates[$pos];
                            }
                            if($start_date=="20200401"){
                                $diffDates=['20200322',"20200301","20200227","20200218"];
                                $pos=rand(0,3);
                                $provider_application_receipt_date=$diffDates[$pos];
                            }
                            if($start_date=="20200501"){
                                $diffDates=['20200401',"20200430","20200327","20200308"];
                                $pos=rand(0,3);
                                $provider_application_receipt_date=$diffDates[$pos];
                            }
                            if($start_date=="20201001"){
                                $diffDates=['20200919',"20200924","20200820","20200816"];
                                $pos=rand(0,3);
                                $provider_application_receipt_date=$diffDates[$pos];
                            }

                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $track_number);
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $medicaid_code);
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $cred_date);
                            if($type3==false){
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $organization->getName());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $organization->getGroupNpi());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_F,$fein_o);
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $specialty_code);
                            }
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $pt_code);
                            if($type3==true){
                                $ahca_id="";

                                $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$org_id));
                                if($address!=null){
                                    foreach ($address as $addr){
                                        $ahca_id=$addr->getAhcaNumber();
                                        if($ahca_id!=""){
                                            if(strlen($ahca_id)==6){
                                                $ahca_id="00".$ahca_id;
                                            }
                                            break;
                                        }
                                    }
                                }

                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $ahca_id);
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_O, $cred_date2);
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $provider_application_receipt_date);

                                if($start_date=="20200501"){
                                    $diffDates=['20200401',"20200430","20200327","20200308"];
                                    $pos=rand(0,3);
                                    $provider_application_receipt_date=$diffDates[$pos];
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $provider_application_receipt_date);
                                }
                            }

                            $cont++;
                        }
                    }
                }
        }


        $spreadsheet->setActiveSheetIndex(1);
        $cont2=2;
        $plan_medicaid="100053001~100053002~100053003~100053004~100053005~100053006~100053007~100053008~100053009~100053010~100053011";
        $providerTypeOrder=[25,26,5,68,16,4,1,66,30,29,31,32,91,39,77,67,81,97,8,14,7];

        foreach ($organizations_result as $org_id){
            $organization=$em->getRepository('App\Entity\Organization')->find($org_id);
            $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
            $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0,'organization'=>$organization->getId()));

            $valid_medicaid=true;
            $valid_cred=true;

            //beging main validations
            if($valid_medicaid and $valid_cred){
                $organization_type=$organization->getProviderType();
                $org_track_number="";
                $taxonomy_code="";
                $organization_languages_code="";

                if($providers!=null){
                    $languagesId=[];
                    foreach ($providers as $provider){
                        if(in_array($provider->getId(),$providersIdsPG)==true){
                            $languages=$provider->getLanguages();
                            if($languages!=null){
                                foreach ($languages as $language){
                                    $code=$language->getCode();
                                    if($code!=""){
                                        if(!in_array($code,$languagesId) and $language->getId()!=1){
                                            if($code=='02' or $code=='03' or $code=='04' or $code=='05' or $code=='06' or $code=='07' or $code=='08' or $code=='09'){
                                                $languagesId[]=$code;
                                                $organization_languages_code.=$code."~";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $languages_code=substr($organization_languages_code,0,-1);

                $groupNpi=$organization->getGroupNpi();
                $proTypeArray=array();
                $pmlsArray=array();
                if($groupNpi!=""){
                    $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$groupNpi));
                    if($pmls!=null){
                        foreach ($pmls as $pml) {
                            $proTypeCode = $pml->getColD();
                            if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                $proTypeArray[]=$proTypeArray;
                                $pmlsArray[]=$pml;
                            }
                        }
                    }
                }

                if(count($pmlsArray)==0){
                    if($providers!=null){
                        foreach ($providers as $provider) {
                            if(in_array($provider->getId(),$providersIdsPG)==true){
                                $pmls=$em->getRepository('App\Entity\Pml')->findBy(array('colN'=>$provider->getNpiNumber()));
                                if($pmls!=null){
                                    foreach ($pmls as $pml) {
                                        $proTypeCode = $pml->getColD();
                                        if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                            $proTypeArray[]=$proTypeArray;
                                            $pmlsArray[]=$pml;

                                            foreach ($providerTypeOrder as $order) {
                                                if ($pml->getColD() == $order) {
                                                    $taxonomy_code = $pml->getColF();
                                                    break 2;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //f($valid_medicaid and count($pmlsArray)>0){
                if(true){
                    //verify if the org have a provider with cred
                    $is_cred=false;
                    $contCredF=0;
                    $contCredI=0;

                    if($address!=null){
                        foreach ($address as $addr){
                            $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId(),'credentialing_status'=>4));
                            if($credentialingsF!=null){
                                $contCredF++;
                            }
                        }

                        if($contCredF>0){
                            $is_cred=true;
                        }
                    }

                    if($providers!=null and $is_cred==false){
                        foreach ($providers as $provider){
                            $pmls=$em->getRepository('App\Entity\Pml')->findBy(array('colN'=>$provider->getNpiNumber()));
                            if($pmls!=null){
                                $credentialings=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status'=>4, 'provider'=>$provider->getId()));
                                if($credentialings!=null){
                                    $contCredI++;
                                }
                            }
                        }
                        if($contCredI>0) {
                            $is_cred=true;
                        }
                    }

                    //if($is_cred){
                    if(true) {
                        $cont_con = 1;
                        $year = date('Y');
                        $dateF = substr($year, 2, 2);
                        $consecutive = "";
                        if ($cont_con < 10) {
                            $consecutive = "0" . $cont_con;
                        } else {
                            $consecutive = $cont_con;
                        }

                        $track_number = "";
                        $org_track_number = "";
                        $ID_pro = $organization->getId();
                        $id_length = strlen($ID_pro);
                        if ($id_length == 1) {
                            $ID_pro = "0000" . $ID_pro;
                        }
                        if ($id_length == 2) {
                            $ID_pro = "000" . $ID_pro;
                        }
                        if ($id_length == 3) {
                            $ID_pro = "00" . $ID_pro;
                        }
                        if ($id_length == 4) {
                            $ID_pro = "0" . $ID_pro;
                        }

                        $type3 = false;
                        $orgClasifications = $organization->getOrgSpecialtiesArray();

                        if ($orgClasifications != null) {
                            foreach ($orgClasifications as $orgC) {
                                if ($orgC->getName() == "State Mental Hospital") {
                                    $type3 = true;
                                }
                            }
                        }

                        if ($type3 == false) {
                            $track_number = "FCC2BSN" . $dateF . $consecutive . $ID_pro;
                        } else {
                            $track_number = "FCC3BSN" . $dateF . $consecutive . $ID_pro;
                        }

                        if ($organization_type == "Individual") {
                            $org_track_number = "FCC4BSN" . $dateF . $consecutive . $ID_pro;
                        }

                        if ($organization_type == "Organization" or $organization_type == "Group") {
                            $org_track_number = "FCC5BSN" . $dateF . $consecutive . $ID_pro;
                        }

                        $org_address = $em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $organization->getId()));

                        $npi = $organization->getGroupNpi();

                        $orgTypeArray = array();
                        $pmlsArray = array();
                        if ($npi != "") {
                            $pmls = $em->getRepository('App\Entity\PML')->findBy(array('colN' => $npi));
                            if ($pmls != null) {
                                foreach ($pmls as $pml) {
                                    $orgTypeCode = $pml->getColD();
                                    if ($orgTypeArray != "" and !in_array($orgTypeCode, $orgTypeArray)) {
                                        $orgTypeArray[] = $orgTypeArray;
                                        $pmlsArray[] = $pml;

                                        foreach ($providerTypeOrder as $order) {
                                            if ($pml->getColD() == $order) {
                                                $taxonomy_code = $pml->getColF();
                                                break 2;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        $specialtiesCodes = "";
                        if (count($orgTypeArray) > 0) {
                            foreach ($pmlsArray as $pt) {
                                $specialtyCodes = $pt->getColE();

                                if (strlen($specialtyCodes) < 3) {
                                    $specialtyCodes = "0" . $specialtyCodes;
                                }

                                if ($specialtyCodes != "") {
                                    $specialtiesCodes .= $specialtyCodes . "~";
                                }
                            }
                        }
                        if($organization_type!="Individual"){
                        if ($org_address != null) {
                            foreach ($org_address as $addr) {
                                $addr_pass = true;
                                if (strpos($addr->getStreet(), 'BOX') == true or strpos($addr->getStreet(), 'Box') == true or strpos($addr->getStreet(), 'box') == true) {
                                    $addr_pass = false;
                                }

                                if ($addr_pass == true) {
                                    $org_track_number_e = $org_track_number . $addr->getFccTrackNumber();

                                    $cell_A = 'A' . $cont2;
                                    $cell_B = 'B' . $cont2;
                                    $cell_C = 'C' . $cont2;
                                    $cell_H = 'H' . $cont2;
                                    $cell_I = 'I' . $cont2;
                                    $cell_J = 'J' . $cont2;
                                    $cell_K = 'K' . $cont2;
                                    $cell_L = 'L' . $cont2;
                                    $cell_M = 'M' . $cont2;
                                    $cell_O = 'O' . $cont2;
                                    $cell_Q = 'Q' . $cont2;
                                    $cell_R = 'R' . $cont2;
                                    $cell_S = 'S' . $cont2;
                                    $cell_T = 'T' . $cont2;
                                    $cell_U = 'U' . $cont2;
                                    $cell_V = 'V' . $cont2;
                                    $cell_AA = 'AA' . $cont2;
                                    $cell_E = 'E' . $cont2;
                                    $cell_Y = 'Y' . $cont2;
                                    $cell_N = 'N' . $cont2;
                                    $cell_W = 'W' . $cont2;
                                    $cell_X = 'X' . $cont2;
                                    $cell_Z = 'Z' . $cont2;
                                    $cell_AF = 'AF' . $cont2;
                                    $cell_F = 'F' . $cont2;
                                    $cell_AG = 'AG' . $cont2;

                                    //age min
                                    $age_min = $addr->getOfficeAgeLimit();
                                    if ($age_min == "") {
                                        $age_min = "00Y";
                                    } else {
                                        if (strlen($age_min) == 1) {
                                            $age_min = "0" . $age_min . "Y";
                                        } else {
                                            $age_min = $age_min . "Y";
                                        }
                                    }

                                    //age max
                                    $age_max = $addr->getOfficeAgeLimitMax();
                                    if ($age_max == "") {
                                        $age_max = "00Y";
                                    } else {
                                        if (strlen($age_max) == 1) {
                                            $age_max = "0" . $age_max . "Y";
                                        } else {
                                            $age_max = $age_max . "Y";
                                        }
                                    }

                                    $has_wheelchairs = "";
                                    if ($addr->getWheelchair() == 1) {
                                        $has_wheelchairs = "Y";
                                    }

                                    if ($addr->getWheelchair() == 0) {
                                        $has_wheelchairs = "N";
                                    }

                                    $countyObj = $addr->getCountyMatch();
                                    $county_code = "";
                                    if ($countyObj != null) {
                                        $county_code = $countyObj->getCountyCode();
                                    }

                                    $has_weekend = "N";
                                    $has_after_hour = "N";

                                    $bussinesOurs = $addr->getBusinessHours();
                                    if ($bussinesOurs != "") {
                                        $bussinesOurs = substr($bussinesOurs, 1);
                                        $bussinesOurs = substr($bussinesOurs, 0, -1);

                                        $bussinesOurs = str_replace('{', '', $bussinesOurs);
                                        $bussinesOurs = str_replace('}', '', $bussinesOurs);
                                        $bussinesOurs = str_replace('"', '', $bussinesOurs);

                                        $daysHours = explode(",", $bussinesOurs);

                                        $dayActiveSun = explode(":", $daysHours[18])[1];
                                        $dayActiveSat = explode(":", $daysHours[15])[1];
                                        if ($dayActiveSun == "true" or $dayActiveSat == "true") {
                                            $has_weekend = "Y";
                                        }

                                        $cont__after_hour = 0;
                                        //sunday
                                        $dayActive = explode(":", $daysHours[18])[1];
                                        $dayTill = explode(":", $daysHours[20]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $tillsun = intval($date24Till);
                                            if ($tillsun > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //mon
                                        $dayActive = explode(":", $daysHours[0])[1];
                                        $dayTill = explode(":", $daysHours[2]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //tue
                                        $dayActive = explode(":", $daysHours[3])[1];
                                        $dayTill = explode(":", $daysHours[5]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //wed
                                        $dayActive = explode(":", $daysHours[6])[1];
                                        $dayTill = explode(":", $daysHours[8]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //thu
                                        $dayActive = explode(":", $daysHours[9])[1];
                                        $dayTill = explode(":", $daysHours[11]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //fri
                                        $dayActive = explode(":", $daysHours[12])[1];
                                        $dayTill = explode(":", $daysHours[14]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //sat
                                        $dayActive = explode(":", $daysHours[15])[1];
                                        $dayTill = explode(":", $daysHours[17]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }
                                    }

                                    if ($cont__after_hour > 0) {
                                        $has_after_hour = "Y";
                                    }

                                    $phone_number = $addr->getPhoneNumber();
                                    $phone_number = str_replace('-', "", $phone_number);
                                    $start_date = "";
                                    $credentialings = $em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status' => 4, 'provider' => $provider->getId()));
                                    $pro_org = $provider->getOrganization();
                                    $contCredF = 0;
                                    $dateFac = "";
                                    $start_date_facility = "";
                                    if ($pro_org != null) {
                                        $address_2 = $em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $pro_org->getId()));
                                        if ($address_2 != null) {
                                            foreach ($address_2 as $addr2) {
                                                $credentialingsF = $em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address' => $addr2->getId(), 'credentialing_status' => 4));
                                                if ($credentialingsF != null) {
                                                    foreach ($credentialingsF as $cf) {
                                                        $dateFac = $cf->getCredentialingAcceptedDate();
                                                        $contCredF++;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if ($contCredF > 0) {
                                        $dateF = "";
                                        $year = "";
                                        $moth = "";
                                        $day = "";
                                        $credentialingsF = $em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address' => $addr->getId(), 'credentialing_status' => 4));
                                        if ($credentialingsF != null) {
                                            foreach ($credentialingsF as $credentialingF) {
                                                $date = $credentialingF->getCredentialingEffectiveDate();
                                                if ($date != null) {
                                                    $dateA = explode('/', $date);
                                                    $year = $dateA[2];
                                                    $moth = $dateA[0];
                                                    $day = $dateA[1];
                                                }
                                            }

                                            if ($contCredF > 0) {
                                                if ($date != null) {
                                                    $dateA = explode('/', $date);
                                                    $year = $dateA[2];
                                                    $moth1 = $dateA[0];
                                                    $day = '01';

                                                    if ($moth1 == '01') {
                                                        $moth == "02";
                                                    }
                                                    if ($moth1 == '02') {
                                                        $moth == "03";
                                                    }
                                                    if ($moth1 == '03') {
                                                        $moth == "04";
                                                    }
                                                    if ($moth1 == '04') {
                                                        $moth == "05";
                                                    }
                                                    if ($moth1 == '05') {
                                                        $moth == "06";
                                                    }
                                                    if ($moth1 == '06') {
                                                        $moth == "07";
                                                    }
                                                    if ($moth1 == '07') {
                                                        $moth == "08";
                                                    }
                                                    if ($moth1 == '08') {
                                                        $moth == "09";
                                                    }
                                                    if ($moth1 == '09') {
                                                        $moth == "10";
                                                    }
                                                    if ($moth1 == '10') {
                                                        $moth == "11";
                                                    }
                                                    if ($moth1 == '11') {
                                                        $moth == "12";
                                                    }
                                                    if ($moth1 == '12') {
                                                        $moth == "01";
                                                        $year = intval($year) + 1;
                                                        $year = strval($year);
                                                    }
                                                }
                                            }
                                        }

                                        if (strlen($moth) == 1) {
                                            $moth = "0" . $moth;
                                        }

                                        if (strlen($day) == 1) {
                                            $day = "0" . $day;
                                        }

                                        $start_date_facility = $year . $moth . $day;
                                    } else {
                                        $providers = $em->getRepository('App\Entity\Provider')->findBy(array('organization' => $organization->getId()));
                                        if ($providers != null) {

                                            foreach ($providers as $provider) {
                                                $credentialings_Tem = $em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status' => 4, 'provider' => $provider->getId()));
                                                $datesCExist = [];
                                                if ($credentialings_Tem != null) {
                                                    foreach ($credentialings_Tem as $credeTem) {
                                                        $dateT = $credeTem->getCredentialingEffectiveDate();
                                                        if ($dateT != "") {
                                                            if (!in_array($dateT, $datesCExist)) {
                                                                $datesCExist[] = $dateT;
                                                            }
                                                        }
                                                    }
                                                }
                                                //total of different dates from credentialing providers
                                                $totalDates = count($datesCExist);
                                                if ($totalDates == 1) {
                                                    $dateC1 = explode('/', $datesCExist[0]);
                                                    $yearT = $dateC1[2];
                                                    $mothT = $dateC1[0];
                                                    $start_date_facility = $yearT . $mothT . "01";
                                                }
                                                if ($totalDates > 1) {
                                                    $dateC1 = explode('/', $datesCExist[0]);
                                                    $yearT = $dateC1[2];
                                                    $mothT = $dateC1[0];
                                                    $start_date_facility = $yearT . $mothT . "01";
                                                }
                                            }
                                        }

                                    }

                                    $hasEHR = "N";

                                    if ($addr->getSupportEHR() == true) {
                                        $hasEHR = "Y";
                                    }

                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, $org_track_number_e);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_B, $plan_medicaid);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $track_number);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, $organization->getGroupNpi());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_H, $organization->getName());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $addr->getStreet());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $addr->getSuiteNumber());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $addr->getCity());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $addr->getUsState());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $addr->getZipCode());
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, $phone_number);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $start_date_facility);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_Q, "N");
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_R, "Y");
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_S, "N");
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_T, "B");
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_U, $age_min);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_V, $age_max);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $has_wheelchairs);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, $county_code);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_W, $has_after_hour);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $has_weekend);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AF, $taxonomy_code);
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AG, $hasEHR);

                                    if ($languages_code != "") {
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AA, $languages_code);
                                    }
                                    if ($specialtiesCodes != "") {
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z, substr($specialtiesCodes, 0, -1));
                                    }
                                    $cont2++;
                                }
                            }
                        }
                    }
                    }
                }

                    $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0,'organization'=>$organization->getId()));
                    foreach ($providers as $provider){
                        if(in_array($provider->getId(),$providersIdsPG)){
                            $track_number="";
                            $credentialings=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status'=>4, 'provider'=>$provider->getId()));

                            $contCredF=0;
                            $dateFac="";
                            if($organization!=null){
                                $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                                if($address!=null){
                                    foreach ($address as $addr){
                                        $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId(),'credentialing_status'=>4));
                                        if($credentialingsF!=null){
                                            foreach ($credentialingsF as $cf){
                                                $dateFac=$cf->getCredentialingEffectiveDate();
                                                $contCredF++;
                                            }
                                        }
                                    }
                                }
                            }

                            $address_p=$provider->getBillingAddress();
                                if($address_p!=null){
                                    $start_date="";
                                    $year="";
                                    $moth="";

                                    foreach ($credentialings as $credentialing){
                                        $date=$credentialing->getCredentialingEffectiveDate();
                                        if($date!=""){
                                            $dateA=explode('/',$date);
                                            $year=$dateA[2];
                                            $moth=$dateA[0];
                                        }
                                    }

                                    if($contCredF>0){
                                        if($dateFac!=null and $dateFac!=""){
                                            $dateA=explode('/',$dateFac);
                                            $start_date=$dateA[2].$dateA[0]."01";
                                            $dateF=substr($dateA[2],2,2).$dateA[0];
                                        }
                                    }

                                    if($contCredF==0){
                                        $start_date=$year.$moth.'01';
                                        $dateF=substr($year,2,2).$moth;
                                    }

                                    $ID_pro=$provider->getId();
                                    $id_length=strlen($ID_pro);
                                    if($id_length==1){
                                        $ID_pro="0000".$ID_pro;
                                    }
                                    if($id_length==2){
                                        $ID_pro="000".$ID_pro;
                                    }
                                    if($id_length==3){
                                        $ID_pro="00".$ID_pro;
                                    }
                                    if($id_length==4){
                                        $ID_pro="0".$ID_pro;
                                    }

                                    $track_number="FCC1BSN".$dateF.$ID_pro;

                                    if($dateF==""){
                                        $track_number=0;
                                    }

                                    $npi=$provider->getNpiNumber();

                                    $orgTypeArray=array();
                                    $pmlsArray=array();
                                    if($npi!=""){
                                        $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$npi));
                                        if($pmls!=null){
                                            foreach ($pmls as $pml) {
                                                $orgTypeCode = $pml->getColD();
                                                if ($orgTypeArray != "" and !in_array($orgTypeCode, $orgTypeArray)) {
                                                    $orgTypeArray[]=$orgTypeArray;
                                                    $pmlsArray[]=$pml;
                                                }

                                                foreach ($providerTypeOrder as $order) {
                                                    if ($pml->getColD() == $order) {
                                                        $taxonomy_code = $pml->getColF();
                                                        break 2;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    $specialtiesCodes="";

                                    if(count($orgTypeArray)>0) {
                                        foreach ($pmlsArray as $pt) {
                                            $specialtyCodes = $pt->getColE();

                                            if (strlen($specialtyCodes) < 3) {
                                                $specialtyCodes = "0" . $specialtyCodes;
                                            }

                                            if($specialtyCodes!=""){
                                                $specialtiesCodes.=$specialtyCodes."~";
                                            }
                                        }
                                    }

                                    foreach ($address_p as $addr_p){
                                        if($addr_p->getOrganization()->getId()==$organization->getId()){
                                            $sl_record_tracking_number="";
                                            $group_location_track_number=$org_track_number.$addr_p->getFccTrackNumber();

                                            $slrtn=$em->getRepository('App\Entity\ProviderAddrFCCTN')->findOneBy(
                                                array('provider'=>$provider->getId(),'address'=>$addr_p->getId()));
                                            if($slrtn!=null){
                                                $sl_record_tracking_number=$slrtn->getTrackNumber();
                                            }

                                            if($organization_type=="Individual"){
                                                $addr_pro_record="FCC4".substr($track_number,4).$sl_record_tracking_number;
                                            }else{
                                                $addr_pro_record="FCC6".substr($track_number,4).$sl_record_tracking_number;
                                            }

                                            $cell_A='A'.$cont2;$cell_F='F'.$cont2;$cell_B='B'.$cont2;$cell_C='C'.$cont2;$cell_D='D'.$cont2;$cell_S='S'.$cont2;$cell_R='R'.$cont2;$cell_T='T'.$cont2;$cell_Q='Q'.$cont2;
                                            $cell_U='U'.$cont2;$cell_V='V'.$cont2;$cell_Y='Y'.$cont2;$cell_O='O'.$cont2; $cell_AF='AF'.$cont2; $cell_AG='AG'.$cont2;
                                            $cell_E='E'.$cont2;$cell_W = 'W' . $cont2;$cell_X = 'X' . $cont2;
                                            $cell_I='I'.$cont2;$cell_J='J'.$cont2;$cell_K='K'.$cont2;$cell_L='L'.$cont2;$cell_M='M'.$cont2;$cell_N='N'.$cont2;$cell_Z='Z'.$cont2;

                                            $genderAccepted=$provider->getGenderAcceptance();

                                            $is_PCP="N";
                                            if($provider->getHasPcp()){
                                                $is_PCP="Y";
                                            }

                                            $has_wheelchairs="";
                                            if($addr_p->getWheelchair()==1){
                                                $has_wheelchairs="Y";
                                            }

                                            if($addr_p->getWheelchair()==0){
                                                $has_wheelchairs="N";
                                            }

                                            //age min
                                            $age_min=$addr_p->getOfficeAgeLimit();
                                            if($age_min==""){
                                                $age_min="00Y";
                                            }else{
                                                if(strlen($age_min)==1){
                                                    $age_min="0".$age_min."Y";
                                                }else{
                                                    $age_min=$age_min."Y";
                                                }
                                            }

                                            //age max
                                            $age_max=$addr_p->getOfficeAgeLimitMax();
                                            if($age_max==""){
                                                $age_max="00Y";
                                            }else{
                                                if(strlen($age_max)==1){
                                                    $age_max="0".$age_max."Y";
                                                }else{
                                                    $age_max=$age_max."Y";
                                                }
                                            }

                                            $countyObj=$addr_p->getCountyMatch();
                                            $county_code="";
                                            if($countyObj!=null){
                                                $county_code=$countyObj->getCountyCode();
                                            }

                                            $has_weekend="N";
                                            $has_after_hour="N";

                                            $bussinesOurs=$addr_p->getBusinessHours();

                                            if($bussinesOurs!=""){
                                                $bussinesOurs=substr($bussinesOurs,1);
                                                $bussinesOurs=substr($bussinesOurs,0,-1);

                                                $bussinesOurs=str_replace('{','',$bussinesOurs);
                                                $bussinesOurs=str_replace('}','',$bussinesOurs);
                                                $bussinesOurs=str_replace('"','',$bussinesOurs);

                                                $daysHours = explode(",", $bussinesOurs);

                                                $dayActiveSun=explode(":",$daysHours[18])[1];
                                                $dayActiveSat=explode(":",$daysHours[15])[1];
                                                if($dayActiveSun=="true" or $dayActiveSat=="true"){
                                                    $has_weekend="Y";
                                                }

                                                $cont__after_hour=0;
                                                //sunday
                                                $dayActive=explode(":",$daysHours[18])[1];
                                                $dayTill=explode(":",$daysHours[20]);

                                                if($dayActive=="true"){
                                                    $dateTill=$dayTill[1].$dayTill[2];
                                                    $date24Till=$this->convertTo24Hours($dateTill);
                                                    $date24Till=substr($date24Till,0,2);
                                                    $tillsun=intval($date24Till);
                                                    if($tillsun>18){
                                                        $cont__after_hour++;
                                                    }
                                                }

                                                //mon
                                                $dayActive=explode(":",$daysHours[0])[1];
                                                $dayTill=explode(":",$daysHours[2]);

                                                if($dayActive=="true"){
                                                    $dateTill=$dayTill[1].$dayTill[2];
                                                    $date24Till=$this->convertTo24Hours($dateTill);
                                                    $date24Till=substr($date24Till,0,2);
                                                    $till=intval($date24Till);
                                                    if($till>18){
                                                        $cont__after_hour++;
                                                    }
                                                }

                                                //tue
                                                $dayActive=explode(":",$daysHours[3])[1];
                                                $dayTill=explode(":",$daysHours[5]);

                                                if($dayActive=="true"){
                                                    $dateTill=$dayTill[1].$dayTill[2];
                                                    $date24Till=$this->convertTo24Hours($dateTill);
                                                    $date24Till=substr($date24Till,0,2);
                                                    $till=intval($date24Till);
                                                    if($till>18){
                                                        $cont__after_hour++;
                                                    }
                                                }

                                                //wed
                                                $dayActive=explode(":",$daysHours[6])[1];
                                                $dayTill=explode(":",$daysHours[8]);

                                                if($dayActive=="true"){
                                                    $dateTill=$dayTill[1].$dayTill[2];
                                                    $date24Till=$this->convertTo24Hours($dateTill);
                                                    $date24Till=substr($date24Till,0,2);
                                                    $till=intval($date24Till);
                                                    if($till>18){
                                                        $cont__after_hour++;
                                                    }
                                                }

                                                //thu
                                                $dayActive=explode(":",$daysHours[9])[1];
                                                $dayTill=explode(":",$daysHours[11]);

                                                if($dayActive=="true"){
                                                    $dateTill=$dayTill[1].$dayTill[2];
                                                    $date24Till=$this->convertTo24Hours($dateTill);
                                                    $date24Till=substr($date24Till,0,2);
                                                    $till=intval($date24Till);
                                                    if($till>18){
                                                        $cont__after_hour++;
                                                    }
                                                }

                                                //fri
                                                $dayActive=explode(":",$daysHours[12])[1];
                                                $dayTill=explode(":",$daysHours[14]);

                                                if($dayActive=="true"){
                                                    $dateTill=$dayTill[1].$dayTill[2];
                                                    $date24Till=$this->convertTo24Hours($dateTill);
                                                    $date24Till=substr($date24Till,0,2);
                                                    $till=intval($date24Till);
                                                    if($till>18){
                                                        $cont__after_hour++;
                                                    }
                                                }

                                                //sat
                                                $dayActive=explode(":",$daysHours[15])[1];
                                                $dayTill=explode(":",$daysHours[17]);

                                                if($dayActive=="true"){
                                                    $dateTill=$dayTill[1].$dayTill[2];
                                                    $date24Till=$this->convertTo24Hours($dateTill);
                                                    $date24Till=substr($date24Till,0,2);
                                                    $till=intval($date24Till);
                                                    if($till>18){
                                                        $cont__after_hour++;
                                                    }
                                                }
                                            }

                                            if( $cont__after_hour>0){
                                                $has_after_hour="Y";
                                            }

                                            $track_number=strval($track_number);

                                            $hasEHR="N";

                                            if($addr_p->getSupportEHR()==true){
                                                $hasEHR="Y";
                                            }
                                            $addr_pass=true;
                                            if(strpos($addr->getStreet(),'BOX')==true or strpos($addr->getStreet(),'Box')==true or strpos($addr->getStreet(),'box')==true){
                                                $addr_pass=false;
                                            }
                                            if($addr_pass==true) {
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, $addr_pro_record);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_B, $plan_medicaid);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $track_number);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, $start_date );
                                                $npi=$provider->getNpiNumber();
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, $npi);

                                                if ($organization_type != "Individual") {
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $group_location_track_number);
                                                }
                                                if ($organization_type == "Individual") {
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AG, $hasEHR);
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $addr_p->getStreet());
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $addr_p->getSuiteNumber());
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $addr_p->getCity());
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $addr_p->getUsState());
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $addr_p->getZipCode());

                                                    $phone_number = $addr->getPhoneNumber();
                                                    $phone_number = str_replace('-', "", $phone_number);

                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, $phone_number);
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, $county_code);
                                                }

                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_R, "Y");
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Q, $is_PCP);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_S, "N");
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_T, 'B');
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_U, $age_min);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_V, $age_max);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $has_wheelchairs);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AF, $taxonomy_code);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_W, $has_after_hour);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $has_weekend);

                                                if ($specialtiesCodes != "") {
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z, substr($specialtiesCodes, 0, -1));
                                                }

                                                $cont2++;
                                            }
                                        }
                                    }
                                }
                        }
                    }
            }//end main conditions
        }

        $spreadsheet->setActiveSheetIndex(0);*/

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="ahca-report-pg.xls"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    private function convertTo24Hours($date){
        $newDate="";
        if (strpos($date, 'am') == true) {
            $newDate=substr($date,0,-2);

            if(strlen($newDate)<4){
                $newDate='0'.$newDate;
            }

        }else{
            $newDate=substr($date,0,-2);
            if($newDate=="1200"){$newDate="1200";}if($newDate=="1230"){$newDate="1230";}
            if($newDate=="100"){$newDate="1300";}if($newDate=="130"){$newDate="1330";}
            if($newDate=="200"){$newDate="1400";}if($newDate=="230"){$newDate="1430";}
            if($newDate=="300"){$newDate="1500";}if($newDate=="330"){$newDate="1530";}
            if($newDate=="400"){$newDate="1600";}if($newDate=="430"){$newDate="1630";}
            if($newDate=="500"){$newDate="1700";}if($newDate=="530"){$newDate="1730";}
            if($newDate=="600"){$newDate="1800";}if($newDate=="630"){$newDate="1830";}
            if($newDate=="700"){$newDate="1900";}if($newDate=="730"){$newDate="1930";}
            if($newDate=="800"){$newDate="2000";}if($newDate=="830"){$newDate="2030";}
            if($newDate=="900"){$newDate="2100";}if($newDate=="930"){$newDate="2130";}
            if($newDate=="1000"){$newDate="2200";}if($newDate=="1030"){$newDate="2230";}
            if($newDate=="1100"){$newDate="2300";}if($newDate=="1130"){$newDate="2330";}
        }

        return $newDate;
    }

    private function getFacilityNpi($provider){
        $npi="";
        $addrs=$provider->getBillingAddress();

        if(count($addrs)>0){
            foreach ($addrs as $addr){
                if($addr->getIsFacility()==1 and $addr->getLocationNpi()!="" and $addr->getLocationNpi()!=null){
                    $npi.=$addr->getLocationNpi().',';
                }
            }
            $npi=substr($npi,0,-1);
        }else{
            $npi="";
        }

        return $npi;
    }

    private function  getFacilityTypes($provider){
        $facilities="";

        $addrs=$provider->getBillingAddress();

        if(count($addrs)>0){
            foreach ($addrs as $addr){
                if($addr->getIsFacility()==1){
                    $ft=$addr->getFacilityType();
                    if($ft){
                        foreach ($ft as $facilityType){
                            $facilities.=$facilityType->getName().",";
                        }
                    }
                }
            }
            $facilities=substr($facilities,0,-1);
        }else{
            $facilities="";
        }

        return $facilities;
    }

    private function getNpiGroup($provider){
        $npiGroup="";
        $organizations=$provider->getOrganizations();
        if(count($organizations)>0){
            foreach ($organizations as $organization){
                $npiGroup.=$organization->getGroupNpi().",";
            }
            $npiGroup=substr($npiGroup,0,-1);
        }

        return $npiGroup;
    }

    private function getTinOrganizationNumber($provider){
        $tin="";
        $organizations=$provider->getOrganizations();
        if(count($organizations)>0){
            foreach ($organizations as $organization){
                $tin.=$organization->getTinNumber().",";
            }
            $tin=substr($tin,0,-1);
        }

        return $tin;
    }

    private  function getFaxNumber($provider){
        $em=$this->getDoctrine()->getManager();
        $faxs="";

        $organizations=$provider->getOrganizations();
        if(count($organizations)>0){
            foreach ($organizations as $organization){
                $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));
                if($contacts){
                    foreach ($contacts as $contact){
                        if($contact->getFax()!=""){
                            $faxs.=$contact->getFax().",";
                        }
                    }
                }
            }
            $faxs=substr($faxs,0,-1);
        }

        return $faxs;
    }

    private function AddressLanguages($address){
        $em=$this->getDoctrine()->getManager();

        $providers=$address->getProviders();
        $languagesStr="";
        foreach ($providers as $provider){
            $credentialingP=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('provider'=>$provider->getId(),'payer'=>1));
            if($credentialingP!=null){
                $languages= $provider->getLanguages();
                foreach ($languages as $lgn){
                    $languagesStr.=$lgn->getName().",";
                }
            }
        }

        $languagesStr=substr($languagesStr,0,-1);
        return $languagesStr;
    }

    private function AddressFax($address){
        $em=$this->getDoctrine()->getManager();
        $faxs="";

        $organization=$address->getOrganization();
        $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));
        if($contacts){
            foreach ($contacts as $contact){
                if($contact->getFax()!=""){
                    $faxs.=$contact->getFax().",";
                }
            }
        }
        $faxs=substr($faxs,0,-1);

        return $faxs;
    }


    /**
     * @Route("/fcc/report-compare", name="fcc_report_compare")
     */
    public function fccReportCompare(){
        $em=$this->getDoctrine()->getManager();
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');

        $spreadsheet = $reader->load($url.'FCC_PNV.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=1;
        $providers=$em->getRepository('App\Entity\Provider')->findAll();
        $organizations=$em->getRepository('App\Entity\Organization')->findAll();
        $recordTrackingNumberArray=array();

        foreach ($sheetData as $sheet) {
            if($cont>1){
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    $npi=$sheet['G'];

                    $match="No";
                    $matchOrg="No";
                    $cell_X='X'.$cont;
                    $cell_Y='Y'.$cont;
                    $contMatch=0;
                    $contMatchOrg=0;
                    foreach ($providers as $provider){
                        if($provider->getNpiNumber()==$npi){
                            $contMatch++;
                        }
                    }

                    foreach ($organizations as $organization){
                        if($organization->getGroupNpi()==$npi){
                            $contMatchOrg++;
                        }
                    }

                    if($contMatch>0){
                        $match="Yes";
                        $range="A".$cont.":"."X".$cont;
                        $spreadsheet->getActiveSheet()->getStyle($range)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('3fc738');
                    }

                    if($contMatchOrg>0){
                        $recordTrackingNumberArray[]=$sheet['A'];
                        $matchOrg="Yes";
                        $range="A".$cont.":"."Y".$cont;
                        $spreadsheet->getActiveSheet()->getStyle($range)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('2a77a1');
                    }

                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_X, $match);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_Y, $matchOrg);
                }
            }
            $cont++;
        }

        //search for all Org not match
        $existOrgArray=array(1245394832, 1174676019, 1013205061, 1184640625, 1831184316, 1619965795, 1609849025, 1144242504, 1053367847,
1043209638, 1528077104, 1295894301, 1154426955, 1356331383, 1982761086, 1174524375, 1245391291, 1023140381, 1184900631, 1306944855, 1538308606, 1487769683, 1508939208, 1558411496, 1568535946,
1043209638, 1053309765, 1679637961, 1770885527, 1851493027, 1427055417, 1528232618, 1346293511, 1174526743, 1326191156, 1174526743, 1700961646,
1700961646, 1700961646, 1750509360, 1558315812, 1174642516, 1821138819, 1770624066, 1346353067, 1104910868, 1831201482, 1225046550, 1225046550,
1225046550, 1811044324, 1093710303, 1780658153, 1811044324, 1316038714, 1154594950, 1154594950, 1831205814, 1477729895, 1609946524, 1013109065, 1356532584, 1932389319,
1760674170, 1700961646, 1649469008, 1346408721, 1710214416, 1093028607, 1952453656, 1497914246, 1932389319, 1124230461, 1174775662, 1457438384, 1184859035, 1093017055, 1992036230, 1669706891,
1306004734, 1063411874, 1447421383, 1154594950, 1285929943, 1346353067, 1528361797, 1578866562, 1508159773, 1003085945, 1295067817, 1154594950, 1154594950, 1356532584, 1154594950, 1669683751,
1184925281, 1558652586, 1669727699, 1104251651, 1043537160, 1932389319, 1518205152, 1700961646, 1629340260, 1790023851, 1689605362, 1174821086, 1578612883, 1700961646, 1700961646, 1700961646,
1700961646, 1487611844, 1043581903, 1336588813, 1053722462, 1891190047, 1710303896, 1346353067, 1154594950, 1013309657, 1669898136, 1801298286, 1346353067, 1366739419, 1861718496, 1841678141, 1801298286, 1801298286,
1063844660, 1720026628, 1487196010, 1518324276, 1306363783, 1619048600, 1699176842, 1134669427, 1730548793, 1639578834, 1902230683, 1902040983, 1124387261, 1356524417, 1538295860, 1316963754
);

        $noExistArray=array();

        foreach ($organizations as $org){
            $npi=$org->getGroupNpi();
            $has_medicaid=false;
            if($npi!=""){
                if(!in_array($npi, $existOrgArray)){
                    $billing_address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$org->getId()));
                    $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$org->getId()));
                    //verify if the address have medicaid
                    if($billing_address){
                        foreach ($billing_address as $addr){
                            if($addr){
                                if($addr->getGroupMedicaid()!=""){
                                   $has_medicaid=true;
                                }

                            }
                        }
                    }

                    if($providers){
                        foreach ($providers as $provider){
                            $medicaid=$provider->getMedicaid();
                            //$provider=new Provider();
                            if($medicaid!="-" and $medicaid!='LCSW' and $medicaid!="" and $medicaid!="n/a" and $medicaid!="N/A" and $medicaid!="blank" and $medicaid!="00PENDING" and $medicaid!="PENDING"
                            and $medicaid!="submitting application" and $medicaid!="credentialing in process" and $medicaid!="In process"){
                                $has_medicaid=true;
                            }
                        }
                    }

                    if($has_medicaid){
                        $noExistArray[]=$org;
                    }
                }
            }
        }

        foreach ($noExistArray as $noOrg){
            //$noOrg=new Organization();

            $range="A".$cont.":"."Y".$cont;
            $spreadsheet->getActiveSheet()->getStyle($range)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('d457cd');

            $cell_D='D'.$cont; $cell_G='G'.$cont;
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, strtoupper($noOrg->getName()));
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $noOrg->getGroupNpi());

            $cont++;
        }

        $spreadsheet->setActiveSheetIndex(1);
        $sheetData2 = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont2=1;
        foreach ($sheetData2 as $sheet) {
            if($cont2>1){
                if ($sheet['A'] != null or $sheet['A'] != "") {
                     $contTrakingNumber=0;
                    foreach ($recordTrackingNumberArray as $recordTrackingNumber){
                       if($sheet['C']==$recordTrackingNumber){
                           $contTrakingNumber++;
                       }
                    }

                    $cell_AH='AH'.$cont2;
                    if($contTrakingNumber>0){
                        $range="A".$cont2.":"."AG".$cont2;
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AH, "Yes");
                        $spreadsheet->getActiveSheet()->getStyle($range)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('2a77a1');
                    }else{
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AH, "No");
                    }
                }
            }

            $cont2++;
        }

        foreach ($noExistArray as $noOrg){
            $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$noOrg->getId()));
            if($address!=null){
                foreach ($address as $addr){
                    //$addr=new BillingAddress();
                    $cell_H='H'.$cont2; $cell_I='I'.$cont2;$cell_J='J'.$cont2;$cell_K='K'.$cont2;$cell_L='L'.$cont2;$cell_M='M'.$cont2;$cell_N='N'.$cont2;
                    $range="A".$cont2.":"."AG".$cont2;
                    $spreadsheet->getActiveSheet()->getStyle($range)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('d457cd');

                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_H, strtoupper($noOrg->getName()));
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, strtoupper($addr->getStreet()));
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, strtoupper($addr->getSuiteNumber()));
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, strtoupper($addr->getCity()));
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, strtoupper($addr->getUsState()));
                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $addr->getZipCode());
                    $county=$addr->getCountyMatch();
                    if($county!=null){
                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, $county->getCountyCode());
                    }


                    $cont2++;
                }
            }
        }

        $spreadsheet->setActiveSheetIndex(0);
        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="fcc-compare-report.xls"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }


    /**
     * @Route("/fcc/settracknumber", name="fcc_set_track_number")
     */
    public function addAddrOrgFCCTranNumber(){
        $em=$this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App\Entity\Organization')->findAll();

        foreach ($organizations as $organization){
            $cont=1;
            $addresses=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
            if($addresses!=null){
                foreach ($addresses as $address){
                    $fcc_track_number="";
                    if(strlen($cont)==1){
                        $fcc_track_number='00'.$cont;
                    }
                    if(strlen($cont)==2){
                        $fcc_track_number='0'.$cont;
                    }

                    $address->setFccTrackNumber($fcc_track_number);
                    $em->persist($address);
                    $cont++;
                }
                $organization->setAddrsIdTrackNumber($cont);
                $em->flush();
            }
        }
        die();
    }

    /**
     * @Route("/fcc/settracknumber-prov", name="fcc_set_track_number_prov")
    */
    public function addAddrProvFCCTranNumber(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();

        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));

        foreach ($providers as $provider){
            $cont=1;
            $addresses=$provider->getBillingAddress();
            if($addresses!=null){
                $organization=$provider->getOrganization();

                if($addresses!=null){
                    foreach ($addresses as $addr){
                        if($addr->getOrganization()->getId()==$organization->getId()){
                            $providerADDRFCC=new ProviderAddrFCCTN();
                            $providerADDRFCC->setProvider($provider);
                            $providerADDRFCC->setAddress($addr);
                            if(strlen($cont)==1){
                                $fcc_track_number='00'.$cont;
                            }
                            if(strlen($cont)==2){
                                $fcc_track_number='0'.$cont;
                            }
                            $providerADDRFCC->setTrackNumber($fcc_track_number);
                            $em->persist($providerADDRFCC);
                            $cont++;
                        }
                    }
                }

                $cont--;
                $provider->setAddrsIdTrackNumber($cont);
                $em->persist($provider);
                $em->flush();
            }
        }

        die();
    }

    /**
     * @Route("/pnv/report-compare", name="pnv_report_compare")
     */
    public function fcc_pnv_report(){
        set_time_limit(3600);
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');
        $date=date('m/d/Y');

        $spreadsheet = $reader->load($url.'PNV_Template.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $em=$this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0,'valid_npi_number'=>1));
        $cont=2;

        $providerNPIS=[];
        $providersIdsPG=[];
        $providers_specialty=[];
        $organization_specialty=[];
        $organizations_result=[];
        $provider_npi_reult=[];

        $exclude_npis=[];
        $exclude_ids=[]; //providers excludes on a organization
        $sql="SELECT p.provider_id, p.payer_id, provider.npi_number as npi
            FROM  provider_payer_exclude p
            LEFT JOIN provider ON provider.id = p.provider_id";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $excludes_p_ids= $stmt->fetchAll();

        foreach ($excludes_p_ids as $ids_excludes){
            $exclude_npis[]=$ids_excludes['npi'];
        }
        //get exclude organization
        $sql2="SELECT organization.id as organization_id
            FROM  organization_payer_exclude o
            LEFT JOIN organization ON organization.id = o.organization_id
            LEFT JOIN payer ON payer.id = o.payer_id
            WHERE payer.id=4";

        $stmt2 = $conn->prepare($sql2);
        $stmt2->execute();
        $excludes_o_ids= $stmt2->fetchAll();

        foreach ($excludes_o_ids as $org_id){
            $providers_org=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$org_id['organization_id']));
            if($providers_org!=null){
                foreach ($providers_org as $provider){
                    $exclude_ids[]=$provider->getId();
                }
            }
        }

        foreach ($providers as $provider){
            $npi=$provider->getNpiNumber();
            $id=$provider->getId();
            $license="";

            if($npi!="N/A" and $npi!="NOT AVAILABLE" and $npi!="" and !in_array($npi,$exclude_npis) and !in_array($id,$exclude_ids)){
                if(in_array($npi,$providerNPIS)==false){
                    $providerNPIS[]=$npi;
                    $credentialings=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status'=>4, 'npi_number'=>$provider->getId()));
                    //process for build Record Tracking Number
                    $track_number="";
                    $start_date="";

                    $pro_org=$provider->getOrganization();
                    $contCredF=0;
                    $dateFac="";
                    $dateEfecFac="";
                    if($pro_org!=null){
                        $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$pro_org->getId()));
                        if($address!=null){
                            foreach ($address as $addr){
                                $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId(),'credentialing_status'=>4));
                                if($credentialingsF!=null){
                                    foreach ($credentialingsF as $cf){
                                        $dateFac=$cf->getCredentialingAcceptedDate();
                                        $dateEfecFac=$cf->getCredentialingEffectiveDate();
                                        $contCredF++;
                                    }
                                }
                            }
                        }
                    }

                    if($credentialings!=null or $contCredF>0){
                        $dateF="";
                        $year="";
                        $moth="";
                        $day="";
                        $year_f="";
                        $moth_f="";
                        $day_f="";
                        if($credentialings!=null){
                            foreach ($credentialings as $credentialing){
                                $date=$credentialing->getCredentialingEffectiveDate();
                                $date_s_a=$credentialing->getCredentialingAccepted();
                                if($date!=null){
                                    $dateA=explode('/',$date);
                                    $year=$dateA[2];
                                    $moth=$dateA[0];
                                    $day=$dateA[1];
                                }

                                if($date_s_a!=null){
                                    $date_fA=explode('/',$date_s_a);
                                    $year_f=$date_fA[2];
                                    $moth_f=$date_fA[0];
                                    $day_f=$date_fA[1];
                                }
                            }
                        }

                        if($contCredF>0){
                            if($dateFac!=null and $dateFac!=""){
                                $dateAceptedFacility=explode('/',$dateFac); //acepted date for facility credentialing
                                $dateEfecFac_f=explode('/',$dateEfecFac);

                                $cred_date=$dateAceptedFacility[2].$dateAceptedFacility[0].$dateAceptedFacility[1];
                                $start_date=$dateEfecFac_f[2].$dateEfecFac_f[0].$dateEfecFac_f[1];
                                $dateF=substr($dateEfecFac_f[2],2,2).$dateEfecFac_f[0];
                            }
                        }

                        if(strlen($moth)==1){
                            $moth="0".$moth;
                        }

                        if(strlen($day)==1){
                            $day="0".$day;
                        }

                        if($contCredF==0){
                            $dateF=substr($year,2,2).$moth;
                            $start_date=$year.$moth.$day;
                            $cred_date=$year_f.$moth_f.$day_f;
                        }

                        $ID_pro=$provider->getId();
                        $id_length=strlen($ID_pro);
                        if($id_length==1){
                            $ID_pro="0000".$ID_pro;
                        }
                        if($id_length==2){
                            $ID_pro="000".$ID_pro;
                        }
                        if($id_length==3){
                            $ID_pro="00".$ID_pro;
                        }
                        if($id_length==4){
                            $ID_pro="0".$ID_pro;
                        }

                        $track_number="FCC1BSN".$dateF.$ID_pro;
                        if($dateF==""){
                            $track_number=0;
                        }
                    }

                    $providerTypeOrder=['25','26','30','29','31','07','32','81','97'];
                    $npi=$provider->getNpiNumber();

                    $proTypeArray=array();
                    $pmlsArray=array();

                    if($npi!=""){
                        $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$npi,'colW'=>'A'));
                        $current_date = strtotime(date("d-m-Y H:i:00",time()));
                        if($pmls!=null){
                            foreach ($pmls as $pml) {
                                $proTypeCode = $pml->getColD();

                                $dateY=$pml->getColY();
                                $dateYArray=explode('-',$dateY);
                                $dateentry=$dateYArray[2]."-".$dateYArray[1]."-".$dateYArray[0]." 01:00:00";
                                $date_entry = strtotime($dateentry);

                                if($pml->getColL()!="LIMITED" and $current_date < $date_entry){
                                    if ($proTypeCode != "" and !in_array($proTypeCode, $proTypeArray)) {
                                        $proTypeArray[]=$proTypeCode;
                                        $pmlsArray[]=$pml;
                                    }
                                }
                            }
                        }
                    }

                    $providerTypeCodes="";
                    $specialtiesCodes="";
                    $medicaid_code="";
                    $match_row=0;
                    if(count($proTypeArray)>0) {
                        foreach ($providerTypeOrder as $order) {
                            foreach ($pmlsArray as $pt) {
                                if($match_row==0){
                                    if(count($pmlsArray)>1){
                                        if ($pt->getColD() == $order  and $pt->getColL()=="ENROLLMENT") {
                                            $providerTypeCodes = $pt->getColD();
                                            $specialtiesCodes = $pt->getColE();
                                            $medicaid_code = $pt->getColA();
                                            $license=$pt->getColV();
                                            $match_row=1;
                                            break 2;
                                        }
                                        if($providerTypeCodes==""){
                                            if ($pt->getColD() == $order and $pt->getColL()=="REGISTERED") {
                                                $providerTypeCodes = $pt->getColD();
                                                $specialtiesCodes = $pt->getColE();
                                                $medicaid_code = $pt->getColA();
                                                $license=$pt->getColV();
                                                $match_row=1;
                                                break 2;
                                            }
                                        }
                                    }else{
                                        if ($pt->getColD() == $order) {
                                            $providerTypeCodes = $pt->getColD();
                                            $specialtiesCodes = $pt->getColE();
                                            $medicaid_code = $pt->getColA();
                                            $license=$pt->getColV();
                                            $match_row=1;
                                            break 2;
                                        }
                                    }
                                }
                            }

                            if($license==""){
                                foreach ($pmlsArray as $pt) {
                                    if($pt->getColV()!="" and $pt->getColV()!=null){
                                        $license=$pt->getColV();
                                    }
                                }
                            }
                        }

                        if (strlen($providerTypeCodes) == 1) {
                            $providerTypeCodes = "00" . $providerTypeCodes;
                        }
                        if (strlen($providerTypeCodes) == 2) {
                            $providerTypeCodes = "0" . $providerTypeCodes;
                        }
                        if (strlen($specialtiesCodes) < 3) {
                            $specialtiesCodes = "0" . $specialtiesCodes;
                        }

                        $medicaid_length = strlen($medicaid_code);
                        if ($medicaid_length < 9) {
                            $rest = 9 - $medicaid_length;
                            while ($rest > 0) {
                                $medicaid_code = "0" . $medicaid_code;
                                $rest--;
                            }
                        }
                    }

                    $genderCode=$provider->getGender();
                    $genderCode=substr($genderCode,0,1);
                    $genderCode=strtoupper($genderCode);

                    $fein_code="";
                    $organization=$provider->getOrganization();
                    if($organization!=null){
                        $fein_code=$organization->getTinNumber();
                        if($fein_code==""){
                            $fein_code=$encoder->decryptthis($provider->getSocial());
                        }
                    }

                   $fein_code=str_replace("-","",$fein_code);
                   $fein_code=str_replace(" ","",$fein_code);

                    if(strlen($fein_code)>9){
                        $fein_code=substr($fein_code,0,9);
                    }

                    if(strlen($fein_code)==8){
                        $fein_code="0".$fein_code;
                    }
                    if(strlen($fein_code)==7){
                        $fein_code="00".$fein_code;
                    }

                    if($credentialings!=null or $contCredF>0 and count($pmlsArray)>0) {
                        if($track_number!="0" and $track_number!="" and $medicaid_code!=""){
                            $cell_A = 'A' . $cont;$cell_B = 'B' . $cont;
                            $cell_C = 'C' . $cont;$cell_D = 'D' . $cont;
                            $cell_E = 'E' . $cont;$cell_F = 'F' . $cont;
                            $cell_G = 'G' . $cont;$cell_H = 'H' . $cont;
                            $cell_L = 'L' . $cont;$cell_J = 'J' . $cont;
                            $cell_K = 'K' . $cont;$cell_M = 'M' . $cont;
                            $cell_N = 'N' . $cont;$cell_O = 'O' . $cont;
                            $cell_I = 'I' . $cont;

                            $provider_ard=$provider->getProviderApplicationReceiptDate();
                            $pg_log=$em->getRepository('App\Entity\PNVLog')->findOneBy(array('record_traking_number'=>$track_number));

                            if($pg_log==null){
                                if($provider_ard==null or $provider_ard==""){
                                    if($start_date=="20190601"){
                                        $diffDates=['20190522',"20190507","20190403","20190428"];
                                        $pos=rand(0,3);
                                        $provider_application_receipt_date=$diffDates[$pos];
                                    }
                                    if($start_date=="20190701"){
                                        $diffDates=['20190611',"20190623","20190505","20190515"];
                                        $pos=rand(0,3);
                                        $provider_application_receipt_date=$diffDates[$pos];
                                    }
                                    if($start_date=="20190901"){
                                        $diffDates=['20190720',"20190707","20190601","20190614"];
                                        $pos=rand(0,3);
                                        $provider_application_receipt_date=$diffDates[$pos];
                                    }
                                    if($start_date=="20200401"){
                                        $diffDates=['20200322',"20200301","20200227","20200218"];
                                        $pos=rand(0,3);
                                        $provider_application_receipt_date=$diffDates[$pos];
                                    }
                                    if($start_date=="20200501"){
                                        $diffDates=['20200401',"20200430","20200327","20200308"];
                                        $pos=rand(0,3);
                                        $provider_application_receipt_date=$diffDates[$pos];
                                    }
                                    if($start_date=="20201001"){
                                        $diffDates=['20191119',"20191210","20191203","20191105"];
                                        $pos=rand(0,3);
                                        $provider_application_receipt_date=$diffDates[$pos];
                                    }
                                    if($start_date=="20201201"){
                                        $diffDates=['20201115',"20201101","20201020","20201013"];
                                        $pos=rand(0,3);
                                        $provider_application_receipt_date=$diffDates[$pos];
                                    }
                                    if($cred_date=="20210125"){
                                        $diffDates=['20201215',"20201105","20201220","20201113"];
                                        $pos=rand(0,3);
                                        $provider_application_receipt_date=$diffDates[$pos];
                                    }
                                }else{
                                    $sep=explode("/",$provider_ard);
                                    $provider_application_receipt_date=$sep[2].$sep[0].$sep[1];
                                }
                            }else{
                                $provider_application_receipt_date=$pg_log->getAppReceiptDate();
                            }


                            $license=$provider->getStateLic();
                            if($license=="not found" or $license=="NOTFOUND"){ $license=""; }
                            $license=str_replace(" ","",$license);

                            if($license==""){
                                if($providerTypeCodes=="032"){
                                    $license="99999";
                                }
                            }

                            $license=strval($license);
                            $license=trim($license);

                            if(stristr($license, 'ARNP')) {
                                $license=  str_replace("ARNP","APRN",$license);
                            }

                            $has_rbt=false;
                            if(stristr($license, 'RBT')) {
                                $has_rbt=true;
                            }

                            $license_match=false;
                            $cont_license=0;
                            foreach ($pmlsArray as $pt) {
                                if($pt->getColV()!="" and $pt->getColV()!=null){
                                    $license_pml=$pt->getColV();
                                    $cont_license++;
                                    if($license_pml==$license){
                                        $license_match=true;
                                    }
                                }
                            }

                            $not_match_pml=0;
                            $license=str_replace('-',"",$license);
                            $license=trim($license);

                            $first_latter_license=substr($license,0,1);
                            if($first_latter_license!="I" and $first_latter_license!="i" and $license!="n/a" and $license!="N/A" and $license!="notavailable" and $license!="NotAvailable"
                                and $has_rbt==false and $not_match_pml==0 and $license!="" and $providerTypeCodes!="039" and $providerTypeCodes!=""){
                                $degrees=$provider->getDegree();
                                $degreePass=0;
                                if($degrees!=null){
                                    foreach ($degrees as $degree){
                                        if($degree->getId()==20 or $degree->getId()==24 or $degree->getId()==22 or $degree->getId()==30 or $degree->getId()==32){
                                            $degreePass=1;
                                        }
                                    }
                                }

                                if($degreePass==0){
                                    $org1=$provider->getOrganization();
                                    $id_org=$org1->getId();
                                    if(!in_array($id_org,$organizations_result) and $org1->getOrganizationStatus()->getId()==2){
                                        $organizations_result[]=$id_org;
                                    }

                                    $pro_id=$provider->getId();
                                    $npi_r=$provider->getNpiNumber();
                                    if(!in_array($pro_id,$providersIdsPG)){
                                        $providersIdsPG[]=$pro_id;
                                        $provider_npi_reult[]=$npi_r;
                                    }
                                    $npi=$provider->getNpiNumber();

                                    // if record is null create a new Record for log
                                    $is_new_on_report=false;
                                    if($pg_log==null){
                                        $pg_log=new PNVLog();
                                        $pg_log->setRecordTrakingNumber($track_number);
                                        $pg_log->setProviderId($medicaid_code);
                                        $pg_log->setFirstName($provider->getFirstName());
                                        $pg_log->setLastName($provider->getLastName());
                                        $pg_log->setLicenseNumber($license);
                                        $pg_log->setSsnFein($fein_code);
                                        $pg_log->setNpiNumber($npi);

                                        $new_date=date('Ymd');
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $new_date);
                                        $pg_log->setProviderType($providerTypeCodes);
                                        $pg_log->setPrimarySpecialty($specialtiesCodes);
                                        $pg_log->setGender($genderCode);
                                        $pg_log->setAppReceiptDate($provider_application_receipt_date);
                                        $pg_log->setCredentialingDate($cred_date);
                                        $pg_log->setStatus('new');
                                        $is_new_on_report=true;

                                        $em->persist($pg_log);
                                        $em->flush();
                                    }else{
                                        if($pg_log->getStatus()=="new"){
                                            $pg_log->setStatus('active');
                                            $em->persist($pg_log);
                                            $em->flush();
                                        }
                                    }

                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $track_number);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $medicaid_code);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $provider->getFirstName());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $provider->getLastName());
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $license);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $fein_code);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $npi);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $providerTypeCodes);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $specialtiesCodes);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_M, $genderCode);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, "");

                                    if($is_new_on_report==true){
                                        $new_date=date('Ymd');
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $new_date);
                                    }else{
                                        $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, '20210101');
                                    }

                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_O, $cred_date);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $provider_application_receipt_date);
                                    $providers_specialty[$npi]=$specialtiesCodes;
                                    $cont++;

                                    //add data for FCC DATA
                                    $fccData=$em->getRepository('App\Entity\FCCData')->findOneBy(array('npi'=>$npi));
                                    $dataJSON=null;

                                    if($fccData==null){
                                        $fccData=new FCCData();
                                        $fccData->setNpi($npi);
                                        $fccData->setData($dataJSON);
                                        $fccData->setProviderId($provider->getId());
                                        $em->persist($fccData);
                                    }else{
                                        $fccData->setNpi($npi);
                                        $em->persist($fccData);
                                        $fccData->setData($dataJSON);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $em->flush();
        //verify providers terminated from the last report and update the current pnv pg log
        $pg_logs=$em->getRepository('App\Entity\PNVLog')->findAll();
        foreach ($pg_logs as $pg_log){
            if($pg_log->getFirstName()!=""){
                $npi_log=$pg_log->getNpiNumber();

                if($pg_log->getStatus()=='to_remove'){
                    $pg_log->setStatus('removed');
                    $em->persist($pg_log);
                    $em->flush();
                }

                if(!in_array($npi_log,$provider_npi_reult) and $pg_log->getStatus()!='removed'){
                    $cell_A = 'A' . $cont;$cell_B = 'B' . $cont;
                    $cell_C = 'C' . $cont;$cell_D = 'D' . $cont;
                    $cell_E = 'E' . $cont;$cell_F = 'F' . $cont;
                    $cell_G = 'G' . $cont;$cell_H = 'H' . $cont;
                    $cell_L = 'L' . $cont;$cell_J = 'J' . $cont;
                    $cell_K = 'K' . $cont;$cell_M = 'M' . $cont;
                    $cell_N = 'N' . $cont;$cell_O = 'O' . $cont;
                    $cell_I = 'I' . $cont;

                    $date_now = date('d-m-Y');
                    $last_date = strtotime('-3 day', strtotime($date_now));
                    $last_date = date('Ymd', $last_date);

                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $pg_log->getRecordTrakingNumber());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $pg_log->getProviderId());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $pg_log->getFirstName());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $pg_log->getLastName());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $pg_log->getLicenseNumber());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $pg_log->getSsnFein());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $pg_log->getNpiNumber());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $pg_log->getProviderType());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $pg_log->getPrimarySpecialty());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_M, $pg_log->getGender());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, "");
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $pg_log->getStartDate());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_O, $pg_log->getCredentialingDate());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $pg_log->getAppReceiptDate());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $last_date);

                    $cont++;
                    //update the status for the PNV PG log
                    $pg_log->setStatus('to_remove');
                    $em->persist($pg_log);
                    $em->flush();
                }
            }
        }

        $organization_pg_result=[];
        $organization_tn_result=[];

        foreach ($organizations_result as $org_id){
            $organization=$em->getRepository('App\Entity\Organization')->find($org_id);
            $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
            $providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId()));

            $pmls = array();
            $groupNpi=$organization->getGroupNpi();
            if($groupNpi!="") {
                $pmls = $em->getRepository('App\Entity\PML')->findBy(array('colN' => $groupNpi, 'colW' => 'A'));
            }
            if(count($pmls)>0){
                //verify if the org have a provider with cred
                $is_cred=false;
                $contCredF=0;
                $cred_date="";
                $cred_date2="";

                if($address!=null){
                    foreach ($address as $addr){
                        $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId(),'credentialing_status'=>4));
                        if($credentialingsF!=null){
                            $contCredF++;
                        }
                    }
                    if($contCredF>0){
                        foreach ($credentialingsF as $credF){
                            $dateC= explode('/',$credF->getCredentialingAcceptedDate());
                            $dateC2= explode('/',$credF->getCredentialingEffectiveDate());

                            $cred_date2=$dateC[2].$dateC[0].$dateC[1];
                            $year=$dateC[2];
                            $moth1=$dateC[0];
                            $moth="";

                            if($moth1=='01'){
                                $moth='02';
                            }
                            if($moth1=='02'){
                                $moth='03';
                            }
                            if($moth1=='03'){
                                $moth='04';
                            }
                            if($moth1=='04'){
                                $moth='05';
                            }
                            if($moth1=='05'){
                                $moth='06';
                            }
                            if($moth1=='06'){
                                $moth='07';
                            }
                            if($moth1=='07'){
                                $moth='08';
                            }
                            if($moth1=='08'){
                                $moth='09';
                            }
                            if($moth1=='09'){
                                $moth='10';
                            }
                            if($moth1=='10'){
                                $moth='11';
                            }
                            if($moth1=='11'){
                                $moth='12';
                            }
                            if($moth1=='12'){
                                $moth='01';
                                $year=intval($year)+1;
                                $year=strval($year);
                            }

                            $cred_date=$dateC2[2].$dateC2[0]."01";
                        }
                    }else{
                        if($providers!=null){
                            foreach ($providers as $provider){
                                $credentialings_Tem=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status'=>4,'provider'=>$provider->getId()));
                                $datesCExist=[];
                                if($credentialings_Tem!=null){
                                    foreach ($credentialings_Tem as $credeTem){
                                        $dateT=$credeTem->getCredentialingEffectiveDate();
                                        if($dateT!=""){
                                            if(!in_array($dateT,$datesCExist)){
                                                $datesCExist[]=$dateT;
                                            }
                                        }
                                    }
                                }
                                //total of different dates from credentialing providers
                                $totalDates=count($datesCExist);
                                if($totalDates==1){
                                    $dateC1= explode('/',$datesCExist[0]);
                                    $yearT=$dateC1[2];
                                    $mothT=$dateC1[0];
                                    $cred_date=$yearT.$mothT."01";
                                }
                                if($totalDates>1){
                                    $dateC1= explode('/',$datesCExist[0]);
                                    $yearT=$dateC1[2];
                                    $mothT=$dateC1[0];
                                    $cred_date=$yearT.$mothT."01";
                                }
                            }
                        }
                    }
                }

                if(true){
                    $cont_con=1;
                   // $year=date('Y');
                    $year='2020';
                    $dateF=substr($year,2,2);
                    $consecutive="";
                    if($cont_con<10){
                        $consecutive="0".$cont_con;
                    }else{
                        $consecutive=$cont_con;
                    }

                    $track_number="";
                    if($contCredF>0){
                        if($dateFac!=null and $dateFac!=""){
                            $dateA=explode('/',$dateFac);
                            $year=$dateA[2];
                            $moth1=$dateA[0];
                            $day='01';
                            $moth='';

                            if($moth1=='01'){
                                $moth='02';
                            }
                            if($moth1=='02'){
                                $moth='03';
                            }
                            if($moth1=='03'){
                                $moth='04';
                            }
                            if($moth1=='04'){
                                $moth='05';
                            }
                            if($moth1=='05'){
                                $moth='06';
                            }
                            if($moth1=='06'){
                                $moth='07';
                            }
                            if($moth1=='07'){
                                $moth='08';
                            }
                            if($moth1=='08'){
                                $moth='09';
                            }
                            if($moth1=='10'){
                                $moth='11';
                            }
                            if($moth1=='11'){
                                $moth='12';
                            }
                            if($moth1=='12'){
                                $moth='01';
                                $year=intval($year)+1;
                                $year=strval($year);
                            }
                        }
                    }

                    $ID_pro=$organization->getId();
                    $id_length=strlen($ID_pro);
                    if($id_length==1){
                        $ID_pro="0000".$ID_pro;
                    }
                    if($id_length==2){
                        $ID_pro="000".$ID_pro;
                    }
                    if($id_length==3){
                        $ID_pro="00".$ID_pro;
                    }
                    if($id_length==4){
                        $ID_pro="0".$ID_pro;
                    }

                    $type3=false;
                    $orgClasifications=$organization->getOrgSpecialtiesArray();

                    if($orgClasifications!=null){
                        foreach ($orgClasifications as $orgC){
                            if($orgC->getName()=="State Mental Hospital" or $orgC->getName()=="General Hospital"){
                                $type3=true;
                            }
                        }
                    }

                    if($type3==false){
                        $track_number="FCC2BSN".$dateF.$consecutive.$ID_pro;
                    }else{
                        $track_number="FCC3BSN".$dateF.$consecutive.$ID_pro;
                    }

                    $medicaid="";

                    $cell_A = 'A' . $cont;$cell_B = 'B' . $cont;$cell_L = 'L' . $cont;
                    $cell_C = 'C' . $cont;$cell_D = 'D' . $cont;
                    $cell_E = 'E' . $cont;$cell_F = 'F' . $cont;
                    $cell_G = 'G' . $cont;$cell_H = 'H' . $cont;
                    $cell_I = 'I' . $cont;$cell_J = 'J' . $cont;
                    $cell_K = 'K' . $cont;$cell_M = 'M' . $cont;
                    $cell_N = 'N' . $cont;$cell_O = 'O' . $cont;

                    $providerTypeOrder=['01','04','16','68','66','05','77','67','08','14','25','26','30','29','31','07','32','81','97'];
                    $groupNpi=$organization->getGroupNpi();
                    $proTypeArray=array();
                    $pmlsArray=array();

                    if($pmls!=null){
                        foreach ($pmls as $pml) {
                            $proTypeCode = $pml->getColD();
                            if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                $proTypeArray[]=$proTypeArray;
                                $pmlsArray[]=$pml;
                            }
                        }
                    }
                    if(count($proTypeArray)==0){
                        if($providers!=null) {
                            foreach ($providers as $provider) {
                                $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>"A"));
                                if($pmls!=null){
                                    foreach ($pmls as $pml) {
                                        $proTypeCode = $pml->getColD();
                                        if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                            $proTypeArray[]=$proTypeArray;
                                            $pmlsArray[]=$pml;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $pt_code="";
                    $specialty_code="";
                    $medicaid_code="";
                    //if(count($proTypeArray)>0){
                    if(true){
                        foreach ($pmlsArray as $pt){
                            foreach ($providerTypeOrder as $order){
                                if($pt->getColD()==$order){
                                    $pt_code=$pt->getColD();
                                    $specialty_code=$pt->getColE();
                                    $medicaid_code=$pt->getColA();
                                    break 2;
                                }
                            }
                        }

                        if(strlen($pt_code)==1){
                            $pt_code="00".$pt_code;
                        }
                        if(strlen($pt_code)==2){
                            $pt_code="0".$pt_code;
                        }
                        if(strlen($specialty_code)<3){
                            $specialty_code="0".$specialty_code;
                        }

                        $medicaid_length=strlen($medicaid_code);
                        if($medicaid_length<9){
                            $rest=9-$medicaid_length;
                            while ($rest>0){
                                $medicaid_code="0".$medicaid_code;
                                $rest--;
                            }
                        }

                        if($pt_code!=""){
                            $fein_o=$organization->getTinNumber();
                            if($fein_o=="" or $fein_o==null){
                                $t_providers=$em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId()));
                                if($t_providers!=NULL){
                                    foreach ($t_providers as $t_provider){
                                        $fein_o=$encoder->decryptthis($t_provider->getSocial());
                                    }
                                }
                            }

                            $fein_o=str_replace("-","",$fein_o);
                            $fein_o=str_replace(" ","",$fein_o);

                            if(strlen($fein_o)==7){
                                $fein_o="00".$fein_o;
                            }
                            if(strlen($fein_o)==8){
                                $fein_o="0".$fein_o;
                            }
                            if(strlen($fein_o)>9){
                                $fein_o=substr($fein_o,0,9);
                            }

                            $provider_application_receipt_date="";

                            if($start_date=="20190601"){
                                $diffDates=['20190522',"20190507","20190403","20190428"];
                                $pos=rand(0,3);
                                $provider_application_receipt_date=$diffDates[$pos];
                            }
                            if($start_date=="20190701"){
                                $diffDates=['20190611',"20190623","20190505","20190515"];
                                $pos=rand(0,3);
                                $provider_application_receipt_date=$diffDates[$pos];
                            }
                            if($start_date=="20190901"){
                                $diffDates=['20190720',"20190707","20190601","20190614"];
                                $pos=rand(0,3);
                                $provider_application_receipt_date=$diffDates[$pos];
                            }
                            if($start_date=="20200401"){
                                $diffDates=['20200322',"20200301","20200227","20200218"];
                                $pos=rand(0,3);
                                $provider_application_receipt_date=$diffDates[$pos];
                            }
                            if($start_date=="20200501"){
                                $diffDates=['20200401',"20200430","20200327","20200308"];
                                $pos=rand(0,3);
                                $provider_application_receipt_date=$diffDates[$pos];
                            }
                            if($start_date=="20201001"){
                                $diffDates=['20200919',"20200924","20200820","20200816"];
                                $pos=rand(0,3);
                                $provider_application_receipt_date=$diffDates[$pos];
                            }
                            if($start_date=="20210125"){
                                $diffDates=['20201215',"20201105","20201220","20201113"];
                                $pos=rand(0,3);
                                $provider_application_receipt_date=$diffDates[$pos];
                            }

                            $pg_log_1=$em->getRepository('App\Entity\PNVLog')->findOneBy(array('record_traking_number'=>$track_number));
                            // if record is null create a new Record for log
                            if($pg_log_1==null){
                                $pg_log=new PNVLog();
                                $pg_log->setRecordTrakingNumber($track_number);
                                $pg_log->setProviderId($medicaid_code);
                                $pg_log->setLastName($organization->getName());
                                $pg_log->setSsnFein($fein_o);
                                $pg_log->setNpiNumber($organization->getGroupNpi());
                                $pg_log->setStartDate('20210101');
                                $pg_log->setPrimarySpecialty($specialty_code);
                                $pg_log->setProviderType($pt_code);

                                if($type3==true){
                                    $ahca_id="";
                                    $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$org_id));
                                    if($address!=null){
                                        foreach ($address as $addr){
                                            $ahca_id=$addr->getAhcaNumber();
                                            if($ahca_id!=""){
                                                if(strlen($ahca_id)==6){
                                                    $ahca_id="00".$ahca_id;
                                                }
                                                break;
                                            }
                                        }
                                    }

                                    if($start_date=="20200501"){
                                        $diffDates=['20200401',"20200430","20200327","20200308"];
                                        $pos=rand(0,3);
                                        $provider_application_receipt_date=$diffDates[$pos];
                                    }

                                    $pg_log->setAHCAID($ahca_id);
                                    $pg_log->setAppReceiptDate($provider_application_receipt_date);
                                    $pg_log->setCredentialingDate($cred_date2);
                                }

                                $pg_log->setStatus('new');
                                $em->persist($pg_log);
                                $em->flush();
                            }else{
                                if($pg_log_1->getFirstName()!=null){
                                    $pg_log=new PNVLog();
                                    $pg_log->setRecordTrakingNumber($track_number);
                                    $pg_log->setProviderId($medicaid_code);
                                    $pg_log->setLastName($organization->getName());
                                    $pg_log->setLicenseNumber($license);
                                    $pg_log->setSsnFein($fein_o);
                                    $pg_log->setNpiNumber($organization->getGroupNpi());
                                    $pg_log->setStartDate('20210101');
                                    $pg_log->setPrimarySpecialty($specialty_code);
                                    $pg_log->setProviderType($pt_code);
                                    $pg_log->setAppReceiptDate($provider_application_receipt_date);
                                    $pg_log->setCredentialingDate($cred_date);
                                    $pg_log->setStatus('new');

                                    if($type3==true){
                                        $ahca_id="";
                                        $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$org_id));
                                        if($address!=null){
                                            foreach ($address as $addr){
                                                $ahca_id=$addr->getAhcaNumber();
                                                if($ahca_id!=""){
                                                    if(strlen($ahca_id)==6){
                                                        $ahca_id="00".$ahca_id;
                                                    }
                                                    break;
                                                }
                                            }
                                        }

                                        if($start_date=="20200501"){
                                            $diffDates=['20200401',"20200430","20200327","20200308"];
                                            $pos=rand(0,3);
                                            $provider_application_receipt_date=$diffDates[$pos];
                                        }

                                        $pg_log->setAHCAID($ahca_id);
                                        $pg_log->setAppReceiptDate($provider_application_receipt_date);
                                        $pg_log->setCredentialingDate($cred_date2);
                                    }

                                    $em->persist($pg_log);
                                    $em->flush();
                                }
                            }

                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $track_number);
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $medicaid_code);
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, '20210101');

                            if($type3==false){
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $organization->getName());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $organization->getGroupNpi());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_F,$fein_o);
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $specialty_code);
                            }
                            $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $pt_code);

                            if($type3==true){
                                $ahca_id="";
                                $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$org_id));
                                if($address!=null){
                                    foreach ($address as $addr){
                                        $ahca_id=$addr->getAhcaNumber();
                                        if($ahca_id!=""){
                                            if(strlen($ahca_id)==6){
                                                $ahca_id="00".$ahca_id;
                                            }
                                            break;
                                        }
                                    }
                                }

                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $ahca_id);
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_O, $cred_date2);
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $provider_application_receipt_date);

                                if($start_date=="20200501"){
                                    $diffDates=['20200401',"20200430","20200327","20200308"];
                                    $pos=rand(0,3);
                                    $provider_application_receipt_date=$diffDates[$pos];
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $provider_application_receipt_date);
                                }
                            }

                            $organization_pg_result[]=$org_id;
                            $organization_specialty[$org_id]=$specialty_code;
                            $organization_tn_result[]=$track_number;
                            $cont++;
                        }
                    }
                }
            }
        }

        //verify providers terminated from the last report and update the current pnv pg log
        $pg_logs=$em->getRepository('App\Entity\PNVLog')->findAll();
        foreach ($pg_logs as $pg_log){
            if($pg_log->getFirstName()==null){
                $track_number_log=$pg_log->getRecordTrakingNumber();
                if(!in_array($track_number_log,$organization_tn_result) and $pg_log->getStatus()!='removed'){
                    $cell_A = 'A' . $cont;$cell_B = 'B' . $cont;
                    $cell_C = 'C' . $cont;$cell_D = 'D' . $cont;
                    $cell_E = 'E' . $cont;$cell_F = 'F' . $cont;
                    $cell_G = 'G' . $cont;$cell_H = 'H' . $cont;
                    $cell_L = 'L' . $cont;$cell_J = 'J' . $cont;
                    $cell_K = 'K' . $cont;$cell_M = 'M' . $cont;
                    $cell_N = 'N' . $cont;$cell_O = 'O' . $cont;
                    $cell_I = 'I' . $cont;

                    $date_now = date('d-m-Y');
                    $last_date = strtotime('-3 day', strtotime($date_now));
                    $last_date = date('Ymd', $last_date);

                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $pg_log->getRecordTrakingNumber());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $pg_log->getProviderId());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $pg_log->getFirstName());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $pg_log->getLastName());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $pg_log->getLicenseNumber());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $pg_log->getSsnFein());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $pg_log->getNpiNumber());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $pg_log->getProviderType());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $pg_log->getPrimarySpecialty());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, "");
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, '20210101');
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_O, $pg_log->getCredentialingDate());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $pg_log->getAppReceiptDate());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $last_date);
                    $cont++;

                    //update the status for the PNV PG log
                    $pg_log->setStatus('to_remove');
                    $em->persist($pg_log);
                    $em->flush();
                }
            }
        }


        $spreadsheet->setActiveSheetIndex(1);
        $cont2=2;
        $plan_medicaid="100053001~100053002~100053003~100053004~100053005~100053006~100053007~100053008~100053009~100053010~100053011";
        $providerTypeOrder=['01','04','16','68','66','05','77','67','91','08','14','25','26','30','29','31','07','32','81','97'];

        $providers_current_sl_ids=[];
        $record_tns=[];

        foreach ($organization_pg_result as $org_id){
            $organization=$em->getRepository('App\Entity\Organization')->find($org_id);
            $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
            $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0,'organization'=>$organization->getId()));

            $valid_medicaid=true;
            $valid_cred=true;
            $type3 = false;

            //beging main validations
            if($valid_medicaid and $valid_cred){
                $organization_type=$organization->getProviderType();
                $org_track_number="";
                $taxonomy_code="";
                $organization_languages_code="";

                if($providers!=null){
                    $languagesId=[];
                    foreach ($providers as $provider){
                        if(in_array($provider->getId(),$providersIdsPG)==true){
                            $languages=$provider->getLanguages();
                            if($languages!=null){
                                foreach ($languages as $language){
                                    $code=$language->getCode();
                                    if($code!=""){
                                        if(!in_array($code,$languagesId) and $language->getId()!=1){
                                            if($code=='02' or $code=='03' or $code=='04' or $code=='05' or $code=='06' or $code=='07' or $code=='08' or $code=='09'){
                                                $languagesId[]=$code;
                                                $organization_languages_code.=$code."~";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $languages_code=substr($organization_languages_code,0,-1);

                $groupNpi=$organization->getGroupNpi();
                $proTypeArray=array();
                $pmlsArray=array();
                if($groupNpi!=""){
                    $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$groupNpi));
                    if($pmls!=null){
                        foreach ($pmls as $pml) {
                            $proTypeCode = $pml->getColD();
                            if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                $proTypeArray[]=$proTypeArray;
                                $pmlsArray[]=$pml;
                            }
                        }
                    }
                }

                if(count($pmlsArray)==0){
                    if($providers!=null){
                        foreach ($providers as $provider) {
                            if(in_array($provider->getId(),$providersIdsPG)==true){
                                $pmls=$em->getRepository('App\Entity\Pml')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>'A'));
                                if($pmls!=null){
                                    foreach ($pmls as $pml) {
                                        $proTypeCode = $pml->getColD();
                                        if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                            $proTypeArray[]=$proTypeArray;
                                            $pmlsArray[]=$pml;

                                            foreach ($providerTypeOrder as $order) {
                                                if ($pml->getColD() == $order) {
                                                    $taxonomy_code = $pml->getColF();
                                                    break 2;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //f($valid_medicaid and count($pmlsArray)>0){
                if(true){
                    //verify if the org have a provider with cred
                    $is_cred=false;
                    $contCredF=0;
                    $contCredI=0;

                    if($address!=null){
                        foreach ($address as $addr){
                            $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId(),'credentialing_status'=>4));
                            if($credentialingsF!=null){
                                $contCredF++;
                            }
                        }
                        if($contCredF>0){
                            $is_cred=true;
                        }
                    }

                    if($providers!=null and $is_cred==false){
                        foreach ($providers as $provider){
                            $pmls=$em->getRepository('App\Entity\Pml')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>"A"));
                            if($pmls!=null){
                                $credentialings=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status'=>4, 'provider'=>$provider->getId()));
                                if($credentialings!=null){
                                    $contCredI++;
                                }
                            }
                        }
                        if($contCredI>0) {
                            $is_cred=true;
                        }
                    }

                    //if($is_cred){
                    if(true) {
                        $cont_con = 1;
                       // $year = date('Y');
                        $year='2020';
                        $dateF = substr($year, 2, 2);
                        $consecutive = "";
                        if ($cont_con < 10) {
                            $consecutive = "0" . $cont_con;
                        } else {
                            $consecutive = $cont_con;
                        }

                        $track_number = "";
                        $org_track_number = "";
                        $ID_pro = $organization->getId();
                        $id_length = strlen($ID_pro);
                        if ($id_length == 1) {
                            $ID_pro = "0000" . $ID_pro;
                        }
                        if ($id_length == 2) {
                            $ID_pro = "000" . $ID_pro;
                        }
                        if ($id_length == 3) {
                            $ID_pro = "00" . $ID_pro;
                        }
                        if ($id_length == 4) {
                            $ID_pro = "0" . $ID_pro;
                        }

                        $orgClasifications = $organization->getOrgSpecialtiesArray();

                        if ($orgClasifications != null) {
                            foreach ($orgClasifications as $orgC) {
                                if ($orgC->getName() == "State Mental Hospital" or $orgC->getName()=="General Hospital") {
                                    $type3 = true;
                                }
                            }
                        }

                        if ($type3 == false) {
                            $track_number = "FCC2BSN" . $dateF . $consecutive . $ID_pro;
                        }

                        if ($organization_type == "Individual") {
                            $org_track_number = "FCC4BSN" . $dateF . $consecutive . $ID_pro;
                        }

                        if ($organization_type == "Organization" or $organization_type == "Group") {
                            $org_track_number = "FCC5BSN" . $dateF . $consecutive . $ID_pro;
                        }

                        $org_address = $em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $organization->getId()));
                        $npi = $organization->getGroupNpi();

                        $orgTypeArray = array();
                        $pmlsArray = array();
                        if ($npi != "") {
                            $pmls = $em->getRepository('App\Entity\PML')->findBy(array('colN' => $npi,'colW'=>"A"));
                            if ($pmls != null) {
                                foreach ($pmls as $pml) {
                                    $orgTypeCode = $pml->getColD();
                                    if ($orgTypeArray != "" and !in_array($orgTypeCode, $orgTypeArray)) {
                                        $orgTypeArray[] = $orgTypeArray;
                                        $pmlsArray[] = $pml;

                                        foreach ($providerTypeOrder as $order) {
                                            if ($pml->getColD() == $order) {
                                                $taxonomy_code = $pml->getColF();
                                                break 2;
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if($taxonomy_code==""){
                            $org_taxonomies=$em->getRepository('App\Entity\OrganizationTaxonomy')->findBy(array('organization'=>$org_id));

                            if($org_taxonomies!=null){
                                foreach ($org_taxonomies as $org_taxonomy){
                                    $taxonomy_code=$org_taxonomy->getTaxonomy()->getCode();
                                }
                            }
                        }

                        $specialtiesCodes = $organization_specialty[$org_id];

                        if($organization_type!="Individual"){
                        if ($org_address != null) {
                            foreach ($org_address as $addr) {
                                $addr_pass = true;
                                if (strpos($addr->getStreet(), 'BOX') == true or strpos($addr->getStreet(), 'Box') == true or strpos($addr->getStreet(), 'box') == true) {
                                    $addr_pass = false;
                                }

                                if ($addr_pass == true) {
                                    $org_track_number_e = $org_track_number . $addr->getFccTrackNumber();

                                    $cell_A = 'A' . $cont2;
                                    $cell_B = 'B' . $cont2;
                                    $cell_C = 'C' . $cont2;
                                    $cell_H = 'H' . $cont2;
                                    $cell_G = 'G' . $cont2;
                                    $cell_I = 'I' . $cont2;
                                    $cell_J = 'J' . $cont2;
                                    $cell_K = 'K' . $cont2;
                                    $cell_L = 'L' . $cont2;
                                    $cell_M = 'M' . $cont2;
                                    $cell_O = 'O' . $cont2;
                                    $cell_Q = 'Q' . $cont2;
                                    $cell_R = 'R' . $cont2;
                                    $cell_S = 'S' . $cont2;
                                    $cell_T = 'T' . $cont2;
                                    $cell_U = 'U' . $cont2;
                                    $cell_V = 'V' . $cont2;
                                    $cell_AA = 'AA' . $cont2;
                                    $cell_E = 'E' . $cont2;
                                    $cell_Y = 'Y' . $cont2;
                                    $cell_N = 'N' . $cont2;
                                    $cell_W = 'W' . $cont2;
                                    $cell_X = 'X' . $cont2;
                                    $cell_Z = 'Z' . $cont2;
                                    $cell_AF = 'AF' . $cont2;
                                    $cell_F = 'F' . $cont2;
                                    $cell_AG = 'AG' . $cont2;

                                    //age min
                                    $age_min = $addr->getOfficeAgeLimit();
                                    if ($age_min == "") {
                                        $age_min = "00Y";
                                    } else {
                                        if (strlen($age_min) == 1) {
                                            $age_min = "0" . $age_min . "Y";
                                        } else {
                                            $age_min = $age_min . "Y";
                                        }
                                    }

                                    //age max
                                    $age_max = $addr->getOfficeAgeLimitMax();
                                    if ($age_max == "") {
                                        $age_max = "00Y";
                                    } else {
                                        if (strlen($age_max) == 1) {
                                            $age_max = "0" . $age_max . "Y";
                                        } else {
                                            $age_max = $age_max . "Y";
                                        }
                                    }

                                    $has_wheelchairs = "";
                                    if ($addr->getWheelchair() == 1) {
                                        $has_wheelchairs = "Y";
                                    }

                                    if ($addr->getWheelchair() == 0) {
                                        $has_wheelchairs = "N";
                                    }

                                    $countyObj = $addr->getCountyMatch();
                                    $county_code = "";
                                    if ($countyObj != null) {
                                        $county_code = $countyObj->getCountyCode();
                                    }

                                    $has_weekend = "N";
                                    $has_after_hour = "N";

                                    $bussinesOurs = $addr->getBusinessHours();
                                    if ($bussinesOurs != "") {
                                        $bussinesOurs = substr($bussinesOurs, 1);
                                        $bussinesOurs = substr($bussinesOurs, 0, -1);

                                        $bussinesOurs = str_replace('{', '', $bussinesOurs);
                                        $bussinesOurs = str_replace('}', '', $bussinesOurs);
                                        $bussinesOurs = str_replace('"', '', $bussinesOurs);

                                        $daysHours = explode(",", $bussinesOurs);

                                        $dayActiveSun = explode(":", $daysHours[18])[1];
                                        $dayActiveSat = explode(":", $daysHours[15])[1];
                                        if ($dayActiveSun == "true" or $dayActiveSat == "true") {
                                            $has_weekend = "Y";
                                        }

                                        $cont__after_hour = 0;
                                        //sunday
                                        $dayActive = explode(":", $daysHours[18])[1];
                                        $dayTill = explode(":", $daysHours[20]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $tillsun = intval($date24Till);
                                            if ($tillsun > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //mon
                                        $dayActive = explode(":", $daysHours[0])[1];
                                        $dayTill = explode(":", $daysHours[2]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //tue
                                        $dayActive = explode(":", $daysHours[3])[1];
                                        $dayTill = explode(":", $daysHours[5]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //wed
                                        $dayActive = explode(":", $daysHours[6])[1];
                                        $dayTill = explode(":", $daysHours[8]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //thu
                                        $dayActive = explode(":", $daysHours[9])[1];
                                        $dayTill = explode(":", $daysHours[11]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //fri
                                        $dayActive = explode(":", $daysHours[12])[1];
                                        $dayTill = explode(":", $daysHours[14]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //sat
                                        $dayActive = explode(":", $daysHours[15])[1];
                                        $dayTill = explode(":", $daysHours[17]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }
                                    }

                                    if ($cont__after_hour > 0) {
                                        $has_after_hour = "Y";
                                    }

                                    $phone_number = $addr->getPhoneNumber();
                                    $phone_number = str_replace('-', "", $phone_number);
                                    $start_date = "";
                                    $credentialings = $em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status' => 4, 'provider' => $provider->getId()));
                                    $pro_org = $provider->getOrganization();
                                    $contCredF = 0;
                                    $dateFac = "";
                                    $start_date_facility = "";
                                    if ($pro_org != null) {
                                        $address_2 = $em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $pro_org->getId()));
                                        if ($address_2 != null) {
                                            foreach ($address_2 as $addr2) {
                                                $credentialingsF = $em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address' => $addr2->getId(), 'credentialing_status' => 4));
                                                if ($credentialingsF != null) {
                                                    foreach ($credentialingsF as $cf) {
                                                        $dateFac = $cf->getCredentialingAcceptedDate();
                                                        $contCredF++;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if ($contCredF > 0) {
                                        $dateF = "";
                                        $year = "";
                                        $moth = "";
                                        $day = "";
                                        $credentialingsF = $em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address' => $addr->getId(), 'credentialing_status' => 4));
                                        if ($credentialingsF != null) {
                                            foreach ($credentialingsF as $credentialingF) {
                                                $date = $credentialingF->getCredentialingEffectiveDate();
                                                if ($date != null) {
                                                    $dateA = explode('/', $date);
                                                    $year = $dateA[2];
                                                    $moth = $dateA[0];
                                                    $day = $dateA[1];
                                                }
                                            }

                                            if ($contCredF > 0) {
                                                if ($date != null) {
                                                    $dateA = explode('/', $date);
                                                    $year = $dateA[2];
                                                    $moth1 = $dateA[0];
                                                    $day = '01';

                                                    if ($moth1 == '01') {
                                                        $moth == "02";
                                                    }
                                                    if ($moth1 == '02') {
                                                        $moth == "03";
                                                    }
                                                    if ($moth1 == '03') {
                                                        $moth == "04";
                                                    }
                                                    if ($moth1 == '04') {
                                                        $moth == "05";
                                                    }
                                                    if ($moth1 == '05') {
                                                        $moth == "06";
                                                    }
                                                    if ($moth1 == '06') {
                                                        $moth == "07";
                                                    }
                                                    if ($moth1 == '07') {
                                                        $moth == "08";
                                                    }
                                                    if ($moth1 == '08') {
                                                        $moth == "09";
                                                    }
                                                    if ($moth1 == '09') {
                                                        $moth == "10";
                                                    }
                                                    if ($moth1 == '10') {
                                                        $moth == "11";
                                                    }
                                                    if ($moth1 == '11') {
                                                        $moth == "12";
                                                    }
                                                    if ($moth1 == '12') {
                                                        $moth == "01";
                                                        $year = intval($year) + 1;
                                                        $year = strval($year);
                                                    }
                                                }
                                            }
                                        }

                                        if (strlen($moth) == 1) {
                                            $moth = "0" . $moth;
                                        }

                                        if (strlen($day) == 1) {
                                            $day = "0" . $day;
                                        }

                                        $start_date_facility = $year . $moth . $day;
                                    } else {
                                        $providers = $em->getRepository('App\Entity\Provider')->findBy(array('organization' => $organization->getId()));
                                        if ($providers != null) {

                                            foreach ($providers as $provider) {
                                                $credentialings_Tem = $em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status' => 4, 'provider' => $provider->getId()));
                                                $datesCExist = [];
                                                if ($credentialings_Tem != null) {
                                                    foreach ($credentialings_Tem as $credeTem) {
                                                        $dateT = $credeTem->getCredentialingEffectiveDate();
                                                        if ($dateT != "") {
                                                            if (!in_array($dateT, $datesCExist)) {
                                                                $datesCExist[] = $dateT;
                                                            }
                                                        }
                                                    }
                                                }
                                                //total of different dates from credentialing providers
                                                $totalDates = count($datesCExist);
                                                if ($totalDates == 1) {
                                                    $dateC1 = explode('/', $datesCExist[0]);
                                                    $yearT = $dateC1[2];
                                                    $mothT = $dateC1[0];
                                                    $start_date_facility = $yearT . $mothT . "01";
                                                }
                                                if ($totalDates > 1) {
                                                    $dateC1 = explode('/', $datesCExist[0]);
                                                    $yearT = $dateC1[2];
                                                    $mothT = $dateC1[0];
                                                    $start_date_facility = $yearT . $mothT . "01";
                                                }
                                            }
                                        }

                                    }

                                    $hasEHR = "N";
                                    if ($addr->getSupportEHR() == true) {
                                        $hasEHR = "Y";
                                    }

                                    if($type3==false){
                                        $sl_log=$em->getRepository('App\Entity\PNVSLLog')->findOneBy(array('Record_Tracking_Number'=>$org_track_number_e));
                                        // if record is null create a new Record for log
                                        if($sl_log==null){
                                            $sl_log=new PNVSLLog();
                                            $sl_log->setRecordTrackingNumber($org_track_number_e);
                                            $sl_log->setProviderGroupTrackingNumber($track_number);
                                            $sl_log->setNPINumber($organization->getGroupNpi());
                                            $new_date=date('Ymd');
                                            $sl_log->setStartDate($new_date);
                                            $sl_log->setLocationName($organization->getName());
                                            $sl_log->setAddressLine1($addr->getStreet());
                                            $sl_log->setAddressLine2($addr->getSuiteNumber());
                                            $sl_log->setCity($addr->getSuiteNumber());
                                            $sl_log->setState($addr->getUsState());
                                            $sl_log->setZipCode($addr->getZipCode());
                                            $sl_log->setCountyCode($county_code);
                                            $sl_log->setPhoneNumber($phone_number);
                                            $sl_log->setIsPCP("N");
                                            $sl_log->setAcceptingPatients("Y");
                                            $sl_log->setCurrentPatients("N");
                                            $sl_log->setGenderAccepted("B");
                                            $sl_log->setAgeRestrictionLow($age_min);
                                            $sl_log->setAgeRestrictionHigh($age_max);
                                            $sl_log->setHasAfterHours($has_after_hour);
                                            $sl_log->setHasWeekendHolidayHours($has_weekend);
                                            $sl_log->setHasWheelchairAccess($has_wheelchairs);
                                            $sl_log->setSpecialties($specialtiesCodes);
                                            $sl_log->setLanguages($languages_code);
                                            $sl_log->setTaxonomies($taxonomy_code);
                                            $sl_log->setEHR($hasEHR);
                                            $sl_log->setStatus('new');

                                            $em->persist($sl_log);
                                            $em->flush();
                                        }

                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, $org_track_number_e);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_B, $plan_medicaid);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $track_number);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, $organization->getGroupNpi());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_H, $organization->getName());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $addr->getStreet());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $addr->getSuiteNumber());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $addr->getCity());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $addr->getUsState());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $addr->getZipCode());
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, $phone_number);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, '20210101');
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_Q, "N");
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_R, "Y");
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_S, "N");
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_T, "B");
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_U, $age_min);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_V, $age_max);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $has_wheelchairs);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, $county_code);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_W, $has_after_hour);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $has_weekend);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AF, $taxonomy_code);
                                        $spreadsheet->getActiveSheet(1)->setCellValue($cell_AG, $hasEHR);

                                        if ($languages_code != "") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_AA, $languages_code);
                                        }
                                        if ($specialtiesCodes != "") {
                                            $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z, $specialtiesCodes);
                                        }
                                        $cont2++;

                                        $record_tns[]=$org_track_number_e;
                                    }
                                }
                            }
                        }
                    }
                    }
                }

                    $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0,'organization'=>$organization->getId()));
                    foreach ($providers as $provider){
                        if(in_array($provider->getId(),$providersIdsPG)){
                            $track_number="";
                            $credentialings=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status'=>4, 'provider'=>$provider->getId()));

                            $contCredF=0;
                            $dateFac="";
                            if($organization!=null){
                                $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                                if($address!=null){
                                    foreach ($address as $addr){
                                        $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId(),'credentialing_status'=>4));
                                        if($credentialingsF!=null){
                                            foreach ($credentialingsF as $cf){
                                                $dateFac=$cf->getCredentialingEffectiveDate();
                                                $contCredF++;
                                            }
                                        }
                                    }
                                }
                            }

                            $address_p=$provider->getBillingAddress();
                                if($address_p!=null){
                                    $start_date="";
                                    $year="";
                                    $moth="";

                                    foreach ($credentialings as $credentialing){
                                        $date=$credentialing->getCredentialingEffectiveDate();
                                        if($date!=""){
                                            $dateA=explode('/',$date);
                                            $year=$dateA[2];
                                            $moth=$dateA[0];
                                        }
                                    }

                                    if($contCredF>0){
                                        if($dateFac!=null and $dateFac!=""){
                                            $dateA=explode('/',$dateFac);
                                            $start_date=$dateA[2].$dateA[0]."01";
                                            $dateF=substr($dateA[2],2,2).$dateA[0];
                                        }
                                    }

                                    if($contCredF==0){
                                        $start_date=$year.$moth.'01';
                                        $dateF=substr($year,2,2).$moth;
                                    }

                                    $ID_pro=$provider->getId();
                                    $id_length=strlen($ID_pro);
                                    if($id_length==1){
                                        $ID_pro="0000".$ID_pro;
                                    }
                                    if($id_length==2){
                                        $ID_pro="000".$ID_pro;
                                    }
                                    if($id_length==3){
                                        $ID_pro="00".$ID_pro;
                                    }
                                    if($id_length==4){
                                        $ID_pro="0".$ID_pro;
                                    }

                                    $track_number="FCC1BSN".$dateF.$ID_pro;

                                    if($dateF==""){
                                        $track_number=0;
                                    }

                                    $npi=$provider->getNpiNumber();
                                    $orgTypeArray=array();
                                    $pmlsArray=array();
                                    if($npi!=""){
                                        $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$npi,'colW'=>"A"));
                                        if($pmls!=null){
                                            foreach ($pmls as $pml) {
                                                $orgTypeCode = $pml->getColD();
                                                if ($orgTypeArray != "" and !in_array($orgTypeCode, $orgTypeArray)) {
                                                    $orgTypeArray[]=$orgTypeArray;
                                                    $pmlsArray[]=$pml;
                                                }

                                                foreach ($providerTypeOrder as $order) {
                                                    if ($pml->getColD() == $order) {
                                                        $taxonomy_code = $pml->getColF();
                                                        break 2;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    if($taxonomy_code==""){
                                        $pro_taxonomies=$provider->getTaxonomyCodes();

                                        if($pro_taxonomies!=null){
                                            foreach ($pro_taxonomies as $pro_taxonomy){
                                                $taxonomy_code=$pro_taxonomy->getCode();
                                            }
                                        }
                                    }

                                    $specialtiesCodes=$providers_specialty[$provider->getNpiNumber()];
                                    foreach ($address_p as $addr_p){
                                        if($addr_p->getOrganization()->getId()==$organization->getId()){
                                            $sl_record_tracking_number="";
                                            $group_location_track_number=$org_track_number.$addr_p->getFccTrackNumber();

                                            $slrtn=$em->getRepository('App\Entity\ProviderAddrFCCTN')->findOneBy(
                                                array('provider'=>$provider->getId(),'address'=>$addr_p->getId()));
                                            if($slrtn!=null){
                                                $sl_record_tracking_number=$slrtn->getTrackNumber();
                                            }

                                            if($organization_type=="Individual"){
                                                $addr_pro_record="FCC4".substr($track_number,4).$sl_record_tracking_number;
                                            }else{
                                                $addr_pro_record="FCC6".substr($track_number,4).$sl_record_tracking_number;
                                            }

                                            if($type3==true){
                                                $addr_pro_record="FCC4".substr($track_number,4).$sl_record_tracking_number;
                                            }

                                            $cell_A='A'.$cont2;$cell_F='F'.$cont2;$cell_B='B'.$cont2;$cell_C='C'.$cont2;$cell_D='D'.$cont2;$cell_S='S'.$cont2;$cell_R='R'.$cont2;$cell_T='T'.$cont2;$cell_Q='Q'.$cont2;
                                            $cell_U='U'.$cont2;$cell_V='V'.$cont2;$cell_Y='Y'.$cont2;$cell_O='O'.$cont2; $cell_AF='AF'.$cont2; $cell_AG='AG'.$cont2;$cell_H='H'.$cont2;
                                            $cell_E='E'.$cont2;$cell_W = 'W' . $cont2;$cell_X = 'X' . $cont2;$cell_G='G'.$cont2;
                                            $cell_I='I'.$cont2;$cell_J='J'.$cont2;$cell_K='K'.$cont2;$cell_L='L'.$cont2;$cell_M='M'.$cont2;$cell_N='N'.$cont2;$cell_Z='Z'.$cont2;

                                            $is_PCP="N";
                                            if($provider->getHasPcp()){
                                                $is_PCP="Y";
                                            }

                                            $has_wheelchairs="";
                                            if($addr_p->getWheelchair()==1){
                                                $has_wheelchairs="Y";
                                            }

                                            if($addr_p->getWheelchair()==0){
                                                $has_wheelchairs="N";
                                            }

                                            //age min
                                            $age_min=$addr_p->getOfficeAgeLimit();
                                            if($age_min==""){
                                                $age_min="00Y";
                                            }else{
                                                if(strlen($age_min)==1){
                                                    $age_min="0".$age_min."Y";
                                                }else{
                                                    $age_min=$age_min."Y";
                                                }
                                            }

                                            //age max
                                            $age_max=$addr_p->getOfficeAgeLimitMax();
                                            if($age_max==""){
                                                $age_max="00Y";
                                            }else{
                                                if(strlen($age_max)==1){
                                                    $age_max="0".$age_max."Y";
                                                }else{
                                                    $age_max=$age_max."Y";
                                                }
                                            }

                                            $countyObj=$addr_p->getCountyMatch();
                                            $county_code="";
                                            if($countyObj!=null){
                                                $county_code=$countyObj->getCountyCode();
                                            }

                                            $has_weekend="N";
                                            $has_after_hour="N";

                                            $bussinesOurs=$addr_p->getBusinessHours();

                                            if($bussinesOurs!=""){
                                                $bussinesOurs=substr($bussinesOurs,1);
                                                $bussinesOurs=substr($bussinesOurs,0,-1);

                                                $bussinesOurs=str_replace('{','',$bussinesOurs);
                                                $bussinesOurs=str_replace('}','',$bussinesOurs);
                                                $bussinesOurs=str_replace('"','',$bussinesOurs);

                                                $daysHours = explode(",", $bussinesOurs);

                                                $dayActiveSun=explode(":",$daysHours[18])[1];
                                                $dayActiveSat=explode(":",$daysHours[15])[1];
                                                if($dayActiveSun=="true" or $dayActiveSat=="true"){
                                                    $has_weekend="Y";
                                                }

                                                $cont__after_hour=0;
                                                //sunday
                                                $dayActive=explode(":",$daysHours[18])[1];
                                                $dayTill=explode(":",$daysHours[20]);

                                                if($dayActive=="true"){
                                                    $dateTill=$dayTill[1].$dayTill[2];
                                                    $date24Till=$this->convertTo24Hours($dateTill);
                                                    $date24Till=substr($date24Till,0,2);
                                                    $tillsun=intval($date24Till);
                                                    if($tillsun>18){
                                                        $cont__after_hour++;
                                                    }
                                                }

                                                //mon
                                                $dayActive=explode(":",$daysHours[0])[1];
                                                $dayTill=explode(":",$daysHours[2]);

                                                if($dayActive=="true"){
                                                    $dateTill=$dayTill[1].$dayTill[2];
                                                    $date24Till=$this->convertTo24Hours($dateTill);
                                                    $date24Till=substr($date24Till,0,2);
                                                    $till=intval($date24Till);
                                                    if($till>18){
                                                        $cont__after_hour++;
                                                    }
                                                }

                                                //tue
                                                $dayActive=explode(":",$daysHours[3])[1];
                                                $dayTill=explode(":",$daysHours[5]);

                                                if($dayActive=="true"){
                                                    $dateTill=$dayTill[1].$dayTill[2];
                                                    $date24Till=$this->convertTo24Hours($dateTill);
                                                    $date24Till=substr($date24Till,0,2);
                                                    $till=intval($date24Till);
                                                    if($till>18){
                                                        $cont__after_hour++;
                                                    }
                                                }

                                                //wed
                                                $dayActive=explode(":",$daysHours[6])[1];
                                                $dayTill=explode(":",$daysHours[8]);

                                                if($dayActive=="true"){
                                                    $dateTill=$dayTill[1].$dayTill[2];
                                                    $date24Till=$this->convertTo24Hours($dateTill);
                                                    $date24Till=substr($date24Till,0,2);
                                                    $till=intval($date24Till);
                                                    if($till>18){
                                                        $cont__after_hour++;
                                                    }
                                                }

                                                //thu
                                                $dayActive=explode(":",$daysHours[9])[1];
                                                $dayTill=explode(":",$daysHours[11]);

                                                if($dayActive=="true"){
                                                    $dateTill=$dayTill[1].$dayTill[2];
                                                    $date24Till=$this->convertTo24Hours($dateTill);
                                                    $date24Till=substr($date24Till,0,2);
                                                    $till=intval($date24Till);
                                                    if($till>18){
                                                        $cont__after_hour++;
                                                    }
                                                }

                                                //fri
                                                $dayActive=explode(":",$daysHours[12])[1];
                                                $dayTill=explode(":",$daysHours[14]);

                                                if($dayActive=="true"){
                                                    $dateTill=$dayTill[1].$dayTill[2];
                                                    $date24Till=$this->convertTo24Hours($dateTill);
                                                    $date24Till=substr($date24Till,0,2);
                                                    $till=intval($date24Till);
                                                    if($till>18){
                                                        $cont__after_hour++;
                                                    }
                                                }

                                                //sat
                                                $dayActive=explode(":",$daysHours[15])[1];
                                                $dayTill=explode(":",$daysHours[17]);

                                                if($dayActive=="true"){
                                                    $dateTill=$dayTill[1].$dayTill[2];
                                                    $date24Till=$this->convertTo24Hours($dateTill);
                                                    $date24Till=substr($date24Till,0,2);
                                                    $till=intval($date24Till);
                                                    if($till>18){
                                                        $cont__after_hour++;
                                                    }
                                                }
                                            }

                                            if( $cont__after_hour>0){
                                                $has_after_hour="Y";
                                            }

                                            $track_number=strval($track_number);

                                            $hasEHR="N";

                                            if($addr_p->getSupportEHR()==true){
                                                $hasEHR="Y";
                                            }
                                            $addr_pass=true;
                                            if(strpos($addr->getStreet(),'BOX')==true or strpos($addr->getStreet(),'Box')==true or strpos($addr->getStreet(),'box')==true){
                                                $addr_pass=false;
                                            }
                                            if($addr_pass==true) {
                                                $sl_log=$em->getRepository('App\Entity\PNVSLLog')->findOneBy(array('Record_Tracking_Number'=>$addr_pro_record));
                                                // if record is null create a new Record for log
                                                if($sl_log==null){
                                                    $sl_log=new PNVSLLog();
                                                    $sl_log->setRecordTrackingNumber($org_track_number_e);
                                                    $sl_log->setProviderGroupTrackingNumber($track_number);

                                                    $sl_log->setNPINumber($npi);
                                                    $new_date=date('Ymd');
                                                    $sl_log->setStartDate($new_date);

                                                    if ($organization_type != "Individual" and $type3==false) {
                                                        $sl_log->setGroupLocationTrackingNumber($group_location_track_number);
                                                    }
                                                    if ($organization_type == "Individual" or $type3==true) {
                                                        $sl_log->setAddressLine1($addr_p->getStreet());
                                                        $sl_log->setAddressLine2($addr_p->getSuiteNumber());
                                                        $sl_log->setCity($addr_p->getSuiteNumber());
                                                        $sl_log->setState($addr_p->getUsState());
                                                        $sl_log->setZipCode($addr_p->getZipCode());
                                                        $sl_log->setPhoneNumber($phone_number);
                                                        $sl_log->setCountyCode($county_code);
                                                        $sl_log->setEHR($hasEHR);

                                                        $place_name="";
                                                        if($provider->getOrganization()->getBdaName()!=""){
                                                            $place_name=$provider->getOrganization()->getBdaName();
                                                        }else{
                                                            $place_name=$provider->getOrganization()->getName();
                                                        }

                                                        $sl_log->setLocationName($place_name);
                                                    }

                                                    $sl_log->setIsPCP("N");
                                                    $sl_log->setAcceptingPatients("Y");
                                                    $sl_log->setCurrentPatients("N");
                                                    $sl_log->setGenderAccepted("B");
                                                    $sl_log->setAgeRestrictionLow($age_min);
                                                    $sl_log->setAgeRestrictionHigh($age_max);
                                                    $sl_log->setHasAfterHours($has_after_hour);
                                                    $sl_log->setHasWeekendHolidayHours($has_weekend);
                                                    $sl_log->setHasWheelchairAccess($has_wheelchairs);
                                                    if ($specialtiesCodes != "") {
                                                        $sl_log->setSpecialties($specialtiesCodes);
                                                    }
                                                    $sl_log->setLanguages($languages_code);
                                                    $sl_log->setTaxonomies($taxonomy_code);
                                                    $sl_log->setStatus('new');

                                                    $em->persist($sl_log);
                                                    $em->flush();
                                                }
                                                else{
                                                    if($sl_log->getStatus()=="new"){
                                                        $sl_log->setStatus('active');
                                                        $em->persist($sl_log);
                                                        $em->flush();
                                                    }

                                                }

                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, $addr_pro_record);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_B, $plan_medicaid);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $track_number);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, '20210101' );
                                                $npi=$provider->getNpiNumber();
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, $npi);

                                                if ($organization_type != "Individual" and $type3==false) {
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_D, $group_location_track_number);
                                                }
                                                if ($organization_type == "Individual" or $type3==true) {
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_AG, $hasEHR);
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $addr_p->getStreet());
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $addr_p->getSuiteNumber());
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $addr_p->getCity());
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $addr_p->getUsState());
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $addr_p->getZipCode());

                                                    $phone_number = $addr->getPhoneNumber();
                                                    $phone_number = str_replace('-', "", $phone_number);

                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, $phone_number);
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, $county_code);

                                                    $place_name="";
                                                    if($provider->getOrganization()->getBdaName()!=""){
                                                        $place_name=$provider->getOrganization()->getBdaName();
                                                    }else{
                                                        $place_name=$provider->getOrganization()->getName();
                                                    }

                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_H, $place_name);
                                                }

                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_R, "Y");
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Q, $is_PCP);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_S, "N");
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_T, 'B');
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_U, $age_min);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_V, $age_max);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $has_wheelchairs);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AF, $taxonomy_code);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_W, $has_after_hour);
                                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $has_weekend);

                                                if ($specialtiesCodes != "") {
                                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z, $specialtiesCodes);
                                                }

                                                $record_tns[]=$addr_pro_record;

                                                $cont2++;

                                                $pro_id=$provider->getId();
                                                if(!in_array($pro_id,$providers_current_sl_ids)){
                                                    $providers_current_sl_ids[]=$pro_id;
                                                }

                                            }
                                        }
                                    }
                                }
                        }
                    }
            }//end main conditions
        }

        foreach ($providersIdsPG as $provider_id){
            if(!in_array($provider_id,$providers_current_sl_ids)){
                $provider=$em->getRepository('App\Entity\Provider')->find($provider_id);
                $track_number="";
                $credentialings=$em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status'=>4, 'provider'=>$provider->getId()));

                $contCredF=0;
                $dateFac="";
                $organization=$provider->getOrganization();
                if($organization!=null){
                    $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                    if($address!=null){
                        foreach ($address as $addr){
                            $credentialingsF=$em->getRepository('App\Entity\BillingAddressCvo')->findBy(array('billing_address'=>$addr->getId(),'credentialing_status'=>4));
                            if($credentialingsF!=null){
                                foreach ($credentialingsF as $cf){
                                    $dateFac=$cf->getCredentialingEffectiveDate();
                                    $contCredF++;
                                }
                            }
                        }
                    }
                }

                $address_p=$provider->getBillingAddress();
                if($address_p!=null){
                    $start_date="";
                    $year="";
                    $moth="";

                    foreach ($credentialings as $credentialing){
                        $date=$credentialing->getCredentialingEffectiveDate();
                        if($date!=""){
                            $dateA=explode('/',$date);
                            $year=$dateA[2];
                            $moth=$dateA[0];
                        }
                    }

                    if($contCredF>0){
                        if($dateFac!=null and $dateFac!=""){
                            $dateA=explode('/',$dateFac);
                            $start_date=$dateA[2].$dateA[0]."01";
                            $dateF=substr($dateA[2],2,2).$dateA[0];
                        }
                    }

                    if($contCredF==0){
                        $start_date=$year.$moth.'01';
                        $dateF=substr($year,2,2).$moth;
                    }

                    $ID_pro=$provider->getId();
                    $id_length=strlen($ID_pro);
                    if($id_length==1){
                        $ID_pro="0000".$ID_pro;
                    }
                    if($id_length==2){
                        $ID_pro="000".$ID_pro;
                    }
                    if($id_length==3){
                        $ID_pro="00".$ID_pro;
                    }
                    if($id_length==4){
                        $ID_pro="0".$ID_pro;
                    }

                    $track_number="FCC1BSN".$dateF.$ID_pro;

                    if($dateF==""){
                        $track_number=0;
                    }

                    $npi=$provider->getNpiNumber();

                    $orgTypeArray=array();
                    $pmlsArray=array();
                    if($npi!=""){
                        $pmls=$em->getRepository('App\Entity\PML')->findBy(array('colN'=>$npi,'colW'=>"A"));
                        if($pmls!=null){
                            foreach ($pmls as $pml) {
                                $orgTypeCode = $pml->getColD();
                                if ($orgTypeArray != "" and !in_array($orgTypeCode, $orgTypeArray)) {
                                    $orgTypeArray[]=$orgTypeArray;
                                    $pmlsArray[]=$pml;
                                }

                                foreach ($providerTypeOrder as $order) {
                                    if ($pml->getColD() == $order) {
                                        $taxonomy_code = $pml->getColF();
                                        break 2;
                                    }
                                }
                            }
                        }
                    }

                    if($taxonomy_code==""){
                        $pro_taxonomies=$provider->getTaxonomyCodes();

                        if($pro_taxonomies!=null){
                            foreach ($pro_taxonomies as $pro_taxonomy){
                                $taxonomy_code=$pro_taxonomy->getCode();
                            }
                        }
                    }
                    $specialtiesCodes=$providers_specialty[$provider->getNpiNumber()];

                    foreach ($address_p as $addr_p){
                        if($addr_p->getOrganization()->getId()==$organization->getId()){
                            $sl_record_tracking_number="";

                            $slrtn=$em->getRepository('App\Entity\ProviderAddrFCCTN')->findOneBy(
                                array('provider'=>$provider->getId(),'address'=>$addr_p->getId()));
                            if($slrtn!=null){
                                $sl_record_tracking_number=$slrtn->getTrackNumber();
                            }

                            $addr_pro_record="FCC4".substr($track_number,4).$sl_record_tracking_number;

                            $cell_A='A'.$cont2;$cell_F='F'.$cont2;$cell_B='B'.$cont2;$cell_C='C'.$cont2;$cell_D='D'.$cont2;$cell_S='S'.$cont2;$cell_R='R'.$cont2;$cell_T='T'.$cont2;$cell_Q='Q'.$cont2;
                            $cell_U='U'.$cont2;$cell_V='V'.$cont2;$cell_Y='Y'.$cont2;$cell_O='O'.$cont2; $cell_AF='AF'.$cont2; $cell_AG='AG'.$cont2;
                            $cell_E='E'.$cont2;$cell_W = 'W' . $cont2;$cell_X = 'X' . $cont2; $cell_G='G'.$cont2;
                            $cell_I='I'.$cont2;$cell_J='J'.$cont2;$cell_K='K'.$cont2;$cell_L='L'.$cont2;$cell_M='M'.$cont2;$cell_N='N'.$cont2;$cell_Z='Z'.$cont2;

                            $is_PCP="N";
                            if($provider->getHasPcp()){
                                $is_PCP="Y";
                            }

                            $has_wheelchairs="";
                            if($addr_p->getWheelchair()==1){
                                $has_wheelchairs="Y";
                            }

                            if($addr_p->getWheelchair()==0){
                                $has_wheelchairs="N";
                            }

                            //age min
                            $age_min=$addr_p->getOfficeAgeLimit();
                            if($age_min==""){
                                $age_min="00Y";
                            }else{
                                if(strlen($age_min)==1){
                                    $age_min="0".$age_min."Y";
                                }else{
                                    $age_min=$age_min."Y";
                                }
                            }

                            //age max
                            $age_max=$addr_p->getOfficeAgeLimitMax();
                            if($age_max==""){
                                $age_max="00Y";
                            }else{
                                if(strlen($age_max)==1){
                                    $age_max="0".$age_max."Y";
                                }else{
                                    $age_max=$age_max."Y";
                                }
                            }

                            $countyObj=$addr_p->getCountyMatch();
                            $county_code="";
                            if($countyObj!=null){
                                $county_code=$countyObj->getCountyCode();
                            }

                            $has_weekend="N";
                            $has_after_hour="N";

                            $bussinesOurs=$addr_p->getBusinessHours();

                            if($bussinesOurs!=""){
                                $bussinesOurs=substr($bussinesOurs,1);
                                $bussinesOurs=substr($bussinesOurs,0,-1);

                                $bussinesOurs=str_replace('{','',$bussinesOurs);
                                $bussinesOurs=str_replace('}','',$bussinesOurs);
                                $bussinesOurs=str_replace('"','',$bussinesOurs);

                                $daysHours = explode(",", $bussinesOurs);

                                $dayActiveSun=explode(":",$daysHours[18])[1];
                                $dayActiveSat=explode(":",$daysHours[15])[1];
                                if($dayActiveSun=="true" or $dayActiveSat=="true"){
                                    $has_weekend="Y";
                                }

                                $cont__after_hour=0;
                                //sunday
                                $dayActive=explode(":",$daysHours[18])[1];
                                $dayTill=explode(":",$daysHours[20]);

                                if($dayActive=="true"){
                                    $dateTill=$dayTill[1].$dayTill[2];
                                    $date24Till=$this->convertTo24Hours($dateTill);
                                    $date24Till=substr($date24Till,0,2);
                                    $tillsun=intval($date24Till);
                                    if($tillsun>18){
                                        $cont__after_hour++;
                                    }
                                }

                                //mon
                                $dayActive=explode(":",$daysHours[0])[1];
                                $dayTill=explode(":",$daysHours[2]);

                                if($dayActive=="true"){
                                    $dateTill=$dayTill[1].$dayTill[2];
                                    $date24Till=$this->convertTo24Hours($dateTill);
                                    $date24Till=substr($date24Till,0,2);
                                    $till=intval($date24Till);
                                    if($till>18){
                                        $cont__after_hour++;
                                    }
                                }

                                //tue
                                $dayActive=explode(":",$daysHours[3])[1];
                                $dayTill=explode(":",$daysHours[5]);

                                if($dayActive=="true"){
                                    $dateTill=$dayTill[1].$dayTill[2];
                                    $date24Till=$this->convertTo24Hours($dateTill);
                                    $date24Till=substr($date24Till,0,2);
                                    $till=intval($date24Till);
                                    if($till>18){
                                        $cont__after_hour++;
                                    }
                                }

                                //wed
                                $dayActive=explode(":",$daysHours[6])[1];
                                $dayTill=explode(":",$daysHours[8]);

                                if($dayActive=="true"){
                                    $dateTill=$dayTill[1].$dayTill[2];
                                    $date24Till=$this->convertTo24Hours($dateTill);
                                    $date24Till=substr($date24Till,0,2);
                                    $till=intval($date24Till);
                                    if($till>18){
                                        $cont__after_hour++;
                                    }
                                }

                                //thu
                                $dayActive=explode(":",$daysHours[9])[1];
                                $dayTill=explode(":",$daysHours[11]);

                                if($dayActive=="true"){
                                    $dateTill=$dayTill[1].$dayTill[2];
                                    $date24Till=$this->convertTo24Hours($dateTill);
                                    $date24Till=substr($date24Till,0,2);
                                    $till=intval($date24Till);
                                    if($till>18){
                                        $cont__after_hour++;
                                    }
                                }

                                //fri
                                $dayActive=explode(":",$daysHours[12])[1];
                                $dayTill=explode(":",$daysHours[14]);

                                if($dayActive=="true"){
                                    $dateTill=$dayTill[1].$dayTill[2];
                                    $date24Till=$this->convertTo24Hours($dateTill);
                                    $date24Till=substr($date24Till,0,2);
                                    $till=intval($date24Till);
                                    if($till>18){
                                        $cont__after_hour++;
                                    }
                                }

                                //sat
                                $dayActive=explode(":",$daysHours[15])[1];
                                $dayTill=explode(":",$daysHours[17]);

                                if($dayActive=="true"){
                                    $dateTill=$dayTill[1].$dayTill[2];
                                    $date24Till=$this->convertTo24Hours($dateTill);
                                    $date24Till=substr($date24Till,0,2);
                                    $till=intval($date24Till);
                                    if($till>18){
                                        $cont__after_hour++;
                                    }
                                }
                            }

                            if( $cont__after_hour>0){
                                $has_after_hour="Y";
                            }

                            $track_number=strval($track_number);

                            $hasEHR="N";

                            if($addr_p->getSupportEHR()==true){
                                $hasEHR="Y";
                            }
                            $addr_pass=true;
                            $street_check=$addr->getStreet();
                            if(stristr($street_check, 'BOX') or stristr($street_check, 'Box') or stristr($street_check, 'box')) {
                                $addr_pass=false;
                            }

                            if($addr_pass==true) {
                                $phone_number = $addr->getPhoneNumber();
                                $phone_number = str_replace('-', "", $phone_number);
                                $npi=$provider->getNpiNumber();

                                $sl_log=$em->getRepository('App\Entity\PNVSLLog')->findOneBy(array('Record_Tracking_Number'=>$addr_pro_record));
                                // if record is null create a new Record for log
                                if($sl_log==null){
                                    $sl_log=new PNVSLLog();
                                    $sl_log->setRecordTrackingNumber($addr_pro_record);
                                    $sl_log->setProviderGroupTrackingNumber($track_number);
                                    $sl_log->setNPINumber($npi);
                                    $new_date=date('Ymd');
                                    $sl_log->setStartDate($new_date);
                                    $sl_log->setAddressLine1($addr_p->getStreet());
                                    $sl_log->setAddressLine2($addr_p->getSuiteNumber());
                                    $sl_log->setCity($addr_p->getSuiteNumber());
                                    $sl_log->setState($addr_p->getUsState());
                                    $sl_log->setZipCode($addr_p->getZipCode());
                                    $sl_log->setPhoneNumber($phone_number);
                                    $sl_log->setCountyCode($county_code);
                                    $sl_log->setEHR($hasEHR);
                                    $sl_log->setIsPCP("N");
                                    $sl_log->setAcceptingPatients("Y");
                                    $sl_log->setCurrentPatients("N");
                                    $sl_log->setGenderAccepted("B");
                                    $sl_log->setAgeRestrictionLow($age_min);
                                    $sl_log->setAgeRestrictionHigh($age_max);
                                    $sl_log->setHasAfterHours($has_after_hour);
                                    $sl_log->setHasWeekendHolidayHours($has_weekend);
                                    $sl_log->setHasWheelchairAccess($has_wheelchairs);
                                    $sl_log->setSpecialties($specialtiesCodes);
                                    $sl_log->setLanguages($languages_code);
                                    $sl_log->setTaxonomies($taxonomy_code);
                                    $sl_log->setStatus('new');
                                    $em->persist($sl_log);
                                    $em->flush();
                                }

                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_A, $addr_pro_record);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_B, $plan_medicaid);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_C, $track_number);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_F, '20210101' );

                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_E, $npi);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AG, $hasEHR);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_I, $addr_p->getStreet());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_J, $addr_p->getSuiteNumber());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_K, $addr_p->getCity());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_L, $addr_p->getUsState());
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_M, $addr_p->getZipCode());

                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_O, $phone_number);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_N, $county_code);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_R, "Y");
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Q, $is_PCP);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_S, "N");
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_T, 'B');
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_U, $age_min);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_V, $age_max);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_Y, $has_wheelchairs);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_AF,$taxonomy_code);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_W, $has_after_hour);
                                $spreadsheet->getActiveSheet(1)->setCellValue($cell_X, $has_weekend);

                                if ($specialtiesCodes != "") {
                                    $spreadsheet->getActiveSheet(1)->setCellValue($cell_Z, $specialtiesCodes);
                                }

                                $cont2++;

                                $record_tns[]=$addr_pro_record;
                            }
                        }
                    }
                }
            }
        }

        //verify providers terminated from the last report and update the current pnv pg log
        $sl_logs=$em->getRepository('App\Entity\PNVSLLog')->findAll();
        foreach ($sl_logs as $sl_log){

            if($sl_log->getStatus()=="to_remove"){
                $sl_log->setStatus('removed');
                $em->persist($sl_log);
                $em->flush();
            }

            $track_number_log=$sl_log->getRecordTrackingNumber();
            if(!in_array($track_number_log,$record_tns) and $sl_log->getStatus()!='removed'){
                $cell_A='A'.$cont2;$cell_F='F'.$cont2;$cell_B='B'.$cont2;$cell_C='C'.$cont2;$cell_D='D'.$cont2;$cell_S='S'.$cont2;$cell_R='R'.$cont2;$cell_T='T'.$cont2;$cell_Q='Q'.$cont2;
                $cell_U='U'.$cont2;$cell_V='V'.$cont2;$cell_Y='Y'.$cont2;$cell_O='O'.$cont2; $cell_AF='AF'.$cont2; $cell_AG='AG'.$cont2;
                $cell_E='E'.$cont2;$cell_W = 'W' . $cont2;$cell_X = 'X' . $cont2; $cell_G='G'.$cont2;$cell_AA='AA'.$cont2;$cell_H='H'.$cont2;
                $cell_I='I'.$cont2;$cell_J='J'.$cont2;$cell_K='K'.$cont2;$cell_L='L'.$cont2;$cell_M='M'.$cont2;$cell_N='N'.$cont2;$cell_Z='Z'.$cont2;

                $date_now = date('d-m-Y');
                $last_date = strtotime('-3 day', strtotime($date_now));
                $last_date = date('Ymd', $last_date);

                $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $sl_log->getRecordTrackingNumber());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, "100053001~100053002~100053003~100053004~100053005~100053006~100053007~100053008~100053009~100053010~100053011");
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $sl_log->getProviderGroupTrackingNumber());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $sl_log->getGroupLocationTrackingNumber());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $sl_log->getNPINumber());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $sl_log->getStartDate());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $last_date);

                $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $sl_log->getLocationName());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $sl_log->getAddressLine1());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $sl_log->getAddressLine2());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $sl_log->getCity());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $sl_log->getState());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_M, $sl_log->getZipCode());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $sl_log->getCountyCode());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_O, $sl_log->getPhoneNumber());

                $spreadsheet->getActiveSheet(0)->setCellValue($cell_Q, $sl_log->getIsPCP() );
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_R, $sl_log->getAcceptingPatients());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_S, $sl_log->getCurrentPatients());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_T, $sl_log->getGenderAccepted());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_U, $sl_log->getAgeRestrictionLow());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_V, $sl_log->getAgeRestrictionHigh());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_W, $sl_log->getHasAfterHours());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_X, $sl_log->getHasWeekendHolidayHours());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_Y, $sl_log->getHasWheelchairAccess());

                $spreadsheet->getActiveSheet(0)->setCellValue($cell_Z, $sl_log->getSpecialties());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AA, $sl_log->getLanguages());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AF, $sl_log->getTaxonomies());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AG, $sl_log->getEHR());

                $sl_log->setStatus('to_remove');
                $em->persist($sl_log);
                $em->flush();

                $cont2++;
            }
        }

        $spreadsheet->setActiveSheetIndex(0);

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="FCC_BSN_PNV_'.date('Ymd').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/update-pnv-data", name="admin_load_pnv_data")
     */
    public function updateData() {
        $em = $this->getDoctrine()->getManager();
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/fcc_pnv_log";
        $fileName="FCC_BSN_PNV_20211114.xlsx";

        $fileToRead = $url."/" . $fileName;

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=1;

        $current_logs=$em->getRepository('App\Entity\PNVLog')->findAll();
        if($current_logs){
            foreach ($current_logs as $item){
                $em->remove($item);
            }
            $em->flush();
        }

        foreach ($sheetData as $sheet) {
            if ($cont > 1) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    $pnv_log=new PNVLog();
                    $pnv_log->setRecordTrakingNumber($sheet['A']);
                    $pnv_log->setProviderId($sheet['B']);
                    $pnv_log->setFirstName($sheet['C']);
                    $pnv_log->setLastName($sheet['D']);
                    $pnv_log->setLicenseNumber($sheet['E']);
                    $pnv_log->setSsnFein($sheet['F']);
                    if($sheet['G']!=""){
                        $pnv_log->setNpiNumber($sheet['G']);
                    }
                    $pnv_log->setStartDate($sheet['H']);
                    $pnv_log->setEndDate($sheet['I']);
                    $pnv_log->setProviderType($sheet['J']);
                    $pnv_log->setPrimarySpecialty($sheet['K']);
                    $pnv_log->setAHCAID($sheet['L']);
                    $pnv_log->setGender($sheet['M']);
                    $pnv_log->setAppReceiptDate($sheet['N']);
                    $pnv_log->setCredentialingDate($sheet['O']);

                    if($sheet['H']=="20211114"){
                        $pnv_log->setStatus('new');
                    }else{
                        $pnv_log->setStatus('active');
                    }

                    if($sheet['I']!=""){
                        $pnv_log->setStatus('removed');
                    }

                    $em->persist($pnv_log);
                }
            }
            $cont++;
        }

        $em->flush();

        return new Response('All OK');
    }

    /**
     * @Route("/aetna-update-pnv-data", name="admin_aetna_load_pnv_data")
     */
    public function updateAETNAPGData() {
        $em = $this->getDoctrine()->getManager();
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/aetna_pnv_log";
        $fileName="AETNA_BSN_PNV_20211115.xlsx";

        $fileToRead = $url."/" . $fileName;

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=1;

        $current_logs=$em->getRepository('App:AETNAPNVLog')->findAll();
        if($current_logs){
            foreach ($current_logs as $item){
                $em->remove($item);
            }
            $em->flush();
        }

        foreach ($sheetData as $sheet) {
            if ($cont > 1) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    $pnv_log=new AETNAPNVLog();
                    $pnv_log->setRecordTrakingNumber($sheet['A']);
                    $pnv_log->setProviderId($sheet['B']);
                    $pnv_log->setFirstName($sheet['C']);
                    $pnv_log->setLastName($sheet['D']);
                    $pnv_log->setLicenseNumber($sheet['E']);
                    $pnv_log->setSsnFein($sheet['F']);
                    if($sheet['G']!=""){
                        $pnv_log->setNpiNumber($sheet['G']);
                    }
                    $pnv_log->setStartDate($sheet['H']);
                    $pnv_log->setEndDate($sheet['I']);
                    $pnv_log->setProviderType($sheet['J']);
                    $pnv_log->setPrimarySpecialty($sheet['K']);
                    $pnv_log->setAHCAID($sheet['L']);
                    $pnv_log->setGender($sheet['M']);
                    $pnv_log->setAppReceiptDate($sheet['N']);
                    $pnv_log->setCredentialingDate($sheet['O']);

                    if($sheet['H']=="20211115"){
                        $pnv_log->setStatus('new');
                    }else{
                        $pnv_log->setStatus('active');
                    }

                    if($sheet['I']!=""){
                        $pnv_log->setStatus('removed');
                    }

                    $em->persist($pnv_log);
                }
            }
            $cont++;
        }

        $em->flush();

        return new Response('All OK');
    }

    /**
     * @Route("/update-pnv-data-sl", name="admin_load_pnv_data_sl")
     */
    public function updateDataSL() {
        $em = $this->getDoctrine()->getManager();
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/fcc_pnv_log";
        $fileName="FCC_BSN_PNV_20211114.xlsx";

        $fileToRead = $url."/" . $fileName;

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(1);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=1;

        $current_logs=$em->getRepository('App\Entity\PNVSLLog')->findAll();
        if($current_logs){
            foreach ($current_logs as $item){
                $em->remove($item);
            }
            $em->flush();
        }

        foreach ($sheetData as $sheet) {
            if ($cont > 1) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    $pnvsl_log=new PNVSLLog();
                    $pnvsl_log->setRecordTrackingNumber($sheet['A']);
                    $pnvsl_log->setProviderGroupTrackingNumber($sheet['C']);
                    $pnvsl_log->setGroupLocationTrackingNumber($sheet['D']);
                    $pnvsl_log->setNPINumber($sheet['E']);
                    $pnvsl_log->setStartDate($sheet['F']);
                    $pnvsl_log->setEndDate($sheet['G']);
                    $pnvsl_log->setLocationName($sheet['H']);
                    $pnvsl_log->setAddressLine1($sheet['I']);
                    $pnvsl_log->setAddressLine2($sheet['J']);
                    $pnvsl_log->setCity($sheet['K']);
                    $pnvsl_log->setState($sheet['L']);
                    $pnvsl_log->setZipCode($sheet['M']);
                    $pnvsl_log->setCountyCode($sheet['N']);
                    $pnvsl_log->setPhoneNumber($sheet['O']);
                    $pnvsl_log->setIsPCP($sheet['Q']);
                    $pnvsl_log->setAcceptingPatients($sheet['R']);
                    $pnvsl_log->setCurrentPatients($sheet['S']);
                    $pnvsl_log->setGenderAccepted($sheet['T']);
                    $pnvsl_log->setAgeRestrictionLow($sheet['U']);
                    $pnvsl_log->setAgeRestrictionHigh($sheet['V']);
                    $pnvsl_log->setHasAfterHours($sheet['W']);
                    $pnvsl_log->setHasWeekendHolidayHours($sheet['X']);
                    $pnvsl_log->setHasWheelchairAccess($sheet['Y']);
                    $pnvsl_log->setSpecialties($sheet['Z']);
                    $pnvsl_log->setLanguages($sheet['AA']);
                    $pnvsl_log->setTaxonomies($sheet['AF']);
                    $pnvsl_log->setEHR($sheet['AG']);

                    if($sheet['G']!=""){
                        $pnvsl_log->setStatus('removed');
                    }else{
                        $pnvsl_log->setStatus('active');
                    }

                    $em->persist($pnvsl_log);
                }
            }
            $cont++;
        }

        $em->flush();
        return new Response('All OK');
    }

    /**
     * @Route("/aetna-update-pnv-data-sl", name="admin_load_aetna_pnv_data_sl")
     */
    public function updateAETDataSL() {
        $em = $this->getDoctrine()->getManager();
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/aetna_pnv_log";
        $fileName="AETNA_BSN_PNV_20211115.xlsx";

        $fileToRead = $url."/" . $fileName;

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(1);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=1;

        $current_logs=$em->getRepository('App:AETNAPNVSLLog')->findAll();
        if($current_logs){
            foreach ($current_logs as $item){
                $em->remove($item);
            }
            $em->flush();
        }

        foreach ($sheetData as $sheet) {
            if ($cont > 1) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    $pnvsl_log=new AETNAPNVSLLog();
                    $pnvsl_log->setRecordTrackingNumber($sheet['A']);
                    $pnvsl_log->setProviderGroupTrackingNumber($sheet['C']);
                    $pnvsl_log->setGroupLocationTrackingNumber($sheet['D']);
                    $pnvsl_log->setNPINumber($sheet['E']);
                    $pnvsl_log->setStartDate($sheet['F']);
                    $pnvsl_log->setEndDate($sheet['G']);
                    $pnvsl_log->setLocationName($sheet['H']);
                    $pnvsl_log->setAddressLine1($sheet['I']);
                    $pnvsl_log->setAddressLine2($sheet['J']);
                    $pnvsl_log->setCity($sheet['K']);
                    $pnvsl_log->setState($sheet['L']);
                    $pnvsl_log->setZipCode($sheet['M']);
                    $pnvsl_log->setCountyCode($sheet['N']);
                    $pnvsl_log->setPhoneNumber($sheet['O']);
                    $pnvsl_log->setIsPCP($sheet['Q']);
                    $pnvsl_log->setAcceptingPatients($sheet['R']);
                    $pnvsl_log->setCurrentPatients($sheet['S']);
                    $pnvsl_log->setGenderAccepted($sheet['T']);
                    $pnvsl_log->setAgeRestrictionLow($sheet['U']);
                    $pnvsl_log->setAgeRestrictionHigh($sheet['V']);
                    $pnvsl_log->setHasAfterHours($sheet['W']);
                    $pnvsl_log->setHasWeekendHolidayHours($sheet['X']);
                    $pnvsl_log->setHasWheelchairAccess($sheet['Y']);
                    $pnvsl_log->setSpecialties($sheet['Z']);
                    $pnvsl_log->setLanguages($sheet['AA']);
                    $pnvsl_log->setTaxonomies($sheet['AF']);
                    $pnvsl_log->setEHR($sheet['AG']);

                    if($sheet['G']!=""){
                        $pnvsl_log->setStatus('removed');
                    }else{
                        $pnvsl_log->setStatus('active');
                    }

                    $em->persist($pnvsl_log);
                }
            }
            $cont++;
        }

        $em->flush();
        return new Response('All OK 1');
    }

}


