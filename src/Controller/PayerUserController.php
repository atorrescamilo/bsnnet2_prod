<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use App\Entity\PayerUser;


/**
 * @Route("/admin/payeruser")
 */
class PayerUserController extends AbstractController{
    /**
     * @Route("/index", name="admin_payeruser")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $payerusers = $em->getRepository('App\Entity\PayerUser')->findAll();
            $delete_form_ajax = $this->createCustomForm('PAYERUSER_ID', 'DELETE', 'admin_delete_payeruser');

            return $this->render('payeruser/index.html.twig', array('payerusers' => $payerusers, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_payeruser")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $roles = $em->getRepository('App\Entity\PayerRole')->findAll();
            $payers = $em->getRepository('App\Entity\Payer')->findAll();

            return $this->render('payeruser/add.html.twig', array('roles' => $roles, 'payers' => $payers));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_payeruser",defaults={"id": null} )
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\PayerUser')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_payeruser');
            }
            $roles = $em->getRepository('App\Entity\PayerRole')->findAll();
            $payers = $em->getRepository('App\Entity\Payer')->findAll();

            return $this->render('payeruser/edit.html.twig', array('document' => $document, 'roles' => $roles, 'payers' => $payers));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_payeruser",defaults={"id": null} )
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\PayerUser')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_payeruser');
            }

            return $this->render('payeruser/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_payeruser")
     */
    public function create(Request $request, UserPasswordEncoderInterface $encoder,\Swift_Mailer $mailer){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $username = $request->get('username');
            $email = $request->get('email');
            $enabled = $request->get('enabled');

            $password = $request->get('password');
            $roles = $request->get('roles');
            $payer = $request->get('payer');

            $new = $request->get('new');

            $payeruser = new PayerUser();
            $payeruser->setName($name);
            $payeruser->setUsername($username);
            $payeruser->setEmail($email);
            $encode = $encoder->encodePassword($payeruser, $password);
            $payeruser->setPassword($encode);
            $payeruser->setEnabled($enabled);
            $payeruser->setIsNew(1);


            $payerObj = $em->getRepository('App\Entity\Payer')->find($payer);
            if ($payerObj != null) {
                $payeruser->setPayer($payerObj);
            }

            if ($roles !== null or $roles !== "") {
                foreach ($roles as $role) {
                    $rol = $em->getRepository('App\Entity\PayerRole')->find($role);
                    if ($rol !== null) {
                        $payeruser->addRole($rol);
                    }
                }
            }

            $em->persist($payeruser);
            $em->flush();

            $full_name=$name." [".$email."]";

            //send email for notification
            $message = (new \Swift_Message('BSN Payer Portal Login'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email)
                ->setBody($this->renderView(
                    'email/templates/new_payer_user_notification.html.twig',array('full_name'=>$full_name,'user_email'=>$email,'password'=>$password)
                )
                    , 'text/html');

            $mailer->send($message);


            $this->addFlash(
                'success',
                'Payer User has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_payeruser');
            }

            return $this->redirectToRoute('admin_payeruser');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/send-email", name="admin_payeruser_send_email")
     */
    public function sendEmail(Request $request, \Swift_Mailer $mailer){
        $em = $this->getDoctrine()->getManager();
        $id=$request->get('id');

        $user=$em->getRepository('App\Entity\PayerUser')->find($id);
        $email=$user->getEmail();

        if($user!=null){
            //send email for notification
            $message = (new \Swift_Message('BSN Portal Login'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email)
                ->setBody($this->renderView(
                    'email/templates/new-payer-user-notification.html.twig',array('user'=>$user)
                )
                    , 'text/html');

            $mailer->send($message);
        }

        return new Response(
            json_encode(array('test' => 1)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/update", name="admin_update_payeruser")
     */
    public function update(Request $request, UserPasswordEncoderInterface $encoder){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $username = $request->get('username');
            $email = $request->get('email');
            $enabled = $request->get('enabled');
            $roles = $request->get('roles');
            $id = $request->get('id');
            $payer = $request->get('payer');
            $new = $request->get('new');
            $password = $request->get('password');

            $payeruser = $em->getRepository('App\Entity\PayerUser')->find($id);

            if ($payeruser == null) {
                $this->addFlash(
                    "danger",
                    "The User can't been updated!"
                );

                return $this->redirectToRoute('admin_payeruser');
            }

            if ($payeruser != null) {

                if($password!=""){
                    $encode = $encoder->encodePassword($payeruser, $password);
                    $payeruser->setPassword($encode);
                }
                $payeruser->setName($name);
                $payeruser->setUsername($username);
                $payeruser->setEmail($email);
                $payeruser->setEnabled($enabled);

                $payerObj = $em->getRepository('App\Entity\Payer')->find($payer);
                if ($payerObj != null) {
                    $payeruser->setPayer($payerObj);
                }

                //check for previos roles
                $rolExist = $payeruser->getPayerUserRoles();
                if ($rolExist != null) {
                    foreach ($rolExist as $re) {
                        if ($re != null)
                            $payeruser->removeRole($re);
                    }
                }

                if ($roles !== null or $roles !== "") {
                    foreach ($roles as $role) {
                        $rol = $em->getRepository('App\Entity\PayerRole')->find($role);
                        if ($rol !== null) {
                            $payeruser->addRole($rol);
                        }
                    }
                }

                $em->persist($payeruser);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "User has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_payeruser');
            }

            return $this->redirectToRoute('admin_payeruser');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_payeruser",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $payeruser = $payeruser = $em->getRepository('App\Entity\User')->find($id);
            $removed = 0;
            $message = "";

            if ($payeruser) {
                try {
                    $em->remove($payeruser);
                    $em->flush();
                    $removed = 1;
                    $message = "The User has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The payeruser can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_payeruser",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $payeruser = $payeruser = $em->getRepository('App\Entity\User')->find($id);

                if ($payeruser) {
                    try {
                        $em->remove($payeruser);
                        $em->flush();
                        $removed = 1;
                        $message = "The User has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The User can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
