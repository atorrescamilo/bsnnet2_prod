<?php

namespace App\Controller;

use App\Entity\Company;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/company")
 */
class CompanyController extends AbstractController
{
    /**
     * @Route("/main-info", name="company_info")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em=$this->getDoctrine()->getManager();
            $company=$em->getRepository('App\Entity\Company')->find(1);

            return $this->render('company/index.html.twig', [
                'company' => $company,
            ]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/save-info", name="company_save_info",methods={"POST"})
     */
    public function save(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em=$this->getDoctrine()->getManager();

            $name=$request->get('name');
            $tin_number=$request->get('tin_number');
            $npi_number=$request->get('npi_number');
            $address=$request->get('address');
            $email=$request->get('email');
            $phone1=$request->get('phone1');
            $phone2=$request->get('phone2');
            $fax=$request->get('fax');
            $web=$request->get('web');

            $company=$em->getRepository('App\Entity\Company')->find(1);

            $company->setName($name);
            $company->setTinNumber($tin_number);
            $company->setAddress($address);
            $company->setEmail($email);
            $company->setPhone1($phone1);
            $company->setPhone2($phone2);
            $company->setFaxNumber($fax);
            $company->setWeb($web);
            $company->setNpiNumber($npi_number);

            $em->persist($company);
            $em->flush();

            $this->addFlash(
                'success',
                'The Company Information has been updated successfully!'
            );

            return $this->redirectToRoute('company_info');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }
}
