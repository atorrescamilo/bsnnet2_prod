<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\Organization;
use App\Entity\OrganizationContact;
use App\Entity\Provider;
use App\Entity\Specialty;
use Doctrine\ORM\Id\BigIntegerIdentityGenerator;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/payer")
 */
class BackendPayerController extends AbstractController
{

    /**
     * @Route("/map", name="payer_map")
     */
    public function Index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $db=$em->getConnection();

            $user=$this->getUser();
            $roles=$user->getRoles();
            $is_SuperAdmin=false;
            $payer=$user->getPayer()->getId();

            foreach ($roles as $role){
                if($role=="PAYER_SUPER_ADMIN"){
                    $is_SuperAdmin=true;
                }
            }

            if($user->getIsNew()==1){
                return $this->redirectToRoute('payer_profile_user');
            }

            $languages = $em->getRepository('App\Entity\Languages')->findBy(array(), array('ord' => 'ASC'));
            $specialtyAreas = $em->getRepository('App\Entity\SpecialtyAreas')->findBy(array(), array('name' => 'ASC'));
            $orgClassifications = $em->getRepository('App:OrgSpecialty')->findBy(array(), array('name' => 'ASC'));
            $organizations = $em->getRepository('App\Entity\Organization')->findBy(array('disabled'=>0), array('name' => 'ASC'));
            $degrees = $em->getRepository('App\Entity\Degree')->findBy(array(), array('name' => 'ASC'));
            $google_apikey = $this->getParameter('google_apikey');

            $organizations_result=[];
            foreach ($organizations as $organization){
                if($organization->getId()!=502){
                    $organizations_result[]=$organization;
                }
            }

            return $this->render('payer_backend\map.html.twig', array('languages' => $languages, 'specialtyAreas' => $specialtyAreas,
                'organizations' => $organizations_result, 'degrees' => $degrees, 'google_apikey' => $google_apikey,'orgClassifications'=>$orgClassifications));
        }else{
            return $this->redirectToRoute('payer_login');
        }
    }

    /**
     * @Route("/load-provider-ajax", name="provider_load_data",methods={"POST"})
     */
    public function loadProvidersAjax(Request $request){
        $em = $this->getDoctrine()->getManager();
        $id=$request->get('id');

        $provider=$em->getRepository('App\Entity\ViewProvidePayer')->find($id);
        $providerR=[];
        $providerTypesArray=[];
        $degreesArray=[];
        $specialtiesArray=[];
        $languagesArray=[];
        $addressesArray=[];
        $contactsArray=[];

        $organization=$provider->getOrganization();
        $addresses=$provider->getBillingAddress();
        $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));

        $addressArray=[];
        if($addresses!=null){
            foreach ($addresses as $address){
                $addressArray['street']=$address->getStreet();
                $addressArray['city']=$address->getCity();
                $addressArray['suite']=$address->getSuiteNumber();
                $addressArray['state']=$address->getUsState();
                $addressArray['zip_code']=$address->getZipCode();
                $addressArray['county']=$address->getCounty();
                $addressArray['region']=$address->getRegion();
                $addressArray['primary']=$address->getPrimaryAddr();
                $addressArray['facility']=$address->getIsFacility();
                $addressArray['npi']=$address->getLocationNpi();
                $addressArray['phone']=$address->getPhoneNumber();

                $addressesArray[]=$addressArray;
            }
        }

        $contactArray=[];
        if($contacts!=null){
            foreach ($contacts as $contact){
                if($contact->getTitle()!=null){
                    $contactArray['title']=$contact->getTitle()->getName();
                }else{
                    $contactArray['title']="";
                }
                $contactArray['name']=$contact->getName();


                if($contact->getPhone()!=null){
                    $contactArray['phone']=$contact->getPhone();
                }else{
                    $contactArray['phone']="-";
                }
                if($contact->getPhoneExt()!=null){
                    $contactArray['phone_ext']=$contact->getPhoneExt();
                }else{
                    $contactArray['phone_ext']="-";
                }

                if($contact->getMobile()!=null){
                    $contactArray['movile']=$contact->getMobile();
                }else{
                    $contactArray['movile']="-";
                }

                if($contact->getFax()!=null){
                    $contactArray['fax']=$contact->getFax();
                }else{
                    $contactArray['fax']="-";
                }

                $contactArray['email']=$contact->getEmail();
                $main="No";
                if($contact->getMain()==true){
                    $main="Yes";
                }
                $contactArray['main']=$main;
                $contactsArray[]=$contactArray;
            }
        }



        $providerR['full_name']=$provider->getFullName();
        $providerR['organization_name']=$organization->getName();
        $providerR['phone']=$organization->getPhone();
        $providerR['phone_billing']=$organization->getBillingPhone();
        $providerR['fax']=$organization->getFaxno();

        $providerTypes=$provider->getProviderType();
        $degrees=$provider->getDegree();
        $specialties=$provider->getPrimarySpecialties();
        $languages=$provider->getLanguages();

        $ptArray=[];
        if($providerTypes!=null){
            foreach ($providerTypes as $pt){
                $ptArray['name']=$pt->getName();
                $providerTypesArray[]=$ptArray;
            }
        }

        $degreeArray=[];
        if($degrees!=null){
            foreach ($degrees as $degree){
                $degreeArray['name']=$degree->getName();
                $degreesArray[]=$degreeArray;
            }
        }

        $specialtyArray=[];
        if($specialties!=null){
            foreach ($specialties as $specialty){
                $specialtyArray['name']=$specialty->getName();
                $specialtiesArray[]=$specialtyArray;
            }
        }

        $languageArray=[];
        if($languages!=null){
            foreach ($languages as $language){
                $languageArray['name']=$language->getName();
                $languagesArray[]=$languageArray;
            }
        }

        return new Response(
            json_encode(array('provider' =>$providerR,'providerTypesArray'=>$providerTypesArray,'degrees'=>$degreesArray,
                'specialties'=>$specialtiesArray,'languages'=>$languagesArray,'addresses'=>$addressesArray,'contacts'=>$contactsArray)),
            200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/load-provider-by-org-ajax", name="provider_by_org_load_data",methods={"POST","GET"})
     */
    public function loadProvidersByOrgAjax(Request $request){
        $em = $this->getDoctrine()->getManager();

        $user=$this->getUser();
        $payer=$user->getPayer();
        $payerId=$payer->getId();

        $org=$request->get('org');
        $providers=$em->getRepository('App\Entity\ViewProviderPayer')->findBy(array('organization'=>$org),array('first_name'=>'ASC'));
        $providersResult=array();
        $provider=array();

        foreach ($providers as $pro){
            $provider['id']=$pro->getId();
            $provider['full_name']=$pro->getFirstName()." ".$pro->getLastName();
            $address=$pro->getBillingAddress();
            $totalAddress=0;
            if($address!=null){
                foreach ($address as $addr){
                    if($payerId==1)
                    if($addr->getRegion()==9 or $addr->getRegion()==10 or $addr->getRegion()==11){
                        $totalAddress++;
                    }
                }
            }
            $provider['total_address']=$totalAddress;
            $provider['degrees']=$pro->getDegrees();
            $provider['specialties']=$pro->getSpecialties();
            $provider['languages']=$pro->getLanguagesStr();
            $provider['gender']=$pro->getGender();

            $providersResult[]=$provider;
        }

        return new Response(
            json_encode(array('providers' =>$providersResult)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/load-all-data", name="payer_all_load_data",methods={"POST","GET"})
     */
    public function loadDataMAP(Request $request){
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $roles=$this->getUser()->getRoles();

        $is_SuperAdmin=false;

        foreach ($roles as $role){
            if($role=="PAYER_SUPER_ADMIN"){
                $is_SuperAdmin=true;
            }
        }

        ////// query builder
        $user = $this->getUser();
        //get counties for payer
        $payer=$user->getPayer()->getId();
        $payerObj=$user->getPayer();
        $size=0;
        if ($request->getMethod() == Request::METHOD_POST) {

            $gender =$request->get('gender');
            $primary_specialty=$request->get('primary_specialty');
            $specialty_areas=$request->get('specialty_areas');
            $languages=$request->get('languages');
            $degrees=$request->get('degrees');
            $organizations=$request->get('organizations');
            $org_clasification=$request->get('org_clasification');

            $qb="";
            if($is_SuperAdmin and $payerObj->getName()=='All'){
                $qb = $em->createQueryBuilder()
                    ->from('App\Entity\ViewProviderPayer', 'provider')
                    ->leftJoin('provider.billing_address', 'address')
                    ->leftJoin('provider.organization', 'organization')
                    ->innerJoin("provider.payers",'provider_payer')
                    ->andWhere('provider.disabled = 0')
                    ->andWhere('address.lat IS NOT NULL AND address.lng IS NOT NULL');
            }else{
                $qb = $em->createQueryBuilder()
                    ->from('App\Entity\ViewProviderPayer', 'provider')
                    ->leftJoin('provider.billing_address', 'address')
                    ->leftJoin('provider.organization', 'organization')
                    ->innerJoin("provider.payers",'provider_payer')
                    ->andWhere('provider_payer.id = :payer')
                    ->andWhere('provider.disabled = 0')
                    ->andWhere('address.lat IS NOT NULL AND address.lng IS NOT NULL')
                    ->setParameter('payer', $payer);
            }

            //filter gender
            if ($gender!="-1") {
                $qb->andWhere('provider.gender = :gender')
                    ->setParameter('gender', $gender);
            }

            // filter specialties
            if (!empty($primary_specialty)) {
                $qb->leftJoin('provider.primary_specialties', 'specialty')
                    ->andWhere('specialty.id IN (:specialties)')
                    ->setParameter('specialties', $primary_specialty);
            }

            // filter specialty_areas
            if (!empty($specialty_areas)) {
                $qb->leftJoin('organization.specialty_areas', 'specialty_area')
                    ->andWhere('specialty_area.id IN (:specialty_areas)')
                    ->setParameter('specialty_areas', $specialty_areas);
            }

            // filter languages
            if (!empty($languages)) {
                $qb->leftJoin('provider.languages', 'language')
                    ->andWhere('language.id IN (:languages)')
                    ->setParameter('languages', $languages);
            }

            // filter organizations
            if (!empty($organizations)) {
                $qb->andWhere('organization.id IN (:organizations)')
                    ->setParameter('organizations', $organizations);
            }

            // filter degrees
            if (!empty($degrees)) {
                $qb->leftJoin('provider.degree', 'degree')
                    ->andWhere('degree.id IN (:degrees)')
                    ->setParameter('degrees', $degrees);
            }

            // filter org taxonomies
            if (!empty($org_clasification)) {
                $qb->leftJoin('organization.org_specialties', 'org_specialties')
                    ->andWhere('org_specialties.id IN (:org_specialties)')
                    ->setParameter('org_specialties', $org_clasification);
            }

            $qb->select('COUNT(DISTINCT provider.id)');

            $qb->resetDQLPart('select');

            $qb->select('
              DISTINCT provider.id
                     , provider.full_name
                     , address.id AS address_id
                     , address.lat, address.lng
                     , organization.id AS organization_id
                     , organization.name AS organization_name
                     , provider.degrees AS degrees
                     , provider.specialties AS specialties
                     , provider.languagesStr AS languages
                     , provider.gender AS gender
            ')
                ->orderBy('provider.full_name');
            $result_providers = [];

            $qb_results = $qb->getQuery()->getArrayResult();

            //group by provider
            $result_providers = [];
            $i=0;
            foreach ($qb_results as $result) {
                if (!isset($result_providers[$result['id']])) {
                    $result_providers[$result['id']] = [
                        'id' => $result['id'],
                        'full_name' => $result['full_name'],
                        'degrees' => $result['degrees'],
                        'specialties' => $result['specialties'],
                        'languages' => $result['languages'],
                        'gender' => $result['gender'],
                        'organization_id' => $result['organization_id'],
                        'organization_name' => $result['organization_name'],
                        'addresses' => []
                    ];
                }
                if (!isset($result_providers[$result['id']]['addresses'][$result['address_id']])) {
                    $result_providers[$result['id']]['addresses'][$result['address_id']] = [
                        'id' => $result['address_id'],
                        'lat' => $result['lat'],
                        'lng' => $result['lng']
                    ];
                }

            }

            // remove indexes
            $final_results = [];

            foreach ($result_providers as $result) {
                $addresses = $result['addresses'];
                $result['addresses'] = [];
                foreach ($addresses as $address) {
                    $result['addresses'][] = $address;
                }
                $final_results[] = $result;
            }

            $results = $final_results;

            return new Response(
                json_encode(array('results' => $results,'gender'=>$gender)), 200, array('Content-Type' => 'application/json')
            );
        }



        return new Response(
            json_encode(array('test' =>$payer)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/load-addr-all-data", name="laod_addr_all_data",methods={"POST"})
     */
    public function loadDataAddrAjax(Request $request){
        $em = $this->getDoctrine()->getManager();
        $id=$request->get('id');

        $address=$em->getRepository('App\Entity\BillingAddress')->find($id);
        $organization=$em->getRepository('App\Entity\Organization')->find($address->getOrganization()->getId());

        $addressResult=array();
        $addressResult['id']=$address->getId();
        $addressResult['bussinees']=$address->getBusinessHours();
        $addressResult['street']=$address->getStreet();
        $addressResult['suite_number']=$address->getSuiteNumber();
        $addressResult['city']=$address->getCity();
        $addressResult['county']=$address->getCounty();
        $addressResult['state']=$address->getUsState();
        $addressResult['region']=$address->getRegion();
        $addressResult['zip_code']=$address->getZipCode();
        $addressResult['country']=$address->getCounty();
        $addressResult['phone_number']=$address->getPhoneNumber();
        $addressResult['beds']=$address->getBeds();
        $addressResult['npi']=$address->getLocationNpi();
        $addressResult['npi2']=$address->getLocationNpi2();
        $addressResult['medicaid']=$address->getGroupMedicaid();
        $addressResult['medicare']=$address->getGroupMedicare();
        $addressResult['taxonomy']=$address->getTaxonomyCode();
        $addressResult['age_limit_min']=$address->getOfficeAgeLimit();
        $addressResult['age_limit_max']=$address->getOfficeAgeLimitMax();
        $addressResult['whellchair']=$address->getWheelchair();
        $addressResult['tdd']=$address->getId();
        $addressResult['ada_compliant']=$address->getAdaCompliant();
        $addressResult['public_transportation']=$address->getPublicTransportation();
        $addressResult['private_transportation']=$address->getId();
        $addressResult['primary_address']=$address->getId();
        $addressResult['billing_address']=$address->getBillingAddr();
        $addressResult['mailing_address']=$address->getMailingAddr();
        $addressResult['is_facility']=$address->getIsFacility();
        $addressResult['facility_number']=$address->getFacilityNumber();
        $addressResult['facility_expedition_number']=$address->getFacilityExemptionNumber();


        $servicesettingsResult=array();
        $ssResult=array();
        $servicesettings=$address->getServiceSettings();
        $totalServiceSetting=0;

        if(count($servicesettings)>0){
            $totalServiceSetting=1;
            foreach ($servicesettings as $servicesetting){
                $servicesettingsResult['id']=$servicesetting->getId();
                $servicesettingsResult['name']=$servicesetting->getName();
                $ssResult[]=$servicesettingsResult;
            }
        }

        $facilityTypeResult=array();
        $fType=array();

        $facilityTypes=$address->getFacilityType();
        $totalFacilityTypes=0;

        if(count($facilityTypes)>0){
            $totalFacilityTypes=1;
            foreach ($facilityTypes as $facitity){
                $fType['id']=$facitity->getId();
                $fType['name']=$facitity->getName();
                $facilityTypeResult[]=$fType;
            }
        }

        $orgResult=array();
        $specialtyAreasR=array();
        $specialtyArea=array();

        if($organization){
            $orgResult['name']=$organization->getName();
            $orgResult['type']=$organization->getProviderType();
            $orgResult['billing_phone']=$organization->getBillingPhone();
            $orgResult['phone']=$organization->getPhone();
            $orgResult['faxno']=$organization->getFaxno();
            $orgResult['website']=$organization->getWebsite();

            $specialtyAreas=$organization->getSpecialtyAreas();
            if($specialtyAreas){
                foreach ($specialtyAreas as $sa){
                    $specialtyArea['name']=$sa->getName();
                    $specialtyAreasR[]=$specialtyArea;
                }

            }
            $orgResult['specialtyareas']=$specialtyAreasR;

        }

        $providerResult=[];
        ////// query builder
        $user = $this->getUser();
        //get counties for payer
        $payer=$user->getPayer()->getId();

        $qb = $em->createQueryBuilder()
            ->from('App\Entity\ViewProviderPayer', 'provider')
            ->leftJoin('provider.billing_address', 'address')
            ->innerJoin("provider.payers",'provider_payer')
            ->andWhere('provider_payer.id = :payer')
            ->andWhere('provider.disabled = 0')
            ->andWhere('address.id = :addr')
            ->andWhere('address.lat IS NOT NULL AND address.lng IS NOT NULL')
            ->setParameter('payer', $payer)
            ->setParameter('addr', $id);

        $qb->select('COUNT(DISTINCT provider.id)');

        $qb->resetDQLPart('select');

        $qb->select('
              DISTINCT provider.id
                     , provider.full_name
                     , provider.npi_number
                     , provider.provider_types as providertypes 
                     , provider.degrees AS degrees
                     , provider.specialties AS specialties
                     , provider.languagesStr AS languages
                     , provider.gender AS gender
            ')
            ->orderBy('provider.full_name');

        $qb_results = $qb->getQuery()->getArrayResult();

        $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));

        $contactArray=[];
        if($contacts!=null){
            foreach ($contacts as $contact){
                if($contact->getTitle()!=null){
                    $contactArray['title']=$contact->getTitle()->getName();
                }else{
                    $contactArray['title']="";
                }
                $contactArray['name']=$contact->getName();


                if($contact->getPhone()!=null){
                    $contactArray['phone']=$contact->getPhone();
                }else{
                    $contactArray['phone']="-";
                }
                if($contact->getPhoneExt()!=null){
                    $contactArray['phone_ext']=$contact->getPhoneExt();
                }else{
                    $contactArray['phone_ext']="-";
                }

                if($contact->getMobile()!=null){
                    $contactArray['movile']=$contact->getMobile();
                }else{
                    $contactArray['movile']="-";
                }

                if($contact->getFax()!=null){
                    $contactArray['fax']=$contact->getFax();
                }else{
                    $contactArray['fax']="-";
                }

                $contactArray['email']=$contact->getEmail();
                $contactsArray[]=$contactArray;
            }
        }

        return new Response(
            json_encode(array('address'=>$addressResult,'ssResult'=>$ssResult,'totalServiceSetting'=>$totalServiceSetting,
                'facilityTipes'=>$facilityTypeResult,'totalFacilityTypes'=>$totalFacilityTypes,'organization'=>$orgResult,'providers'=>$qb_results,
                'contacts'=>$contactsArray)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/key-indicators-dashboard", name="payer_key_indicator_dashboard")
     */
    public function keyIndicatorsDashbard(Request $request){
        $em = $this->getDoctrine()->getManager();

        $year=date('Y');
        $mmm_indicators=$em->getRepository('App\Entity\MMMIndicator')->findBy(array('year'=>$year,'payer'=>1),array('id'=>'ASC'));

        $data=[];
        foreach ($mmm_indicators as $indicator){
            $c_data=explode(',',$indicator->getData());
            $data[$indicator->getTopic()][$indicator->getIndicator()]=$c_data;
        }

        return $this->render('payer_backend/key_indicators_dashboard.html.twig',['mmm_indicators'=>$mmm_indicators,'data'=>$data]);
    }

    /**
     * @Route("/export-provider-roster", name="export_provider_roster_mmm",methods={"POST"})
     */
    public function exportProviderRosterAjax(Request $request){
        $em=$this->getDoctrine()->getManager();
        $npis=$request->get('npis');
        $npis=explode(',',$npis);

        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');
        $date=date('m/d/Y');
        $spreadsheet = $reader->load($url.'template-mmm.xlsx');
        $spreadsheet->setActiveSheetIndex(1);

        $providers_npis=[];
        $results=[];

        foreach ($npis as $npi){
            if(!in_array($npi,$providers_npis)){
                $providers_npis[]=$npi;
                $provider_data=$em->getRepository('App:MMMRosterIndividualData')->findBy(array('npi'=>$npi));
                if($provider_data){
                    foreach ($provider_data as $pdata){
                        $results[]=$pdata;
                    }
                }
            }
        }

        $cont=2;
        foreach ($results as $result){
            $cell_C = 'C' . $cont;$cell_D = 'D' . $cont;$cell_E = 'E' . $cont;
            $cell_F = 'F' . $cont;$cell_G = 'G' . $cont;$cell_I = 'I' . $cont;
            $cell_J = 'J' . $cont;$cell_K = 'K' . $cont;$cell_L = 'L' . $cont;
            $cell_M = 'M' . $cont;$cell_N = 'N' . $cont;$cell_O = 'O' . $cont;
            $cell_P = 'P' . $cont;$cell_Q = 'Q' . $cont;

            $cell_V = 'V' . $cont;
            $cell_W = 'W' . $cont;
            $cell_X = 'X' . $cont;
            $cell_Y = 'Y' . $cont;
            $cell_Z = 'Z' . $cont;
            $cell_AA = 'AA' . $cont;
            $cell_AB = 'AB' . $cont;
            $cell_AC = 'AC' . $cont;
            $cell_AD = 'AD' . $cont;
            $cell_AE = 'AE' . $cont;
            $cell_AF = 'AF' . $cont;
            $cell_AG = 'AG' . $cont;
            $cell_AH = 'AH' . $cont;
            $cell_AZ = 'AZ' . $cont;
            $cell_BB = 'BB' . $cont;
            $cell_BC = 'BC' . $cont;
            $cell_BV = 'BV' . $cont;
            $cell_BW = 'BW' . $cont;
            $cell_BX = 'BX' . $cont;
            $cell_BY = 'BY' . $cont;
            $cell_CA = 'CA' . $cont;
            $cell_CB = 'CB' . $cont;
            $cell_BZ = 'BZ' . $cont;
            $cell_AI = 'AI' . $cont;
            $cell_AJ = 'AJ' . $cont;
            $cell_AK = 'AK' . $cont;
            $cell_AL = 'AL' . $cont;
            $cell_AM = 'AM' . $cont;
            $cell_AN = 'AN' . $cont;
            $cell_AO = 'AO' . $cont;
            $cell_AP = 'AP' . $cont;
            $cell_AQ = 'AQ' . $cont;
            $cell_AR = 'AR' . $cont;
            $cell_AS = 'AS' . $cont;
            $cell_AT = 'AT' . $cont;
            $cell_AU = 'AU' . $cont;
            $cell_AV = 'AV' . $cont;
            $cell_AW = 'AW' . $cont;
            $cell_AX = 'AX' . $cont;
            $cell_BA = 'BA' . $cont;

            $delegated_entity_name="Behavioral Services Network, LLC";
            $data=$result->getData();

            $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $delegated_entity_name);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $date);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, 'PAR');
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $data['col_f']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $data['col_g']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $data['col_i']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $data['col_j']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $data['col_k']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $data['col_l']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_M, $data['col_m']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_N,"Yes" );
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_O, "No");
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_P, "Yes");
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_Q, "Yes");
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_V, $data['col_v']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_W, $data['col_w']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_X, $data['col_x']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_Y, $data['col_y']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_Z, $data['col_z']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AA, $data['col_aa']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AB, $data['col_ab']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AC, $data['col_ac']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AD, $data['col_ad']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AE, $data['col_ae']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AF, $data['col_af']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AG, $data['col_ag']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AH, $data['col_ah']);

            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AZ, 'No');
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BB, 'Yes');
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BC, '85%');
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BV, 'Behavioral Services Network, LLC');
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BW, '82-3133751');
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BX, '8200 NW 41st Street Doral, FL 33166');
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BZ, $data['col_bz']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BY, $data['col_by']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_CA, 'No');
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_CB, strval($data['col_cb']));

            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AI, $data['col_ai']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AJ, $data['col_aj']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AK, $data['col_ak']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AL, $data['col_al']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AM, $data['col_am']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AN, $data['col_an']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AO, $data['col_ao']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AP, $data['col_ap']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AQ, $data['col_aq']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AR, $data['col_ar']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AS, $data['col_as']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AT, $data['col_at']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AU, $data['col_au']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AV, $data['col_av']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AW, $data['col_aw']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_AX, $data['col_ax']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_BA, $data['col_ba']);

            $cont++;
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel; charset=utf-8');
        header("Content-type: application/x-msexcel; charset=utf-8");
        header('Content-type: application/ms-excel');
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false);
        header('Content-Disposition: attachment;filename="BSN_MMM_Roster_Individuals.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        return new Response();
    }

    /**
     * @Route("/report-provider-roster", name="report_provider_roster_mmm")
     */
    public function reportProviderRosterAjax(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $provider_data=$em->getRepository('App:MMMRosterIndividualData')->findAll();

            $results=[];
            $result=[];
            if($provider_data){
                foreach ($provider_data as $pdata){
                    $data=$pdata->getData();
                    $result['col_f']=$data['col_f'];
                    $result['col_f']=$data['col_f'];
                    $result['col_g']=$data['col_g'];
                    $result['col_i']=$data['col_i'];
                    $result['col_j']=$data['col_j'];
                    $result['col_k']=$data['col_k'];
                    $result['col_l']=$data['col_l'];
                    $result['col_m']=$data['col_m'];
                    $result['col_n']="Yes";
                    $result['col_o']="No";
                    $result['col_p']="Yes";
                    $result['col_q']="Yes";
                    $result['col_v']=$data['col_v'];
                    $result['col_w']=$data['col_w'];
                    $result['col_x']=$data['col_x'];
                    $result['col_y']=$data['col_y'];
                    $result['col_z']=$data['col_z'];
                    $result['col_aa']=$data['col_aa'];
                    $result['col_ab']=$data['col_ab'];
                    $result['col_ac']=$data['col_ac'];
                    $result['col_ad']=$data['col_ad'];
                    $result['col_ae']=$data['col_ae'];
                    $result['col_af']=$data['col_af'];
                    $result['col_ag']=$data['col_ag'];
                    $result['col_ah']=$data['col_ah'];
                    $result['col_az']="No";
                    $result['col_bb']="Yes";
                    $result['col_bc']="85%";
                    $result['col_bv']="Behavioral Services Network, LLC";
                    $result['col_bw']="82-3133751";
                    $result['col_bx']="8200 NW 41st Street Doral, FL 33166";
                    $result['col_bz']=$data['col_bz'];
                    $result['col_by']=$data['col_by'];
                    $result['col_ca']="No";
                    $result['col_cb']=$data['col_cb'];
                    $result['col_ai']=$data['col_ai'];
                    $result['col_aj']=$data['col_aj'];
                    $result['col_ak']=$data['col_ak'];
                    $result['col_al']=$data['col_al'];
                    $result['col_am']=$data['col_am'];
                    $result['col_an']=$data['col_an'];
                    $result['col_ao']=$data['col_ao'];
                    $result['col_ap']=$data['col_ap'];
                    $result['col_aq']=$data['col_aq'];
                    $result['col_ar']=$data['col_ar'];
                    $result['col_as']=$data['col_as'];
                    $result['col_at']=$data['col_at'];
                    $result['col_au']=$data['col_au'];
                    $result['col_av']=$data['col_av'];
                    $result['col_aw']=$data['col_aw'];
                    $result['col_ax']=$data['col_ax'];
                    $result['col_ba']=$data['col_ba'];

                    $results[]=$result;
                }
            }
            $date=date('m/d/Y');

            return $this->render('payer_backend/individual_roster_report.html.twig',['provider_data'=>$results,'date'=>$date]);
        }else{
            return $this->redirectToRoute('payer_login');
        }
    }
}
