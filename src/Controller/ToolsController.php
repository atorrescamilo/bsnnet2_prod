<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use PhpOffice\PhpSpreadsheet\Reader;

/**
 * @Route("/admin/tools")
 */
class ToolsController extends AbstractController
{
    /**
     * @Route("/providers-licenses-report", name="providers_licenses_report")
     */
    public function providerLicensesReport()
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');;

        $spreadsheet = $reader->load($url.'provider_licenses_template.xlsx');
        $spreadsheet->setActiveSheetIndex(0);

        $sql="SELECT p.id, p.npi_number, p.disabled,p.state_lic,p.first_name, p.last_name,
            GROUP_CONCAT(DISTINCT pml2.col_v ORDER BY pml2.id ASC SEPARATOR ', ') AS pml_licenses
            FROM  provider p 
            LEFT JOIN pml2 ON pml2.col_n = p.npi_number
            WHERE p.disabled=0
            GROUP BY p.id
            ORDER BY p.id";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $providers= $stmt->fetchAll();

        $cont=2;
        foreach ($providers as $provider){
            $cell_A='A'.$cont;
            $cell_B='B'.$cont;
            $cell_C='C'.$cont;
            $cell_D='D'.$cont;
            $cell_E='E'.$cont;;

            $full_name=$provider['first_name']." ".$provider['last_name'];

            $pmls_licenses=explode(', ',$provider['pml_licenses']);

            $math_license=false;
            foreach ($pmls_licenses as $licens){
                if($licens==$provider['state_lic']){
                    $math_license=true;
                }
            }

            if($math_license and $provider['state_lic']!="-" and $provider['state_lic']!=""){
                $range="A".$cont.":"."E".$cont;
                $spreadsheet->getActiveSheet()->getStyle($range)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('1dcc45');
            }

            if($math_license==false and $provider['state_lic']!="-" and $provider['state_lic']!=""){
                $range="A".$cont.":"."E".$cont;
                $spreadsheet->getActiveSheet()->getStyle($range)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('d6370f');
            }

            if($pmls_licenses[0]!="" and ($provider['state_lic']=="-" or $provider['state_lic']=="")){
                $range="A".$cont.":"."E".$cont;
                $spreadsheet->getActiveSheet()->getStyle($range)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('d6370f');
            }

            if(count($pmls_licenses)>1){
                if($pmls_licenses[1]!="" and ($provider['state_lic']=="-" or $provider['state_lic']=="")){
                    $range="A".$cont.":"."E".$cont;
                    $spreadsheet->getActiveSheet()->getStyle($range)->getFill()->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)->getStartColor()->setRGB('d6370f');
                }
            }

            $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $provider['id']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $provider['npi_number']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $full_name);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $provider['state_lic']);
            $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $provider['pml_licenses']);

            $cont++;
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="ProvidersLicensesReport'.date('Ymd').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/providers-licenses-remove-spaces", name="providers_licenses_remove_spaces")
     */
    public function providerLicensesRemoveSpaces()
    {
        $em = $this->getDoctrine()->getManager();
        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));

        foreach ($providers as $provider){
            $current_license=$provider->getStateLic();
            $current_license=str_replace(" ", "",$current_license);

           $provider->setStateLic($current_license);
           $em->persist($provider);
        }

        $em->flush();

        die();
    }

    /**
     * @Route("/providers-aetna-report", name="providers_aetna_report")
     */
    public function providerAetnaReport()
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));

            $providers_result=[];

            foreach ($providers as $provider){
                $npi=$provider->getNpiNumber();
                $organization_id=$provider->getOrganization()->getId();
                if($npi!="" and $npi!="N/A" and $npi!="NOT AVAILABLE" and $organization_id!=502){

                    $organization=$provider->getOrganization();
                    $providersOthers=$organization->getProviders();
                    $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));

                    $has_medicaid=false;
                    $has_medicare=false;
                    $medicaid_number=$provider->getMedicaid();

                    if($medicaid_number!="-" and $medicaid_number!="" and $medicaid_number!="N/A" and $medicaid_number!="In Progress" and $medicaid_number!='0' and $medicaid_number!="Plans only" and $medicaid_number!="Pending" and
                     $medicaid_number!="NA" and $medicaid_number!="PENDING" and $medicaid_number!="000000" and $medicaid_number!="0000000" and $medicaid_number!="0000000000" and $medicaid_number!="ARNP" and $medicaid_number!="n/a" and
                    $medicaid_number!="In process" and $medicaid_number!="Pending" and $medicaid_number!="credentialing in process" and $medicaid_number!="n/a" and $medicaid_number!="00pending" and $medicaid_number!="blank" and
                    $medicaid_number!="MD" and $medicaid_number!="APRN" and $medicaid_number!="PsyD"){
                        $has_medicaid=true;
                    }

                    if($has_medicaid==false) {
                        if($address){
                            foreach ($address as $addr) {
                                $medicaid_addr=$addr->getGroupMedicaid();
                                if($medicaid_addr!=""){
                                    $has_medicaid=true;
                                }
                            }
                        }
                    }

                    if($has_medicaid==false){
                        $sql="SELECT pml2.id
                        FROM  pml2
                        WHERE pml2.col_n=$npi";

                        $stmt = $conn->prepare($sql);
                        $stmt->execute();
                        $pml= $stmt->fetchAll();

                        if(count($pml)>0){
                            $has_medicaid=true;
                        }
                    }

                    $medicare=$provider->getMedicare();
                    if($medicare!="" and $medicare!="-" and $medicare!="." and $medicare!="-N/A" and $medicare!="0" and $medicare!="00000" and $medicare!="000000" and $medicare!="0000000"
                        and $medicare!="00000000" and $medicare!="000000000" and $medicare!="000000000000" and $medicare!="n/a" and $medicare!="55525 - opted out" and
                    $medicare!="Interal Medicine" and $medicare!="Internal Medicine" and $medicare!="N/A" and $medicare!="NA" and $medicare!="None" and $medicare!="Pending" and
                    $medicare!="Psychiatrist" and $medicare!="Psychologist" and $medicare!="in procress" and $medicare!="in progress" and $medicare!="submitting application" and
                    $medicare!="In-progress" and $medicare!="N/A" and $medicare!="PENDING" and $medicare!="Pending" and $medicare!="pending" and $medicare!="blank"){
                       $has_medicare=true;
                    }

                    if($has_medicare==false){
                        if($providersOthers!=null) {
                            foreach ($providersOthers as $proOther) {
                                $medi=$proOther->getMedicare();
                                if($medi!="" and $medi!="-" and $medi!="." and $medi!="-N/A" and $medi!="0" and $medi!="00000" and $medi!="000000" and $medi!="0000000"
                                    and $medi!="00000000" and $medi!="000000000" and $medi!="000000000000" and $medi!="n/a" and $medi!="55525 - opted out" and
                                    $medi!="Interal Medicine" and $medi!="Internal Medicine" and $medi!="N/A" and $medi!="NA" and $medi!="None" and $medi!="Pending" and
                                    $medi!="Psychiatrist" and $medi!="Psychologist" and $medi!="in procress" and $medi!="in progress" and $medi!="submitting application" and $medi!="In-progress"
                                    and $medi!="N/A" and $medi!="PENDING" and $medi!="Pending" and $medi!="pending" and $medi!="blank"){

                                    $has_medicare=true;
                                }
                            }
                        }

                        if($has_medicare==false){
                            if($address){
                                foreach ($address as $addr) {
                                    $medi=$addr->getGroupMedicare();
                                    if($medi!="" and $medi!="-" and $medi!="In progress" and $medi!="In-progress"
                                        and $medi!="N/A" and $medi!="PENDING" and $medi!="Pending" and $medi!="pending" and $medi!="blank") {
                                        $has_medicare=true;
                                    }
                                }
                            }
                        }
                    }

                    $lob="";

                    if($has_medicaid){
                        $lob="Medicaid";
                    }
                    if($has_medicare){
                        $lob="Medicare";
                    }

                    if($has_medicaid and $has_medicare){
                        $lob="BOTH";
                    }

                    $providerArray=[];
                    $address=$provider->getBillingAddress();
                    if($address){
                        foreach ($address as $addr){
                            $street_check=$addr->getStreet();
                            $addr_pass=true;
                            if(stristr($street_check, 'BOX') or stristr($street_check, 'Box') or stristr($street_check, 'box') or $addr->getUsState()!="FL" or $addr->getDisabled()==1) {
                                $addr_pass=false;
                            }

                            if($addr_pass){
                                $min_age=$addr->getOfficeAgeLimit();
                                $max_age=$addr->getOfficeAgeLimitMax();

                                $providerType="LPHA";

                                $proTypes=$provider->getProviderType();

                                if($proTypes){
                                    foreach ($proTypes as $pt){
                                        if($pt->getId()==9 or $pt->getId()==10){
                                            if(intval($min_age)<=18 and intval($max_age)>18){
                                                $providerType="Psychiatrist - Both";
                                            }

                                            if(intval($min_age)>18){
                                                $providerType="Psychiatrist - Adult";
                                            }

                                            if(intval($max_age)<=18 ){
                                                $providerType="Psychiatrist - Child";
                                            }
                                        }
                                    }
                                }

                                $providerArray['id']=$provider->getId();
                                $providerArray['full_name']=$provider->getFirstName()." ".$provider->getLastName();
                                $providerArray['npi']=$provider->getNpiNumber();
                                $providerArray['organization_name']=$provider->getOrganization()->getName();
                                $providerArray['organization_npi']=$provider->getOrganization()->getGroupNpi();
                                $providerArray['street']=$addr->getStreet();
                                $providerArray['county']=$addr->getCounty();
                                $providerArray['city']=$addr->getCity();
                                $providerArray['zipcode']=$addr->getZipCode();
                                $providerArray['state']=$addr->getUsState();
                                $providerArray['region']=$addr->getRegion();
                                $providerArray['lob']=$lob;
                                $providerArray['provider_type']=$providerType;
                                $providerArray['min_age']=$min_age;
                                $providerArray['max_age']=$max_age;

                                $providers_result[]=$providerArray;
                            }
                        }
                    }
                }
            }

            return $this->render('tools/aetna-report.html.twig',['providers'=>$providers_result]);
        }else{
            return $this->redirectToRoute('admin_login');
        }

    }

    /**
     * @Route("/convert-pnv-to-dat", name="convert_pnv_to_dat")
     */
    public function convertPNVToDAT(){

        return $this->render('tools/load-pnv-to-convert.html.twig');
    }

    /**
     * @Route("/convert-fhkc-to-dat", name="convert_fhkc_to_dat")
     */
    public function convertFHKCToDAT(){

        return $this->render('tools/load-fhkc-to-convert.html.twig');
    }

    /**
     * @Route("/aetna-reports-list", name="aetna-reports-list")
     */
    public function  AETAReportList(){

        $reports=[];
        $reports_dir = $this->getparameter('aetana_dat_loaded');

        if (is_dir($reports_dir)) {
            $file_maneger = opendir($reports_dir);

            while (($file = readdir($file_maneger)) !== false)  {
                if ($file != "." && $file != "..") {
                    $reports[]=$file;
                }

            }
            closedir($file_maneger);
        }

        return $this->render('tools/aetna_reports_list.html.twig',['reports'=>$reports]);
    }


    /**
     * @Route("/pnv-upload-proccess", name="admin_pnv_upload_proccess",methods={"POST"})
     */
    function uploadPNVtoDatProccess(Request $request){
        set_time_limit(8600);
        $publicDirectory = $this->getParameter('kernel.project_dir');
        $file = $request->files->get('pnv_load');

        $fileName = "";
        $fileExtension="";
        if ($file != "") {
            $nombreoriginal = $file->getClientOriginalName();
            $fileName = $nombreoriginal;
            $adjuntosDir = $this->getparameter('pnv_loaded');
            $fileExtension=$file->guessExtension();
            $file->move($adjuntosDir, $fileName);
        }

        //step for read the excel and read de information
        $fileToRead = $this->getparameter('pnv_loaded') . "/" . $fileName;

        if($fileExtension=="xlsx"){
            $reader = new Reader\Xlsx();
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($fileToRead);
            $spreadsheet->setActiveSheetIndex(0);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

            $spreadsheet->setActiveSheetIndex(1);
            $sheetDataSL = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        }

        if($fileExtension=="xls"){
            $reader = new Reader\Xls();
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($fileToRead);
            $spreadsheet->setActiveSheetIndex(0);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        }

        $cont=1;
        $folder = $publicDirectory.'/public/aetna-reports';

        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }

        //read and convert the PG tab

        $date=date('Ymd');
        $name="PGBSN".$date.".dat";
        $file_name=$folder."/".$name;

        $string_to_save_pg="";
        foreach ($sheetData as $sheet) {
            if ($cont > 2) {
                $a=$sheet['A']."|";
                $b=$sheet['B']."|";
                $c=$sheet['C']."|";
                $d=$sheet['D']."|";
                $e=$sheet['E']."|";
                $f=$sheet['F']."|";
                $g=$sheet['G']."|";
                $h=$sheet['H']."|";
                $i=$sheet['I']."|";
                $j=$sheet['J']."|";
                $k=$sheet['K']."|";
                $l=$sheet['L']."|";
                $m=$sheet['M']."|";
                $n=$sheet['N']."|";
                $o=$sheet['O']."|";
                $p=$sheet['P']."|";
                $q=$sheet['Q']."|";
                $r=$sheet['R']."|";
                $s=$sheet['S']."|";
                $t=$sheet['T']."|";
                $u=$sheet['U']."|";
                $w=$sheet['W']."|";
                $x=$sheet['X']."|";
                $y=$sheet['Y'];

                $string_to_save_pg.=$a.$b.$c.$d.$e.$f.$g.$h.$i.$j.$k.$l.$m.$n.$o.$p.$q.$r.$s.$t.$u.$w.$x.$y;

                $last_chart=substr($string_to_save_pg,-1);
                if($last_chart=="|"){
                    $string_to_save_pg=substr($string_to_save_pg,0,-1);
                }

                $string_to_save_pg=$string_to_save_pg."\n";
            }

            $cont++;
        }
        $file_dat = fopen($file_name,"w");
        fwrite($file_dat,$string_to_save_pg);
        fclose($file_dat);

        //read and convert the SL tab
        $name_sl="SLBSN".$date.".dat";
        $file_name_sl=$folder."/".$name_sl;
        $string_to_save_sl="";
        $cont=1;
        foreach ($sheetDataSL as $sheet) {
            if ($cont > 2) {
                $a=$sheet['A']."|";
                $b=$sheet['B']."|";
                $c=$sheet['C']."|";
                $d=$sheet['D']."|";
                $e=$sheet['E']."|";
                $f=$sheet['F']."|";
                $g=$sheet['G']."|";
                $h=$sheet['H']."|";
                $i=$sheet['I']."|";
                $j=$sheet['J']."|";
                $k=$sheet['K']."|";
                $l=$sheet['L']."|";
                $m=$sheet['M']."|";
                $n=$sheet['N']."|";
                $o=$sheet['O']."|";
                $p=$sheet['P']."|";
                $q=$sheet['Q']."|";
                $r=$sheet['R']."|";
                $s=$sheet['S']."|";
                $t=$sheet['T']."|";
                $u=$sheet['U']."|";
                $v=$sheet['V']."|";
                $w=$sheet['W']."|";
                $x=$sheet['X']."|";
                $y=$sheet['Y']."|";
                $z=$sheet['Z']."|";
                $aa=$sheet['AA']."|";
                $ab=$sheet['AB']."|";
                $ac=$sheet['AC']."|";
                $ad=$sheet['AD']."|";
                $ae=$sheet['AE']."|";
                $af=$sheet['AF']."|";
                $ag=$sheet['AG'];

                $string_to_save_sl.=$a.$b.$c.$d.$e.$f.$g.$h.$i.$j.$k.$l.$m.$n.$o.$p.$q.$r.$s.$t.$u.$v.$w.$x.$y.$z.$aa.$ab.$ac.$ad.$ae.$af.$ag;

               /* $last_chart=substr($string_to_save_sl,-1);
                if($last_chart==" "){
                    $string_to_save_sl=substr($string_to_save_sl,0,-1);
                }
                if($last_chart=="|"){
                    $string_to_save_sl=substr($string_to_save_sl,0,-1);
                }*/
                $string_to_save_sl=$string_to_save_sl."\n";
            }
            $cont++;
        }

        $file_dat_sl = fopen($file_name_sl,"w");
        fwrite($file_dat_sl,$string_to_save_sl);
        fclose($file_dat_sl);

        return $this->redirectToRoute('home');
    }

    /**
     * @Route("/update-license-from-credsimple", name="update-license-from-credsimple")
     */
    function UpdateLicenseFromCredSimple(){
        $em=$this->getDoctrine()->getManager();

        $providers=$em->getRepository('App:Provider')->findBy(array('disabled'=>0));

        $cont=1;
        foreach ($providers as $provider){

            $npi=$provider->getNpiNumber();
            if($npi!="NOT AVAILABLE" and $npi!=""){
               $license=$provider->getStateLic();
               $expire_license=$provider->getLicExpiresDate();

               if($license=="" or $license=="-" or $expire_license==""){

                   $credentialing=$em->getRepository('App:CredSimpleRecord')->findOneBy(array('npi'=>$npi));

                   if($credentialing!=null){
                       $cred_expire_license=$credentialing->getLicenseExpiresOn();

                       if($expire_license=="" and $license!="" and $license!="-"){
                           if($cred_expire_license!=""){
                               $cred_expire_license_array=explode('/',$cred_expire_license );
                               $moth= $cred_expire_license_array[0];
                               $day= $cred_expire_license_array[1];
                               $year= $cred_expire_license_array[2];

                               $newdate=$moth."/".$day."/".$year;
                               $provider->setLicExpiresDate($newdate);
                           }
                       }
                   }
               }
            }
        }

        $em->flush();
        echo "OK";
        die();
    }


    /**
     * @Route("/set-specialty-area-type", name="set-specialty-area-type")
     */
    function setSpecialtyAreaType(){
        $em=$this->getDoctrine()->getManager();

        $specialties=$em->getRepository('App:SpecialtyAreas')->findAll();

        foreach ($specialties as $specialty){

            $area_1=$em->getRepository('App:SpecialtyAreaType')->find(1);
            $area_2=$em->getRepository('App:SpecialtyAreaType')->find(2);
            $area_3=$em->getRepository('App:SpecialtyAreaType')->find(3);

            $id=$specialty->getId();
            $sa_elements=[8,76,77];
            $ser_elements=[63,65,73,32,15,48,22,35,47,14,43];
            if(in_array($id,$sa_elements) ){
                $specialty->setSpecialtyAreaType($area_2);
            }elseif (in_array($id, $ser_elements)){
                $specialty->setSpecialtyAreaType($area_3);
            }else{
                $specialty->setSpecialtyAreaType($area_1);
            }

            $name=$specialty->getName();

            $new_name=strtoupper($name);

            $specialty->setName($new_name);
            $em->persist($specialty);
        }

        $em->flush();
        die();
    }

    /**
     * @Route("/set-specialty-area-from-providers", name="set-specialty-area-from-providers")
     */
    function setSpecialtyAreaFromProviders(){
        $em=$this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App:Organization')->findAll();

        foreach ($organizations as $organization){

            $current_specialty_areas=$organization->getSpecialtyAreas();
            $current_specialty_area_ids=[];
            if(count($current_specialty_areas)>0){

            }

            $providers=$em->getRepository('App:Provider')->findBy(array('organization'=>$organization->getId()));
            if($providers){
                foreach ($providers as $provider){
                    $specialty_areas=$provider->getSpecialtiesArea();

                    $specialties_ids=[];
                    if(count($specialty_areas)>0){
                        foreach ($specialty_areas as $specialty_area){

                        }
                    }
                }
            }
        }
        die();
    }

    /**
     * @Route("/fhkc-upload-proccess", name="admin_fhkc_upload_proccess",methods={"POST"})
     */
    function uploadFHKCtoDatProccess(Request $request){
        set_time_limit(8600);
        $publicDirectory = $this->getParameter('kernel.project_dir');
        $file = $request->files->get('fhkc_load');

        $fileName = "";
        $fileExtension="";
        if ($file != "") {
            $nombreoriginal = $file->getClientOriginalName();
            $fileName = $nombreoriginal;
            $adjuntosDir = $this->getparameter('fhkc_loaded');
            $fileExtension=$file->guessExtension();
            $file->move($adjuntosDir, $fileName);
        }

        //step for read the excel and read de information
        $fileToRead = $this->getparameter('fhkc_loaded') . "/" . $fileName;

        if($fileExtension=="xlsx"){
            $reader = new Reader\Xlsx();
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($fileToRead);
            $spreadsheet->setActiveSheetIndex(1);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        }

        $cont=1;
        $folder = $publicDirectory.'/public/fhkc-reports';

        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }

        //read and convert the PG tab

        $date=date('Ymd');
        $name="BSN_FHK_Centralized_ProviderDirectory".$date.".dat";
        $file_name=$folder."/".$name;

        $string_to_save="";
        foreach ($sheetData as $sheet) {
            if ($cont > 2) {
                $a=$sheet['A']."|";
                $b=$sheet['B']."|";
                $c=$sheet['C']."|";
                $d=$sheet['D']."|";
                $e=$sheet['E']."|";
                $f=$sheet['F']."|";
                $g=$sheet['G']."|";
                $h=$sheet['H']."|";
                $i=$sheet['I']."|";
                $j=$sheet['J']."|";
                $k=$sheet['K']."|";
                $l=$sheet['L']."|";
                $m=$sheet['M']."|";
                $n=$sheet['N']."|";
                $o=$sheet['O']."|";
                $p=$sheet['P']."|";
                $q=$sheet['Q']."|";
                $r=$sheet['R']."|";
                $s=$sheet['S']."|";
                $t=$sheet['T']."|";
                $u=$sheet['U']."|";
                $w=$sheet['W']."|";
                $x=$sheet['X']."|";
                $y=$sheet['Y']."|";
                $z=$sheet['Z']."|";
                $aa=$sheet['AA']."|";
                $ab=$sheet['AB']."|";
                $ac=$sheet['AC']."|";
                $ad=$sheet['AD']."|";
                $ae=$sheet['AE']."|";
                $af=$sheet['AF']."|";
                $ag=$sheet['AG']."|";
                $ah=$sheet['AH']."|";
                $ai=$sheet['AI']."|";
                $aj=$sheet['AJ']."|";
                $ak=$sheet['AK']."|";
                $al=$sheet['AL']."|";
                $am=$sheet['AM']."|";
                $an=$sheet['AN']."|";
                $ao=$sheet['AO']."|";
                $ap=$sheet['AP']."|";
                $aq=$sheet['AQ'];

                $string_to_save.=$a.$b.$c.$d.$e.$f.$g.$h.$i.$j.$k.$l.$m.$n.$o.$p.$q.$r.$s.$t.$u.$w.$x.$y.$z.$aa.$ab.$ac.$ad.$ae.$af.$ag.$ah.$ai.$aj.$ak.$al.$am.$an.$ao.$ap.$aq;

                $last_chart=substr($string_to_save,-1);

                $string_to_save=$string_to_save."\n";
            }

            $cont++;
        }
        $file_dat = fopen($file_name,"w");
        fwrite($file_dat,$string_to_save);
        fclose($file_dat);

        return $this->redirectToRoute('home');
    }


    /**
     * @Route("/load-dea-information", name="admin_dea_information_proccess")
     */
    function loadDEAInformationProccess(Request $request)
    {
        set_time_limit(8600);

        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "data/Full_roster_9.28.csv";
        $projectDir = $this->getParameter('kernel.project_dir');

        $reader = new Reader\Csv();
        $spreadsheet = $reader->load($fileToRead);
        $reader->setDelimiter(',');
        $reader->setEnclosure('');
        $reader->setSheetIndex(0);

        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        $cont=1;

        foreach ($sheetData as $sheet) {
            if ($cont > 1) {
                if ($sheet['A'] != null or $sheet['A'] != "") {

                    $npi=$sheet['T'];
                    $dea_state=$sheet['CO'];
                    $dea_number=$sheet['CV'];
                    $dea_expirations=$sheet['CX'];
                    $license_granted_on=$sheet['AU'];


                    $dea_expirations=explode('-',$dea_expirations);
                    $dea_expirations_date="";
                    $month=$dea_expirations[0];
                    if(strlen($month)==1){
                        $month='0'.$month;
                    }

                    if(count($dea_expirations)>1){
                        $dea_expirations_date=$month."/".$dea_expirations[1]."/".$dea_expirations[2];
                    }

                    $license_granted_on=explode('-',$license_granted_on);
                    $license_granted_on_date="";
                    $month=$license_granted_on[0];
                    if(strlen($month)==1){
                        $month='0'.$month;
                    }

                    if(count($license_granted_on)>1){
                        $license_granted_on_date=$month."/".$license_granted_on[1]."/".$license_granted_on[2];
                    }


                    $providers=$em->getRepository('App:Provider')->findBy(array('npi_number'=>$npi));
                    if($providers){
                        foreach ($providers as $provider){
                            $degrees=$provider->getDegree();
                            $degree_pass=false;
                            foreach ($degrees as $degree){
                               if($degree->getName()=="MD" or $degree->getName()=="DO"){
                                   $degree_pass=true;
                               }
                            }

                            if($degree_pass==true){
                                $provider->setDeaNumber($dea_number);
                                $provider->setDeaState($dea_state);
                                $provider->setDeaExpirations($dea_expirations_date);

                                $em->persist($provider);
                            }

                            $provider->setStateLicenseEffectiveDate($license_granted_on_date);
                        }
                    }

                    $em->flush();
                }
            }

            $cont++;
        }

        die();
    }

    /**
     * @Route("/load-license-board-information", name="admin_license_board_information_proccess")
     */
    function loadLicenseBoardInformationProccess(Request $request){
        set_time_limit(8600);

        $em=$this->getDoctrine()->getManager();
        $fileToRead =  "data/Full_roster_9.28.csv";
        $projectDir = $this->getParameter('kernel.project_dir');

        $reader = new Reader\Csv();
        $spreadsheet = $reader->load($fileToRead);
        $reader->setDelimiter(',');
        $reader->setEnclosure('');
        $reader->setSheetIndex(0);

        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

        $cont=1;

        $current_date = strtotime(date("m/d/Y H:i:00",time()));

        foreach ($sheetData as $sheet) {
            if ($cont > 1) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    $npi=$sheet['T'];



                    $specialties_name=[$sheet['GM'], $sheet['GY'], $sheet['HK'], $sheet['HW'], $sheet['II']];
                    $board_cert_effective_date=[$sheet['GJ'],$sheet['GV'],$sheet['HH'],$sheet['HT'],$sheet['IF']];
                    $board_cert_exp_date=[$sheet['GK'],$sheet['GW'],$sheet['HI'],$sheet['HU'],$sheet['IG']];
                    $spec_board_cert=[$sheet['GP'],$sheet['HB'],$sheet['HN'],$sheet['HZ'],$sheet['IL']];

                    $providers=$em->getRepository('App:Provider')->findBy(array('npi_number'=>$npi));
                    if($providers) {
                        foreach ($providers as $provider) {

                            $cont_spe=0;
                            $specialties=$provider->getPrimarySpecialties();
                            if($specialties){
                                foreach ($specialties as $specialty){
                                    $specialty_name=$specialty->getName();

                                    while($cont_spe<4){
                                        $board_specialty_name=$specialties_name[$cont_spe];

                                        $specialty_math=false;
                                        if($specialties_name[$cont_spe]=="Child & Adolescent Psychiatry"){
                                            if($specialty_name=="Psychiatry Child"){
                                                $specialty_math=true;
                                            }
                                        }
                                        if($board_specialty_name==$specialty_name){
                                            $specialty_math=true;
                                        }

                                        if( $specialty_math==true){
                                            $dateYArray=[];
                                            if(stristr($board_cert_exp_date[$cont_spe], '/')) {
                                                $dateYArray=explode('/',$board_cert_exp_date[$cont_spe]);
                                            }

                                            if(stristr($board_cert_exp_date[$cont_spe], '-')) {
                                                $dateYArray=explode('-',$board_cert_exp_date[$cont_spe]);
                                            }

                                            $month_entry="";
                                            $day_entry="";

                                            if($board_cert_exp_date[$cont_spe]!=""){
                                                $month_entry=$dateYArray[1];
                                                $day_entry=$dateYArray[2];

                                                if(strlen($month_entry)==1){
                                                    $month_entry='0'.$month_entry;
                                                }

                                                if(strlen($day_entry)==1){
                                                    $day_entry='0'.$day_entry;
                                                }
                                            }

                                            if($board_cert_exp_date[$cont_spe]!=""){

                                                $dateentry=$month_entry."/".$day_entry."/".$dateYArray[0]." 01:00:00";
                                                $date_entry = strtotime($dateentry);

                                                if( $current_date < $date_entry){
                                                    echo $provider->getNpiNumber()." ".$dateentry."=>".$current_date."<br/>";
                                                }
                                            }
                                        }

                                        $cont_spe++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $cont++;
        }

        die();
    }
}
