<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OneController extends AbstractController
{

    /**
     * @Route("/one", name="admin_one_123")
     *
     */
    public function index(): Response
    {
        return $this->render('one/index.html.twig', [
            'controller_name' => 'OneController',
        ]);
    }
}
