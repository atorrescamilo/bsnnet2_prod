<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Languages;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/language")
 */
class LanguageController extends AbstractController{

    /**
     * @Route("/index", name="admin_language")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $languages = $em->getRepository('App\Entity\Languages')->findAll();
            $delete_form_ajax = $this->createCustomForm('LANGUAGE_ID', 'DELETE', 'admin_delete_language');

            return $this->render('language/index.html.twig', array('languages' => $languages, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_language")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('language/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_language", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Languages')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_language');
            }

            return $this->render('language/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_language", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Languages')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_language');
            }

            return $this->render('language/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_language")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $order = $request->get('order');
            $code = $request->get('code');
            $new = $request->get('new');
            $lang_code=$request->get('lang_code');

            $language = new Languages();
            $language->setName($name);
            $language->setOrder($order);
            $language->setCode($code);
            $language->setDescription($description);
            $language->setLangCd($lang_code);

            $em->persist($language);
            $em->flush();

            $this->addFlash(
                'success',
                'Language has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_language');
            }

            return $this->redirectToRoute('admin_language');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_language")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $order = $request->get('order');
            $code = $request->get('code');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');
            $lang_code=$request->get('lang_code');

            $language = $em->getRepository('App\Entity\Languages')->find($id);

            if ($language == null) {
                $this->addFlash(
                    "danger",
                    "The Language can't been updated!"
                );

                return $this->redirectToRoute('admin_language');
            }

            if ($language != null) {
                $language->setName($name);
                $language->setOrder($order);
                $language->setCode($code);
                $language->setDescription($description);
                $language->setLangCd($lang_code);

                $em->persist($language);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "Language has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_language');
            }

            return $this->redirectToRoute('admin_language');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_language",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $language = $language = $em->getRepository('App\Entity\Languages')->find($id);
            $removed = 0;
            $message = "";

            if ($language) {
                try {
                    $em->remove($language);
                    $em->flush();
                    $removed = 1;
                    $message = "The Language has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The language can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_language",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $language = $language = $em->getRepository('App\Entity\Languages')->find($id);

                if ($language) {
                    try {
                        $em->remove($language);
                        $em->flush();
                        $removed = 1;
                        $message = "The Language has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Languages can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
