<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\PayerUser;
use App\Entity\Provider;
use App\Entity\ProviderCredentialing;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/payer")
 */
class PayerPanelController extends AbstractController{

    /**
     * @Route("/dashboard", name="payer_dashboard")
     *
    */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $is_SuperAdmin=false;
            $roles=$this->getUser()->getRoles();
            foreach ($roles as $role){
                if($role=="PAYER_SUPER_ADMIN"){
                    $is_SuperAdmin=true;
                }
            }

            $user = $this->getUser();
            $payerObj=$user->getPayer();
            $payer = $payerObj->getId();

            if($user->getIsNew()==1){
                return $this->redirectToRoute('payer_profile_user');
            }

            if($user->getEnabled()==false){
                return $this->redirectToRoute('payer_login');
            }

            //All Payer
            if($payer==3){
                $sql="SELECT p.id,p.org_id,
            COUNT(DISTINCT `billing_address`.`id`) AS `billing_address_count`
            FROM  provider p 
            LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
            LEFT JOIN payer ON payer.id = provider_payer.payer_id
            LEFT JOIN provider_billing_address ON provider_billing_address.provider_id = p.id
            LEFT JOIN billing_address ON billing_address.id = provider_billing_address.billing_address_id
            WHERE p.disabled=0 and payer.id=1 or payer.id=2 or payer.id=4
            GROUP BY p.id
            ORDER BY p.first_name";
            }else{
                $sql="SELECT p.id,p.org_id,
            COUNT(DISTINCT `billing_address`.`id`) AS `billing_address_count`
            FROM  provider p 
            LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
            LEFT JOIN payer ON payer.id = provider_payer.payer_id
            LEFT JOIN provider_billing_address ON provider_billing_address.provider_id = p.id
            LEFT JOIN billing_address ON billing_address.id = provider_billing_address.billing_address_id
            WHERE p.disabled=0 and payer.id=".$payer."
            GROUP BY p.id
            ORDER BY p.first_name";
            }


            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $providers= $stmt->fetchAll();
            $size=count($providers);

            $orgs_id=[];
            $cont_addrs=0;
            foreach ($providers as $provider){
                $org_id=$provider['org_id'];
                if(!in_array($org_id,$orgs_id)){
                    $orgs_id[]=$org_id;
                }

                $cont_addrs+=intval($provider['billing_address_count']);
            }

            $total_organizations=count($orgs_id);

            $payers=$em->getRepository('App\Entity\Payer')->findBy(array('enabled'=>1),array('name'=>'ASC'));

            //All Payer
            if($payer==3){
                $sql2="SELECT billing_address.id
            FROM  provider p 
            LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
            LEFT JOIN payer ON payer.id = provider_payer.payer_id
            LEFT JOIN provider_billing_address ON provider_billing_address.provider_id=p.id
            LEFT JOIN billing_address on billing_address.id=provider_billing_address.billing_address_id
            LEFT JOIN organization on billing_address.organization_id=organization.id
            WHERE p.disabled=0 and payer.id=1 or payer.id=2 or payer.id=4
            GROUP BY billing_address.id
            ORDER BY billing_address.id";
            }else{
                $sql2="SELECT billing_address.id
            FROM  provider p 
            LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
            LEFT JOIN payer ON payer.id = provider_payer.payer_id
            LEFT JOIN provider_billing_address ON provider_billing_address.provider_id=p.id
            LEFT JOIN billing_address on billing_address.id=provider_billing_address.billing_address_id
            LEFT JOIN organization on billing_address.organization_id=organization.id
            WHERE p.disabled=0 and payer.id=".$payer."
            GROUP BY billing_address.id
            ORDER BY billing_address.id";
            }



            $stmt2 = $conn->prepare($sql2);
            $stmt2->execute();
            $addressResult= $stmt2->fetchAll();

            $cont_addrs=count($addressResult);

            return $this->render('payer_backend/dashboard.html.twig', array('size'=>$size,'payers'=>$payers,'super_admin'=>$is_SuperAdmin,'address_count'=>$cont_addrs,'total_organizations'=>$total_organizations));
        }else{
            return $this->redirectToRoute('payer_login');
        }
    }



    /**
     * @Route("/providers", name="payer_providers_list")
     *
    */
    public function providers_list(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $is_SuperAdmin=false;
            $roles=$this->getUser()->getRoles();
            foreach ($roles as $role){
                if($role=="PAYER_SUPER_ADMIN"){
                    $is_SuperAdmin=true;
                }
            }

            $user = $this->getUser();
            $payer = $user->getPayer()->getId();

            if($user->getIsNew()==1){
                return $this->redirectToRoute('payer_profile_user');
            }

            $conn = $em->getConnection();

            //ALL Payer
            if($payer==3){
                $sql="SELECT p.id, p.first_name, p.last_name,p.npi_number,
            GROUP_CONCAT(DISTINCT `degree`.`name` ORDER BY `degree`.`name` ASC SEPARATOR ', ') AS `degrees`,
            GROUP_CONCAT(DISTINCT `provider_type`.`name` ORDER BY `provider_type`.`name` ASC SEPARATOR ', ') AS `provider_types`,
            GROUP_CONCAT(DISTINCT `specialty`.`name` ORDER BY `specialty`.`name` ASC SEPARATOR ', ') AS `specialties`,
            GROUP_CONCAT(DISTINCT `languages`.`name` ORDER BY `languages`.`name` ASC SEPARATOR ', ') AS `languages`,
            GROUP_CONCAT(DISTINCT `organization`.`phone` ORDER BY `organization`.`phone` ASC SEPARATOR ', ') AS `phonenumber`,
            GROUP_CONCAT(DISTINCT `taxonomy_code`.`specialization` ORDER BY `taxonomy_code`.`id` ASC SEPARATOR ', ') AS `taxonomies_specialty`,
            organization.name as organization_name
            FROM  provider p 
            LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
            LEFT JOIN payer ON payer.id = provider_payer.payer_id
            LEFT JOIN provider_degree ON `provider_degree`.provider_id = p.id
            LEFT JOIN degree ON degree.id = provider_degree.degree_id
            LEFT JOIN organization ON organization.id = p.org_id
            LEFT JOIN provider_provider_types ON provider_provider_types.provider_id = p.id
            LEFT JOIN provider_type ON provider_type.id = provider_provider_types.provider_type_id
            LEFT JOIN provider_primary_specialties ON provider_primary_specialties.provider_id = p.id
            LEFT JOIN specialty ON specialty.id = provider_primary_specialties.primary_specialty_id
            LEFT JOIN provider_languages ON provider_languages.provider_id = p.id
            LEFT JOIN languages ON languages.id = provider_languages.languages_id
            LEFT JOIN organization_taxonomy ON organization_taxonomy.organization_id = organization.id
            LEFT JOIN taxonomy_code ON taxonomy_code.id = organization_taxonomy.taxonomy_id
            WHERE p.disabled=0 and (payer.id=1 or payer.id=2 or payer.id=4)
            GROUP BY p.id
            ORDER BY p.first_name";
            }else{
                $sql="SELECT p.id, p.first_name, p.last_name,p.npi_number,
            GROUP_CONCAT(DISTINCT `degree`.`name` ORDER BY `degree`.`name` ASC SEPARATOR ', ') AS `degrees`,
            GROUP_CONCAT(DISTINCT `provider_type`.`name` ORDER BY `provider_type`.`name` ASC SEPARATOR ', ') AS `provider_types`,
            GROUP_CONCAT(DISTINCT `specialty`.`name` ORDER BY `specialty`.`name` ASC SEPARATOR ', ') AS `specialties`,
            GROUP_CONCAT(DISTINCT `languages`.`name` ORDER BY `languages`.`name` ASC SEPARATOR ', ') AS `languages`,
            GROUP_CONCAT(DISTINCT `organization`.`phone` ORDER BY `organization`.`phone` ASC SEPARATOR ', ') AS `phonenumber`,
            GROUP_CONCAT(DISTINCT `taxonomy_code`.`specialization` ORDER BY `taxonomy_code`.`id` ASC SEPARATOR ', ') AS `taxonomies_specialty`,
            organization.name as organization_name
            FROM  provider p 
            LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
            LEFT JOIN payer ON payer.id = provider_payer.payer_id
            LEFT JOIN provider_degree ON `provider_degree`.provider_id = p.id
            LEFT JOIN degree ON degree.id = provider_degree.degree_id
            LEFT JOIN organization ON organization.id = p.org_id
            LEFT JOIN provider_provider_types ON provider_provider_types.provider_id = p.id
            LEFT JOIN provider_type ON provider_type.id = provider_provider_types.provider_type_id
            LEFT JOIN provider_primary_specialties ON provider_primary_specialties.provider_id = p.id
            LEFT JOIN specialty ON specialty.id = provider_primary_specialties.primary_specialty_id
            LEFT JOIN provider_languages ON provider_languages.provider_id = p.id
            LEFT JOIN languages ON languages.id = provider_languages.languages_id
            LEFT JOIN organization_taxonomy ON organization_taxonomy.organization_id = organization.id
            LEFT JOIN taxonomy_code ON taxonomy_code.id = organization_taxonomy.taxonomy_id
            WHERE p.disabled=0 and payer.id=".$payer."
            GROUP BY p.id
            ORDER BY p.first_name";
            }

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $providers= $stmt->fetchAll();

            return $this->render('payer_backend/providers.html.twig', array('providers'=>$providers,'payer'=>$payer));
        }else{
            return $this->redirectToRoute('payer_login');
        }
    }

    /**
     * @Route ("/provider_detail/{id}",name="provider_detail_payer",  defaults={"id": null})
    */
     public function viewProvider($id){
         if($this->isGranted('IS_AUTHENTICATED_FULLY')) {
             $em = $this->getDoctrine()->getManager();
             $provider = $em->getRepository('App\Entity\Provider')->find($id);

             $conn = $em->getConnection();
             $sql="SELECT ba.id, ba.street, ba.city, ba.us_state as state, ba.zip_code, ba.county, ba.region, ba.primary_addr as 'primary', ba.is_facility,ba.location_npi as npi, ba.phone_number as phone    
             FROM  billing_address ba
             LEFT JOIN provider_billing_address ON provider_billing_address.billing_address_id=ba.id
             LEFT JOIN provider ON provider.id =provider_billing_address.provider_id
             WHERE  provider.id=".$id."  
             GROUP BY ba.id
             ORDER BY ba.street";

             $stmt = $conn->prepare($sql);
             $stmt->execute();
             $addressResult= $stmt->fetchAll();

             $user = $this->getUser();
             $payer = $user->getPayer()->getId();

         return $this->render('payer_backend/provider_details.html.twig',array('provider'=>$provider,'address'=>$addressResult,'payer'=>$payer));

         }else{
             return $this->redirectToRoute('payer_login');
         }

     }

    /**
     * @Route("/load_payer_top_bar", name="load_payer_top_bar")
     *
     */
    public function load_payer_top_bar(){
        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {
        $em=$this->getDoctrine()->getManager();

        $payers=$em->getRepository('App\Entity\Payer')->findAll();
        $is_SuperAdmin=false;
        $roles=$this->getUser()->getRoles();
        foreach ($roles as $role){
            if($role=="PAYER_SUPER_ADMIN"){
                $is_SuperAdmin=true;
            }
        }

        $user = $this->getUser();
        $payerObj=$user->getPayer();

        return $this->render('payer_backend/payer_top.html.twig',[
            'payers'=>$payers,
            'super_admin'=>$is_SuperAdmin,
            'payer'=>$payerObj
        ]);
        }else{
            return $this->redirectToRoute('payer_login');
        }
    }

    /**
     * @Route("/set_user_payer", name="set_user_payer",methods={"POST"})
     *
     */
    public function set_user_payer(Request $request){
        $em=$this->getDoctrine()->getManager();
        $id=$request->get('payer');
        $payer=$em->getRepository('App\Entity\Payer')->find($id);
        $user=$this->getUser();

        if($payer!=null){
            $user->setPayer($payer);
            $em->persist($user);
            $em->flush();
        }

        return $this->redirectToRoute('payer_dashboard');
    }

    /**
     * @Route("/user-profile", name="payer_profile_user")
     *
     */
    public function payer_profile_user(){
        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $user = $this->getUser();

            return $this->render('payer_backend/profile.html.twig', array('user' => $user));
        }else{
            return $this->redirectToRoute('payer_login');
        }
    }

    /**
     * @Route("/user-profile-update", name="payer_user_profile_update",methods={"POST"})
     *
     */
    public function payer_profile_user_update(Request $request, UserPasswordEncoderInterface $encoder){
        if($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em=$this->getDoctrine()->getManager();

            $name=$request->get('name');
            $email=$request->get('email');
            $new_password=$request->get('new_password');

            $user = $this->getUser();
            $user->setName($name);
            $user->setEmail($email);
            $user->setIsNew(0);

            if($new_password!=""){
                $encode = $encoder->encodePassword($user, $new_password);
                $user->setPassword($encode);
            }

            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('payer_dashboard');

        }else{
            return $this->redirectToRoute('payer_login');
        }
    }

    /**
     * @Route("/recovery-password", name="payer_user_recovery")
     *
     */
    public function payer_user_recovery(){

        return $this->render('payeruser/password_recovery_email.html.twig');
    }

    /**
     * @Route("/check-email", name="payer_check_email")
     *
     */
    public function payer_check_email(){
        return $this->render('payeruser/check_email.html.twig');
    }


    /**
     * @Route("/send-email-recovery-password", name="send_payer_user_recovery", methods={"POST"})
     *
     */
    public function payer_user_recovery_email(Request $request,\Swift_Mailer $mailer){
        $em=$this->getDoctrine()->getManager();

        $email=$request->get('email');
        $code_hass=uniqid("ppr_",true);

        $user=$em->getRepository('App\Entity\PayerUser')->findOneBy(array('email'=>$email));

        if($user!=null){
            $user->setResetCodeHass($code_hass);
            $em->persist($user);
            $em->flush();

            $name=$user->getName();

            $link="";
            $server= $_SERVER['SERVER_NAME'];

            if($server=="127.0.0.1"){
                $link="http://127.0.0.1:8000/payer/set-new-password/".$code_hass;
            }else{
                $link="https://admin.bsnnet.com/payer/set-new-password/".$code_hass;
            }

            $message = (new \Swift_Message('Get a new Password'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email)
                ->setBody($this->renderView(
                    'email/templates/payer_recovery_password.html.twig',array('name'=>$name,'code_hass'=>$code_hass,'link'=>$link)
                    )
                , 'text/html');

            $mailer->send($message);
        }

        return $this->redirectToRoute('payer_check_email');
    }

    /**
     * @Route("/set-new-password/{code}", name="send_payer_user_set_password",methods={"GET"})
     *
     */
    public function payer_user_set_password($code){
        $em=$this->getDoctrine()->getManager();
        $user=$em->getRepository('App\Entity\PayerUser')->findOneBy(array('reset_code_hass'=>$code));

        return $this->render('payeruser/reset_password_form.html.twig',array('user'=>$user,'code'=>$code));
    }


    /**
     * @Route("/set-new-pasword", name="payer_user_save_new_password", methods={"POST"})
     *
     */
    public function payer_user_save_new_password(Request $request,UserPasswordEncoderInterface $encoder){

        $em=$this->getDoctrine()->getManager();
        $code=$request->get('code');
        $password=$request->get('password');

        $user=$em->getRepository('App\Entity\PayerUser')->findOneBy(array('reset_code_hass'=>$code));

        if($user!=null) {
            $encode = $encoder->encodePassword($user, $password);
            $user->setPassword($encode);

            $em->persist($user);
            $em->flush();
        }

     return  $this->redirectToRoute('payer_login');
    }

    /**
     * @Route("/organization-address", name="payer_address_list")
     *
     */
    public function address_list(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $is_SuperAdmin=false;
            $roles=$this->getUser()->getRoles();
            foreach ($roles as $role){
                if($role=="PAYER_SUPER_ADMIN"){
                    $is_SuperAdmin=true;
                }
            }

            $user = $this->getUser();
            $payer = $user->getPayer()->getId();

            if($user->getIsNew()==1){
                return $this->redirectToRoute('payer_profile_user');
            }

            if($payer==3){
                $sql="SELECT billing_address.id,billing_address.street, billing_address.suite_number,billing_address.county,billing_address.region,
            organization.name as org_name, billing_address.zip_code,billing_address.location_npi,billing_address.is_facility,
            COUNT(DISTINCT p.id) AS provider_count, organization.group_npi as group_npi,
            GROUP_CONCAT(DISTINCT `service_settings`.`name` ORDER BY `service_settings`.`id` ASC SEPARATOR ', ') AS `service_settings`,
            GROUP_CONCAT(DISTINCT `org_specialty`.`name` ORDER BY `org_specialty`.`name` ASC SEPARATOR ', ') AS `org_clasification`,
            GROUP_CONCAT(DISTINCT taxonomy_code.specialization  ORDER BY `taxonomy_code`.`id` ASC SEPARATOR ', ') AS `taxonomies`
            FROM  provider p 
            LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
            LEFT JOIN payer ON payer.id = provider_payer.payer_id
            LEFT JOIN provider_billing_address ON provider_billing_address.provider_id=p.id
            LEFT JOIN billing_address on billing_address.id=provider_billing_address.billing_address_id
            LEFT JOIN organization on billing_address.organization_id=organization.id
            LEFT JOIN billing_address_service_settings ON billing_address_service_settings.billing_address_id = billing_address.id
            LEFT JOIN organization_taxonomy ON organization_taxonomy.organization_id =organization.id
            LEFT JOIN taxonomy_code ON taxonomy_code.id =organization_taxonomy.taxonomy_id
            LEFT JOIN service_settings ON service_settings.id = billing_address_service_settings.service_settings_id
            LEFT JOIN organization_org_specialties ON organization_org_specialties.organization_id =organization.id    
            LEFT JOIN org_specialty ON org_specialty.id =organization_org_specialties.org_specialty_id    
            WHERE p.disabled=0 and payer.id=1 or payer.id=2 or payer.id=4
            GROUP BY billing_address.id
            ORDER BY billing_address.id";
            }else{
                $sql="SELECT billing_address.id,billing_address.street, billing_address.suite_number,billing_address.county,billing_address.region,
            organization.name as org_name, billing_address.zip_code,billing_address.location_npi,billing_address.is_facility,
            COUNT(DISTINCT p.id) AS provider_count, organization.group_npi as group_npi,
            GROUP_CONCAT(DISTINCT `service_settings`.`name` ORDER BY `service_settings`.`id` ASC SEPARATOR ', ') AS `service_settings`,
            GROUP_CONCAT(DISTINCT `org_specialty`.`name` ORDER BY `org_specialty`.`name` ASC SEPARATOR ', ') AS `org_clasification`,
            GROUP_CONCAT(DISTINCT taxonomy_code.specialization ORDER BY `taxonomy_code`.`id` ASC SEPARATOR ', ') AS `taxonomies`
            FROM  provider p 
            LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
            LEFT JOIN payer ON payer.id = provider_payer.payer_id
            LEFT JOIN provider_billing_address ON provider_billing_address.provider_id=p.id
            LEFT JOIN billing_address on billing_address.id=provider_billing_address.billing_address_id
            LEFT JOIN organization on billing_address.organization_id=organization.id
            LEFT JOIN organization_taxonomy ON organization_taxonomy.organization_id =organization.id
            LEFT JOIN taxonomy_code ON taxonomy_code.id =organization_taxonomy.taxonomy_id
            LEFT JOIN billing_address_service_settings ON billing_address_service_settings.billing_address_id = billing_address.id
            LEFT JOIN service_settings ON service_settings.id = billing_address_service_settings.service_settings_id
            LEFT JOIN organization_org_specialties ON organization_org_specialties.organization_id =organization.id    
            LEFT JOIN org_specialty ON org_specialty.id =organization_org_specialties.org_specialty_id    
            WHERE p.disabled=0 and payer.id=".$payer."
            GROUP BY billing_address.id
            ORDER BY billing_address.id";
            }



            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $addressResult= $stmt->fetchAll();


            return $this->render('payer_backend/address.html.twig',['address'=>$addressResult,'payer'=>$payer]);
        }else{
            return $this->redirectToRoute('payer_login');
        }
    }

    /**
     * @Route("/ibh-survey-aggregate-list", name="fcc_survey_ibh_survey_aggregate_list_2")
     */
    public function surveyAggregateList2(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $fcc_surveys = $em->getRepository('App\Entity\FCCSurvey')->findAll();

            return $this->render('fcc_survey/list2.html.twig', [
                'fcc_surveys'=>$fcc_surveys
            ]);
        }else{
            return $this->redirectToRoute('payer_login');
        }
    }

    /**
     * @Route("/ibh-survey-aggregate-data", name="fcc_survey_ibh_survey_aggregate_data2")
     */
    public function surveyAggregateData(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em=$this->getDoctrine()->getManager();

            $fcc_surveys=$em->getRepository('App\Entity\FCCSurvey')->findAll();
            $result=[];
            $p1_a=0; $p1_b=0; $p1_c=0;
            $p2_a=0; $p2_b=0; $p2_c=0;
            $p3_a=0; $p3_b=0; $p3_c=0;
            $p4_a=0; $p4_b=0; $p4_c=0;
            $p5_a=0; $p5_b=0; $p5_c=0;

            $cc1a_a=0;$cc1a_b=0;$cc1a_c=0;
            $cc1b_a=0;$cc1b_b=0;$cc1b_c=0;
            $cc1c_a=0;$cc1c_b=0;$cc1c_c=0;
            $cc2a_a=0;$cc2a_b=0;$cc2a_c=0;
            $cc2b_a=0;$cc2b_b=0;$cc2b_c=0;
            $cc3a_a=0;$cc3a_b=0;$cc3a_c=0;
            $cc3b_a=0;$cc3b_b=0;$cc3b_c=0;
            $cc3c_a=0;$cc3c_b=0;$cc3c_c=0;
            $cc3d_a=0;$cc3d_b=0;$cc3d_c=0;
            $cc3e_a=0;$cc3e_b=0;$cc3e_c=0;
            $cc3f_a=0;$cc3f_b=0;$cc3f_c=0;
            $cc4a_a=0;$cc4a_b=0;$cc4a_c=0;
            $cc4b_a=0;$cc4b_b=0;$cc4b_c=0;
            $cc4c_a=0;$cc4c_b=0;$cc4c_c=0;
            $cc4d_a=0;$cc4d_b=0;$cc4d_c=0;
            $cc4e_a=0;$cc4e_b=0;$cc4e_c=0;
            $cc4f_a=0;$cc4f_b=0;$cc4f_c=0;
            $cc5a_a=0;$cc5a_b=0;$cc5a_c=0;
            $cc5b_a=0;$cc5b_b=0;$cc5b_c=0;
            $cc5c_a=0;$cc5c_b=0;$cc5c_c=0;
            $cc6a_a=0;$cc6a_b=0;$cc6a_c=0;
            $cc6b_a=0;$cc6b_b=0;$cc6b_c=0;
            $cc6c_a=0;$cc6c_b=0;$cc6c_c=0;
            $cc7a_a=0;$cc7a_b=0;$cc7a_c=0;
            $cc7b_a=0;$cc7b_b=0;$cc7b_c=0;
            $cc7c_a=0;$cc7c_b=0;$cc7c_c=0;

            $total=count($fcc_surveys);

            if($fcc_surveys!=null){
                foreach ($fcc_surveys as $fcc_survey){
                    $p1=$fcc_survey->getP1();
                    $p2=$fcc_survey->getP2();
                    $p3=$fcc_survey->getP3();
                    $p4=$fcc_survey->getP4();
                    $p5=$fcc_survey->getP5();

                    $cc1a=$fcc_survey->getCc1a();
                    $cc1b=$fcc_survey->getCc1b();
                    $cc1c=$fcc_survey->getCc1c();
                    $cc2a=$fcc_survey->getCc2a();
                    $cc2b=$fcc_survey->getCc2b();
                    $cc3a=$fcc_survey->getCc3a();
                    $cc3b=$fcc_survey->getCc3b();
                    $cc3c=$fcc_survey->getCc3c();
                    $cc3d=$fcc_survey->getCc3d();
                    $cc3e=$fcc_survey->getCc3e();
                    $cc3f=$fcc_survey->getCc3f();
                    $cc4a=$fcc_survey->getCc4a();
                    $cc4b=$fcc_survey->getCc4b();
                    $cc4c=$fcc_survey->getCc4c();
                    $cc4d=$fcc_survey->getCc4d();
                    $cc4e=$fcc_survey->getCc4e();
                    $cc4f=$fcc_survey->getCc4f();
                    $cc5a=$fcc_survey->getCc5a();
                    $cc5b=$fcc_survey->getCc5b();
                    $cc5c=$fcc_survey->getCc5c();
                    $cc6a=$fcc_survey->getCc6a();
                    $cc6b=$fcc_survey->getCc6b();
                    $cc6c=$fcc_survey->getCc6c();
                    $cc7a=$fcc_survey->getCc7a();
                    $cc7b=$fcc_survey->getCc7b();
                    $cc7c=$fcc_survey->getCc7c();

                    if($p1==1) $p1_a++; if($p1==2) $p1_b++; if($p1==3) $p1_c++;
                    if($p2==1) $p2_a++; if($p2==2) $p2_b++; if($p2==3) $p2_c++;
                    if($p3==1) $p3_a++; if($p3==2) $p3_b++; if($p3==3) $p3_c++;
                    if($p4==1) $p4_a++; if($p4==2) $p4_b++; if($p4==3) $p4_c++;
                    if($p5==1) $p5_a++; if($p5==2) $p5_b++; if($p5==3) $p5_c++;

                    if($cc1a==1) $cc1a_a++; if($cc1a==2) $cc1a_b++; if($cc1a==3) $cc1a_c++;
                    if($cc1b==1) $cc1b_a++; if($cc1b==2) $cc1b_b++; if($cc1b==3) $cc1b_c++;
                    if($cc1c==1) $cc1c_a++; if($cc1c==2) $cc1c_b++; if($cc1c==3) $cc1c_c++;

                    if($cc2a==1) $cc2a_a++; if($cc2a==2) $cc2a_b++; if($cc2a==3) $cc2a_c++;
                    if($cc2b==1) $cc2b_a++; if($cc2b==2) $cc2b_b++; if($cc2b==3) $cc2b_c++;

                    if($cc3a==1) $cc3a_a++; if($cc3a==2) $cc3a_b++; if($cc3a==3) $cc3a_c++;
                    if($cc3b==1) $cc3b_a++; if($cc3b==2) $cc3b_b++; if($cc3b==3) $cc3b_c++;
                    if($cc3c==1) $cc3c_a++; if($cc3c==2) $cc3c_b++; if($cc3c==3) $cc3c_c++;
                    if($cc3d==1) $cc3d_a++; if($cc3d==2) $cc3d_b++; if($cc3d==3) $cc3d_c++;
                    if($cc3e==1) $cc3e_a++; if($cc3e==2) $cc3e_b++; if($cc3e==3) $cc3e_c++;
                    if($cc3f==1) $cc3f_a++; if($cc3f==2) $cc3f_b++; if($cc3f==3) $cc3f_c++;

                    if($cc4a==1) $cc4a_a++; if($cc4a==2) $cc4a_b++; if($cc4a==3) $cc4a_c++;
                    if($cc4b==1) $cc4b_a++; if($cc4b==2) $cc4b_b++; if($cc4b==3) $cc4b_c++;
                    if($cc4c==1) $cc4c_a++; if($cc4c==2) $cc4c_b++; if($cc4c==3) $cc4c_c++;
                    if($cc4d==1) $cc4d_a++; if($cc4d==2) $cc4d_b++; if($cc4d==3) $cc4d_c++;
                    if($cc4e==1) $cc4e_a++; if($cc4e==2) $cc4e_b++; if($cc4e==3) $cc4e_c++;
                    if($cc4f==1) $cc4f_a++; if($cc4f==2) $cc4f_b++; if($cc4f==3) $cc4f_c++;

                    if($cc5a==1) $cc5a_a++; if($cc5a==2) $cc5a_b++; if($cc5a==3) $cc5a_c++;
                    if($cc5b==1) $cc5b_a++; if($cc5b==2) $cc5b_b++; if($cc5b==3) $cc5b_c++;
                    if($cc5c==1) $cc5c_a++; if($cc5c==2) $cc5c_b++; if($cc5c==3) $cc5c_c++;

                    if($cc6a==1) $cc6a_a++; if($cc6a==2) $cc6a_b++; if($cc6a==3) $cc6a_c++;
                    if($cc6b==1) $cc6b_a++; if($cc6b==2) $cc6b_b++; if($cc6b==3) $cc6b_c++;
                    if($cc6c==1) $cc6c_a++; if($cc6c==2) $cc6c_b++; if($cc6c==3) $cc6c_c++;

                    if($cc7a==1) $cc7a_a++; if($cc7a==2) $cc7a_b++; if($cc7a==3) $cc7a_c++;
                    if($cc7b==1) $cc7b_a++; if($cc7b==2) $cc7b_b++; if($cc7b==3) $cc7b_c++;
                    if($cc7c==1) $cc7c_a++; if($cc7c==2) $cc7c_b++; if($cc7c==3) $cc7c_c++;
                }
            }

            $result['p1'][0]=$p1_a; $result['p1'][1]=$p1_b; $result['p1'][2]=$p1_c;
            $result['p2'][0]=$p2_a; $result['p2'][1]=$p2_b; $result['p2'][2]=$p2_c;
            $result['p3'][0]=$p3_a; $result['p3'][1]=$p3_b; $result['p3'][2]=$p3_c;
            $result['p4'][0]=$p4_a; $result['p4'][1]=$p4_b; $result['p4'][2]=$p4_c;
            $result['p5'][0]=$p5_a; $result['p5'][1]=$p5_b; $result['p5'][2]=$p5_c;

            $result['cc1a'][0]=$cc1a_a; $result['cc1a'][1]=$cc1a_b; $result['cc1a'][2]=$cc1a_c;
            $result['cc1b'][0]=$cc1b_a; $result['cc1b'][1]=$cc1b_b; $result['cc1b'][2]=$cc1b_c;
            $result['cc1c'][0]=$cc1c_a; $result['cc1c'][1]=$cc1c_b; $result['cc1c'][2]=$cc1c_c;

            $result['cc2a'][0]=$cc2a_a; $result['cc2a'][1]=$cc2a_b; $result['cc2a'][2]=$cc2a_c;
            $result['cc2b'][0]=$cc2b_a; $result['cc2b'][1]=$cc2b_b; $result['cc2b'][2]=$cc2b_c;

            $result['cc3a'][0]=$cc3a_a; $result['cc3a'][1]=$cc3a_b; $result['cc3a'][2]=$cc3a_c;
            $result['cc3b'][0]=$cc3b_a; $result['cc3b'][1]=$cc3b_b; $result['cc3b'][2]=$cc3b_c;
            $result['cc3c'][0]=$cc3c_a; $result['cc3c'][1]=$cc3c_b; $result['cc3c'][2]=$cc3c_c;
            $result['cc3d'][0]=$cc3d_a; $result['cc3d'][1]=$cc3d_b; $result['cc3d'][2]=$cc3d_c;
            $result['cc3e'][0]=$cc3e_a; $result['cc3e'][1]=$cc3e_b; $result['cc3e'][2]=$cc3e_c;
            $result['cc3f'][0]=$cc3f_a; $result['cc3f'][1]=$cc3f_b; $result['cc3f'][2]=$cc3f_c;

            $result['cc4a'][0]=$cc4a_a; $result['cc4a'][1]=$cc4a_b; $result['cc4a'][2]=$cc4a_c;
            $result['cc4b'][0]=$cc4b_a; $result['cc4b'][1]=$cc4b_b; $result['cc4b'][2]=$cc4b_c;
            $result['cc4c'][0]=$cc4c_a; $result['cc4c'][1]=$cc4c_b; $result['cc4c'][2]=$cc4c_c;
            $result['cc4d'][0]=$cc4d_a; $result['cc4d'][1]=$cc4d_b; $result['cc4d'][2]=$cc4d_c;
            $result['cc4e'][0]=$cc4e_a; $result['cc4e'][1]=$cc4e_b; $result['cc4e'][2]=$cc4e_c;
            $result['cc4f'][0]=$cc4f_a; $result['cc4f'][1]=$cc4f_b; $result['cc4f'][2]=$cc4f_c;

            $result['cc5a'][0]=$cc5a_a; $result['cc5a'][1]=$cc5a_b; $result['cc5a'][2]=$cc5a_c;
            $result['cc5b'][0]=$cc5b_a; $result['cc5b'][1]=$cc5b_b; $result['cc5b'][2]=$cc5b_c;
            $result['cc5c'][0]=$cc5c_a; $result['cc5c'][1]=$cc5c_b; $result['cc5c'][2]=$cc5c_c;

            $result['cc6a'][0]=$cc6a_a; $result['cc6a'][1]=$cc6a_b; $result['cc6a'][2]=$cc6a_c;
            $result['cc6b'][0]=$cc6b_a; $result['cc6b'][1]=$cc6b_b; $result['cc6b'][2]=$cc6b_c;
            $result['cc6c'][0]=$cc6c_a; $result['cc6c'][1]=$cc6c_b; $result['cc6c'][2]=$cc6c_c;

            $result['cc7a'][0]=$cc7a_a; $result['cc7a'][1]=$cc7a_b; $result['cc7a'][2]=$cc7a_c;
            $result['cc7b'][0]=$cc7b_a; $result['cc7b'][1]=$cc7b_b; $result['cc7b'][2]=$cc7b_c;
            $result['cc7c'][0]=$cc7c_a; $result['cc7c'][1]=$cc7c_b; $result['cc7c'][2]=$cc7c_c;

            $p1_a_p=0;$p1_b_p=0;$p1_c_p=0;
            $p2_a_p=0;$p2_b_p=0;$p2_c_p=0;$p3_a_p=0;
            $p3_b_p=0;$p3_c_p=0;$p4_a_p=0;
            $p4_b_p=0;$p4_c_p=0;$p5_a_p=0;
            $p5_b_p=0;$p5_c_p=0;

            $cc1a_a_p=0;$cc1a_b_p=0;$cc1a_c_p=0;$cc1b_a_p=0;$cc1b_b_p=0;
            $cc1b_c_p=0;$cc1c_a_p=0;$cc1c_b_p=0;$cc1c_c_p=0;$cc2a_a_p=0;
            $cc2a_b_p=0;$cc2a_c_p=0;$cc2b_a_p=0;$cc2b_b_p=0;$cc2b_c_p=0;
            $cc3a_a_p=0;$cc3a_b_p=0;$cc3a_c_p=0;$cc3b_a_p=0;$cc3b_b_p=0;
            $cc3b_c_p=0;$cc3c_a_p=0;$cc3c_b_p=0;$cc3c_c_p=0;$cc3d_a_p=0;
            $cc3d_b_p=0;$cc3d_c_p=0;$cc3e_a_p=0;$cc3e_b_p=0;$cc3e_c_p=0;
            $cc3f_a_p=0;$cc3f_b_p=0;$cc3f_c_p=0;$cc4a_a_p=0;$cc4a_b_p=0;
            $cc4a_c_p=0;$cc4b_a_p=0;$cc4b_b_p=0;$cc4b_c_p=0;$cc4c_a_p=0;
            $cc4c_b_p=0;$cc4c_c_p=0;$cc4d_a_p=0;$cc4d_b_p=0;$cc4d_c_p=0;
            $cc4e_a_p=0;$cc4e_b_p=0;$cc4e_c_p=0;$cc4f_a_p=0;$cc4f_b_p=0;
            $cc4f_c_p=0;$cc5a_a_p=0;$cc5a_b_p=0;$cc5a_c_p=0;$cc5b_a_p=0;
            $cc5b_b_p=0;$cc5b_c_p=0;$cc5c_a_p=0;$cc5c_b_p=0;$cc5c_c_p=0;
            $cc6a_a_p=0;$cc6a_b_p=0;$cc6a_c_p=0;$cc6b_a_p=0;$cc6b_b_p=0;
            $cc6b_c_p=0;$cc6c_a_p=0;$cc6c_b_p=0;$cc6c_c_p=0;$cc7a_a_p=0;
            $cc7a_b_p=0;$cc7a_c_p=0;$cc7b_a_p=0;$cc7b_b_p=0;$cc7b_c_p=0;
            $cc7c_a_p=0;$cc7c_b_p=0;$cc7c_c_p=0;

            if($total>0){
                $p1_a_p=round(($p1_a*100)/$total,2);
                $p1_b_p=round(($p1_b*100)/$total,2);
                $p1_c_p=round(($p1_c*100)/$total,2);
                $p2_a_p=round(($p2_a*100)/$total,2);
                $p2_b_p=round(($p2_b*100)/$total,2);
                $p2_c_p=round(($p2_c*100)/$total,2);
                $p3_a_p=round(($p3_a*100)/$total,2);
                $p3_b_p=round(($p3_b*100)/$total,2);
                $p3_c_p=round(($p3_c*100)/$total,2);
                $p4_a_p=round(($p4_a*100)/$total,2);
                $p4_b_p=round(($p4_b*100)/$total,2);
                $p4_c_p=round(($p4_c*100)/$total,2);
                $p5_a_p=round(($p5_a*100)/$total,2);
                $p5_b_p=round(($p5_b*100)/$total,2);
                $p5_c_p=round(($p5_c*100)/$total,2);

                $cc1a_a_p=round(($cc1a_a*100)/$total,2);
                $cc1a_b_p=round(($cc1a_b*100)/$total,2);
                $cc1a_c_p=round(($cc1a_c*100)/$total,2);
                $cc1b_a_p=round(($cc1b_a*100)/$total,2);
                $cc1b_b_p=round(($cc1b_b*100)/$total,2);
                $cc1b_c_p=round(($cc1b_c*100)/$total,2);
                $cc1c_a_p=round(($cc1c_a*100)/$total,2);
                $cc1c_b_p=round(($cc1c_b*100)/$total,2);
                $cc1c_c_p=round(($cc1c_c*100)/$total,2);
                $cc2a_a_p=round(($cc2a_a*100)/$total,2);
                $cc2a_b_p=round(($cc2a_b*100)/$total,2);
                $cc2a_c_p=round(($cc2a_c*100)/$total,2);
                $cc2b_a_p=round(($cc2b_a*100)/$total,2);
                $cc2b_b_p=round(($cc2b_b*100)/$total,2);
                $cc2b_c_p=round(($cc2b_c*100)/$total,2);
                $cc3a_a_p=round(($cc3a_a*100)/$total,2);
                $cc3a_b_p=round(($cc3a_b*100)/$total,2);
                $cc3a_c_p=round(($cc3a_c*100)/$total,2);
                $cc3b_a_p=round(($cc3b_a*100)/$total,2);
                $cc3b_b_p=round(($cc3b_b*100)/$total,2);
                $cc3b_c_p=round(($cc3b_c*100)/$total,2);
                $cc3c_a_p=round(($cc3c_a*100)/$total,2);
                $cc3c_b_p=round(($cc3c_b*100)/$total,2);
                $cc3c_c_p=round(($cc3c_c*100)/$total,2);
                $cc3d_a_p=round(($cc3d_a*100)/$total,2);
                $cc3d_b_p=round(($cc3d_b*100)/$total,2);
                $cc3d_c_p=round(($cc3d_c*100)/$total,2);
                $cc3e_a_p=round(($cc3e_a*100)/$total,2);
                $cc3e_b_p=round(($cc3e_b*100)/$total,2);
                $cc3e_c_p=round(($cc3e_c*100)/$total,2);
                $cc3f_a_p=round(($cc3f_a*100)/$total,2);
                $cc3f_b_p=round(($cc3f_b*100)/$total,2);
                $cc3f_c_p=round(($cc3f_c*100)/$total,2);
                $cc4a_a_p=round(($cc4a_a*100)/$total,2);
                $cc4a_b_p=round(($cc4a_b*100)/$total,2);
                $cc4a_c_p=round(($cc4a_c*100)/$total,2);
                $cc4b_a_p=round(($cc4b_a*100)/$total,2);
                $cc4b_b_p=round(($cc4b_b*100)/$total,2);
                $cc4b_c_p=round(($cc4b_c*100)/$total,2);
                $cc4c_a_p=round(($cc4c_a*100)/$total,2);
                $cc4c_b_p=round(($cc4c_b*100)/$total,2);
                $cc4c_c_p=round(($cc4c_c*100)/$total,2);
                $cc4d_a_p=round(($cc4d_a*100)/$total,2);
                $cc4d_b_p=round(($cc4d_b*100)/$total,2);
                $cc4d_c_p=round(($cc4d_c*100)/$total,2);
                $cc4e_a_p=round(($cc4e_a*100)/$total,2);
                $cc4e_b_p=round(($cc4e_b*100)/$total,2);
                $cc4e_c_p=round(($cc4e_c*100)/$total,2);
                $cc4f_a_p=round(($cc4f_a*100)/$total,2);
                $cc4f_b_p=round(($cc4f_b*100)/$total,2);
                $cc4f_c_p=round(($cc4f_c*100)/$total,2);
                $cc5a_a_p=round(($cc5a_a*100)/$total,2);
                $cc5a_b_p=round(($cc5a_b*100)/$total,2);
                $cc5a_c_p=round(($cc5a_c*100)/$total,2);
                $cc5b_a_p=round(($cc5b_a*100)/$total,2);
                $cc5b_b_p=round(($cc5b_b*100)/$total,2);
                $cc5b_c_p=round(($cc5b_c*100)/$total,2);
                $cc5c_a_p=round(($cc5c_a*100)/$total,2);
                $cc5c_b_p=round(($cc5c_b*100)/$total,2);
                $cc5c_c_p=round(($cc5c_c*100)/$total,2);
                $cc6a_a_p=round(($cc6a_a*100)/$total,2);
                $cc6a_b_p=round(($cc6a_b*100)/$total,2);
                $cc6a_c_p=round(($cc6a_c*100)/$total,2);
                $cc6b_a_p=round(($cc6b_a*100)/$total,2);
                $cc6b_b_p=round(($cc6b_b*100)/$total,2);
                $cc6b_c_p=round(($cc6b_c*100)/$total,2);
                $cc6c_a_p=round(($cc6c_a*100)/$total,2);
                $cc6c_b_p=round(($cc6c_b*100)/$total,2);
                $cc6c_c_p=round(($cc6c_c*100)/$total,2);
                $cc7a_a_p=round(($cc7a_a*100)/$total,2);
                $cc7a_b_p=round(($cc7a_b*100)/$total,2);
                $cc7a_c_p=round(($cc7a_c*100)/$total,2);
                $cc7b_a_p=round(($cc7b_a*100)/$total,2);
                $cc7b_b_p=round(($cc7b_b*100)/$total,2);
                $cc7b_c_p=round(($cc7b_c*100)/$total,2);
                $cc7c_a_p=round(($cc7c_a*100)/$total,2);
                $cc7c_b_p=round(($cc7c_b*100)/$total,2);
                $cc7c_c_p=round(($cc7c_c*100)/$total,2);
            }


            $result['p1_p'][0]=$p1_a_p;$result['p1_p'][1]=$p1_b_p;$result['p1_p'][2]=$p1_c_p;
            $result['p2_p'][0]=$p2_a_p;$result['p2_p'][1]=$p2_b_p;$result['p2_p'][2]=$p2_c_p;
            $result['p3_p'][0]=$p3_a_p;$result['p3_p'][1]=$p3_b_p;$result['p3_p'][2]=$p3_c_p;
            $result['p4_p'][0]=$p4_a_p;$result['p4_p'][1]=$p4_b_p;$result['p4_p'][2]=$p4_c_p;
            $result['p5_p'][0]=$p5_a_p;$result['p5_p'][1]=$p5_b_p;$result['p5_p'][2]=$p5_c_p;

            $result['cc1a_p'][0]=$cc1a_a_p; $result['cc1a_p'][1]=$cc1a_b_p; $result['cc1a_p'][2]=$cc1a_c_p;
            $result['cc1b_p'][0]=$cc1b_a_p; $result['cc1b_p'][1]=$cc1b_b_p; $result['cc1b_p'][2]=$cc1b_c_p;
            $result['cc1c_p'][0]=$cc1c_a_p; $result['cc1c_p'][1]=$cc1c_b_p; $result['cc1c_p'][2]=$cc1c_c_p;

            $result['cc2a_p'][0]=$cc2a_a_p; $result['cc2a_p'][1]=$cc2a_b_p; $result['cc2a_p'][2]=$cc2a_c_p;
            $result['cc2b_p'][0]=$cc2b_a_p; $result['cc2b_p'][1]=$cc2b_b_p; $result['cc2b_p'][2]=$cc2b_c_p;

            $result['cc3a_p'][0]=$cc3a_a_p; $result['cc3a_p'][1]=$cc3a_b_p; $result['cc3a_p'][2]=$cc3a_c_p;
            $result['cc3b_p'][0]=$cc3b_a_p; $result['cc3b_p'][1]=$cc3b_b_p; $result['cc3b_p'][2]=$cc3b_c_p;
            $result['cc3c_p'][0]=$cc3c_a_p; $result['cc3c_p'][1]=$cc3c_b_p; $result['cc3c_p'][2]=$cc3c_c_p;
            $result['cc3d_p'][0]=$cc3d_a_p; $result['cc3d_p'][1]=$cc3d_b_p; $result['cc3d_p'][2]=$cc3d_c_p;
            $result['cc3e_p'][0]=$cc3e_a_p; $result['cc3e_p'][1]=$cc3e_b_p; $result['cc3e_p'][2]=$cc3e_c_p;
            $result['cc3f_p'][0]=$cc3f_a_p; $result['cc3f_p'][1]=$cc3f_b_p; $result['cc3f_p'][2]=$cc3f_c_p;

            $result['cc4a_p'][0]=$cc4a_a_p; $result['cc4a_p'][1]=$cc4a_b_p; $result['cc4a_p'][2]=$cc4a_c_p;
            $result['cc4b_p'][0]=$cc4b_a_p; $result['cc4b_p'][1]=$cc4b_b_p; $result['cc4b_p'][2]=$cc4b_c_p;
            $result['cc4c_p'][0]=$cc4c_a_p; $result['cc4c_p'][1]=$cc4c_b_p; $result['cc4c_p'][2]=$cc4c_c_p;
            $result['cc4d_p'][0]=$cc4d_a_p; $result['cc4d_p'][1]=$cc4d_b_p; $result['cc4d_p'][2]=$cc4d_c_p;
            $result['cc4e_p'][0]=$cc4e_a_p; $result['cc4e_p'][1]=$cc4e_b_p; $result['cc4e_p'][2]=$cc4e_c_p;
            $result['cc4f_p'][0]=$cc4f_a_p; $result['cc4f_p'][1]=$cc4f_b_p; $result['cc4f_p'][2]=$cc4f_c_p;

            $result['cc5a_p'][0]=$cc5a_a_p; $result['cc5a_p'][1]=$cc5a_b_p; $result['cc5a_p'][2]=$cc5a_c_p;
            $result['cc5b_p'][0]=$cc5b_a_p; $result['cc5b_p'][1]=$cc5b_b_p; $result['cc5b_p'][2]=$cc5b_c_p;
            $result['cc5c_p'][0]=$cc5c_a_p; $result['cc5c_p'][1]=$cc5c_b_p; $result['cc5c_p'][2]=$cc5c_c_p;

            $result['cc6a_p'][0]=$cc6a_a_p; $result['cc6a_p'][1]=$cc6a_b_p; $result['cc6a_p'][2]=$cc6a_c_p;
            $result['cc6b_p'][0]=$cc6b_a_p; $result['cc6b_p'][1]=$cc6b_b_p; $result['cc6b_p'][2]=$cc6b_c_p;
            $result['cc6c_p'][0]=$cc6c_a_p; $result['cc6c_p'][1]=$cc6c_b_p; $result['cc6c_p'][2]=$cc6c_c_p;

            $result['cc7a_p'][0]=$cc7a_a_p; $result['cc7a_p'][1]=$cc7a_b_p; $result['cc7a_p'][2]=$cc7a_c_p;
            $result['cc7b_p'][0]=$cc7b_a_p; $result['cc7b_p'][1]=$cc7b_b_p; $result['cc7b_p'][2]=$cc7b_c_p;
            $result['cc7c_p'][0]=$cc7c_a_p; $result['cc7c_p'][1]=$cc7c_b_p; $result['cc7c_p'][2]=$cc7c_c_p;


            return $this->render('fcc_survey/survey_aggregate_data2.html.twig', [
                'total' => $total, 'result'=>$result
            ]);
        }else{
            return $this->redirectToRoute('payer_login');
        }
    }


}
