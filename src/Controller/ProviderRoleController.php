<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\ProviderRole;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/providerrole")
 */
class ProviderRoleController extends AbstractController{
    /**
     * @Route("/index", name="admin_providerrole")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $providerroles = $em->getRepository('App\Entity\ProviderRole')->findAll();

            $delete_form_ajax = $this->createCustomForm('PROVIDERROLE_ID', 'DELETE', 'admin_delete_providerrole');


            return $this->render('providerrole/index.html.twig', array('providerroles' => $providerroles, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{

        }
    }

    /**
     * @Route("/new", name="admin_new_providerrole")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('providerrole/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_providerrole", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ProviderRole')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_providerrole');
            }

            return $this->render('providerrole/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_providerrole", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ProviderRole')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_providerrole');
            }

            return $this->render('providerrole/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_providerrole")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');

            $providerrole = new ProviderRole();
            $providerrole->setName($name);
            $providerrole->setDescription($description);

            $em->persist($providerrole);
            $em->flush();

            $this->addFlash(
                'success',
                'ProviderRole has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_providerrole');
            }

            return $this->redirectToRoute('admin_providerrole');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_providerrole")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $providerrole = $em->getRepository('App\Entity\ProviderRole')->find($id);

            if ($providerrole == null) {
                $this->addFlash(
                    "danger",
                    "The ProviderRole can't been updated!"
                );

                return $this->redirectToRoute('admin_providerrole');
            }

            if ($providerrole != null) {
                $providerrole->setName($name);
                $providerrole->setDescription($description);

                $em->persist($providerrole);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "ProviderRole has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_providerrole');
            }

            return $this->redirectToRoute('admin_providerrole');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_providerrole",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $providerrole = $providerrole = $em->getRepository('App\Entity\ProviderRole')->find($id);
            $removed = 0;
            $message = "";

            if ($providerrole) {
                try {
                    $em->remove($providerrole);
                    $em->flush();
                    $removed = 1;
                    $message = "The ProviderRole has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The providerrole can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_providerrole",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $providerrole = $providerrole = $em->getRepository('App\Entity\ProviderRole')->find($id);

                if ($providerrole) {
                    try {
                        $em->remove($providerrole);
                        $em->flush();
                        $removed = 1;
                        $message = "The ProviderRole has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The ProviderRole can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}

