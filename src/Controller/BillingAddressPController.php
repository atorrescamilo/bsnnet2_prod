<?php

namespace App\Controller;

use App\Entity\HoursOperationSchema;
use App\Entity\OrganizationP;
use App\Entity\Provider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\BillingAddressP;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Service\BillingAddressFileUploader;

/**
 * @Route("/provider/billing_p")
 */
class BillingAddressPController extends AbstractController{

    /**
     * @Route("/index", name="provider_billing")
     *
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            //temp organization
            $user = $this->getUser();

            $delete_form_ajax = $this->createCustomForm('BILLINGADDRESS_ID', 'DELETE', 'provider_delete_billing');

            $billing_address = $em->getRepository('App\Entity\BillingAddressP')->findBy(array('user' => $user->getId()));

            return $this->render('billing_address_p/index.html.twig', array('delete_form_ajax' => $delete_form_ajax->createView(), 'billing_address' => $billing_address));
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/new", name="provider_new_billing")
     *
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $user = $this->getUser();
            $serviceSettings = $em->getRepository('App\Entity\ServiceSettings')->findBy(array(),array('name'=>'ASC'));
            $facilityType = $em->getRepository('App\Entity\FacilityType')->findBy(array(),array('name'=>'ASC'));

            $totalserviceSettings=count($serviceSettings);
            $serviceSettings_first_column_elements=($totalserviceSettings/3);
            if(is_float($serviceSettings_first_column_elements)){
                $serviceSettings_first_column_elements=intval($serviceSettings_first_column_elements+1);
            }
            $serviceSettings_rest_columns_element=intval( ($totalserviceSettings-$serviceSettings_first_column_elements)/2);

            $totalfacilityType=count($facilityType);
            $facilityType_first_column_elements=($totalfacilityType/3);
            if(is_float($facilityType_first_column_elements)){
                $facilityType_first_column_elements=intval($facilityType_first_column_elements+1);
            }
            $facilityType_rest_columns_element=intval( ($totalfacilityType-$facilityType_first_column_elements)/2);


            $google_apikey = $this->getParameter('google_apikey');

            $providers = $em->getRepository('App\Entity\ProviderP')->findBy(array('user' => $user->getId()));

            //get if exist the hours Operation Schema
            $hoursOperationSchema="";

            return $this->render('billing_address_p/add.html.twig', array('hoursOperationSchema'=>$hoursOperationSchema,'google_apikey' => $google_apikey, 'serviceSettings' => $serviceSettings, 'facilityType' => $facilityType, 'providers' => $providers,
              'totalserviceSettings'=>$totalserviceSettings,'serviceSettings_first_column_elements'=>$serviceSettings_first_column_elements,'serviceSettings_rest_columns_element'=>$serviceSettings_rest_columns_element,
                'totalfacilityType'=>$totalfacilityType,'facilityType_first_column_elements'=>$facilityType_first_column_elements,'facilityType_rest_columns_element'=>$facilityType_rest_columns_element));
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="provider_edit_billing",defaults={"id": null})
     *
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $db = $em->getConnection();
            $user = $this->getUser();
            $document = $em->getRepository('App\Entity\BillingAddressP')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('provider_billing');
            }

            $serviceSettings = $em->getRepository('App\Entity\ServiceSettings')->findBy(array(),array('name'=>'ASC'));
            $facilityType = $em->getRepository('App\Entity\FacilityType')->findBy(array(),array('name'=>'ASC'));

            $organization = 1;
            $google_apikey = $this->getParameter('google_apikey');
            $providers = $em->getRepository('App\Entity\ProviderP')->findBy(array('user' => $user->getId()));

            $totalserviceSettings=count($serviceSettings);
            $serviceSettings_first_column_elements=($totalserviceSettings/3);
            if(is_float($serviceSettings_first_column_elements)){
                $serviceSettings_first_column_elements=intval($serviceSettings_first_column_elements+1);
            }
            $serviceSettings_rest_columns_element=intval( ($totalserviceSettings-$serviceSettings_first_column_elements)/2);

            $totalfacilityType=count($facilityType);
            $facilityType_first_column_elements=($totalfacilityType/3);
            if(is_float($facilityType_first_column_elements)){
                $facilityType_first_column_elements=intval($facilityType_first_column_elements+1);
            }
            $facilityType_rest_columns_element=intval( ($totalfacilityType-$facilityType_first_column_elements)/2);


            $query1 = "SELECT * FROM providerp_billing_address WHERE  providerp_billing_address.billing_address_id=$id;";
            $stmt1 = $db->prepare($query1);
            $stmt1->execute();

            $providersBillingAddress=$stmt1->fetchAll();
            $providerOnAddress=array();

            if($providersBillingAddress!=null){
                foreach ($providersBillingAddress as $pba){
                    $provider=$em->getRepository('App\Entity\ProviderP')->find($pba['provider_id']);
                    if($provider){
                        $providerOnAddress[]=$provider;
                    }
                }
            }


            return $this->render('billing_address_p/edit.html.twig', array('document' => $document, 'google_apikey' => $google_apikey,
                'org' => $organization, 'serviceSettings' => $serviceSettings, 'facilityType' => $facilityType,'providers' => $providers,'providerOnAddress'=>$providerOnAddress,
                'totalserviceSettings'=>$totalserviceSettings,'serviceSettings_first_column_elements'=>$serviceSettings_first_column_elements,'serviceSettings_rest_columns_element'=>$serviceSettings_rest_columns_element,
                'totalfacilityType'=>$totalfacilityType,'facilityType_first_column_elements'=>$facilityType_first_column_elements,'facilityType_rest_columns_element'=>$facilityType_rest_columns_element));
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/view/{id}", name="provider_view_billing",defaults={"id": null,"org":null})
     *
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\BillingAddressP')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('provider_view_organization');
            }

            $serviceSettings = $em->getRepository('App\Entity\ServiceSettings')->findAll();
            $facilityType = $em->getRepository('App\Entity\FacilityType')->findAll();

            return $this->render('billing_new_p/view.html.twig', array('document' => $document, 'serviceSettings' => $serviceSettings, 'facilityType' => $facilityType));
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/create", name="provider_create_billing")
     *
    */
    public function create(Request $request, BillingAddressFileUploader $fileUploader){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $db = $em->getConnection();

            $save_status = $request->get('save_status');
            $form_step = $request->get('form_step');

            $user=$this->getUser();

            if ($save_status == 0) {
                $billinAddress = new BillingAddressP();
            } else {
                $billinAddress = $em->getRepository('App\Entity\BillingAddressP')->find($save_status);
            }

            if ($form_step == 1) {
                $business_hours = $request->get('business_hours_data');
                $street = $request->get('street');
                $suite_number = $request->get('suite_number');
                $city = $request->get('city');
                $state = $request->get('state');
                $zip_code = $request->get('zip_code');
                $country = $request->get('country');
                $phone = $request->get('phone');
                $org = $request->get('org');


                $billinAddress->setBusinessHours($business_hours);
                $billinAddress->setStreet($street);
                $billinAddress->setSuiteNumber($suite_number);
                $billinAddress->setCity($city);

                $billinAddress->setUsState($state);
                $billinAddress->setZipCode($zip_code);
                $billinAddress->setCountry($country);
                $billinAddress->setPhoneNumber($phone);
                $billinAddress->setUser($user);

                $em->persist($billinAddress);
                $em->flush();

            }
            if ($form_step == 2) {
                $serviceSelected = $request->get('serviceSelected');

                $sqlDelete = "DELETE FROM `billing_address_p_service_settings` WHERE `billing_address_p_service_settings`.`billing_address_id` =  $save_status";
                $stmt = $db->prepare($sqlDelete);
                $stmt->execute();

                $services = substr($serviceSelected, 0, -1);
                $lbs = explode(",", $services);

                if ($lbs != null) {
                    foreach ($lbs as $service) {
                        $serviceObj = $em->getRepository('App\Entity\ServiceSettings')->find($service);

                        if ($serviceObj != null) {
                            $billinAddress->addServiceSetting($serviceObj);
                        }
                    }

                    $em->persist($billinAddress);
                    $em->flush();
                }
            }
            if ($form_step == 3) {
                $number_of_beds = $request->get('number_of_beds');
                $location_npi = $request->get('location_npi');
                $location_npi2 = $request->get('location_npi2');
                $group_medicaid = $request->get('group_medicaid');
                $group_medicare = $request->get('group_medicare');
                $taxonomy_code = $request->get('taxonomy_code');
                $limit_age_min = $request->get('limit_age_min');
                $limit_age_max = $request->get('limit_age_max');

                $billinAddress->setBeds($number_of_beds);
                $billinAddress->setLocationNpi($location_npi);
                $billinAddress->setLocationNpi2($location_npi2);
                $billinAddress->setGroupMedicaid($group_medicaid);
                $billinAddress->setGroupMedicare($group_medicare);
                $billinAddress->setTaxonomyCode($taxonomy_code);
                if ($limit_age_min != "")
                    $billinAddress->setOfficeAgeLimit($limit_age_min);
                if ($limit_age_max != "")
                    $billinAddress->setOfficeAgeLimitMax($limit_age_max);

                $em->persist($billinAddress);
                $em->flush();
            }

            if ($form_step == 4) {
                $wheelchair_accessibility = $request->get('wheelchair_accessibility');
                $tdd = $request->get('tdd');
                $tdd_number=$request->get('tdd_number');
                $private_provider_transportation = $request->get('private_provider_transportation');
                $is_primary_address = $request->get('is_primary_address');
                $is_billing_address = $request->get('is_billing_address');
                $is_mailing_address = $request->get('is_mailing_address');
                $is_facility = $request->get('is_facility');
                $facilityTypesSelected = $request->get('facilityTypesSelected');
                $facility_number = $request->get('facility_number');
                $facility_exemption_number = $request->get('facility_exemption_number');

                $billinAddress->setWheelchair($wheelchair_accessibility);
                $billinAddress->setTdd($tdd);
                $billinAddress->setTddNumber($tdd_number);
                $billinAddress->setPrivateProviderTransportation($private_provider_transportation);
                $billinAddress->setPrimaryAddr($is_primary_address);
                $billinAddress->setBillingAddr($is_billing_address);
                $billinAddress->setMailingAddr($is_mailing_address);
                $billinAddress->setIsFacility($is_facility);


                if ($facilityTypesSelected != "") {

                    $facilities=$billinAddress->getFacilityType();

                    if($facilities){
                        foreach ($facilities as $facility){
                            $billinAddress->removeFacilityType($facility);
                        }
                    }

                    $facilities = substr($facilityTypesSelected, 0, -1);
                    $lbs = explode(",", $facilities);

                    foreach ($lbs as $facility) {
                        if ($facility != null) {
                            $facilityObj = $em->getRepository('App\Entity\FacilityType')->find($facility);
                            if ($facilityObj != null) {
                                $billinAddress->addFacilityType($facilityObj);
                            }
                        }
                    }
                }
                if ($facility_number == "") {
                    $billinAddress->setFacilityNumber(null);
                } else {
                    $billinAddress->setFacilityNumber(intval($facility_number));
                }

                if ($facility_exemption_number == "") {
                    $billinAddress->setFacilityExemptionNumber(null);
                } else {
                    $billinAddress->setFacilityExemptionNumber($facility_exemption_number);
                }


                $em->persist($billinAddress);
                $em->flush();
            }


            if ($form_step == 6) {
                $providerSelected = $request->get('providerSelected');

                $sqlDelete = "DELETE FROM `providerp_billing_address` WHERE `providerp_billing_address`.`billing_address_id` =  $save_status";
                $stmt = $db->prepare($sqlDelete);
                $stmt->execute();

                $providers = substr($providerSelected, 0, -1);
                $lbs = explode(",", $providers);

                if ($lbs != null) {
                    foreach ($lbs as $provider) {
                        $providerObj = $em->getRepository('App\Entity\ProviderP')->find($provider);
                        if ($providerObj != null) {
                            $providerObj->addBillingAddress($billinAddress);
                        }
                        $em->persist($providerObj);
                        $em->flush();
                    }
                }
            }

            return new Response(
                json_encode(array('id' => $billinAddress->getId())), 200, array('Content-Type' => 'application/json')
            );

        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/update", name="provider_update_billing")
     *
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
        $em=$this->getDoctrine()->getManager();

        $name=$request->get('name');
        $description=$request->get('description');
        $new=$request->get('new');
        $id=$request->get('id');

        $billing=$em->getRepository('App\Entity\Comorbidity')->find($id);

        if($billing==null){
            $this->addFlash(
                "danger",
                "The Address can't been updated!"
            );

            return $this->redirectToRoute('provider_billing');
        }

        if($billing!=null){
            $billing->setName($name);
            $billing->setDescription($description);

            $em->persist($billing);
            $em->flush();
        }

        $this->addFlash(
            "success",
            "The Address has been updated successfully!"
        );

        if($new==1){
            return $this->redirectToRoute('provider_new_billing');
        }

        return $this->redirectToRoute('provider_billing');
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="provider_delete_billing",methods={"POST","DELETE"})
     *
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $billing = $billing = $em->getRepository('App\Entity\BillingAddressP')->find($id);
            $removed = 0;
            $message = "";

            //delete servicesSetting
            $services=$billing->getServiceSettings();
            if($services){
                foreach ($services as $service){
                    if($service){
                        $billing->removeServiceSetting($service);
                    }
                }
            }

            $facilityTypes=$billing->getFacilityType();
            if($facilityTypes){
                foreach ($facilityTypes as $facilityType){
                    if($facilityType){
                        $billing->removeFacilityType($facilityType);
                    }
                }
            }

            //delete hours operation schema
            $hourOperationSchema=$em->getRepository('App\Entity\HoursOperationSchema')->findBy(array('billing_address'=>$id));

            if($hourOperationSchema){
                foreach ($hourOperationSchema as $hoshema){
                    $em->remove($hoshema);
                    $em->flush();
                }
            }

            if ($billing) {
                try {
                    $em->remove($billing);
                    $em->flush();
                    $removed = 1;
                    $message = "The Address has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Address can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="provider_delete_multiple_billing",methods={"POST","DELETE"})
     *
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $billing = $billing = $em->getRepository('App\Entity\Comorbidity')->find($id);

                if ($billing) {
                    try {
                        $em->remove($billing);
                        $em->flush();
                        $removed = 1;
                        $message = "The Co Morbidity has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Co Morbidity can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
