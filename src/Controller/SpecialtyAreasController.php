<?php

namespace App\Controller;

use App\Form\SpecialtyAreasType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\SpecialtyAreas;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/specialtyareas")
 */
class SpecialtyAreasController extends AbstractController{

    /**
     * @Route("/index", name="admin_specialtyareas")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $specialtyareas = $em->getRepository('App\Entity\SpecialtyAreas')->findAll(array('name' => 'ASC'));

            $delete_form_ajax = $this->createCustomForm('SPECIALTY_ID', 'DELETE', 'admin_delete_specialtyareas');

            return $this->render('specialtyareas/index.html.twig', array('specialtysareas' => $specialtyareas, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_specialtyareas")
     */
    public function add(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            $em = $this->getDoctrine()->getManager();
            $specialtyArea=new SpecialtyAreas();
            $form = $this->createForm(SpecialtyAreasType::class, $specialtyArea);


            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $em->persist($specialtyArea);
                $em->flush();

                $action_type=$form['action_type']->getData();

                if($action_type==1){
                    return  $this->redirectToRoute('admin_specialtyareas');
                }else{
                    return  $this->redirectToRoute('admin_new_specialtyareas');
                }

                $this->addFlash(
                    "success",
                    "The Specialty Area has been created successfully!"
                );
            }

            return $this->render('specialtyareas/form.html.twig', ['form' => $form->createView(), 'action' => 'New']);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_specialtyareas", defaults={"id": null})
     */
    public function edit($id, Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            $em = $this->getDoctrine()->getManager();
            $specialtyArea=$em->getRepository('App:SpecialtyAreas')->find($id);
            $form = $this->createForm(SpecialtyAreasType::class, $specialtyArea);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){

                $em->persist($specialtyArea);
                $em->flush();

                $action_type=$form['action_type']->getData();

                if($action_type==1){
                    return  $this->redirectToRoute('admin_specialtyareas');
                }else{
                    return  $this->redirectToRoute('admin_new_specialtyareas');
                }

                $this->addFlash(
                    "success",
                    "The Specialty Area has been created successfully!"
                );
            }

            return $this->render('specialtyareas/form.html.twig', ['form' => $form->createView(), 'action' => 'Edit']);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }



    /**
     * @Route("/view/{id}", name="admin_view_specialtyareas", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\SpecialtyAreas')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_specialtyareas');
            }

            return $this->render('specialtyareas/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_specialtyareas",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $specialtyareas = $specialtyareas = $em->getRepository('App\Entity\SpecialtyAreas')->find($id);
            $removed = 0;
            $message = "";

            if ($specialtyareas) {
                try {
                    $em->remove($specialtyareas);
                    $em->flush();
                    $removed = 1;
                    $message = "The Specialtys Areas has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The specialtyareas can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_specialtyareas",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $specialtyareas = $specialtyareas = $em->getRepository('App\Entity\SpecialtyAreas')->find($id);

                if ($specialtyareas) {
                    try {
                        $em->remove($specialtyareas);
                        $em->flush();
                        $removed = 1;
                        $message = "The Specialtys Areas has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Specialtys Areas can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
