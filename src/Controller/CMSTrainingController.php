<?php

namespace App\Controller;

use App\Entity\CmsTraining;
use App\Entity\Role;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/cms-training")
 */
class CMSTrainingController extends AbstractController{
    /**
     * @Route("/index", name="cms_training")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $trainings = $em->getRepository('App\Entity\CmsTraining')->findAll();
            $delete_form_ajax = $this->createCustomForm('TRAINING_ID', 'DELETE', 'admin_delete_training');

            return $this->render('cms_training/index.html.twig', [
                'trainings' => $trainings,'delete_form_ajax' => $delete_form_ajax->createView()
            ]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_cms_training")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $payers=$em->getRepository('App\Entity\Payer')->findAll();

            return $this->render('cms_training/add.html.twig',['payers'=>$payers]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_cms_training",defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\CmsTraining')->find($id);

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('admin_accreditation');
            }
            $payers=$em->getRepository('App\Entity\Payer')->findAll();

            return $this->render('cms_training/edit.html.twig', array('document' => $document,'payers'=>$payers));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_cms_training", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $document = $em->getRepository('App\Entity\CmsTraining')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('cms_training');
            }

            return $this->render('cms_training/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/create", name="admin_create_training")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $payers=$request->get('payers');
            $new = $request->get('new');

            $training = new CmsTraining();
            $training->setName($name);
            $training->setDescription($description);

            if($payers!=""){
                foreach ($payers as $lb) {
                    $payerObj = $em->getRepository('App\Entity\Payer')->find($lb);
                    if ($payerObj != null) {
                        $training->addPayer($payerObj);
                    }
                }
            }

            $em->persist($training);
            $em->flush();

            $this->addFlash(
                'success',
                'The Training has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_cms_training');
            }

            return $this->redirectToRoute('cms_training');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_training")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $payers=$request->get('payers');
            $id=$request->get('id');
            $new = $request->get('new');

            $training = $em->getRepository('App\Entity\CmsTraining')->find($id);
            $training->setName($name);
            $training->setDescription($description);

            $currentpayers=$training->getPayers();
            if($currentpayers!=null){
                foreach ($currentpayers as $cp){
                    $training->removePayer($cp);
                    $em->persist($training);
                }
                $em->flush();
            }

            if($payers!=""){
                foreach ($payers as $lb) {
                    $payerObj = $em->getRepository('App\Entity\Payer')->find($lb);
                    if ($payerObj != null) {
                        $training->addPayer($payerObj);
                    }
                }
            }

            $em->persist($training);
            $em->flush();

            $this->addFlash(
                'success',
                'The Training has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_cms_training');
            }

            return $this->redirectToRoute('cms_training');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_training",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $training  = $em->getRepository('App\Entity\CmsTraining')->find($id);
            $removed = 0;
            $message = "";

            if ($training) {
                try {
                    $em->remove($training);
                    $em->flush();
                    $removed = 1;
                    $message = "The Training has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Training can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_training",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $training = $em->getRepository('App\Entity\CmsTraining')->find($id);

                if ($training) {
                    try {
                        $em->remove($training);
                        $em->flush();
                        $removed = 1;
                        $message = "The CMS Training has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The CMS Training can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }
}
