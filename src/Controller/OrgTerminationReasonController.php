<?php

namespace App\Controller;

use App\Entity\OrgTerminationReason;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/org-termination-reason")
 */
class OrgTerminationReasonController extends AbstractController
{
    /**
     * @Route("/index", name="org_termination_reason_index")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em=$this->getDoctrine()->getManager();

            $teminations=$em->getRepository('App\Entity\OrgTerminationReason')->findAll();
            $delete_form_ajax = $this->createCustomForm('PAYER_ID', 'DELETE', 'admin_delete_payer');

            return $this->render('org_termination_reason/index.html.twig', ['terminations'=>$teminations,
                'delete_form_ajax' => $delete_form_ajax->createView()
            ]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_org_termination_reason")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('org_termination_reason/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_org_termination_reason")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');

            $obj = new OrgTerminationReason();
            $obj->setName($name);
            $obj->setDescription($description);

            $em->persist($obj);
            $em->flush();

            $this->addFlash(
                'success',
                'The Organization Termination Reason has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_org_termination_reason');
            }

            return $this->redirectToRoute('org_termination_reason_index');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_org_termination_reason", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\OrgTerminationReason')->find($id);

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('org_termination_reason_index');
            }

            return $this->render('org_termination_reason/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_org_termination_reason")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $cvo = $em->getRepository('App\Entity\OrgTerminationReason')->find($id);

            if ($cvo == null) {
                $this->addFlash(
                    "danger",
                    "The Organization Termination Reason can't been updated!"
                );

                return $this->redirectToRoute('org_termination_reason_index');
            }

            if ($cvo != null) {
                $cvo->setName($name);
                $cvo->setDescription($description);

                $em->persist($cvo);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "The Organization Termination Reason has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_org_termination_reason');
            }

            return $this->redirectToRoute('org_termination_reason_index');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_org_termination_reason", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\OrgTerminationReason')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_cvo');
            }

            return $this->render('org_termination_reason/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }
}
