<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\ContactP;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/provider/contacts")
 */
class ContactPController extends AbstractController{
    /**
     * @Route("/index", name="provider_contact")
     *
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $user = $this->getUser();

            $contacts = $em->getRepository('App\Entity\ContactP')->findBy(array('user' => $user->getId()));
            $delete_form_ajax = $this->createCustomForm('CONTACT_ID', 'DELETE', 'provider_delete_contact');


            return $this->render('contactp/index.html.twig', array('contacts' => $contacts, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/new", name="provider_new_contact")
     *
    */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $titles = $em->getRepository('App\Entity\ContactTitle')->findBy(array(),array('name' => 'ASC'));
            $bests=$em->getRepository('App\Entity\ContactPBestChoice')->findBy(array(),array('name'=>'ASC'));

            return $this->render('contactp/add.html.twig', array('titles' => $titles,'bests'=>$bests));
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="provider_edit_contact", defaults={"id": null})
     *
    */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ContactP')->find($id);
            $titles = $em->getRepository('App\Entity\ContactTitle')->findAll(array('name' => 'ASC'));

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('provider_contact');
            }

            $bests=$em->getRepository('App\Entity\ContactPBestChoice')->findBy(array(),array('name'=>'ASC'));

            return $this->render('contactp/edit.html.twig', array('document' => $document, 'titles' => $titles,'bests'=>$bests));
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/view/{id}", name="provider_view_contact", defaults={"id": null})
     *
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\ContactP')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('provider_contact');
            }

            return $this->render('contactp/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/create", name="provider_create_contact")
     *
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $title = $request->get('title');
            $phone = $request->get('phone');
            $phone_ext = $request->get('phone_ext');
            $mobile = $request->get('mobile');
            $fax = $request->get('fax');
            $email = $request->get('email');
            $main = $request->get('main');
            $address = $request->get('address');
            $city = $request->get('city');
            $state = $request->get('state');
            $zip_code = $request->get('zip_code');
            $bestSelected=$request->get('bestSelected');

            $contact = new ContactP();
            $contact->setName($name);
            $contact->setDescription($description);

            $titleObj = $em->getRepository('App\Entity\ContactTitle')->find($title);
            if ($titleObj != null) {
                $contact->setTitle($titleObj);
            }
            $contact->setPhone($phone);
            $contact->setPhoneExt($phone_ext);
            $contact->setMobile($mobile);
            $contact->setFax($fax);
            $contact->setEmail($email);
            $contact->setMain($main);
            $contact->setAddress($address);
            $contact->setCity($city);
            $contact->setState($state);
            $contact->setZipCode($zip_code);
            $contact->setUser($user);

            $em->persist($contact);
            $em->flush();

            $bests = substr($bestSelected, 0, -1);
            $lbs = explode(",", $bests);

            //remove bests
            $bestLast = $contact->getBestChoices();
            if ($bestLast != null) {
                foreach ($bestLast as $lng) {
                    $contact->removeBestChoice($lng);
                }
            }
            foreach ($lbs as $bests) {
                if ($contact != null) {
                    $bestObj= $em->getRepository('App\Entity\ContactPBestChoice')->find($bests);
                    if ($bests != null) {
                        $contact->addBestChoice($bestObj);
                    }
                }
            }

            $em->persist($contact);
            $em->flush();

            $this->addFlash(
                'success',
                'The Contact has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('provider_new_contact');
            }

            return $this->redirectToRoute('provider_contact');
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/update", name="provider_update_contact")
     *
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $title = $request->get('title');
            $phone = $request->get('phone');
            $phone_ext = $request->get('phone_ext');
            $mobile = $request->get('mobile');
            $fax = $request->get('fax');
            $email = $request->get('email');
            $main = $request->get('main');
            $address = $request->get('address');
            $city = $request->get('city');
            $state = $request->get('state');
            $zip_code = $request->get('zip_code');
            $id = $request->get('id');
            $bestSelected=$request->get('bestSelected');

            $contact = $em->getRepository('App\Entity\ContactP')->find($id);

            if ($contact == null) {
                $this->addFlash(
                    "danger",
                    "The Contact can't been updated!"
                );

                return $this->redirectToRoute('provider_contact');
            }

            if ($contact != null) {
                $contact->setName($name);
                $contact->setDescription($description);

                $titleObj = $em->getRepository('App\Entity\ContactTitle')->find($title);
                if ($titleObj != null) {
                    $contact->setTitle($titleObj);
                }
                $contact->setPhone($phone);
                $contact->setPhoneExt($phone_ext);
                $contact->setMobile($mobile);
                $contact->setFax($fax);
                $contact->setEmail($email);
                $contact->setMain($main);
                $contact->setAddress($address);
                $contact->setCity($city);
                $contact->setState($state);
                $contact->setZipCode($zip_code);

                $bests = substr($bestSelected, 0, -1);
                $lbs = explode(",", $bests);

                //remove bests
                $bestLast = $contact->getBestChoices();
                if ($bestLast != null) {
                    foreach ($bestLast as $lng) {
                        $contact->removeBestChoice($lng);
                    }
                }
                foreach ($lbs as $bests) {
                    if ($contact != null) {
                        $bestObj= $em->getRepository('App\Entity\ContactPBestChoice')->find($bests);
                        if ($bests != null) {
                            $contact->addBestChoice($bestObj);
                        }
                    }
                }

                $em->persist($contact);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "The Contact has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('provider_new_contact');
            }

            return $this->redirectToRoute('provider_contact');
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="provider_delete_contact",methods={"POST","DELETE"})
     *
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $contact = $em->getRepository('App\Entity\OrganizationContact')->find($id);
            $removed = 0;
            $message = "";

            if ($contact) {
                try {
                    $em->remove($contact);
                    $em->flush();
                    $removed = 1;
                    $message = "The Contact has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Contact can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message,'test'=>1)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="provider_delete_multiple_contact",methods={"POST","DELETE"})
     *
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $contact = $contact = $em->getRepository('App\Entity\Role')->find($id);

                if ($contact) {
                    try {
                        $em->remove($contact);
                        $em->flush();
                        $removed = 1;
                        $message = "The Role has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Role can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
