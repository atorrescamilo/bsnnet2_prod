<?php

namespace App\Controller;

use App\Entity\SpecialtyAreas;
use App\Form\SpecialtyAreasType;
use App\Form\SpecialtyType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Specialty;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/specialty")
 */
class SpecialtyController extends AbstractController{
    /**
     * @Route("/index", name="admin_specialty")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $specialtys = $em->getRepository('App\Entity\Specialty')->findAll();

            $delete_form_ajax = $this->createCustomForm('SPECIALTY_ID', 'DELETE', 'admin_delete_specialty');

            return $this->render('specialty/index.html.twig', array('specialtys' => $specialtys, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_specialty")
     */
    public function add(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $specialty=new Specialty();
            $form = $this->createForm(SpecialtyType::class, $specialty);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $em->persist($specialty);
                $em->flush();

                $action_type=$form['action_type']->getData();

                if($action_type==1){
                    return  $this->redirectToRoute('admin_specialty');
                }else{
                    return  $this->redirectToRoute('admin_new_specialty');
                }

                $this->addFlash(
                    "success",
                    "Accreditation has been created successfully!"
                );
            }

            return $this->render('specialty/form.html.twig', ['form' => $form->createView(), 'action' => 'New']);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_specialty", defaults={"id": null})
     */
    public function edit($id, Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $specialty=$em->getRepository('App:Specialty')->find($id);
            $form = $this->createForm(SpecialtyType::class, $specialty);

            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()){
                $em->persist($specialty);
                $em->flush();

                $action_type=$form['action_type']->getData();

                if($action_type==1){
                    return  $this->redirectToRoute('admin_specialty');
                }else{
                    return  $this->redirectToRoute('admin_new_specialty');
                }

                $this->addFlash(
                    "success",
                    "Accreditation has been updated successfully!"
                );
            }

            return $this->render('specialty/form.html.twig', ['form' => $form->createView(), 'action' => 'New']);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_specialty", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Specialty')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_specialty');
            }

            return $this->render('specialty/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_specialty",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $specialty = $specialty = $em->getRepository('App\Entity\Specialty')->find($id);
            $removed = 0;
            $message = "";

            if ($specialty) {
                try {
                    $em->remove($specialty);
                    $em->flush();
                    $removed = 1;
                    $message = "The Specialty has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The specialty can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_specialty",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $specialty = $specialty = $em->getRepository('App\Entity\Specialty')->find($id);

                if ($specialty) {
                    try {
                        $em->remove($specialty);
                        $em->flush();
                        $removed = 1;
                        $message = "The Specialty has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Specialty can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/isUnique", name="admin_isunique_specialty",methods={"POST"})
     */
    public function isUnique(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $code = $request->get('code');

            $specialty = $em->getRepository('App\Entity\Specialty')->findOneBy(array('code' => $code));

            $isUnique = 0;
            if ($specialty != null) {
                $isUnique = 1;
            }

            return new Response(
                json_encode(array('isUnique' => $isUnique)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
