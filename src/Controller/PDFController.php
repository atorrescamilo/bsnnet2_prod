<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Dompdf\Dompdf;
use Dompdf\Options;


/**
 * @Route("/admin/pdf")
 */
class PDFController extends AbstractController
{

    /**
     * @Route("/generate-provider-amendment", name="admin_pdf_provider_amendment")
     */
    public function index(): Response
    {

        $em=$this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App:Organization')->findAll();

        $publicDirectory = $this->getParameter('kernel.project_dir');

        foreach ($organizations as $organization){
            $id=$organization->getId();
            $pdfOptions = new Options();
            $pdfOptions->set('defaultFont', 'Arial');
            $dompdf = new Dompdf($pdfOptions);
            $html = $this->renderView('pdf/provider_amendment_contract_changes_090821.html.twig', [
                'organization' => $organization
            ]);

            $dompdf->loadHtml($html);
            $dompdf->setPaper('A4', 'portrait');

            $dompdf->render();
            $output = $dompdf->output();

            //create folder for save
            $folder = $publicDirectory.'/public/docs/organizations/organizations-amendment/'.$id;
            if (!file_exists($folder)) {
                mkdir($folder, 0777, true);
            }

            $pdfFilepath =  $publicDirectory . '/public/docs/organizations/organizations-amendment/'.$id.'/BSN_Provider_Amendment_Contract_Changes_090821.pdf';
            file_put_contents($pdfFilepath, $output);
        }



       return new Response('All Completed OK');
    }


}
