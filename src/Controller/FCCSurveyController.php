<?php

namespace App\Controller;

use App\Entity\FCCSurvey;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FCCSurveyController extends AbstractController
{
    /**
     * @Route("/survey", name="fcc_survey")
     */
    public function index(){

        return $this->render('fcc_survey/index.html.twig');
    }

    /**
     * @Route("/admin/ibh-survey-aggregate-list", name="fcc_survey_ibh_survey_aggregate_list")
     */
    public function surveyAggregateList(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $fcc_surveys = $em->getRepository('App\Entity\FCCSurvey')->findAll();

            return $this->render('fcc_survey/list.html.twig', [
                'fcc_surveys'=>$fcc_surveys
            ]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/admin/survey-edit/{id}", name="admin_edit_survey", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App:FCCSurvey')->find($id);

            return $this->render('fcc_survey/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/admin/survey-update", name="admin_update_survey", methods={"POST"})
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $npi=$request->get('npi');
            $original_npi=$request->get('original_npi');
            $id=$request->get('id');

            $survey=$em->getRepository('App:FCCSurvey')->find($id);

            if($survey){
                $survey->setNpiNumber($npi);
                $survey->setOriginalNpi($original_npi);

                $em->persist($survey);
                $em->flush();

                return $this->redirectToRoute('fcc_survey_ibh_survey_aggregate_list');
            }


        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/admin/ibh-survey-aggregate-data", name="fcc_survey_ibh_survey_aggregate_data")
     */
    public function surveyAggregateData(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em=$this->getDoctrine()->getManager();

            $fcc_surveys=$em->getRepository('App\Entity\FCCSurvey')->findAll();
            $result=[];
            $p1_a=0; $p1_b=0; $p1_c=0;
            $p2_a=0; $p2_b=0; $p2_c=0;
            $p3_a=0; $p3_b=0; $p3_c=0;
            $p4_a=0; $p4_b=0; $p4_c=0;
            $p5_a=0; $p5_b=0; $p5_c=0;

            $cc1a_a=0;$cc1a_b=0;$cc1a_c=0;
            $cc1b_a=0;$cc1b_b=0;$cc1b_c=0;
            $cc1c_a=0;$cc1c_b=0;$cc1c_c=0;
            $cc2a_a=0;$cc2a_b=0;$cc2a_c=0;
            $cc2b_a=0;$cc2b_b=0;$cc2b_c=0;
            $cc3a_a=0;$cc3a_b=0;$cc3a_c=0;
            $cc3b_a=0;$cc3b_b=0;$cc3b_c=0;
            $cc3c_a=0;$cc3c_b=0;$cc3c_c=0;
            $cc3d_a=0;$cc3d_b=0;$cc3d_c=0;
            $cc3e_a=0;$cc3e_b=0;$cc3e_c=0;
            $cc3f_a=0;$cc3f_b=0;$cc3f_c=0;
            $cc4a_a=0;$cc4a_b=0;$cc4a_c=0;
            $cc4b_a=0;$cc4b_b=0;$cc4b_c=0;
            $cc4c_a=0;$cc4c_b=0;$cc4c_c=0;
            $cc4d_a=0;$cc4d_b=0;$cc4d_c=0;
            $cc4e_a=0;$cc4e_b=0;$cc4e_c=0;
            $cc4f_a=0;$cc4f_b=0;$cc4f_c=0;
            $cc5a_a=0;$cc5a_b=0;$cc5a_c=0;
            $cc5b_a=0;$cc5b_b=0;$cc5b_c=0;
            $cc5c_a=0;$cc5c_b=0;$cc5c_c=0;
            $cc6a_a=0;$cc6a_b=0;$cc6a_c=0;
            $cc6b_a=0;$cc6b_b=0;$cc6b_c=0;
            $cc6c_a=0;$cc6c_b=0;$cc6c_c=0;
            $cc7a_a=0;$cc7a_b=0;$cc7a_c=0;
            $cc7b_a=0;$cc7b_b=0;$cc7b_c=0;
            $cc7c_a=0;$cc7c_b=0;$cc7c_c=0;

            $total=count($fcc_surveys);

            if($fcc_surveys!=null){
                foreach ($fcc_surveys as $fcc_survey){
                    $p1=$fcc_survey->getP1();
                    $p2=$fcc_survey->getP2();
                    $p3=$fcc_survey->getP3();
                    $p4=$fcc_survey->getP4();
                    $p5=$fcc_survey->getP5();

                    $cc1a=$fcc_survey->getCc1a();
                    $cc1b=$fcc_survey->getCc1b();
                    $cc1c=$fcc_survey->getCc1c();
                    $cc2a=$fcc_survey->getCc2a();
                    $cc2b=$fcc_survey->getCc2b();
                    $cc3a=$fcc_survey->getCc3a();
                    $cc3b=$fcc_survey->getCc3b();
                    $cc3c=$fcc_survey->getCc3c();
                    $cc3d=$fcc_survey->getCc3d();
                    $cc3e=$fcc_survey->getCc3e();
                    $cc3f=$fcc_survey->getCc3f();
                    $cc4a=$fcc_survey->getCc4a();
                    $cc4b=$fcc_survey->getCc4b();
                    $cc4c=$fcc_survey->getCc4c();
                    $cc4d=$fcc_survey->getCc4d();
                    $cc4e=$fcc_survey->getCc4e();
                    $cc4f=$fcc_survey->getCc4f();
                    $cc5a=$fcc_survey->getCc5a();
                    $cc5b=$fcc_survey->getCc5b();
                    $cc5c=$fcc_survey->getCc5c();
                    $cc6a=$fcc_survey->getCc6a();
                    $cc6b=$fcc_survey->getCc6b();
                    $cc6c=$fcc_survey->getCc6c();
                    $cc7a=$fcc_survey->getCc7a();
                    $cc7b=$fcc_survey->getCc7b();
                    $cc7c=$fcc_survey->getCc7c();

                    if($p1==1) $p1_a++; if($p1==2) $p1_b++; if($p1==3) $p1_c++;
                    if($p2==1) $p2_a++; if($p2==2) $p2_b++; if($p2==3) $p2_c++;
                    if($p3==1) $p3_a++; if($p3==2) $p3_b++; if($p3==3) $p3_c++;
                    if($p4==1) $p4_a++; if($p4==2) $p4_b++; if($p4==3) $p4_c++;
                    if($p5==1) $p5_a++; if($p5==2) $p5_b++; if($p5==3) $p5_c++;

                    if($cc1a==1) $cc1a_a++; if($cc1a==2) $cc1a_b++; if($cc1a==3) $cc1a_c++;
                    if($cc1b==1) $cc1b_a++; if($cc1b==2) $cc1b_b++; if($cc1b==3) $cc1b_c++;
                    if($cc1c==1) $cc1c_a++; if($cc1c==2) $cc1c_b++; if($cc1c==3) $cc1c_c++;

                    if($cc2a==1) $cc2a_a++; if($cc2a==2) $cc2a_b++; if($cc2a==3) $cc2a_c++;
                    if($cc2b==1) $cc2b_a++; if($cc2b==2) $cc2b_b++; if($cc2b==3) $cc2b_c++;

                    if($cc3a==1) $cc3a_a++; if($cc3a==2) $cc3a_b++; if($cc3a==3) $cc3a_c++;
                    if($cc3b==1) $cc3b_a++; if($cc3b==2) $cc3b_b++; if($cc3b==3) $cc3b_c++;
                    if($cc3c==1) $cc3c_a++; if($cc3c==2) $cc3c_b++; if($cc3c==3) $cc3c_c++;
                    if($cc3d==1) $cc3d_a++; if($cc3d==2) $cc3d_b++; if($cc3d==3) $cc3d_c++;
                    if($cc3e==1) $cc3e_a++; if($cc3e==2) $cc3e_b++; if($cc3e==3) $cc3e_c++;
                    if($cc3f==1) $cc3f_a++; if($cc3f==2) $cc3f_b++; if($cc3f==3) $cc3f_c++;

                    if($cc4a==1) $cc4a_a++; if($cc4a==2) $cc4a_b++; if($cc4a==3) $cc4a_c++;
                    if($cc4b==1) $cc4b_a++; if($cc4b==2) $cc4b_b++; if($cc4b==3) $cc4b_c++;
                    if($cc4c==1) $cc4c_a++; if($cc4c==2) $cc4c_b++; if($cc4c==3) $cc4c_c++;
                    if($cc4d==1) $cc4d_a++; if($cc4d==2) $cc4d_b++; if($cc4d==3) $cc4d_c++;
                    if($cc4e==1) $cc4e_a++; if($cc4e==2) $cc4e_b++; if($cc4e==3) $cc4e_c++;
                    if($cc4f==1) $cc4f_a++; if($cc4f==2) $cc4f_b++; if($cc4f==3) $cc4f_c++;

                    if($cc5a==1) $cc5a_a++; if($cc5a==2) $cc5a_b++; if($cc5a==3) $cc5a_c++;
                    if($cc5b==1) $cc5b_a++; if($cc5b==2) $cc5b_b++; if($cc5b==3) $cc5b_c++;
                    if($cc5c==1) $cc5c_a++; if($cc5c==2) $cc5c_b++; if($cc5c==3) $cc5c_c++;

                    if($cc6a==1) $cc6a_a++; if($cc6a==2) $cc6a_b++; if($cc6a==3) $cc6a_c++;
                    if($cc6b==1) $cc6b_a++; if($cc6b==2) $cc6b_b++; if($cc6b==3) $cc6b_c++;
                    if($cc6c==1) $cc6c_a++; if($cc6c==2) $cc6c_b++; if($cc6c==3) $cc6c_c++;

                    if($cc7a==1) $cc7a_a++; if($cc7a==2) $cc7a_b++; if($cc7a==3) $cc7a_c++;
                    if($cc7b==1) $cc7b_a++; if($cc7b==2) $cc7b_b++; if($cc7b==3) $cc7b_c++;
                    if($cc7c==1) $cc7c_a++; if($cc7c==2) $cc7c_b++; if($cc7c==3) $cc7c_c++;
                }
            }

            $result['p1'][0]=$p1_a; $result['p1'][1]=$p1_b; $result['p1'][2]=$p1_c;
            $result['p2'][0]=$p2_a; $result['p2'][1]=$p2_b; $result['p2'][2]=$p2_c;
            $result['p3'][0]=$p3_a; $result['p3'][1]=$p3_b; $result['p3'][2]=$p3_c;
            $result['p4'][0]=$p4_a; $result['p4'][1]=$p4_b; $result['p4'][2]=$p4_c;
            $result['p5'][0]=$p5_a; $result['p5'][1]=$p5_b; $result['p5'][2]=$p5_c;

            $result['cc1a'][0]=$cc1a_a; $result['cc1a'][1]=$cc1a_b; $result['cc1a'][2]=$cc1a_c;
            $result['cc1b'][0]=$cc1b_a; $result['cc1b'][1]=$cc1b_b; $result['cc1b'][2]=$cc1b_c;
            $result['cc1c'][0]=$cc1c_a; $result['cc1c'][1]=$cc1c_b; $result['cc1c'][2]=$cc1c_c;

            $result['cc2a'][0]=$cc2a_a; $result['cc2a'][1]=$cc2a_b; $result['cc2a'][2]=$cc2a_c;
            $result['cc2b'][0]=$cc2b_a; $result['cc2b'][1]=$cc2b_b; $result['cc2b'][2]=$cc2b_c;

            $result['cc3a'][0]=$cc3a_a; $result['cc3a'][1]=$cc3a_b; $result['cc3a'][2]=$cc3a_c;
            $result['cc3b'][0]=$cc3b_a; $result['cc3b'][1]=$cc3b_b; $result['cc3b'][2]=$cc3b_c;
            $result['cc3c'][0]=$cc3c_a; $result['cc3c'][1]=$cc3c_b; $result['cc3c'][2]=$cc3c_c;
            $result['cc3d'][0]=$cc3d_a; $result['cc3d'][1]=$cc3d_b; $result['cc3d'][2]=$cc3d_c;
            $result['cc3e'][0]=$cc3e_a; $result['cc3e'][1]=$cc3e_b; $result['cc3e'][2]=$cc3e_c;
            $result['cc3f'][0]=$cc3f_a; $result['cc3f'][1]=$cc3f_b; $result['cc3f'][2]=$cc3f_c;

            $result['cc4a'][0]=$cc4a_a; $result['cc4a'][1]=$cc4a_b; $result['cc4a'][2]=$cc4a_c;
            $result['cc4b'][0]=$cc4b_a; $result['cc4b'][1]=$cc4b_b; $result['cc4b'][2]=$cc4b_c;
            $result['cc4c'][0]=$cc4c_a; $result['cc4c'][1]=$cc4c_b; $result['cc4c'][2]=$cc4c_c;
            $result['cc4d'][0]=$cc4d_a; $result['cc4d'][1]=$cc4d_b; $result['cc4d'][2]=$cc4d_c;
            $result['cc4e'][0]=$cc4e_a; $result['cc4e'][1]=$cc4e_b; $result['cc4e'][2]=$cc4e_c;
            $result['cc4f'][0]=$cc4f_a; $result['cc4f'][1]=$cc4f_b; $result['cc4f'][2]=$cc4f_c;

            $result['cc5a'][0]=$cc5a_a; $result['cc5a'][1]=$cc5a_b; $result['cc5a'][2]=$cc5a_c;
            $result['cc5b'][0]=$cc5b_a; $result['cc5b'][1]=$cc5b_b; $result['cc5b'][2]=$cc5b_c;
            $result['cc5c'][0]=$cc5c_a; $result['cc5c'][1]=$cc5c_b; $result['cc5c'][2]=$cc5c_c;

            $result['cc6a'][0]=$cc6a_a; $result['cc6a'][1]=$cc6a_b; $result['cc6a'][2]=$cc6a_c;
            $result['cc6b'][0]=$cc6b_a; $result['cc6b'][1]=$cc6b_b; $result['cc6b'][2]=$cc6b_c;
            $result['cc6c'][0]=$cc6c_a; $result['cc6c'][1]=$cc6c_b; $result['cc6c'][2]=$cc6c_c;

            $result['cc7a'][0]=$cc7a_a; $result['cc7a'][1]=$cc7a_b; $result['cc7a'][2]=$cc7a_c;
            $result['cc7b'][0]=$cc7b_a; $result['cc7b'][1]=$cc7b_b; $result['cc7b'][2]=$cc7b_c;
            $result['cc7c'][0]=$cc7c_a; $result['cc7c'][1]=$cc7c_b; $result['cc7c'][2]=$cc7c_c;

            $p1_a_p=0;$p1_b_p=0;$p1_c_p=0;
            $p2_a_p=0;$p2_b_p=0;$p2_c_p=0;$p3_a_p=0;
            $p3_b_p=0;$p3_c_p=0;$p4_a_p=0;
            $p4_b_p=0;$p4_c_p=0;$p5_a_p=0;
            $p5_b_p=0;$p5_c_p=0;

            $cc1a_a_p=0;$cc1a_b_p=0;$cc1a_c_p=0;$cc1b_a_p=0;$cc1b_b_p=0;
            $cc1b_c_p=0;$cc1c_a_p=0;$cc1c_b_p=0;$cc1c_c_p=0;$cc2a_a_p=0;
            $cc2a_b_p=0;$cc2a_c_p=0;$cc2b_a_p=0;$cc2b_b_p=0;$cc2b_c_p=0;
            $cc3a_a_p=0;$cc3a_b_p=0;$cc3a_c_p=0;$cc3b_a_p=0;$cc3b_b_p=0;
            $cc3b_c_p=0;$cc3c_a_p=0;$cc3c_b_p=0;$cc3c_c_p=0;$cc3d_a_p=0;
            $cc3d_b_p=0;$cc3d_c_p=0;$cc3e_a_p=0;$cc3e_b_p=0;$cc3e_c_p=0;
            $cc3f_a_p=0;$cc3f_b_p=0;$cc3f_c_p=0;$cc4a_a_p=0;$cc4a_b_p=0;
            $cc4a_c_p=0;$cc4b_a_p=0;$cc4b_b_p=0;$cc4b_c_p=0;$cc4c_a_p=0;
            $cc4c_b_p=0;$cc4c_c_p=0;$cc4d_a_p=0;$cc4d_b_p=0;$cc4d_c_p=0;
            $cc4e_a_p=0;$cc4e_b_p=0;$cc4e_c_p=0;$cc4f_a_p=0;$cc4f_b_p=0;
            $cc4f_c_p=0;$cc5a_a_p=0;$cc5a_b_p=0;$cc5a_c_p=0;$cc5b_a_p=0;
            $cc5b_b_p=0;$cc5b_c_p=0;$cc5c_a_p=0;$cc5c_b_p=0;$cc5c_c_p=0;
            $cc6a_a_p=0;$cc6a_b_p=0;$cc6a_c_p=0;$cc6b_a_p=0;$cc6b_b_p=0;
            $cc6b_c_p=0;$cc6c_a_p=0;$cc6c_b_p=0;$cc6c_c_p=0;$cc7a_a_p=0;
            $cc7a_b_p=0;$cc7a_c_p=0;$cc7b_a_p=0;$cc7b_b_p=0;$cc7b_c_p=0;
            $cc7c_a_p=0;$cc7c_b_p=0;$cc7c_c_p=0;

            if($total>0){
                $p1_a_p=round(($p1_a*100)/$total,2);
                $p1_b_p=round(($p1_b*100)/$total,2);
                $p1_c_p=round(($p1_c*100)/$total,2);
                $p2_a_p=round(($p2_a*100)/$total,2);
                $p2_b_p=round(($p2_b*100)/$total,2);
                $p2_c_p=round(($p2_c*100)/$total,2);
                $p3_a_p=round(($p3_a*100)/$total,2);
                $p3_b_p=round(($p3_b*100)/$total,2);
                $p3_c_p=round(($p3_c*100)/$total,2);
                $p4_a_p=round(($p4_a*100)/$total,2);
                $p4_b_p=round(($p4_b*100)/$total,2);
                $p4_c_p=round(($p4_c*100)/$total,2);
                $p5_a_p=round(($p5_a*100)/$total,2);
                $p5_b_p=round(($p5_b*100)/$total,2);
                $p5_c_p=round(($p5_c*100)/$total,2);

                $cc1a_a_p=round(($cc1a_a*100)/$total,2);
                $cc1a_b_p=round(($cc1a_b*100)/$total,2);
                $cc1a_c_p=round(($cc1a_c*100)/$total,2);
                $cc1b_a_p=round(($cc1b_a*100)/$total,2);
                $cc1b_b_p=round(($cc1b_b*100)/$total,2);
                $cc1b_c_p=round(($cc1b_c*100)/$total,2);
                $cc1c_a_p=round(($cc1c_a*100)/$total,2);
                $cc1c_b_p=round(($cc1c_b*100)/$total,2);
                $cc1c_c_p=round(($cc1c_c*100)/$total,2);
                $cc2a_a_p=round(($cc2a_a*100)/$total,2);
                $cc2a_b_p=round(($cc2a_b*100)/$total,2);
                $cc2a_c_p=round(($cc2a_c*100)/$total,2);
                $cc2b_a_p=round(($cc2b_a*100)/$total,2);
                $cc2b_b_p=round(($cc2b_b*100)/$total,2);
                $cc2b_c_p=round(($cc2b_c*100)/$total,2);
                $cc3a_a_p=round(($cc3a_a*100)/$total,2);
                $cc3a_b_p=round(($cc3a_b*100)/$total,2);
                $cc3a_c_p=round(($cc3a_c*100)/$total,2);
                $cc3b_a_p=round(($cc3b_a*100)/$total,2);
                $cc3b_b_p=round(($cc3b_b*100)/$total,2);
                $cc3b_c_p=round(($cc3b_c*100)/$total,2);
                $cc3c_a_p=round(($cc3c_a*100)/$total,2);
                $cc3c_b_p=round(($cc3c_b*100)/$total,2);
                $cc3c_c_p=round(($cc3c_c*100)/$total,2);
                $cc3d_a_p=round(($cc3d_a*100)/$total,2);
                $cc3d_b_p=round(($cc3d_b*100)/$total,2);
                $cc3d_c_p=round(($cc3d_c*100)/$total,2);
                $cc3e_a_p=round(($cc3e_a*100)/$total,2);
                $cc3e_b_p=round(($cc3e_b*100)/$total,2);
                $cc3e_c_p=round(($cc3e_c*100)/$total,2);
                $cc3f_a_p=round(($cc3f_a*100)/$total,2);
                $cc3f_b_p=round(($cc3f_b*100)/$total,2);
                $cc3f_c_p=round(($cc3f_c*100)/$total,2);
                $cc4a_a_p=round(($cc4a_a*100)/$total,2);
                $cc4a_b_p=round(($cc4a_b*100)/$total,2);
                $cc4a_c_p=round(($cc4a_c*100)/$total,2);
                $cc4b_a_p=round(($cc4b_a*100)/$total,2);
                $cc4b_b_p=round(($cc4b_b*100)/$total,2);
                $cc4b_c_p=round(($cc4b_c*100)/$total,2);
                $cc4c_a_p=round(($cc4c_a*100)/$total,2);
                $cc4c_b_p=round(($cc4c_b*100)/$total,2);
                $cc4c_c_p=round(($cc4c_c*100)/$total,2);
                $cc4d_a_p=round(($cc4d_a*100)/$total,2);
                $cc4d_b_p=round(($cc4d_b*100)/$total,2);
                $cc4d_c_p=round(($cc4d_c*100)/$total,2);
                $cc4e_a_p=round(($cc4e_a*100)/$total,2);
                $cc4e_b_p=round(($cc4e_b*100)/$total,2);
                $cc4e_c_p=round(($cc4e_c*100)/$total,2);
                $cc4f_a_p=round(($cc4f_a*100)/$total,2);
                $cc4f_b_p=round(($cc4f_b*100)/$total,2);
                $cc4f_c_p=round(($cc4f_c*100)/$total,2);
                $cc5a_a_p=round(($cc5a_a*100)/$total,2);
                $cc5a_b_p=round(($cc5a_b*100)/$total,2);
                $cc5a_c_p=round(($cc5a_c*100)/$total,2);
                $cc5b_a_p=round(($cc5b_a*100)/$total,2);
                $cc5b_b_p=round(($cc5b_b*100)/$total,2);
                $cc5b_c_p=round(($cc5b_c*100)/$total,2);
                $cc5c_a_p=round(($cc5c_a*100)/$total,2);
                $cc5c_b_p=round(($cc5c_b*100)/$total,2);
                $cc5c_c_p=round(($cc5c_c*100)/$total,2);
                $cc6a_a_p=round(($cc6a_a*100)/$total,2);
                $cc6a_b_p=round(($cc6a_b*100)/$total,2);
                $cc6a_c_p=round(($cc6a_c*100)/$total,2);
                $cc6b_a_p=round(($cc6b_a*100)/$total,2);
                $cc6b_b_p=round(($cc6b_b*100)/$total,2);
                $cc6b_c_p=round(($cc6b_c*100)/$total,2);
                $cc6c_a_p=round(($cc6c_a*100)/$total,2);
                $cc6c_b_p=round(($cc6c_b*100)/$total,2);
                $cc6c_c_p=round(($cc6c_c*100)/$total,2);
                $cc7a_a_p=round(($cc7a_a*100)/$total,2);
                $cc7a_b_p=round(($cc7a_b*100)/$total,2);
                $cc7a_c_p=round(($cc7a_c*100)/$total,2);
                $cc7b_a_p=round(($cc7b_a*100)/$total,2);
                $cc7b_b_p=round(($cc7b_b*100)/$total,2);
                $cc7b_c_p=round(($cc7b_c*100)/$total,2);
                $cc7c_a_p=round(($cc7c_a*100)/$total,2);
                $cc7c_b_p=round(($cc7c_b*100)/$total,2);
                $cc7c_c_p=round(($cc7c_c*100)/$total,2);
            }


            $result['p1_p'][0]=$p1_a_p;$result['p1_p'][1]=$p1_b_p;$result['p1_p'][2]=$p1_c_p;
            $result['p2_p'][0]=$p2_a_p;$result['p2_p'][1]=$p2_b_p;$result['p2_p'][2]=$p2_c_p;
            $result['p3_p'][0]=$p3_a_p;$result['p3_p'][1]=$p3_b_p;$result['p3_p'][2]=$p3_c_p;
            $result['p4_p'][0]=$p4_a_p;$result['p4_p'][1]=$p4_b_p;$result['p4_p'][2]=$p4_c_p;
            $result['p5_p'][0]=$p5_a_p;$result['p5_p'][1]=$p5_b_p;$result['p5_p'][2]=$p5_c_p;

            $result['cc1a_p'][0]=$cc1a_a_p; $result['cc1a_p'][1]=$cc1a_b_p; $result['cc1a_p'][2]=$cc1a_c_p;
            $result['cc1b_p'][0]=$cc1b_a_p; $result['cc1b_p'][1]=$cc1b_b_p; $result['cc1b_p'][2]=$cc1b_c_p;
            $result['cc1c_p'][0]=$cc1c_a_p; $result['cc1c_p'][1]=$cc1c_b_p; $result['cc1c_p'][2]=$cc1c_c_p;

            $result['cc2a_p'][0]=$cc2a_a_p; $result['cc2a_p'][1]=$cc2a_b_p; $result['cc2a_p'][2]=$cc2a_c_p;
            $result['cc2b_p'][0]=$cc2b_a_p; $result['cc2b_p'][1]=$cc2b_b_p; $result['cc2b_p'][2]=$cc2b_c_p;

            $result['cc3a_p'][0]=$cc3a_a_p; $result['cc3a_p'][1]=$cc3a_b_p; $result['cc3a_p'][2]=$cc3a_c_p;
            $result['cc3b_p'][0]=$cc3b_a_p; $result['cc3b_p'][1]=$cc3b_b_p; $result['cc3b_p'][2]=$cc3b_c_p;
            $result['cc3c_p'][0]=$cc3c_a_p; $result['cc3c_p'][1]=$cc3c_b_p; $result['cc3c_p'][2]=$cc3c_c_p;
            $result['cc3d_p'][0]=$cc3d_a_p; $result['cc3d_p'][1]=$cc3d_b_p; $result['cc3d_p'][2]=$cc3d_c_p;
            $result['cc3e_p'][0]=$cc3e_a_p; $result['cc3e_p'][1]=$cc3e_b_p; $result['cc3e_p'][2]=$cc3e_c_p;
            $result['cc3f_p'][0]=$cc3f_a_p; $result['cc3f_p'][1]=$cc3f_b_p; $result['cc3f_p'][2]=$cc3f_c_p;

            $result['cc4a_p'][0]=$cc4a_a_p; $result['cc4a_p'][1]=$cc4a_b_p; $result['cc4a_p'][2]=$cc4a_c_p;
            $result['cc4b_p'][0]=$cc4b_a_p; $result['cc4b_p'][1]=$cc4b_b_p; $result['cc4b_p'][2]=$cc4b_c_p;
            $result['cc4c_p'][0]=$cc4c_a_p; $result['cc4c_p'][1]=$cc4c_b_p; $result['cc4c_p'][2]=$cc4c_c_p;
            $result['cc4d_p'][0]=$cc4d_a_p; $result['cc4d_p'][1]=$cc4d_b_p; $result['cc4d_p'][2]=$cc4d_c_p;
            $result['cc4e_p'][0]=$cc4e_a_p; $result['cc4e_p'][1]=$cc4e_b_p; $result['cc4e_p'][2]=$cc4e_c_p;
            $result['cc4f_p'][0]=$cc4f_a_p; $result['cc4f_p'][1]=$cc4f_b_p; $result['cc4f_p'][2]=$cc4f_c_p;

            $result['cc5a_p'][0]=$cc5a_a_p; $result['cc5a_p'][1]=$cc5a_b_p; $result['cc5a_p'][2]=$cc5a_c_p;
            $result['cc5b_p'][0]=$cc5b_a_p; $result['cc5b_p'][1]=$cc5b_b_p; $result['cc5b_p'][2]=$cc5b_c_p;
            $result['cc5c_p'][0]=$cc5c_a_p; $result['cc5c_p'][1]=$cc5c_b_p; $result['cc5c_p'][2]=$cc5c_c_p;

            $result['cc6a_p'][0]=$cc6a_a_p; $result['cc6a_p'][1]=$cc6a_b_p; $result['cc6a_p'][2]=$cc6a_c_p;
            $result['cc6b_p'][0]=$cc6b_a_p; $result['cc6b_p'][1]=$cc6b_b_p; $result['cc6b_p'][2]=$cc6b_c_p;
            $result['cc6c_p'][0]=$cc6c_a_p; $result['cc6c_p'][1]=$cc6c_b_p; $result['cc6c_p'][2]=$cc6c_c_p;

            $result['cc7a_p'][0]=$cc7a_a_p; $result['cc7a_p'][1]=$cc7a_b_p; $result['cc7a_p'][2]=$cc7a_c_p;
            $result['cc7b_p'][0]=$cc7b_a_p; $result['cc7b_p'][1]=$cc7b_b_p; $result['cc7b_p'][2]=$cc7b_c_p;
            $result['cc7c_p'][0]=$cc7c_a_p; $result['cc7c_p'][1]=$cc7c_b_p; $result['cc7c_p'][2]=$cc7c_c_p;


        return $this->render('fcc_survey/survey_aggregate_data.html.twig', [
            'total' => $total, 'result'=>$result
        ]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/checknpi", name="survey_check_npi",methods={"POST"})
     */
    public function checkNPI(Request $request) {
        $npi = $request->get('npi');
        $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=1&skip=&pretty=on&version=2.1";
        $json=file_get_contents($url_result);

        return new Response(
            json_encode(array('json_result' =>$json)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/checknpi-exist", name="survey_check_npi_exist",methods={"POST"})
     */
    public function checkNPIExist(Request $request) {
        $npi = $request->get('npi');
        $em=$this->getDoctrine()->getManager();

        $survey=$em->getRepository('App\Entity\FCCSurvey')->findOneBy(array('npi_number'=>$npi));

        $exist=0;
        if($survey!=null){
            $exist=1;
        }

        return new Response(
            json_encode(array('exist'=>$exist)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/survey-final", name="survey_final")
     */
    public function final() {

        return $this->render('fcc_survey/final.html.twig');
    }

    /**
     * @Route("/save-fcc-survey", name="save_fcc_survey",methods={"POST"})
     */
    public function save(Request  $request){
        $em=$this->getDoctrine()->getManager();

        $npi_number=$request->get('npi_number');
        $name=$request->get('name');
        $email=$request->get('email');
        $provider_type=$request->get('provider_type');

        $p1=$request->get('p1');
        $p2=$request->get('p2');
        $p3=$request->get('p3');
        $p4=$request->get('p4');
        $p5=$request->get('p5');

        $cc1_a=$request->get('cc1_a');
        $cc1_b=$request->get('cc1_b');
        $cc1_c=$request->get('cc1_c');
        $cc2_a=$request->get('cc2_a');
        $cc2_b=$request->get('cc2_a');
        $cc3_a=$request->get('cc3_a');
        $cc3_b=$request->get('cc3_b');
        $cc3_c=$request->get('cc3_c');
        $cc3_d=$request->get('cc3_d');
        $cc3_e=$request->get('cc3_e');
        $cc3_f=$request->get('cc3_f');
        $cc4_a=$request->get('cc4_a');
        $cc4_b=$request->get('cc4_b');
        $cc4_c=$request->get('cc4_c');
        $cc4_d=$request->get('cc4_d');
        $cc4_e=$request->get('cc4_e');
        $cc4_f=$request->get('cc4_f');
        $cc5_a=$request->get('cc5_a');
        $cc5_b=$request->get('cc5_b');
        $cc5_c=$request->get('cc5_c');
        $cc6_a=$request->get('cc6_a');
        $cc6_b=$request->get('cc6_b');
        $cc6_c=$request->get('cc6_c');
        $cc7_a=$request->get('cc7_a');
        $cc7_b=$request->get('cc7_b');
        $cc7_c=$request->get('cc7_c');

        $fcc_survey=new FCCSurvey();
        $fcc_survey->setNpiNumber($npi_number);
        $fcc_survey->setName($name);
        $fcc_survey->setEmail($email);
        $fcc_survey->setProviderType($provider_type);
        $fcc_survey->setP1($p1);
        $fcc_survey->setP2($p2);
        $fcc_survey->setP3($p3);
        $fcc_survey->setP4($p4);
        $fcc_survey->setP5($p5);
        $fcc_survey->setCc1a($cc1_a);
        $fcc_survey->setCc1b($cc1_b);
        $fcc_survey->setCc1c($cc1_c);
        $fcc_survey->setCc2a($cc2_a);
        $fcc_survey->setCc2b($cc2_b);
        $fcc_survey->setCc3a($cc3_a);
        $fcc_survey->setCc3b($cc3_b);
        $fcc_survey->setCc3c($cc3_c);
        $fcc_survey->setCc3d($cc3_d);
        $fcc_survey->setCc3e($cc3_e);
        $fcc_survey->setCc3f($cc3_f);
        $fcc_survey->setCc4a($cc4_a);
        $fcc_survey->setCc4b($cc4_b);
        $fcc_survey->setCc4c($cc4_c);
        $fcc_survey->setCc4d($cc4_d);
        $fcc_survey->setCc4e($cc4_e);
        $fcc_survey->setCc4f($cc4_f);
        $fcc_survey->setCc5a($cc5_a);
        $fcc_survey->setCc5b($cc5_b);
        $fcc_survey->setCc5c($cc5_c);
        $fcc_survey->setCc6a($cc6_a);
        $fcc_survey->setCc6b($cc6_b);
        $fcc_survey->setCc6c($cc6_c);
        $fcc_survey->setCc7a($cc7_a);
        $fcc_survey->setCc7b($cc7_b);
        $fcc_survey->setCc7c($cc7_c);

        $em->persist($fcc_survey);
        $em->flush();

        return $this->redirectToRoute('survey_final');
    }

    /**
     * @Route("/admin/ibh-survey-aggregate-data-export", name="fcc_survey_ibh_survey_aggregate_data_export")
     */
    public function surveyAggregateDataExport(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();

        $fcc_surveys=$em->getRepository('App\Entity\FCCSurvey')->findAll();
        $result=[];
        $p1_a=0; $p1_b=0; $p1_c=0;
        $p2_a=0; $p2_b=0; $p2_c=0;
        $p3_a=0; $p3_b=0; $p3_c=0;
        $p4_a=0; $p4_b=0; $p4_c=0;
        $p5_a=0; $p5_b=0; $p5_c=0;

        $cc1a_a=0;$cc1a_b=0;$cc1a_c=0;
        $cc1b_a=0;$cc1b_b=0;$cc1b_c=0;
        $cc1c_a=0;$cc1c_b=0;$cc1c_c=0;
        $cc2a_a=0;$cc2a_b=0;$cc2a_c=0;
        $cc2b_a=0;$cc2b_b=0;$cc2b_c=0;
        $cc3a_a=0;$cc3a_b=0;$cc3a_c=0;
        $cc3b_a=0;$cc3b_b=0;$cc3b_c=0;
        $cc3c_a=0;$cc3c_b=0;$cc3c_c=0;
        $cc3d_a=0;$cc3d_b=0;$cc3d_c=0;
        $cc3e_a=0;$cc3e_b=0;$cc3e_c=0;
        $cc3f_a=0;$cc3f_b=0;$cc3f_c=0;
        $cc4a_a=0;$cc4a_b=0;$cc4a_c=0;
        $cc4b_a=0;$cc4b_b=0;$cc4b_c=0;
        $cc4c_a=0;$cc4c_b=0;$cc4c_c=0;
        $cc4d_a=0;$cc4d_b=0;$cc4d_c=0;
        $cc4e_a=0;$cc4e_b=0;$cc4e_c=0;
        $cc4f_a=0;$cc4f_b=0;$cc4f_c=0;
        $cc5a_a=0;$cc5a_b=0;$cc5a_c=0;
        $cc5b_a=0;$cc5b_b=0;$cc5b_c=0;
        $cc5c_a=0;$cc5c_b=0;$cc5c_c=0;
        $cc6a_a=0;$cc6a_b=0;$cc6a_c=0;
        $cc6b_a=0;$cc6b_b=0;$cc6b_c=0;
        $cc6c_a=0;$cc6c_b=0;$cc6c_c=0;
        $cc7a_a=0;$cc7a_b=0;$cc7a_c=0;
        $cc7b_a=0;$cc7b_b=0;$cc7b_c=0;
        $cc7c_a=0;$cc7c_b=0;$cc7c_c=0;

        $total=count($fcc_surveys);

        if($fcc_surveys!=null){
            foreach ($fcc_surveys as $fcc_survey){
                $p1=$fcc_survey->getP1();
                $p2=$fcc_survey->getP2();
                $p3=$fcc_survey->getP3();
                $p4=$fcc_survey->getP4();
                $p5=$fcc_survey->getP5();

                $cc1a=$fcc_survey->getCc1a();
                $cc1b=$fcc_survey->getCc1b();
                $cc1c=$fcc_survey->getCc1c();
                $cc2a=$fcc_survey->getCc2a();
                $cc2b=$fcc_survey->getCc2b();
                $cc3a=$fcc_survey->getCc3a();
                $cc3b=$fcc_survey->getCc3b();
                $cc3c=$fcc_survey->getCc3c();
                $cc3d=$fcc_survey->getCc3d();
                $cc3e=$fcc_survey->getCc3e();
                $cc3f=$fcc_survey->getCc3f();
                $cc4a=$fcc_survey->getCc4a();
                $cc4b=$fcc_survey->getCc4b();
                $cc4c=$fcc_survey->getCc4c();
                $cc4d=$fcc_survey->getCc4d();
                $cc4e=$fcc_survey->getCc4e();
                $cc4f=$fcc_survey->getCc4f();
                $cc5a=$fcc_survey->getCc5a();
                $cc5b=$fcc_survey->getCc5b();
                $cc5c=$fcc_survey->getCc5c();
                $cc6a=$fcc_survey->getCc6a();
                $cc6b=$fcc_survey->getCc6b();
                $cc6c=$fcc_survey->getCc6c();
                $cc7a=$fcc_survey->getCc7a();
                $cc7b=$fcc_survey->getCc7b();
                $cc7c=$fcc_survey->getCc7c();

                if($p1==1) $p1_a++; if($p1==2) $p1_b++; if($p1==3) $p1_c++;
                if($p2==1) $p2_a++; if($p2==2) $p2_b++; if($p2==3) $p2_c++;
                if($p3==1) $p3_a++; if($p3==2) $p3_b++; if($p3==3) $p3_c++;
                if($p4==1) $p4_a++; if($p4==2) $p4_b++; if($p4==3) $p4_c++;
                if($p5==1) $p5_a++; if($p5==2) $p5_b++; if($p5==3) $p5_c++;

                if($cc1a==1) $cc1a_a++; if($cc1a==2) $cc1a_b++; if($cc1a==3) $cc1a_c++;
                if($cc1b==1) $cc1b_a++; if($cc1b==2) $cc1b_b++; if($cc1b==3) $cc1b_c++;
                if($cc1c==1) $cc1c_a++; if($cc1c==2) $cc1c_b++; if($cc1c==3) $cc1c_c++;

                if($cc2a==1) $cc2a_a++; if($cc2a==2) $cc2a_b++; if($cc2a==3) $cc2a_c++;
                if($cc2b==1) $cc2b_a++; if($cc2b==2) $cc2b_b++; if($cc2b==3) $cc2b_c++;

                if($cc3a==1) $cc3a_a++; if($cc3a==2) $cc3a_b++; if($cc3a==3) $cc3a_c++;
                if($cc3b==1) $cc3b_a++; if($cc3b==2) $cc3b_b++; if($cc3b==3) $cc3b_c++;
                if($cc3c==1) $cc3c_a++; if($cc3c==2) $cc3c_b++; if($cc3c==3) $cc3c_c++;
                if($cc3d==1) $cc3d_a++; if($cc3d==2) $cc3d_b++; if($cc3d==3) $cc3d_c++;
                if($cc3e==1) $cc3e_a++; if($cc3e==2) $cc3e_b++; if($cc3e==3) $cc3e_c++;
                if($cc3f==1) $cc3f_a++; if($cc3f==2) $cc3f_b++; if($cc3f==3) $cc3f_c++;

                if($cc4a==1) $cc4a_a++; if($cc4a==2) $cc4a_b++; if($cc4a==3) $cc4a_c++;
                if($cc4b==1) $cc4b_a++; if($cc4b==2) $cc4b_b++; if($cc4b==3) $cc4b_c++;
                if($cc4c==1) $cc4c_a++; if($cc4c==2) $cc4c_b++; if($cc4c==3) $cc4c_c++;
                if($cc4d==1) $cc4d_a++; if($cc4d==2) $cc4d_b++; if($cc4d==3) $cc4d_c++;
                if($cc4e==1) $cc4e_a++; if($cc4e==2) $cc4e_b++; if($cc4e==3) $cc4e_c++;
                if($cc4f==1) $cc4f_a++; if($cc4f==2) $cc4f_b++; if($cc4f==3) $cc4f_c++;

                if($cc5a==1) $cc5a_a++; if($cc5a==2) $cc5a_b++; if($cc5a==3) $cc5a_c++;
                if($cc5b==1) $cc5b_a++; if($cc5b==2) $cc5b_b++; if($cc5b==3) $cc5b_c++;
                if($cc5c==1) $cc5c_a++; if($cc5c==2) $cc5c_b++; if($cc5c==3) $cc5c_c++;

                if($cc6a==1) $cc6a_a++; if($cc6a==2) $cc6a_b++; if($cc6a==3) $cc6a_c++;
                if($cc6b==1) $cc6b_a++; if($cc6b==2) $cc6b_b++; if($cc6b==3) $cc6b_c++;
                if($cc6c==1) $cc6c_a++; if($cc6c==2) $cc6c_b++; if($cc6c==3) $cc6c_c++;

                if($cc7a==1) $cc7a_a++; if($cc7a==2) $cc7a_b++; if($cc7a==3) $cc7a_c++;
                if($cc7b==1) $cc7b_a++; if($cc7b==2) $cc7b_b++; if($cc7b==3) $cc7b_c++;
                if($cc7c==1) $cc7c_a++; if($cc7c==2) $cc7c_b++; if($cc7c==3) $cc7c_c++;
            }
        }

        $p1_a_p=round(($p1_a*100)/$total,2);
        $p1_b_p=round(($p1_b*100)/$total,2);
        $p1_c_p=round(($p1_c*100)/$total,2);
        $p2_a_p=round(($p2_a*100)/$total,2);
        $p2_b_p=round(($p2_b*100)/$total,2);
        $p2_c_p=round(($p2_c*100)/$total,2);
        $p3_a_p=round(($p3_a*100)/$total,2);
        $p3_b_p=round(($p3_b*100)/$total,2);
        $p3_c_p=round(($p3_c*100)/$total,2);
        $p4_a_p=round(($p4_a*100)/$total,2);
        $p4_b_p=round(($p4_b*100)/$total,2);
        $p4_c_p=round(($p4_c*100)/$total,2);
        $p5_a_p=round(($p5_a*100)/$total,2);
        $p5_b_p=round(($p5_b*100)/$total,2);
        $p5_c_p=round(($p5_c*100)/$total,2);

        $cc1a_a_p=round(($cc1a_a*100)/$total,2);
        $cc1a_b_p=round(($cc1a_b*100)/$total,2);
        $cc1a_c_p=round(($cc1a_c*100)/$total,2);
        $cc1b_a_p=round(($cc1b_a*100)/$total,2);
        $cc1b_b_p=round(($cc1b_b*100)/$total,2);
        $cc1b_c_p=round(($cc1b_c*100)/$total,2);
        $cc1c_a_p=round(($cc1c_a*100)/$total,2);
        $cc1c_b_p=round(($cc1c_b*100)/$total,2);
        $cc1c_c_p=round(($cc1c_c*100)/$total,2);
        $cc2a_a_p=round(($cc2a_a*100)/$total,2);
        $cc2a_b_p=round(($cc2a_b*100)/$total,2);
        $cc2a_c_p=round(($cc2a_c*100)/$total,2);
        $cc2b_a_p=round(($cc2b_a*100)/$total,2);
        $cc2b_b_p=round(($cc2b_b*100)/$total,2);
        $cc2b_c_p=round(($cc2b_c*100)/$total,2);
        $cc3a_a_p=round(($cc3a_a*100)/$total,2);
        $cc3a_b_p=round(($cc3a_b*100)/$total,2);
        $cc3a_c_p=round(($cc3a_c*100)/$total,2);
        $cc3b_a_p=round(($cc3b_a*100)/$total,2);
        $cc3b_b_p=round(($cc3b_b*100)/$total,2);
        $cc3b_c_p=round(($cc3b_c*100)/$total,2);
        $cc3c_a_p=round(($cc3c_a*100)/$total,2);
        $cc3c_b_p=round(($cc3c_b*100)/$total,2);
        $cc3c_c_p=round(($cc3c_c*100)/$total,2);
        $cc3d_a_p=round(($cc3d_a*100)/$total,2);
        $cc3d_b_p=round(($cc3d_b*100)/$total,2);
        $cc3d_c_p=round(($cc3d_c*100)/$total,2);
        $cc3e_a_p=round(($cc3e_a*100)/$total,2);
        $cc3e_b_p=round(($cc3e_b*100)/$total,2);
        $cc3e_c_p=round(($cc3e_c*100)/$total,2);
        $cc3f_a_p=round(($cc3f_a*100)/$total,2);
        $cc3f_b_p=round(($cc3f_b*100)/$total,2);
        $cc3f_c_p=round(($cc3f_c*100)/$total,2);
        $cc4a_a_p=round(($cc4a_a*100)/$total,2);
        $cc4a_b_p=round(($cc4a_b*100)/$total,2);
        $cc4a_c_p=round(($cc4a_c*100)/$total,2);
        $cc4b_a_p=round(($cc4b_a*100)/$total,2);
        $cc4b_b_p=round(($cc4b_b*100)/$total,2);
        $cc4b_c_p=round(($cc4b_c*100)/$total,2);
        $cc4c_a_p=round(($cc4c_a*100)/$total,2);
        $cc4c_b_p=round(($cc4c_b*100)/$total,2);
        $cc4c_c_p=round(($cc4c_c*100)/$total,2);
        $cc4d_a_p=round(($cc4d_a*100)/$total,2);
        $cc4d_b_p=round(($cc4d_b*100)/$total,2);
        $cc4d_c_p=round(($cc4d_c*100)/$total,2);
        $cc4e_a_p=round(($cc4e_a*100)/$total,2);
        $cc4e_b_p=round(($cc4e_b*100)/$total,2);
        $cc4e_c_p=round(($cc4e_c*100)/$total,2);
        $cc4f_a_p=round(($cc4f_a*100)/$total,2);
        $cc4f_b_p=round(($cc4f_b*100)/$total,2);
        $cc4f_c_p=round(($cc4f_c*100)/$total,2);
        $cc5a_a_p=round(($cc5a_a*100)/$total,2);
        $cc5a_b_p=round(($cc5a_b*100)/$total,2);
        $cc5a_c_p=round(($cc5a_c*100)/$total,2);
        $cc5b_a_p=round(($cc5b_a*100)/$total,2);
        $cc5b_b_p=round(($cc5b_b*100)/$total,2);
        $cc5b_c_p=round(($cc5b_c*100)/$total,2);
        $cc5c_a_p=round(($cc5c_a*100)/$total,2);
        $cc5c_b_p=round(($cc5c_b*100)/$total,2);
        $cc5c_c_p=round(($cc5c_c*100)/$total,2);
        $cc6a_a_p=round(($cc6a_a*100)/$total,2);
        $cc6a_b_p=round(($cc6a_b*100)/$total,2);
        $cc6a_c_p=round(($cc6a_c*100)/$total,2);
        $cc6b_a_p=round(($cc6b_a*100)/$total,2);
        $cc6b_b_p=round(($cc6b_b*100)/$total,2);
        $cc6b_c_p=round(($cc6b_c*100)/$total,2);
        $cc6c_a_p=round(($cc6c_a*100)/$total,2);
        $cc6c_b_p=round(($cc6c_b*100)/$total,2);
        $cc6c_c_p=round(($cc6c_c*100)/$total,2);
        $cc7a_a_p=round(($cc7a_a*100)/$total,2);
        $cc7a_b_p=round(($cc7a_b*100)/$total,2);
        $cc7a_c_p=round(($cc7a_c*100)/$total,2);
        $cc7b_a_p=round(($cc7b_a*100)/$total,2);
        $cc7b_b_p=round(($cc7b_b*100)/$total,2);
        $cc7b_c_p=round(($cc7b_c*100)/$total,2);
        $cc7c_a_p=round(($cc7c_a*100)/$total,2);
        $cc7c_b_p=round(($cc7c_b*100)/$total,2);
        $cc7c_c_p=round(($cc7c_c*100)/$total,2);

        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');
        $date=date('m/d/Y');

        $spreadsheet = $reader->load($url.'Integrated_Behavioral_Health_Care_Survey Results_Template.xlsx');

        $spreadsheet->setActiveSheetIndex(3);

        $spreadsheet->getActiveSheet(0)->setCellValue('C3',strval($p1_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D3',strval($p1_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E3',strval($p1_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F3',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C4',strval($p1_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D4',strval($p1_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E4',strval($p1_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F4',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C6',strval($p2_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D6',strval($p2_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E6',strval($p2_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F6',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C7',strval($p2_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D7',strval($p2_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E7',strval($p2_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F7',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C9',strval($p3_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D9',strval($p3_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E9',strval($p3_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F9',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C10',strval($p3_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D10',strval($p3_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E10',strval($p3_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F10',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C12',strval($p4_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D12',strval($p4_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E12',strval($p4_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F12',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C13',strval($p4_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D13',strval($p4_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E13',strval($p4_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F13',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C15',strval($p5_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D15',strval($p5_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E15',strval($p5_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F15',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C16',strval($p5_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D16',strval($p5_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E16',strval($p5_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F16',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C19',strval($cc1a_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D19',strval($cc1a_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E19',strval($cc1a_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F19',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C20',strval($cc1a_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D20',strval($cc1a_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E20',strval($cc1a_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F20',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C21',strval($cc1b_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D21',strval($cc1b_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E21',strval($cc1b_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F21',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C22',strval($cc1b_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D22',strval($cc1b_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E22',strval($cc1b_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F22',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C23',strval($cc1c_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D23',strval($cc1c_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E23',strval($cc1c_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F23',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C24',strval($cc1c_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D24',strval($cc1c_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E24',strval($cc1c_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F24',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C26',strval($cc2a_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D26',strval($cc2a_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E26',strval($cc2a_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F26',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C27',strval($cc2a_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D27',strval($cc2a_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E27',strval($cc2a_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F27',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C28',strval($cc2b_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D28',strval($cc2b_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E28',strval($cc2b_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F28',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C29',strval($cc2b_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D29',strval($cc2b_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E29',strval($cc2b_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F29',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C31',strval($cc3a_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D31',strval($cc3a_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E31',strval($cc3a_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F31',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C32',strval($cc3a_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D32',strval($cc3a_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E32',strval($cc3a_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F32',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C33',strval($cc3b_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D33',strval($cc3b_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E33',strval($cc3b_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F33',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C34',strval($cc3b_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D34',strval($cc3b_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E34',strval($cc3b_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F34',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C35',strval($cc3c_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D35',strval($cc3c_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E35',strval($cc3c_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F35',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C36',strval($cc3c_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D36',strval($cc3c_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E36',strval($cc3c_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F36',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C37',strval($cc3d_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D37',strval($cc3d_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E37',strval($cc3d_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F37',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C38',strval($cc3d_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D38',strval($cc3d_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E38',strval($cc3d_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F38',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C39',strval($cc3e_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D39',strval($cc3e_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E39',strval($cc3e_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F39',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C40',strval($cc3e_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D40',strval($cc3e_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E40',strval($cc3e_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F40',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C41',strval($cc3f_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D41',strval($cc3f_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E41',strval($cc3f_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F41',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C42',strval($cc3f_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D42',strval($cc3f_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E42',strval($cc3f_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F42',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C44',strval($cc4a_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D44',strval($cc4a_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E44',strval($cc4a_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F44',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C45',strval($cc4a_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D45',strval($cc4a_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E45',strval($cc4a_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F45',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C46',strval($cc4b_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D46',strval($cc4b_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E46',strval($cc4b_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F46',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C47',strval($cc4b_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D47',strval($cc4b_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E47',strval($cc4b_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F47',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C48',strval($cc4c_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D48',strval($cc4c_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E48',strval($cc4c_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F48',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C49',strval($cc4c_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D49',strval($cc4c_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E49',strval($cc4c_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F49',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C50',strval($cc4d_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D50',strval($cc4d_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E50',strval($cc4d_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F50',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C51',strval($cc4d_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D51',strval($cc4d_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E51',strval($cc4d_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F51',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C52',strval($cc4e_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D52',strval($cc4e_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E52',strval($cc4e_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F52',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C53',strval($cc4e_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D53',strval($cc4e_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E53',strval($cc4e_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F53',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C54',strval($cc4f_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D54',strval($cc4f_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E54',strval($cc4f_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F54',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C55',strval($cc4f_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D55',strval($cc4f_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E55',strval($cc4f_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F55',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C57',strval($cc5a_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D57',strval($cc5a_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E57',strval($cc5a_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F57',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C58',strval($cc5a_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D58',strval($cc5a_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E58',strval($cc5a_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F58',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C59',strval($cc5b_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D59',strval($cc5b_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E59',strval($cc5b_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F59',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C60',strval($cc5b_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D60',strval($cc5b_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E60',strval($cc5b_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F60',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C61',strval($cc5c_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D61',strval($cc5c_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E61',strval($cc5c_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F61',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C62',strval($cc5c_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D62',strval($cc5c_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E62',strval($cc5c_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F62',"100%");


        $spreadsheet->getActiveSheet(0)->setCellValue('C64',strval($cc6a_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D64',strval($cc6a_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E64',strval($cc6a_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F64',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C65',strval($cc6a_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D65',strval($cc6a_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E65',strval($cc6a_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F65',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C66',strval($cc6b_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D66',strval($cc6b_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E66',strval($cc6b_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F66',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C67',strval($cc6b_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D67',strval($cc6b_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E67',strval($cc6b_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F67',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C68',strval($cc6c_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D68',strval($cc6c_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E68',strval($cc6c_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F68',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C69',strval($cc6c_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D69',strval($cc6c_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E69',strval($cc6c_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F69',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C71',strval($cc7a_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D71',strval($cc7a_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E71',strval($cc7a_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F71',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C72',strval($cc7a_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D72',strval($cc7a_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E72',strval($cc7a_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F72',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C73',strval($cc7b_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D73',strval($cc7b_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E73',strval($cc7b_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F73',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C74',strval($cc7b_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D74',strval($cc7b_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E74',strval($cc7b_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F74',"100%");

        $spreadsheet->getActiveSheet(0)->setCellValue('C75',strval($cc7c_a));
        $spreadsheet->getActiveSheet(0)->setCellValue('D75',strval($cc7c_b));
        $spreadsheet->getActiveSheet(0)->setCellValue('E75',strval($cc7c_c));
        $spreadsheet->getActiveSheet(0)->setCellValue('F75',strval($total));
        $spreadsheet->getActiveSheet(0)->setCellValue('C76',strval($cc7c_a_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('D76',strval($cc7c_b_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('E76',strval($cc7c_c_p)."%");
        $spreadsheet->getActiveSheet(0)->setCellValue('F76',"100%");


        $spreadsheet->setActiveSheetIndex(2);

        $cont=2;

        $records=$em->getRepository('App\Entity\FCCSurvey')->findAll();

        if($records!=null){
            foreach ($records as $record){
                $cell_A = 'A' . $cont;
                $cell_B = 'B' . $cont;
                $cell_C = 'C' . $cont;
                $cell_D = 'D' . $cont;
                $cell_E = 'E' . $cont;
                $cell_F = 'F' . $cont;
                $cell_G = 'G' . $cont;
                $cell_H = 'H' . $cont;
                $cell_I = 'I' . $cont;
                $cell_J = 'J' . $cont;
                $cell_K = 'K' . $cont;
                $cell_L = 'L' . $cont;
                $cell_M = 'M' . $cont;
                $cell_N = 'N' . $cont;
                $cell_O = 'O' . $cont;
                $cell_P = 'P' . $cont;
                $cell_Q = 'Q' . $cont;
                $cell_R = 'R' . $cont;
                $cell_S = 'S' . $cont;
                $cell_T = 'T' . $cont;
                $cell_U = 'U' . $cont;
                $cell_V = 'V' . $cont;
                $cell_W = 'W' . $cont;
                $cell_X = 'X' . $cont;
                $cell_Y = 'Y' . $cont;
                $cell_Z = 'Z' . $cont;
                $cell_AA = 'AA' . $cont;
                $cell_AB = 'AB' . $cont;
                $cell_AC = 'AC' . $cont;
                $cell_AD = 'AD' . $cont;
                $cell_AE = 'AE' . $cont;
                $cell_AF = 'AF' . $cont;
                $cell_AG = 'AG' . $cont;
                $cell_AH = 'AH' . $cont;
                $cell_AI = 'AI' . $cont;
                $cell_AJ = 'AJ' . $cont;
                $cell_AK = 'AK' . $cont;

                $current=intval($cont-1);

                $spreadsheet->getActiveSheet(0)->setCellValue($cell_A,strval($current));
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_B,$record->getNpiNumber());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_D,$record->getProviderType());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_G,$record->getP1());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_H,$record->getP2());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_I,$record->getP3());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_J,$record->getP4());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_K,$record->getP5());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_L,$record->getCc1a());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_M,$record->getCc1b());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_N,$record->getCc1c());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_O,$record->getCc2a());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_P,$record->getCc2b());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_Q,$record->getCc3a());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_R,$record->getCc3b());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_S,$record->getCc3c());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_T,$record->getCc3d());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_U,$record->getCc3e());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_V,$record->getCc3f());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_W,$record->getCc4a());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_X,$record->getCc4b());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_Y,$record->getCc4c());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_Z,$record->getCc4d());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AA,$record->getCc4e());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AB,$record->getCc4f());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AC,$record->getCc5a());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AD,$record->getCc5b());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AE,$record->getCc5c());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AF,$record->getCc6a());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AG,$record->getCc6b());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AH,$record->getCc6c());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AI,$record->getCc7a());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AJ,$record->getCc7b());
                $spreadsheet->getActiveSheet(0)->setCellValue($cell_AK,$record->getCc7c());

                $cont++;
            }
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Integrated_Behavioral_Health_Care_Survey Results_'.date('Ymd').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");

    }
}
