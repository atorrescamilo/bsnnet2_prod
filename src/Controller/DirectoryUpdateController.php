<?php

namespace App\Controller;

use App\Entity\DirectoryUpdate;
use App\Service\JoinFileUploader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/directory-update")
 */
class DirectoryUpdateController extends AbstractController
{

    /**
     * @Route("/directory-update-list", name="directory_update_list")
     */
    public function list(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $sql = "SELECT d.id, organization.name as organization_name, user.name as  username, d.date,
            d.representative_name, d.phone, d.email
            FROM  directory_update d
            LEFT JOIN organization ON organization.id = d.organization_id
            LEFT JOIN user ON user.id = d.user_id
            GROUP BY d.id
            ORDER BY d.id";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $records = $stmt->fetchAll();

            return $this->render('directory_update/list.html.twig', ['records' => $records]);
        } else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/directory-update-view/{id}", name="directory_update_view",defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $document = $em->getRepository('App:DirectoryUpdate')->find($id);


            return $this->render('directory_update/view.html.twig', ['document' => $document]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/directory/update", name="directory_update")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();
            $organization = $user->getOrganization();

            $directory_update_records = [];
            $total_Records = 0;
            $show_form = true;
            $record = null;

            if ($organization) {
                $directory_update_records = $em->getRepository('App:DirectoryUpdate')->findBy(array('organization' => $organization), array('id' => 'ASC'));

                if ($directory_update_records) {
                    $total_Records = count($directory_update_records);
                    $record = $directory_update_records[intval($total_Records - 1)];
                }
            }

            $moths=0;

            if($record){
                $last_date = $record->getDate();
                $last_date_array = explode('/', $last_date);

                $begin = $last_date_array[2] . "-" . $last_date_array[0] . "-" . $last_date_array[1] . " 00:00:00";

                $current_date = date('Y-m-d H:i:s');


                $datetime1 = new \DateTime($begin);
                $datetime2 = new \DateTime($current_date);

                // obtenemos la diferencia entre las dos fechas
                $interval = $datetime2->diff($datetime1);

                // obtenemos la diferencia en meses
                $intervalMeses = $interval->format("%m");

                // obtenemos la diferencia en años y la multiplicamos por 12 para tener los meses
                $intervalAnos = $interval->format("%y") * 12;

                $moths = $intervalMeses + $intervalAnos;
            }


            if ($total_Records > 0 and $moths < 3) {
                $show_form = false;
            }

            return $this->render('directory_update/index.html.twig', [
                'organization' => $organization, 'total_Records' => $total_Records, 'show_form' => $show_form, 'directory_update_records' => $directory_update_records
            ]);
        } else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/save-directory-update", name="save_directory_update",methods={"POST"})
     */
    public function save(Request  $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $date = $request->get('date');
            $organization_name = $request->get('organization_name');
            $authorized_representative_name = $request->get('authorized_representative_name');
            $authorized_representative_phone = $request->get('authorized_representative_phone');
            $authorized_representative_email = $request->get('authorized_representative_email');

            $provider_name_correct = $request->get('provider_name_correct');
            $practice_name_correct = $request->get('practice_name_correct');
            $specialties_correct = $request->get('specialties_correct');
            $practice_correct_listed = $request->get('practice_correct_listed');
            $provider_accepting_patients = $request->get('provider_accepting_patients');
            $address_phone_correct = $request->get('address_phone_correct');

            $user = $this->getUser();
            $organization = $user->getOrganization();

            $directory = new DirectoryUpdate();

            if ($provider_name_correct) {
                $directory->setIsProviderNameCorrect(true);
            } else {
                $directory->setIsProviderNameCorrect(false);
            }

            if ($practice_name_correct) {
                $directory->setIsPracticeNameCorrect(true);
            } else {
                $directory->setIsPracticeNameCorrect(false);
            }

            if ($specialties_correct) {
                $directory->setIsSpecialtiesCorrectedListed(true);
            } else {
                $directory->setIsSpecialtiesCorrectedListed(false);
            }

            if ($practice_correct_listed) {
                $directory->setIsPracticeLocationesListed(true);
            } else {
                $directory->setIsPracticeLocationesListed(true);
            }

            if ($provider_accepting_patients) {
                $directory->setIsProviderAcceptinNewPatients(true);
            } else {
                $directory->setIsProviderAcceptinNewPatients(false);
            }

            if ($address_phone_correct) {
                $directory->setIsAddressPhoneCorrect(true);
            } else {
                $directory->setIsAddressPhoneCorrect(false);
            }

            $directory->setDate($date);
            $directory->setOrganizationName($organization_name);
            $directory->setOrganization($organization);
            $directory->setUser($user);
            $directory->setRepresentativeName($authorized_representative_name);
            $directory->setPhone($authorized_representative_phone);
            $directory->setEmail($authorized_representative_email);

            $em->persist($directory);
            $em->flush();

            return $this->redirectToRoute('home');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

}
