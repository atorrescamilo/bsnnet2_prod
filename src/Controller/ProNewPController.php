<?php

namespace App\Controller;

use App\Entity\ChangeLog;
use App\Entity\Provider;
use App\Form\ProviderPType;
use App\Form\ProviderPEditType;
use App\Utils\My_Mcript;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/admin/providers-org")
 */
class ProNewPController extends AbstractController
{
    /**
     * @Route("/list", name="pro_new_p_list")
    */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();
            $orgId=$this->getUser()->getOrganization()->getId();

            $sql="SELECT p.*,
            GROUP_CONCAT(DISTINCT `credentialing_status`.`name` ORDER BY `credentialing_status`.`name` ASC SEPARATOR ', ') AS `status_cred`,
            GROUP_CONCAT(DISTINCT `provider_credentialing`.`credentialing_effective_date` ORDER BY `provider_credentialing`.`id` ASC SEPARATOR ' ') AS `effective_date`,
            GROUP_CONCAT(DISTINCT `degree`.`name` ORDER BY `degree`.`name` ASC SEPARATOR ', ') AS `degrees`
            FROM  provider p 
            LEFT JOIN organization ON organization.id = p.org_id
            LEFT JOIN provider_credentialing ON provider_credentialing.npi_number = p.npi_number
            LEFT JOIN credentialing_status ON provider_credentialing.credentialing_status_id = credentialing_status.id
            LEFT JOIN provider_degree ON provider_degree.provider_id = p.id    
            LEFT JOIN degree ON degree.id = provider_degree.degree_id
            WHERE organization.id=$orgId and p.disabled=0
            GROUP BY p.id
            ORDER BY p.first_name";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $providers= $stmt->fetchAll();

            $terminationReason = $em->getRepository('App\Entity\TerminationReason')->findBy(array(),array('name'=>'ASC'));

        
            $terminationReasonResult=[];
            foreach ($terminationReason as $tr){
                if($tr->getId()==1 or $tr->getId()==2 or $tr->getId()==3 or $tr->getId()==4 or $tr->getId()==12){
                    $terminationReasonResult[]=$tr;
                }
            }

            return $this->render('pro_new_p/index.html.twig',[
                'providers'=>$providers,'organization'=>$orgId, 'terminationreasons' => $terminationReasonResult
            ]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/provider-new",name="pro_new_p_add")
     *
     */
    public function new2(Request  $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $private_key=$this->getParameter('private_key');
            $encoder=new My_Mcript($private_key);
            $provider = new Provider();

            $gender_acceptance['Both'] = 'Both';
            $gender_acceptance['Male'] = 'Male';
            $gender_acceptance['Female'] = 'Female';

            $hospital_privileges_type['[ -- Select One -- ]'] = '';
            $hospital_privileges_type['Admitting / Full and unrestricted'] = 'Admitting / Full and unrestricted';
            $hospital_privileges_type['Consulting'] = 'Consulting';
            $hospital_privileges_type['Surgical'] = 'Surgical';

            //get all specialty area by Type
            $specialties_mh=$em->getRepository('App:SpecialtyAreaProvider')->findBy(array('specialty_area_type'=>1));
            $specialties_sa=$em->getRepository('App:SpecialtyAreaProvider')->findBy(array('specialty_area_type'=>2));
            $specialties_se=$em->getRepository('App:SpecialtyAreaProvider')->findBy(array('specialty_area_type'=>3));


            $formOptions = array('gender_acceptance' => $gender_acceptance,'hospital_privileges_type'=>$hospital_privileges_type);


            $form = $this->createForm(ProviderPType::class, $provider, $formOptions);
            $form->handleRequest($request);
            $validate=$form['validation_required']->getData();

            if($form->isSubmitted() && $form->isValid() &&  $validate==1){

                $social=$form['social']->getData();
                $specialties_areas_ids=$form['specialties_area']->getData();

                $specialties_areas_ids = substr($specialties_areas_ids, 0, -1);
                $lbs = explode(",", $specialties_areas_ids);

                if ($lbs != "") {
                    foreach ($lbs as $lb) {
                        $specialties_area = $em->getRepository('App:SpecialtyAreaProvider')->find($lb);
                        if ($specialties_area) {
                            $provider->addSpecialtiesAreon($specialties_area);
                        }
                    }
                }

                $social_encode=$encoder->encryptthis($social);

                $user=$this->getUser();
                $organization=$user->getOrganization();

                $provider->setSocial($social_encode);
                $provider->setValidNpiNumber(true);
                $provider->setIsFacility(false);
                $provider->setDisabled(false);
                $provider->setOrganization($organization);
                $em->persist($provider);
                $em->flush();

                //verify address in the organization and set it to providers (only if the organization have one)
                $current_addr=$provider->getBillingAddress();
                $addresses=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization));

                if($current_addr==null or count($current_addr)==0){
                    if(count($addresses)==1){
                        foreach ($addresses as $addr){
                            $provider->addBillingAddress($addr);
                            $em->persist($provider);
                        }
                    }
                }

                $em->flush();
                return $this->redirectToRoute('pro_new_p_list');
            }

            if($form->isSubmitted() &&  $validate==0){

                $social=$form['social']->getData();
                $specialties_areas_ids=$form['specialties_area']->getData();

                $specialties_areas_ids = substr($specialties_areas_ids, 0, -1);
                $lbs = explode(",", $specialties_areas_ids);

                if ($lbs != "") {
                    foreach ($lbs as $lb) {
                        $specialties_area = $em->getRepository('App:SpecialtyAreaProvider')->find($lb);
                        if ($specialties_area) {
                            $provider->addSpecialtiesAreon($specialties_area);
                        }
                    }
                }

                $social_encode=$encoder->encryptthis($social);

                $user=$this->getUser();
                $organization=$user->getOrganization();

                $provider->setSocial($social_encode);
                $provider->setValidNpiNumber(true);
                $provider->setIsFacility(false);
                $provider->setDisabled(false);
                $provider->setOrganization($organization);
                $em->persist($provider);
                $em->flush();

                //verify address in the organization and set it to providers (only if the organization have one)
                $current_addr=$provider->getBillingAddress();
                $addresses=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization));

                if($current_addr==null or count($current_addr)==0){
                    if(count($addresses)==1){
                        foreach ($addresses as $addr){
                            $provider->addBillingAddress($addr);
                            $em->persist($provider);
                        }
                    }
                }

                //create the action logs
                $this->SaveLog(1,$provider->getId(),"New Provider from Provider Portal", "provider_portal");


                $em->flush();
                return $this->redirectToRoute('pro_new_p_list');
            }

            return $this->render('pro_new_p/form.html.twig', ['form' => $form->createView(), 'action' => 'New','specialties_mh'=>$specialties_mh,
                'specialties_sa'=>$specialties_sa,'specialties_se'=>$specialties_se, 'errors'=>null]);
        }
        else {
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/provider-edit/{id}",name="pro_new_p_edit", defaults={"id": null})
     *
     */
    public function edit2(Request  $request, $id, ValidatorInterface $validator){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $private_key=$this->getParameter('private_key');
            $encoder=new My_Mcript($private_key);


            $provider = $em->getRepository('App:Provider')->find($id);
            $social=$encoder->decryptthis($provider->getSocial());
            $provider->setSocial($social);
            $errors = $validator->validate($provider);

            //get all specialty area by Type
            $specialties_mh=$em->getRepository('App:SpecialtyAreaProvider')->findBy(array('specialty_area_type'=>1));
            $specialties_sa=$em->getRepository('App:SpecialtyAreaProvider')->findBy(array('specialty_area_type'=>2));
            $specialties_se=$em->getRepository('App:SpecialtyAreaProvider')->findBy(array('specialty_area_type'=>3));

            $provider_specialties_areas=$provider->getSpecialtiesArea();

            $gender_acceptance['both'] = 'Both';
            $gender_acceptance['male'] = 'Male';
            $gender_acceptance['female'] = 'Female';

            $hospital_privileges_type['[ -- Select One -- ]'] = '';
            $hospital_privileges_type['Admitting / Full and unrestricted'] = 'Admitting / Full and unrestricted';
            $hospital_privileges_type['Consulting'] = 'Consulting';
            $hospital_privileges_type['Surgical'] = 'Surgical';

            $formOptions = array('gender_acceptance' => $gender_acceptance, 'hospital_privileges_type'=>$hospital_privileges_type);

            $form = $this->createForm(ProviderPType::class, $provider, $formOptions);
            $form->handleRequest($request);

            $validate=$form['validation_required']->getData();

            if ($form->isSubmitted() && $form->isValid() &&  $validate==1) {
                $social = $form['social']->getData();
                $social_encode = $encoder->encryptthis($social);

                $specialties_areas_ids=$form['specialties_area']->getData();

                $specialties_areas_ids = substr($specialties_areas_ids, 0, -1);
                $lbs = explode(",", $specialties_areas_ids);

                $current_sa=$provider->getSpecialtiesArea();
                if($current_sa){
                    foreach ($current_sa as $item){
                        $provider->removeSpecialtiesAreon($item);
                    }

                    $em->flush();
                }

                if ($lbs != "") {
                    foreach ($lbs as $lb) {
                        $specialties_area = $em->getRepository('App:SpecialtyAreaProvider')->find($lb);
                        if ($specialties_area) {
                            $provider->addSpecialtiesAreon($specialties_area);
                        }
                    }
                }

                if ($social != "") {
                    $provider->setSocial($social_encode);
                }
                $em->persist($provider);
                $em->flush();

                //create the action logs
                $this->SaveLog(2,$provider->getId(),"Edit Provider from Provider Portal", "provider_portal");

                return $this->redirectToRoute('pro_new_p_list');
            }

            if ($form->isSubmitted() && $validate==0) {

                $social = $form['social']->getData();
                $social_encode = $encoder->encryptthis($social);

                $specialties_areas_ids=$form['specialties_area']->getData();

                $specialties_areas_ids = substr($specialties_areas_ids, 0, -1);
                $lbs = explode(",", $specialties_areas_ids);

                $current_sa=$provider->getSpecialtiesArea();
                if($current_sa){
                    foreach ($current_sa as $item){
                        $provider->removeSpecialtiesAreon($item);
                    }

                    $em->flush();
                }

                if ($lbs != "") {
                    foreach ($lbs as $lb) {
                        $specialties_area = $em->getRepository('App:SpecialtyAreaProvider')->find($lb);
                        if ($specialties_area) {
                            $provider->addSpecialtiesAreon($specialties_area);
                        }
                    }
                }

                if ($social != "") {
                    $provider->setSocial($social_encode);
                }
                $em->persist($provider);
                $em->flush();

                return $this->redirectToRoute('pro_new_p_list');
            }

            return $this->render('pro_new_p/form.html.twig', ['form' => $form->createView(), 'action' => 'Edit','specialties_mh'=>$specialties_mh,
                'specialties_sa'=>$specialties_sa,'specialties_se'=>$specialties_se,'provider_specialties_areas'=>$provider_specialties_areas,
                'errors' => $errors]);
        }
        else {
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/view/{id}/{tab}", name="pro_new_p_view_provider",defaults={"id": null, "tab": 1} )
    */
    public function view($id,$tab){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $document = $em->getRepository('App\Entity\Provider')->find($id);

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('admin_provider');
            }

            $credentialing = $em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('provider' => $id));
            $delete_form_ajax = $this->createCustomForm('PROVIDERCREDENTIALING_ID', 'DELETE', 'admin_delete_provider_credentialing');

            $private_key=$this->getParameter('private_key');
            $encoder=new My_Mcript($private_key);
            $social="";
            if($document){
                $social=$encoder->decryptthis($document->getSocial());
            }

            $addresses=$document->getBillingAddress();
            //Get al addresses for the organization
            $organization=$document->getOrganization();
            $allAddressess=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));

            $npi=$document->getNpiNumber();
            $medicaid=$document->getMedicaid();


            //load all File for Credentialing
            $cred_files=$em->getRepository('App\Entity\ProviderCredFile')->findBy(array('provider'=>$id));

            return $this->render('pro_new_p/view.html.twig', array('allAddressess'=>$allAddressess,'document' => $document, 'credentialings' => $credentialing,'social'=>$social, 'tab' => $tab,
                'addresses'=>$addresses,'cred_files'=>$cred_files,'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }

    private function SaveLog($action_id,$entity_id,$note,$source){
        $em=$this->getDoctrine()->getManager();
        $user=$this->getUser();

        $log=new ChangeLog();
        $log->setUser($user);
        $action=$em->getRepository('App\Entity\ActionLog')->find($action_id);
        $log->setAction($action);
        $log->setEntityId($entity_id);
        $log->setEntityClass('Provider');
        $log->setSource($source);
        $log->setNote($note);

        $em->persist($log);
        $em->flush();
    }
}
