<?php

namespace App\Controller;

use App\Entity\Organization;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/convert")
 */
class ConvertFromPPortalController extends AbstractController
{
    /**
     * @Route("/organization", name="convert_from_organization")
     */
    public function index()
    {
        $em=$this->getDoctrine()->getManager();
        $organziationsP=$em->getRepository('App\Entity\OrganizationP')->findBy(array('is_inserted'=>0));

        foreach ($organziationsP as $organizationP){

            $orgNew=new Organization();

            // Inserted from Provider Portal
            $insertedFromObj=$em->getRepository('App\Entity\InsertedFrom')->find(2);
            $orgNew->setInsertedFrom($insertedFromObj);

            echo $organizationP->getId()."<br/>";

        }


        return new  Response('Process Completed');
    }
}
