<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\OrganizationContact;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/organizationcontact")
 */
class OrganizationContactController extends AbstractController
{

    /**
     * @Route("/new/{id}", name="admin_new_organizationcontact")
     */
    public function add($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $organization = $em->getRepository('App\Entity\Organization')->find($id);
            $bests=$em->getRepository('App\Entity\ContactPBestChoice')->findAll();

            return $this->render('organizationcontact/add.html.twig', array('organization' => $organization, 'bests'=>$bests));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/report", name="admin_aggrid_organizationcontact")
     */
    public function aggrid(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $contacts = $em->getRepository('App\Entity\OrganizationContact')->findBy(array(),array('id'=>'ASC'));

            $contactVerify=[];

            foreach ($contacts as $contact){
                $email=$contact->getEmail();
                if($email!=""){
                    $user=$em->getRepository('App\Entity\User')->findBy(array('email'=>$email));
                    if($user!=null){
                        $contactVerify[$contact->getId()]="Yes";
                    }else{
                        $contactVerify[$contact->getId()]="No";
                    }
                }
            }

            $email_templates=$em->getRepository('App\Entity\EmailType')->findBy(array(),array('name'=>'ASC'));

            return $this->render('organizationcontact/aggrid.html.twig', array('contacts' => $contacts,'contactVerify'=>$contactVerify,
                'email_templates'=>$email_templates));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}/{id_org}", name="admin_edit_organizationcontact", defaults={"id": null, "id_org": null})
     */
    public function edit($id,$id_org)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\OrganizationContact')->find($id);

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('admin_organizationcontact');
            }

            $organization = $em->getRepository('App\Entity\Organization')->find($id_org);
            $bests=$em->getRepository('App\Entity\ContactPBestChoice')->findAll();

            return $this->render('organizationcontact/edit.html.twig', array('document' => $document, 'organization' => $organization,
            'bests'=>$bests));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}/{id_org}", name="admin_view_organizationcontact", defaults={"id": null, "id_org": null})
     */
    public function view($id,$id_org)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\OrganizationContact')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_organizationcontact');
            }

            $organization = $em->getRepository('App\Entity\Organization')->find($id_org);
            $bests=$document->getBestChoices();

            return $this->render('organizationcontact/view.html.twig', array('document' => $document,
                'organization' => $organization,
                'bests'=>$bests
            ));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_organizationcontact")
     */
    public function create(Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $title = $request->get('title_str');
            $phone = $request->get('phone');
            $phone_ext = $request->get('phone_ext');
            $mobile = $request->get('mobile');
            $fax = $request->get('fax');
            $email = $request->get('email');
            $new = $request->get('new');
            $id_organization = $request->get('id_organization');
            $bestSelected=$request->get('bestSelected');
            $organization = $em->getRepository('App\Entity\Organization')->find($id_organization);

            $organizationcontact = new OrganizationContact();
            $organizationcontact->setName($name);
            if ($organization != null) {
                $organizationcontact->setOrganization($organization);
            }

            $organizationcontact->setTitleStr($title);
            $organizationcontact->setPhone($phone);
            $organizationcontact->setPhoneExt($phone_ext);
            $organizationcontact->setMobile($mobile);
            $organizationcontact->setFax($fax);
            $organizationcontact->setEmail($email);

            $em->persist($organizationcontact);
            $em->flush();

            $bests = substr($bestSelected, 0, -1);
            $lbs = explode(",", $bests);

            //remove bests
            $bestLast = $organizationcontact->getBestChoices();
            if ($bestLast != null) {
                foreach ($bestLast as $lng) {
                    $organizationcontact->removeBestChoice($lng);
                }
            }
            foreach ($lbs as $bests) {
                if ($organizationcontact != null) {
                    $bestObj= $em->getRepository('App\Entity\ContactPBestChoice')->find($bests);
                    if ($bests != null) {
                        $organizationcontact->addBestChoice($bestObj);
                    }
                }
            }

            $em->persist($organizationcontact);
            $em->flush();

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_organizationcontact', ['id' => $id_organization]);
            }

            return $this->redirectToRoute('admin_view_organization', ['id' => $id_organization, 'tab' => 4]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_organizationcontact")
     */
    public function update(Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $title = $request->get('title_str');
            $phone = $request->get('phone');
            $phone_ext = $request->get('phone_ext');
            $mobile = $request->get('mobile');
            $fax = $request->get('fax');
            $email = $request->get('email');
            $id_organization = $request->get('id_organization');
            $bestSelected=$request->get('bestSelected');


            $new = $request->get('new');
            $id = $request->get('id');

            $organizationcontact = $em->getRepository('App\Entity\OrganizationContact')->find($id);

            if ($organizationcontact == null) {
                $this->addFlash(
                    "danger",
                    "The Contact can't been updated!"
                );

                return $this->redirectToRoute('admin_organizationcontact');
            }

            if ($organizationcontact != null) {
                $organizationcontact->setName($name);
                $organizationcontact->setTitleStr($title);
                $organizationcontact->setPhone($phone);
                $organizationcontact->setPhoneExt($phone_ext);
                $organizationcontact->setMobile($mobile);
                $organizationcontact->setFax($fax);
                $organizationcontact->setEmail($email);

                $bests = substr($bestSelected, 0, -1);
                $lbs = explode(",", $bests);

                //remove bests
                $bestLast = $organizationcontact->getBestChoices();
                if ($bestLast != null) {
                    foreach ($bestLast as $lng) {
                        $organizationcontact->removeBestChoice($lng);
                    }
                }
                foreach ($lbs as $bests) {
                    if ($organizationcontact != null) {
                        $bestObj= $em->getRepository('App\Entity\ContactPBestChoice')->find($bests);
                        if ($bests != null) {
                            $organizationcontact->addBestChoice($bestObj);
                        }
                    }
                }

                $em->persist($organizationcontact);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "The Contact has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_organizationcontact', ['id' => $id_organization]);
            }

            return $this->redirectToRoute('admin_view_organization', ['id' => $id_organization, 'tab' => 4]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_organizationcontact",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $organizationcontact = $organizationcontact = $em->getRepository('App\Entity\OrganizationContact')->find($id);
            $removed = 0;
            $message = "";

            if ($organizationcontact) {
                try {
                    $em->remove($organizationcontact);
                    $em->flush();
                    $removed = 1;
                    $message = "The Contact has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Contact can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_organizationcontact",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $organizationcontact = $organizationcontact = $em->getRepository('App\Entity\OrganizationContact')->find($id);

                if ($organizationcontact) {
                    try {
                        $em->remove($organizationcontact);
                        $em->flush();
                        $removed = 1;
                        $message = "The Contact has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Contact can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/contacts-elegibles-medicaid", name="admin_elegible_medicaid_organizationcontact")
     */
    public function elegiblesMedicaid(Request $request){
        $orgs=[84,130,2,179,44,140,67,83,36,28,94,175,174,131,166,164,50,122,20,173,153,26,156,128,157,72,85,160,149,46,147,180,37,
            64,34,137,21,183,5,4,74,32,11,126,45,89,42,19,182,49,116,95,108,154,177,161,62,48,63,6,141,31,80,144,13,107,115,66,117,105,58,103,55,
            68,41,113,106,123,163,112,25,75,120,16,51,69,133,119,93,184,76,71,7,77,47,57,104,10,24,110,30,145,40,159,18,139,53,79,60,9,142,81,91,
            88,54,65,100,138,35,92,155,151,101,23,102,86,38,73,146,61,121,132,118,22,78,125,15,152,178,185,90,12,186,188,192,198,194,197,191,195,
            199,200,201,202,203,204,205,206,209,210,211,212,213,214,216,217,218,221,222,223,224,225,226,228,229,230,231,232,233,234,235,236,237,
            238,240,242,243,244,246,247,248,249,250,251,252,253,294,302,306,280,282,308,258,298,276,293,284,299,281,268,311,270,274,283,272,254,
            287,296,277,300,304,285,275,269,286,266,290,271,288,267,255,303,279,257,264,263,261,292,310,309,295,262,297,291,289,256,312,313,314,315,
            316,317,318,319,320,321,322,323,324,307,325,326,327,328,329,330,331,333,334,335,336,337,338,339,342,346,345,343,349,350,352,14,353,354,
            355,356,357,358,359,361,362,363,364,365,366,367,368,369,370,371,372,374,375,376,377,378,379,380,382,384,385,386,387,388,389,390,391,392,
            341,344,196,148,70,162,394,396,398,399,400,404,397,405,406,402,407,408,410,412,413,415,381,416,417,418,421,422,124,423,424,428,429,430,431,432,433,
            434,435,437,440,441,445,446,442,447,448,451,452,458,460,459,463,464,465,466,470,471,468,474,475,476,478,482,484,486,490,495,500,511,488];

        $em=$this->getDoctrine()->getManager();
        $cont=1;
        $contactsId=[];
        foreach ($orgs as $org){
            $organization=$em->getRepository('App\Entity\Organization')->find($org);
            $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$org));
            if($contacts!=null){
                foreach ($contacts as $contact){
                    $email=$contact->getEmail();
                    if(!in_array($email,$contactsId)){
                        $contactsId[]=$email;
                        echo $contact->getPhone()."<br/>";
                        $cont++;
                    }

                }
            }
        }


        die();
    }


    private function createCustomForm($id, $method, $route)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }

    /**
     * @Route("/best-option-report", name="admin_best-option-report_organizationcontact")
     */
    public function bestOptionReport(Request $request){
        $em = $this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App:Organization')->findAll();

        $cont=1;
        foreach ($organizations as $organization){
            $contacts=$em->getRepository('App:OrganizationContact')->findBy(array('organization'=>$organization->getId()));

            if($contacts==null){
                echo $organization->getGroupNpi()." ".$organization->getName()."<br/>";
            }else{
                if(count($contacts)==1){
                    foreach ($contacts as $contact){

                        //remove al besOption
                        $choises=$contact->getBestChoices();
                        if($choises!=null){
                            foreach ($choises as $choise){
                                $contact->removeBestChoice($choise);
                            }
                            $em->persist($contact);
                            $em->flush();
                        }

                        $choise1=$em->getRepository('App:ContactPBestChoice')->find(1);
                        $choise2=$em->getRepository('App:ContactPBestChoice')->find(2);
                        $choise3=$em->getRepository('App:ContactPBestChoice')->find(3);
                        $choise4=$em->getRepository('App:ContactPBestChoice')->find(4);

                        $contact->addBestChoice($choise1);
                        $contact->addBestChoice($choise2);
                        $contact->addBestChoice($choise3);
                        $contact->addBestChoice($choise4);

                        $em->persist($contact);
                        $em->flush();
                    }
                    $cont++;
                }


            }
        }

        die();
    }

    /**
     * @Route("/best-option-report-2", name="admin_best-option-report_organizationcontact_2")
     */
    public function bestOptionReport2(Request $request){
        $em = $this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App:Organization')->findAll();

        $cont=1;
        foreach ($organizations as $organization){
            $contacts=$em->getRepository('App:OrganizationContact')->findBy(array('organization'=>$organization->getId()));

            if($contacts==null){
                echo $organization->getGroupNpi()." ".$organization->getName()."<br/>";
            }else{
                if(count($contacts)>1){
                    $cont_billing=0;
                    $cont_credentialing=0;
                    $cont_contrating=0;
                    $cont_roster=0;

                    foreach ($contacts as $contact){
                        $choises=$contact->getBestChoices();
                        if($choises!=null){
                            foreach ($choises as $choise){
                                if($choise->getId()==1){
                                    $cont_billing++;
                                }
                                if($choise->getId()==2){
                                    $cont_credentialing++;
                                }
                                if($choise->getId()==3){
                                    $cont_contrating++;
                                }
                                if($choise->getId()==4){
                                    $cont_roster++;
                                }
                            }
                        }
                    }

                    if($cont_billing==0 or $cont_credentialing==0 or $cont_contrating==0 or $cont_roster==0){
                        echo $cont_roster."<br>";
                        /*echo $organization->getGroupNpi()." ".$organization->getName()." ".$cont_billing." ".$cont_credentialing." ".$cont_contrating." ".$cont_roster."<br>";
                        echo $organization->getGroupNpi()." ".$organization->getName()." ".$cont_billing." ".$cont_credentialing." ".$cont_contrating." ".$cont_roster."<br>";
                        echo $organization->getGroupNpi()." ".$organization->getName()." ".$cont_billing." ".$cont_credentialing." ".$cont_contrating." ".$cont_roster."<br>";
                        echo $organization->getGroupNpi()." ".$organization->getName()." ".$cont_billing." ".$cont_credentialing." ".$cont_contrating." ".$cont_roster."<br>";
                        echo $organization->getGroupNpi()." ".$organization->getName()." ".$cont_billing." ".$cont_credentialing." ".$cont_contrating." ".$cont_roster."<br>";
                        echo $organization->getGroupNpi()." ".$organization->getName()." ".$cont_billing." ".$cont_credentialing." ".$cont_contrating." ".$cont_roster."<br>";
                        echo $organization->getGroupNpi()." ".$organization->getName()." ".$cont_billing." ".$cont_credentialing." ".$cont_contrating." ".$cont_roster."<br>";
                        echo $organization->getGroupNpi()." ".$organization->getName()." ".$cont_billing." ".$cont_credentialing." ".$cont_contrating." ".$cont_roster."<br>";
                        echo $organization->getGroupNpi()." ".$organization->getName()." ".$cont_billing." ".$cont_credentialing." ".$cont_contrating." ".$cont_roster."<br>";*/
                        $cont++;
                    }
                }


            }
        }

        die();
    }



}

