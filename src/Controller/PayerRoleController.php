<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\PayerRole;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/payer_role")
 */
class PayerRoleController extends AbstractController{

    /**
     * @Route("/index", name="admin_payer_role")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $roles = $em->getRepository('App\Entity\PayerRole')->findAll();

            $delete_form_ajax = $this->createCustomForm('ROLE_ID', 'DELETE', 'admin_delete_payer_role');


            return $this->render('payer_role/index.html.twig', array('roles' => $roles, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_payer_role")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('payer_role/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_payer_role", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\PayerRole')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_payer_role');
            }

            return $this->render('payer_role/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_payer_role", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\PayerRole')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_payer_role');
            }

            return $this->render('payer_role/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_payer_role")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');

            $role = new PayerRole();
            $role->setName($name);
            $role->setDescription($description);

            $em->persist($role);
            $em->flush();

            $this->addFlash(
                'success',
                'Payer Role has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_payer_role');
            }

            return $this->redirectToRoute('admin_payer_role');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_payer_role")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $role = $em->getRepository('App\Entity\PayerRole')->find($id);

            if ($role == null) {
                $this->addFlash(
                    "danger",
                    "The Payer Role can't been updated!"
                );

                return $this->redirectToRoute('admin_payer_role');
            }

            if ($role != null) {
                $role->setName($name);
                $role->setDescription($description);

                $em->persist($role);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "Payer Role has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_payer_role');
            }

            return $this->redirectToRoute('admin_payer_role');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_payer_role",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $role = $role = $em->getRepository('App\Entity\PayerRole')->find($id);
            $removed = 0;
            $message = "";

            if ($role) {
                try {
                    $em->remove($role);
                    $em->flush();
                    $removed = 1;
                    $message = "The Payer Role has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Payer Role can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple__payer_role",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $role = $role = $em->getRepository('App\Entity\PayerRole')->find($id);

                if ($role) {
                    try {
                        $em->remove($role);
                        $em->flush();
                        $removed = 1;
                        $message = "The Payer Role has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Payer Role can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
