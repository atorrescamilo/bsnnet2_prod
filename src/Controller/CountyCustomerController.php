<?php

namespace App\Controller;

use App\Entity\PayerCustomerCounty;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\CountyCustomerDataType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/county-customer")
 */
class CountyCustomerController extends AbstractController
{
    /**
     * @Route("/index", name="county_customer_index")
     */
    public function index()
    {
        return $this->render('county_customer/index.html.twig', [
            'controller_name' => 'CountyCustomerController',
        ]);
    }

    /**
     * @Route("/new/{payer}", name="county_customer_add", defaults={"payer":null})
    */
    public function add(Request $request, $payer)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $county_data=new PayerCustomerCounty();
            $form=$this->createForm(CountyCustomerDataType::class, $county_data);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){

                $payerObj=$em->getRepository('App:Payer')->find($payer);
                $county_data->setPayer($payerObj);
                $em->persist($county_data);
                $em->flush();
                
                $action_type=$form['action_type']->getData();

                if($action_type==1){
                    return  $this->redirectToRoute('admin_view_payer',['id'=>$payer]);
                }else{
                    return  $this->redirectToRoute('county_customer_add',['payer'=>$payer]);
                }
            }

            return $this->render('county_customer/form.html.twig', ['form'=>$form->createView(),'action' => 'New','payer'=>$payer]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit/{id}", name="county_customer_edit", defaults={"id": null})
    */
    public function edit(Request $request, $id)
    {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $county_data=$em->getRepository('App:PayerCustomerCounty')->find($id);
            $payer=$county_data->getPayer()->getId();
            $form=$this->createForm(CountyCustomerDataType::class, $county_data);

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){

                $em->persist($county_data);
                $em->flush();

                $action_type=$form['action_type']->getData();

                if($action_type==1){
                    return  $this->redirectToRoute('admin_view_payer',['id'=>$payer]);
                }else{
                    return  $this->redirectToRoute('county_customer_add',['payer'=>$payer]);
                }
            }

            return $this->render('county_customer/form.html.twig', ['form'=>$form->createView(),'action' => 'Edit','payer'=>$payer]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_county_customer",methods={"POST","DELETE"})
    */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $countyCustomer = $em->getRepository('App:PayerCustomerCounty')->find($id);
            $removed = 0;
            $message = "";

            if ($countyCustomer) {
                try {
                    $em->remove($countyCustomer);
                    $em->flush();
                    $removed = 1;
                    $message = "The Payer Customer County has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Payer Customer County can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_county_customer",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $cvo= $em->getRepository('App:PayerCustomerCounty')->find($id);

                if ($cvo) {
                    try {
                        $em->remove($cvo);
                        $em->flush();
                        $removed = 1;
                        $message = "The Payer Customer County has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Payer Customer County can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

}
