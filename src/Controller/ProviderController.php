<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\ChangeLog;
use App\Entity\Organization;
use App\Entity\PDataMedicare;
use App\Entity\ProviderCredentialing;
use App\Entity\ProviderPayerExcluded;
use App\Entity\TaxonomyCode;
use App\Form\ProviderPType;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Shared\JAMA\QRDecomposition;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Provider;
use App\Service\ProviderFileUploader;
use PhpOffice\PhpSpreadsheet\Reader;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Utils\My_Mcript;
use Vich\UploaderBundle\Naming\OrignameNamer;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * @Route("/admin/provider")
 */
class ProviderController extends AbstractController{

    /**
     * @Route("/list", name="admin_provider_list")
     */
    public function list(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $delete_form_ajax = $this->createCustomForm('PROVIDER_ID', 'DELETE', 'admin_delete_provider');

            $terminationReason = $em->getRepository('App\Entity\TerminationReason')->findBy(array(),array('name'=>'ASC'));

            $cvos=$em->getRepository('App\Entity\Cvo')->findAll();
            $credentialingStatus=$em->getRepository('App\Entity\CredentialingStatus')->findAll();
            $payers=$em->getRepository('App\Entity\Payer')->findAll();

            $organizations=$em->getRepository('App\Entity\Organization')->findBy(array('disabled'=>0));

            $sql="SELECT p.id, p.first_name, p.last_name, organization.name as organization_name, p.npi_number, p.state_lic,
            p.medicare, p.medicaid, p.caqh,
            GROUP_CONCAT(DISTINCT degree.name ORDER BY degree.name ASC SEPARATOR ', ') AS degrees, p.is_facility, p.disabled,
            GROUP_CONCAT(DISTINCT credentialing_status.name ORDER BY credentialing_status.name ASC SEPARATOR ', ') AS status_cred,
            GROUP_CONCAT(DISTINCT `payer`.`alias` ORDER BY `payer`.`id` ASC SEPARATOR ',') AS `payers`,
            GROUP_CONCAT(DISTINCT `taxonomy_code`.`code` ORDER BY `taxonomy_code`.`id` ASC SEPARATOR ',') AS `taxonomies`,
            GROUP_CONCAT(DISTINCT organization_credentialing.credentialing_status_id ORDER BY organization_credentialing.credentialing_status_id ASC SEPARATOR ', ') AS status_cred_facility
            FROM  provider p
            LEFT JOIN organization ON p.org_id = organization.id
            LEFT JOIN provider_degree ON provider_degree.provider_id = p.id
            LEFT JOIN degree ON degree.id = provider_degree.degree_id
            LEFT JOIN provider_credentialing ON provider_credentialing.npi_number = p.npi_number
            LEFT JOIN credentialing_status ON provider_credentialing.credentialing_status_id = credentialing_status.id
            LEFT JOIN provider_billing_address ON provider_billing_address.provider_id = p.id
            LEFT JOIN billing_address ON billing_address.id = provider_billing_address.billing_address_id
            LEFT JOIN organization_credentialing ON organization_credentialing.organization_id = organization.id
            LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
            LEFT JOIN payer ON payer.id = provider_payer.payer_id
            LEFT JOIN provider_taxonomy_code ON provider_taxonomy_code.provider_id = p.id
            LEFT JOIN taxonomy_code ON taxonomy_code.id = provider_taxonomy_code.taxonomy_id
            GROUP BY p.id
            ORDER BY p.first_name";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $providers= $stmt->fetchAll();

            return $this->render('provider/list.html.twig', array('payers'=>$payers,'credentialingStatus'=>$credentialingStatus,'cvos'=>$cvos,
                'providers' => $providers,'organizations'=>$organizations,'terminationreasons' => $terminationReason, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/ag-report", name="admin_provider_agreport")
    */
    public function aggrid(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $sql="SELECT p.id, p.first_name, p.last_name, organization.name as organization_name, p.npi_number, p.state_lic,
            p.medicare, p.medicaid, p.caqh,
            GROUP_CONCAT(DISTINCT degree.name ORDER BY degree.name ASC SEPARATOR ', ') AS degrees, p.is_facility, p.disabled,
            GROUP_CONCAT(DISTINCT credentialing_status.name ORDER BY credentialing_status.name ASC SEPARATOR ', ') AS status_cred,
            GROUP_CONCAT(DISTINCT `payer`.`alias` ORDER BY `payer`.`id` ASC SEPARATOR ',') AS `payers`,
            GROUP_CONCAT(DISTINCT `taxonomy_code`.`code` ORDER BY `taxonomy_code`.`id` ASC SEPARATOR ',') AS `taxonomies`,
            GROUP_CONCAT(DISTINCT organization_credentialing.credentialing_status_id ORDER BY organization_credentialing.credentialing_status_id ASC SEPARATOR ', ') AS status_cred_facility
            FROM  provider p
            LEFT JOIN organization ON p.org_id = organization.id
            LEFT JOIN provider_degree ON provider_degree.provider_id = p.id
            LEFT JOIN degree ON degree.id = provider_degree.degree_id
            LEFT JOIN provider_credentialing ON provider_credentialing.npi_number = p.npi_number
            LEFT JOIN credentialing_status ON provider_credentialing.credentialing_status_id = credentialing_status.id
            LEFT JOIN provider_billing_address ON provider_billing_address.provider_id = p.id
            LEFT JOIN billing_address ON billing_address.id = provider_billing_address.billing_address_id
            LEFT JOIN organization_credentialing ON organization_credentialing.organization_id = organization.id
            LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
            LEFT JOIN payer ON payer.id = provider_payer.payer_id
            LEFT JOIN provider_taxonomy_code ON provider_taxonomy_code.provider_id = p.id
            LEFT JOIN taxonomy_code ON taxonomy_code.id = provider_taxonomy_code.taxonomy_id
            GROUP BY p.id
            ORDER BY p.first_name";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $providers= $stmt->fetchAll();

            return $this->render('provider/aggrid.html.twig', array('providers' => $providers));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new/{org}", name="admin_new_provider", defaults={"org": null})
    */
    public function add($org){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $organizations = $em->getRepository('App\Entity\Organization')->findBy(array(),array('name'=>'ASC'));
            $generalCategories = $em->getRepository('App\Entity\GeneralCategories')->findBy(array(),array('name'=>'ASC'));
            $lineofbusiness = $em->getRepository('App\Entity\LineOfBusiness')->findBy(array(),array('name'=>'ASC'));
            $terminationReason = $em->getRepository('App\Entity\TerminationReason')->findBy(array(),array('name'=>'ASC'));
            $languages = $em->getRepository('App\Entity\Languages')->findBy(array(),array('ord'=>'ASC'));
            $providertypes = $em->getRepository('App\Entity\ProviderType')->findBy(array(),array('name'=>'ASC'));
            $degrees = $em->getRepository('App\Entity\Degree')->findBy(array(),array('name'=>'ASC'));
            $specialties = $em->getRepository('App\Entity\Specialty')->findBy(array(),array('name'=>'ASC'));
            $taxonomies=$em->getRepository('App\Entity\TaxonomyCode')->findAll();
            $age_ranges=$em->getRepository('App:AgesRange')->findAll();


            return $this->render('provider/add.html.twig', array('organizations' => $organizations,'age_ranges'=>$age_ranges,
                'generalCategories' => $generalCategories, 'lineofbusiness' => $lineofbusiness, 'terminationreasons' => $terminationReason,
                'languages' => $languages, 'providertypes' => $providertypes, 'degrees' => $degrees, 'specialties' => $specialties, 'org' => $org,'taxonomies'=>$taxonomies));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new-2/{org}", name="admin_new_provider_2", defaults={"org": null})
     */
    public function add2($org, Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            $em = $this->getDoctrine()->getManager();
            $private_key=$this->getParameter('private_key');
            $encoder=new My_Mcript($private_key);
            $provider = new Provider();

            $gender_acceptance['Both'] = 'Both';
            $gender_acceptance['Male'] = 'Male';
            $gender_acceptance['Female'] = 'Female';

            $hospital_privileges_type['[ -- Select One -- ]'] = '';
            $hospital_privileges_type['Admitting / Full and unrestricted'] = 'Admitting / Full and unrestricted';
            $hospital_privileges_type['Consulting'] = 'Consulting';
            $hospital_privileges_type['Surgical'] = 'Surgical';

            //get all specialty area by Type
            $specialties_mh=$em->getRepository('App:SpecialtyAreaProvider')->findBy(array('specialty_area_type'=>1));
            $specialties_sa=$em->getRepository('App:SpecialtyAreaProvider')->findBy(array('specialty_area_type'=>2));
            $specialties_se=$em->getRepository('App:SpecialtyAreaProvider')->findBy(array('specialty_area_type'=>3));


            $formOptions = array('gender_acceptance' => $gender_acceptance,'hospital_privileges_type'=>$hospital_privileges_type);


            $form = $this->createForm(ProviderPType::class, $provider, $formOptions);
            $form->handleRequest($request);
            $validate=$form['validation_required']->getData();

            if($form->isSubmitted() && $form->isValid() &&  $validate==1){

                $social=$form['social']->getData();
                $specialties_areas_ids=$form['specialties_area']->getData();

                $specialties_areas_ids = substr($specialties_areas_ids, 0, -1);
                $lbs = explode(",", $specialties_areas_ids);

                if ($lbs != "") {
                    foreach ($lbs as $lb) {
                        $specialties_area = $em->getRepository('App:SpecialtyAreaProvider')->find($lb);
                        if ($specialties_area) {
                            $provider->addSpecialtiesAreon($specialties_area);
                        }
                    }
                }

                $social_encode=$encoder->encryptthis($social);

                $user=$this->getUser();
                $organization=$user->getOrganization();

                $provider->setSocial($social_encode);
                $provider->setValidNpiNumber(true);
                $provider->setIsFacility(false);
                $provider->setDisabled(false);
                $provider->setOrganization($organization);
                $em->persist($provider);
                $em->flush();

                //verify address in the organization and set it to providers (only if the organization have one)
                $current_addr=$provider->getBillingAddress();
                $addresses=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization));

                if($current_addr==null or count($current_addr)==0){
                    if(count($addresses)==1){
                        foreach ($addresses as $addr){
                            $provider->addBillingAddress($addr);
                            $em->persist($provider);
                        }
                    }
                }

                $em->flush();
                return $this->redirectToRoute('pro_new_p_list');
            }

            if($form->isSubmitted() &&  $validate==0){

                $social=$form['social']->getData();
                $specialties_areas_ids=$form['specialties_area']->getData();

                $specialties_areas_ids = substr($specialties_areas_ids, 0, -1);
                $lbs = explode(",", $specialties_areas_ids);

                if ($lbs != "") {
                    foreach ($lbs as $lb) {
                        $specialties_area = $em->getRepository('App:SpecialtyAreaProvider')->find($lb);
                        if ($specialties_area) {
                            $provider->addSpecialtiesAreon($specialties_area);
                        }
                    }
                }

                $social_encode=$encoder->encryptthis($social);

                $user=$this->getUser();
                $organization=$user->getOrganization();

                $provider->setSocial($social_encode);
                $provider->setValidNpiNumber(true);
                $provider->setIsFacility(false);
                $provider->setDisabled(false);
                $provider->setOrganization($organization);
                $em->persist($provider);
                $em->flush();

                //verify address in the organization and set it to providers (only if the organization have one)
                $current_addr=$provider->getBillingAddress();
                $addresses=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization));

                if($current_addr==null or count($current_addr)==0){
                    if(count($addresses)==1){
                        foreach ($addresses as $addr){
                            $provider->addBillingAddress($addr);
                            $em->persist($provider);
                        }
                    }
                }

                //create the action logs
                $this->SaveLog(1,$provider->getId(),"New Provider from Provider Portal", "provider_portal");


                $em->flush();
                return $this->redirectToRoute('pro_new_p_list');
            }

            return $this->render('provider/form.html.twig', ['form' => $form->createView(), 'action' => 'New','specialties_mh'=>$specialties_mh,
                'specialties_sa'=>$specialties_sa,'specialties_se'=>$specialties_se, 'errors'=>null]);

        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/upload/{id}", name="admin_upload_provider", defaults={"id":null})
    */
    function upload($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->render('provider/upload.html.twig',array('org'=>$id));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/upload-with-addrs/{id}", name="admin_upload_provider_addr", defaults={"id":null})
     */
    function uploadAddr($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->render('provider/upload_addr.html.twig',array('org'=>$id));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create_ajax", name="admin_create_ajax_provider")
     */
    function createAjax(Request $request){
            $em = $this->getDoctrine()->getManager();
            $private_key=$this->getParameter('private_key');
            $encoder=new My_Mcript($private_key);

            $organization=$request->get('organization');
            $firstname = $request->get('fisrtname');
            $lastname = $request->get('lastname');
            $initial = $request->get('initial');
            $dob = $request->get('dob');
            $social = $request->get('social');
            $social_encode=$encoder->encryptthis($social);
            $gender = $request->get('gender');
            $npi = $request->get('npi');
            $caqh = $request->get('caqh');
            $state_licence_number = $request->get('statelicencenumber');
            $state_license_issue_date = $request->get('statelicenseissuedate');
            $languages=$request->get('languages');
            $provider_type=$request->get('provider_type');
            $provider_specialty=$request->get('provider_specialty');
            $medicaid=$request->get('medicaid');
            $medicare=$request->get('medicare');
            $degrees=$request->get('degrees');
            $exist=false;

            $providerId=0;
            if($exist==false) {
                $provider = new Provider();
                $organizationObj = $em->getRepository('App\Entity\Organization')->find($organization);
                $provider->setFirstName($firstname);
                $provider->setLastName($lastname);
                $provider->setOrganization($organizationObj);
                $provider->setDisabled(0);
                $provider->setInitial($initial);
                $provider->setDateOfBirth($dob);
                $provider->setSocial($social_encode);
                $provider->setGender($gender);

                if(strlen($npi)<10){
                    $provider->setNpiNumber("N/A");
                }else{
                    $provider->setNpiNumber($npi);
                }

                $provider->setCaqh($caqh);
                $provider->setStateLic($state_licence_number);
                $provider->setLicIssueDate($state_license_issue_date);
                $provider->setDisabled(0);

                $degreesArray = explode(",", $degrees);

                if (count($degreesArray) > 0) {
                    foreach ($degreesArray as $de) {
                        $degreeObj = $em->getRepository('App\Entity\Degree')->findOneBy(array('name' => $de));
                        if ($degreeObj != null) {
                            $provider->addDegree($degreeObj);
                        }
                    }
                }

                $languagesArray = explode(",", $languages);

                if (count($languagesArray) > 0) {
                    foreach ($languagesArray as $lan) {
                        $languageObj = $em->getRepository('App\Entity\Languages')->findOneBy(array('name' => $lan));
                        if ($languageObj != null) {
                            $provider->addLanguage($languageObj);
                        }
                    }
                }

                $provider_typeArray = explode(",", $provider_type);

                if (count($provider_typeArray) > 0) {
                    foreach ($provider_typeArray as $pt) {
                        $ptObj = $em->getRepository('App\Entity\ProviderType')->findOneBy(array('name' => $pt));
                        if ($ptObj != null) {
                            $provider->addProviderType($ptObj);
                        }
                    }
                }

                $provider_specialtyArray = explode(",", $provider_specialty);

                if (count($provider_specialtyArray) > 0) {
                    foreach ($provider_specialtyArray as $ps) {
                        $psObj = $em->getRepository('App\Entity\Specialty')->findOneBy(array('name' => $ps));
                        if ($psObj != null) {
                            $provider->addPrimarySpecialty($psObj);
                        }
                    }
                }

                $provider->setMedicaid($medicaid);
                $provider->setMedicare($medicare);

                $em->persist($provider);
                $em->flush();
                $providerId=$provider->getId();
                $this->setFacilityProvider($providerId);
            }

         return new Response(
                json_encode(array('id' =>$providerId,'exist'=>$exist)), 200, array('Content-Type' => 'application/json')
         );
    }

    /**
     * @Route("/upload-proccess", name="admin_upload_proccess_provider")
     */
    function uploadProccess(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $file = $request->files->get('providers_load');
            $em = $this->getDoctrine()->getManager();

            $organization=$request->get('org');
            $orgObj=$em->getRepository('App\Entity\Organization')->find($organization);
            $providersOrg=$this->providersByOrganization($organization);

            $terminationReason = $em->getRepository('App\Entity\TerminationReason')->findBy(array(),array('name'=>'ASC'));

            $fileName = "";
            if ($file != "") {
                $nombreoriginal = $file->getClientOriginalName();
                $fileName = $nombreoriginal . '-' . time() . '.' . $file->guessExtension();
                $adjuntosDir = $this->getparameter('providers_loaded');
                $file->move($adjuntosDir, $fileName);
            }

            //step for read the excel and read de information
            $fileToRead = $this->getparameter('providers_loaded') . "/" . $fileName;

            $reader = new Reader\Xlsx();
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($fileToRead);
            $spreadsheet->setActiveSheetIndex(0);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

            $cont = 0;
            $totalExist = 0;
            $totalExistOrganization=0;

            $providersResult = array();
            $provider = array();
            $headerColumns = array();
            $npiColumn = "";

            $columsDef = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T'];

            foreach ($sheetData as $sheet) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    //set header for table
                    if ($cont == 0) {
                        foreach ($columsDef as $colD) {
                            if (isset($sheet[$colD])) {
                                if ($sheet[$colD] != null or $sheet[$colD] != "") {
                                    $headerColumns[] = $sheet[$colD];
                                    if ($sheet[$colD] == "Provider NPI Number") {
                                        $npiColumn = $colD;
                                    }
                                }
                            }
                        }
                    }
                    if ($cont > 0) {
                        $totalColumns = count($headerColumns);
                        $column = 1;
                        $provider['exist'] = false;
                        $provider['exist_org'] = false;
                        foreach ($columsDef as $colD) {
                            if ($column <= $totalColumns) {
                                //check if npi number exist on database
                                $c=0;
                                $d=0;
                                if ($colD == $npiColumn) {
                                    $npi = $sheet[$colD];
                                    $providerObj = $em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number' => $npi));
                                    if ($providerObj != null) {
                                        $c=1;
                                        $totalExist++;
                                        //check if this providerExist on this Organization
                                        $contProv_Org=0;
                                        $org=$providerObj->getOrganization();

                                        if($org->getId()==$organization){
                                            $contProv_Org++;
                                            $d=1;
                                            $c=0;
                                            $totalExist--;
                                        }
                                    }
                                    if($c==1){
                                        $provider['exist'] = true;
                                    }
                                    if($d==1){
                                        $provider['exist_org'] = true;
                                        $totalExistOrganization++;
                                    }
                                    $provider['column' . $column] = $sheet[$colD];
                                    $provider['npi']=$sheet[$colD];
                                }else{
                                    if ($sheet[$colD] != null || $sheet[$colD] != "") {
                                        if ($colD=="E" or $colD=="K" ) {
                                            $provider['column' . $column] = $this->coverDateFromExcel($sheet[$colD]);
                                        } else {
                                            $provider['column' . $column] = $sheet[$colD];
                                        }
                                    } else {
                                        $provider['column' . $column] = "-";
                                    }
                                }
                            }
                            $column++;
                        }
                        $providersResult[] = $provider;
                    }
                    $cont++;
                }
            }

            $providerProperties=$em->getRepository('App\Entity\ProviderProperties')->findBy(array(),array('name'=>'ASC'));
            $billingAddress=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization));

            $totalNew=count($providersResult)-($totalExist+$totalExistOrganization);

            return $this->render('provider/processUploadSteps.html.twig', array('cont' => ($cont - 3), 'providers' => $providersResult, 'totalExist' => $totalExist,
                'headerColumns' => $headerColumns,'org'=>$organization, 'providerProperties'=>$providerProperties,'totalExistOrganization'=>$totalExistOrganization,'providersOrg'=>$providersOrg,
                'orgObj'=>$orgObj,'terminationreasons' => $terminationReason,'billingAddress'=>$billingAddress,'totalNew'=>$totalNew));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function coverDateFromExcel($date){
        if($date=="N/A" or $date=="" or $date=="n/a"){
            return "N/A";
        }else{
            $unix_date = ($date - 25569) * 86400;
            $date = 25569 + ($unix_date / 86400);
            $unix_date = ($date - 25569) * 86400;

            return gmdate("m-d-Y", $unix_date);
        }
    }

    /**
     * @Route("/edit/{id}", name="admin_edit_provider",defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Provider')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_provider_list');
            }

            $organizations = $em->getRepository('App\Entity\Organization')->findAll();
            $generalCategories = $em->getRepository('App\Entity\GeneralCategories')->findAll();
            $lineofbusiness = $em->getRepository('App\Entity\LineOfBusiness')->findAll();
            $terminationReason = $em->getRepository('App\Entity\TerminationReason')->findAll();
            $languages = $em->getRepository('App\Entity\Languages')->findAll(array('ord' => 'ASC'));
            $providertypes = $em->getRepository('App\Entity\ProviderType')->findAll(array('name' => 'ASC'));
            $degrees = $em->getRepository('App\Entity\Degree')->findAll(array('name' => 'ASC'));
            $specialties = $em->getRepository('App\Entity\Specialty')->findAll(array('name' => 'ASC'));
            $age_ranges=$em->getRepository('App:AgesRange')->findAll();


            $private_key=$this->getParameter('private_key');
            $encoder=new My_Mcript($private_key);
            $social="";
            if($document){
                $social=$encoder->decryptthis($document->getSocial());
            }

            $taxonomies=$em->getRepository('App\Entity\TaxonomyCode')->findAll();
            $hospitals=$em->getRepository('App\Entity\FloridaHospital')->findAll();


            return $this->render('provider/edit.html.twig', array('document' => $document, 'organizations' => $organizations,'hospitals'=>$hospitals,'age_ranges'=>$age_ranges,
                'generalCategories' => $generalCategories, 'lineofbusiness' => $lineofbusiness, 'terminationreasons' => $terminationReason,
                'languages' => $languages, 'providertypes' => $providertypes, 'degrees' => $degrees, 'specialties' => $specialties,'social'=>$social,'taxonomies'=>$taxonomies));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}/{tab}", name="admin_view_provider",defaults={"id": null, "tab": 1} )
    */
    public function view($id,$tab){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $document = $em->getRepository('App:Provider')->find($id);

            $delete_payer_assigned_form_ajax = $this->createCustomForm('PAYER_ID', 'DELETE', 'admin_delete_payer_assigned');

            if ($document == null) {
                //create a flash message
                return $this->redirectToRoute('admin_provider');
            }

            $credentialing = $em->getRepository('App:ProviderCredentialing')->findBy(array('npi_number' => $document->getNpiNumber()));
            $delete_form_ajax = $this->createCustomForm('PROVIDERCREDENTIALING_ID', 'DELETE', 'admin_delete_provider_credentialing');

            $private_key=$this->getParameter('private_key');
            $encoder=new My_Mcript($private_key);
            $social="";
            if($document){
                $social=$encoder->decryptthis($document->getSocial());
            }

            $addresses=$document->getBillingAddress();
            //Get al addresses for the organization
            $allAddressess=array();

            $organization=$document->getOrganization();

            $addrs=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));

            if($addrs!=null){
                foreach ($addrs as $addr){
                    $allAddressess[]=$addr;
                }
            }

            $npi=$document->getNpiNumber();
            $medicaid=$document->getMedicaid();
            $sql="SELECT * FROM pml2 p WHERE p.col_n=:pml or p.col_a=:medicaid";
            $stmt = $conn->prepare($sql);
            $stmt->execute(array('pml'=>$npi,'medicaid'=>$medicaid));
            $pmls= $stmt->fetchAll();

            //load all File for Credentialing
            $cred_files=$em->getRepository('App\Entity\ProviderCredFile')->findBy(array('provider'=>$id));

            $bc_specialties=array();
            $bcs=$document->getBoardCertifiedSpecialty();

            if($bcs!=""){
                $bc_specialties=explode(',',$bcs);
            }

            $thps=array();
            $thp=$document->getHopitalPrivilegesType();

            if($thp!=""){
                $thps=explode(',',$thp);
            }

            //load info from NPPES NPI Registry
            $npi=$document->getNpiNumber();

            $pdata=$em->getRepository('App\Entity\PDataMedicare')->findOneBy(array('npi_number'=>$npi));
            $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=1&skip=&pretty=on&version=2.1";

            $addressPdata=array();
            $pdataArray=array();

            $json="";
            if($pdata!=null){
                $json=$pdata->getData();
            }else{
                $json=file_get_contents($url_result);
            }

            $datos=json_decode($json,true);

            $taxonomies=[];
            if(count($datos)>1){
                $pdataArray['npi']=$datos['results'][0]['number'];
                $pdataArray['name']=$datos['results'][0]['basic']['first_name']." ".$datos['results'][0]['basic']['last_name'];
                $pdataArray['status']=$datos['results'][0]['basic']['status'];
                $pdataArray['npi_type']=$datos['results'][0]['enumeration_type'];

                $address=$datos['results'][0]['addresses'];
                $addressPdata=$datos['results'][0]['addresses'];

                foreach ($address as $addr){
                    if($addr['address_purpose']=="LOCATION"){
                        $addr_str="";
                        $addr_str=$addr['address_1'].",".$addr['address_2']." \n".$addr['city']." , ".$addr['state']." ";
                        $codeP=$addr['postal_code'];
                        $codeP=substr($codeP,0,5)."-".substr($codeP,5,4);
                        $addr_str.=$codeP;
                        $pdataArray['prymary_location']=$addr_str;
                        $pdataArray['phone']=$addr['telephone_number'];
                    }
                }

                $taxonomies=$datos['results'][0]['taxonomies'];

                foreach ($taxonomies as $taxonomy){
                    if($taxonomy['primary']==true){
                        $pdataArray['taxonomy']=$taxonomy['desc']." - (".$taxonomy['code'].")";
                    }
                }
            }


            return $this->render('provider/view.html.twig', array('pmls'=>$pmls,'allAddressess'=>$allAddressess,'document' => $document, 'credentialings' => $credentialing,'social'=>$social, 'tab' => $tab,
                'addresses'=>$addresses,'cred_files'=>$cred_files,'delete_form_ajax' => $delete_form_ajax->createView(), 'delete_payer_assigned_form_ajax' => $delete_payer_assigned_form_ajax->createView(),
                'bc_specialties'=>$bc_specialties,'thps'=>$thps,'pdata'=>$pdataArray,'addressPdata'=>$addressPdata,'taxonomies'=>$taxonomies));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_provider")
    */
    public function create(Request $request,ProviderFileUploader $fileUploader){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $db = $em->getConnection();
            $private_key=$this->getParameter('private_key');
            $encoder=new My_Mcript($private_key);

            $save_status = $request->get('save_status');
            $page = $request->get('page');
            $id = 0;

            if ($save_status == 0) {
                $provider = new Provider();
            } else {
                $provider = $em->getRepository('App\Entity\Provider')->find($save_status);
            }

            //save fisrt part for provider
            if ($page == 0) {
                $npi_number = $request->get('npi_number');
                $name = $request->get('name');
                $last_name = $request->get('last_name');
                $initial = $request->get('initial');
                $organization = $request->get('organization');
                $gender = $request->get('gender');
                $dateofbirth = $request->get('dateofbirth');
                $social = $request->get('social');
                $social_encode=$encoder->encryptthis($social);
                $caqh = $request->get('caqh');
                $na_caqh=$request->get('na_caqh');
                $pending_caqh=$request->get('pending_caqh');
                $taxonomies=$request->get('taxonomies');
                $gender_acceptance = $request->get('gender_acceptance');
                $application_receipt_date=$request->get('application_receipt_date');
                $agesRangeSelected = $request->get('agesRangeSelected');

                $general_category = $request->get('general_category');
                $lineofbusinessSelected = $request->get('lineofbusinessSelected');
                $medicare = $request->get('medicare');
                $medicare_out=$request->get('medicare_out');
                $medicaid = $request->get('medicaid');

                $state_lic = $request->get('state_lic');
                $licstate = $request->get('licstate');
                $license_expires_date=$request->get('license_expires_date');
                $license_issue_date=$request->get('license_issue_date');

                $board_certified = $request->get('board_certified');
                $board_name = $request->get('board_name');
                $board_certified_date = $request->get('board_certified_date');
                $board_certified_specialty=$request->get('board_certified_specialty');
                $certification_expires_date=$request->get('certification_expires_date');

                $is_hospital_privileges=$request->get('is_hospital_privileges');
                $is_peer_support=$request->get('is_peer_support');
                $hospital_privileges_type=$request->get('hospital_privileges_type');
                $hospital_privileges=$request->get('hospital_privileges');
                $hospital_ahca_number=$request->get('hospital_ahca_number');

                $has_alf_and_ltc=$request->get('has_alf_and_ltc');
                $alf_and_ltc = $request->get('alf_and_ltc');
                $has_telemedicine_service=$request->get('has_telemedicine_service');
                $telemedicine_company_name=$request->get('telemedicine_company_name');
                $interested_telemedicine_services = $request->get('interested_telemedicine_services');
                $accepting_new_patients = $request->get('accepting_new_patients');
                $are_you_pcp = $request->get('are_you_pcp');
                $notes = $request->get('notes');
                $confirmed_doh=$request->get('confirmed_doh');


                //step 1
                $provider->setNpiNumber($npi_number);
                $provider->setFirstName($name);
                $provider->setLastName($last_name);
                $provider->setInitial($initial);
                $organizationObj=$em->getRepository('App\Entity\Organization')->find($organization);

                if($organizationObj!=null){
                    $provider->setOrganization($organizationObj);
                }else{
                    $provider->setOrganization(null);
                }
                $provider->setGender($gender);
                $provider->setDateOfBirth($dateofbirth);
                $provider->setSocial($social_encode);
                $provider->setCaqh($caqh);
                $provider->setNaCaqh($na_caqh);
                $provider->setPeddingCaqh($pending_caqh);
                $provider->setProviderApplicationReceiptDate($application_receipt_date);
                //remove taxonomies code
                $taxonomiesObj = $provider->getTaxonomyCodes();
                if ($taxonomiesObj != null) {
                    foreach ($taxonomiesObj as $taxonomyObj) {
                        $provider->removeTaxonomyCode($taxonomyObj);
                    }
                }

                if ($taxonomies != "") {
                    foreach ($taxonomies as $lb) {
                        $taxonomyObj = $em->getRepository('App\Entity\TaxonomyCode')->find($lb);
                        if ($taxonomyObj != null) {
                            $provider->addTaxonomyCode($taxonomyObj);
                        }
                    }
                }

                if ($gender_acceptance != "0") {
                    $test=1;
                    $provider->setGenderAcceptance($gender_acceptance);
                }

                //step 2
                //remove General categories
                $categories = $provider->getGeneralCategories();
                if ($categories != null) {
                    foreach ($categories as $gc) {
                        $provider->removeGeneralCategory($gc);
                    }
                }

                if ($general_category != "") {
                    foreach ($general_category as $gc) {
                        $generalCategory = $em->getRepository('App\Entity\GeneralCategories')->find($gc);
                        if ($generalCategory != null) {
                            $provider->addGeneralCategory($generalCategory);
                        }

                    }
                }
                //remove lines of bussiness
                $lines = $provider->getLineOfBusiness();
                if ($lines != null) {
                    foreach ($lines as $line) {
                        $provider->removeLineOfBusines($line);
                    }
                }

                $lineofbusiness = substr($lineofbusinessSelected, 0, -1);
                $lbs = explode(",", $lineofbusiness);

                if ($lbs != "") {
                    foreach ($lbs as $lb) {
                        $lineOfB = $em->getRepository('App\Entity\LineOfBusiness')->find($lb);
                        if ($lineOfB != null) {
                            $provider->addLineOfBusines($lineOfB);
                        }
                    }
                }

                //remove age ranges
                $ages_range = $provider->getAgeRanges();
                if ($ages_range != null) {
                    foreach ($ages_range as $age_range) {
                        $provider->removeAgeRange($age_range);
                    }
                }

                $agesRange = substr($agesRangeSelected, 0, -1);
                $lbs = explode(",", $agesRange);

                if ($lbs != "") {
                    foreach ($lbs as $lb) {
                        $Age = $em->getRepository('App:AgesRange')->find($lb);
                        if ($Age != null) {
                            $provider->addAgeRange($Age);
                        }
                    }
                }

                $provider->setMedicare($medicare);
                $provider->setMedicaid($medicaid);
                $provider->setOptionOutMedicare($medicare_out);

                //step 3
                $provider->setStateLic($state_lic);
                $provider->setLicState($licstate);
                $provider->setLicExpiresDate($license_expires_date);
                $provider->setLicIssueDate($license_issue_date);
                $provider->setConfirmedDoh($confirmed_doh);

                //step 4
                $provider->setBoardCertified($board_certified);
                $provider->setBoardName($board_name);
                $provider->setBoardCertifiedDate($board_certified_date);
                $provider->setBoardCertifiedSpecialty($board_certified_specialty);
                $provider->setBoardCertificationExpiresDate($certification_expires_date);

                //step 5
                $provider->setIsHospitalAffiliations($is_hospital_privileges);
                $provider->setHopitalPrivilegesType($hospital_privileges_type);
                $provider->setHopitalPrivileges($hospital_privileges);
                $provider->setHospitalAchaNumber($hospital_ahca_number);
                $provider->setPeerSupport($is_peer_support);

                //step 6
                $provider->setHasResidentialServices($has_alf_and_ltc);
                $provider->setInterestedInResidentialServices($alf_and_ltc);
                $provider->setHasTelemedicine($has_telemedicine_service);
                $provider->setTelemedicineCompany($telemedicine_company_name);
                $provider->setInterestedInTelemedicine($interested_telemedicine_services);
                $provider->setAcceptingNewPatients($accepting_new_patients);
                $provider->setHasPcp($are_you_pcp);
                $provider->setNotes($notes);


                $provider->setDisabled(0);

                $em->persist($provider);
                $em->flush();

                //save log actions
                if ($save_status == 0) {
                    $this->SaveLog(1,'Provider',$id,'New provider');
                }else{
                    $this->SaveLog(2,'Provider',$id,'Edit provider');
                }

                $id = $provider->getId();
                $this->setFacilityProvider($id);

                //if the organization have only one address then set it to provider
                $addresses=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization));
                if($addresses!=null){
                    $contF=0; //count for facilities
                    foreach ($addresses as $address){
                       if($address->getIsFacility()==true){
                           $contF++;
                       }
                    }

                    if($contF>0){
                        $provider->setIsFacility(true);
                    }

                    $em->persist($provider);
                }

                //check the organization in the organization
                $current_addr=$provider->getBillingAddress();
                if($current_addr==null or count($current_addr)==0){
                    if(count($addresses)==1){
                        foreach ($addresses as $addr){
                            $provider->addBillingAddress($addr);
                            $em->persist($provider);
                        }
                    }
                }

                $em->flush();

            }

            //set languages
            if ($page == 3) {
                $id = $request->get('id');
                $languagesSelected = $request->get('languagesSelected');
                $provider = $em->getRepository('App\Entity\Provider')->find($id);

                $sqlDelete = "DELETE FROM `provider_languages` WHERE `provider_languages`.`provider_id` = $id";
                $stmt = $db->prepare($sqlDelete);
                $stmt->execute();

                $languages = substr($languagesSelected, 0, -1);
                $lbs = explode(",", $languages);

                //remove languages
                $languagesLast = $provider->getLanguages();
                if ($languagesLast != null) {
                    foreach ($languagesLast as $lng) {
                        $provider->removeLanguage($lng);
                    }
                }
                foreach ($lbs as $language) {
                    if ($provider != null) {
                        $lang = $em->getRepository('App\Entity\Languages')->find($language);
                        if ($lang != null) {
                            $provider->addLanguage($lang);
                        }
                    }
                }

                $em->persist($provider);
                $em->flush();

            }

            //set provider Type and Primary Specialties
            if ($page == 2) {
                $id = $request->get('id');
                $providertypesSelected = $request->get('providertypesSelected');
                $specialtiesSelected = $request->get('specialtiesSelected');

                $provider = $em->getRepository('App\Entity\Provider')->find($id);

                $sqlDelete = "DELETE FROM `provider_provider_types` WHERE `provider_provider_types`.`provider_id` = $id";
                $stmt = $db->prepare($sqlDelete);
                $stmt->execute();

                $sqlDelete = "DELETE FROM `provider_primary_specialties` WHERE `provider_primary_specialties`.`provider_id` = $id";
                $stmt = $db->prepare($sqlDelete);
                $stmt->execute();

                $providertypes = substr($providertypesSelected, 0, -1);
                $lbs = explode(",", $providertypes);

                foreach ($lbs as $providertype) {
                    if ($provider != null) {
                        $providerT = $em->getRepository('App\Entity\ProviderType')->find($providertype);
                        if ($providerT != null) {
                            $provider->addProviderType($providerT);
                        }
                    }
                }

                $specialties = substr($specialtiesSelected, 0, -1);
                $lbs = explode(",", $specialties);

                foreach ($lbs as $specialty) {
                    if ($provider != null) {
                        $specialtyObj = $em->getRepository('App\Entity\Specialty')->find($specialty);
                        $actualSpecialties=$provider->getPrimarySpecialties();
                        $exist=false;
                        if($actualSpecialties!=null){
                            foreach ($actualSpecialties as $actualSp){
                                if($actualSp->getId()==$specialty){
                                    $exist=true;
                                }
                            }
                        }
                        if ($specialtyObj != null and $exist==false) {
                            $provider->addPrimarySpecialty($specialtyObj);
                        }
                    }
                }

                $em->persist($provider);
                $em->flush();
            }

            //set Degrees
            if ($page == 1) {
                $id = $request->get('id');
                $degreesSelected = $request->get('degreesSelected');
                $provider = $em->getRepository('App\Entity\Provider')->find($id);

                $sqlDelete = "DELETE FROM `provider_degree` WHERE `provider_degree`.`provider_id` = $id";
                $stmt = $db->prepare($sqlDelete);
                $stmt->execute();

                $degrees = substr($degreesSelected, 0, -1);
                $lbs = explode(",", $degrees);

                foreach ($lbs as $degree) {
                    if ($provider != null) {
                        $degreeObj = $em->getRepository('App\Entity\Degree')->find($degree);
                        if ($degreeObj != null) {
                            $provider->addDegree($degreeObj);
                        }
                    }
                }

                $em->persist($provider);
                $em->flush();
            }

            //upload files
            $uploadError = array();

            if ($page == 4) {
                $id = $request->get('save_status');
                $provider = $em->getRepository('App\Entity\Provider')->find($id);

                $provider_agreement_file = $request->files->get('provider_agreement_file');
                $credentialing_application_file = $request->files->get('credentialing_application_file');
                $state_licence_file = $request->files->get('state_licence_file');
                $provider_data_form_file = $request->files->get('provider_data_form_file');
                $w9_file = $request->files->get('w9_file');
                $general_liability_coverage_file = $request->files->get('general_liability_coverage_file');
                $loa_loi_file = $request->files->get('loa_loi_file');
                $fwa_coi_attestation_file=$request->files->get('fwa_coi_attestation_file');

                if ($provider_agreement_file != "") {
                    $file_provider_agreement = $fileUploader->upload($provider_agreement_file, $id, "provider_agreement_file");
                    if ($file_provider_agreement != "error") {
                        $provider->setProviderAgreementFile($file_provider_agreement);
                        $uploadError[0] = 0;
                    } else {
                        $uploadError[0] = 1;
                    }
                }

                if ($credentialing_application_file != "") {
                    $file = $fileUploader->upload($credentialing_application_file, $id, "credentialing_application_file");
                    if ($file != "error") {
                        $provider->setCredentialingApplicationFile($file);
                        $uploadError[1] = 0;
                    } else {
                        $uploadError[1] = 1;
                    }
                }
                if ($state_licence_file != "") {
                    $file = $fileUploader->upload($state_licence_file, $id, "state_licence_file");
                    if ($file != "error") {
                        $provider->setStateLicenseFile($file);
                        $uploadError[2] = 0;
                    } else {
                        $uploadError[2] = 1;
                    }
                }
                if ($provider_data_form_file != "") {
                    $file = $fileUploader->upload($provider_data_form_file, $id, "provider_data_form_file");
                    if ($file != "error") {
                        $provider->setProviderDataFormFile($file);
                        $uploadError[3] = 0;
                    } else {
                        $uploadError[3] = 1;
                    }
                }
                if ($w9_file != "") {
                    $file = $fileUploader->upload($w9_file, $id, "w9_file");
                    if ($file != "error") {
                        $provider->setW9File($file);
                        $uploadError[4] = 0;
                    } else {
                        $uploadError[4] = 1;
                    }
                }

                if ($general_liability_coverage_file != "") {
                    $file = $fileUploader->upload($general_liability_coverage_file, $id, "general_liability_coverage_file");
                    if ($file != "error") {
                        $provider->setGeneralLiabilityCoverageFile($file);
                        $uploadError[5] = 0;
                    } else {
                        $uploadError[5] = 1;
                    }
                }
                if ($loa_loi_file != "") {
                    $file = $fileUploader->upload($loa_loi_file, $id, "loa_loi_file");
                    if ($file != "error") {
                        $provider->setLoaLoiFile($file);
                        $uploadError[6] = 0;
                    } else {
                        $uploadError[6] = 1;
                    }
                }

                if ($fwa_coi_attestation_file != "") {
                    $file = $fileUploader->upload($fwa_coi_attestation_file, $id, "fwa_coi_attestation_file");
                    if ($file != "error") {
                        $provider->setFwaCoiAttestationFile($file);
                        $uploadError[7] = 0;
                    } else {
                        $uploadError[7] = 1;
                    }
                }

                $em->persist($provider);
                $em->flush();

                $id_provider=$provider->getId();

                $filesSaved=array();
                $allFileUploaded=true;
                $webPath = $this->getParameter('kernel.project_dir') . '/public/uploads/providers/'.$id_provider;

                if ($provider_agreement_file != "") {
                     $file_name=$provider->getProviderAgreementFile();
                     $file_name_a = explode(".", $file_name);
                     $extension = $file_name_a[1];
                     $file_name=$webPath."/provider_agreement_file-".$id_provider.".".$extension;

                    if (file_exists($file_name)) {
                        $filesSaved[0]=1;
                    } else {
                        $filesSaved[0]=0;
                        $allFileUploaded=false;
                    }
                }

                if ($credentialing_application_file != "") {
                    $file_name=$provider->getCredentialingApplicationFile();
                    $file_name_a = explode(".", $file_name);
                    $extension = $file_name_a[1];
                    $file_name=$webPath."/credentialing_application_file-".$id_provider.".".$extension;

                    if (file_exists($file_name)) {
                        $filesSaved[1]=1;
                    } else {
                        $filesSaved[1]=0;
                        $allFileUploaded=false;
                    }
                }

                if ($state_licence_file!= "") {
                    $file_name=$provider->getStateLicenseFile();
                    $file_name_a = explode(".", $file_name);
                    $extension = $file_name_a[1];
                    $file_name=$webPath."/state_licence_file-".$id_provider.".".$extension;

                    if (file_exists($file_name)) {
                        $filesSaved[2]=1;
                    } else {
                        $filesSaved[2]=0;
                        $allFileUploaded=false;
                    }
                }

                if ($provider_data_form_file != "") {
                    $file_name=$provider->getProviderDataFormFile();
                    $file_name_a = explode(".", $file_name);
                    $extension = $file_name_a[1];
                    $file_name=$webPath."/provider_data_form_file-".$id_provider.".".$extension;

                    if (file_exists($file_name)) {
                        $filesSaved[3]=1;
                    } else {
                        $filesSaved[3]=0;
                        $allFileUploaded=false;
                    }
                }

                if ($w9_file != "") {
                    $file_name=$provider->getW9File();
                    $file_name_a = explode(".", $file_name);
                    $extension = $file_name_a[1];
                    $file_name=$webPath."/w9_file-".$id_provider.".".$extension;

                    if (file_exists($file_name)) {
                        $filesSaved[4]=1;
                    } else {
                        $filesSaved[4]=0;
                        $allFileUploaded=false;
                    }
                }

                if ($general_liability_coverage_file != "") {
                    $file_name=$provider->getGeneralLiabilityCoverageFile();
                    $file_name_a = explode(".", $file_name);
                    $extension = $file_name_a[1];
                    $file_name=$webPath."/general_liability_coverage_file-".$id_provider.".".$extension;

                    if (file_exists($file_name)) {
                        $filesSaved[5]=1;
                    } else {
                        $filesSaved[5]=0;
                        $allFileUploaded=false;
                    }
                }

                if ($loa_loi_file != "") {
                    $file_name=$provider->getLoaLoiFile();
                    $file_name_a = explode(".", $file_name);
                    $extension = $file_name_a[1];
                    $file_name=$webPath."/loa_loi_file-".$id_provider.".".$extension;

                    if (file_exists($file_name)) {
                        $filesSaved[6]=1;
                    } else {
                        $filesSaved[6]=0;
                        $allFileUploaded=false;
                    }
                }

                if ($fwa_coi_attestation_file != "") {
                    $file_name=$provider->getFwaCoiAttestationFile();
                    $file_name_a = explode(".", $file_name);
                    $extension = $file_name_a[1];
                    $file_name=$webPath."/fwa_coi_attestation_file-".$id_provider.".".$extension;

                    if (file_exists($file_name)) {
                        $filesSaved[7]=1;
                    } else {
                        $filesSaved[7]=0;
                        $allFileUploaded=false;
                    }
                }

                return new Response(
                    json_encode(array('id' => $id,'test'=>1,'page' => $page,'uploadError'=>$uploadError,'filesSaved'=>$filesSaved,'allFileUploaded'=>$allFileUploaded,
                        'fwa_coi_attestation_file'=>$fwa_coi_attestation_file)), 200, array('Content-Type' => 'application/json')
                );
            }

            return new Response(
                json_encode(array('id' => $id, 'page' => $page,'test'=>1)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_provider",methods={"POST","DELETE"})
     */
    public function delete(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $provider =  $em->getRepository('App\Entity\Provider')->find($id);
            $removed = 0;
            $message = "";

            if ($provider) {
                try {
                    $em->remove($provider);
                    $em->flush();
                    $removed = 1;
                    $message = "The Provider has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The provider can't be removed";
                }
            }

            $this->SaveLog(3,'Provider',$id,'Delete provider');

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete-payer-assigned/{id}", name="admin_delete_payer_assigned",methods={"POST","DELETE"})
     */
    public function deletePayerAssignedAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $provider_id = $request->get('provider_id');
            $payer_id = $request->get('id');

            $provider = $em->getRepository('App\Entity\Provider')->find($provider_id);
            $payer = $em->getRepository('App\Entity\Payer')->find($payer_id);


            $removed = 0;
            $message = "";

            if ($provider!=null && $payer!=null) {
                try {

                    $provider->removePayer($payer);
                    $em->persist($provider);
                    $em->flush();

                    $removed = 1;
                    $message = "The Payer Assigned has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Payer Assigned can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_provider",methods={"POST","DELETE"})
     */
    public function deleteMultiple(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $provider = $provider = $em->getRepository('App\Entity\Provider')->find($id);

                if ($provider) {
                    try {
                        $em->remove($provider);
                        $em->flush();
                        $removed = 1;
                        $message = "The Provider has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Provider can't be removed";
                    }
                }
            }

            $this->SaveLog(1,'Provider',implode($ids),'Delete multiples providers');

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/checknpi", name="admin_check_npi",methods={"POST"})
     */
    public function checkNPI(Request $request) {
        $npi = $request->get('npi');

        $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=1&skip=&pretty=on&version=2.1";
        $json=file_get_contents($url_result);

        return new Response(
            json_encode(array('json_result' =>$json)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/get-api-data", name="admin_api_data")
     */
    public function getAPIData() {
        set_time_limit(88200);
        $em = $this->getDoctrine()->getManager();
        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));

        $current_data=$em->getRepository('App:PDataMedicare')->findAll();
        foreach ($current_data as $datum){
            $em->remove($datum);
        }
        $em->flush();

        foreach ($providers as $provider){
            $npi=$provider->getNpiNumber();
            if ($npi != "" and $npi != "NOT AVAILABLE" and $npi != "N/A") {
                $pData=$em->getRepository('App\Entity\PDataMedicare')->findOneBy(array('npi_number'=>$npi));
                if($pData==null) {
                    $url_result = "https://npiregistry.cms.hhs.gov/api/?number=$npi&enumeration_type=&taxonomy_description=&first_name=&use_first_name_alias=&last_name=&organization_name=&address_purpose=&city=&state=&postal_code=&country_code=&limit=200&skip=&pretty=on&version=2.1";
                    $json = file_get_contents($url_result);

                    $pData = new PDataMedicare();
                    $pData->setNpiNumber($npi);
                    $pData->setData($json);
                    $em->persist($pData);
                    $em->flush();
                }
            }
        }

        die();
    }


    private function providersByOrganization($organization) {
            $em = $this->getDoctrine()->getManager();
            $providers = $em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0,'organization'=>$organization));

            return $providers;
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }

    /**
     * @Route("/disabled_provider", name="admin_disabled_provider",methods={"POST"})
     */
    public function disabledProvider(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $providerId=$request->get('id');
        $termination_reasonId=$request->get('termination_reason');
        $date_of_disabled=$request->get('date_of_disabled');

        $provider=$em->getRepository('App\Entity\Provider')->find($providerId);

        $disabled=false;
        if($provider){
          $provider->setDisabled(1);
          $provider->setTerminationReason($em->getRepository('App\Entity\TerminationReason')->find($termination_reasonId));
          $provider->setDisabledDate($date_of_disabled);

          $em->persist($provider);
          $em->flush();

          $disabled=true;
        }

        $this->SaveLog(6,'Provider',$provider->getId(),'Disable provider');

        return new Response(
            json_encode(array('id' => 1, 'disabled'=>$disabled)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/active_address", name="admin_active_address_provider",methods={"POST"})
    */
    public function activeAddressProvider(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $id=$request->get('id');
        $active=$request->get('active');
        $provider_id=$request->get('provider_id');
        $provider=$em->getRepository('App\Entity\Provider')->find($provider_id);

        $address=$em->getRepository('App\Entity\BillingAddress')->find($id);

        //add a new Address
        if($active==1){

            if($address->getIsFacility()==true){
                $provider->setIsFacility(true);
            }
            $provider->addBillingAddress($address);
        }

        if($active==0){
            $provider->removeBillingAddress($address);
        }

        $em->persist($provider);
        $em->flush();

        return new Response(
            json_encode(array('id' => 1)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/load_address_ajax", name="admin_load_address_ajax",methods={"POST"})
    */
    public function loadAddressAjax(Request $request) {
        $em = $this->getDoctrine()->getManager();

        $id=$request->get('id');
        $address=$em->getRepository('App\Entity\BillingAddress')->find($id);
       // $address=new BillingAddress();

        $addressResult=array();

        $addressResult['id']=$address->getId();
        $addressResult['street']=$address->getStreet();
        $addressResult['bussinesshours']=$address->getBusinessHours();
        $addressResult['city']=$address->getCity();
        $addressResult['suite']=$address->getSuiteNumber();
        $addressResult['county']=$address->getCounty();
        $addressResult['region']=$address->getRegion();
        $addressResult['state']=$address->getUsState();
        $addressResult['zipcode']=$address->getZipCode();
        $addressResult['phone']=$address->getPhoneNumber();
        $addressResult['beds']=$address->getBeds();
        $addressResult['npi']=$address->getLocationNpi();
        $addressResult['npi2']=$address->getLocationNpi2();
        $addressResult['medicaid']=$address->getGroupMedicaid();
        $addressResult['medicare']=$address->getGroupMedicare();
        $addressResult['taxonomy']=$address->getTaxonomyCode();
        $addressResult['limit_min']=$address->getOfficeAgeLimit();
        $addressResult['limit_max']=$address->getOfficeAgeLimitMax();
        $addressResult['wheelchair']=$address->getWheelchair();
        $addressResult['tdd']=$address->getTdd();
        $addressResult['private_transport']=$address->getPrivateProviderTransportation();
        $addressResult['primary_addr']=$address->getPrimaryAddr();
        $addressResult['billing_addr']=$address->getBillingAddr();
        $addressResult['mailing_addr']=$address->getMailingAddr();
        $addressResult['is_facility']=$address->getIsFacility();

        return new Response(
            json_encode(array('address' => $addressResult)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/load_provider_added_ajax", name="admin_load_provider_add_ajax",methods={"POST"})
     */
    public function loadProvAddedAjax(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $ids=$request->get('id_providers_added');
        $ids = substr($ids, 0, -1);
        $idsArray=explode(",",$ids);

        $providersResult=array();
        $total=0;
        if (count($idsArray)>0){
            foreach ($idsArray as $id){
                $provider=$em->getRepository('App\Entity\Provider')->find($id);
                $providerObj=array();
                if($provider!=null){
                    $providerObj['id']=$provider->getId();
                    $providerObj['npi']=$provider->getNpiNumber();
                    $providerObj['name']=$provider->getFirstName()." ".$provider->getLastName();
                    $providersResult[]=$providerObj;
                }
            }
        }

        return new Response(
            json_encode(array('providers' => $providersResult,'total'=>$total)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/add_provider_addr_ajax", name="add_provider_addr_ajax",methods={"POST"})
     */
    public function addProviderAddrAjax(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $addrId=$request->get('addrId');
        $providerId=$request->get('providerId');

        $provider=$em->getRepository('App\Entity\Provider')->find($providerId);
        $address=$em->getRepository('App\Entity\BillingAddress')->find($addrId);

        if($provider!=null){
            if($address!=null){

                //search if the provider have it address
                $current_address=$provider->getBillingAddress();
                $exist=0;
                if($current_address!=null){
                    foreach ($current_address as $ca){
                        if($ca->getId()==$addrId){
                            $provider->removeBillingAddress($ca);
                            $em->persist($provider);
                            $em->flush();
                        }
                    }
                }

                $provider->addBillingAddress($address);
                $em->persist($provider);
                $em->flush();
            }
        }
        return new Response(
            json_encode(array('addrId' =>$addrId,'providerId'=>$providerId)), 200, array('Content-Type' => 'application/json')
        );
    }

    private function SaveLog($action_id,$entity,$entity_id,$note){
        $em=$this->getDoctrine()->getManager();
        $user=$this->getUser();

        $log=new ChangeLog();
        $log->setUser($user);
        $action=$em->getRepository('App\Entity\ActionLog')->find($action_id);
        $log->setAction($action);
        $log->setEntityId($entity_id);
        $log->setEntityClass($entity);
        $log->setNote($note);

        $em->persist($log);
        $em->flush();
    }

    /**
     * @Route("/modified_medicaid", name="admin_modified_medicaid")
     */
    public function modifiedMedicaid(){
        $em=$this->getDoctrine()->getManager();

        $allProviders=$em->getRepository('App\Entity\Provider')->findAll();

        foreach ($allProviders as $provider){
            $medicaid=$provider->getMedicaid();
            $newMedicaid="";
            if($medicaid!=null or $medicaid!=""){
                $lengMedicaid=strlen($medicaid);
                if($lengMedicaid<9){
                    $zeroToadd=9-$lengMedicaid;
                    while ($zeroToadd>0){
                        $newMedicaid.="0";
                        $zeroToadd--;
                    }
                    $newMedicaid.=$medicaid;
                    echo $provider->getId().":".$medicaid." | New ".$newMedicaid." <br/>";
                }else{
                    $newMedicaid=$medicaid;
                }
                $provider->setMedicaid($newMedicaid);
                $em->persist($provider);
                $em->flush();
            }
        }

        return new Response('All Medicaids Changed');
    }


    /**
     * @Route("/modified_group_medicaid", name="admin_modified_group_medicaid")
    */
    public function modifiedGroupMedicaid(){
        $em=$this->getDoctrine()->getManager();

        $allAddress=$em->getRepository('App\Entity\BillingAddress')->findAll();
        foreach ($allAddress as $address){
            $medicaid=$address->getGroupMedicaid();
            $newMedicaid="";
            if($medicaid!=null or $medicaid!=""){
                $lengMedicaid=strlen($medicaid);
                if($lengMedicaid<9){
                    $zeroToadd=9-$lengMedicaid;
                    while ($zeroToadd>0){
                        $newMedicaid.="0";
                        $zeroToadd--;
                    }
                    $newMedicaid.=$medicaid;
                    echo $address->getId().":".$medicaid." | New ".$newMedicaid." <br/>";
                }else{
                    $newMedicaid=$medicaid;
                }
                $address->setGroupMedicaid($newMedicaid);
                $em->persist($address);
                $em->flush();
            }
        }

        return new Response('All Group Medicaids Changed');
    }

    /**
     * @Route("/set_taxonomy_code", name="admin_set_taxonomy")
     */
    public function setTaxonomyCode(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();

        $providers=$em->getRepository('App\Entity\Provider')->findAll();
        foreach ($providers as $provider){
           $taxonomy=$provider->getTaxonomyCode();

           if($taxonomy!="" and  $taxonomy!="-" and  $taxonomy!="N/A" and  $taxonomy!="0" and $taxonomy!="00000000" and $taxonomy!="n/a" and $taxonomy!="NOT AVAILABLE"){
               $taxonomyObj=$em->getRepository('App\Entity\TaxonomyCode')->findOneBy(array('code'=>$taxonomy));
               if($taxonomyObj!=null){
                  $provider->addTaxonomyCode($taxonomyObj);

                  $em->persist($provider);
                  $em->flush();
               }
           }

        }

        return new Response('All Group Medicaids Changed');
    }

    /**
     * @Route("/proccess-api-npi", name="admin_api_npi")
     */
    public function testAPI(Request $request){

        $npi=$request->get('npi');
        $first_name=$request->get('first_name');
        $last_name=$request->get('last_name');
        $organization_name=$request->get('organization_name');

        $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&enumeration_type=&taxonomy_description=&first_name=".$first_name."&use_first_name_alias=&last_name=$last_name&organization_name=$organization_name&address_purpose=&city=&state=&postal_code=&country_code=&limit=200&skip=&pretty=on&version=2.1";
        $json=file_get_contents($url_result);

        return new Response(
            json_encode(array('json_result' =>$json)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/search-npi", name="admin_search_api")
     */
    public function searchAPI(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {

            return $this->render('provider/search_npi.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/setFacility", name="admin_set_facility")
     */
    public function setFacility(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();
        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));

        foreach ($providers as $provider){
            $is_facility=false;
            if($provider!=null){
                $organization=$provider->getOrganization();
                $contFacility=0;

                if($organization!=null){
                    $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                    if(count($address)>0){
                        foreach ($address as $addr){
                            if($addr->getIsFacility()==true){
                                $contFacility++;
                            }
                        }
                    }

                    if($contFacility>0){
                        $is_facility=true;
                    }

                    $provider->setIsFacility($is_facility);
                    $em->persist($provider);
                }

            }
        }
        $em->flush();

        return new Response('Process Finish');
    }

    public function setFacilityProvider($id){
        $em=$this->getDoctrine()->getManager();
        $is_facility=false;
        $provider=$em->getRepository('App\Entity\Provider')->find($id);

            if($provider!=null){
                $organization=$provider->getOrganization();
                $organization=new Organization();
                $organization_clasification=$organization->getProviderType();


                $contFacility=0;
                $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                if(count($address)>0){
                    foreach ($address as $addr){
                        if($addr->getIsFacility()==true) {
                            $contFacility++;
                        }
                    }
                }

                if($contFacility>0){
                    $is_facility=true;
                }

                $provider->setIsFacility($is_facility);
                $em->persist($provider);
                $em->flush();
            }
    }


    /**
     * @Route("/proccess-update-payers", name="admin_update_payers_providers")
     */
    public function updatePayers(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();

        $facilityCredentialings=$em->getRepository('App\Entity\BillingAddressCvo')->findAll();

        foreach ($facilityCredentialings as $facilityCredentialing){
            $payers=$facilityCredentialing->getPayers();
            if($payers->getPayers()!=null){

                $organization=$facilityCredentialing->getBillingAddress()->getOrganization();
                $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0,$organization=>$organization->getId()));

                if($providers!=null){
                    foreach ($providers as $provider){
                        foreach ($payers as $payer){
                            $currentPayers=$provider->getPayers();
                            $applyPayer=true;
                            if($currentPayers!=null){
                                foreach ($currentPayers as $currentPayer){
                                    if($currentPayer->getId()==$payer->getId()){
                                        $applyPayer=false;
                                    }
                                }
                            }

                            if($applyPayer==true){
                                $provider->addPayer($payer);
                                $em->persist($provider);
                                $em->flush();
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @Route("/add_payer_provider", name="admin_add_provider_payer",methods={"POST"})
     */
    public function addPayer(Request $request){
        $em=$this->getDoctrine()->getManager();

        $ids = $request->get('ids');
        $payer=$request->get('payer');

        $payerObj=$em->getRepository('App\Entity\Payer')->find($payer);
        $contP=0;
        foreach ($ids as $id) {
            $provider = $em->getRepository('App\Entity\Provider')->find($id);

            if($provider!=null){
                //check if the provider have the payer selected
                $flag=0;
                $existPayer=false;
                $payers=$provider->getPayers();
                $isFacility=false;
                //check ig the provider is credentialing approved
                $credentialing=$em->getRepository('App\Entity\ProviderCredentialing')->findOneBy(array('provider'=>$id,'credentialing_status'=>4));

                $organization=$provider->getOrganization();
                $addresses=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));

                foreach ($addresses as $addr1){
                    if($addr1->getIsFacility()==true){
                        $isFacility=true;
                    }
                }

                if($credentialing!=null and $isFacility==false){
                    if($payers!=null){
                        foreach ($payers as $p){
                            if($p!=null){
                                if($p->getId()==$payer){
                                    $existPayer=true;
                                }
                            }
                        }
                    }
                    if($existPayer==false){
                        //MMM of florida
                        if($payer==1){
                            $medicare=$provider->getMedicare();
                            if($medicare!="" and $medicare!="-" and $medicare!="In progress" and $medicare!="In-progress"
                                and $medicare!="N/A" and $medicare!="PENDING" and $medicare!="Pending" and $medicare!="pending" and $medicare!="blank") {
                                $address=$provider->getBillingAddress();
                                if($address!=null){
                                    foreach ($address as $addr){
                                        if($addr->getRegion()==9 or $addr->getRegion()==10 or $addr->getRegion()==11){
                                            $flag=1;
                                        }
                                    }
                                }
                            }
                        }
                        if($payer==4){
                            $flag=1;
                        }
                    }
                }
            }
            if($flag==1){
                $contP++;
                $provider->addPayer($payerObj);
                $em->persist($provider);
                $em->flush();
            }
        }

        return new Response(
            json_encode(array('providers' =>$contP)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/exclude_payer_provider", name="admin_exclude_provider_payer",methods={"POST"})
    */
    public function excludePayer(Request $request){
        $em=$this->getDoctrine()->getManager();

        $ids=$request->get('ids');
        $payer_id=$request->get('payer');
        $date=$request->get('date');
        $dateArray=explode('/',$date);

        $newDate=$dateArray[2]."-".$dateArray[0]."-".$dateArray[1];

        $payerObj=$em->getRepository('App\Entity\Payer')->find($payer_id);
        foreach ($ids as $id) {
            $provider = $em->getRepository('App:Provider')->find($id);
            if($provider!=null){
                $provider_payer_excluded=$em->getRepository('App:ProviderPayerExcluded')->findOneBy(array('provider'=>$id,'payer'=>$payer_id));

                if($provider_payer_excluded==null){
                    $excluded=new ProviderPayerExcluded();
                    $excluded->setCreateAt(new \DateTime($newDate));
                    $excluded->setProvider($provider);
                    $excluded->setPayer($payerObj);

                    $em->persist($excluded);
                }
            }
        }

        $em->flush();

        return new Response(
            json_encode(array('id' => 1)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/delete_payer_provider", name="admin_delete_provider_payer",methods={"POST"})
     */
    public function deletePayerProvider(Request $request){
        $em=$this->getDoctrine()->getManager();

        $ids=$request->get('ids');
        $payer_id=$request->get('payer');

        $payerObj=$em->getRepository('App\Entity\Payer')->find($payer_id);
        foreach ($ids as $id) {
            $provider = $em->getRepository('App\Entity\Provider')->find($id);
            if($provider!=null){
                $payers=$provider->getPayers();
                if($payers!=null){
                    foreach ($payers as $payer){
                        if($payer->getId()==$payer_id){
                           $provider->removePayer($payerObj);
                        }
                    }
                }
            }
        }
        $em->flush();

        return new Response(
            json_encode(array('id' => 1)), 200, array('Content-Type' => 'application/json')
        );
    }


    /**
     * @Route("/updateinfo_nppes", name="admin_provider_update_info_NPPES")
     */
    public function updateInfoFromNPPES(){
        set_time_limit(88200);
        $em = $this->getDoctrine()->getManager();

        $providers=$em->getRepository('App\Entity\Provider')->findAll();
        $allTaxonomies=$em->getRepository('App\Entity\TaxonomyCode')->findAll();
        $taxonomiesArray=array();
        foreach ($allTaxonomies as $allTaxonomy){
            $taxonomiesArray[]=$allTaxonomy->getCode();

        }
        foreach ($providers as $provider){
                $npi=$provider->getNpiNumber();
                $taxonomies=$provider->getTaxonomyCodes();

                if($npi!="" and $npi!="NOT AVAILABLE" and $npi!="N/A"){
                    $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=200&skip=&pretty=on&version=2.1";
                    $json=file_get_contents($url_result);
                    $data=json_decode($json,true);

                    if($data['result_count']>0){
                        $type=$data['results'][0]['enumeration_type'];
                        if($type=='NPI-1'){
                            $taxonomiesNPPES=$data['results'][0]['taxonomies'];
                            $gender=$data['results'][0]['basic']['gender'];

                            $newgender="";
                            if($gender=="F"){
                                $newgender="Fenale";
                            }
                            if($gender=="M"){
                                $newgender="Male";
                            }
                            $provider->setGender($newgender);
                            $em->persist($provider);
                            $em->flush();
                            
                            foreach ($taxonomiesNPPES as $taxonomyNPPE){
                                $code=$taxonomyNPPE['code'];
                                $spe=$taxonomyNPPE['desc'];

                                $licence=$provider->getStateLic();
                                if($taxonomyNPPE['primary']==true){
                                    if($taxonomyNPPE['license']!=""){
                                        $licence=  $taxonomyNPPE['license'];
                                    }
                                }

                                $provider->setStateLic($licence);

                                $em->persist($provider);
                                $em->flush();

                                if(in_array($code,$taxonomiesArray)==false){
                                    $newCode=new TaxonomyCode();

                                    $newCode->setCode($code);
                                    $newCode->setSpecialization($spe);
                                    $em->persist($newCode);
                                    $em->flush();
                                }
                                $ready=0;
                                if($taxonomies!=null){
                                    foreach ($taxonomies as $taxonomy){
                                        if($taxonomy->getCode()==$code){
                                            $ready++;
                                        }
                                    }
                                    if($ready==0){
                                        $taxObj=$em->getRepository('App\Entity\TaxonomyCode')->findOneBy(array('code'=>$code));
                                        if($taxObj!=null){
                                            $currentTaxonomies=$provider->getTaxonomyCodes();
                                            $cont_current_tax=0;
                                            if($currentTaxonomies!=null){
                                                foreach ($currentTaxonomies as $currentTaxonomy){
                                                    if($currentTaxonomy->getCode()==$taxObj->getCode()){
                                                        $cont_current_tax++;
                                                    }
                                                }
                                            }
                                            if($cont_current_tax==0){
                                                $provider->addTaxonomyCode($taxObj);
                                                $em->persist($provider);
                                                $em->flush();
                                            }
                                        }
                                    }
                                }else{
                                    $taxObj=$em->getRepository('App\Entity\TaxonomyCode')->findOneBy(array('code'=>$code));
                                    if($taxObj!=null){
                                        $provider->addTaxonomyCode($taxObj);
                                        $em->persist($provider);
                                        $em->flush();
                                    }
                                }

                            }
                        }
                    }
                }
        }

        return new Response('All OK');
    }

    /**
     * @Route("/update_facility_providers", name="admin_providers_update_facility")
     */
    public function updateFacilityProviders(){
        set_time_limit(88200);
        $em = $this->getDoctrine()->getManager();

        $providers=$em->getRepository('App\Entity\Provider')->findAll();

        foreach ($providers as $provider){
           $organization=$provider->getOrganization();
           if($organization!=null){
               $contF=0;
               $addresses=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));

               if($addresses!=null){
                   foreach ($addresses as $address){
                       if($address->getIsFacility()==true){
                           $contF++;
                       }
                   }
                   if($contF>0){
                       $provider->setIsFacility(true);
                   }
                   if($contF==0){
                       $provider->setIsFacility(false);
                   }

                   $em->persist($provider);

               }
           }
        }
        $em->flush();

        return new Response('All OK');
    }

    /**
     * @Route("/update_caqh_providers", name="admin_providers_update_caqh")
     */
    public function updateCAQHProviders(){
        set_time_limit(88200);
        $em = $this->getDoctrine()->getManager();

        $providers=$em->getRepository('App\Entity\Provider')->findAll();

        foreach ($providers as $provider){
            $caqh=$provider->getCaqh();
            if($caqh=='n/a' or $caqh=='not available' or $caqh=='NOT AVAILABLE' or $caqh=='N/A'){
               $provider->setCaqh('Not Available');
               $provider->setNaCaqh(true);
               $em->persist($provider);
            }

            if($caqh=='pending' or $caqh=='PENDING'){
                $provider->setCaqh('Pending');
                $provider->setNaCaqh(true);
                $em->persist($provider);
            }
        }

        foreach ($providers as $provider){
            $caqh=$provider->getCaqh();
            if($caqh=='Not Available'){
                $provider->setNaCaqh(true);
                $em->persist($provider);
            }

            if($caqh=='Pending'){
                $provider->setPeddingCaqh(true);
                $provider->setNaCaqh(false);
                $em->persist($provider);
            }
        }

        $em->flush();
        return new Response('All OK');
    }

    /**
     * @Route("/get-locations-fcc", name="admin_provider_locations_fcc")
     */
    public function locationsFcc(){
        $em=$this->getDoctrine()->getManager();
        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));

        foreach ($providers as $pro){
            $avalidable="No";

            $organization=$pro->getOrganization();
            $isFacility=false;
            $providersOthers=$organization->getProviders();
            $address=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));

            foreach ($address as $addr1){
                if($addr1->getIsFacility()==true){
                    $isFacility=true;
                }
            }

            if($pro->getMedicaid()!="" and $pro->getMedicaid()!="-" and $pro->getMedicaid()!="In progress" and $pro->getMedicaid()!="In-progress"
                and $pro->getMedicaid()!="N/A" and $pro->getMedicaid()!="PENDING" and $pro->getMedicaid()!="Pending" and $pro->getMedicaid()!="pending" and $pro->getMedicaid()!="blank"){
                $avalidable="Yes";
            }else{
                if($isFacility==true){
                    if($providersOthers!=null) {
                        foreach ($providersOthers as $proOther) {
                            $medi=$proOther->getMedicaid();
                            if($medi!="" and $medi!="-" and $medi!="In progress" and $medi!="In-progress"
                                and $medi!="N/A" and $medi!="PENDING" and $medi!="Pending" and $medi!="pending" and $medi!="blank") {
                                $avalidable="Yes";
                            }
                        }
                    }
                    if($address!=null){
                        foreach ($address as $addr) {
                            $medi=$addr->getGroupMedicaid();
                            if($medi!="" and $medi!="-" and $medi!="In progress" and $medi!="In-progress"
                                and $medi!="N/A" and $medi!="PENDING" and $medi!="Pending" and $medi!="pending" and $medi!="blank") {
                                $avalidable="Yes";
                            }
                        }
                    }
                }
            }

            if($avalidable=="Yes"){
                $providersAvailableMedicaid[]=$pro;
            }
        }

        $addressId=[];
        $addressResul=[];
      foreach ($providersAvailableMedicaid as $provider){
          $address=$provider->getBillingAddress();
          if($address!=null){
              foreach ($address as $addr){
                  if(!in_array($addr->getId(),$addressId)){
                      $addressId[]=$addr->getId();
                      $addressResul[]=$addr;
                  }
              }
          }
      }

      echo "Totals:".count($addressId)."<br/>";
       foreach ($addressResul as $addre){
         echo $addre->getId(). "<br/>";
       }
       die();
    }

    /**
     * @Route("/duplicate-providers", name="admin_providers_duplicate",methods={"POST","GET"})
    */
    public function duplicateProviders(Request  $request){

        $em=$this->getDoctrine()->getManager();
        $org_id=$request->get('organization');
        $ids=$request->get('ids');
        $organization=$em->getRepository('App\Entity\Organization')->find($org_id);

        foreach ($ids as $id){
            $provider=$em->getRepository('App\Entity\Provider')->find($id);
            if($provider!=null){

                //1- get all data from the provider
                $first_name=$provider->getFirstName();
                $last_name=$provider->getLastName();
                $initial=$provider->getInitial();
                $date_of_birth=$provider->getDateOfBirth();
                $gender=$provider->getGender();
                $social=$provider->getSocial();
                $state_lic=$provider->getStateLic();
                $lic_state=$provider->getLicState();
                $lic_issue_date=$provider->getLicIssueDate();
                $sanction_date=$provider->getSanctionDate();
                $email_send_date=$provider->getEmailSendDate();
                $email=$provider->getEmail();
                $caqh=$provider->getCaqh();
                $disabled=$provider->getDisabled();
                $board_certified=$provider->getBoardCertified();
                $board_name=$provider->getBoardName();
                $medicaid=$provider->getMedicaid();
                $medicare=$provider->getMedicare();
                $taxonomy_code=$provider->getTaxonomyCode();
                $npi_number=$provider->getNpiNumber();
                $accepting_new_patients=$provider->getAcceptingNewPatients();
                $interested_in_telemedicine=$provider->getInterestedInTelemedicine();
                $interested_in_residential_services=$provider->getInterestedInResidentialServices();
                $has_pcp=$provider->getHasPcp();
                $gender_acceptance=$provider->getGenderAcceptance();
                $notes=$provider->getNotes();
                $provider_agreement_file=$provider->getProviderAgreementFile();
                $credentialing_application_file=$provider->getCredentialingApplicationFile();
                $state_license_file=$provider->getStateLicenseFile();
                $provider_data_form_file=$provider->getProviderDataFormFile();
                $w9_file=$provider->getW9File();
                $general_liability_coverage_file=$provider->getGeneralLiabilityCoverageFile();
                $loa_loi_file=$provider->getLoaLoiFile();
                $disabled_date=$provider->getDisabledDate();
                $board_certified_specialty=$provider->getBoardCertifiedSpecialty();
                $hospital_npi=$provider->getHospitalNpi();
                $sent_credsimple=$provider->getSentCredsimple();
                $option_out_medicare=$provider->getOptionOutMedicare();
                $has_telemedicine=$provider->getHasTelemedicine();
                $telemedicine_company=$provider->getTelemedicineCompany();
                $fwa_coi_attestation_file=$provider->getFwaCoiAttestationFile();
                $created_at=$provider->getCreatedAt();
                $updated_at=$provider->getUpdatedAt();
                $board_certified_date=$provider->getBoardCertifiedDate();
                $hopital_privileges=$provider->getHopitalPrivileges();
                $hopital_privileges_type=$provider->getHopitalPrivilegesType();
                $ahca_id=$provider->getAhcaId();
                $is_facility=$provider->getIsFacility();
                $lic_expires_date=$provider->getLicExpiresDate();
                $board_certification_expires_date=$provider->getBoardCertificationExpiresDate();
                $has_residential_services=$provider->getHasResidentialServices();
                $hospital_acha_number=$provider->getHospitalAchaNumber();
                $na_caqh=$provider->getNaCaqh();
                $pedding_caqh=$provider->getPeddingCaqh();
                $is_hospital_affiliations=$provider->getIsHospitalAffiliations();

                $newProvider=new Provider();
                $newProvider->setOrganization($organization);
                $newProvider->setFirstName($first_name);
                $newProvider->setLastName($last_name);
                $newProvider->setInitial($initial);
                $newProvider->setDateOfBirth($date_of_birth);
                $newProvider->setGender($gender);
                $newProvider->setSocial($social);
                $newProvider->setStateLic($state_lic);
                $newProvider->setLicState($lic_state);
                $newProvider->setLicIssueDate($lic_issue_date);
                $newProvider->setSanctionDate($sanction_date);
                $newProvider->setEmailSendDate($email_send_date);
                $newProvider->setEmail($email);
                $newProvider->setCaqh($caqh);
                $newProvider->setDisabled($disabled);
                $newProvider->setBoardCertified($board_certified);
                $newProvider->setBoardName($board_name);
                $newProvider->setMedicaid($medicaid);
                $newProvider->setMedicare($medicare);
                $newProvider->setTaxonomyCode($taxonomy_code);
                $newProvider->setNpiNumber($npi_number);
                $newProvider->setAcceptingNewPatients($accepting_new_patients);
                $newProvider->setInterestedInTelemedicine($interested_in_telemedicine);
                $newProvider->setInterestedInResidentialServices($interested_in_residential_services);
                $newProvider->setHasPcp($has_pcp);
                $newProvider->setGenderAcceptance($gender_acceptance);
                $newProvider->setNotes($notes);
                $newProvider->setProviderAgreementFile($provider_agreement_file);
                $newProvider->setCredentialingApplicationFile($credentialing_application_file);
                $newProvider->setStateLicenseFile($state_license_file);
                $newProvider->setProviderDataFormFile($provider_data_form_file);
                $newProvider->setW9File($w9_file);
                $newProvider->setGeneralLiabilityCoverageFile($general_liability_coverage_file);
                $newProvider->setLoaLoiFile($loa_loi_file);
                $newProvider->setDisabledDate($disabled_date);
                $newProvider->setBoardCertifiedSpecialty($board_certified_specialty);
                $newProvider->setHospitalNpi($hospital_npi);
                $newProvider->setSentCredsimple($sent_credsimple);
                $newProvider->setOptionOutMedicare($option_out_medicare);
                $newProvider->setHasTelemedicine($has_telemedicine);
                $newProvider->setTelemedicineCompany($telemedicine_company);
                $newProvider->setFwaCoiAttestationFile($fwa_coi_attestation_file);
                $newProvider->setCreatedAt($created_at);
                $newProvider->setUpdatedAt($updated_at);
                $newProvider->setBoardCertifiedDate($board_certified_date);
                $newProvider->setHopitalPrivileges($hopital_privileges);
                $newProvider->setHopitalPrivilegesType($hopital_privileges_type);
                $newProvider->setAhcaId($ahca_id);
                $newProvider->setIsFacility($is_facility);
                $newProvider->setLicExpiresDate($lic_expires_date);
                $newProvider->setBoardCertificationExpiresDate($board_certification_expires_date);
                $newProvider->setHasResidentialServices($has_residential_services);
                $newProvider->setHospitalAchaNumber($hospital_acha_number);
                $newProvider->setNaCaqh($na_caqh);
                $newProvider->setPeddingCaqh($pedding_caqh);
                $newProvider->setIsHospitalAffiliations($is_hospital_affiliations);

                $degrees=$provider->getDegree();
                if($degrees!=null){
                    foreach ($degrees as $degree){
                        $newProvider->addDegree($degree);
                    }
                }


                $eap_onlys=$provider->getEapOnlys();
                if($eap_onlys!=null){
                    foreach ($eap_onlys as $eap_only){
                        $newProvider->addEapOnly($eap_only);
                    }
                }

                $general_categories=$provider->getGeneralCategories();
                if($general_categories!=null){
                    foreach ($general_categories as $general_category){
                        $newProvider->addGeneralCategory($general_category);

                    }
                }

                $languages=$provider->getLanguages();
                if($languages!=null){
                    foreach ($languages as $language){
                        $newProvider->addLanguage($language);
                    }
                }

                $lineofbusiness=$provider->getLineOfBusiness();
                if($lineofbusiness!=null){
                    foreach ($lineofbusiness as $lineofbusine){
                        $newProvider->addLineOfBusines($lineofbusine);
                    }
                }

                $payers=$provider->getPayers();
                if($payers!=null){
                    foreach ($payers as $payer){
                        $newProvider->addPayer($payer);
                    }
                }

                $primary_specialties=$provider->getPrimarySpecialties();
                if($primary_specialties!=null){
                    foreach ($primary_specialties as $primary_specialty){
                        $newProvider->addPrimarySpecialty($primary_specialty);
                    }
                }

                $provider_types=$provider->getProviderType();
                if($provider_types!=null){
                    foreach ($provider_types as $provider_type){
                        $newProvider->addProviderType($provider_type);
                    }

                }

                $taxonomy_codes=$provider->getTaxonomyCodes();
                if($taxonomy_codes!=null){
                    foreach ($taxonomy_codes as $taxonomy_code){
                        $newProvider->addTaxonomyCode($taxonomy_code);
                    }
                }
            }
            $em->persist($newProvider);
            $em->flush();

            $credentialing=$em->getRepository('App\Entity\ProviderCredentialing')->findOneBy(array('npi_number'=>$npi_number));
            if($credentialing!=null){

                $cvo_id=$credentialing->getCvo();
                $credentialing_status_id=$credentialing->getCredentialingStatus();
                $credentialing_date=$credentialing->getCredentialingDate();
                $credentialing_effective_date=$credentialing->getCredentialingEffectiveDate();
                $sanction_date=$credentialing->getSanctionDate();
                $credentialing_accepted=$credentialing->getCredentialingAccepted();
                $credentialing_denied=$credentialing->getCredentialingDenied();
                $credentialing_appeal=$credentialing->getCredentialingAppeal();
                $credentialing_complete_date=$credentialing->getCredentialingCompleteDate();
                $created_at=$credentialing->getCreatedAt();
                $updated_at=$credentialing->getUpdatedAt();
                $npi_number=$credentialing->getNpiNumber();
                $organization_id=$credentialing->getOrganization();
                $report_pdf=$credentialing->getReportPdf();

                $newCredentialing=new ProviderCredentialing();
                $newCredentialing->setCvo($cvo_id);
                $newCredentialing->setProvider($newProvider);
                $newCredentialing->setCredentialingStatus($credentialing_status_id);
                $newCredentialing->setCredentialingDate($credentialing_date);
                $newCredentialing->setCredentialingEffectiveDate($credentialing_effective_date);
                $newCredentialing->setSanctionDate($sanction_date);
                $newCredentialing->setCredentialingAccepted($credentialing_accepted);
                $newCredentialing->setCredentialingDenied($credentialing_denied);
                $newCredentialing->setCredentialingAppeal($credentialing_appeal);
                $newCredentialing->setCredentialingCompleteDate($credentialing_complete_date);
                $newCredentialing->setCreatedAt($created_at);
                $newCredentialing->setUpdatedAt($updated_at);
                $newCredentialing->setNpiNumber($npi_number);
                $newCredentialing->setOrganization($organization_id);
                $newCredentialing->setReportPdf($report_pdf);

                $em->persist($newCredentialing);
                $em->flush();
            }
        }

        return new Response(
            json_encode(array('ok' =>1)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/not-address-providers", name="admin_providers_not-address",methods={"POST","GET"})
     */
    public function notAddressProviders(Request  $request){
        $em=$this->getDoctrine()->getManager();

        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));

        foreach ($providers as $provider){
            $address=$provider->getBillingAddress();

            if(count($address)==0){
                echo $provider->getNpiNumber()."<br/>";
            }
        }

        die();
    }

    /**
     * @Route("/report-specialtyBycounty/{county}", name="admin_providers_specialty_county",methods={"POST","GET"})
     */
    public function reportSpecialtyByCounty(Request  $request,$county){
        $em=$this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $sql="SELECT p.id
            FROM  provider p 
            LEFT JOIN provider_billing_address ON provider_billing_address.provider_id = p.id
            LEFT JOIN billing_address ON billing_address.id = provider_billing_address.billing_address_id
            LEFT JOIN provider_primary_specialties ON provider_primary_specialties.provider_id = p.id
            LEFT JOIN specialty ON specialty.id = provider_primary_specialties.primary_specialty_id
            WHERE p.disabled=0 and billing_address.county='$county' and specialty.code IN ('044','066','076','096','907','929','930','931','932','124','067')
            GROUP BY p.id
            ORDER BY p.first_name";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $providers= $stmt->fetchAll();

        echo count($providers);

        die();
    }


    /**
     * @Route("/checkNPI", name="admin_providers_check_npi",methods={"POST","GET"})
     */
    public function checkNPI_NPPES(){
        $em=$this->getDoctrine()->getManager();
        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));


        foreach ($providers as $provider){
            $npi=$provider->getNpiNumber();
            if($npi!="" and $npi!="NOT AVAILABLE" and $npi!="N/A"){
                $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=200&skip=&pretty=on&version=2.1";
                $json=file_get_contents($url_result,false);

                $data=json_decode($json,true);

                if($data['result_count']==0){
                   echo "BAD ".$npi."<br/>";
                }else{
                    echo "200 OK ".$npi."<br/>";
                }
            }

        }


        die();
    }

    /**
     * @Route("/setCapitalize", name="admin_set_capitalize",methods={"POST","GET"})
     */
    public function setCapitalize(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();
        $providers=$em->getRepository('App\Entity\Provider')->findAll();

        foreach ($providers as $provider){
            $name=strtolower($provider->getFirstName());
            $last=strtolower($provider->getLastName());

            $name=ucwords ( $name );
            $last=ucwords ( $last );

            $provider->setFirstName($name);
            $provider->setLastName($last);
            $em->persist($provider);
        }
        $em->flush();
        die();
    }

    /**
     * @Route("/fix-gender", name="admin_provider_fix_gender")
     */
    public function fixGender() {
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();
        $providers=$em->getRepository('App\Entity\Provider')->findAll();
        foreach ($providers as $provider){
            $npi=$provider->getNpiNumber();
            $current_gender=$provider->getGender();

            if($current_gender=="" or $current_gender==null){
                if($npi!="" and $npi!="N/A" and $npi!="NOT AVAILABLE"){
                    $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=200&skip=&pretty=on&version=2.1";
                    $json=file_get_contents($url_result,false);
                    $data=json_decode($json,true);

                    if(count($data)>1 and $data['result_count']>0){
                        $gender=$data['results'][0]['basic']['gender'];

                        if($gender=="F"){
                            $gender="Female";
                        }
                        if($gender=="M"){
                            $gender="Male";
                        }

                        $provider->setGender($gender);
                        $em->persist($provider);
                    }
                }
            }
        }

        $em->flush();

       return new Response('All Genders Fixed');
    }

    /**
     * @Route("/report-providers-eligible-medicare", name="report_providers_eligible_medicare")
     *
     */
    public function providersEligibleMedicare(){
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');

        $spreadsheet = $reader->load($url.'providers_eligibles_medicare.xlsx');
        $spreadsheet->setActiveSheetIndex(0);

        $organizations=$em->getRepository('App:Organization')->findBy(array ('disabled'=>0));
        $providersNPI=[];
        $cont=2;

        foreach ($organizations as $organization){
            $org_ready_medicare=false;
            $organization_medicare="";
            $address=$em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization->getId()));

            if($address!=null){
                foreach ($address as $addr){
                    $group_medicare=$addr->getGroupMedicare();
                    if($group_medicare!="" and  $group_medicare!="-" and $group_medicare!="N/A" and $group_medicare!="Pending"){
                        $org_ready_medicare=true;
                        $organization_medicare=$group_medicare;
                        break;
                    }
                }
            }

            if($org_ready_medicare==false){
                $providers=$em->getRepository('App:Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));
                if($providers!=null){
                    foreach ($providers as $provider){
                        $medicare=$provider->getMedicare();

                        if($medicare!="" and $medicare!="-" and $medicare!="0" and $medicare!="N/A" and $medicare!="None" and $medicare!="Pending" and $medicare!="in process"
                            and $medicare!="NA" and $medicare!="in proceses" and $medicare!="55525 - opted out" and $medicare!="submitting application" and $medicare!="In-process"){
                            $org_ready_medicare=true;
                            $organization_medicare=$medicare;
                            break;
                        }
                    }
                }
            }

            if($org_ready_medicare==true){
                $providers_reault=$em->getRepository('App:Provider')->findBy(array('organization'=>$organization->getId(),'disabled'=>0));

                foreach ($providers_reault as $pro){
                    $cont_unique=0;
                    $npi=$pro->getNpiNumber();

                    if(!in_array($npi,$providersNPI)) {
                        $providersNPI[] = $npi;
                        $address=$pro->getBillingAddress();
                        if($address!=null){
                            foreach ($address as $addr){

                                $unique_str="U";
                                $cont_unique++;
                                if($cont_unique>1){
                                    $unique_str="";
                                }

                                $cell_A = 'A' . $cont;
                                $cell_B = 'B' . $cont;
                                $cell_C = 'C' . $cont;
                                $cell_D = 'D' . $cont;
                                $cell_E = 'E' . $cont;
                                $cell_F = 'F' . $cont;
                                $cell_G = 'G' . $cont;
                                $cell_H = 'H' . $cont;
                                $cell_I = 'I' . $cont;
                                $cell_J = 'J' . $cont;
                                $cell_K = 'K' . $cont;
                                $cell_L = 'L' . $cont;
                                $cell_M = 'M' . $cont;
                                $cell_N = 'N' . $cont;
                                $cell_O = 'O' . $cont;

                                $gender="";
                                if($pro->getGender()=="female" or $pro->getGender()=="Female" or $pro->getGender()=="Fenale"){
                                    $gender="F";
                                }
                                if($pro->getGender()=="male" or $pro->getGender()=="Male"){
                                    $gender="M";
                                }

                                $medicare="";
                                $pro_medicare=$pro->getMedicare();
                                if($pro_medicare!="" and $pro_medicare!="-" and $pro_medicare!="0" and $pro_medicare!="N/A" and $pro_medicare!="None" and $pro_medicare!="Pending" and $pro_medicare!="in process"
                                    and $pro_medicare!="NA" and $pro_medicare!="in proceses" and $pro_medicare!="55525 - opted out" and $pro_medicare!="submitting application" and $pro_medicare!="In-process"){
                                    $medicare=$pro->getMedicare();
                                }

                                if( $medicare==""){
                                    $medicare=$organization_medicare;
                                }

                                $id=$pro->getId();
                                $sql="SELECT p.id, 
                                GROUP_CONCAT(DISTINCT `degree`.`name` ORDER BY `degree`.`name` ASC SEPARATOR ', ') AS degrees
                                FROM  provider p
                                LEFT JOIN provider_degree ON provider_degree.provider_id = p.id
                                LEFT JOIN degree ON degree.id = provider_degree.degree_id
                                WHERE p.id=".$id."
                                GROUP BY p.id";

                                $stmt = $conn->prepare($sql);
                                $stmt->execute();

                                $provider_degree= $stmt->fetchAll();

                                $str_degree="";
                                foreach ($provider_degree as $pro_degree){
                                    $str_degree.=$pro_degree['degrees'];
                                }

                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $unique_str);
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $npi);
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $pro->getFirstName());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $pro->getLastName());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $organization->getName());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $medicare);
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $str_degree);
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_O, $gender);
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $addr->getStreet());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $addr->getSuiteNumber());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $addr->getCity());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $addr->getCounty());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_L, $addr->getUsState());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_M, $addr->getRegion());
                                $spreadsheet->getActiveSheet(0)->setCellValue($cell_N, $addr->getPhoneNumber());

                                $cont++;

                            }
                        }
                    }
                }
            }
        }

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Providers_Medicare_'.date('Ymd').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/providers_by_regions", name="report_providers_by_regions")
     *
     */
    public function providersByRegions(){
        $em = $this->getDoctrine()->getManager();

        $providers=$em->getRepository('App:Provider')->findBy(array('disabled'=>0));

        $cont1=0;
        $cont2=0;
        $cont3=0;
        $cont4=0;
        $cont5=0;
        $cont6=0;
        $cont7=0;
        $cont8=0;
        $cont9=0;
        $cont10=0;
        $cont11=0;

        foreach ($providers as $provider){
            $addresses=$provider->getBillingAddress();
            if($addresses){
                foreach ($addresses as $address){
                    if($address->getRegion()==1){
                        $cont1++;
                    }
                    if($address->getRegion()==2){
                        $cont2++;
                    }
                    if($address->getRegion()==3){
                        $cont3++;
                    }
                    if($address->getRegion()==4){
                        $cont4++;
                    }
                    if($address->getRegion()==5){
                        $cont5++;
                    }
                    if($address->getRegion()==6){
                        $cont6++;
                    }
                    if($address->getRegion()==7){
                        $cont7++;
                    }
                    if($address->getRegion()==8){
                        $cont8++;
                    }
                    if($address->getRegion()==9){
                        $cont9++;
                    }
                    if($address->getRegion()==10){
                        $cont10++;
                    }
                    if($address->getRegion()==11){
                        $cont11++;
                    }
                }
            }
        }

        echo $cont1."<br/>";
        echo $cont2."<br/>";
        echo $cont3."<br/>";
        echo $cont4."<br/>";
        echo $cont5."<br/>";
        echo $cont6."<br/>";
        echo $cont7."<br/>";
        echo $cont8."<br/>";
        echo $cont9."<br/>";
        echo $cont10."<br/>";
        echo $cont11."<br/>";


        die();
    }

    /**
     * @Route("/providers_by_telehealth", name="report_providers_by_providers_by_telehealth")
     *
     */
    public function providersByTelehealth()
    {
        $em = $this->getDoctrine()->getManager();

        $providers = $em->getRepository('App:Provider')->findBy(array('disabled' => 0));

        $cont=0;
        $contMD_DO=0;
        $contPHD=0;
        $contOthers=0;

        foreach ($providers as $provider) {
            $addresses = $provider->getBillingAddress();
            if ($addresses) {
                foreach ($addresses as $address){
                    $serviceSettings=$address->getServiceSettings();

                    if($serviceSettings){
                        foreach ($serviceSettings as $serviceSetting){
                            if($serviceSetting->getId()==17){
                                $cont++;

                                $degres=$provider->getDegree();

                                if($degres){
                                    foreach ($degres as $degre){
                                        if($degre->getId()==7 or $degre->getId()==3){
                                            $contMD_DO++;
                                        }
                                        if($degre->getId()==10 or $degre->getId()==11){
                                            $contPHD++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $contOthers=$cont-($contMD_DO+$contPHD);

        echo "Total: ".$cont."<br>";
        echo "MD DO: ".$contMD_DO."<br>";
        echo "PHD PSYD: ".$contPHD."<br>";
        echo "Others: ".$contOthers."<br>";
        die();
    }

    /**
     * @Route("/upload-proccess-addr", name="admin_upload_proccess_provider_addr")
     */
    function uploadProccessAddress(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $file = $request->files->get('providers_load');
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $private_key=$this->getParameter('private_key');
            $organization=$request->get('org');
            $orgObj=$em->getRepository('App\Entity\Organization')->find($organization);
            $providersOrg=$this->providersByOrganization($organization);
            $encoder=new My_Mcript($private_key);
            $fileName = "";
            if ($file != "") {
                $nombreoriginal = $file->getClientOriginalName();
                $fileName = $nombreoriginal . '-' . time() . '.' . $file->guessExtension();
                $adjuntosDir = $this->getparameter('providers_loaded');
                $file->move($adjuntosDir, $fileName);
            }

            //step for read the excel and read de information
            $fileToRead = $this->getparameter('providers_loaded') . "/" . $fileName;

            $reader = new Reader\Xlsx();
            $reader->setReadDataOnly(true);
            $spreadsheet = $reader->load($fileToRead);
            $spreadsheet->setActiveSheetIndex(1);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);

            $cont=0;
            $current_npi="";
            foreach ($sheetData as $sheet) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                      if($cont>=1){
                          $npi=$sheet['H'];
                          $first_name=$sheet['B'];
                          $last_name=$sheet['A'];
                          $middle_name=$sheet['C'];
                          $dob=$sheet['E'];
                          $social = $sheet['F'];
                          $social=str_replace("-","",$social);
                          $social_encode=$encoder->encryptthis($social);
                          $caqh=$sheet['I'];
                          $state_license=$sheet['J'];
                          $state_license_issue_date=$sheet['K'];
                          $state=$sheet['L'];
                          $medicaid=$sheet['M'];
                          $medicare=$sheet['N'];

                          $language1=$sheet['O'];
                          $language2=$sheet['P'];
                          $language3=$sheet['Q'];
                          $language4=$sheet['R'];

                          $languages=[];
                          $languages[]=$language1;
                          $languages[]=$language2;
                          $languages[]=$language3;
                          $languages[]=$language4;


                          $gender=$sheet['G'];

                          if($gender=="F"){
                              $gender="Female";
                          }
                          if($gender=="M"){
                              $gender="Male";
                          }

                          if($current_npi!=$npi){
                              $current_npi=$npi;
                          }

                          //check if exit this provider on this organization

                          $current_provider=$em->getRepository('App:Provider')->findBy(array('organization'=>$organization,'npi_number'=>$current_npi));
                          if($current_provider==null){
                             $provider=new Provider();

                             $provider->setNpiNumber($current_npi);
                             $provider->setFirstName($first_name);
                             $provider->setLastName($last_name);
                             $provider->setInitial($middle_name);
                             $provider->setOrganization($orgObj);
                             $provider->setDisabled(false);
                             $provider->setValidNpiNumber(true);
                             $provider->setDateOfBirth($dob);
                             $provider->setSocial($social_encode);
                             $provider->setGender($gender);
                             $provider->setCaqh($caqh);
                             $provider->setStateLic($state_license);
                             $provider->setLicIssueDate($state_license_issue_date);
                             $provider->setLicState($state);
                             $provider->setMedicaid($medicaid);
                             $provider->setMedicare($medicare);

                             foreach ($languages as $language) {
                                 $langObj = null;
                                 if ($language == "ENG" or $language == "English" or $language == "ENGLISH") {
                                     $langObj = $em->getRepository('App:Languages')->find(1);
                                 }
                                 if ($language == "SPA" or $language == "Spanish" or $language == "SPANISH") {
                                     $langObj = $em->getRepository('App:Languages')->find(2);
                                 }

                                 if ($langObj != null) {
                                     $provider->addLanguage($langObj);
                                 }
                             }

                             $em->persist($provider);
                             $em->flush();
                          }
                      }
                       $cont++;
                }
            }

            $cont=0;
            foreach ($sheetData as $sheet) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    if($cont>=1){
                        $npi= $sheet['H'];

                        $provider=$em->getRepository('App:Provider')->findOneBy(array('npi_number'=>$npi,'organization'=>$organization));

                        if($provider!=null){
                            $street=$sheet['Z'];
                            $zipcode=$sheet['AD'];

                            $result = $em->getRepository("App:BillingAddress")->createQueryBuilder('ba')
                                ->where('ba.street LIKE :street')
                                ->andWhere('ba.zipCode = :zipcode')
                                ->andWhere('ba.organization = :organization')
                                ->setParameter('street', '%'.$street.'%')
                                ->setParameter('zipcode', $zipcode)
                                ->setParameter('organization', $organization)
                                ->getQuery()
                                ->getResult();
                            
                            if($result!=null){
                               foreach ($result as $addr){

                                   //check if the provider have this organization
                                   $addr_id=$addr->getId();

                                   $current_address=$provider->getBillingAddress();
                                   $cont_c=0;
                                   if($current_address){
                                       foreach ($current_address as $current_addr){
                                           if($current_addr->getId()==$addr_id){
                                               $cont_c++;
                                           }
                                       }
                                   }

                                   if($cont_c==0){
                                       $provider->addBillingAddress($addr);
                                       $em->persist($provider);
                                       $em->flush();
                                   }
                               }
                            }
                        }
                    }
                }
                $cont++;
            }

            return $this->redirectToRoute('admin_view_organization',['id'=>$organization,'tab'=>2]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/check-providers-from-fcc-phy", name="check-providers-from-fcc-phy")
    */
    function checkroviderFromFCCPhyquiatric(Request $request){

        $em = $this->getDoctrine()->getManager();
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls";

        $fileName="FCC_Utilization_Psychiatry.xlsx";

        $fileToRead = $url."/" . $fileName;

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(0);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=1;

        foreach ($sheetData as $sheet) {
            if ($cont > 1) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                  $npi=$sheet['B'];

                  $current="Yes";
                  $provider=$em->getRepository('App:Provider')->findBy(array('npi_number'=>$npi,'disabled'=>0));

                  if($provider==null){
                      $current="No";
                  }

                  $cell_E='E'.$cont;

                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $current);
                }
            }
            $cont++;
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="FCC_Utilization_Psychiatry.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/check-providers-from-fcc-phy", name="check-providers-from-fcc-phy")
     */
    function getDataNPPESFCCPhyquiatric(Request $request){
        $em = $this->getDoctrine()->getManager();
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls";

        $fileName="FCC_Utilization_Psychiatry_BSN_Participate_06242021_only_no.xlsx";

        $fileToRead = $url."/" . $fileName;

        $reader = new Reader\Xlsx();
        $reader->setReadDataOnly(true);
        $spreadsheet = $reader->load($fileToRead);
        $spreadsheet->setActiveSheetIndex(2);
        $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
        $cont=1;

        foreach ($sheetData as $sheet) {
            if ($cont > 1) {
                if ($sheet['A'] != null or $sheet['A'] != "") {
                    $npi = $sheet['B'];
                    if($npi!="" and $npi!="NOT AVAILABLE" and $npi!="N/A") {

                        $url_result = "https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=200&skip=&pretty=on&version=2.1";

                        $json=file_get_contents($url_result);
                        $data=json_decode($json,true);
                        $type=$data['results'][0]['enumeration_type'];
                        if($data['result_count']>0){
                            $address=$data['results'][0]['addresses'];

                            foreach ($address as $addr){
                                if($addr['address_purpose']=="LOCATION"){

                                    $cell_F='F'.$cont;
                                    $cell_G='G'.$cont;
                                    $cell_H='H'.$cont;
                                    $cell_I='I'.$cont;
                                    $cell_J='J'.$cont;
                                    $cell_K='K'.$cont;
                                    $cell_L='L'.$cont;

                                    $zipcode=substr($addr['postal_code'],0,5);

                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $addr['address_1']);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $addr['address_2']);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, $addr['city']);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $zipcode);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $addr['state']);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_K, $addr['telephone_number']);
                                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_L,$type);
                                }
                            }
                        }
                    }
                }
            }
            $cont++;
        }
        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="FCC_Utilization_Psychiatry_Addr_data.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }


    /**
     * @Route("/update-taxonomies-nppes", name="admin_provider_update_taxonomies_NPPES")
     */
    public function updateTaxonomiesFromNPPES(){
        set_time_limit(88200);
        $em = $this->getDoctrine()->getManager();

        $providers=$em->getRepository('App\Entity\Provider')->findAll();
        $allTaxonomies=$em->getRepository('App\Entity\TaxonomyCode')->findAll();
        $taxonomiesArray=array();
        foreach ($allTaxonomies as $allTaxonomy){
            $taxonomiesArray[]=$allTaxonomy->getCode();

        }
        foreach ($providers as $provider){
            $npi=$provider->getNpiNumber();

            if($npi!="" and $npi!="NOT AVAILABLE" and $npi!="N/A"){
                $current_taxonomies=$provider->getTaxonomyCodes();
                if(count($current_taxonomies)==0){
                    $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=200&skip=&pretty=on&version=2.1";
                    $json=file_get_contents($url_result);
                    $data=json_decode($json,true);

                    if(count($data)>1){
                        if($data['result_count']>0){
                            $type=$data['results'][0]['enumeration_type'];
                            if($type=='NPI-1'){
                                $taxonomiesNPPES=$data['results'][0]['taxonomies'];

                                foreach ($taxonomiesNPPES as $taxonomyNPPE){
                                    $code=$taxonomyNPPE['code'];
                                    $taxObj=$em->getRepository('App\Entity\TaxonomyCode')->findOneBy(array('code'=>$code));

                                    $valid=0;
                                    $current_records=$provider->getTaxonomyCodes();
                                    if(count($current_records)>0){
                                        foreach ($current_records as $current_record){
                                            if($current_record->getCode()==$code){
                                                $valid++;
                                            }
                                        }
                                    }

                                    if($taxObj!=null and $valid==0){
                                        $provider->addTaxonomyCode($taxObj);
                                        $em->persist($provider);
                                        $em->flush();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        return new Response('All OK');
    }

    /**
     * @Route("/allow-credentialing-exception", name="allow-credentialing-exception",methods={"POST"})
    */
    public function allowCredentialingException(Request $request) {
        $em=$this->getDoctrine()->getManager();

        $ids = $request->get('ids');

        foreach ($ids as $id) {
            $provider=$em->getRepository('App:Provider')->find($id);

            if($provider){
                $provider->setAllowCred(true);
                $em->persist($provider);
            }
        }

        $em->flush();

        return new Response(
            json_encode(array('ok' =>1)), 200, array('Content-Type' => 'application/json')
        );
    }


}

