<?php

namespace App\Controller;

use App\Entity\BillingAddress;
use App\Entity\PayerCustomerCounty;
use App\Entity\Provider;
use FontLib\Table\Type\loca;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/company_status")
 */
class CompanyStatusController extends AbstractController
{
    /**
     * @Route("/providers", name="company_status_providers")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

        // providers by specialty
        $query = $em->createQuery('
          SELECT s.id, s.name, COUNT(p.id) AS provcount
          FROM App\Entity\Provider p
          LEFT JOIN p.primary_specialties s
          GROUP BY s.id, s.name
          ORDER BY s.name
        ');

        $specialty_result = $query->getArrayResult();

        // providers by degree
        $query = $em->createQuery('
          SELECT d.id, d.name, COUNT(p.id) AS provcount
          FROM App\Entity\Provider p
          LEFT JOIN p.degree d
          GROUP BY d.id, d.name
          ORDER BY d.name
        ');

        $degree_result = $query->getArrayResult();

            ///// providers by medicaid/madicare
            $total = $em->createQuery("SELECT COUNT(p) FROM App\Entity\Provider p")->getSingleScalarResult();
            $both = $em->createQuery("SELECT COUNT(p) FROM App\Entity\Provider p WHERE p.medicaid IS NOT NULL AND p.medicaid <> '-' AND p.medicare IS NOT NULL AND p.medicare <> '-'")->getSingleScalarResult();
            $medicaid = $em->createQuery("SELECT COUNT(p) FROM App\Entity\Provider p WHERE p.medicaid IS NOT NULL AND p.medicaid <> '-' AND (p.medicare IS NULL OR p.medicare = '-')")->getSingleScalarResult();
            $medicare = $em->createQuery("SELECT COUNT(p) FROM App\Entity\Provider p WHERE (p.medicaid IS NULL OR p.medicaid = '-') AND p.medicare IS NOT NULL AND p.medicare <> '-'")->getSingleScalarResult();

            $medic_result = [
                'total' => $total,
                'both' => $both,
                'medicaid' => $medicaid,
                'medicare' => $medicare,
                'none' => ($total - $both - $medicaid - $medicare)
            ];

            ///// providers by medicaid/madicare
            $by_location_total = $em->createQuery("SELECT COUNT(e) FROM App\Entity\BillingAddress e")->getSingleScalarResult();
            $by_location = $em->createQuery("SELECT COUNT(e) FROM App\Entity\BillingAddress e WHERE e.is_facility = 1")->getSingleScalarResult();
            $by_provider_total = $em->createQuery("SELECT COUNT(e) FROM App\Entity\Provider e")->getSingleScalarResult();

            $by_provider=0;
            $providers=$em->getRepository('App:Provider')->findBy(array('disabled'=>0));

            foreach ($providers as $provider){
                if($provider->getIsFacility()==true){
                    $by_provider++;
                }
            }

            $facilities_result = [
                'by_location_total' => $by_location_total,
                'by_location' => $by_location,
                'by_provider_total' => $by_provider_total,
                'by_provider' => $by_provider,
            ];

            $conn = $em->getConnection();
            $sql="SELECT p.id FROM provider p INNER JOIN provider_payer ON provider_payer.provider_id = p.id WHERE  p.disabled=:disabled GROUP BY p.id";
            $stmt = $conn->prepare($sql);
            $stmt->execute(array('disabled'=>0));
            $totalProviders= $stmt->rowCount();

            //provider asigned to MMM Payer
            $sql="SELECT p.id FROM provider p INNER JOIN provider_payer ON provider_payer.provider_id = p.id
             WHERE p.disabled =:disabled AND provider_payer.payer_id=:payer";
            $stmt = $conn->prepare($sql);
            $stmt->execute(array('disabled'=>0,'payer'=>1));
            $totalProvidersMMM= $stmt->rowCount();

            //provider asigned to Pending Payer
            $sql2="SELECT p.id FROM provider p INNER JOIN provider_payer ON provider_payer.provider_id = p.id
            WHERE p.disabled =:disabled AND provider_payer.payer_id=:payer";
            $stmt2 = $conn->prepare($sql2);
            $stmt2->execute(array('disabled'=>0,'payer'=>2));
            $totalProvidersPending= $stmt2->rowCount();

            //provider with facilities and not (Total)
            $sql1="SELECT p.id FROM provider p
            INNER JOIN provider_payer ON provider_payer.provider_id = p.id
            INNER JOIN provider_billing_address ON provider_billing_address.provider_id = p.id
            INNER JOIN billing_address ON provider_billing_address.billing_address_id = billing_address.id
            WHERE p.disabled =:disabled AND billing_address.is_facility=:facility GROUP BY p.id";
            $stmt1 = $conn->prepare($sql1);
            $stmt1->execute(array('disabled'=>0,'facility'=>1));
            $totalProvidersFacility= $stmt1->rowCount();

            $totalProvidersNotFacility= $totalProviders-$totalProvidersFacility;

            //provider with facilities and not (MMM)
            $sql4="SELECT p.id FROM provider p
            INNER JOIN provider_payer ON provider_payer.provider_id = p.id
            INNER JOIN provider_billing_address ON provider_billing_address.provider_id = p.id
            INNER JOIN billing_address ON provider_billing_address.billing_address_id = billing_address.id
            WHERE p.disabled =:disabled AND billing_address.is_facility=:facility and  provider_payer.payer_id=:payer GROUP BY p.id";
            $stmt4 = $conn->prepare($sql4);
            $stmt4->execute(array('disabled'=>0,'facility'=>1,'payer'=>1));
            $mmmProvidersFacility= $stmt4->rowCount();

            $mmmProvidersNotFacility= $totalProvidersMMM-$mmmProvidersFacility;


            //provider with facilities and not (Pending)
            $sql5="SELECT p.id FROM provider p
        INNER JOIN provider_payer ON provider_payer.provider_id = p.id
        INNER JOIN provider_billing_address ON provider_billing_address.provider_id = p.id
        INNER JOIN billing_address ON provider_billing_address.billing_address_id = billing_address.id
        WHERE p.disabled =:disabled AND billing_address.is_facility=:facility and  provider_payer.payer_id=:payer GROUP BY p.id";
            $stmt5 = $conn->prepare($sql5);
            $stmt5->execute(array('disabled'=>0,'facility'=>1,'payer'=>2));
            $pendingProvidersFacility= $stmt5->rowCount();

            $pendingProvidersNotFacility=$totalProvidersPending-$pendingProvidersFacility;

            $payerTotal=array();
            $payerMMM=array();
            $payerPending=array();

            $payerTotal[0]=$totalProviders;
            $payerTotal[1]=$totalProvidersFacility;
            $payerTotal[2]=$totalProvidersNotFacility;

            $payerMMM[0]=$totalProvidersMMM;
            $payerMMM[1]=$mmmProvidersFacility;
            $payerMMM[2]=$mmmProvidersNotFacility;

            $payerPending[0]=$totalProvidersPending;
            $payerPending[1]=$pendingProvidersFacility;
            $payerPending[2]=$pendingProvidersNotFacility;


            return $this->render('company_status/providers.html.twig',
                array('specialty_result' => $specialty_result,
                    'degree_result' => $degree_result,
                    'medic_result' => $medic_result,
                    'facilities_result' => $facilities_result,
                    'payerTotal'=>$payerTotal,
                    'payerMMM'=>$payerMMM,
                    'payerPending'=>$payerPending));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/degrees", name="company_status_degrees")
     */
    public function degrees(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $allCounties=$em->getRepository('App\Entity\CountyMatch')->findAll();
            $degreeByCounty=array();

            foreach ($allCounties as $county){
                $cont_MD_DO=0;
                $cont_PSYD_PHD=0;
                $cont_LCSW=0;
                $cont_LMFT=0;
                //get all locations for each county
                $billingAddress=$em->getRepository('App\Entity\BillingAddress')->findBy(array('county_match'=>$county->getId()));
                if($billingAddress!=null){
                    foreach ($billingAddress as $billingAddr){
                        $idBillindAddress=$billingAddr->getId();

                        $sql="SELECT * FROM provider_billing_address p WHERE p.billing_address_id=:idBillindAddress";
                        $stmt = $conn->prepare($sql);
                        $stmt->execute(array('idBillindAddress'=>$idBillindAddress));

                        $provider_billingAddress= $stmt->fetchAll();
                        foreach ($provider_billingAddress as $provider_ba) {
                           $provider_id=$provider_ba['provider_id'];
                           $provider=$em->getRepository('App\Entity\Provider')->find($provider_id);
                           if($provider!=null){
                               $providerDegrees=$provider->getDegree();
                               if($providerDegrees!=null){
                                   foreach ($providerDegrees as $degree){
                                       if($degree->getName()=='MD' || $degree->getName()=='DO'){
                                           $cont_MD_DO++;
                                       }
                                       if($degree->getName()=='PSYD' || $degree->getName()=='PHD'){
                                           $cont_PSYD_PHD++;
                                       }
                                       if($degree->getName()=='LCSW'){
                                           $cont_LCSW++;
                                       }
                                       if($degree->getName()=='LMFT'){
                                           $cont_LMFT++;
                                       }
                                   }
                               }
                           }
                        }
                    }
                }

                $countyID=$county->getId();
                $degreeByCounty[$countyID][0]=$cont_MD_DO;
                $degreeByCounty[$countyID][1]=$cont_PSYD_PHD;
                $degreeByCounty[$countyID][2]=$cont_LCSW;
                $degreeByCounty[$countyID][3]=$cont_LMFT;

            }

            $payers=$em->getRepository('App\Entity\Payer')->findAll();

            return $this->render('company_status/degrees.html.twig',array('allCounties'=>$allCounties,'degreesByCounty'=>$degreeByCounty,'payers'=>$payers));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/load_customer_payer", name="load_customer_payer")
     */
    public function loadCustomerPayer(Request $request){
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $id=$request->get('id');
        $customers=$em->getRepository('App\Entity\PayerCustomerCounty')->findBy(array('payer'=>$id));

        $customerResult=array();
        $customerR=array();

        if($customers!=null){
            foreach ($customers as $customer){
                $customerR['total']=$customer->getTotal();
                $customerR['county']=$customer->getCounty()->getId();
                $customerResult[]=$customerR;
            }
        }


        return new Response(
            json_encode(array('customers'=>$customerResult)), 200, array('Content-Type' => 'application/json')
        );

    }

    /**
     * @Route("/map-test", name="report_map_example")
     */
    public function mapTest(){
        $em = $this->getDoctrine()->getManager();
        $google_apikey = $this->getParameter('google_apikey');

        $allLocations=$em->getRepository('App\Entity\BillingAddress')->findAll();

        $address=array();
        $cont=1;
        foreach ($allLocations as $location){
            if($location->getLat()!=null and $location->getLng()!=null and $cont<=10){
                $address[]=$location;
                $cont++;
            }
        }

        return $this->render('company_status/map_ratio.html.twig',array('google_apikey'=>$google_apikey,'address'=>$address));
    }

    /**
     * @Route("/map-test-2", name="report_map_example-2")
     */
    public function mapTest2(){

        $google_apikey = $this->getParameter('google_apikey');

        return $this->render('company_status/map_test.html.twig',array('google_apikey'=>$google_apikey));

    }


    /**
     * @Route("/zip-code-radius", name="report_zip-code-radius")
     */
    public function zipCodeRadius(){
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $google_apikey = $this->getParameter('google_apikey');

        $addressR=array();
        $sql="SELECT b.zip_code FROM billing_address b GROUP BY b.zip_code LIMIT 15";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $zips= $stmt->fetchAll();

        $cont=1;
        foreach ($zips as $zip){
            $zip_code=$zip['zip_code'];
            $addr=$em->getRepository('App\Entity\BillingAddress')->findOneBy(array('zipCode'=>$zip_code));
            $addressR[]=$addr;
        }

        $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));

        return $this->render('company_status/zipcoderadio.html.twig',array('google_apikey'=>$google_apikey,'address'=>$addressR,'providers'=>$providers));
    }


    /**
     * @Route("/load_address_degree", name="load_address_degree")
     */
    public function loadAddressDegree(Request $request){
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $degree=$request->get('degree');

        //$url_result='https://www.zipcodeapi.com/rest/xx3kAjJztnKWw8aq7fl6ikoBK44jrWoa7F5TvgVgEReT2hZojP4x2Xsy5ABnDOOb/radius.json/32704/$mile/mile';
        // $json=file_get_contents($url_result);
        $addressR=array();
        $addressRFinal=array();

        $sql="SELECT billing_address.* from billing_address
             WHERE billing_address.region IN (9,10,11) and billing_address.county!='Monroe'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $addressR= $stmt->fetchAll();

        if($degree==1){
            foreach ($addressR as $addr){
                $sql="SELECT p.* from provider p 
                LEFT JOIN `provider_degree` ON `provider_degree`.provider_id = p.id
                LEFT JOIN `degree` ON `degree`.`id` = `provider_degree`.`degree_id`
                LEFT JOIN `provider_billing_address` ON `provider_billing_address`.provider_id = p.id
                LEFT JOIN `billing_address` ON `billing_address`.`id` = `provider_billing_address`.`billing_address_id`
                WHERE degree.id IN (3,7) and billing_address.id=:addr_id";

                $stmt2 = $conn->prepare($sql);
                $stmt2->execute(array('addr_id'=>$addr['id']));
                $providers=$stmt2->fetchAll();

                if($providers!=null and count($providers)>0){
                    $addressRFinal[]=$addr;
                }
            }
        }

        if($degree==2){
            foreach ($addressR as $addr){
                $sql="SELECT p.* from provider p 
                LEFT JOIN `provider_degree` ON `provider_degree`.provider_id = p.id
                LEFT JOIN `degree` ON `degree`.`id` = `provider_degree`.`degree_id`
                LEFT JOIN `provider_billing_address` ON `provider_billing_address`.provider_id = p.id
                LEFT JOIN `billing_address` ON `billing_address`.`id` = `provider_billing_address`.`billing_address_id`
                WHERE degree.id IN (10,11) and billing_address.id=:addr_id";

                $stmt2 = $conn->prepare($sql);
                $stmt2->execute(array('addr_id'=>$addr['id']));
                $providers=$stmt2->fetchAll();

                if($providers!=null and count($providers)>0){
                    $addressRFinal[]=$addr;
                }
            }
        }

        if($degree==3){
            foreach ($addressR as $addr){
                $sql="SELECT p.* from provider p 
                LEFT JOIN `provider_degree` ON `provider_degree`.provider_id = p.id
                LEFT JOIN `degree` ON `degree`.`id` = `provider_degree`.`degree_id`
                LEFT JOIN `provider_billing_address` ON `provider_billing_address`.provider_id = p.id
                LEFT JOIN `billing_address` ON `billing_address`.`id` = `provider_billing_address`.`billing_address_id`
                WHERE degree.id IN (4) and billing_address.id=:addr_id";

                $stmt2 = $conn->prepare($sql);
                $stmt2->execute(array('addr_id'=>$addr['id']));
                $providers=$stmt2->fetchAll();

                if($providers!=null and count($providers)>0){
                    $addressRFinal[]=$addr;
                }
            }
        }

        if($degree==4){
            foreach ($addressR as $addr){
                $sql="SELECT p.* from provider p 
                LEFT JOIN `provider_degree` ON `provider_degree`.provider_id = p.id
                LEFT JOIN `degree` ON `degree`.`id` = `provider_degree`.`degree_id`
                LEFT JOIN `provider_billing_address` ON `provider_billing_address`.provider_id = p.id
                LEFT JOIN `billing_address` ON `billing_address`.`id` = `provider_billing_address`.`billing_address_id`
                WHERE degree.id IN (5) and billing_address.id=:addr_id";

                $stmt2 = $conn->prepare($sql);
                $stmt2->execute(array('addr_id'=>$addr['id']));
                $providers=$stmt2->fetchAll();

                if($providers!=null and count($providers)>0){
                    $addressRFinal[]=$addr;
                }
            }
        }

        return new Response(
            json_encode(array('addressR'=>$addressRFinal)), 200, array('Content-Type' => 'application/json')
        );
    }

}
