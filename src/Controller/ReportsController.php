<?php

namespace App\Controller;

use PhpOffice\PhpSpreadsheet\IOFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/report")
 */
class ReportsController extends AbstractController
{
    /**
     * @Route("/teleheath-report", name="reports-teleheath_report")
     */
    public function index()
    {
        set_time_limit(3600);
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');
        $spreadsheet = $reader->load($url.'telehealth-service-report.xlsx');
        $spreadsheet->setActiveSheetIndex(0);

        $em=$this->getDoctrine()->getManager();

        $organizations=$em->getRepository('App\Entity\Organization')->findBy(array('disabled'=>0,'organization_status'=>2));

        $cont=2;
        foreach ($organizations as $organization){

            $addresses=$em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
            $match=0;
            if($addresses!=null){
                 foreach ($addresses as $address){
                     $serviceSettings=$address->getServiceSettings();
                     if($serviceSettings!=null){
                        foreach ($serviceSettings as $serviceSetting){
                            if($serviceSetting->getId()==17){
                               $match=1;
                            }
                        }
                     }

                     if($address->getPrimaryAddr()==true and $match==1){
                         $providers=$em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0,'organization'=>$organization->getId()));
                         if($providers!=null){

                             foreach ($providers as $provider){
                                 $cell_A = 'A' . $cont;
                                 $cell_B = 'B' . $cont;
                                 $cell_C = 'C' . $cont;
                                 $cell_D = 'D' . $cont;
                                 $cell_E = 'E' . $cont;
                                 $cell_F = 'F' . $cont;
                                 $cell_G = 'G' . $cont;

                                 $degrees=$provider->getDegree();
                                 $degree_str="";
                                 if($degrees!=null){
                                     foreach ($degrees as $degree){
                                         $degree_str=$degree->getName();
                                         break;
                                     }
                                 }

                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $organization->getName());
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_B,$provider->getFirstName()." ".$provider->getLastName());
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $degree_str);
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $address->getStreet());
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $address->getCity());
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, $address->getCounty());
                                 $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $address->getRegion());
                                 $cont++;
                             }
                         }
                     }
                 }
            }
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="telehealth_report'.date('Ymd').'.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/aetna-coventry", name="reports-aetna-coventry")
     */
    public function aetnaCoventry(){
        set_time_limit(3600);
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');
        $spreadsheet = $reader->load($url.'aetna_coventry_template.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $em=$this->getDoctrine()->getManager();

        $cont=2;

        $pnv_data=$em->getRepository('App:AETData')->findAll();
        $npis_array=[];
        foreach ($pnv_data as $data){
            $id=$data->getProviderId();
            $provider=$em->getRepository('App:Provider')->find($id);
            $npi=$data->getNpi();

            $datum=$data->getData();
            $specialtyCode=$datum['primary_specialty'];
            $credentialing_date=$datum['credentialed_date'];

            $moth=substr($credentialing_date,4,2);
            $day=substr($credentialing_date,6,2);
            $year=substr($credentialing_date,0,4);

            $date=$moth."/".$day."/".$year;


            $specialty=$em->getRepository('App:Specialty')->findOneBy(array('code'=>$specialtyCode));

            $specialty_name="";
            if($specialty){
                $specialty_name=$specialty->getName();
            }

            $cell_A = 'A' . $cont;
            $cell_B = 'B' . $cont;
            $cell_C = 'C' . $cont;
            $cell_D = 'D' . $cont;
            $cell_E = 'E' . $cont;
            $cell_F = 'F' . $cont;
            $cell_G = 'G' . $cont;
            $cell_H = 'H' . $cont;
            $cell_I = 'I' . $cont;
            $cell_J = 'J' . $cont;

            $degree_str="";
            $degree=$provider->getDegree();
            if($degree){
                foreach ($degree as $dr){
                    $degree_str=$dr->getName().", ";
                }
            }

            $degree_str=substr($degree_str, 0,-2);

            $board="No";
            $board_certified=$provider->getBoardCertified();
            if($board_certified==1){
                $board="Yes";
            }

            $ind_cred=$em->getRepository('App:CredSimpleRecord')->findOneBy(array('npi'=>$npi));

            if($ind_cred and !in_array($npi,$npis_array)){
                if($ind_cred->getStatus()=='approved'){
                    $npis_array[]=$npi;
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $provider->getFirstName()." ".$provider->getLastName());
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $degree_str);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, "Specialist");
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, $specialty_name);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $date);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_G, $date);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, "Initial");
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_H, "FL");
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_I, $npi);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_J, $board);

                    $cont++;
                }
            }
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="aetna_coventry_credentialed_practitioners.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");
    }

    /**
     * @Route("/aetna-coventry-facility", name="reports-aetna-coventry-facility")
     */
    public function aetnaCoventryFacility(){
        set_time_limit(3600);
        $projectDir = $this->getParameter('kernel.project_dir');
        $url= $projectDir."/public/template_xls/";
        $reader=IOFactory::createReader('Xlsx');
        $spreadsheet = $reader->load($url.'aetna_coventry_facility_template.xlsx');
        $spreadsheet->setActiveSheetIndex(0);
        $em=$this->getDoctrine()->getManager();

        $cont=2;

        $pnv_data=$em->getRepository('App:AETDataOrg')->findAll();

        foreach ($pnv_data as $data){
            $org_id=$data->getOrganizationId();

            $organization=$em->getRepository('App:Organization')->find($org_id);

            $cell_A = 'A' . $cont;
            $cell_B = 'B' . $cont;
            $cell_C = 'C' . $cont;
            $cell_D = 'D' . $cont;
            $cell_E = 'E' . $cont;
            $cell_F = 'F' . $cont;



            //check if the organization is a facility
            $address=$em->getRepository('App:BillingAddress')->findBy(array('organization'=>$org_id));
            $is_facility=false;
            if($address){
                foreach ($address as $addr){
                   if($addr->getIsFacility()==true){
                       $is_facility=true;
                   }
                }
            }

            if($is_facility==true){
                $date="";
                $credentialing=$em->getRepository('App:OrganizationCredentialing')->findOneBy(array('organization'=>$org_id,'credentialing_status'=>4));

                if($credentialing){

                    $facility_type="";

                    $addresses=$em->getRepository('App:BillingAddress')->findBy(array('organization'=>$org_id));
                    if($addresses){
                        foreach ($addresses as $address){
                            if($address->getIsFacility()){
                                $types=$address->getFacilityType();
                                if(count($types)){
                                    foreach ($types as $type){
                                        $facility_type.=$type->getName().", ";
                                    }
                                }
                            }
                        }
                    }

                    if($facility_type!=""){
                        $facility_type=substr($facility_type,0,-2);
                    }

                    $date= $credentialing->getCredentialingAcceptedDate();
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_A, $organization->getName());

                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_B, $facility_type);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_C, $date);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_D, "Initial");
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_E, $date);
                    $spreadsheet->getActiveSheet(0)->setCellValue($cell_F, "FL");

                    $cont++;
                }
            }
        }

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="aetna_coventry_credentialed_facilities.xlsx"');

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');

        // Return a text response to the browser saying that the excel was succesfully created
        return new Response("Report Export Succefully");

    }

    /**
     * @Route("/aetna-practitioner-type", name="reports-aetna-practitioner-type")
     */
    public function aetnaPractitionerType(){
        set_time_limit(3600);
        $em=$this->getDoctrine()->getManager();

        $result=0;
        $providers=$em->getRepository('App:Provider')->findBy(array('disabled'=>0));
        $individuals_ids=[];
        foreach ($providers as $provider){
            $npi=$provider->getNpiNumber();
            $credentialing=$em->getRepository('App:ProviderCredentialing')->findOneBy(array('credentialing_status'=>4,'npi_number'=>$npi));
            $address=$provider->getBillingAddress();
            $degrees=$provider->getDegree();

            if($credentialing){
               //degress accepted
               // 7,3,10,11,4,6,26,34,5
               if($degrees){
                   foreach ($degrees as $degree){
                       $id_degree=$degree->getId();
                        if($id_degree==7 or $id_degree==3 or $id_degree==10 or $id_degree==11 or $id_degree==4 or $id_degree==6 or $id_degree==26
                            or $id_degree==434 or $id_degree==5){
                            $individuals_ids[]=$provider->getId();
                            if($address){
                                foreach ($address as $addr){
                                    $result++;
                                }
                            }

                            //check if this provider facility have other record on other not facility organizaiton
                            $other_providers=$em->getRepository('App:Provider')->findBy(array('disabled'=>0, 'npi_number'=>$npi));
                            if($other_providers){
                                foreach ($other_providers as $other_provider){
                                    $id=$other_provider->getId();
                                    if(!in_array($id,$individuals_ids)){
                                        $individuals_ids[]=$provider->getId();
                                        $o_addresses=$other_provider->getBillingAddress();
                                        if($o_addresses){
                                            foreach ($o_addresses as $ty){
                                                $result++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                   }
               }
            }

            //facility credentialing
            $organization=$provider->getOrganization();
            $org_credentialing=$em->getRepository('App:OrganizationCredentialing')->findBy(array('organization'=>$organization->getId(),'credentialing_status'=>4));
            if($org_credentialing){
                if($degrees){
                    foreach ($degrees as $degree){
                        $id_degree=$degree->getId();
                        if($id_degree==7 or $id_degree==3 or $id_degree==10 or $id_degree==11 or $id_degree==4 or $id_degree==6 or $id_degree==26
                            or $id_degree==434 or $id_degree==5){
                            $individuals_ids[]=$provider->getId();
                            if($address){
                                foreach ($address as $addr){
                                    $result++;
                                }
                            }

                            //check if this provider facility have other record on other not facility organizaiton
                            $other_providers=$em->getRepository('App:Provider')->findBy(array('disabled'=>0, 'npi_number'=>$npi));
                            if($other_providers){
                                foreach ($other_providers as $other_provider){
                                    $id=$other_provider->getId();
                                    if(!in_array($id,$individuals_ids)){
                                        $individuals_ids[]=$provider->getId();
                                        $o_addresses=$other_provider->getBillingAddress();
                                        if($o_addresses){
                                            foreach ($o_addresses as $ty){
                                                $result++;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        echo $result;
        die();
    }




}
