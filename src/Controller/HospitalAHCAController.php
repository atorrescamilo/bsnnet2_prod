<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/hospitals")
 */
class HospitalAHCAController extends AbstractController
{
    /**
     * @Route("/index", name="admin_hospital_index")
     */
    public function index(){
        $em=$this->getDoctrine()->getManager();

        $hospitals=$em->getRepository('App\Entity\HospitalAHCA')->findAll();
        $delete_form_ajax = $this->createCustomForm('CVO_ID', 'DELETE', 'admin_delete_hospital');

        return $this->render('hospital_ahca/index.html.twig', [
            'hospitals' => $hospitals,'delete_form_ajax' => $delete_form_ajax->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_hospital",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $cvo = $cvo = $em->getRepository('App\Entity\HospitalAHCA')->find($id);
            $removed = 0;
            $message = "";

            if ($cvo) {
                try {
                    $em->remove($cvo);
                    $em->flush();
                    $removed = 1;
                    $message = "The Hospital has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Hospital can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_hospital",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $hospital= $em->getRepository('App\Entity\HospitalAHCA')->find($id);

                if ($hospital) {
                    try {
                        $em->remove($hospital);
                        $em->flush();
                        $removed = 1;
                        $message = "The Hospital has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Hospital can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }
}
