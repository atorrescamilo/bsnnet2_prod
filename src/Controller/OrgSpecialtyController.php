<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\OrgSpecialty;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/orgspecialty")
 */
class OrgSpecialtyController extends AbstractController{

    /**
     * @Route("/index", name="admin_orgspecialty")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $orgspecialtys = $em->getRepository('App\Entity\OrgSpecialty')->findAll();

            $delete_form_ajax = $this->createCustomForm('ORGSPECIALTY_ID', 'DELETE', 'admin_delete_orgspecialty');


            return $this->render('orgspecialty/index.html.twig', array('orgspecialtys' => $orgspecialtys, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_orgspecialty")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('orgspecialty/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_orgspecialty", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\OrgSpecialty')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_orgspecialty');
            }

            return $this->render('orgspecialty/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_orgspecialty", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\OrgSpecialty')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_orgspecialty');
            }

            return $this->render('orgspecialty/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_orgspecialty")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $code = $request->get('code');
            $new = $request->get('new');

            $orgspecialty = new OrgSpecialty();
            $orgspecialty->setName($name);
            $orgspecialty->setCode($code);

            $em->persist($orgspecialty);
            $em->flush();

            $this->addFlash(
                'success',
                'Organization Specialty has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_orgspecialty');
            }

            return $this->redirectToRoute('admin_orgspecialty');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_orgspecialty")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $code = $request->get('code');
            $new = $request->get('new');
            $id = $request->get('id');

            $orgspecialty = $em->getRepository('App\Entity\OrgSpecialty')->find($id);

            if ($orgspecialty == null) {
                $this->addFlash(
                    "danger",
                    "The Organization Specialty can't been updated!"
                );

                return $this->redirectToRoute('admin_orgspecialty');
            }

            if ($orgspecialty != null) {
                $orgspecialty->setName($name);
                $orgspecialty->setCode($code);

                $em->persist($orgspecialty);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "Organization Specialty has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_orgspecialty');
            }

            return $this->redirectToRoute('admin_orgspecialty');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_orgspecialty",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $orgspecialty = $orgspecialty = $em->getRepository('App\Entity\OrgSpecialty')->find($id);
            $removed = 0;
            $message = "";

            if ($orgspecialty) {
                try {
                    $em->remove($orgspecialty);
                    $em->flush();
                    $removed = 1;
                    $message = "The Organization Specialty has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The orgspecialty can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_orgspecialty",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $orgspecialty = $orgspecialty = $em->getRepository('App\Entity\OrgSpecialty')->find($id);

                if ($orgspecialty) {
                    try {
                        $em->remove($orgspecialty);
                        $em->flush();
                        $removed = 1;
                        $message = "The Organization Specialty has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Organization Specialty can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/isUnique", name="admin_isunique_orgspecialty",methods={"POST"})
     */
    public function isUnique(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $code = $request->get('code');

            $orgspecialty = $em->getRepository('App\Entity\OrgSpecialty')->findOneBy(array('code' => $code));

            $isUnique = 0;
            if ($orgspecialty != null) {
                $isUnique = 1;
            }

            return new Response(
                json_encode(array('isUnique' => $isUnique)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
