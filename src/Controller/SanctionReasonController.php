<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\SanctionReason;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/sanctionreason")
 */
class SanctionReasonController extends AbstractController{
    /**
     * @Route("/index", name="admin_sanctionreason")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $sanctionreasons = $em->getRepository('App\Entity\SanctionReason')->findAll();

            $delete_form_ajax = $this->createCustomForm('SANCTIONREASON_ID', 'DELETE', 'admin_delete_sanctionreason');


            return $this->render('sanctionreason/index.html.twig', array('sanctionreasons' => $sanctionreasons, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_sanctionreason")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('sanctionreason/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_sanctionreason", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\SanctionReason')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_sanctionreason');
            }

            return $this->render('sanctionreason/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_sanctionreason", defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\SanctionReason')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_sanctionreason');
            }

            return $this->render('sanctionreason/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_sanctionreason")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');

            $sanctionreason = new SanctionReason();
            $sanctionreason->setName($name);
            $sanctionreason->setDescription($description);

            $em->persist($sanctionreason);
            $em->flush();

            $this->addFlash(
                'success',
                'The Sanction Reason has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_sanctionreason');
            }

            return $this->redirectToRoute('admin_sanctionreason');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_sanctionreason")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $sanctionreason = $em->getRepository('App\Entity\SanctionReason')->find($id);

            if ($sanctionreason == null) {
                $this->addFlash(
                    "danger",
                    "The Sanction Reason can't been updated!"
                );

                return $this->redirectToRoute('admin_sanctionreason');
            }

            if ($sanctionreason != null) {
                $sanctionreason->setName($name);
                $sanctionreason->setDescription($description);

                $em->persist($sanctionreason);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "The Sanction Reason has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_sanctionreason');
            }

            return $this->redirectToRoute('admin_sanctionreason');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_sanctionreason",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $sanctionreason = $sanctionreason = $em->getRepository('App\Entity\SanctionReason')->find($id);
            $removed = 0;
            $message = "";

            if ($sanctionreason) {
                try {
                    $em->remove($sanctionreason);
                    $em->flush();
                    $removed = 1;
                    $message = "The Sanction Reason has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Sanction Reason can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_sanctionreason",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $sanctionreason = $sanctionreason = $em->getRepository('App\Entity\SanctionReason')->find($id);

                if ($sanctionreason) {
                    try {
                        $em->remove($sanctionreason);
                        $em->flush();
                        $removed = 1;
                        $message = "The Sanction Reason has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Sanction Reason can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
