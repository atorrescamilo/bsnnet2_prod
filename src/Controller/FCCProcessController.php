<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/fcc-process")
 */
class FCCProcessController extends AbstractController
{
    /**
     * @Route("/providers-list", name="fcc_process_providers")
     */
    public function index(){
        $em=$this->getDoctrine()->getManager();
        $conn = $em->getConnection();

        $sql="SELECT p.id,  p.first_name,  p.last_name, p.npi_number, o.name as organization_name
            FROM  provider p 
            LEFT JOIN organization o  ON p.org_id = o.id
            WHERE p.disabled=0
            GROUP BY p.id
            ORDER BY p.id";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $providers= $stmt->fetchAll();

        return $this->render('fcc_process/providers.html.twig', [
            'providers' => $providers,
        ]);
    }

    /**
     * @Route("/providers-list", name="fcc_process_organizations")
     */
    public function indexOrganizations(){
        $em=$this->getDoctrine()->getManager();

        return $this->render('providers.html.twig', [
            'controller_name' => 'FCCProcessController',
        ]);
    }
}
