<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Comorbidity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/comorbidity")
 */
class ComorbidityController extends AbstractController{

    /**
     * @Route("/index", name="admin_comorbidity")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $comorbiditys = $em->getRepository('App\Entity\Comorbidity')->findAll();

            $delete_form_ajax = $this->createCustomForm('COMORBIDITY_ID', 'DELETE', 'admin_delete_comorbidity');


            return $this->render('comorbidity/index.html.twig', array('comorbiditys' => $comorbiditys, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_comorbidity")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('comorbidity/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_comorbidity",defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Comorbidity')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_comorbidity');
            }

            return $this->render('comorbidity/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_comorbidity",defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\Comorbidity')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_comorbidity');
            }

            return $this->render('comorbidity/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_comorbidity")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');

            $comorbidity = new Comorbidity();
            $comorbidity->setName($name);
            $comorbidity->setDescription($description);

            $em->persist($comorbidity);
            $em->flush();

            $this->addFlash(
                'success',
                'Co Morbidity has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_comorbidity');
            }

            return $this->redirectToRoute('admin_comorbidity');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_comorbidity")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $comorbidity = $em->getRepository('App\Entity\Comorbidity')->find($id);

            if ($comorbidity == null) {
                $this->addFlash(
                    "danger",
                    "The Co Morbidity can't been updated!"
                );

                return $this->redirectToRoute('admin_comorbidity');
            }

            if ($comorbidity != null) {
                $comorbidity->setName($name);
                $comorbidity->setDescription($description);

                $em->persist($comorbidity);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "Co Morbidity has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_comorbidity');
            }

            return $this->redirectToRoute('admin_comorbidity');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_comorbidity",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $comorbidity = $comorbidity = $em->getRepository('App\Entity\Comorbidity')->find($id);
            $removed = 0;
            $message = "";

            if ($comorbidity) {
                try {
                    $em->remove($comorbidity);
                    $em->flush();
                    $removed = 1;
                    $message = "The Co Morbidity has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Co Morbidity can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_comorbidity",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $comorbidity = $comorbidity = $em->getRepository('App\Entity\Comorbidity')->find($id);

                if ($comorbidity) {
                    try {
                        $em->remove($comorbidity);
                        $em->flush();
                        $removed = 1;
                        $message = "The Co Morbidity has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Co Morbidity can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
