<?php

namespace App\Controller;

use App\Entity\ProviderUser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\Bridge\Google\Smtp\GmailTransport;
use Symfony\Component\Mailer\Mailer;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * @Route("/provider")
 */
class ProviderSecurityController extends AbstractController
{
    /**
     * @Route("/login", name="provider_login")
    */
    public function provider_login(Request $request, AuthenticationUtils $authenticationUtils)
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        /*
        if ($request->isMethod('POST')) {
            return $this->redirectToRoute('provider_dashboard');
        }*/

        return $this->render('security/providerlogin.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="provider_logout")
    */
    public function logout(){
        return $this->redirectToRoute('default');
    }

    /**
     * @Route("/register", name="register")
    */
    public function register(){
        $em=$this->getDoctrine()->getManager();
        $roles=$em->getRepository('App\Entity\ProviderRole')->findAll();

        return $this->render('provideruser/registration.html.twig',array('roles'=>$roles));
    }

    /**
     * @Route("/new-register/{vh}", name="registerasd", methods="GET")
     */
    public function newregister($vh){
        $em=$this->getDoctrine()->getManager();
        $roles=$em->getRepository('App\Entity\ProviderRole')->findAll();

        $emaillog=$em->getRepository('App\Entity\EmailLog')->findOneBy(array('verification_hass'=>$vh));
        $organization=null;
        $contact=null;
        if($emaillog!=null) {
            $organization=$emaillog->getOrganization();
            $contact=$emaillog->getContact();
        }

        return $this->render('user/registration.html.twig',array('roles'=>$roles,'organization'=>$organization,'contact'=>$contact));
    }

    /**
     * @Route("/recovery", name="provider_password_reset_request")
    */
    public function recovery(Request $request, UserPasswordEncoderInterface $encoder){
        $em=$this->getDoctrine()->getManager();
        $email = $request->get('email');

        if (!empty($email)) {
            $providerUser=$em->getRepository('App\Entity\ProviderUser')->findOneBy(array('email'=>$email));

            if($providerUser!=null){
                $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                $charactersLength = strlen($characters);
                $randomString = '';
                for ($i = 0; $i < 8; $i++) {
                    $randomString .= $characters[rand(0, $charactersLength - 1)];
                }

                $encode= $encoder->encodePassword($providerUser, $randomString);
                $providerUser->setPassword($encode);

                $em->persist($providerUser);
                $em->flush();

                $html="";
                $email_to=$email;

                $html.='<h3>Change password</h3>';
                $html.='<p>Use this temporal password for login</p>';
                $html.='<p><strong>New Password:</strong> '.$randomString.'</p>';
                $html.='<p>Click <a href="http://providers.bsnnet.com/provider/login">here</a> to go to the Provider portal</p>';
                $html.='<p>For additional information contact BSN at 305-907-7470 or email us at <a href="mailto:info@bsnnet.com">info@bsnnet.com</a></p>';

                $email = (new Email())
                    ->from('info@bsnnet.com')
                    ->to($email_to)
                    //->contrating_credentialing('contrating_credentialing@example.com')
                    //->bcc('bcc@example.com')
                    //->replyTo('fabien@example.com')
                    ->priority(Email::PRIORITY_HIGH)
                    ->subject('Recovery password for Provider Portal (BSN)')
                    ->html($html);

                $transport=new GmailTransport('infobsnnet', '314156aA@');
                $mailer=new Mailer($transport);
                $mailer->send($email);

                return $this->redirectToRoute('provider_login');

            }
        }


        return $this->render('security/recovery_provider.html.twig',array('email'=>$email));
    }


    /**
     * @Route("/profile_email_ckeck", name="provider_user_email_check")
    */
    public function checkEmail(Request $request){

        $em=$this->getDoctrine()->getManager();

        $email=$request->get('email');
        $exist=0;

        $providerUser=$em->getRepository('App\Entity\ProviderUser')->findOneBy(array('email'=>$email));

        if($providerUser){
            $exist=1;
        }

        return new Response(
            json_encode(array('exist' => $exist)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/provider_email_ckeck", name="admin_user_email_check")
     */
    public function checkEmailUser(Request $request){

        $em=$this->getDoctrine()->getManager();

        $email=$request->get('email');
        $exist=0;

        $providerUser=$em->getRepository('App\Entity\User')->findOneBy(array('email'=>$email));

        if($providerUser){
            $exist=1;
        }

        return new Response(
            json_encode(array('exist' => $exist)), 200, array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/profile_update", name="provider_user_profile_update")
    */
    public function  update_profile(Request $request,UserPasswordEncoderInterface $encoder){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $db = $em->getConnection();

            $role=$request->get('role');
            $firstname=$request->get('firstname');
            $lastname=$request->get('lastname');
            $username=$request->get('user_name');
            $email=$request->get('email');
            $old_password=$request->get('old_password');
            $new_password=$request->get('new_password');

            $user=$this->getUser();

            if($user){
                $user->setUsername($username);
                $user->setFirstName($firstname);
                $user->setLastName($lastname);
                $user->setEmail($email);

                $id=$user->getId();

                $sqlDelete="DELETE FROM `provideruser_roles` WHERE `provideruser_roles`.`user_id` = $id";
                $stmt = $db->prepare($sqlDelete);
                $stmt->execute();

                $rol=$em->getRepository('App\Entity\ProviderRole')->find($role);
                if($rol!==null){
                    $user->addRole($rol);
                }

                //change password
                if($new_password!=""){
                    $encode= $encoder->encodePassword($user, $new_password);
                    $user->setPassword($encode);
                }

                $em->persist($user);
                $em->flush();

                $this->addFlash(
                    'success',
                    'Your Profile has been updated successfully!'
                );

                return $this->redirectToRoute('provider_dashboard');
            }



        }else{
            return $this->redirectToRoute('provider_login');
        }
    }

}
