<?php

namespace App\Controller;

use App\Entity\Provider;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\CredentialingStatus;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\Reader;

/**
 * @Route("/admin/credentialingstatus")
 */
class CredentialingStatusController extends AbstractController{

    /**
     * @Route("/index", name="admin_credentialingstatus")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $credentialingstatuss = $em->getRepository('App\Entity\CredentialingStatus')->findAll();

            $delete_form_ajax = $this->createCustomForm('CREDENTIALINGSTATUS_ID', 'DELETE', 'admin_delete_credentialingstatus');


            return $this->render('credentialingstatus/index.html.twig', array('credentialingstatuss' => $credentialingstatuss, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_credentialingstatus")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('credentialingstatus/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_credentialingstatus", defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\CredentialingStatus')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_credentialingstatus');
            }

            return $this->render('credentialingstatus/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_credentialingstatus" , defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\CredentialingStatus')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_credentialingstatus');
            }

            return $this->render('credentialingstatus/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_credentialingstatus")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');

            $credentialingstatus = new CredentialingStatus();
            $credentialingstatus->setName($name);
            $credentialingstatus->setDescription($description);

            $em->persist($credentialingstatus);
            $em->flush();

            $this->addFlash(
                'success',
                'The Credentialing Status has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_credentialingstatus');
            }

            return $this->redirectToRoute('admin_credentialingstatus');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_credentialingstatus")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $description = $request->get('description');
            $new = $request->get('new');
            $id = $request->get('id');

            $credentialingstatus = $em->getRepository('App\Entity\CredentialingStatus')->find($id);

            if ($credentialingstatus == null) {
                $this->addFlash(
                    "danger",
                    "The Credentialing Status can't been updated!"
                );

                return $this->redirectToRoute('admin_credentialingstatus');
            }

            if ($credentialingstatus != null) {
                $credentialingstatus->setName($name);
                $credentialingstatus->setDescription($description);

                $em->persist($credentialingstatus);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "The Credentialing Status has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_credentialingstatus');
            }

            return $this->redirectToRoute('admin_credentialingstatus');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_credentialingstatus",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $credentialingstatus = $credentialingstatus = $em->getRepository('App\Entity\CredentialingStatus')->find($id);
            $removed = 0;
            $message = "";

            if ($credentialingstatus) {
                try {
                    $em->remove($credentialingstatus);
                    $em->flush();
                    $removed = 1;
                    $message = "The Credentialing Status has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Credentialing Status can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_credentialingstatus",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $credentialingstatus = $credentialingstatus = $em->getRepository('App\Entity\CredentialingStatus')->find($id);

                if ($credentialingstatus) {
                    try {
                        $em->remove($credentialingstatus);
                        $em->flush();
                        $removed = 1;
                        $message = "The Credentialing Status has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Credentialing Status can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }

}
