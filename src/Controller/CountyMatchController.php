<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\CountyMatch;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/admin/countymatch")
 */
class CountyMatchController extends AbstractController{

    /**
     * @Route("/index", name="admin_countymatch")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $countymatchs = $em->getRepository('App\Entity\CountyMatch')->findAll();
            $delete_form_ajax = $this->createCustomForm('COUNTYMATCH_ID', 'DELETE', 'admin_delete_countymatch');

            return $this->render('countymatch/index.html.twig', array('countymatchs' => $countymatchs, 'delete_form_ajax' => $delete_form_ajax->createView()));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new", name="admin_new_countymatch")
     */
    public function add(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            return $this->render('countymatch/add.html.twig');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_countymatch",defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\CountyMatch')->find($id);

            if ($document == null) {
                return $this->redirectToRoute('admin_countymatch');
            }

            return $this->render('countymatch/edit.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_countymatch",defaults={"id": null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\CountyMatch')->find($id);

            if ($document == null) {
                //create a flash message

                return $this->redirectToRoute('admin_countymatch');
            }

            return $this->render('countymatch/view.html.twig', array('document' => $document));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/create", name="admin_create_countymatch")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $county_code = $request->get('county_code');
            $region = $request->get('region');
            $new = $request->get('new');
            $zone= $request->get('zone');

            $countymatch = new CountyMatch();
            $countymatch->setName($name);
            $countymatch->setCountyCode($county_code);
            $countymatch->setRegion($region);
            $countymatch->setZone($zone);

            $em->persist($countymatch);
            $em->flush();

            $this->addFlash(
                'success',
                'County has been saved successfully!'
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_countymatch');
            }

            return $this->redirectToRoute('admin_countymatch');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_countymatch")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $name = $request->get('name');
            $county_code = $request->get('county_code');
            $region = $request->get('region');
            $new = $request->get('new');
            $id = $request->get('id');
            $zone = $request->get('zone');

            $countymatch = $em->getRepository('App\Entity\CountyMatch')->find($id);

            if ($countymatch == null) {
                $this->addFlash(
                    "danger",
                    "The County can't been updated!"
                );

                return $this->redirectToRoute('admin_countymatch');
            }

            if ($countymatch != null) {
                $countymatch->setName($name);
                $countymatch->setCountyCode($county_code);
                $countymatch->setRegion($region);
                $countymatch->setZone($zone);

                $em->persist($countymatch);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "County has been updated successfully!"
            );

            if ($new == 1) {
                return $this->redirectToRoute('admin_new_countymatch');
            }

            return $this->redirectToRoute('admin_countymatch');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_countymatch",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $countymatch = $countymatch = $em->getRepository('App\Entity\CountyMatch')->find($id);
            $removed = 0;
            $message = "";

            if ($countymatch) {
                try {
                    $em->remove($countymatch);
                    $em->flush();
                    $removed = 1;
                    $message = "The County has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The countymatch can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_countymatch",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $countymatch = $countymatch = $em->getRepository('App\Entity\CountyMatch')->find($id);

                if ($countymatch) {
                    try {
                        $em->remove($countymatch);
                        $em->flush();
                        $removed = 1;
                        $message = "The CountyMatch has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The County can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
