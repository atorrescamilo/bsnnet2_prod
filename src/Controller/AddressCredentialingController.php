<?php

namespace App\Controller;

use App\Entity\EmailQueued;
use App\Entity\Organization;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\BillingAddressCvo;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


/**
 * @Route("/admin/address_credentialing")
 */
class AddressCredentialingController extends AbstractController{

    /**
     * @Route("/list", name="admin_address_credentialing_list")
     */
    public function index(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $credentialings = $em->getRepository('App\Entity\BillingAddressCvo')->findAll();



            $delete_form_ajax = $this->createCustomForm('PROVIDER_ID', 'DELETE', 'admin_delete_provider');

            return $this->render('billingaddress_credentialing/index.html.twig', array(
                'credentialings' => $credentialings,'delete_form_ajax' => $delete_form_ajax->createView()
            ));

        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/organization_list", name="admin_address_credentialing_list")
     */
    public function list(){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $conn = $em->getConnection();

            $sql="SELECT ba.id, organization.name as organization_name, ba.street, ba.region, ba.disabled, 
            IF(SUM(IF(ba.is_facility, 1, 0)) > 0, 'Yes', 'No') AS is_facility,
            GROUP_CONCAT(DISTINCT credentialing_status.name ORDER BY credentialing_status.id ASC SEPARATOR ', ') AS status
            FROM  billing_address ba
            LEFT JOIN organization ON organization.id = ba.organization_id    
            LEFT JOIN billing_address_cvo ON billing_address_cvo.address_id = ba.id
            LEFT JOIN credentialing_status ON credentialing_status.id = billing_address_cvo.credentialing_status_id
            WHERE ba.disabled=0
            GROUP BY ba.id
            ORDER BY ba.id";

            $sql="SELECT o.id, o.name
            FROM  organization o
            WHERE ba.disabled=0
            GROUP BY ba.id
            ORDER BY ba.id";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $billing_address= $stmt->fetchAll();

            $credentialings = $em->getRepository('App\Entity\BillingAddressCvo')->findAll();
            $delete_form_ajax = $this->createCustomForm('PROVIDER_ID', 'DELETE', 'admin_delete_provider');

            return $this->render('organization_credentialing/index.html.twig', array(
                'credentialings' => $credentialings, 'billing_address'=>$billing_address ,'delete_form_ajax' => $delete_form_ajax->createView()
            ));

        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/new/{addr}", name="admin_new_address_credentialing",defaults={"addr": null})
     */
    public function add($addr){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $credentialingStatus = $em->getRepository('App\Entity\CredentialingStatus')->findAll();
            $cvos = $em->getRepository('App\Entity\Cvo')->findAll();
            $payers = $em->getRepository('App\Entity\Payer')->findAll();

            $addrObj = $em->getRepository('App\Entity\BillingAddress')->find($addr);

            return $this->render('billingaddress_credentialing/add.html.twig', array(
                'credentialingstatus' => $credentialingStatus,
                'cvos' => $cvos,
                'payers' => $payers,
                'addr' => $addrObj
            ));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/edit/{id}", name="admin_edit_address_credentialing",defaults={"id": null})
     */
    public function edit($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\BillingAddressCvo')->find($id);

            $credentialingStatus = $em->getRepository('App\Entity\CredentialingStatus')->findAll();
            $cvos = $em->getRepository('App\Entity\Cvo')->findAll();
            $payers = $em->getRepository('App\Entity\Payer')->findAll();

            $addrObj = $document->getBillingAddress();

            return $this->render('billingaddress_credentialing/edit.html.twig', array('document' => $document,
                'credentialingstatus' => $credentialingStatus,
                'cvos' => $cvos,
                'payers' => $payers,
                'addr' => $addrObj));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/edit2/{id}", name="admin_edit2_address_credentialing",defaults={"id": null})
     */
    public function edit2($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $document = $em->getRepository('App\Entity\BillingAddressCvo')->find($id);

            $credentialingStatus = $em->getRepository('App\Entity\CredentialingStatus')->findAll();
            $cvos = $em->getRepository('App\Entity\Cvo')->findAll();
            $payers = $em->getRepository('App\Entity\Payer')->findAll();

            $addrObj = $document->getBillingAddress();

            return $this->render('billingaddress_credentialing/edit2.html.twig', array('document' => $document,
                'credentialingstatus' => $credentialingStatus,
                'cvos' => $cvos,
                'payers' => $payers,
                'addr' => $addrObj));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view/{id}", name="admin_view_address_credentialing",defaults={"id": null,"org":null})
     */
    public function view($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $document = $em->getRepository('App\Entity\BillingAddressCvo')->find($id);

            $addrObj = $document->getBillingAddress();
            return $this->render('billingaddress_credentialing/view.html.twig', array('document' => $document,'addr' => $addrObj));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/view2/{id}", name="admin_view2_address_credentialing",defaults={"id": null,"org":null})
     */
    public function view2($id){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $document = $em->getRepository('App\Entity\BillingAddressCvo')->find($id);

            $addrObj = $document->getBillingAddress();
            return $this->render('billingaddress_credentialing/view2.html.twig', array('document' => $document,'addr' => $addrObj));
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }


    /**
     * @Route("/create", name="admin_create_address_credentialing")
     */
    public function create(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $credentialing_status = $request->get('credentialing_status');
            $cvo = $request->get('cvo');
            $payers = $request->get('payers');
            $addr = $request->get('addr');
            $sumitted_to_cvo = $request->get('sumitted_to_cvo');
            $app_received_date = $request->get('app_received_date');
            $complete_date = $request->get('complete_date');
            $saction_date = $request->get('saction_date');
            $credentialing_accepted_date=$request->get('credentialing_accepted_date');
            $credentialing_denied_date=$request->get('credentialing_denied_date');

            $address_Credentialing = new BillingAddressCvo();

            $credentialingStatusObj = $em->getRepository('App\Entity\CredentialingStatus')->find($credentialing_status);
            $cvoObj = $em->getRepository('App\Entity\Cvo')->find($cvo);
            $addressObj = $em->getRepository('App\Entity\BillingAddress')->find($addr);

            if ($credentialingStatusObj != null) {
                $address_Credentialing->setCredentialingStatus($credentialingStatusObj);
            }

            if ($cvoObj != null) {
                $address_Credentialing->setCvo($cvoObj);
            }

            if ($addressObj != null) {
                $address_Credentialing->setBillingAddress($addressObj);
            }

            if($payers!=""){
                foreach ($payers as $payer){
                    $payerObj=$em->getRepository('App\Entity\Payer')->find($payer);
                    if($payerObj!=null){
                        $address_Credentialing->addPayer($payerObj);
                    }
                }
            }

            $address_Credentialing->setCredentialingDate($sumitted_to_cvo);
            $address_Credentialing->setCredentialingReceivedDate($app_received_date);
            $address_Credentialing->setCredentialingCompleteDate($complete_date);
            $address_Credentialing->setSanctionDate($saction_date);
            $address_Credentialing->setCredentialingAcceptedDate($credentialing_accepted_date);
            $address_Credentialing->setCredentialingDeniedDate($credentialing_denied_date);

            if($credentialing_accepted_date!=""){
                //set the effective date
                $dateS=explode("/",$credentialing_accepted_date);
                $yearAccepted=$dateS[2];
                $mothAccepted=$dateS[0];
                $newDateEffectiveDate="";

                if($mothAccepted=='01'){
                    $newDateEffectiveDate="02/01/".$yearAccepted;
                }
                if($mothAccepted=='02'){
                    $newDateEffectiveDate="03/01/".$yearAccepted;
                }
                if($mothAccepted=='03'){
                    $newDateEffectiveDate="04/01/".$yearAccepted;
                }
                if($mothAccepted=='04'){
                    $newDateEffectiveDate="05/01/".$yearAccepted;
                }
                if($mothAccepted=='05'){
                    $newDateEffectiveDate="06/01/".$yearAccepted;
                }
                if($mothAccepted=='06'){
                    $newDateEffectiveDate="07/01/".$yearAccepted;
                }
                if($mothAccepted=='07'){
                    $newDateEffectiveDate="08/01/".$yearAccepted;
                }
                if($mothAccepted=='08'){
                    $newDateEffectiveDate="09/01/".$yearAccepted;
                }
                if($mothAccepted=='09'){
                    $newDateEffectiveDate="10/01/".$yearAccepted;
                }
                if($mothAccepted=='10'){
                    $newDateEffectiveDate="11/01/".$yearAccepted;
                }
                if($mothAccepted=='11'){
                    $newDateEffectiveDate="12/01/".$yearAccepted;
                }
                if($mothAccepted=='12'){
                    $newDateEffectiveDate="01/01/".$yearAccepted+1;
                }

                $address_Credentialing->setCredentialingEffectiveDate($newDateEffectiveDate);
            }

            $em->persist($address_Credentialing);
            $em->flush();

            $idOrg = $addressObj->getOrganization()->getId();

            //create a email queue for credentialing notification

            $organization=$addressObj->getOrganization();
            if($organization!=null){
                $contacts=$em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));
                $status=$em->getRepository('App\Entity\EmailStatus')->find(1);
                $template=$em->getRepository('App\Entity\EmailType')->find(4);
                $date=date('m/d/Y');
                if($credentialing_status ==4){
                    if($contacts!=null){
                        foreach ($contacts as $contact){
                            $email=$contact->getEmail();
                            if($email!="" and $email!=null){
                                $emailQ=new EmailQueued();

                                $emailQ->setSentTo($email);
                                $emailQ->setContact($contact);
                                $emailQ->setStatus($status);
                                $emailQ->setEmailType($template);
                                $emailQ->setProviders(null);
                                $emailQ->setDateToSend(new \DateTime($date));
                                $emailQ->setOrganization($organization);
                                $emailQ->setApprovedDate($credentialing_accepted_date);

                                $em->persist($emailQ);
                                $em->flush();
                            }
                        }
                    }
                }
            }

            return $this->redirectToRoute('admin_view_billing', ['id' => $addr, 'org' => $idOrg]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update", name="admin_update_address_credentialing")
     */
    public function update(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $credentialing_status = $request->get('credentialing_status');
            $cvo = $request->get('cvo');
            $payers = $request->get('payers');
            $addr = $request->get('addr');
            $sumitted_to_cvo = $request->get('sumitted_to_cvo');
            $app_received_date = $request->get('app_received_date');
            $complete_date = $request->get('complete_date');
            $saction_date = $request->get('saction_date');
            $credentialing_accepted_date=$request->get('credentialing_accepted_date');
            $credentialing_denied_date=$request->get('credentialing_denied_date');
            $new = $request->get('new');
            $id = $request->get('id');

            $address_Credentialing = $em->getRepository('App\Entity\BillingAddressCvo')->find($id);

            if ($address_Credentialing == null) {
                $this->addFlash(
                    "danger",
                    "The Facility Credentialing can't been updated!"
                );

                return $this->redirectToRoute('admin_address_credentialing');
            }

            if ( $address_Credentialing != null) {

                $credentialingStatusObj = $em->getRepository('App\Entity\CredentialingStatus')->find($credentialing_status);
                $cvoObj = $em->getRepository('App\Entity\Cvo')->find($cvo);
                $addressObj = $em->getRepository('App\Entity\BillingAddress')->find($addr);

                if ($credentialingStatusObj != null) {
                    $address_Credentialing->setCredentialingStatus($credentialingStatusObj);
                }

                if ($cvoObj != null) {
                    $address_Credentialing->setCvo($cvoObj);
                }

                if ($addressObj != null) {
                    $address_Credentialing->setBillingAddress($addressObj);
                }

                $actualPayers=$address_Credentialing->getPayers();
                if($actualPayers!=null){
                    foreach ($actualPayers as $actualPayer){
                        $address_Credentialing->removePayer($actualPayer);
                    }
                }

                if($payers!=""){
                    foreach ($payers as $payer){
                        $payerObj=$em->getRepository('App\Entity\Payer')->find($payer);
                        if($payerObj!=null){
                            $address_Credentialing->addPayer($payerObj);
                        }
                    }
                }

                if($credentialing_accepted_date!=""){
                    //set the effective date
                    $dateS=explode("/",$credentialing_accepted_date);
                    $yearAccepted=$dateS[2];
                    $mothAccepted=$dateS[0];
                    $newDateEffectiveDate="";

                    if($mothAccepted=='01'){
                        $newDateEffectiveDate="02/01/".$yearAccepted;
                    }
                    if($mothAccepted=='02'){
                        $newDateEffectiveDate="03/01/".$yearAccepted;
                    }
                    if($mothAccepted=='03'){
                        $newDateEffectiveDate="04/01/".$yearAccepted;
                    }
                    if($mothAccepted=='04'){
                        $newDateEffectiveDate="05/01/".$yearAccepted;
                    }
                    if($mothAccepted=='05'){
                        $newDateEffectiveDate="06/01/".$yearAccepted;
                    }
                    if($mothAccepted=='06'){
                        $newDateEffectiveDate="07/01/".$yearAccepted;
                    }
                    if($mothAccepted=='07'){
                        $newDateEffectiveDate="08/01/".$yearAccepted;
                    }
                    if($mothAccepted=='08'){
                        $newDateEffectiveDate="09/01/".$yearAccepted;
                    }
                    if($mothAccepted=='09'){
                        $newDateEffectiveDate="10/01/".$yearAccepted;
                    }
                    if($mothAccepted=='10'){
                        $newDateEffectiveDate="11/01/".$yearAccepted;
                    }
                    if($mothAccepted=='11'){
                        $newDateEffectiveDate="12/01/".$yearAccepted;
                    }
                    if($mothAccepted=='12'){
                        $newDateEffectiveDate="01/01/".$yearAccepted+1;
                    }

                    $address_Credentialing->setCredentialingEffectiveDate($newDateEffectiveDate);
                }

                $address_Credentialing->setCredentialingDate($sumitted_to_cvo);
                $address_Credentialing->setCredentialingReceivedDate($app_received_date);
                $address_Credentialing->setCredentialingCompleteDate($complete_date);
                $address_Credentialing->setSanctionDate($saction_date);
                $address_Credentialing->setCredentialingAcceptedDate($credentialing_accepted_date);
                $address_Credentialing->setCredentialingDeniedDate($credentialing_denied_date);

                $em->persist($address_Credentialing);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "The Facility Credentiliang has been updated successfully!"
            );

            $idOrg = $addressObj->getOrganization()->getId();
            return $this->redirectToRoute('admin_address_credentialing_list');
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/update2", name="admin_update2_address_credentialing")
     */
    public function update2(Request $request){
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $credentialing_status = $request->get('credentialing_status');
            $cvo = $request->get('cvo');
            $payers = $request->get('payers');
            $addr = $request->get('addr');
            $sumitted_to_cvo = $request->get('sumitted_to_cvo');
            $app_received_date = $request->get('app_received_date');
            $complete_date = $request->get('complete_date');
            $saction_date = $request->get('saction_date');
            $credentialing_accepted_date=$request->get('credentialing_accepted_date');
            $credentialing_denied_date=$request->get('credentialing_denied_date');
            $new = $request->get('new');
            $id = $request->get('id');

            $address_Credentialing = $em->getRepository('App\Entity\BillingAddressCvo')->find($id);

            if ($address_Credentialing == null) {
                $this->addFlash(
                    "danger",
                    "The Facility Credentialing can't been updated!"
                );

                return $this->redirectToRoute('admin_address_credentialing');
            }

            if ( $address_Credentialing != null) {

                $credentialingStatusObj = $em->getRepository('App\Entity\CredentialingStatus')->find($credentialing_status);
                $cvoObj = $em->getRepository('App\Entity\Cvo')->find($cvo);
                $addressObj = $em->getRepository('App\Entity\BillingAddress')->find($addr);

                if ($credentialingStatusObj != null) {
                    $address_Credentialing->setCredentialingStatus($credentialingStatusObj);
                }

                if ($cvoObj != null) {
                    $address_Credentialing->setCvo($cvoObj);
                }

                if ($addressObj != null) {
                    $address_Credentialing->setBillingAddress($addressObj);
                }

                $actualPayers=$address_Credentialing->getPayers();
                if($actualPayers!=null){
                    foreach ($actualPayers as $actualPayer){
                        $address_Credentialing->removePayer($actualPayer);
                    }
                }

                if($payers!=""){
                    foreach ($payers as $payer){
                        $payerObj=$em->getRepository('App\Entity\Payer')->find($payer);
                        if($payerObj!=null){
                            $address_Credentialing->addPayer($payerObj);
                        }
                    }
                }

                if($credentialing_accepted_date!=""){
                    //set the effective date
                    $dateS=explode("/",$credentialing_accepted_date);
                    $yearAccepted=$dateS[2];
                    $mothAccepted=$dateS[0];
                    $newDateEffectiveDate="";

                    if($mothAccepted=='01'){
                        $newDateEffectiveDate="02/01/".$yearAccepted;
                    }
                    if($mothAccepted=='02'){
                        $newDateEffectiveDate="03/01/".$yearAccepted;
                    }
                    if($mothAccepted=='03'){
                        $newDateEffectiveDate="04/01/".$yearAccepted;
                    }
                    if($mothAccepted=='04'){
                        $newDateEffectiveDate="05/01/".$yearAccepted;
                    }
                    if($mothAccepted=='05'){
                        $newDateEffectiveDate="06/01/".$yearAccepted;
                    }
                    if($mothAccepted=='06'){
                        $newDateEffectiveDate="07/01/".$yearAccepted;
                    }
                    if($mothAccepted=='07'){
                        $newDateEffectiveDate="08/01/".$yearAccepted;
                    }
                    if($mothAccepted=='08'){
                        $newDateEffectiveDate="09/01/".$yearAccepted;
                    }
                    if($mothAccepted=='09'){
                        $newDateEffectiveDate="10/01/".$yearAccepted;
                    }
                    if($mothAccepted=='10'){
                        $newDateEffectiveDate="11/01/".$yearAccepted;
                    }
                    if($mothAccepted=='11'){
                        $newDateEffectiveDate="12/01/".$yearAccepted;
                    }
                    if($mothAccepted=='12'){
                        $newDateEffectiveDate="01/01/".$yearAccepted+1;
                    }

                    $address_Credentialing->setCredentialingEffectiveDate($newDateEffectiveDate);
                }

                $address_Credentialing->setCredentialingDate($sumitted_to_cvo);
                $address_Credentialing->setCredentialingReceivedDate($app_received_date);
                $address_Credentialing->setCredentialingCompleteDate($complete_date);
                $address_Credentialing->setSanctionDate($saction_date);
                $address_Credentialing->setCredentialingAcceptedDate($credentialing_accepted_date);
                $address_Credentialing->setCredentialingDeniedDate($credentialing_denied_date);

                $em->persist($address_Credentialing);
                $em->flush();
            }

            $this->addFlash(
                "success",
                "The Facility Credentiliang has been updated successfully!"
            );

            $idOrg = $addressObj->getOrganization()->getId();
            return $this->redirectToRoute('admin_view_billing', ['id' => $addr, 'org' => $idOrg]);
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete/{id}", name="admin_delete_address_credentialing",methods={"POST","DELETE"})
     */
    public function deleteAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();
            $id = $request->get('id');

            $address_credentialing = $address_credentialing = $em->getRepository('App\Entity\Comorbidity')->find($id);
            $removed = 0;
            $message = "";

            if ($address_credentialing) {
                try {
                    $em->remove($address_credentialing);
                    $em->flush();
                    $removed = 1;
                    $message = "The Address Credentialing has been Successfully removed";
                } catch (Exception $ex) {
                    $removed = 0;
                    $message = "The Address Credentialing can't be removed";
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    /**
     * @Route("/delete_multiple", name="admin_delete_multiple_address_credentialing",methods={"POST","DELETE"})
     */
    public function deleteMultipleAction(Request $request) {
        if ($this->isGranted('IS_AUTHENTICATED_FULLY')) {
            $em = $this->getDoctrine()->getManager();

            $ids = $request->get('ids');
            $removed = 0;
            $message = "";

            foreach ($ids as $id) {
                $address_credentialing = $address_credentialing = $em->getRepository('App\Entity\Comorbidity')->find($id);

                if ($address_credentialing) {
                    try {
                        $em->remove($address_credentialing);
                        $em->flush();
                        $removed = 1;
                        $message = "The Address Credentialing has been Successfully removed";
                    } catch (Exception $ex) {
                        $removed = 0;
                        $message = "The Address Credentialing can't be removed";
                    }
                }
            }

            return new Response(
                json_encode(array('removed' => $removed, 'message' => $message)), 200, array('Content-Type' => 'application/json')
            );
        }else{
            return $this->redirectToRoute('admin_login');
        }
    }

    private function createCustomForm($id, $method, $route) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl($route, array('id' => $id)))
            ->setMethod($method)
            ->getForm();
    }


}
