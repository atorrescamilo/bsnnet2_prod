-- noinspection SqlNoDataSourceInspectionForFile

CREATE OR REPLACE
ALGORITHM = UNDEFINED
VIEW `v_organization`
AS SELECT
`organization`.*,
COUNT(DISTINCT `provider`.`id`) AS `provider_count`,
GROUP_CONCAT(DISTINCT `organization_contact`.`name` ORDER BY `organization_contact`.`name` ASC SEPARATOR ', ') AS `contact_names`,
GROUP_CONCAT(DISTINCT `organization_contact`.`email` ORDER BY `organization_contact`.`email` ASC SEPARATOR ', ') AS `contact_emails`,
GROUP_CONCAT(DISTINCT `org_specialty`.`name` ORDER BY `org_specialty`.`name` ASC SEPARATOR ', ') AS `org_clasification`,
GROUP_CONCAT(DISTINCT `specialty_areas`.`name` ORDER BY `specialty_areas`.`name` ASC SEPARATOR ', ') AS `specialty_areas`,
GROUP_CONCAT(DISTINCT `languages`.`name` ORDER BY `languages`.`name` ASC SEPARATOR ', ') AS `languages`,
GROUP_CONCAT(DISTINCT `languages`.`id` ORDER BY `languages`.`id` ASC SEPARATOR ', ') AS `languages_id`,
GROUP_CONCAT(DISTINCT `taxonomy_code`.`specialization` ORDER BY `taxonomy_code`.`id` ASC SEPARATOR ', ') AS `taxonomies_specialty`,
GROUP_CONCAT(DISTINCT `payer`.`name` ORDER BY `payer`.`name` ASC SEPARATOR ', ') AS `payer_str`
FROM `organization`
LEFT JOIN `provider` ON `provider`.`org_id` = `organization`.`id`
LEFT JOIN `organization_taxonomy` ON `organization_taxonomy`.`organization_id` = `organization`.`id`
LEFT JOIN `provider_languages` ON `provider_languages`.`provider_id` = `provider`.`id`
LEFT JOIN `taxonomy_code` ON `taxonomy_code`.`id` = `organization_taxonomy`.`taxonomy_id`
LEFT JOIN `provider_payer` ON `provider_payer`.`provider_id` = `provider`.`id`
LEFT JOIN `payer` ON `payer`.`id` = `provider_payer`.`payer_id`
LEFT JOIN `languages` ON `languages`.`id` = `provider_languages`.`languages_id`
LEFT JOIN `organization_contact` ON `organization`.`id` = `organization_contact`.`organization_id`
LEFT JOIN `organization_org_specialties` ON `organization_org_specialties`.organization_id = `organization`.`id`
LEFT JOIN `org_specialty` ON `org_specialty`.`id` = `organization_org_specialties`.`org_specialty_id`
LEFT JOIN `organization_specialty_areas` ON `organization_specialty_areas`.organization_id = `organization`.`id`
LEFT JOIN `specialty_areas` ON `specialty_areas`.`id` = `organization_specialty_areas`.`specialtyarea_id`
GROUP BY `organization`.`id`;

CREATE OR REPLACE
 ALGORITHM = UNDEFINED
 VIEW `v_provider`
 AS SELECT
 `provider`.*,
 COUNT(DISTINCT `billing_address`.`id`) AS `billing_address_count`,
 GROUP_CONCAT(DISTINCT `degree`.`name` ORDER BY `degree`.`name` ASC SEPARATOR ', ') AS `degrees`,
 CONCAT(`provider`.`first_name`, ' ', `provider`.`last_name`) AS `full_name`,
 GROUP_CONCAT(DISTINCT `provider_type`.`name` ORDER BY `provider_type`.`name` ASC SEPARATOR ', ') AS `provider_types`,
 GROUP_CONCAT(DISTINCT `organization`.`name` ORDER BY `organization`.`name` ASC SEPARATOR ', ') AS `organizationsstr`,
 GROUP_CONCAT(DISTINCT `organization`.`tin_number` ORDER BY `organization`.`tin_number` ASC SEPARATOR ', ') AS `tin_numbers`,
 GROUP_CONCAT(DISTINCT `organization`.`id` ORDER BY `organization`.`name` ASC SEPARATOR ', ') AS `organizations`,
 GROUP_CONCAT(DISTINCT `organization`.`phone` ORDER BY `organization`.`phone` ASC SEPARATOR ', ') AS `phone_number`,
 GROUP_CONCAT(DISTINCT `languages`.`name` ORDER BY `languages`.`name` ASC SEPARATOR ', ') AS `languages_str`,
 GROUP_CONCAT(DISTINCT `payer`.`id` ORDER BY `payer`.`id` ASC SEPARATOR ',') AS `payers`,
 GROUP_CONCAT(DISTINCT `payer`.`alias` ORDER BY `payer`.`alias` ASC SEPARATOR ',') AS `payers_name`,
 GROUP_CONCAT(DISTINCT `specialty`.`name` ORDER BY `specialty`.`name` ASC SEPARATOR ', ') AS `specialties`,
 GROUP_CONCAT(DISTINCT `taxonomy_code`.`code` ORDER BY `taxonomy_code`.`code` ASC SEPARATOR ', ') AS `taxonomy_codes_code`,
 GROUP_CONCAT(DISTINCT `provider_credentialing`.`cvo_id` ORDER BY `provider_credentialing`.`id` ASC SEPARATOR ', ') AS `cvos`,
 GROUP_CONCAT(DISTINCT `provider_credentialing`.`credentialing_accepted` ORDER BY `provider_credentialing`.`id` ASC SEPARATOR ' ') AS `decision_date`,
 GROUP_CONCAT(DISTINCT `credentialing_status`.`name` ORDER BY `credentialing_status`.`name` ASC SEPARATOR ', ') AS `status_cred`,
 GROUP_CONCAT(DISTINCT `organization_credentialing`.`credentialing_status_id` ORDER BY `organization_credentialing`.`credentialing_status_id` ASC SEPARATOR ', ') AS `status_cred_facility`,
 GROUP_CONCAT(DISTINCT `billing_address`.`region` ORDER BY `billing_address`.`region` ASC SEPARATOR ', ') AS `regions`,
 GROUP_CONCAT(DISTINCT `organization_contact`.`email` ORDER BY `organization_contact`.`email` ASC SEPARATOR ', ') AS `contact_emails`,
 GROUP_CONCAT(DISTINCT `taxonomy_code`.`specialization` ORDER BY `taxonomy_code`.`id` ASC SEPARATOR ', ') AS `taxonomies_specialty`
FROM `provider`
LEFT JOIN `provider_credentialing` ON `provider_credentialing`.`npi_number` = `provider`.`npi_number`
LEFT JOIN `credentialing_status` ON `provider_credentialing`.`credentialing_status_id` = `credentialing_status`.`id`
LEFT JOIN `provider_degree` ON `provider_degree`.provider_id = `provider`.`id`
LEFT JOIN `degree` ON `degree`.`id` = `provider_degree`.`degree_id`
LEFT JOIN `provider_provider_types` ON `provider_provider_types`.provider_id = `provider`.`id`
LEFT JOIN `provider_type` ON `provider_type`.`id` = `provider_provider_types`.`provider_type_id`
LEFT JOIN `organization` ON `organization`.`id` = `provider`.`org_id`
LEFT JOIN `organization_contact` ON `organization`.`id` = `organization_contact`.`organization_id`
LEFT JOIN `provider_languages` ON `provider_languages`.provider_id = `provider`.`id`
LEFT JOIN `languages` ON `languages`.`id` = `provider_languages`.`languages_id`
LEFT JOIN `provider_payer` ON `provider_payer`.provider_id = `provider`.`id`
LEFT JOIN `payer` ON `payer`.`id` = `provider_payer`.`payer_id`
LEFT JOIN `provider_primary_specialties` ON `provider_primary_specialties`.provider_id = `provider`.`id`
LEFT JOIN `specialty` ON `specialty`.`id` = `provider_primary_specialties`.`primary_specialty_id`
LEFT JOIN `provider_taxonomy_code` ON `provider_taxonomy_code`.provider_id = `provider`.`id`
LEFT JOIN `taxonomy_code` ON `taxonomy_code`.`id` = `provider_taxonomy_code`.`taxonomy_id`
LEFT JOIN `provider_billing_address` ON `provider_billing_address`.`provider_id` = `provider`.`id`
LEFT JOIN `billing_address` ON `billing_address`.`id` = `provider_billing_address`.`billing_address_id`
LEFT JOIN `organization_credentialing` ON `organization_credentialing`.`organization_id` = `organization`.`id`
GROUP BY `provider`.`id`;

CREATE OR REPLACE
 ALGORITHM = UNDEFINED
 VIEW `v_billing_address`
 AS SELECT
 `billing_address`.*,
 IF(`credentialing_complete_date`, true, false) AS is_credentialed,
 `organization`.`name` AS `organization_name`,
FROM `billing_address`
INNER JOIN `organization` ON `organization`.`id` = `billing_address`.`organization_id`
GROUP BY `billing_address`.`id`;

/* SQL View for Credentialing*/
CREATE OR REPLACE
ALGORITHM = UNDEFINED
VIEW `v_credentialing`
AS SELECT
`provider_credentialing`.*,
GROUP_CONCAT(DISTINCT `degree`.`name` ORDER BY `degree`.`name` ASC SEPARATOR ', ') AS `degrees`,
IF(SUM(IF(`billing_address`.`is_facility`, 1, 0)) > 0, true, false) AS `is_facility`
FROM `provider_credentialing`
INNER JOIN `provider` ON `provider`.`id` = `provider_credentialing`.`provider_id`
LEFT JOIN `provider_degree` ON `provider_degree`.`provider_id` = `provider`.`id`
LEFT JOIN `provider_billing_address` ON `provider_billing_address`.`provider_id` = `provider`.`id`
LEFT JOIN `degree` ON `degree`.`id` = `provider_degree`.`degree_id`
LEFT JOIN `billing_address` ON `billing_address`.`id` = `provider_billing_address`.`billing_address_id`
GROUP BY `provider_credentialing`.`id`;

CREATE OR REPLACE
ALGORITHM = UNDEFINED
VIEW `v_provider_payer`
AS SELECT
`provider`.*,
GROUP_CONCAT(DISTINCT `degree`.`name` ORDER BY `degree`.`name` ASC SEPARATOR ', ') AS `degrees`,
GROUP_CONCAT(DISTINCT `provider_type`.`name` ORDER BY `provider_type`.`name` ASC SEPARATOR ', ') AS `provider_types`,
GROUP_CONCAT(DISTINCT `organization`.`phone` ORDER BY `organization`.`phone` ASC SEPARATOR ', ') AS `phone_number`,
GROUP_CONCAT(DISTINCT `languages`.`name` ORDER BY `languages`.`name` ASC SEPARATOR ', ') AS `languages_str`,
GROUP_CONCAT(DISTINCT `specialty`.`name` ORDER BY `specialty`.`name` ASC SEPARATOR ', ') AS `specialties`,
GROUP_CONCAT(DISTINCT `organization`.`name` ORDER BY `organization`.`name` ASC SEPARATOR ', ') AS `organizationsstr`,
GROUP_CONCAT(DISTINCT `taxonomy_code`.`specialization` ORDER BY `taxonomy_code`.`id` ASC SEPARATOR ', ') AS `taxonomies_specialty`,
GROUP_CONCAT(DISTINCT `taxonomy_code`.`id` ORDER BY `taxonomy_code`.`id` ASC SEPARATOR ', ') AS `taxonomies`,
GROUP_CONCAT(DISTINCT `payer`.`id` ORDER BY `payer`.`id` ASC SEPARATOR ', ') AS `payers`,
CONCAT(`provider`.`first_name`, ' ', `provider`.`last_name`) AS `full_name`

FROM `provider`
LEFT JOIN `provider_degree` ON `provider_degree`.provider_id = `provider`.`id`
LEFT JOIN `degree` ON `degree`.`id` = `provider_degree`.`degree_id`
LEFT JOIN `provider_provider_types` ON `provider_provider_types`.provider_id = `provider`.`id`
LEFT JOIN `provider_type` ON `provider_type`.`id` = `provider_provider_types`.`provider_type_id`
LEFT JOIN `organization` ON `organization`.`id` = `provider`.`org_id`
LEFT JOIN `provider_languages` ON `provider_languages`.provider_id = `provider`.`id`
LEFT JOIN `languages` ON `languages`.`id` = `provider_languages`.`languages_id`
LEFT JOIN `provider_primary_specialties` ON `provider_primary_specialties`.provider_id = `provider`.`id`
LEFT JOIN `specialty` ON `specialty`.`id` = `provider_primary_specialties`.`primary_specialty_id`
LEFT JOIN `organization_taxonomy` ON `organization_taxonomy`.`organization_id` = `organization`.`id`
LEFT JOIN `taxonomy_code` ON `taxonomy_code`.`id` = `organization_taxonomy`.`taxonomy_id`
LEFT JOIN `provider_payer` ON `provider_payer`.provider_id = `provider`.`id`
LEFT JOIN `payer` ON `payer`.`id` = `provider_payer`.`payer_id`

WHERE `provider`.`disabled`=0
GROUP BY `provider`.`id`;


