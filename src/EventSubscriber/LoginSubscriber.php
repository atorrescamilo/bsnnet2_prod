<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Doctrine\ORM\EntityManagerInterface;


class LoginSubscriber implements EventSubscriberInterface
{

    private $em;

    public function __construct(
        EntityManagerInterface $em) {
        $this->em = $em;
    }

    public function onAuthenticationEvent(AuthenticationEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();

        if($user){
            if($user instanceof UserInterface){
                $user->updateLastLogin(new \DateTime());
                $this->em->persist($user);
                $this->em->flush();
            }
        }
    }

    public static function getSubscribedEvents()
    {
        return [
            AuthenticationEvent::class => 'onAuthenticationEvent',
        ];
    }
}
