<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CustomerCountyFileUploader
{
    private $targetDirectory;

    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(UploadedFile $file, $name)
    {

        $fileName = $name . '.' . $file->guessExtension();
        $directory= $this->getTargetDirectory()."/";

        try {
            $file->move($directory, $fileName);
        } catch (FileException $e) {
            return "error";
        }

        return $fileName;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}
