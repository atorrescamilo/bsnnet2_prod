<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class EmailAttachFileUploader
{
    private $targetDirectory;

    public function __construct($targetDirectory)
    {
        $this->targetDirectory = $targetDirectory;
    }

    public function upload(UploadedFile $file, $id, $name)
    {

        $fileName = $name."-".$id. '.' . $file->guessExtension();
        $directory= $this->getTargetDirectory()."/".$id."/";

        try {
            $file->move($directory, $fileName);
        } catch (FileException $e) {
            return "error";
        }

        return $fileName;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}
