<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ToolUppercaseProvidersCommand extends Command
{
    protected static $defaultName = 'tool:uppercase';
    private $em;
    private $conn;
    private $params;

    public function __construct( EntityManagerInterface $em, ParameterBagInterface $params)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
        $this->params = $params;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $providers=$this->em->getRepository('App:Provider')->findAll();
        $organizations=$this->em->getRepository('App:Organization')->findAll();

        foreach ($providers as $provider){
            $first_name=$provider->getFirstName();
            $last_name=$provider->getLastName();

            $new_first_name=strtoupper($first_name);
            $new_last_name=strtoupper($last_name);

            $provider->setFirstName($new_first_name);
            $provider->setLastName($new_last_name);

            $this->em->persist($provider);
        }

        foreach ($organizations as $organization){
            $name=$organization->getName();

            $new_name=strtoupper($name);
            $organization->setName($new_name);
            $this->em->persist($organization);
        }

        $this->em->flush();

        $io->success('Process Finished');
    }
}
