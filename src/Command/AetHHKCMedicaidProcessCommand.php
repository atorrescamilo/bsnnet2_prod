<?php

namespace App\Command;

use App\Entity\AETFHKCMedicaidData;
use App\Entity\AetFHKCMedicaidLog;
use App\Entity\BillingAddress;
use App\Entity\Organization;
use App\Entity\Provider;
use App\Entity\TaxonomyPM;
use App\Utils\My_Mcript;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;


class AetHHKCMedicaidProcessCommand extends Command
{

    protected static $defaultName = 'aet:fhkc:start';
    private $em;
    private $conn;
    private $private_key;
    private $encoder;
    private $params;

    public function __construct( EntityManagerInterface $em, ParameterBagInterface $params)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
        $this->params = $params;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $aetna_data=$this->em->getRepository('App:AETData')->findAll();
        $aetna_data_org=$this->em->getRepository('App:AETDataOrg')->findAll();

        //remove all fccroster_data records
        $aet_roster_data=$this->em->getRepository('App:AETFHKCMedicaidData')->findAll();
        if($aet_roster_data){
            foreach ($aet_roster_data as $item){
                $this->em->remove($item);
            }
            $this->em->flush();
        }

        $cont=1;

        foreach ($aetna_data as $datum){
            $provider_id=$datum->getProviderId();
            $npi=$datum->getNpi();

            $data=$datum->getData();

            $license_number=$data['license_number'];
            $license_number=strtoupper($license_number);

            $current_suf=substr($license_number,0,2);
            if($current_suf=="ME"){
                $license_number = str_replace("ME", "MD", $license_number);

            }else if($current_suf=="OS"){
                $license_number = str_replace("OS", "DO", $license_number);
            }

            $license_number=str_replace('-',"",$license_number);
            $license_number=str_replace(' ',"",$license_number);
            $license_number=trim($license_number);
            $first_latter_license=substr($license_number,0,1);

            $provider=$this->em->getRepository('App\Entity\Provider')->find($provider_id);
            //$provider=new Provider();

            $address=$provider->getBillingAddress();
            $this->private_key=$this->params->get('private_key');
            $encoder=new My_Mcript($this->private_key);

            //Action type for Columb B
            /*For the initial data submission, all records will be "add". Each month thereafter, only submit changes to the data.
            For new records (new providers, new locations, new specialties, etc.), indicate "add". For changes to existing records
            (phone number changes, address replacement, provider name change, etc.), indicate "edit". For record terminations
            (providers leaving network, locations leaving network, etc.), indicate "term".*/

            $action_type="Add";

            $plan_type="Health";
            $plan_name="Aetna Better Health of Florida";
            $provider_directory="Y";

            $first_name= strtoupper($provider->getFirstName());
            $last_name= strtoupper($provider->getLastName());
            $sufix=strtoupper($provider->getInitial());

            if($sufix=="-"){
                $sufix="";
            }

            $gender=$provider->getGender();
            if($gender=="male"){
                $gender="Male";
            }

            if($gender=="female"){
                $gender="Female";
            }

            $language1="";
            $language2="";
            $language3="";

            $languages=$provider->getLanguages();
            if($languages){
                if(count($languages)>0){
                    $cont=1;
                    foreach ($languages as $language){
                        if($cont==1){
                            $language1=$language->getName();
                        }
                        if($cont==2){
                            $language2=$language->getName();
                        }
                        if($cont==3){
                            $language3=$language->getName();
                        }
                        $cont++;
                    }
                }else{
                    $language1="English";
                }
            }

            $organization=$provider->getOrganization();
            $website=$organization->getWebsite();
            $practice_name=$organization->getName();

            $board_certified="No";
            if($provider->getBoardCertified()==true){
                $board_certified="Yes";
            }

            $taxonomies=$provider->getTaxonomyCodes();
            $taxonomy_code="";
            if($taxonomies){
                if(count($taxonomies)>0){
                    $taxonomy_code=$taxonomies[0]->getCode();
                }
            }

            if($taxonomy_code==""){
                $taxonomy_code=$provider->getTaxonomyCode();
            }

            $specialty1="";
            $specialty2="";
            $specialty3="";
            $specialty4="";

            $taxonomy_mapping=$this->em->getRepository('App:TaxonomyPM')->findOneBy(array('code'=>$taxonomy_code));
            if($taxonomy_mapping){
                $specialty1=$taxonomy_mapping->getSpecialty1();
                $specialty2=$taxonomy_mapping->getSpecialty2();
                $specialty3=$taxonomy_mapping->getSpecialty3();
                $specialty4=$taxonomy_mapping->getSpecialty4();
            }

            $hospital_privileges="";
            if($provider->getHopitalPrivileges()!=null and $provider->getHopitalPrivileges()!=""){
                $hospital_privileges=$provider->getHopitalPrivileges();
            }

            foreach ($address as $addr){
                $addr_track_number=$this->em->getRepository('App:ProviderAddrFCCTN')->findOneBy(array('address'=>$addr->getId(),'provider'=>$provider_id));
                $key="COV".$provider->getNpiNumber()."-".'00'.$addr_track_number->getTrackNumber();

                $organization=$addr->getOrganization();
                $age_min=$addr->getOfficeAgeLimit();
                $age_max=$addr->getOfficeAgeLimitMax();

                if ($age_min == "") {
                    $age_min = "00";
                } else {
                    if (strlen($age_min) == 1) {
                        $age_min = "0" . $age_min;
                    }
                }

                if ($age_max == "") {
                    $age_max = "99";
                } else {
                    if (strlen($age_max) == 1) {
                        $age_max = "0" . $age_max;
                    } else {
                        $age_max = $age_max;
                    }
                }

                if($age_max=="00"){
                    $age_max="99";
                }

                $age_limitations=$age_min."-".$age_max;

                $street=$addr->getStreet();
                $suite=$addr->getSuiteNumber();
                $city=$addr->getCity();
                $state=$addr->getUsState();
                $zip_code=$addr->getZipCode();

                if(strlen($zip_code)>5){
                    $zip_code=substr($zip_code,0,5);
                }

                $phone_number=$addr->getPhoneNumber();
                $phone_number=str_replace("-",'',$phone_number);
                $phone_number=str_replace("/",'',$phone_number);

                if($phone_number=="" or $phone_number==null){
                    $phone_number=$organization->getPhone();
                    $phone_number=str_replace("-",'',$phone_number);
                    $phone_number=str_replace("/",'',$phone_number);
                }

                $Mon_START="";
                $Mon_END="";
                $Tues_START="";
                $Tues_END="";
                $Wed_START="";
                $Wed_END="";
                $Thurs_START="";
                $Thurs_END="";
                $Fri_Start="";
                $Fri_END="";
                $Sat_START="";
                $Sat_END="";
                $Sun_START="";
                $Sun_END="";

                $hours_operation_monday="";
                $hours_operation_tuesday="";
                $hours_operation_wednesday="";
                $hours_operation_thursday="";
                $hours_operation_friday="";
                $hours_operation_saturday="";
                $hours_operation_sunday="";


                $bussinesOurs = $addr->getBusinessHours();
                if ($bussinesOurs != "") {
                    $bussinesOurs = substr($bussinesOurs, 1);
                    $bussinesOurs = substr($bussinesOurs, 0, -1);

                    $bussinesOurs = str_replace('{', '', $bussinesOurs);
                    $bussinesOurs = str_replace('}', '', $bussinesOurs);
                    $bussinesOurs = str_replace('"', '', $bussinesOurs);

                    $daysHours = explode(",", $bussinesOurs);

                    //monday
                    $dayActive = explode(":", $daysHours[0])[1];
                    $startHour = substr($daysHours[1], 9);
                    $tillHour = substr($daysHours[2], 9);

                    if ($dayActive == "true") {
                        $Mon_START = $this->convertTo24Hours($startHour);
                        $Mon_END = $this->convertTo24Hours($tillHour);
                        $hours_operation_monday=$Mon_START."-".$Mon_END;
                    }

                    //tuesday
                    $dayActive = explode(":", $daysHours[3])[1];
                    $startHour = substr($daysHours[4], 9);
                    $tillHour = substr($daysHours[5], 9);
                    if ($dayActive == "true") {
                        $Tues_START = $this->convertTo24Hours($startHour);
                        $Tues_END = $this->convertTo24Hours($tillHour);
                        $hours_operation_tuesday=$Tues_START."-".$Tues_END;
                    }

                    //Wednesday
                    $dayActive = explode(":", $daysHours[6])[1];
                    $startHour = substr($daysHours[7], 9);
                    $tillHour = substr($daysHours[8], 9);
                    if ($dayActive == "true") {
                        $Wed_START = $this->convertTo24Hours($startHour);
                        $Wed_END = $this->convertTo24Hours($tillHour);
                        $hours_operation_wednesday=$Wed_START."-".$Wed_END;
                    }

                    //Thurs
                    $dayActive = explode(":", $daysHours[9])[1];
                    $startHour = substr($daysHours[10], 9);
                    $tillHour = substr($daysHours[11], 9);
                    if ($dayActive == "true") {
                        $Thurs_START = $this->convertTo24Hours($startHour);
                        $Thurs_END = $this->convertTo24Hours($tillHour);
                        $hours_operation_thursday=$Thurs_START."-".$Thurs_END;
                    }

                    //Frid
                    $dayActive = explode(":", $daysHours[12])[1];
                    $startHour = substr($daysHours[13], 9);
                    $tillHour = substr($daysHours[14], 9);
                    if ($dayActive == "true") {
                        $Fri_Start = $this->convertTo24Hours($startHour);
                        $Fri_END = $this->convertTo24Hours($tillHour);
                        $hours_operation_friday=$Fri_Start."-".$Fri_END;
                    }

                    //sat
                    $dayActive = explode(":", $daysHours[15])[1];
                    $startHour = substr($daysHours[16], 9);
                    $tillHour = substr($daysHours[17], 9);
                    if ($dayActive == "true") {
                        $Sat_START = $this->convertTo24Hours($startHour);
                        $Sat_END = $this->convertTo24Hours($tillHour);
                        $hours_operation_saturday=$Sat_START."-".$Sat_END;
                    }

                    //sunday
                    $dayActive = explode(":", $daysHours[18])[1];
                    $startHour = substr($daysHours[19], 9);
                    $tillHour = substr($daysHours[20], 9);

                    if ($dayActive == "true") {
                        $Sun_START = $this->convertTo24Hours($startHour);
                        $Sun_END = $this->convertTo24Hours($tillHour);
                        $hours_operation_sunday=$Sun_START."-".$Sun_END;
                    }
                }

                $accesible="No";

                if($addr->getWheelchair()==true){
                    $accesible="Yes";
                }

                $data=[
                  'col_a'=>$key,
                  'col_b'=>$action_type,
                  'col_c'=>$plan_type,
                  'col_d'=>$plan_name,
                  'col_e'=>$npi,
                  'col_f'=>$taxonomy_code,
                  'col_g'=>$provider_directory,
                  'col_h'=>$first_name,
                  'col_i'=>$last_name,
                  'col_j'=>$sufix,
                  'col_k'=>$license_number,
                  'col_l'=>$gender,
                  'col_m'=>$accesible,
                  'col_n'=>$age_limitations,
                  'col_o'=>$language1,
                  'col_p'=>$language2,
                  'col_q'=>$language3,
                  'col_r'=>"",
                  'col_s'=>"",
                  'col_t'=>$practice_name,
                  'col_u'=>$hospital_privileges,
                  'col_v'=>"",
                  'col_w'=>"",
                  'col_x'=>$website,
                  'col_y'=>"Yes",
                  'col_z'=>$specialty1,
                  'col_aa'=>$specialty2,
                  'col_ab'=>$specialty3,
                  'col_ac'=>$specialty4,
                  'col_ad'=>$board_certified,
                  'col_ae'=>$street,
                  'col_af'=>$suite,
                  'col_ag'=>$city,
                  'col_ah'=>$state,
                  'col_ai'=>$zip_code,
                  'col_aj'=>$phone_number,
                  'col_ak'=>$hours_operation_monday,
                  'col_al'=>$hours_operation_tuesday,
                  'col_am'=>$hours_operation_wednesday,
                  'col_an'=>$hours_operation_thursday,
                  'col_ao'=>$hours_operation_friday,
                  'col_ap'=>$hours_operation_saturday,
                  'col_aq'=>$hours_operation_sunday,
                ];

                if($license_number!="" and $license_number!="N/A" and $license_number!="n/a" and $first_latter_license!="I"
                    and $first_latter_license!="i" and $license_number!="notavailable" and $license_number!="NotAvailable" and $license_number!="Florida" and $license_number!="Fl"
                and $license_number!="FL"){
                    //create a new Record
                    $fhkcData=new AETFHKCMedicaidData();
                    $fhkcData->setData($data);
                    $fhkcData->setKeyLog($key);
                    $fhkcData->setNpi($npi);
                    $fhkcData->setStatus('active');

                    $this->em->persist($fhkcData);

                    $io->writeln($cont." ".$key);
                    $cont++;

                    //save the log
                    $fhkcLog=new AetFHKCMedicaidLog();
                    $fhkcLog->setUniqueRecordId($key);
                    $fhkcLog->setActionType($action_type);
                    $fhkcLog->setPlanType($plan_type);
                    $fhkcLog->setPlanName($plan_name);
                    $fhkcLog->setNpi($npi);
                    $fhkcLog->setTaxonomyCode($taxonomy_code);
                    $fhkcLog->setProviderDirectory($provider_directory);
                    $fhkcLog->setFirstName($first_name);
                    $fhkcLog->setLastName($last_name);
                    $fhkcLog->setSufix($sufix);
                    $fhkcLog->setLicenseNumber($license_number);
                    $fhkcLog->setGender($gender);
                    $fhkcLog->setAccesible($accesible);
                    $fhkcLog->setAgeLimitations($age_limitations);
                    $fhkcLog->setLanguage1($language1);
                    $fhkcLog->setLanguage2($language2);
                    $fhkcLog->setLanguage3($language3);
                    $fhkcLog->setPracticeName($practice_name);
                    $fhkcLog->setHospitalPrivileges($hospital_privileges);
                    $fhkcLog->setWebsite($website);
                    $fhkcLog->setSpecialty1($specialty1);
                    $fhkcLog->setSpecialty2($specialty2);
                    $fhkcLog->setSpecialty3($specialty3);
                    $fhkcLog->setSpecialty4($specialty4);
                    $fhkcLog->setBoardCertified($board_certified);
                    $fhkcLog->setStreet($street);
                    $fhkcLog->setSuite($suite);
                    $fhkcLog->setCity($city);
                    $fhkcLog->setState($state);
                    $fhkcLog->setZipCode($zip_code);
                    $fhkcLog->setPhoneNumber($phone_number);
                    $fhkcLog->setHoursOperationMonday($hours_operation_monday);
                    $fhkcLog->setHoursOperationTuesday($hours_operation_tuesday);
                    $fhkcLog->setHoursOperationWednesday($hours_operation_wednesday);
                    $fhkcLog->setHoursOperationThursday($hours_operation_thursday);
                    $fhkcLog->setHoursOperationFriday($hours_operation_friday);
                    $fhkcLog->setHoursOperationSaturday($hours_operation_saturday);
                    $fhkcLog->setHoursOperationSunday($hours_operation_sunday);

                    $this->em->persist($fhkcLog);

                    $data=[
                        'col_ak'=>$hours_operation_monday,
                        'col_al'=>$hours_operation_tuesday,
                        'col_am'=>$hours_operation_wednesday,
                        'col_an'=>$hours_operation_thursday,
                        'col_ao'=>$hours_operation_friday,
                        'col_ap'=>$hours_operation_saturday,
                        'col_aq'=>$hours_operation_sunday,
                    ];

                }
            }
        }

        $this->em->flush();


        $io->success('FHKC Medicaid Process Finished');

        return Command::SUCCESS;
    }

    private function convertTo24Hours($date){
        if (strpos($date, 'am') == true) {
            $newDate=substr($date,0,-2);

            if(strlen($newDate)<4){
                $newDate='0'.$newDate;
            }
        }else{
            $newDate=substr($date,0,-2);

            if($newDate=="12:00"){$newDate="12:00";}
            if($newDate=="12:30"){$newDate="12:30";}
            if($newDate=="1:00"){$newDate="13:00";}
            if($newDate=="1:30"){$newDate="13:30";}
            if($newDate=="2:00"){$newDate="14:00";}
            if($newDate=="2:30"){$newDate="14:30";}
            if($newDate=="3:00"){$newDate="15:00";}
            if($newDate=="3:30"){$newDate="15:30";}
            if($newDate=="4:00"){$newDate="16:00";}
            if($newDate=="4:30"){$newDate="16:30";}
            if($newDate=="5:00"){$newDate="17:00";}
            if($newDate=="5:30"){$newDate="17:30";}
            if($newDate=="6:00"){$newDate="18:00";}
            if($newDate=="6:30"){$newDate="18:30";}
            if($newDate=="7:00"){$newDate="19:00";}
            if($newDate=="7:30"){$newDate="19:30";}
            if($newDate=="8:00"){$newDate="20:00";}
            if($newDate=="8:30"){$newDate="20:30";}
            if($newDate=="9:00"){$newDate="21:00";}
            if($newDate=="9:30"){$newDate="21:30";}
            if($newDate=="10:00"){$newDate="22:00";}
            if($newDate=="10:30"){$newDate="22:30";}
            if($newDate=="11:00"){$newDate="23:00";}
            if($newDate=="11:30"){$newDate="23:30";}
        }

        return $newDate;
    }
}
