<?php

namespace App\Command;

use App\Entity\BillingAddress;
use App\Entity\MMMRosterIndividualData;
use App\Entity\Organization;
use App\Entity\Provider;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Utils\My_Mcript;

class MMMRosterCommand extends Command
{
    protected static $defaultName = 'mmm:roster:providers';
    private $em;
    private $conn;
    private $params;
    private $private_key;
    private $encoder;

    public function __construct( EntityManagerInterface $em, ParameterBagInterface $params)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
        $this->params = $params;
        $this->private_key=$this->params->get('private_key');
        $this->encoder=new My_Mcript($this->private_key);
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $payer=1;

        //remove all current records
        $roster_records=$this->em->getRepository('App:MMMRosterIndividualData')->findAll();

        if($roster_records){
            foreach ($roster_records as $record){
                $this->em->remove($record);
            }
            $this->em->flush();
        }

        $sql="SELECT p.id
            FROM  provider p 
            LEFT JOIN provider_payer ON provider_payer.provider_id = p.id
            LEFT JOIN payer ON payer.id = provider_payer.payer_id
            WHERE p.disabled=0 and payer.id=".$payer."
            GROUP BY p.id
            ORDER BY p.id";

        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $providers= $stmt->fetchAll();
        $providers_npi=[];

        $cont=1;
        foreach ($providers as $id){
            $provider=$this->em->getRepository('App:Provider')->find($id);
            $npi=$provider->getNpiNumber();

            if(!in_array($npi,$providers_npi)){
                $providers_npi[]=$npi;

                //get all providers with this NPI
                $providers2=$this->em->getRepository('App:Provider')->findBy(array('npi_number'=>$npi,'disabled'=>0));
                foreach ($providers2 as $pro){
                    //$pro=new Provider();

                    $organization=$pro->getOrganization();

                    $effectiveDate="";
                    if($pro->getIsFacility()==false){
                        $credentialings=$this->em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('npi_number'=>$pro->getNpiNumber()));
                        if($credentialings!=null){
                            foreach ($credentialings as $credentialing){
                                if($credentialing->getCredentialingEffectiveDate()!=""){
                                    $effectiveDate=$credentialing->getCredentialingEffectiveDate();
                                }
                            }
                        }
                    }else{
                        $org_credentialings=$this->em->getRepository('App:OrganizationCredentialing')->findBy(array('organization'=>$organization->getId()));

                        if($org_credentialings){
                            foreach ($org_credentialings as $org_cred){
                                $effectiveDate=$org_cred->getCredentialingEffectiveDate();
                            }

                        }
                    }

                    $last_name=$pro->getLastName();
                    $first_name=$pro->getFirstName();
                    $initial=$pro->getInitial();

                    $pdata = $this->em->getRepository('App:PDataMedicare')->findOneBy(array('npi_number' => $npi));
                    $specialty = "";
                    if ($pdata != null) {
                        $json = $pdata->getData();
                        $datos = json_decode($json, true);

                        if(count($datos)>1){
                            if(isset($datos['results'][0]['taxonomies'])){
                                $taxonomies = $datos['results'][0]['taxonomies'];
                                foreach ($taxonomies as $taxonomy) {
                                    if ($taxonomy['primary'] == 1) {
                                        $specialty = $taxonomy['desc'];
                                    }
                                }
                            }
                        }
                    }

                    $email="";
                    $contacts=$this->em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));

                    if($contacts){
                        foreach ($contacts as $contact){
                            if($contact->getEmail()!=""){
                                $email=$contact->getEmaiL();
                                break;
                            }
                        }
                    }

                    $degress=$pro->getDegree();
                    $degree_str="";
                    if($degress){
                       foreach ($degress as $degre){
                        $degree_str.=$degre->getName().", ";
                       }
                    }

                    if($degree_str!=""){
                        $degree_str=substr($degree_str,0,-2);
                    }

                    $organization=$pro->getOrganization();
                    $organization_str="";
                    if($organization){
                        $organization_str=$organization->getName();
                    }

                    $billing_npi="";
                    if($organization){
                        $billing_npi=$organization->getGroupNpi();
                    }

                    //get TIN
                    $tin=$organization->getTinNumber();
                    if($tin==null or $tin==""){
                        $tin=$this->encoder->decryptthis($pro->getSocial());
                    }

                    $addresses=$pro->getBillingAddress();
                    if($addresses){
                        foreach ($addresses as $address){
                           // $address=new BillingAddress();
                            $street=$address->getStreet();
                            $suite_number=$address->getSuiteNumber();
                            $city=$address->getCity();
                            $state=$address->getUsState();
                            $zip_code=$address->getZipCode();
                            $phone_number=$address->getPhoneNumber();
                            $fax=$this->AddressFax($address);

                            $bussinesOurs=$address->getBusinessHours();

                            $bh_sun="";
                            $bh_mon="";
                            $bh_tue="";
                            $bh_wed="";
                            $bh_thu="";
                            $bh_fri="";
                            $bh_sat="";

                            if($bussinesOurs!=null and $bussinesOurs!=""){
                                $bussinesOurs=substr($bussinesOurs,1);
                                $bussinesOurs=substr($bussinesOurs,0,-1);

                                $bussinesOurs=str_replace('{','',$bussinesOurs);
                                $bussinesOurs=str_replace('}','',$bussinesOurs);
                                $bussinesOurs=str_replace('"','',$bussinesOurs);
                                $daysHours = explode(",", $bussinesOurs);

                                //sunday
                                $dayActive=explode(":",$daysHours[18])[1];
                                $dayStart=explode(":",$daysHours[19]);
                                $dayTill=explode(":",$daysHours[20]);

                                if($dayActive=="true"){
                                    $dateStart=$dayStart[1].$dayStart[2];
                                    $date24Start=$this->convertTo24Hours($dateStart);
                                    $dateTill=$dayTill[1].$dayTill[2];
                                    $date24Till=$this->convertTo24Hours($dateTill);
                                    $bh_sun=$date24Start."-".$date24Till;
                                }


                                //monday
                                $dayActive=explode(":",$daysHours[0])[1];
                                $dayStart=explode(":",$daysHours[1]);
                                $dayTill=explode(":",$daysHours[2]);

                                if($dayActive=="true"){
                                    $dateStart=$dayStart[1].$dayStart[2];
                                    $date24Start=$this->convertTo24Hours($dateStart);
                                    $dateTill=$dayTill[1].$dayTill[2];
                                    $date24Till=$this->convertTo24Hours($dateTill);
                                    $bh_mon=$date24Start."-".$date24Till;
                                }

                                //tuesday
                                $dayActive=explode(":",$daysHours[3])[1];
                                $dayStart=explode(":",$daysHours[4]);
                                $dayTill=explode(":",$daysHours[5]);

                                if($dayActive=="true"){
                                    $dateStart=$dayStart[1].$dayStart[2];
                                    $date24Start=$this->convertTo24Hours($dateStart);
                                    $dateTill=$dayTill[1].$dayTill[2];
                                    $date24Till=$this->convertTo24Hours($dateTill);
                                    $bh_tue=$date24Start."-".$date24Till;
                                }

                                //wednesday
                                $dayActive=explode(":",$daysHours[6])[1];
                                $dayStart=explode(":",$daysHours[7]);
                                $dayTill=explode(":",$daysHours[8]);

                                if($dayActive=="true"){
                                    $dateStart=$dayStart[1].$dayStart[2];
                                    $date24Start=$this->convertTo24Hours($dateStart);
                                    $dateTill=$dayTill[1].$dayTill[2];
                                    $date24Till=$this->convertTo24Hours($dateTill);
                                    $bh_wed=$date24Start."-".$date24Till;
                                }


                                //thuerday
                                $dayActive=explode(":",$daysHours[9])[1];
                                $dayStart=explode(":",$daysHours[10]);
                                $dayTill=explode(":",$daysHours[11]);

                                if($dayActive=="true"){
                                    $dateStart=$dayStart[1].$dayStart[2];
                                    $date24Start=$this->convertTo24Hours($dateStart);
                                    $dateTill=$dayTill[1].$dayTill[2];
                                    $date24Till=$this->convertTo24Hours($dateTill);
                                    $bh_thu=$date24Start."-".$date24Till;
                                }

                                //friday
                                $dayActive=explode(":",$daysHours[12])[1];
                                $dayStart=explode(":",$daysHours[13]);
                                $dayTill=explode(":",$daysHours[14]);

                                if($dayActive=="true"){
                                    $dateStart=$dayStart[1].$dayStart[2];
                                    $date24Start=$this->convertTo24Hours($dateStart);
                                    $dateTill=$dayTill[1].$dayTill[2];
                                    $date24Till=$this->convertTo24Hours($dateTill);
                                    $bh_fri=$date24Start."-".$date24Till;
                                }

                                //saturday
                                $dayActive=explode(":",$daysHours[15])[1];
                                $dayStart=explode(":",$daysHours[16]);
                                $dayTill=explode(":",$daysHours[17]);

                                if($dayActive=="true"){
                                    $dateStart=$dayStart[1].$dayStart[2];
                                    $date24Start=$this->convertTo24Hours($dateStart);
                                    $dateTill=$dayTill[1].$dayTill[2];
                                    $date24Till=$this->convertTo24Hours($dateTill);
                                    $bh_sat=$date24Start."-".$date24Till;
                                }
                            }

                            $street_billing="";
                            $suite_number_billing="";
                            $city_billing="";
                            $state_billing="";
                            $zip_code_billing="";
                            $phone_number_billing="";
                            $fax_billing="";

                            if($address->getBillingAddr()==true){
                                $street_billing=$address->getStreet();
                                $suite_number_billing=$address->getSuiteNumber();
                                $city_billing=$address->getCity();
                                $state_billing=$address->getUsState();
                                $zip_code_billing=$address->getZipCode();
                                $phone_number_billing=$address->getPhoneNumber();
                                $fax_billing=$this->AddressFax($address);
                            }else{

                                if($organization){
                                    $addresses2=$this->em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization->getId()));

                                    if(count($addresses2)==1){
                                        $street_billing=$address->getStreet();
                                        $suite_number_billing=$address->getSuiteNumber();
                                        $city_billing=$address->getCity();
                                        $state_billing=$address->getUsState();
                                        $zip_code_billing=$address->getZipCode();
                                        $phone_number_billing=$address->getPhoneNumber();
                                        $fax_billing=$this->AddressFax($address);
                                    }else{
                                        foreach ($addresses2 as $addr2){
                                            if($addr2->getBillingAddr()==true){
                                                $street_billing=$addr2->getStreet();
                                                $suite_number_billing=$addr2->getSuiteNumber();
                                                $city_billing=$addr2->getCity();
                                                $state_billing=$addr2->getUsState();
                                                $zip_code_billing=$addr2->getZipCode();
                                                $phone_number_billing=$addr2->getPhoneNumber();
                                                $fax_billing=$this->AddressFax($addr2);
                                            }
                                        }

                                        if($street_billing=="" and $suite_number_billing=="" and $city_billing=="" and $state_billing==""){
                                            foreach ($addresses2 as $addr2){
                                                if($addr2->getPrimaryAddr()==true or $addr2->getMailingAddr()==true){
                                                    $street_billing=$addr2->getStreet();
                                                    $suite_number_billing=$addr2->getSuiteNumber();
                                                    $city_billing=$addr2->getCity();
                                                    $state_billing=$addr2->getUsState();
                                                    $zip_code_billing=$addr2->getZipCode();
                                                    $phone_number_billing=$addr2->getPhoneNumber();
                                                    $fax_billing=$this->AddressFax($addr2);
                                                }
                                            }
                                        }
                                    }

                                }
                            }

                            if($street_billing=="" and $suite_number_billing=="" and $city_billing=="" and $state_billing==""){
                                $street_billing=$address->getStreet();
                                $suite_number_billing=$address->getSuiteNumber();
                                $city_billing=$address->getCity();
                                $state_billing=$address->getUsState();
                                $zip_code_billing=$address->getZipCode();
                                $phone_number_billing=$address->getPhoneNumber();
                                $fax_billing=$this->AddressFax($address);
                            }

                            $serviceSettings=$address->getServiceSettings();
                            $serviceSettingStr="";

                            if(count($serviceSettings)>0){
                                foreach ($serviceSettings as $serviceSetting){
                                    $serviceSettingStr.=$serviceSetting->getName().",";
                                }
                                $serviceSettingStr=substr($serviceSettingStr,0,-1);
                            }

                            $boardCertified="No";
                            if($pro->getBoardCertified()==1){
                                $boardCertified="Yes";
                            }

                            $languages_str="";
                            $languages=$pro->getLanguages();
                            if($languages){
                                foreach ($languages as $language){
                                    $languages_str.=$language->getName().", ";
                                }

                                $languages_str=substr($languages_str,0,-2);
                            }



                            $data=[
                                'col_f'=>$npi,
                                'col_g'=>$effectiveDate,
                                'col_i'=>$last_name,
                                'col_j'=>$initial,
                                'col_k'=>$first_name,
                                'col_l'=>$specialty,
                                'col_m'=>$email,
                                'col_v'=>$degree_str,
                                'col_w'=>$organization_str,
                                'col_x'=>$billing_npi,
                                'col_y'=>$tin,
                                'col_z'=>$specialty,
                                'col_aa'=>$organization_str,
                                'col_ab'=>$street,
                                'col_ac'=>$suite_number,
                                'col_ad'=>$city,
                                'col_ae'=>$state,
                                'col_af'=>$zip_code,
                                'col_ag'=>$phone_number,
                                'col_ah'=>$fax,
                                'col_ai'=>$bh_sun,
                                'col_aj'=>$bh_mon,
                                'col_ak'=>$bh_tue,
                                'col_al'=>$bh_wed,
                                'col_am'=>$bh_thu,
                                'col_an'=>$bh_fri,
                                'col_ao'=>$bh_sat,
                                'col_ap'=>$street_billing,
                                'col_aq'=>$suite_number_billing,
                                'col_ar'=>$city_billing,
                                'col_as'=>$state_billing,
                                'col_at'=>$zip_code_billing,
                                'col_au'=>$phone_number_billing,
                                'col_av'=>$fax_billing,
                                'col_aw'=>$pro->getHopitalPrivilegesType(),
                                'col_ax'=>$pro->getHopitalPrivileges(),
                                'col_ba'=>$serviceSettingStr,
                                'col_by'=>$boardCertified,
                                'col_cb'=>$languages_str,
                                'col_bz'=>$pro->getBoardCertifiedSpecialty()
                            ];

                            $record=new MMMRosterIndividualData();
                            $record->setNpi($npi);
                            $record->setData($data);

                            $this->em->persist($record);

                            $io->writeln($cont." ".$provider->getFirstName(). " ".$npi." ".$languages_str);
                            $cont++;
                        }
                    }
                }
            }
        }

        $this->em->flush();

        $io->success('Process Finished');
    }
    private function AddressFax($address){
        $fax="";

        $organization=$address->getOrganization();
        $contacts=$this->em->getRepository('App\Entity\OrganizationContact')->findBy(array('organization'=>$organization->getId()));
        $faxs=array();
        if($contacts){
            foreach ($contacts as $contact){
                if($contact->getFax()!=""){
                    $faxs[]=$contact->getFax();
                }
            }
        }
        if(count($faxs)>0){
            $fax=$faxs[0];
        }

        return $fax;
    }

    private function convertTo24Hours($date){
        $newDate="";
        if (strpos($date, 'am') == true) {
            $newDate=substr($date,0,-2);

            if(strlen($newDate)<4){
                $newDate='0'.$newDate;
            }

        }else{
            $newDate=substr($date,0,-2);
            if($newDate=="1200"){$newDate="1200";}if($newDate=="1230"){$newDate="1230";}
            if($newDate=="100"){$newDate="1300";}if($newDate=="130"){$newDate="1330";}
            if($newDate=="200"){$newDate="1400";}if($newDate=="230"){$newDate="1430";}
            if($newDate=="300"){$newDate="1500";}if($newDate=="330"){$newDate="1530";}
            if($newDate=="400"){$newDate="1600";}if($newDate=="430"){$newDate="1630";}
            if($newDate=="500"){$newDate="1700";}if($newDate=="530"){$newDate="1730";}
            if($newDate=="600"){$newDate="1800";}if($newDate=="630"){$newDate="1830";}
            if($newDate=="700"){$newDate="1900";}if($newDate=="730"){$newDate="1930";}
            if($newDate=="800"){$newDate="2000";}if($newDate=="830"){$newDate="2030";}
            if($newDate=="900"){$newDate="2100";}if($newDate=="930"){$newDate="2130";}
            if($newDate=="1000"){$newDate="2200";}if($newDate=="1030"){$newDate="2230";}
            if($newDate=="1100"){$newDate="2300";}if($newDate=="1130"){$newDate="2330";}
        }

        return $newDate;
    }
}
