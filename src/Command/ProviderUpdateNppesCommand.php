<?php

namespace App\Command;

use App\Entity\PDataMedicare;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ProviderUpdateNppesCommand extends Command
{

    protected static $defaultName = 'provider:update:nppes';
    private $em;
    private $conn;
    private $private_key;
    private $encoder;
    private $params;

    public function __construct( EntityManagerInterface $em, ParameterBagInterface $params)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
        $this->params = $params;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $providers=$this->em->getRepository('App:Provider')->findBy(array('disabled'=>0));

        $cont=1;
        foreach ($providers as $provider){
            $npi=$provider->getNpiNumber();
            if ($npi != "" and $npi != "NOT AVAILABLE" and $npi != "N/A") {

                $record=$this->em->getRepository('App:PDataMedicare')->findBy(array('npi_number'=>$npi));
                if($record==null){
                    $url_result = "https://npiregistry.cms.hhs.gov/api/?number=$npi&enumeration_type=&taxonomy_description=&first_name=&use_first_name_alias=&last_name=&organization_name=&address_purpose=&city=&state=&postal_code=&country_code=&limit=200&skip=&pretty=on&version=2.1";
                    $json = file_get_contents($url_result);

                    $pData = new PDataMedicare();
                    $pData->setNpiNumber($npi);
                    $pData->setData($json);
                    $this->em->persist($pData);
                    $this->em->flush();

                    $io->writeln($cont." ".$npi);
                    $cont++;
                }
            }
        }

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return Command::SUCCESS;
    }
}
