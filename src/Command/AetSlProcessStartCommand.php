<?php

namespace App\Command;

use App\Entity\AETDataSL;
use App\Utils\My_Mcript;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AetSlProcessStartCommand extends Command
{
    protected static $defaultName = 'aet:sl-process:start';
    private $em;
    private $conn;
    private $private_key;
    private $encoder;
    private $params;

    public function __construct( EntityManagerInterface $em, ParameterBagInterface $params)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
        $this->params = $params;
    }

    protected function configure()
    {
        $this
            ->addArgument('regions', InputArgument::REQUIRED, 'Filter Regions 1-> 6,7 and 11, 2->for All regions',null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $param_region=$input->getArgument('regions');
        $this->private_key=$this->params->get('private_key');
        $this->encoder=new My_Mcript($this->private_key);

        //get all data from PG record , this data not need validations
        $aet_data=$this->em->getRepository('App:AETData')->findBy(array('track_number_type'=>6));
        $aet_data4=$this->em->getRepository('App:AETData')->findBy(array('track_number_type'=>4));
        $aet_data_org=$this->em->getRepository('App:AETDataOrg')->findAll();

        $record_tracking_number_array=[];

        $providersIdsPG=[];
        foreach ($aet_data as $datum){
            $id=$datum->getProviderId();
            $providersIdsPG[]=$id;
        }

        $providersIdsPG4=[];
        foreach ($aet_data4 as $datum4){
            $id=$datum4->getProviderId();
            $providersIdsPG4[]=$id;
        }

        //remove all previus records
        $all_sl=$this->em->getRepository('App:AETDataSL')->findAll();

        foreach ($all_sl as $item){
            $this->em->remove($item);
        }
        $this->em->flush();

        $cont=1;
        foreach ($aet_data_org as $org_data){
            $org_id=$org_data->getOrganizationId();

            $organization=$this->em->getRepository('App\Entity\Organization')->find($org_id);
            $address=$this->em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
            $providers=$this->em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0,'organization'=>$organization->getId()));

            $npi=$organization->getGroupNpi();
            $data=$org_data->getData();

            $pass=true;
            if(stristr($data['record_tracking_number'], 'COV3')){
                $pass=false;
            }

            if($pass){
                $organization_languages_code="";
                if($providers!=null){
                    $languagesId=[];
                    foreach ($providers as $provider){
                        if(in_array($provider->getId(),$providersIdsPG)==true){
                            $languages=$provider->getLanguages();
                            if($languages!=null){
                                foreach ($languages as $language){
                                    $code=$language->getCode();
                                    if($code!=""){
                                        if(!in_array($code,$languagesId) and $language->getId()!=1){
                                            if($code=='02' or $code=='03' or $code=='04' or $code=='05' or $code=='06' or $code=='07' or $code=='08' or $code=='09'){
                                                $languagesId[]=$code;
                                                $organization_languages_code.=$code."~";
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $languages_code=substr($organization_languages_code,0,-1);
                $groupNpi=$organization->getGroupNpi();

                $cont_con = 1;
                $year='2021';
                $dateF = substr($year, 2, 2);
                $consecutive = "";
                if ($cont_con < 10) {
                    $consecutive = "0" . $cont_con;
                } else {
                    $consecutive = $cont_con;
                }

                $track_number = "";
                $org_track_number = "";
                $ID_pro = $organization->getId();
                $id_length = strlen($ID_pro);

                if ($id_length == 1) {
                    $ID_pro = "0000" . $ID_pro;
                }
                if ($id_length == 2) {
                    $ID_pro = "000" . $ID_pro;
                }
                if ($id_length == 3) {
                    $ID_pro = "00" . $ID_pro;
                }
                if ($id_length == 4) {
                    $ID_pro = "0" . $ID_pro;
                }

                $orgClasifications = $organization->getOrgSpecialtiesArray();
                $organization_type=$organization->getProviderType();
                $type3 = false;

                if ($orgClasifications != null) {
                    foreach ($orgClasifications as $orgC) {
                        if ($orgC->getName() == "State Mental Hospital" or $orgC->getName()=="General Hospital") {
                            $type3 = true;
                        }
                    }
                }

                $org_track_number="";
                if ($organization_type == "Individual") {
                    $org_track_number = "COV4BSN" . $dateF . $consecutive . $ID_pro;
                }

                if ($organization_type == "Organization" or $organization_type == "Group") {
                    $org_track_number = "COV5BSN" . $dateF . $consecutive . $ID_pro;
                }

                $location_name=$organization->getName();

                if($type3){
                    $location_name="";
                }

                if($organization_type!="Individual"){
                    if ($address != null) {
                        foreach ($address as $addr) {

                            if($addr->getRegion()==6 or $addr->getRegion()==7 or $addr->getRegion()==11 or $param_region==2){
                                $addr_pass = true;
                                if (strpos($addr->getStreet(), 'BOX') == true or strpos($addr->getStreet(), 'Box') == true or strpos($addr->getStreet(), 'box') == true or $addr->getUsState()!="FL") {
                                    $addr_pass = false;
                                }

                                if($addr->getId()==1274){
                                    $addr_pass = true;
                                }

                                if ($addr_pass == true) {
                                    $countyObj = $addr->getCountyMatch();
                                    $county_code = "";
                                    if ($countyObj != null) {
                                        $county_code = $countyObj->getCountyCode();
                                    }

                                    //age min
                                    $age_min = $addr->getOfficeAgeLimit();
                                    if ($age_min == "") {
                                        $age_min = "00Y";
                                    } else {
                                        if (strlen($age_min) == 1) {
                                            $age_min = "0" . $age_min . "Y";
                                        } else {
                                            $age_min = $age_min . "Y";
                                        }
                                    }

                                    //age max
                                    $age_max = $addr->getOfficeAgeLimitMax();
                                    if ($age_max == "") {
                                        $age_max = "99Y";
                                    } else {
                                        if (strlen($age_max) == 1) {
                                            $age_max = "0" . $age_max . "Y";
                                        } else {
                                            $age_max = $age_max . "Y";
                                        }
                                    }

                                    if($age_max=="00Y"){
                                        $age_max="99Y";
                                    }

                                    $phone_number = $addr->getPhoneNumber();
                                    $phone_number = str_replace('-', "", $phone_number);

                                    $has_weekend = "N";
                                    $has_after_hour = "N";
                                    $cont__after_hour=0;
                                    $bussinesOurs = $addr->getBusinessHours();

                                    if ($bussinesOurs != "") {
                                        $bussinesOurs = substr($bussinesOurs, 1);
                                        $bussinesOurs = substr($bussinesOurs, 0, -1);

                                        $bussinesOurs = str_replace('{', '', $bussinesOurs);
                                        $bussinesOurs = str_replace('}', '', $bussinesOurs);
                                        $bussinesOurs = str_replace('"', '', $bussinesOurs);

                                        $daysHours = explode(",", $bussinesOurs);

                                        $dayActiveSun = explode(":", $daysHours[18])[1];
                                        $dayActiveSat = explode(":", $daysHours[15])[1];
                                        if ($dayActiveSun == "true" or $dayActiveSat == "true") {
                                            $has_weekend = "Y";
                                        }

                                        //sunday
                                        $dayActive = explode(":", $daysHours[18])[1];
                                        $dayTill = explode(":", $daysHours[20]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $tillsun = intval($date24Till);
                                            if ($tillsun > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //mon
                                        $dayActive = explode(":", $daysHours[0])[1];
                                        $dayTill = explode(":", $daysHours[2]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //tue
                                        $dayActive = explode(":", $daysHours[3])[1];
                                        $dayTill = explode(":", $daysHours[5]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //wed
                                        $dayActive = explode(":", $daysHours[6])[1];
                                        $dayTill = explode(":", $daysHours[8]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //thu
                                        $dayActive = explode(":", $daysHours[9])[1];
                                        $dayTill = explode(":", $daysHours[11]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //fri
                                        $dayActive = explode(":", $daysHours[12])[1];
                                        $dayTill = explode(":", $daysHours[14]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //sat
                                        $dayActive = explode(":", $daysHours[15])[1];
                                        $dayTill = explode(":", $daysHours[17]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }
                                    }

                                    if ($cont__after_hour > 0) {
                                        $has_after_hour = "Y";
                                    }

                                    $has_wheelchairs="N";
                                    if($addr->getWheelchair()==1){
                                        $has_wheelchairs="Y";
                                    }

                                    $taxonomy_code="";

                                    if ($npi != "") {
                                        $pmls = $this->em->getRepository('App\Entity\PML2')->findBy(array('colN' => $npi,'colW'=>"A",'colL'=>'ENROLLMENT'));
                                        if ($pmls != null) {
                                            foreach ($pmls as $pml) {
                                                $orgTypeCode = $pml->getColD();
                                                $pg_providerType=$data['provider_type'];
                                                $pg_providerType=substr($pg_providerType,1,2);
                                                if($orgTypeCode==$pg_providerType){
                                                    $taxonomy_code = $pml->getColF();
                                                    break;
                                                }
                                            }
                                        }
                                    }

                                    if($taxonomy_code==""){
                                        $org_taxonomies=$this->em->getRepository('App\Entity\OrganizationTaxonomy')->findBy(array('organization'=>$org_id));

                                        if($org_taxonomies!=null){
                                            foreach ($org_taxonomies as $org_taxonomy){
                                                $taxonomy_code=$org_taxonomy->getTaxonomy()->getCode();
                                            }
                                        }
                                    }

                                    $hasEHR = "N";
                                    if ($addr->getSupportEHR() == true) {
                                        $hasEHR = "Y";
                                    }

                                    $enddate=$data['end_date'];
                                    $org_track_number_e = $org_track_number . $addr->getFccTrackNumber();
                                    $record_tracking_number_array[]=$org_track_number_e;
                                    $record=[
                                        'col_a'=>$org_track_number_e,
                                        'col_b'=>'',
                                        'col_c'=>$data['record_tracking_number'],
                                        'col_d'=>'',
                                        'col_e'=>$npi,
                                        'col_f'=>$data['start_date'],
                                        'col_g'=>$enddate,
                                        'col_h'=>$location_name,
                                        'col_i'=>$addr->getStreet(),
                                        'col_j'=>$addr->getSuiteNumber(),
                                        'col_k'=>$addr->getCity(),
                                        'col_l'=>$addr->getUsState(),
                                        'col_m'=>$addr->getZipCode(),
                                        'col_n'=>$county_code,
                                        'col_o'=>$phone_number,
                                        'col_q'=>'N',
                                        'col_r'=>'Y',
                                        'col_s'=>'N',
                                        'col_t'=>'B',
                                        'col_u'=>$age_min,
                                        'col_v'=>$age_max,
                                        'col_w'=>$has_after_hour,
                                        'col_x'=>$has_weekend,
                                        'col_y'=>$has_wheelchairs,
                                        'col_z'=>$data['primary_specialty'],
                                        'col_aa'=>$languages_code,
                                        'col_af'=>$taxonomy_code,
                                        'col_ag'=>$hasEHR,
                                    ];

                                    $new_data=new AETDataSL();
                                    $new_data->setData($record);
                                    $new_data->setStatus($org_data->getStatus());
                                    $new_data->setNpi($npi);
                                    $this->em->persist($new_data);

                                    $io->writeln($cont." ".$organization->getName()." ".$addr->getRegion());
                                    $cont++;
                                }
                            }
                        }
                    }
                }
            }

            $providers=$this->em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0,'organization'=>$organization->getId()));
            foreach ($providers as $provider) {
                if (in_array($provider->getId(), $providersIdsPG)) {
                    $fcc_data_pg=$this->em->getRepository('App:AETData')->findOneBy(array('provider_id'=>$provider->getId()));
                    $address_p=$provider->getBillingAddress();
                    $pgData=$fcc_data_pg->getData();

                    $track_number=$pgData['record_tracking_number'];
                    $npi=$provider->getNpiNumber();

                    $on_regions=false;
                    foreach ($address_p as $addr_p_t){
                        if($addr_p_t->getRegion()==6 or $addr_p_t->getRegion()==7 or $addr_p_t->getRegion()==11 or $param_region==2){
                            $on_regions=true;
                        }
                    }

                    if($on_regions==true){
                        $taxonomy_code="";
                        if ($npi != "") {
                            $pmls = $this->em->getRepository('App\Entity\PML2')->findBy(array('colN' => $npi,'colW'=>"A",'colL'=>'ENROLLMENT'));
                            if ($pmls != null) {
                                foreach ($pmls as $pml) {
                                    $orgTypeCode = $pml->getColD();
                                    $pg_providerType=$data['provider_type'];
                                    $pg_providerType=substr($pg_providerType,1,2);
                                    if($orgTypeCode==$pg_providerType){
                                        $taxonomy_code = $pml->getColF();
                                        break;
                                    }
                                }
                            }
                        }

                        if($taxonomy_code==""){
                            $pro_taxonomies=$provider->getTaxonomyCodes();

                            if($pro_taxonomies!=null){
                                foreach ($pro_taxonomies as $pro_taxonomy){
                                    $taxonomy_code=$pro_taxonomy->getCode();
                                }
                            }
                        }

                        $specialtiesCodes=$pgData['primary_specialty'];
                        foreach ($address_p as $addr_p){
                            if($addr_p->getOrganization()->getId()==$organization->getId() and $addr_p->getUsState()=="FL"){
                                if($addr_p->getRegion()==6 or $addr_p->getRegion()==7 or $addr_p->getRegion()==11 or $param_region==2){

                                    $sl_record_tracking_number="";
                                    $group_location_track_number=$org_track_number.$addr_p->getFccTrackNumber();

                                    $slrtn=$this->em->getRepository('App\Entity\ProviderAddrFCCTN')->findOneBy(
                                        array('provider'=>$provider->getId(),'address'=>$addr_p->getId()));
                                    if($slrtn!=null){
                                        $sl_record_tracking_number=$slrtn->getTrackNumber();
                                    }

                                    $location_name="";
                                    $street="";
                                    $suite="";
                                    $city="";
                                    $state="";
                                    $zipCode="";
                                    $countyCode="";
                                    $phone_number="";

                                    $hasEHR = "";
                                    if($organization_type=="Individual"){
                                        $addr_pro_record="COV4".substr($track_number,4).$sl_record_tracking_number;
                                        $group_location_track_number="";
                                        $location_name=$organization->getName();
                                        $street=$addr_p->getStreet();
                                        $suite=$addr_p->getSuiteNumber();
                                        $city=$addr_p->getCity();
                                        $state=$addr_p->getUsState();
                                        $zipCode=$addr_p->getZipCode();

                                        $phone_number = $addr_p->getPhoneNumber();
                                        $phone_number = str_replace('-', "", $phone_number);

                                        $countyObj = $addr_p->getCountyMatch();
                                        if ($countyObj != null) {
                                            $countyCode = $countyObj->getCountyCode();
                                        }

                                        if ($addr_p->getSupportEHR() == true) {
                                            $hasEHR = "Y";
                                        }else{
                                            $hasEHR = "N";
                                        }
                                    }else{
                                        $addr_pro_record="COV6".substr($track_number,4).$sl_record_tracking_number;
                                    }

                                    if($type3==true){
                                        $addr_pro_record="COV4".substr($track_number,4).$sl_record_tracking_number;
                                        $group_location_track_number="";
                                    }

                                    $has_wheelchairs="N";
                                    if($addr_p->getWheelchair()==1){
                                        $has_wheelchairs="Y";
                                    }

                                    //age min
                                    $age_min = $addr_p->getOfficeAgeLimit();
                                    if ($age_min == "") {
                                        $age_min = "00Y";
                                    } else {
                                        if (strlen($age_min) == 1) {
                                            $age_min = "0" . $age_min . "Y";
                                        } else {
                                            $age_min = $age_min . "Y";
                                        }
                                    }

                                    //age max
                                    $age_max = $addr_p->getOfficeAgeLimitMax();
                                    if ($age_max == "") {
                                        $age_max = "00Y";
                                    } else {
                                        if (strlen($age_max) == 1) {
                                            $age_max = "0" . $age_max . "Y";
                                        } else {
                                            $age_max = $age_max . "Y";
                                        }
                                    }

                                    $has_weekend = "N";
                                    $has_after_hour = "N";

                                    $bussinesOurs = $addr_p->getBusinessHours();
                                    if ($bussinesOurs != "") {
                                        $bussinesOurs = substr($bussinesOurs, 1);
                                        $bussinesOurs = substr($bussinesOurs, 0, -1);

                                        $bussinesOurs = str_replace('{', '', $bussinesOurs);
                                        $bussinesOurs = str_replace('}', '', $bussinesOurs);
                                        $bussinesOurs = str_replace('"', '', $bussinesOurs);

                                        $daysHours = explode(",", $bussinesOurs);

                                        $dayActiveSun = explode(":", $daysHours[18])[1];
                                        $dayActiveSat = explode(":", $daysHours[15])[1];
                                        if ($dayActiveSun == "true" or $dayActiveSat == "true") {
                                            $has_weekend = "Y";
                                        }

                                        $cont__after_hour = 0;
                                        //sunday
                                        $dayActive = explode(":", $daysHours[18])[1];
                                        $dayTill = explode(":", $daysHours[20]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $tillsun = intval($date24Till);
                                            if ($tillsun > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //mon
                                        $dayActive = explode(":", $daysHours[0])[1];
                                        $dayTill = explode(":", $daysHours[2]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //tue
                                        $dayActive = explode(":", $daysHours[3])[1];
                                        $dayTill = explode(":", $daysHours[5]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //wed
                                        $dayActive = explode(":", $daysHours[6])[1];
                                        $dayTill = explode(":", $daysHours[8]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //thu
                                        $dayActive = explode(":", $daysHours[9])[1];
                                        $dayTill = explode(":", $daysHours[11]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //fri
                                        $dayActive = explode(":", $daysHours[12])[1];
                                        $dayTill = explode(":", $daysHours[14]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }

                                        //sat
                                        $dayActive = explode(":", $daysHours[15])[1];
                                        $dayTill = explode(":", $daysHours[17]);

                                        if ($dayActive == "true") {
                                            $dateTill = $dayTill[1] . $dayTill[2];
                                            $date24Till = $this->convertTo24Hours($dateTill);
                                            $date24Till = substr($date24Till, 0, 2);
                                            $till = intval($date24Till);
                                            if ($till > 18) {
                                                $cont__after_hour++;
                                            }
                                        }
                                    }

                                    if ($cont__after_hour > 0) {
                                        $has_after_hour = "Y";
                                    }

                                    $record_tracking_number_array[]=$addr_pro_record;

                                    $record=[
                                        'col_a'=>$addr_pro_record,
                                        'col_b'=>'',
                                        'col_c'=>$track_number,
                                        'col_d'=>$group_location_track_number,
                                        'col_e'=>$npi,
                                        'col_f'=>$pgData['start_date'],
                                        'col_g'=>'',
                                        'col_h'=>$location_name,
                                        'col_i'=>$street,
                                        'col_j'=>$suite,
                                        'col_k'=>$city,
                                        'col_l'=>$state,
                                        'col_m'=>$zipCode,
                                        'col_n'=>$countyCode,
                                        'col_o'=>$phone_number,
                                        'col_q'=>'N',
                                        'col_r'=>'Y',
                                        'col_s'=>'N',
                                        'col_t'=>'B',
                                        'col_u'=>$age_min,
                                        'col_v'=>$age_max,
                                        'col_w'=>$has_after_hour,
                                        'col_x'=>$has_weekend,
                                        'col_y'=>$has_wheelchairs,
                                        'col_z'=>$pgData['primary_specialty'],
                                        'col_aa'=>'',
                                        'col_af'=>$taxonomy_code,
                                        'col_ag'=>$hasEHR,
                                    ];

                                    $new_data=new AETDataSL();
                                    $new_data->setData($record);
                                    $new_data->setStatus($fcc_data_pg->getStatus());
                                    $new_data->setNpi($npi);
                                    $this->em->persist($new_data);

                                    $io->writeln($cont." ".$provider->getNpiNumber()." ".$provider->getFirstName()." ".$provider->getLastName()." ".$track_number. " ".$addr_p->getRegion());
                                    $cont++;
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->em->flush();

        $providers=$this->em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));
        foreach ($providers as $provider){
            if(in_array($provider->getId(),$providersIdsPG4)){
                $type3=true;
                $languagesId=[];
                $organization_languages_code="";
                $languages=$provider->getLanguages();
                if($languages!=null){
                    foreach ($languages as $language){
                        $code=$language->getCode();
                        if($code!=""){
                            if(!in_array($code,$languagesId) and $language->getId()!=1){
                                if($code=='02' or $code=='03' or $code=='04' or $code=='05' or $code=='06' or $code=='07' or $code=='08' or $code=='09'){
                                    $languagesId[]=$code;
                                    $organization_languages_code.=$code."~";
                                }
                            }
                        }
                    }
                }

                $aet_data_pg=$this->em->getRepository('App:AETData')->findOneBy(array('provider_id'=>$provider->getId()));
                $address_p=$provider->getBillingAddress();
                $pgData=$aet_data_pg->getData();

                $track_number=$pgData['record_tracking_number'];
                $npi=$provider->getNpiNumber();

                $taxonomy_code="";
                if ($npi != "") {
                    $pmls = $this->em->getRepository('App\Entity\PML2')->findBy(array('colN' => $npi,'colW'=>"A", 'colL'=>'ENROLLMENT'));
                    if ($pmls != null) {
                        foreach ($pmls as $pml) {
                            $orgTypeCode = $pml->getColD();
                            $pg_providerType=$data['provider_type'];
                            $pg_providerType=substr($pg_providerType,1,2);
                            if($orgTypeCode==$pg_providerType){
                                $taxonomy_code = $pml->getColF();
                                break;
                            }
                        }
                    }
                }

                if($taxonomy_code==""){
                    $pro_taxonomies=$provider->getTaxonomyCodes();

                    if($pro_taxonomies!=null){
                        foreach ($pro_taxonomies as $pro_taxonomy){
                            $taxonomy_code=$pro_taxonomy->getCode();
                        }
                    }
                }

                $organization=$provider->getOrganization();
                foreach ($address_p as $addr_p){
                    if($addr_p->getRegion()==6 or $addr_p->getRegion()==7 or $addr_p->getRegion()==11 or $param_region==2){
                        if($addr_p->getOrganization()->getId()==$organization->getId()){
                            $sl_record_tracking_number="";
                            $slrtn=$this->em->getRepository('App\Entity\ProviderAddrFCCTN')->findOneBy(
                                array('provider'=>$provider->getId(),'address'=>$addr_p->getId()));
                            if($slrtn!=null){
                                $sl_record_tracking_number=$slrtn->getTrackNumber();
                            }

                            $countyCode="";

                            $addr_pro_record="COV4".substr($track_number,4).$sl_record_tracking_number;
                            $group_location_track_number="";
                            $location_name=$organization->getName();
                            $street=$addr_p->getStreet();
                            $suite=$addr_p->getSuiteNumber();
                            $city=$addr_p->getCity();
                            $state=$addr_p->getUsState();
                            $zipCode=$addr_p->getZipCode();

                            $phone_number = $addr_p->getPhoneNumber();
                            $phone_number = str_replace('-', "", $phone_number);

                            $countyObj = $addr_p->getCountyMatch();
                            if ($countyObj != null) {
                                $countyCode = $countyObj->getCountyCode();
                            }

                            if ($addr_p->getSupportEHR() == true) {
                                $hasEHR = "Y";
                            }else{
                                $hasEHR = "N";
                            }

                            if($type3==true){
                                $addr_pro_record="COV4".substr($track_number,4).$sl_record_tracking_number;
                                $group_location_track_number="";
                            }

                            $has_wheelchairs="N";
                            if($addr_p->getWheelchair()==1){
                                $has_wheelchairs="Y";
                            }

                            //age min
                            $age_min = $addr_p->getOfficeAgeLimit();
                            if ($age_min == "") {
                                $age_min = "00Y";
                            } else {
                                if (strlen($age_min) == 1) {
                                    $age_min = "0" . $age_min . "Y";
                                } else {
                                    $age_min = $age_min . "Y";
                                }
                            }

                            //age max
                            $age_max = $addr_p->getOfficeAgeLimitMax();
                            if ($age_max == "") {
                                $age_max = "00Y";
                            } else {
                                if (strlen($age_max) == 1) {
                                    $age_max = "0" . $age_max . "Y";
                                } else {
                                    $age_max = $age_max . "Y";
                                }
                            }

                            if($age_max=="00Y"){
                                $age_max="99Y";
                            }

                            $has_weekend = "N";
                            $has_after_hour = "N";
                            $bussinesOurs = $addr_p->getBusinessHours();
                            if ($bussinesOurs != "") {
                                $bussinesOurs = substr($bussinesOurs, 1);
                                $bussinesOurs = substr($bussinesOurs, 0, -1);

                                $bussinesOurs = str_replace('{', '', $bussinesOurs);
                                $bussinesOurs = str_replace('}', '', $bussinesOurs);
                                $bussinesOurs = str_replace('"', '', $bussinesOurs);

                                $daysHours = explode(",", $bussinesOurs);

                                $dayActiveSun = explode(":", $daysHours[18])[1];
                                $dayActiveSat = explode(":", $daysHours[15])[1];
                                if ($dayActiveSun == "true" or $dayActiveSat == "true") {
                                    $has_weekend = "Y";
                                }

                                $cont__after_hour = 0;
                                //sunday
                                $dayActive = explode(":", $daysHours[18])[1];
                                $dayTill = explode(":", $daysHours[20]);

                                if ($dayActive == "true") {
                                    $dateTill = $dayTill[1] . $dayTill[2];
                                    $date24Till = $this->convertTo24Hours($dateTill);
                                    $date24Till = substr($date24Till, 0, 2);
                                    $tillsun = intval($date24Till);
                                    if ($tillsun > 18) {
                                        $cont__after_hour++;
                                    }
                                }

                                //mon
                                $dayActive = explode(":", $daysHours[0])[1];
                                $dayTill = explode(":", $daysHours[2]);

                                if ($dayActive == "true") {
                                    $dateTill = $dayTill[1] . $dayTill[2];
                                    $date24Till = $this->convertTo24Hours($dateTill);
                                    $date24Till = substr($date24Till, 0, 2);
                                    $till = intval($date24Till);
                                    if ($till > 18) {
                                        $cont__after_hour++;
                                    }
                                }

                                //tue
                                $dayActive = explode(":", $daysHours[3])[1];
                                $dayTill = explode(":", $daysHours[5]);

                                if ($dayActive == "true") {
                                    $dateTill = $dayTill[1] . $dayTill[2];
                                    $date24Till = $this->convertTo24Hours($dateTill);
                                    $date24Till = substr($date24Till, 0, 2);
                                    $till = intval($date24Till);
                                    if ($till > 18) {
                                        $cont__after_hour++;
                                    }
                                }

                                //wed
                                $dayActive = explode(":", $daysHours[6])[1];
                                $dayTill = explode(":", $daysHours[8]);

                                if ($dayActive == "true") {
                                    $dateTill = $dayTill[1] . $dayTill[2];
                                    $date24Till = $this->convertTo24Hours($dateTill);
                                    $date24Till = substr($date24Till, 0, 2);
                                    $till = intval($date24Till);
                                    if ($till > 18) {
                                        $cont__after_hour++;
                                    }
                                }

                                //thu
                                $dayActive = explode(":", $daysHours[9])[1];
                                $dayTill = explode(":", $daysHours[11]);

                                if ($dayActive == "true") {
                                    $dateTill = $dayTill[1] . $dayTill[2];
                                    $date24Till = $this->convertTo24Hours($dateTill);
                                    $date24Till = substr($date24Till, 0, 2);
                                    $till = intval($date24Till);
                                    if ($till > 18) {
                                        $cont__after_hour++;
                                    }
                                }

                                //fri
                                $dayActive = explode(":", $daysHours[12])[1];
                                $dayTill = explode(":", $daysHours[14]);

                                if ($dayActive == "true") {
                                    $dateTill = $dayTill[1] . $dayTill[2];
                                    $date24Till = $this->convertTo24Hours($dateTill);
                                    $date24Till = substr($date24Till, 0, 2);
                                    $till = intval($date24Till);
                                    if ($till > 18) {
                                        $cont__after_hour++;
                                    }
                                }

                                //sat
                                $dayActive = explode(":", $daysHours[15])[1];
                                $dayTill = explode(":", $daysHours[17]);

                                if ($dayActive == "true") {
                                    $dateTill = $dayTill[1] . $dayTill[2];
                                    $date24Till = $this->convertTo24Hours($dateTill);
                                    $date24Till = substr($date24Till, 0, 2);
                                    $till = intval($date24Till);
                                    if ($till > 18) {
                                        $cont__after_hour++;
                                    }
                                }
                            }

                            if ($cont__after_hour > 0) {
                                $has_after_hour = "Y";
                            }
                            $record_tracking_number_array[]=$addr_pro_record;

                            $record=[
                                'col_a'=>$addr_pro_record,
                                'col_b'=>'',
                                'col_c'=>$track_number,
                                'col_d'=>$group_location_track_number,
                                'col_e'=>$npi,
                                'col_f'=>$pgData['start_date'],
                                'col_g'=>'',
                                'col_h'=>$location_name,
                                'col_i'=>$street,
                                'col_j'=>$suite,
                                'col_k'=>$city,
                                'col_l'=>$state,
                                'col_m'=>$zipCode,
                                'col_n'=>$countyCode,
                                'col_o'=>$phone_number,
                                'col_q'=>'N',
                                'col_r'=>'Y',
                                'col_s'=>'N',
                                'col_t'=>'B',
                                'col_u'=>$age_min,
                                'col_v'=>$age_max,
                                'col_w'=>$has_after_hour,
                                'col_x'=>$has_weekend,
                                'col_y'=>$has_wheelchairs,
                                'col_z'=>$pgData['primary_specialty'],
                                'col_aa'=>'',
                                'col_af'=>$taxonomy_code,
                                'col_ag'=>$hasEHR,
                            ];

                            $new_data=new AETDataSL();
                            $new_data->setData($record);
                            $new_data->setStatus('New');
                            $new_data->setNpi($npi);
                            $this->em->persist($new_data);
                            $this->em->flush();

                            $io->writeln($cont." ".$provider->getNpiNumber()." Type 4"." ".$addr_p->getRegion());
                            $cont++;

                        }
                    }
                }
            }
        }

        //verify all record on log
        $sl_logs=$this->em->getRepository('App:AETNAPNVSLLog')->findAll();
        $date_now = date('d-m-Y');
        $last_date = strtotime('-3 day', strtotime($date_now));
        $last_date = date('Ymd', $last_date);

        foreach ($sl_logs as $sl_log){
            $track_number_log=$sl_log->getRecordTrackingNumber();
            if(!in_array($track_number_log,$record_tracking_number_array) and $sl_log->getStatus() != 'removed'){
                $record=[
                    'col_a'=>$sl_log->getRecordTrackingNumber(),
                    'col_b'=>'',
                    'col_c'=>$sl_log->getProviderGroupTrackingNumber(),
                    'col_d'=>$sl_log->getGroupLocationTrackingNumber(),
                    'col_e'=>$sl_log->getNPINumber(),
                    'col_f'=>$sl_log->getStartDate(),
                    'col_g'=>$last_date,
                    'col_h'=>$sl_log->getLocationName(),
                    'col_i'=>$sl_log->getAddressLine1(),
                    'col_j'=>$sl_log->getAddressLine2(),
                    'col_k'=>$sl_log->getCity(),
                    'col_l'=>$sl_log->getState(),
                    'col_m'=>$sl_log->getZipCode(),
                    'col_n'=>$sl_log->getCountyCode(),
                    'col_o'=>$sl_log->getPhoneNumber(),
                    'col_q'=>'N',
                    'col_r'=>'Y',
                    'col_s'=>'N',
                    'col_t'=>'B',
                    'col_u'=>$sl_log->getAgeRestrictionLow(),
                    'col_v'=>$sl_log->getAgeRestrictionHigh(),
                    'col_w'=>$sl_log->getHasAfterHours(),
                    'col_x'=>$sl_log->getHasWeekendHolidayHours(),
                    'col_y'=>$sl_log->getHasWheelchairAccess(),
                    'col_z'=>$sl_log->getSpecialties(),
                    'col_aa'=>'',
                    'col_af'=>$sl_log->getTaxonomies(),
                    'col_ag'=>$sl_log->getEHR(),
                ];

                $new_data=new AETDataSL();
                $new_data->setData($record);
                $new_data->setStatus('to_remove');
                $new_data->setNpi($sl_log->getNPINumber());
                $this->em->persist($new_data);
                $this->em->flush();

                $io->writeln($track_number_log." to remove");
            }
        }
        $io->success('All completed.');
    }

    private function convertTo24Hours($date){
        $newDate="";
        if (strpos($date, 'am') == true) {
            $newDate=substr($date,0,-2);

            if(strlen($newDate)<4){
                $newDate='0'.$newDate;
            }

        }else{
            $newDate=substr($date,0,-2);
            if($newDate=="1200"){$newDate="1200";}if($newDate=="1230"){$newDate="1230";}
            if($newDate=="100"){$newDate="1300";}if($newDate=="130"){$newDate="1330";}
            if($newDate=="200"){$newDate="1400";}if($newDate=="230"){$newDate="1430";}
            if($newDate=="300"){$newDate="1500";}if($newDate=="330"){$newDate="1530";}
            if($newDate=="400"){$newDate="1600";}if($newDate=="430"){$newDate="1630";}
            if($newDate=="500"){$newDate="1700";}if($newDate=="530"){$newDate="1730";}
            if($newDate=="600"){$newDate="1800";}if($newDate=="630"){$newDate="1830";}
            if($newDate=="700"){$newDate="1900";}if($newDate=="730"){$newDate="1930";}
            if($newDate=="800"){$newDate="2000";}if($newDate=="830"){$newDate="2030";}
            if($newDate=="900"){$newDate="2100";}if($newDate=="930"){$newDate="2130";}
            if($newDate=="1000"){$newDate="2200";}if($newDate=="1030"){$newDate="2230";}
            if($newDate=="1100"){$newDate="2300";}if($newDate=="1130"){$newDate="2330";}
        }

        return $newDate;
    }
}
