<?php

namespace App\Command;

use App\Entity\ProviderAddrFCCTN;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FCCTrackNumberCommand extends Command
{
    protected static $defaultName = 'fcc:set:tracknumber';
    private $em;
    private $conn;

    public function __construct( EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $organizations=$this->em->getRepository('App\Entity\Organization')->findAll();
        foreach ($organizations as $organization){
            $cont=1;
            $addresses=$this->em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));

            if($addresses!=null){
                foreach ($addresses as $address){
                    $fcc_track_number="";
                    if(strlen($cont)==1){
                        $fcc_track_number='00'.$cont;
                    }
                    if(strlen($cont)==2){
                        $fcc_track_number='0'.$cont;
                    }

                    $address->setFccTrackNumber($fcc_track_number);
                    $this->em->persist($address);
                    $cont++;

                    $output->writeln($organization->getName()." ".$address->getStreet());
                }
                $organization->setAddrsIdTrackNumber($cont);
                $this->em->flush();
            }
        }

        $output->writeln('Deleting current Records for Providers');
        $records=$this->em->getRepository('App\Entity\ProviderAddrFCCTN')->findAll();
        foreach ($records as $record){
            $this->em->remove($record);
        }
        $this->em->flush();

        $cont_pro=1;
        $providers=$this->em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));
        foreach ($providers as $provider) {
            $cont = 1;
            $addresses = $provider->getBillingAddress();
            $io->writeln(count($addresses));
            if($addresses!=null) {
                $organization = $provider->getOrganization();

                foreach ($addresses as $addr){
                    if($addr->getOrganization()->getId()==$organization->getId()){
                        $providerADDRFCC=new ProviderAddrFCCTN();
                        $providerADDRFCC->setProvider($provider);
                        $providerADDRFCC->setAddress($addr);
                        if(strlen($cont)==1){
                            $fcc_track_number='00'.$cont;
                        }
                        if(strlen($cont)==2){
                            $fcc_track_number='0'.$cont;
                        }
                        $providerADDRFCC->setTrackNumber($fcc_track_number);
                        $this->em->persist($providerADDRFCC);
                        $this->em->flush();
                        $cont++;
                    }
                }

                $cont--;
                $provider->setAddrsIdTrackNumber($cont);
                $this->em->persist($provider);
                $output->writeln($cont_pro." ".$provider->getNpiNumber());
                $cont_pro++;
            }
        }

        $this->em->flush();
        $io->success('Process Completed');
        return 0;
    }
}
