<?php

namespace App\Command;

use App\Entity\BillingAddress;
use App\Entity\FCCRosterData;
use App\Entity\FCCRosterLog;
use App\Utils\My_Mcript;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Entity\FCCData;
use App\Entity\FCCDataOrg;
use App\Entity\Organization;
use App\Entity\PNVSLLog;
use App\Entity\Provider;

class FCCRosterProcessCommand extends Command
{
    protected static $defaultName = 'fcc:roster:start';
    private $em;
    private $conn;
    private $private_key;
    private $encoder;
    private $params;

    public function __construct( EntityManagerInterface $em, ParameterBagInterface $params)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
        $this->params = $params;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $this->private_key=$this->params->get('private_key');
        $this->encoder=new My_Mcript($this->private_key);

        //get all data from PG record , this data not need validations
        $fcc_data=$this->em->getRepository('App\Entity\FCCData')->findAll();
        $fcc_data_org=$this->em->getRepository('App\Entity\FCCDataOrg')->findAll();
        $roster_los=$this->em->getRepository('App\Entity\FCCRosterLog')->findAll();

        //remove all fccroster_data records
        $fcc_roster_data=$this->em->getRepository('App\Entity\FCCRosterData')->findAll();
        if($fcc_roster_data){
            foreach ($fcc_roster_data as $item){
                $this->em->remove($item);
            }
            $this->em->flush();
        }

        $cont=1;

        foreach ($fcc_data as $datum){
            $endDateP=$datum->getData()['end_date'];

            $provider_id=$datum->getProviderId();
            $npi=$datum->getNpi();

            $provider=$this->em->getRepository('App\Entity\Provider')->find($provider_id);

            $address=$provider->getBillingAddress();
            $this->private_key=$this->params->get('private_key');
            $encoder=new My_Mcript($this->private_key);
            $hospitals=$provider->getHospitals();

            $ahca_id="";
            if($hospitals!=null){
                foreach ($hospitals as $hospital){
                    $ahca_id=$hospital->getAhcaNumber();
                }
            }

            $social=$encoder->decryptthis($provider->getSocial());
            $orga=$provider->getOrganization();
            if($orga->getTinNumber()!=""){
                $irs=$orga->getTinNumber();
                $irs_type="T";
            }else{
                $irs=$social;
                $irs_type="S";
            }

            $irs=str_replace("-","",$irs);
            if(strlen($irs)==8){
                $irs="0".$irs;
            }

            $key_cont=1;
            $isDr="";
            $degrees=$provider->getDegree();
            if($degrees!=null){
                foreach ($degrees as $degree){
                    $isDr=$degree->getName();
                }
            }

            $genderCode=$provider->getGender();
            if($genderCode!=""){
                $genderCode=substr($genderCode,0,1);
                $genderCode=strtoupper($genderCode);
            }else{
                $genderCode="";
            }

            $language1="";$language2="";$language3="";$language4="";$language5="";$language6="";$language7="";$language8="";
            $languages=$provider->getLanguages();
            if($languages!=null){
                if(count($languages)>0){
                    $language1=$languages[0]->getLangCd();
                }
                if(count($languages)>1){
                    $language2=$languages[1]->getLangCd();
                }
                if(count($languages)>2){
                    $language3=$languages[2]->getLangCd();
                }
                if(count($languages)>3){
                    $language4=$languages[3]->getLangCd();
                }
                if(count($languages)>4){
                    $language5=$languages[4]->getLangCd();
                }
                if(count($languages)>5){
                    $language6=$languages[5]->getLangCd();
                }
                if(count($languages)>6){
                    $language7=$languages[6]->getLangCd();
                }
                if(count($languages)>7){
                    $language8=$languages[7]->getLangCd();
                }
            }

            if($language1==""){
                $language1="ENG";
            }

            $taxonomyCode1="";$taxonomyCode2="";$taxonomyCode3="";
            $taxonomiesCode=array();
            $taxonomies=array();
            $medicaids=array();
            $medicaid=$datum->getData()['provider_id'];
            $ind_or_group="I";
            $providerTypesArray=array();

            $pg_record=$datum->getData();

            $specialty1 = "";
            $specialty2 = "";
            $specialty3 = "";
            $specialty4 = "";
            $specialtiesArray=array();
            $stateLic=$datum->getData()['license_number'];
            $provider_type=$datum->getData()['provider_type'];

            $pmls=$this->em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>'A','colL'=>'ENROLLMENT'));
            foreach ($pmls as $pml){
                $spe=$pml->getColE();
                $tax=$pml->getColF();
                if($spe!=""){
                    if(!in_array($spe,$specialtiesArray)){
                        $specialtiesArray[]=$spe;
                    }
                }
                if($tax!=""){
                    if(!in_array($tax,$taxonomiesCode)){
                        $taxonomiesCode[]=$tax;
                    }
                }
            }

            if(count($taxonomiesCode)>=1){
                $taxonomyCode1=$taxonomiesCode[0];
            }
            if(count($taxonomiesCode)>=2){
                $taxonomyCode2=$taxonomiesCode[1];
            }
            if(count($taxonomiesCode)>=3){
                $taxonomyCode3=$taxonomiesCode[2];
            }

            if(count($specialtiesArray)>=1){
                $specialty1=$specialtiesArray[0]."01";
            }
            if(count($specialtiesArray)>=2){
                $specialty2=$specialtiesArray[1]."01";
            }
            if(count($specialtiesArray)>=3){
                $specialty3=$specialtiesArray[2]."01";
            }
            if(count($specialtiesArray)>=4){
                $specialty4=$specialtiesArray[3]."01";
            }

            $key_cont=1;
            $organization=$provider->getOrganization();

            if($taxonomyCode1==""){
                $org_taxonomies=$this->em->getRepository('App\Entity\OrganizationTaxonomy')->findBy(array('organization'=>$organization->getId()));
                if($org_taxonomies!=null){
                    if(count($org_taxonomies)>=1){
                        $taxonomyCode1=$org_taxonomies[0]->getTaxonomy()->getCode();
                    }
                    if(count($org_taxonomies)>=2){
                        $taxonomyCode2=$org_taxonomies[1]->getTaxonomy()->getCode();
                    }
                    if(count($org_taxonomies)>=3){
                        $taxonomyCode3=$org_taxonomies[2]->getTaxonomy()->getCode();
                    }
                }
            }

            foreach ($address as $addr){
                $street_check=$addr->getStreet();
                $addr_pass=true;
                if(stristr($street_check, 'BOX') or stristr($street_check, 'Box') or stristr($street_check, 'box') or $addr->getUsState()!="FL" or $addr->getDisabled()==1) {
                    $addr_pass=false;
                }
                if($addr_pass==true and $endDateP==""){
                    $eClaimProviderType = "SP";
                    $pro_degrees = $provider->getDegree();
                    if ($pro_degrees != null) {
                        foreach ($pro_degrees as $degree) {
                            if ($degree->getId() == 1) {
                                $eClaimProviderType = "NP";
                                break;
                            }
                            if ($degree->getId() == 19) {
                                if($specialty1!='92901' and $specialty2!='92901' and $specialty3!='92901' and $specialty4!='92901'){
                                    $eClaimProviderType = "PA";
                                    break;
                                }
                            }
                        }
                    }

                    $site_index="";
                    $key="";
                    if($key_cont<10){
                        $key=$provider->getNpiNumber()."-".'00'.$key_cont;
                        $site_index="00".$key_cont;
                    }else{
                        $key=$provider->getNpiNumber()."-".'0'.$key_cont;
                        $site_index="0".$key_cont;
                    }

                    $name=strtolower($provider->getFirstName());
                    $last=strtolower($provider->getLastName());

                    $name=ucwords ( $name );
                    $last=ucwords ( $last );

                    $exempt="";
                    if($addr->getSeePatientsAddr()==false){
                        $exempt="DE";
                    }

                    $tin = $organization->getTinNumber();
                    $Prac_IRSType="";
                    $Prac_IRS_No="";
                    if ($tin != "" and $tin!=null) {
                        $Prac_IRSType='T';
                        $Prac_IRS_No= $tin;
                    }else{
                        $tin=$this->encoder->decryptthis($provider->getSocial());
                        $Prac_IRSType='S';
                        $Prac_IRS_No= $tin;
                    }

                    $Prac_IRS_No=str_replace("-","",$Prac_IRS_No);

                    $phone_number = $addr->getPhoneNumber();
                    $phone_number = str_replace('-', "", $phone_number);

                    $ada = "N";
                    if ($addr->getWheelchair() == true) {
                        $ada = "Y";
                    }

                    $Mon_START="";
                    $Mon_END="";
                    $Tues_START="";
                    $Tues_END="";
                    $Wed_START="";
                    $Wed_END="";
                    $Thurs_START="";
                    $Thurs_END="";
                    $Fri_Start="";
                    $Fri_END="";
                    $Sat_START="";
                    $Sat_END="";
                    $Sun_START="";
                    $Sun_END="";

                    $hasWeekend="N";
                    $hasEvening="N";

                    $bussinesOurs = $addr->getBusinessHours();
                    if($bussinesOurs!=""){
                        $bussinesOurs = substr($bussinesOurs, 1);
                        $bussinesOurs = substr($bussinesOurs, 0, -1);

                        $bussinesOurs = str_replace('{', '', $bussinesOurs);
                        $bussinesOurs = str_replace('}', '', $bussinesOurs);
                        $bussinesOurs = str_replace('"', '', $bussinesOurs);

                        $daysHours = explode(",", $bussinesOurs);

                        //sunday
                        $dayActive = explode(":", $daysHours[18])[1];
                        $startHour = substr($daysHours[19], 9);
                        $tillHour = substr($daysHours[20], 9);

                        if ($dayActive == "true") {
                            $Sun_START=strtoupper($startHour);
                            $Sun_END=strtoupper($tillHour);

                            $hasWeekend="Y";
                            $hourTill=explode(':',$tillHour);
                            $hourTill=intval($hourTill[0]);
                            if($hourTill>6){
                                $hasEvening="Y";
                            }
                        }

                        //monday
                        $dayActive = explode(":", $daysHours[0])[1];
                        $startHour = substr($daysHours[1], 9);
                        $tillHour = substr($daysHours[2], 9);
                        if ($dayActive == "true") {
                            $Mon_START=strtoupper($startHour);
                            $Mon_END= strtoupper($tillHour);
                            $hourTill=explode(':',$tillHour);
                            $hourTill=intval($hourTill[0]);
                            if($hourTill>6){
                                $hasEvening="Y";
                            }
                        }

                        //tuesday
                        $dayActive = explode(":", $daysHours[3])[1];
                        $startHour = substr($daysHours[4], 9);
                        $tillHour = substr($daysHours[5], 9);
                        if ($dayActive == "true") {
                            $Tues_START=strtoupper($startHour);
                            $Tues_END=strtoupper($tillHour);
                            $hourTill=explode(':',$tillHour);
                            $hourTill=intval($hourTill[0]);
                            if($hourTill>6){
                                $hasEvening="Y";
                            }
                        }

                        //Wednesday
                        $dayActive = explode(":", $daysHours[6])[1];
                        $startHour = substr($daysHours[7], 9);
                        $tillHour = substr($daysHours[8], 9);
                        if ($dayActive == "true") {
                            $Wed_START=strtoupper($startHour);
                            $Wed_END=strtoupper($tillHour);
                            $hourTill=explode(':',$tillHour);
                            $hourTill=intval($hourTill[0]);
                            if($hourTill>6){
                                $hasEvening="Y";
                            }
                        }

                        //Thurs
                        $dayActive = explode(":", $daysHours[9])[1];
                        $startHour = substr($daysHours[10], 9);
                        $tillHour = substr($daysHours[11], 9);
                        if ($dayActive == "true") {
                            $Thurs_START=strtoupper($startHour);
                            $Thurs_END=strtoupper($tillHour);
                            $hourTill=explode(':',$tillHour);
                            $hourTill=intval($hourTill[0]);
                            if($hourTill>6){
                                $hasEvening="Y";
                            }
                        }

                        //Frid
                        $dayActive = explode(":", $daysHours[12])[1];
                        $startHour = substr($daysHours[13], 9);
                        $tillHour = substr($daysHours[14], 9);
                        if ($dayActive == "true") {
                            $Fri_Start=strtoupper($startHour);
                            $Fri_END=strtoupper($tillHour);
                            $hourTill=explode(':',$tillHour);
                            $hourTill=intval($hourTill[0]);
                            if($hourTill>6){
                                $hasEvening="Y";
                            }
                        }

                        //sat
                        $dayActive = explode(":", $daysHours[15])[1];
                        $startHour = substr($daysHours[16], 9);
                        $tillHour = substr($daysHours[17], 9);
                        if ($dayActive == "true") {
                            $Sat_START=strtoupper($startHour);
                            $Sat_END=strtoupper($tillHour);

                            $hasWeekend="Y";
                            $hourTill=explode(':',$tillHour);
                            $hourTill=intval($hourTill[0]);
                            if($hourTill>6){
                                $hasEvening="Y";
                            }
                        }

                    }

                    //get billing Address
                    $orgAddress = $this->em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $organization->getId()));

                    $payto_street = "";
                    $payto_city = "";
                    $payto_usState = "";
                    $payto_zipCode = "";
                    $payto_phoneNumber = "";

                    if (count($orgAddress) == 1) {
                        foreach ($orgAddress as $addrB) {
                            $payto_street=$addrB->getStreet();
                            $payto_city=$addrB->getCity();
                            $payto_usState=$addrB->getUsState();
                            $payto_zipCode=$addrB->getZipCode();
                            $phone_number_B = $addrB->getPhoneNumber();
                            $phone_number_B = str_replace('-', "", $phone_number_B);
                            $payto_phoneNumber=$phone_number_B;
                        }
                    }

                    if (count($orgAddress) > 1) {
                        foreach ($orgAddress as $addrB) {
                            if ($addrB->getBillingAddr() == true) {
                                $hasBilling = true;
                                $street = $addrB->getStreet();
                                $city = $addrB->getCity();
                                $usState = $addrB->getUsState();
                                $zipCode = $addrB->getZipCode();
                                $phone_number_B = $addrB->getPhoneNumber();
                                $phone_number_B = str_replace('-', "", $phone_number_B);
                            }
                        }

                        if ($hasBilling == true) {
                            $payto_street=$street;
                            $payto_city=$city;
                            $payto_usState=$usState;
                            $payto_zipCode=$zipCode;
                            $payto_phoneNumber=$phone_number_B;
                        }else{

                            foreach ($orgAddress as $addrB) {
                                $payto_street=$addrB->getStreet();
                                $payto_city=$addrB->getCity();
                                $payto_usState=$addrB->getUsState();
                                $payto_zipCode=$addrB->getZipCode();
                                $phone_number_B = $addrB->getPhoneNumber();
                                $phone_number_B = str_replace('-', "", $phone_number_B);
                                $payto_phoneNumber=$phone_number_B;
                                break;
                            }
                        }
                    }

                    $minAge=strval($addr->getOfficeAgeLimit());
                    $maxAge=strval($addr->getOfficeAgeLimitMax());

                    if(strlen($minAge)==1){
                        $minAge='0'.$minAge;
                    }
                    $minAge=$minAge."Y";

                    if(strlen($maxAge)==1){
                        $maxAge='0'.$maxAge;
                    }
                    $maxAge=$maxAge."Y";

                    if($maxAge=="00Y"){
                        $maxAge="99Y";
                    }

                    $record=[
                        'col_a'=>$key,
                        'col_c'=>$site_index,
                        'col_e'=>$exempt,
                        'col_f'=>$last,
                        'col_g'=>$name,
                        'col_h'=>$isDr,
                        'col_i'=>'Y',
                        'col_j'=>$eClaimProviderType,
                        'col_k'=>$provider_type,
                        'col_m'=>$specialty1,
                        'col_p'=>$specialty2,
                        'col_s'=>$specialty3,
                        'col_v'=>$specialty4,
                        'col_z'=>$npi,
                        'col_aa'=>$medicaid,
                        'col_ab'=>$stateLic,
                        'col_ac'=>'',
                        'col_ad'=>$genderCode,
                        'col_ae'=>$language1,
                        'col_af'=>$language2,
                        'col_ag'=>$language3,
                        'col_ah'=>$language4,
                        'col_ai'=>$language5,
                        'col_aj'=>$language6,
                        'col_ak'=>$language7,
                        'col_al'=>$language8,
                        'col_bo'=>$site_index,
                        'col_bp'=>$pg_record['start_date'],
                        'col_bq'=>$Prac_IRSType,
                        'col_br'=>$Prac_IRS_No,
                        'col_bs'=>$addr->getSuiteNumber(),
                        'col_bt'=>$addr->getStreet(),
                        'col_bu'=>$addr->getCity(),
                        'col_bv'=>$addr->getUsState(),
                        'col_bw'=>$addr->getZipCode(),
                        'col_by'=>$phone_number,
                        'col_cb'=>$ada,
                        'col_cc'=>'Y',
                        'col_cd'=>'B',
                        'col_cf'=>'I',
                        'col_cg'=>$taxonomyCode1,
                        'col_ch'=>$taxonomyCode2,
                        'col_ci'=>$taxonomyCode3,
                        'col_co'=>'N',
                        'col_cp'=>$hasEvening,
                        'col_cq'=>$hasWeekend,
                        'col_cr'=>$Mon_START,
                        'col_cs'=>$Mon_END,
                        'col_ct'=>$Tues_START,
                        'col_cu'=>$Tues_END,
                        'col_cv'=>$Wed_START,
                        'col_cw'=>$Wed_END,
                        'col_cx'=>$Thurs_START,
                        'col_cy'=>$Thurs_END,
                        'col_cz'=>$Fri_Start,
                        'col_da'=>$Fri_END,
                        'col_db'=>$Sat_START,
                        'col_dc'=>$Sat_END,
                        'col_dd'=>$Sun_START,
                        'col_de'=>$Sun_END,
                        'col_do'=>$payto_street,
                        'col_dp'=>$payto_city,
                        'col_dq'=>$payto_usState,
                        'col_dr'=>$payto_zipCode,
                        'col_dt'=>$payto_phoneNumber,
                        'col_du'=>'MCD',
                        'col_dv'=>'100%',
                        'col_dy'=>$minAge,
                        'col_dz'=>$maxAge,
                    ];

                    //create a new Record
                    $fccRosterData=new FCCRosterData();
                    $fccRosterData->setNpi($npi);
                    $fccRosterData->setKeyLog($key);
                    $fccRosterData->setData($record);
                    $fccRosterData->setStatus('active');

                    $this->em->persist($fccRosterData);

                    $io->writeln($cont." ".$key." ".$site_index." ".$provider->getFirstName()." ".$provider->getLastName()." ".$addr->getStreet());
                    $cont++;
                    $key_cont++;
                }
            }

            //get the addresses for others locations
            $providers_npi=$this->em->getRepository('App\Entity\Provider')->findBy(array('npi_number'=>$npi,'disabled'=>0));
            if(count($providers_npi)>1){
                foreach ($providers_npi as $item){
                    if($item->getId()!=$provider_id){

                        $provider_addrs1=$item->getBillingAddress();
                        $organization_b=$item->getOrganization();
                        if(count($provider_addrs1)>0){
                            foreach ($provider_addrs1 as $addr2){
                                $org_addr2=$addr2->getOrganization();
                                if($org_addr2->getOrganizationStatus()->getId()==2 and $org_addr2->getDisabled()==0){
                                    $street_check=$addr2->getStreet();
                                    $addr_pass=true;
                                    if(stristr($street_check, 'BOX') or stristr($street_check, 'Box') or stristr($street_check, 'box') or $addr2->getUsState()!="FL") {
                                        $addr2_pass=false;
                                    }
                                    if($addr_pass==true){
                                        $eClaimProviderType = "SP";
                                        $pro_degrees = $provider->getDegree();
                                        if ($pro_degrees != null) {
                                            foreach ($pro_degrees as $degree) {
                                                if ($degree->getId() == 1) {
                                                    $eClaimProviderType = "NP";
                                                    break;
                                                }
                                                if ($degree->getId() == 19) {
                                                    if($specialty1!='92901' and $specialty2!='92901' and $specialty3!='92901' and $specialty4!='92901'){
                                                        $eClaimProviderType = "PA";
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        $site_index="";
                                        $key="";
                                        if($key_cont<10){
                                            $key=$provider->getNpiNumber()."-".'00'.$key_cont;
                                            $site_index="00".$key_cont;
                                        }else{
                                            $key=$provider->getNpiNumber()."-".'0'.$key_cont;
                                            $site_index="0".$key_cont;
                                        }

                                        $name=strtolower($provider->getFirstName());
                                        $last=strtolower($provider->getLastName());

                                        $name=ucwords ( $name );
                                        $last=ucwords ( $last );

                                        $exempt="";
                                        if($addr2->getSeePatientsAddr()==false){
                                            $exempt="DE";
                                        }

                                        $tin = $organization->getTinNumber();
                                        $Prac_IRSType="";
                                        $Prac_IRS_No="";
                                        if ($tin != "" and $tin!=null) {
                                            $Prac_IRSType='T';
                                            $Prac_IRS_No= $tin;
                                        }else{
                                            $tin=$this->encoder->decryptthis($provider->getSocial());
                                            $Prac_IRSType='S';
                                            $Prac_IRS_No= $tin;
                                        }

                                        $Prac_IRS_No=str_replace('-',"",$Prac_IRS_No);


                                        $phone_number = $addr2->getPhoneNumber();
                                        $phone_number = str_replace('-', "", $phone_number);

                                        $ada = "N";
                                        if ($addr2->getWheelchair() == true) {
                                            $ada = "Y";
                                        }

                                        $Mon_START="";
                                        $Mon_END="";
                                        $Tues_START="";
                                        $Tues_END="";
                                        $Wed_START="";
                                        $Wed_END="";
                                        $Thurs_START="";
                                        $Thurs_END="";
                                        $Fri_Start="";
                                        $Fri_END="";
                                        $Sat_START="";
                                        $Sat_END="";
                                        $Sun_START="";
                                        $Sun_END="";

                                        $hasWeekend="N";
                                        $hasEvening="N";

                                        $bussinesOurs = $addr2->getBusinessHours();
                                        if($bussinesOurs!=""){
                                            $bussinesOurs = substr($bussinesOurs, 1);
                                            $bussinesOurs = substr($bussinesOurs, 0, -1);

                                            $bussinesOurs = str_replace('{', '', $bussinesOurs);
                                            $bussinesOurs = str_replace('}', '', $bussinesOurs);
                                            $bussinesOurs = str_replace('"', '', $bussinesOurs);

                                            $daysHours = explode(",", $bussinesOurs);

                                            //sunday
                                            $dayActive = explode(":", $daysHours[18])[1];
                                            $startHour = substr($daysHours[19], 9);
                                            $tillHour = substr($daysHours[20], 9);

                                            if ($dayActive == "true") {
                                                $Sun_START=strtoupper($startHour);
                                                $Sun_END=strtoupper($tillHour);

                                                $hasWeekend="Y";
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //monday
                                            $dayActive = explode(":", $daysHours[0])[1];
                                            $startHour = substr($daysHours[1], 9);
                                            $tillHour = substr($daysHours[2], 9);
                                            if ($dayActive == "true") {
                                                $Mon_START=strtoupper($startHour);
                                                $Mon_END= strtoupper($tillHour);
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //tuesday
                                            $dayActive = explode(":", $daysHours[3])[1];
                                            $startHour = substr($daysHours[4], 9);
                                            $tillHour = substr($daysHours[5], 9);
                                            if ($dayActive == "true") {
                                                $Tues_START=strtoupper($startHour);
                                                $Tues_END=strtoupper($tillHour);
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //Wednesday
                                            $dayActive = explode(":", $daysHours[6])[1];
                                            $startHour = substr($daysHours[7], 9);
                                            $tillHour = substr($daysHours[8], 9);
                                            if ($dayActive == "true") {
                                                $Wed_START=strtoupper($startHour);
                                                $Wed_END=strtoupper($tillHour);
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //Thurs
                                            $dayActive = explode(":", $daysHours[9])[1];
                                            $startHour = substr($daysHours[10], 9);
                                            $tillHour = substr($daysHours[11], 9);
                                            if ($dayActive == "true") {
                                                $Thurs_START=strtoupper($startHour);
                                                $Thurs_END=strtoupper($tillHour);
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //Frid
                                            $dayActive = explode(":", $daysHours[12])[1];
                                            $startHour = substr($daysHours[13], 9);
                                            $tillHour = substr($daysHours[14], 9);
                                            if ($dayActive == "true") {
                                                $Fri_Start=strtoupper($startHour);
                                                $Fri_END=strtoupper($tillHour);
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                            //sat
                                            $dayActive = explode(":", $daysHours[15])[1];
                                            $startHour = substr($daysHours[16], 9);
                                            $tillHour = substr($daysHours[17], 9);
                                            if ($dayActive == "true") {
                                                $Sat_START=strtoupper($startHour);
                                                $Sat_END=strtoupper($tillHour);

                                                $hasWeekend="Y";
                                                $hourTill=explode(':',$tillHour);
                                                $hourTill=intval($hourTill[0]);
                                                if($hourTill>6){
                                                    $hasEvening="Y";
                                                }
                                            }

                                        }

                                        //get billing Address
                                        $orgAddress = $this->em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $organization_b->getId()));

                                        $payto_street = "";
                                        $payto_city = "";
                                        $payto_usState = "";
                                        $payto_zipCode = "";
                                        $payto_phoneNumber = "";

                                        if (count($orgAddress) == 1) {
                                            foreach ($orgAddress as $addrB) {
                                                $payto_street=$addrB->getStreet();
                                                $payto_city=$addrB->getCity();
                                                $payto_usState=$addrB->getUsState();
                                                $payto_zipCode=$addrB->getZipCode();
                                                $phone_number_B = $addrB->getPhoneNumber();
                                                $phone_number_B = str_replace('-', "", $phone_number_B);
                                                $payto_phoneNumber=$phone_number_B;
                                            }
                                        }

                                        if (count($orgAddress) > 1) {
                                            foreach ($orgAddress as $addrB) {
                                                if ($addrB->getBillingAddr() == true) {
                                                    $hasBilling = true;
                                                    $street = $addrB->getStreet();
                                                    $city = $addrB->getCity();
                                                    $usState = $addrB->getUsState();
                                                    $zipCode = $addrB->getZipCode();
                                                    $phone_number_B = $addrB->getPhoneNumber();
                                                    $phone_number_B = str_replace('-', "", $phone_number_B);
                                                }
                                            }

                                            if ($hasBilling == true) {
                                                $payto_street=$street;
                                                $payto_city=$city;
                                                $payto_usState=$usState;
                                                $payto_zipCode=$zipCode;
                                                $payto_phoneNumber=$phone_number_B;
                                            }else{
                                                foreach ($orgAddress as $addrB) {
                                                    $payto_street=$addrB->getStreet();
                                                    $payto_city=$addrB->getCity();
                                                    $payto_usState=$addrB->getUsState();
                                                    $payto_zipCode=$addrB->getZipCode();
                                                    $phone_number_B = $addrB->getPhoneNumber();
                                                    $phone_number_B = str_replace('-', "", $phone_number_B);
                                                    $payto_phoneNumber=$phone_number_B;
                                                    break;
                                                }
                                            }
                                        }

                                        $minAge=strval($addr2->getOfficeAgeLimit());
                                        $maxAge=strval($addr2->getOfficeAgeLimitMax());

                                        if(strlen($minAge)==1){
                                            $minAge='0'.$minAge;
                                        }
                                        $minAge=$minAge."Y";

                                        if(strlen($maxAge)==1){
                                            $maxAge='0'.$maxAge;
                                        }
                                        $maxAge=$maxAge."Y";

                                        if($maxAge=="00Y"){
                                            $maxAge="99Y";
                                        }

                                        $record=[
                                            'col_a'=>$key,
                                            'col_c'=>$site_index,
                                            'col_e'=>$exempt,
                                            'col_f'=>$last,
                                            'col_g'=>$name,
                                            'col_h'=>$isDr,
                                            'col_i'=>'Y',
                                            'col_j'=>$eClaimProviderType,
                                            'col_k'=>$provider_type,
                                            'col_m'=>$specialty1,
                                            'col_p'=>$specialty2,
                                            'col_s'=>$specialty3,
                                            'col_v'=>$specialty4,
                                            'col_z'=>$npi,
                                            'col_aa'=>$medicaid,
                                            'col_ab'=>$stateLic,
                                            'col_ac'=>'',
                                            'col_ad'=>$genderCode,
                                            'col_ae'=>$language1,
                                            'col_af'=>$language2,
                                            'col_ag'=>$language3,
                                            'col_ah'=>$language4,
                                            'col_ai'=>$language5,
                                            'col_aj'=>$language6,
                                            'col_ak'=>$language7,
                                            'col_al'=>$language8,
                                            'col_bo'=>$site_index,
                                            'col_bp'=>$pg_record['start_date'],
                                            'col_bq'=>$Prac_IRSType,
                                            'col_br'=>$Prac_IRS_No,
                                            'col_bs'=>$addr2->getSuiteNumber(),
                                            'col_bt'=>$addr2->getStreet(),
                                            'col_bu'=>$addr2->getCity(),
                                            'col_bv'=>$addr2->getUsState(),
                                            'col_bw'=>$addr2->getZipCode(),
                                            'col_by'=>$phone_number,
                                            'col_cb'=>$ada,
                                            'col_cc'=>'Y',
                                            'col_cd'=>'B',
                                            'col_cf'=>'I',
                                            'col_cg'=>$taxonomyCode1,
                                            'col_ch'=>$taxonomyCode2,
                                            'col_ci'=>$taxonomyCode3,
                                            'col_co'=>'N',
                                            'col_cp'=>$hasEvening,
                                            'col_cq'=>$hasWeekend,
                                            'col_cr'=>$Mon_START,
                                            'col_cs'=>$Mon_END,
                                            'col_ct'=>$Tues_START,
                                            'col_cu'=>$Tues_END,
                                            'col_cv'=>$Wed_START,
                                            'col_cw'=>$Wed_END,
                                            'col_cx'=>$Thurs_START,
                                            'col_cy'=>$Thurs_END,
                                            'col_cz'=>$Fri_Start,
                                            'col_da'=>$Fri_END,
                                            'col_db'=>$Sat_START,
                                            'col_dc'=>$Sat_END,
                                            'col_dd'=>$Sun_START,
                                            'col_de'=>$Sun_END,
                                            'col_do'=>$payto_street,
                                            'col_dp'=>$payto_city,
                                            'col_dq'=>$payto_usState,
                                            'col_dr'=>$payto_zipCode,
                                            'col_dt'=>$payto_phoneNumber,
                                            'col_du'=>'MCD',
                                            'col_dv'=>'100%',
                                            'col_dy'=>$minAge,
                                            'col_dz'=>$maxAge,
                                        ];

                                        //create a new Record
                                        $fccRosterData=new FCCRosterData();
                                        $fccRosterData->setNpi($npi);
                                        $fccRosterData->setKeyLog($key);
                                        $fccRosterData->setData($record);
                                        $fccRosterData->setStatus('active');

                                        $this->em->persist($fccRosterData);

                                        $io->writeln($cont." ".$key." ".$site_index." ".$provider->getFirstName()." ".$provider->getLastName()." ".$addr2->getStreet()." ->Other Locations");
                                        $cont++;
                                        $key_cont++;
                                    }

                                }
                            }
                        }

                    }
                }
            }

        }
        $this->em->flush();

        $io->writeln("============ Organizations =============");
        foreach ($fcc_data_org as $item){
            $organization_id=$item->getOrganizationId();
            $organization=$this->em->getRepository('App\Entity\Organization')->find($item->getOrganizationId($organization_id));

            $npi=$organization->getGroupNpi();
            $data=$item->getData();

            $address=$this->em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
            $key_cont=1;
            $pmls=$this->em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$npi,'colW'=>'A','colL'=>'ENROLLMENT'));

            //get exclude organization
            $sql2="SELECT organization.id as organization_id
            FROM  organization_payer_exclude o
            LEFT JOIN organization ON organization.id = o.organization_id
            LEFT JOIN payer ON payer.id = o.payer_id
            WHERE payer.id=4";

            $stmt2 = $this->conn->prepare($sql2);
            $stmt2->execute();
            $excludes_o_ids= $stmt2->fetchAll();
            $excludesOrgs=[];

            $endDate=$data['end_date'];
            foreach ($excludes_o_ids as $org_id){
                $excludesOrgs[]=$org_id['organization_id'];
                $providers_org=$this->em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$org_id['organization_id']));
                if($providers_org!=null){
                    foreach ($providers_org as $provider){
                        $exclude_ids[]=$provider->getId();
                    }
                }
            }

            if($address!=null and !in_array($organization_id,$excludesOrgs) and $endDate=="") {
                foreach ($address as $addr) {
                    $street_check=$addr->getStreet();
                    $state_check=$addr->getUsState();
                    $pass=1;
                    $medicaid_code=$data['provider_id'];

                    if(stristr($street_check, 'BOX')==true or stristr($street_check, 'Box')==true or stristr($street_check, 'box')==true or $state_check!="FL" or $addr->getDisabled()==1) {
                        $pass=0;
                    }
                    if($pass==1){
                        if($pmls==null){
                            $npiR=$organization->getGroupNpi();
                            $pmls=$this->em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$npiR,'colW'=>'A','colL'=>'ENROLLMENT'));
                            foreach ($pmls as $pml1){
                                $medicaid_code=$pml1->getColA();
                            }
                        }else{
                            foreach ($pmls as $pml1){
                                $medicaid_code=$pml1->getColA();
                            }
                        }

                        $site_index = "";
                        $ind_or_group = "";

                        if ($key_cont < 10) {
                            $key = $organization->getGroupNpi() . "-" . '00' . $key_cont;
                            $site_index = "00" . $key_cont;
                        } else {
                            $key = $organization->getGroupNpi() . "-" . '0' . $key_cont;
                            $site_index = "0" . $key_cont;
                        }

                        $providerType = $data['provider_type'];
                        $specialty1 = "";
                        $specialty2 = "";
                        $specialty3 = "";
                        $specialty4 = "";

                        $taxonomyCode1="";
                        $taxonomyCode2="";
                        $taxonomyCode3="";

                        $types=array();
                        $specialtiesArray=array();
                        $taxonomiesCode=array();

                        if(count($pmls)>0) {
                            foreach ($pmls as $pml) {
                                $spe=$pml->getColE();
                                $tax=$pml->getColF();

                                if($spe!=""){
                                    if(!in_array($spe,$specialtiesArray)){
                                        $specialtiesArray[]=$spe;
                                    }
                                }
                                if($tax!=""){
                                    if(!in_array($tax,$taxonomiesCode)){
                                        $taxonomiesCode[]=$tax;
                                    }
                                }
                            }
                        }

                        if(count($specialtiesArray)>=1){
                            $specialty1=$specialtiesArray[0]."01";
                        }
                        if(count($specialtiesArray)>=2){
                            $specialty2=$specialtiesArray[1]."01";
                        }
                        if(count($specialtiesArray)>=3){
                            $specialty3=$specialtiesArray[2]."01";
                        }
                        if(count($specialtiesArray)>=4){
                            $specialty4=$specialtiesArray[3]."01";
                        }
                        if(count($taxonomiesCode)>=1){
                            $taxonomyCode1=$taxonomiesCode[0];
                        }
                        if(count($taxonomiesCode)>=2){
                            $taxonomyCode2=$taxonomiesCode[1];
                        }
                        if(count($taxonomiesCode)>=3){
                            $taxonomyCode3=$taxonomiesCode[2];
                        }

                        $type3 = false;
                        $orgClasifications = $organization->getOrgSpecialtiesArray();

                        if ($orgClasifications != null) {
                            foreach ($orgClasifications as $orgC) {
                                if ($orgC->getName() == "State Mental Hospital") {
                                    $type3 = true;
                                }
                            }
                        }

                        $ahca_id="";
                        if($type3==true){
                            $address2=$this->em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                            if($address2!=null){
                                foreach ($address2 as $addr2){
                                    $ahca_id=$addr2->getAhcaNumber();
                                    if($ahca_id!=""){
                                        if(strlen($ahca_id)==6){
                                            $ahca_id="00".$ahca_id;
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                        $exempt="";
                        if($addr->getSeePatientsAddr()==false){
                            $exempt="DE";
                        }

                        $eClaimProviderType="GR";
                        if($specialty1=="20001" or $specialty1=="20101" or $specialty1=="90101" or $specialty1=="30001" or $specialty1=="30101"){
                            $eClaimProviderType="HS";
                        }

                        if($specialty2=="20001" or $specialty2=="20101" or $specialty2=="90101" or $specialty2=="30001" or $specialty2=="30101"){
                            $eClaimProviderType="HS";
                        }

                        if($specialty3=="20001" or $specialty3=="20101" or $specialty3=="90101" or $specialty3=="30001" or $specialty3=="30101"){
                            $eClaimProviderType="HS";
                        }
                        $ahca_id="";

                        if($type3==true){
                            $ahca_id="";
                            $address2=$this->em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                            if($address2!=null){
                                foreach ($address2 as $addr2){
                                    $ahca_id=$addr2->getAhcaNumber();
                                    if($ahca_id!=""){
                                        if(strlen($ahca_id)==6){
                                            $ahca_id="00".$ahca_id;
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                        $providers_te=$this->em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId()));
                        $languagesId=[];
                        if($providers_te!=null){
                            foreach ($providers_te as $provider){
                                $languages=$provider->getLanguages();
                                if($languages!=null){
                                    foreach ($languages as $lng){
                                        if(!in_array($lng->getId(),$languagesId)){
                                            $languagesId[]=$lng->getId();
                                        }
                                    }
                                }
                            }
                        }

                        $language1 = "";
                        $language2 = "";
                        $language3 = "";
                        $language4 = "";
                        $language5 = "";
                        $language6 = "";
                        $language7 = "";
                        $language8 = "";
                        $languagesArray = array();
                        if ($languagesId != null) {
                            foreach ($languagesId as $id) {
                                $language = $this->em->getRepository('App\Entity\Languages')->find($id);
                                if ($language != null) {
                                    $languagesArray[] = $language;
                                }
                            }
                        }

                        if ($languagesArray != null) {
                            if (count($languagesArray) > 0) {
                                $language1 = $languagesArray[0]->getLangCd();
                            }
                            if (count($languagesArray) > 1) {
                                $language2 = $languagesArray[1]->getLangCd();
                            }
                            if (count($languagesArray) > 2) {
                                $language3 = $languagesArray[2]->getLangCd();
                            }
                            if (count($languagesArray) > 3) {
                                $language4 = $languagesArray[3]->getLangCd();
                            }
                            if (count($languagesArray) > 4) {
                                $language5 = $languagesArray[4]->getLangCd();
                            }
                            if (count($languagesArray) > 5) {
                                $language6 = $languagesArray[5]->getLangCd();
                            }
                            if (count($languagesArray) > 6) {
                                $language7 = $languagesArray[6]->getLangCd();
                            }
                            if (count($languagesArray) > 7) {
                                $language8 = $languagesArray[7]->getLangCd();
                            }
                        }

                        if($language1==""){
                            $language1="ENG";
                        }

                        $ada = "N";
                        if ($addr->getWheelchair() == true) {
                            $ada = "Y";
                        }

                        $phone_number = $addr->getPhoneNumber();
                        $phone_number = str_replace('-', "", $phone_number);

                        $tin = $organization->getTinNumber();
                        $Prac_IRSType="";
                        $Prac_IRS_No="";

                        if ($tin != "") {
                            $Prac_IRSType='T';
                            $Prac_IRS_No= $tin;
                        }else{
                            $provider=$this->em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number'=>$organization->getGroupNpi()));
                            if($provider!=null){
                                $tin=$this->encoder->decryptthis($provider->getSocial());
                                $Prac_IRSType='S';
                                $Prac_IRS_No= $tin;
                            }
                        }

                        $Prac_IRS_No=str_replace("-","",$Prac_IRS_No);

                        if($taxonomyCode1==""){
                            $org_taxonomies=$this->em->getRepository('App\Entity\OrganizationTaxonomy')->findBy(array('organization'=>$organization->getId()));
                            if($org_taxonomies!=null){
                                if(count($org_taxonomies)>=1){
                                    $taxonomyCode1=$org_taxonomies[0]->getTaxonomy()->getCode();
                                }
                                if(count($org_taxonomies)>=2){
                                    $taxonomyCode2=$org_taxonomies[1]->getTaxonomy()->getCode();
                                }
                                if(count($org_taxonomies)>=3){
                                    $taxonomyCode3=$org_taxonomies[2]->getTaxonomy()->getCode();
                                }
                            }
                        }

                        $Mon_START="";
                        $Mon_END="";
                        $Tues_START="";
                        $Tues_END="";
                        $Wed_START="";
                        $Wed_END="";
                        $Thurs_START="";
                        $Thurs_END="";
                        $Fri_Start="";
                        $Fri_END="";
                        $Sat_START="";
                        $Sat_END="";
                        $Sun_START="";
                        $Sun_END="";

                        $hasWeekend="N";
                        $hasEvening="N";

                        $bussinesOurs = $addr->getBusinessHours();

                        if($bussinesOurs!=""){
                            $bussinesOurs = substr($bussinesOurs, 1);
                            $bussinesOurs = substr($bussinesOurs, 0, -1);

                            $bussinesOurs = str_replace('{', '', $bussinesOurs);
                            $bussinesOurs = str_replace('}', '', $bussinesOurs);
                            $bussinesOurs = str_replace('"', '', $bussinesOurs);

                            $daysHours = explode(",", $bussinesOurs);

                            //sunday
                            $dayActive = explode(":", $daysHours[18])[1];
                            $startHour = substr($daysHours[19], 9);
                            $tillHour = substr($daysHours[20], 9);

                            if ($dayActive == "true") {
                                $Sun_START=strtoupper($startHour);
                                $Sun_END=strtoupper($tillHour);

                                $hasWeekend="Y";
                                $hourTill=explode(':',$tillHour);
                                $hourTill=intval($hourTill[0]);
                                if($hourTill>6){
                                    $hasEvening="Y";
                                }
                            }

                            //monday
                            $dayActive = explode(":", $daysHours[0])[1];
                            $startHour = substr($daysHours[1], 9);
                            $tillHour = substr($daysHours[2], 9);
                            if ($dayActive == "true") {
                                $Mon_START=strtoupper($startHour);
                                $Mon_END= strtoupper($tillHour);
                                $hourTill=explode(':',$tillHour);
                                $hourTill=intval($hourTill[0]);
                                if($hourTill>6){
                                    $hasEvening="Y";
                                }
                            }

                            //tuesday
                            $dayActive = explode(":", $daysHours[3])[1];
                            $startHour = substr($daysHours[4], 9);
                            $tillHour = substr($daysHours[5], 9);
                            if ($dayActive == "true") {
                                $Tues_START=strtoupper($startHour);
                                $Tues_END=strtoupper($tillHour);
                                $hourTill=explode(':',$tillHour);
                                $hourTill=intval($hourTill[0]);
                                if($hourTill>6){
                                    $hasEvening="Y";
                                }
                            }

                            //Wednesday
                            $dayActive = explode(":", $daysHours[6])[1];
                            $startHour = substr($daysHours[7], 9);
                            $tillHour = substr($daysHours[8], 9);
                            if ($dayActive == "true") {
                                $Wed_START=strtoupper($startHour);
                                $Wed_END=strtoupper($tillHour);
                                $hourTill=explode(':',$tillHour);
                                $hourTill=intval($hourTill[0]);
                                if($hourTill>6){
                                    $hasEvening="Y";
                                }
                            }

                            //Thurs
                            $dayActive = explode(":", $daysHours[9])[1];
                            $startHour = substr($daysHours[10], 9);
                            $tillHour = substr($daysHours[11], 9);
                            if ($dayActive == "true") {
                                $Thurs_START=strtoupper($startHour);
                                $Thurs_END=strtoupper($tillHour);
                                $hourTill=explode(':',$tillHour);
                                $hourTill=intval($hourTill[0]);
                                if($hourTill>6){
                                    $hasEvening="Y";
                                }
                            }

                            //Frid
                            $dayActive = explode(":", $daysHours[12])[1];
                            $startHour = substr($daysHours[13], 9);
                            $tillHour = substr($daysHours[14], 9);
                            if ($dayActive == "true") {
                                $Fri_Start=strtoupper($startHour);
                                $Fri_END=strtoupper($tillHour);
                                $hourTill=explode(':',$tillHour);
                                $hourTill=intval($hourTill[0]);
                                if($hourTill>6){
                                    $hasEvening="Y";
                                }
                            }

                            //sat
                            $dayActive = explode(":", $daysHours[15])[1];
                            $startHour = substr($daysHours[16], 9);
                            $tillHour = substr($daysHours[17], 9);
                            if ($dayActive == "true") {
                                $Sat_START=strtoupper($startHour);
                                $Sat_END=strtoupper($tillHour);

                                $hasWeekend="Y";
                                $hourTill=explode(':',$tillHour);
                                $hourTill=intval($hourTill[0]);
                                if($hourTill>6){
                                    $hasEvening="Y";
                                }
                            }
                        }

                        $orgAddress = $this->em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $organization->getId()));

                        $payto_street = "";
                        $payto_city = "";
                        $payto_UsState = "";
                        $payto_zipCode = "";
                        $payto_phoneNumber = "";
                        if (count($orgAddress) == 1) {
                            foreach ($orgAddress as $addrB) {
                                $payto_street= $addrB->getStreet();
                                $payto_city= $addrB->getCity();
                                $payto_UsState= $addrB->getUsState();
                                $payto_zipCode= $addrB->getZipCode();
                                $phone_number_B = $addrB->getPhoneNumber();
                                $phone_number_B = str_replace('-', "", $phone_number_B);
                                $payto_phoneNumber= $phone_number_B;
                            }
                        }

                        if (count($orgAddress) > 1) {
                            foreach ($orgAddress as $addrB) {
                                if ($addrB->getBillingAddr() == true) {
                                    $payto_street= $addrB->getStreet();
                                    $payto_city= $addrB->getCity();
                                    $payto_UsState= $addrB->getUsState();
                                    $payto_zipCode= $addrB->getZipCode();
                                    $phone_number_B = $addrB->getPhoneNumber();
                                    $phone_number_B = str_replace('-', "", $phone_number_B);
                                    $payto_phoneNumber= $phone_number_B;
                                }
                            }

                            if($payto_street==""){
                                foreach ($orgAddress as $addrB) {
                                    $payto_street= $addrB->getStreet();
                                    $payto_city= $addrB->getCity();
                                    $payto_UsState= $addrB->getUsState();
                                    $payto_zipCode= $addrB->getZipCode();
                                    $phone_number_B = $addrB->getPhoneNumber();
                                    $phone_number_B = str_replace('-', "", $phone_number_B);
                                    $payto_phoneNumber= $phone_number_B;
                                    break;
                                }
                            }
                        }

                        $minAge=strval($addr2->getOfficeAgeLimit());
                        $maxAge=strval($addr2->getOfficeAgeLimitMax());

                        if(strlen($minAge)==1){
                            $minAge='0'.$minAge;
                        }
                        $minAge=$minAge."Y";

                        if(strlen($maxAge)==1){
                            $maxAge='0'.$maxAge;
                        }
                        $maxAge=$maxAge."Y";

                        if($maxAge=="00Y"){
                            $maxAge="99Y";
                        }

                            $record=[
                            'col_a'=>$key,
                            'col_c'=>$site_index,
                            'col_e'=>$exempt,
                            'col_f'=>$organization->getName(),
                            'col_g'=>'',
                            'col_h'=>'',
                            'col_i'=>'N',
                            'col_j'=>$eClaimProviderType,
                            'col_k'=>$data['provider_type'],
                            'col_m'=>$specialty1,
                            'col_p'=>$specialty2,
                            'col_s'=>$specialty3,
                            'col_v'=>$specialty4,
                            'col_z'=>$npi,
                            'col_aa'=>$medicaid_code,
                            'col_ab'=>'',
                            'col_ac'=>$ahca_id,
                            'col_ad'=>'',
                            'col_ae'=>$language1,
                            'col_af'=>$language2,
                            'col_ag'=>$language3,
                            'col_ah'=>$language4,
                            'col_ai'=>$language5,
                            'col_aj'=>$language6,
                            'col_ak'=>$language7,
                            'col_al'=>$language8,
                            'col_bo'=>$site_index,
                            'col_bp'=>$data['start_date'],
                            'col_bq'=>$Prac_IRSType,
                            'col_br'=>$Prac_IRS_No,
                            'col_bs'=>$addr->getSuiteNumber(),
                            'col_bt'=>$addr->getStreet(),
                            'col_bu'=>$addr->getCity(),
                            'col_bv'=>$addr->getUsState(),
                            'col_bw'=>$addr->getZipCode(),
                            'col_by'=>$phone_number,
                            'col_cb'=>$ada,
                            'col_cc'=>'Y',
                            'col_cd'=>'B',
                            'col_cf'=>'O',
                            'col_cg'=>$taxonomyCode1,
                            'col_ch'=>$taxonomyCode2,
                            'col_ci'=>$taxonomyCode3,
                            'col_co'=>'N',
                            'col_cp'=>$hasEvening,
                            'col_cq'=>$hasWeekend,
                            'col_cr'=>$Mon_START,
                            'col_cs'=>$Mon_END,
                            'col_ct'=>$Tues_START,
                            'col_cu'=>$Tues_END,
                            'col_cv'=>$Wed_START,
                            'col_cw'=>$Wed_END,
                            'col_cx'=>$Thurs_START,
                            'col_cy'=>$Thurs_END,
                            'col_cz'=>$Fri_Start,
                            'col_da'=>$Fri_END,
                            'col_db'=>$Sat_START,
                            'col_dc'=>$Sat_END,
                            'col_dd'=>$Sun_START,
                            'col_de'=>$Sun_END,
                            'col_do'=>$payto_street,
                            'col_dp'=>$payto_city,
                            'col_dq'=>$payto_UsState,
                            'col_dr'=>$payto_zipCode,
                            'col_dt'=>$payto_phoneNumber,
                            'col_du'=>'MCD',
                            'col_dv'=>'100%',
                            'col_dy'=>$minAge,
                            'col_dz'=>$maxAge,
                        ];

                        $fccRosterData=new FCCRosterData();
                        $fccRosterData->setNpi($npi);
                        $fccRosterData->setKeyLog($key);
                        $fccRosterData->setData($record);
                        $fccRosterData->setStatus('active');

                        $this->em->persist($fccRosterData);

                        $io->writeln($cont." ".$organization->getName(). " ". $addr->getStreet());
                        $cont++;
                        $key_cont++;
                    }
                }
            }
        }

        $this->em->flush();

        $io->writeln('Checking the changes on the rosters');
        $io->writeln('New records');


        //get all current records for roster for check the new ewcords
        $all_current_records=$this->em->getRepository('App\Entity\FCCRosterData')->findAll();
        foreach ($all_current_records as $current_record){
            $key_log=$current_record->getKeyLog();
            $cont_match=0;
            foreach ($roster_los as $roster_log){
                $key=$roster_log->getIdKey();

                if($key==$key_log){
                    $cont_match++;
                }
            }

            if($cont_match==0){
                $current_record->setStatus('new');
                $this->em->persist($current_record);
                $this->em->flush();

                $io->writeln('New '.$key_log);
            }
        }

        foreach ($roster_los as $roster_log){
            $key=substr($roster_log->getIdKey(),0,10);
            $cont_match=0;
            foreach ($all_current_records as $current_record){
                $keylog=substr($current_record->getKeyLog(),0,10);
                if($keylog==$key){
                    $cont_match++;
                }
            }

            if($cont_match==0){
                $this->em->flush();
                $io->writeln('To remove '.$key);
                $record=[
                    'col_a'=>$roster_log->getIdKey(),
                    'col_c'=>"",
                    'col_e'=>"",
                    'col_f'=>$roster_log->getLastName(),
                    'col_g'=>$roster_log->getFirstName(),
                    'col_h'=>$roster_log->getProvTitle(),
                    'col_i'=>'',
                    'col_j'=>$roster_log->getEclaimsProviderType(),
                    'col_k'=>$roster_log->getAhcaProviderType(),
                    'col_m'=>$roster_log->getSpec1(),
                    'col_p'=>$roster_log->getSpec1(),
                    'col_s'=>$roster_log->getSpec1(),
                    'col_v'=>$roster_log->getSpec1(),
                    'col_z'=>$roster_log->getNpi(),
                    'col_aa'=>$roster_log->getMedicaidId(),
                    'col_ab'=>$roster_log->getStateLicense(),
                    'col_ac'=>"",
                    'col_ad'=>'',
                    'col_ae'=>$roster_log->getLng1(),
                    'col_af'=>$roster_log->getLng2(),
                    'col_ag'=>$roster_log->getLng3(),
                    'col_ah'=>$roster_log->getLng4(),
                    'col_ai'=>$roster_log->getLng5(),
                    'col_aj'=>$roster_log->getLng6(),
                    'col_ak'=>$roster_log->getLng7(),
                    'col_al'=>$roster_log->getLng8(),
                    'col_bo'=>"",
                    'col_bp'=>'20210101',
                    'col_bq'=>$roster_log->getPracIsrType(),
                    'col_br'=>$roster_log->getPracIsr(),
                    'col_bs'=>$roster_log->getPracticeSuite(),
                    'col_bt'=>$roster_log->getPracStreet(),
                    'col_bu'=>$roster_log->getPracCity(),
                    'col_bv'=>$roster_log->getPracState(),
                    'col_bw'=>$roster_log->getPracZipcode(),
                    'col_by'=>$roster_log->getPracPhone(),
                    'col_cb'=>$roster_log->getAdaComp(),
                    'col_cc'=>'Y',
                    'col_cd'=>'B',
                    'col_cf'=>'I',
                    'col_cg'=>$roster_log->getTaxonomy1(),
                    'col_ch'=>$roster_log->getTaxonomy2(),
                    'col_ci'=>$roster_log->getTaxonomy3(),
                    'col_co'=>'N',
                    'col_cp'=>$roster_log->getEveningHrs(),
                    'col_cq'=>$roster_log->getWeekendHrs(),
                    'col_cr'=>$roster_log->getMonStart(),
                    'col_cs'=>$roster_log->getMonEnd(),
                    'col_ct'=>$roster_log->getTuesStart(),
                    'col_cu'=>$roster_log->getTuesEnd(),
                    'col_cv'=>$roster_log->getWedStart(),
                    'col_cw'=>$roster_log->getWedEnd(),
                    'col_cx'=>$roster_log->getThursStart(),
                    'col_cy'=>$roster_log->getThursEnd(),
                    'col_cz'=>$roster_log->getFriStart(),
                    'col_da'=>$roster_log->getFriEnd(),
                    'col_db'=>$roster_log->getSatStart(),
                    'col_dc'=>$roster_log->getSatEnd(),
                    'col_dd'=>$roster_log->getSunStart(),
                    'col_de'=>$roster_log->getSunEnd(),
                    'col_do'=>$roster_log->getPaytoStreet(),
                    'col_dp'=>$roster_log->getPaytoCity(),
                    'col_dq'=>$roster_log->getPaytoState(),
                    'col_dr'=>$roster_log->getPaytoZipcode(),
                    'col_dt'=>$roster_log->getPaytoPhone(),
                    'col_du'=>'MCD',
                    'col_dv'=>'100%',
                    'col_dy'=>'',
                    'col_dz'=>'',
                ];

                $fccRosterData=new FCCRosterData();
                $fccRosterData->setNpi($roster_log->getNpi());
                $fccRosterData->setKeyLog($key);
                $fccRosterData->setData($record);
                $fccRosterData->setStatus('to_remove');

                $this->em->persist($fccRosterData);
            }

        }
        $this->em->flush();

        //Verify for changes
        $io->writeln("Checking Changes on Roster");
        $all_current_records=$this->em->getRepository('App\Entity\FCCRosterData')->findAll();
        foreach ($all_current_records as $current_record){

            //$current_record=new FCCRosterData();
            $data=$current_record->getData();
            $keylog=$current_record->getKeyLog();

            $addr=$data['col_bt'];
            $name_switch_flag_log=$data['col_i'];

            $roster_record_log=$this->em->getRepository('App\Entity\FCCRosterLog')->findOneBy(array('id_key'=>$keylog,'prac_street'=>$addr,'name_switch_flag'=>$name_switch_flag_log));
            $changes=0;

            if($roster_record_log!=null){
                //$roster_record_log=new FCCRosterLog();
                $last_name=$data['col_f'];
                $prov_title=$data['col_h'];
                $eclaim_provider_type=$data['col_j'];

                $spec1_log=$data['col_m'];
                $spec2_log=$data['col_p'];
                $spec3_log=$data['col_s'];

                $tax1_log=$data['col_cg'];
                $tax2_log=$data['col_ch'];
                $tax3_log=$data['col_ci'];

                $street_log=$data['col_bt'];
                $suite_log=$data['col_bs'];
                $city_log=$data['col_bu'];
                $zipcode_log=$data['col_bw'];
                $phone_log=$data['col_by'];

                $MonSTART=$data['col_cr'];
                $MonEND=$data['col_cs'];
                $TuesSTART=$data['col_ct'];
                $TuesEND=$data['col_cu'];
                $WedSTART=$data['col_cv'];
                $WedEND=$data['col_cw'];
                $ThursSTART=$data['col_cx'];
                $ThursEND=$data['col_cy'];
                $FriStart=$data['col_cz'];
                $FriEND=$data['col_da'];
                $SatSTART=$data['col_db'];
                $SatEND=$data['col_dc'];
                $SunSTART=$data['col_dd'];
                $SunEND=$data['col_de'];

                $lang1_log=$data['col_ae'];
                $lang2_log=$data['col_af'];
                $lang3_log=$data['col_ag'];
                $lang4_log=$data['col_ah'];
                $lang5_log=$data['col_ai'];
                $lang6_log=$data['col_aj'];
                $lang7_log=$data['col_ak'];
                $lang8_log=$data['col_al'];

                if($lang1_log!=$roster_record_log->getLng1()){ $changes++; }
                if($lang2_log!=$roster_record_log->getLng2()){ $changes++; }
                if($lang3_log!=$roster_record_log->getLng3()){ $changes++; }
                if($lang4_log!=$roster_record_log->getLng4()){ $changes++; }
                if($lang5_log!=$roster_record_log->getLng5()){ $changes++; }
                if($lang6_log!=$roster_record_log->getLng6()){ $changes++; }
                if($lang7_log!=$roster_record_log->getLng7()){ $changes++; }
                if($lang8_log!=$roster_record_log->getLng8()){ $changes++; }

                if($MonSTART!=$roster_record_log->getMonStart()){ $changes++; }
                if($MonEND!=$roster_record_log->getMonEnd()){ $changes++; }
                if($TuesSTART!=$roster_record_log->getTuesStart()){ $changes++; }
                if($TuesEND!=$roster_record_log->getTuesEnd()){ $changes++; }
                if($WedSTART!=$roster_record_log->getWedStart()){ $changes++; }
                if($WedEND!=$roster_record_log->getWedEnd()){ $changes++; }
                if($ThursSTART!=$roster_record_log->getThursStart()){ $changes++; }
                if($ThursEND!=$roster_record_log->getThursEnd()){ $changes++; }
                if($FriStart!=$roster_record_log->getFriStart()){ $changes++; }
                if($FriEND!=$roster_record_log->getFriEnd()){ $changes++; }
                if($SatSTART!=$roster_record_log->getSatStart()){ $changes++; }
                if($SatEND!=$roster_record_log->getSatEnd()){ $changes++; }
                if($SunSTART!=$roster_record_log->getSunStart()){ $changes++; }
                if($SunEND!=$roster_record_log->getSunEnd()){ $changes++; }



                if($street_log!=$roster_record_log->getPracStreet()){ $changes++; }
                if($suite_log!=$roster_record_log->getPracticeSuite()){ $changes++; }
                if($city_log!=$roster_record_log->getPracCity()){ $changes++; }
                if($zipcode_log!=$roster_record_log->getPracZipcode()){ $changes++; }
                if($phone_log!=$roster_record_log->getPracPhone()){ $changes++; }

                if(strtolower($last_name)!=strtolower($roster_record_log->getLastName())){ $changes++; }

                if($prov_title!=$roster_record_log->getProvTitle()){ $changes++; }

                if($eclaim_provider_type!=$roster_record_log->getEclaimsProviderType()){ $changes++; }

                if($spec1_log!=$roster_record_log->getSpec1()){ $changes++; }

                if($spec2_log!=$roster_record_log->getSpec2()){ $changes++; }

                if($spec3_log!=$roster_record_log->getSpec3()){ $changes++; }

                if($tax1_log!=$roster_record_log->getTaxonomy1()){ $changes++; }

                if($tax2_log!=$roster_record_log->getTaxonomy2()){ $changes++; }

                if($tax3_log!=$roster_record_log->getTaxonomy3()){ $changes++; }

                if($changes>0){
                    //cahged
                    $io->writeln('Changed : '.strtolower($last_name). " - ".$roster_record_log->getLastName(). " ".$keylog);

                    $record=[
                        'col_a'=>$roster_record_log->getIdKey(),
                        'col_c'=>"",
                        'col_e'=>"",
                        'col_f'=>$roster_record_log->getLastName(),
                        'col_g'=>$roster_record_log->getFirstName(),
                        'col_h'=>$roster_record_log->getProvTitle(),
                        'col_i'=>'',
                        'col_j'=>$roster_record_log->getEclaimsProviderType(),
                        'col_k'=>$roster_record_log->getAhcaProviderType(),
                        'col_m'=>$roster_record_log->getSpec1(),
                        'col_p'=>$roster_record_log->getSpec1(),
                        'col_s'=>$roster_record_log->getSpec1(),
                        'col_v'=>$roster_record_log->getSpec1(),
                        'col_z'=>$roster_record_log->getNpi(),
                        'col_aa'=>$roster_record_log->getMedicaidId(),
                        'col_ab'=>$roster_record_log->getStateLicense(),
                        'col_ac'=>"",
                        'col_ad'=>'',
                        'col_ae'=>$roster_record_log->getLng1(),
                        'col_af'=>$roster_record_log->getLng2(),
                        'col_ag'=>$roster_record_log->getLng3(),
                        'col_ah'=>$roster_record_log->getLng4(),
                        'col_ai'=>$roster_record_log->getLng5(),
                        'col_aj'=>$roster_record_log->getLng6(),
                        'col_ak'=>$roster_record_log->getLng7(),
                        'col_al'=>$roster_record_log->getLng8(),
                        'col_bo'=>"",
                        'col_bp'=>'20210101',
                        'col_bq'=>$roster_record_log->getPracIsrType(),
                        'col_br'=>$roster_record_log->getPracIsr(),
                        'col_bs'=>$roster_record_log->getPracticeSuite(),
                        'col_bt'=>$roster_record_log->getPracStreet(),
                        'col_bu'=>$roster_record_log->getPracCity(),
                        'col_bv'=>$roster_record_log->getPracState(),
                        'col_bw'=>$roster_record_log->getPracZipcode(),
                        'col_by'=>$roster_record_log->getPracPhone(),
                        'col_cb'=>$roster_record_log->getAdaComp(),
                        'col_cc'=>'Y',
                        'col_cd'=>'B',
                        'col_cf'=>'I',
                        'col_cg'=>$roster_record_log->getTaxonomy1(),
                        'col_ch'=>$roster_record_log->getTaxonomy2(),
                        'col_ci'=>$roster_record_log->getTaxonomy3(),
                        'col_co'=>'N',
                        'col_cp'=>$roster_record_log->getEveningHrs(),
                        'col_cq'=>$roster_record_log->getWeekendHrs(),
                        'col_cr'=>$roster_record_log->getMonStart(),
                        'col_cs'=>$roster_record_log->getMonEnd(),
                        'col_ct'=>$roster_record_log->getTuesStart(),
                        'col_cu'=>$roster_record_log->getTuesEnd(),
                        'col_cv'=>$roster_record_log->getWedStart(),
                        'col_cw'=>$roster_record_log->getWedEnd(),
                        'col_cx'=>$roster_record_log->getThursStart(),
                        'col_cy'=>$roster_record_log->getThursEnd(),
                        'col_cz'=>$roster_record_log->getFriStart(),
                        'col_da'=>$roster_record_log->getFriEnd(),
                        'col_db'=>$roster_record_log->getSatStart(),
                        'col_dc'=>$roster_record_log->getSatEnd(),
                        'col_dd'=>$roster_record_log->getSunStart(),
                        'col_de'=>$roster_record_log->getSunEnd(),
                        'col_do'=>$roster_record_log->getPaytoStreet(),
                        'col_dp'=>$roster_record_log->getPaytoCity(),
                        'col_dq'=>$roster_record_log->getPaytoState(),
                        'col_dr'=>$roster_record_log->getPaytoZipcode(),
                        'col_dt'=>$roster_record_log->getPaytoPhone(),
                        'col_du'=>'MCD',
                        'col_dv'=>'100%',
                        'col_dy'=>'',
                        'col_dz'=>'',
                    ];

                    $fccRosterData=new FCCRosterData();
                    $fccRosterData->setNpi($roster_log->getNpi());
                    $fccRosterData->setKeyLog($key);
                    $fccRosterData->setData($record);
                    $fccRosterData->setStatus('changed');

                    $this->em->persist($fccRosterData);
                    $this->em->flush();
                }
            }

        }

        $io->success('All completed.');
    }
}
