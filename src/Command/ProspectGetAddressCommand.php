<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ProspectGetAddressCommand extends Command
{
    protected static $defaultName = 'prospect:get:address';
    private $em;
    private $conn;

    public function __construct( EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $prospects=$this->em->getRepository('App:Prospect')->findAll();
        $cont=1;
        foreach($prospects as $prospect){

            $output->writeln($cont." ".$prospect->getId());
            $cont++;
        }

        $io->success('Finish process');
    }
}
