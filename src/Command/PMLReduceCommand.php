<?php

namespace App\Command;

use App\Entity\BillingAddress;
use App\Entity\Organization;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Entity\PML;
use App\Entity\PML2;

class PMLReduceCommand extends Command
{
    protected static $defaultName = 'pml:reduce';
    private $em;
    private $conn;

    public function __construct( EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $providers=$this->em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0,'valid_npi_number'=>1));
        $organizations=$this->em->getRepository('App\Entity\Organization')->findBy(array('disabled'=>0));
        $pmls2=$this->em->getRepository('App\Entity\PML2')->findAll();

        foreach ($pmls2 as $pml){
            $this->em->remove($pml);
        }
        $this->em->flush();

        $npi_arrays=[];
        $cont=0;
        foreach ($providers as $provider){
            $npi=$provider->getNpiNumber();
            if(!in_array($npi,$npi_arrays) and $npi!="N/A" and $npi!="NOT AVAILABLE" and $npi!=""){
                $npi_arrays[]=$npi;
                $pmls=$this->em->getRepository('App\Entity\PML')->findBy(array('colN'=>$npi));

                if($pmls!=null){
                    foreach ($pmls as $pml){
                        $pml2=new PML2();
                        $pml2->setColA($pml->getColA());
                        $pml2->setCoLB($pml->getColB());
                        $pml2->setColC($pml->getColC());
                        $pml2->setColD($pml->getColD());
                        $pml2->setColE($pml->getColE());
                        $pml2->setColF($pml->getColF());
                        $pml2->setColG($pml->getColG());
                        $pml2->setColH($pml->getColH());
                        $pml2->setColI($pml->getColI());
                        $pml2->setColJ($pml->getColJ());
                        $pml2->setColK($pml->getCoLK());
                        $pml2->setColL($pml->getColL());
                        $pml2->setColM($pml->getColM());
                        $pml2->setColN($pml->getColN());
                        $pml2->setColO($pml->getColO());
                        $pml2->setColP($pml->getColP());
                        $pml2->setColQ($pml->getColQ());
                        $pml2->setColR($pml->getColR());
                        $pml2->setColS($pml->getColS());
                        $pml2->setColT($pml->getColT());
                        $pml2->setColU($pml->getColU());
                        $pml2->setColV($pml->getColV());
                        $pml2->setColW($pml->getColW());
                        $pml2->setColX($pml->getColX());
                        $pml2->setColY($pml->getColY());

                        $this->em->persist($pml2);
                        $this->em->flush();
                    }

                }

                $output->writeln($cont." ".$npi);
                $cont++;
            }
        }

        foreach ($organizations as $organization){
            $npi=$organization->getGroupNpi();
            $billing_addr=$this->em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$npi));

            if(!in_array($npi,$npi_arrays) and $npi!="N/A" and $npi!="NOT AVAILABLE" and $npi!=""){
                $npi_arrays[]=$npi;
                $pmls=$this->em->getRepository('App\Entity\PML')->findBy(array('colN'=>$npi));

                if($pmls!=null) {
                    foreach ($pmls as $pml) {
                        $pml2=new PML2();
                        $pml2->setColA($pml->getColA());
                        $pml2->setCoLB($pml->getColB());
                        $pml2->setColC($pml->getColC());
                        $pml2->setColD($pml->getColD());
                        $pml2->setColE($pml->getColE());
                        $pml2->setColF($pml->getColF());
                        $pml2->setColG($pml->getColG());
                        $pml2->setColH($pml->getColH());
                        $pml2->setColI($pml->getColI());
                        $pml2->setColJ($pml->getColJ());
                        $pml2->setColK($pml->getCoLK());
                        $pml2->setColL($pml->getColL());
                        $pml2->setColM($pml->getColM());
                        $pml2->setColN($pml->getColN());
                        $pml2->setColO($pml->getColO());
                        $pml2->setColP($pml->getColP());
                        $pml2->setColQ($pml->getColQ());
                        $pml2->setColR($pml->getColR());
                        $pml2->setColS($pml->getColS());
                        $pml2->setColT($pml->getColT());
                        $pml2->setColU($pml->getColU());
                        $pml2->setColV($pml->getColV());
                        $pml2->setColW($pml->getColW());
                        $pml2->setColX($pml->getColX());
                        $pml2->setColY($pml->getColY());

                        $this->em->persist($pml2);
                        $this->em->flush();

                        $output->writeln($organization->getName()." ".$npi);
                    }
                }
            }

            if($billing_addr){
                foreach ($billing_addr as $addr){
                    $location_npi=$addr->getLocationNpi();
                    if($location_npi!=$npi){
                        if(!in_array($location_npi,$npi_arrays) and $location_npi!="N/A" and $location_npi!="NOT AVAILABLE" and $location_npi!=""){
                            $npi_arrays[]=$location_npi;
                            $pmls=$this->em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$location_npi));
                            if($pmls!=null) {
                                foreach ($pmls as $pml) {
                                    $pml2=new PML2();
                                    $pml2->setColA($pml->getColA());
                                    $pml2->setCoLB($pml->getColB());
                                    $pml2->setColC($pml->getColC());
                                    $pml2->setColD($pml->getColD());
                                    $pml2->setColE($pml->getColE());
                                    $pml2->setColF($pml->getColF());
                                    $pml2->setColG($pml->getColG());
                                    $pml2->setColH($pml->getColH());
                                    $pml2->setColI($pml->getColI());
                                    $pml2->setColJ($pml->getColJ());
                                    $pml2->setColK($pml->getCoLK());
                                    $pml2->setColL($pml->getColL());
                                    $pml2->setColM($pml->getColM());
                                    $pml2->setColN($pml->getColN());
                                    $pml2->setColO($pml->getColO());
                                    $pml2->setColP($pml->getColP());
                                    $pml2->setColQ($pml->getColQ());
                                    $pml2->setColR($pml->getColR());
                                    $pml2->setColS($pml->getColS());
                                    $pml2->setColT($pml->getColT());
                                    $pml2->setColU($pml->getColU());
                                    $pml2->setColV($pml->getColV());
                                    $pml2->setColW($pml->getColW());
                                    $pml2->setColX($pml->getColX());
                                    $pml2->setColY($pml->getColY());

                                    $this->em->persist($pml2);
                                    $this->em->flush();

                                    $output->writeln($addr->getStreet()." ".$location_npi);
                                }
                            }
                        }
                    }
                }
            }
        }

        $io->success('All completed.');
    }
}
