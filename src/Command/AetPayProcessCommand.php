<?php

namespace App\Command;

use App\Entity\AETPayData;
use App\Entity\BillingAddress;
use App\Entity\Organization;
use App\Entity\OrganizationCredentialing;
use App\Entity\Provider;
use App\Utils\My_Mcript;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AetPayProcessCommand extends Command
{

    protected static $defaultName = 'aet:pay:process';
    private $em;
    private $conn;
    private $private_key;
    private $encoder;
    private $params;

    public function __construct( EntityManagerInterface $em, ParameterBagInterface $params)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
        $this->params = $params;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $this->private_key=$this->params->get('private_key');
        $encoder=new My_Mcript($this->private_key);

        $aetna_data=$this->em->getRepository('App:AETData')->findAll();
        $aetna_data_org=$this->em->getRepository('App:AETDataOrg')->findAll();

        //remove all fccroster_data records
        $aet_pay_data=$this->em->getRepository('App:AETPayData')->findAll();
        if($aet_pay_data){
            foreach ($aet_pay_data as $item){
                $this->em->remove($item);
            }
            $this->em->flush();
        }

        $cont=1;

        foreach ($aetna_data as $datum) {
            $provider_id = $datum->getProviderId();
            $npi = $datum->getNpi();

            $data = $datum->getData();
            $provider=$this->em->getRepository('App:Provider')->find($provider_id);

            $last_name=$data['last_name'];
            $first_name=$data['first_name'];
            $initial=$provider->getInitial();

            //get Degree
            $degrees=$provider->getDegree();
            $degree_str="";
            if($degrees){
                foreach ($degrees as $degree){
                    $degree_str=$degree->getName();
                }
            }

            //gender
            $gender_str="";
            if($provider->getGender()=="Male"){
                $gender_str="M";
            }else{
                $gender_str="F";
            }

            //DOB
            $dob=$provider->getDateOfBirth();
            $dob=str_replace('-','/',$dob);

            if($dob!="" and $dob!="/"){
                $dob=explode('/',$dob);
                $dob_day=$dob[1];
                $dob_month=$dob[0];
                $dob_year=$dob[2];

                if(strlen($dob_day)==1){
                    $dob_day='0'.$dob_day;
                }

                if(strlen($dob_month)==1){
                    $dob_month='0'.$dob_month;
                }

                $dob=$dob_year."/".$dob_month."/".$dob_day;
            }

            if($dob=="/" or $dob=="0000/01/01"){
                $dob="";
            }


            //specialties
            $specialties=$provider->getPrimarySpecialties();
            $specialty_code_1="";
            $specialty_code_2="";
            $specialty_code_3="";
            $specialty_code_4="";

            $specialty_name_1="";
            $specialty_name_2="";
            $specialty_name_3="";
            $specialty_name_4="";

            $cont_spc=1;
            if($specialties){
                foreach ($specialties as $specialty){
                    if($cont_spc==1){
                        $specialty_name_1=$specialty->getName();
                        $specialty_code_1=$specialty->getCode();
                    }
                    if($cont_spc==2){
                        $specialty_name_2=$specialty->getName();
                        $specialty_code_2=$specialty->getCode();
                    }
                    if($cont_spc==3){
                        $specialty_name_3=$specialty->getName();
                        $specialty_code_3=$specialty->getCode();
                    }
                    if($cont_spc==4){
                        $specialty_name_4=$specialty->getName();
                        $specialty_code_4=$specialty->getCode();
                    }

                    $cont_spc++;
                }
            }

            $medicaid_id=$data['provider_id'];

            $license_expire_date=$provider->getLicExpiresDate();
            $state_license=$data['license_number'];
            $dea_expirations=$provider->getDeaExpirations();
            $dea_number=$provider->getDeaNumber();
            $dea_state=$provider->getDeaState();

            $language1="";
            $language2="";
            $language3="";
            $language4="";
            $language5="";
            $language6="";
            $language7="";
            $language8="";

            $languages=$provider->getLanguages();
            $cont_lng=1;
            if($languages){
                foreach ($languages as $language){
                    if($cont_lng==1){ $language1=$language->getName(); }
                    if($cont_lng==2){ $language2=$language->getName(); }
                    if($cont_lng==3){ $language3=$language->getName(); }
                    if($cont_lng==4){ $language4=$language->getName(); }
                    if($cont_lng==5){ $language5=$language->getName(); }
                    if($cont_lng==6){ $language6=$language->getName(); }
                    if($cont_lng==7){ $language7=$language->getName(); }
                    if($cont_lng==8){ $language8=$language->getName(); }

                    $cont_lng++;
                }
            }

            $organization=$provider->getOrganization();

            $practice_name=$organization->getName();
            $group_npi=$organization->getGroupNpi();
            $fax_no=$organization->getFaxNo();

            if($fax_no!=null and $fax_no!=""){
                $fax_no=str_replace("-","",$fax_no);
            }

            $Prac_IRSType="";
            $Prac_IRS="";

            $tin=$organization->getTinNumber();
            if ($tin != "" and $tin!=null) {
                $Prac_IRSType='T';
                $Prac_IRS= $tin;
            }else{
                $tin=$encoder->decryptthis($provider->getSocial());
                $Prac_IRSType='S';
                $Prac_IRS= $tin;
            }

            $Prac_IRS=str_replace("-","",$Prac_IRS);

            $taxonomyCode1="";
            $taxonomyCode2="";
            $taxonomyCode3="";

            $taxonomiesCode=[];

            $pmls=$this->em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$npi,'colW'=>'A','colL'=>'ENROLLMENT'));
            $medicaid_license_expiration_date="";
            if($pmls){
                foreach ($pmls as $pml){
                    $tax=$pml->getColF();
                    if($tax!=""){
                        if(!in_array($tax,$taxonomiesCode)){
                            $taxonomiesCode[]=$tax;
                        }
                    }

                    if($pml->getColA()==$medicaid_id){
                        $mled=$pml->getColY();
                        $mled=str_replace('-','',$mled);
                        $medicaid_license_expiration_date=$mled;
                    }
                }
            }

            if(count($taxonomiesCode)>=1){
                $taxonomyCode1=$taxonomiesCode[0];
            }
            if(count($taxonomiesCode)>=2){
                $taxonomyCode2=$taxonomiesCode[1];
            }
            if(count($taxonomiesCode)>=3){
                $taxonomyCode3=$taxonomiesCode[2];
            }

            $cred_date=$data['credentialed_date'];

            $key_cont=1;

            $cred_effective_date="";
            $month=substr($cred_date,4,2);
            $year=substr($cred_date,0,4);

            if($month=='01') { $month=='02'; }
            if($month=='02') { $month=='03'; }
            if($month=='03') { $month=='04'; }
            if($month=='04') { $month=='05'; }
            if($month=='05') { $month=='06'; }
            if($month=='06') { $month=='07'; }
            if($month=='07') { $month=='08'; }
            if($month=='08') { $month=='09'; }
            if($month=='09') { $month=='10'; }
            if($month=='10') { $month=='11'; }
            if($month=='11') { $month=='12'; }
            if($month=='12') { $month=='01'; $year=$year+1; }

            $cred_effective_date=$year."/".$month.'/01';
            $state_license_effective_date=$provider->getStateLicenseEffectiveDate();

            $addresses=$provider->getBillingAddress();

            if($addresses){
                foreach ($addresses as $address){
                    //$address=new BillingAddress();
                    $min_age=$address->getOfficeAgeLimit();
                    $max_age=$address->getOfficeAgeLimitMax();
                    $plan_EPSDT="No";
                    if($min_age<19){
                        $plan_EPSDT="Yes";
                    }

                    if(strlen($min_age)==1){
                        $min_age='0'.$min_age;
                    }
                    if(strlen($max_age)==1){
                        $max_age='0'.$max_age;
                    }

                    if($max_age=="00"){
                        $max_age="99";
                    }

                    $is_primary="No";
                    if($address->getPrimaryAddr()==true){
                        $is_primary="Yes";
                    }

                    $see_pattiens="No";

                    if($address->getSeePatientsAddr()==true){
                        $see_pattiens="Yes";
                    }

                    $street=$address->getStreet();
                    $suite=$address->getSuiteNumber();
                    $city=$address->getCity();
                    $state=$address->getUsState();
                    $zipcode=$address->getZipCode();
                    $phone=$address->getPhoneNumber();

                    $phone = str_replace('-', "", $phone);

                    $ada = "N";
                    if ($address->getWheelchair() == true) {
                        $ada = "Y";
                    }

                    $Mon_START="";
                    $Mon_END="";
                    $Tues_START="";
                    $Tues_END="";
                    $Wed_START="";
                    $Wed_END="";
                    $Thurs_START="";
                    $Thurs_END="";
                    $Fri_Start="";
                    $Fri_END="";
                    $Sat_START="";
                    $Sat_END="";
                    $Sun_START="";
                    $Sun_END="";

                    $bussinesOurs = $address->getBusinessHours();
                    if($bussinesOurs!=""){
                        $bussinesOurs = substr($bussinesOurs, 1);
                        $bussinesOurs = substr($bussinesOurs, 0, -1);

                        $bussinesOurs = str_replace('{', '', $bussinesOurs);
                        $bussinesOurs = str_replace('}', '', $bussinesOurs);
                        $bussinesOurs = str_replace('"', '', $bussinesOurs);

                        $daysHours = explode(",", $bussinesOurs);

                        //sunday
                        $dayActive = explode(":", $daysHours[18])[1];
                        $startHour = substr($daysHours[19], 9);
                        $tillHour = substr($daysHours[20], 9);

                        if ($dayActive == "true") {
                            $Sun_START=strtoupper($startHour);
                            $Sun_END=strtoupper($tillHour);
                        }

                        //monday
                        $dayActive = explode(":", $daysHours[0])[1];
                        $startHour = substr($daysHours[1], 9);
                        $tillHour = substr($daysHours[2], 9);
                        if ($dayActive == "true") {
                            $Mon_START=strtoupper($startHour);
                            $Mon_END= strtoupper($tillHour);
                        }

                        //tuesday
                        $dayActive = explode(":", $daysHours[3])[1];
                        $startHour = substr($daysHours[4], 9);
                        $tillHour = substr($daysHours[5], 9);
                        if ($dayActive == "true") {
                            $Tues_START=strtoupper($startHour);
                            $Tues_END=strtoupper($tillHour);
                        }

                        //Wednesday
                        $dayActive = explode(":", $daysHours[6])[1];
                        $startHour = substr($daysHours[7], 9);
                        $tillHour = substr($daysHours[8], 9);
                        if ($dayActive == "true") {
                            $Wed_START=strtoupper($startHour);
                            $Wed_END=strtoupper($tillHour);
                        }

                        //Thurs
                        $dayActive = explode(":", $daysHours[9])[1];
                        $startHour = substr($daysHours[10], 9);
                        $tillHour = substr($daysHours[11], 9);
                        if ($dayActive == "true") {
                            $Thurs_START=strtoupper($startHour);
                            $Thurs_END=strtoupper($tillHour);
                        }

                        //Frid
                        $dayActive = explode(":", $daysHours[12])[1];
                        $startHour = substr($daysHours[13], 9);
                        $tillHour = substr($daysHours[14], 9);
                        if ($dayActive == "true") {
                            $Fri_Start=strtoupper($startHour);
                            $Fri_END=strtoupper($tillHour);
                        }

                        //sat
                        $dayActive = explode(":", $daysHours[15])[1];
                        $startHour = substr($daysHours[16], 9);
                        $tillHour = substr($daysHours[17], 9);
                        if ($dayActive == "true") {
                            $Sat_START=strtoupper($startHour);
                            $Sat_END=strtoupper($tillHour);
                        }
                    }

                    $key="";
                    if($key_cont<10){
                        $key=$provider->getNpiNumber()."-".'00'.$key_cont;
                    }else{
                        $key=$provider->getNpiNumber()."-".'0'.$key_cont;
                    }

                    $key_cont++;


                    //get billing Address
                    $orgAddress = $this->em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $organization->getId()));

                    $payto_street = "";
                    $payto_city = "";
                    $payto_usState = "";
                    $payto_zipCode = "";
                    $payto_phoneNumber = "";

                    $payto_group_state_medicaid_ID="";

                    if (count($orgAddress) == 1) {
                        foreach ($orgAddress as $addrB) {
                            $payto_street=$addrB->getStreet();
                            $payto_city=$addrB->getCity();
                            $payto_usState=$addrB->getUsState();
                            $payto_zipCode=$addrB->getZipCode();
                            $phone_number_B = $addrB->getPhoneNumber();
                            $phone_number_B = str_replace('-', "", $phone_number_B);
                            $payto_phoneNumber=$phone_number_B;

                            $payto_group_state_medicaid_ID=$addrB->getGroupMedicaid();
                        }
                    }

                    if (count($orgAddress) > 1) {
                        foreach ($orgAddress as $addrB) {
                            if ($addrB->getBillingAddr() == true) {
                                $hasBilling = true;
                                $street = $addrB->getStreet();
                                $city = $addrB->getCity();
                                $usState = $addrB->getUsState();
                                $zipCode = $addrB->getZipCode();
                                $phone_number_B = $addrB->getPhoneNumber();
                                $phone_number_B = str_replace('-', "", $phone_number_B);
                                $payto_group_state_medicaid_ID=$addrB->getGroupMedicaid();
                            }
                        }

                        if ($hasBilling == true) {
                            $payto_street=$street;
                            $payto_city=$city;
                            $payto_usState=$usState;
                            $payto_zipCode=$zipCode;
                            $payto_phoneNumber=$phone_number_B;
                        }else{

                            foreach ($orgAddress as $addrB) {
                                $payto_street=$addrB->getStreet();
                                $payto_city=$addrB->getCity();
                                $payto_usState=$addrB->getUsState();
                                $payto_zipCode=$addrB->getZipCode();
                                $phone_number_B = $addrB->getPhoneNumber();
                                $phone_number_B = str_replace('-', "", $phone_number_B);
                                $payto_phoneNumber=$phone_number_B;
                                $payto_group_state_medicaid_ID=$addrB->getGroupMedicaid();
                                break;
                            }
                        }
                    }

                    $practice_location_name=$address->getLocationName();

                    $data=[
                        'col_a'=>'',
                        'col_b'=>$see_pattiens,
                        'col_c'=>$cred_effective_date,
                        'col_d'=>$npi,
                        'col_e'=>$last_name,
                        'col_f'=>$initial,
                        'col_g'=>$first_name,
                        'col_h'=>$degree_str,
                        'col_i'=>$gender_str,
                        'col_j'=>$dob,
                        'col_l'=>$specialty_name_1,
                        'col_m'=>$specialty_code_1,
                        'col_r'=>$specialty_name_2,
                        'col_s'=>$specialty_code_2,
                        'col_x'=>$specialty_name_3,
                        'col_y'=>$specialty_code_3,
                        'col_ad'=>$specialty_name_4,
                        'col_ae'=>$specialty_code_4,
                        'col_an'=>$medicaid_id,
                        'col_ao'=>$medicaid_license_expiration_date,
                        'col_ap'=>$state_license,
                        'col_aq'=>$state_license_effective_date,
                        'col_ar'=>$license_expire_date,
                        'col_as'=>$dea_expirations,
                        'col_at'=>$dea_number,
                        'col_au'=>$dea_state,
                        'col_ax'=>$language1,
                        'col_ay'=>$language2,
                        'col_az'=>$language3,
                        'col_ba'=>$language4,
                        'col_bb'=>$language5,
                        'col_bc'=>$language6,
                        'col_bd'=>$language7,
                        'col_be'=>$language8,
                        'col_bf'=>$min_age,
                        'col_bg'=>$max_age,
                        'col_bh'=>$plan_EPSDT,
                        'col_bi'=>$practice_name,
                        'col_bj'=>$group_npi,
                        'col_bk'=>$fax_no,
                        'col_bl'=>$Prac_IRSType,
                        'col_bm'=>$Prac_IRS,
                        'col_bn'=>$is_primary,
                        'col_bp'=>$see_pattiens,
                        'col_bo'=>$practice_location_name,
                        'col_bq'=>$cred_effective_date,
                        'col_br'=>$suite,
                        'col_bs'=>$street,
                        'col_bt'=>$city,
                        'col_bu'=>$state,
                        'col_bw'=>$zipcode,
                        'col_bx'=>$phone,
                        'col_by'=>$fax_no,
                        'col_bz'=>'Y',
                        'col_ca'=>'both',
                        'col_cb'=>'N',
                        'col_cc'=>$ada,
                        'col_cn'=>$taxonomyCode1,
                        'col_co'=>$taxonomyCode2,
                        'col_cp'=>$taxonomyCode3,
                        'col_cq'=>$cred_date,
                        'col_cr'=>$Mon_START,
                        'col_cs'=>$Mon_END,
                        'col_ct'=>$Tues_START,
                        'col_cu'=>$Tues_END,
                        'col_cv'=>$Wed_START,
                        'col_cw'=>$Wed_END,
                        'col_cx'=>$Thurs_START,
                        'col_cy'=>$Thurs_END,
                        'col_cz'=>$Fri_Start,
                        'col_da'=>$Fri_END,
                        'col_db'=>$Sat_START,
                        'col_dc'=>$Sat_END,
                        'col_dd'=>$Sun_START,
                        'col_de'=>$Sun_END,
                        'col_dn'=>$payto_group_state_medicaid_ID,
                        'col_do'=>$payto_street,
                        'col_dp'=>$payto_city,
                        'col_dq'=>$payto_usState,
                        'col_ds'=>$payto_zipCode,
                        'col_dt'=>$payto_phoneNumber,
                        'col_du'=>$fax_no
                    ];

                    $payData=new AETPayData();
                    $payData->setData($data);
                    $payData->setNpi($npi);
                    $payData->setStatus('active');

                    $this->em->persist($payData);
                    //$io->writeln($cont." ".$npi." ");

                    $cont++;
                }
            }
        }

        $this->em->flush();
        /*
        foreach ($aetna_data_org as $datum){
            $organization_id = $datum->getOrganizationId();
            $npi = $datum->getNpi();

            $data = $datum->getData();
            $organization=$this->em->getRepository('App:Organization')->find($organization_id);
            $last_name=$data['last_name'];

            $addresses=$this->em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization_id));

            if($addresses){
                foreach ($addresses as $address){
                    $data=[
                        'col_a'=>'',
                        'col_b'=>'',
                        'col_c'=>'',
                        'col_d'=>$npi,
                        'col_e'=>$last_name,
                        'col_f'=>'',
                        'col_g'=>'',
                        'col_h'=>'',
                        'col_i'=>'',
                        'col_j'=>'',
                        'col_l'=>'',
                        'col_m'=>'',
                        'col_r'=>'',
                        'col_s'=>'',
                        'col_x'=>'',
                        'col_y'=>'',
                        'col_ad'=>'',
                        'col_ae'=>'',
                        'col_an'=>'',
                        'col_ao'=>'',
                        'col_ap'=>'',
                        'col_ar'=>'',
                        'col_as'=>'',
                        'col_at'=>'',
                        'col_au'=>'',
                        'col_ax'=>'',
                        'col_ay'=>'',
                        'col_az'=>'',
                        'col_ba'=>'',
                        'col_bb'=>'',
                        'col_bc'=>'',
                        'col_bd'=>'',
                        'col_be'=>'',
                        'col_bf'=>'',
                        'col_bg'=>'',
                        'col_bh'=>'',
                        'col_bi'=>'',
                        'col_bj'=>'',
                        'col_bk'=>'',
                        'col_bl'=>'',
                        'col_bm'=>'',
                        'col_bn'=>'',
                        'col_bp'=>'',
                        'col_bq'=>'',
                        'col_br'=>'',
                        'col_bs'=>'',
                        'col_bt'=>'',
                        'col_bu'=>'',
                        'col_bw'=>'',
                        'col_bx'=>'',
                        'col_by'=>'',
                        'col_bz'=>'Y',
                        'col_ca'=>'both',
                        'col_cb'=>'N',
                        'col_cc'=>'',
                        'col_cn'=>'',
                        'col_co'=>'',
                        'col_cp'=>'',
                        'col_cq'=>'',
                        'col_cr'=>'',
                        'col_cs'=>'',
                        'col_ct'=>'',
                        'col_cu'=>'',
                        'col_cv'=>'',
                        'col_cw'=>'',
                        'col_cx'=>'',
                        'col_cy'=>'',
                        'col_cz'=>'',
                        'col_da'=>'',
                        'col_db'=>'',
                        'col_dc'=>'',
                        'col_dd'=>'',
                        'col_de'=>'',
                        'col_dn'=>'',
                        'col_do'=>'',
                        'col_dp'=>'',
                        'col_dq'=>'',
                        'col_ds'=>'',
                        'col_dt'=>'',
                        'col_du'=>''
                    ];

                    $payData=new AETPayData();
                    $payData->setData($data);
                    $payData->setNpi($npi);
                    $payData->setStatus('active');

                    $this->em->persist($payData);
                    $io->writeln($cont." ".$organization->getName()." ");

                    $cont++;
                }
            }
        }*/

        $this->em->flush();

        $io->success('Process Pay Finished');

        return Command::SUCCESS;
    }
}
