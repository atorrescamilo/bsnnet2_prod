<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UpdateIndicatorsCommand extends Command
{
    protected static $defaultName = 'app:update-indicators';
    private $entityManager;

    protected function configure(){
        $this
            ->setDescription('Command for update the data for Key Indicators for MMM');
    }

    public function __construct( EntityManagerInterface $entityManager){
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $current_year=date('Y');
        //process al save the different data

        //1 -Contating
        //1.1 Number of providers that requested to be part of the network (Get this info from join us form)
        $joins=$this->entityManager->getRepository('App\Entity\JOIN')->findAll();
        $mmm_indicator_1_1=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>$current_year,'indicator'=>'Number of providers that requested to be part of the network'));

        $cont_mont=[0,0,0,0,0,0,0,0,0,0,0,0,0];

        foreach ($joins as $join){
            $moth=$join->getCreatedAt()->format('m');
            if($moth=='01'){ $cont_mont[0]++; }
            if($moth=='02'){ $cont_mont[1]++; }
            if($moth=='03'){ $cont_mont[2]++; }
            if($moth=='04'){ $cont_mont[3]++; }
            if($moth=='05'){ $cont_mont[4]++; }
            if($moth=='06'){ $cont_mont[5]++; }
            if($moth=='07'){ $cont_mont[6]++; }
            if($moth=='08'){ $cont_mont[7]++; }
            if($moth=='09'){ $cont_mont[8]++; }
            if($moth=='10'){ $cont_mont[9]++; }
            if($moth=='11'){ $cont_mont[10]++; }
            if($moth=='12'){ $cont_mont[11]++; }
        }
        $mmm_indicator_1_1->setData(implode(",",$cont_mont));

        $this->entityManager->persist($mmm_indicator_1_1);
        $this->entityManager->flush();

        //1.2 Number of providers that signed contract
        $organizations=$this->entityManager->getRepository('App\Entity\Organization')->findAll();

        $mmm_indicator_1_2=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>$current_year,'indicator'=>'Number of providers that signed contract'));

        //set data for 2020
        $mmm_indicator_1_2_l=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>2020,'indicator'=>'Number of providers that signed contract'));

        $cont_mont_2_2020=[0,0,0,0,0,0,0,0,0,0,0,0,0];
        $cont_mont_2=[0,0,0,0,0,0,0,0,0,0,0,0,0];
        foreach ($organizations as $organization){
            $moth=$organization->getCreatedOn()->format('m');
            $year=$organization->getCreatedOn()->format('Y');

            if($year=='2020'){
                if($moth=='01'){ $cont_mont_2_2020[0]++; }
                if($moth=='02'){ $cont_mont_2_2020[1]++; }
                if($moth=='03'){ $cont_mont_2_2020[2]++; }
                if($moth=='04'){ $cont_mont_2_2020[3]++; }
                if($moth=='05'){ $cont_mont_2_2020[4]++; }
                if($moth=='06'){ $cont_mont_2_2020[5]++; }
                if($moth=='07'){ $cont_mont_2_2020[6]++; }
                if($moth=='08'){ $cont_mont_2_2020[7]++; }
                if($moth=='09'){ $cont_mont_2_2020[8]++; }
                if($moth=='10'){ $cont_mont_2_2020[9]++; }
                if($moth=='11'){ $cont_mont_2_2020[10]++; }
                if($moth=='12'){ $cont_mont_2_2020[11]++; }
            }
            if($year=='2021'){
                if($moth=='01'){ $cont_mont_2[0]++; }
                if($moth=='02'){ $cont_mont_2[1]++; }
                if($moth=='03'){ $cont_mont_2[2]++; }
                if($moth=='04'){ $cont_mont_2[3]++; }
                if($moth=='05'){ $cont_mont_2[4]++; }
                if($moth=='06'){ $cont_mont_2[5]++; }
                if($moth=='07'){ $cont_mont_2[6]++; }
                if($moth=='08'){ $cont_mont_2[7]++; }
                if($moth=='09'){ $cont_mont_2[8]++; }
                if($moth=='10'){ $cont_mont_2[9]++; }
                if($moth=='11'){ $cont_mont_2[10]++; }
                if($moth=='12'){ $cont_mont_2[11]++; }
            }
        }

        $mmm_indicator_1_2->setData(implode(",",$cont_mont_2));
        $this->entityManager->persist($mmm_indicator_1_2);
        $this->entityManager->flush();

        $mmm_indicator_1_2_l->setData(implode(",",$cont_mont_2_2020));
        $this->entityManager->persist($mmm_indicator_1_2_l);
        $this->entityManager->flush();

        //1.3 Number of provider voluntarily terminated from the network
        $mmm_indicator_1_3_l=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>2020,'indicator'=>'Number of provider voluntarily terminated from the network'));
        $mmm_indicator_1_3=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>$current_year,'indicator'=>'Number of provider voluntarily terminated from the network'));

        $providers=$this->entityManager->getRepository('App\Entity\Provider')->findBy(array('disabled'=>1));
        $cont_mont_3_2020=[0,0,0,0,0,0,0,0,0,0,0,0,0];
        $cont_mont_3=[0,0,0,0,0,0,0,0,0,0,0,0,0];


        foreach ($providers as $provider){
            $tr=$provider->getTerminationReason();
            $dr=0;
            if($tr!=null){
                $dr=$tr->getId();
            }

            if($dr==1 or $dr==2 or $dr==3 or $dr==4 or $dr==10 or $dr==11 or $dr==12){
                $disabled_date=$provider->getDisabledDate();
                if($disabled_date!=null and $disabled_date!=""){
                    $dd=explode('/',$disabled_date);
                    $moth=$dd[0];
                    $year=$dd[2];

                    if($year=='2020'){
                        if($moth=='01'){ $cont_mont_3_2020[0]++; }
                        if($moth=='02'){ $cont_mont_3_2020[1]++; }
                        if($moth=='03'){ $cont_mont_3_2020[2]++; }
                        if($moth=='04'){ $cont_mont_3_2020[3]++; }
                        if($moth=='05'){ $cont_mont_3_2020[4]++; }
                        if($moth=='06'){ $cont_mont_3_2020[5]++; }
                        if($moth=='07'){ $cont_mont_3_2020[6]++; }
                        if($moth=='08'){ $cont_mont_3_2020[7]++; }
                        if($moth=='09'){ $cont_mont_3_2020[8]++; }
                        if($moth=='10'){ $cont_mont_3_2020[9]++; }
                        if($moth=='11'){ $cont_mont_3_2020[10]++; }
                        if($moth=='12'){ $cont_mont_3_2020[11]++; }
                    }
                    if($year=='2021'){
                        if($moth=='01'){ $cont_mont_3[0]++; }
                        if($moth=='02'){ $cont_mont_3[1]++; }
                        if($moth=='03'){ $cont_mont_3[2]++; }
                        if($moth=='04'){ $cont_mont_3[3]++; }
                        if($moth=='05'){ $cont_mont_3[4]++; }
                        if($moth=='06'){ $cont_mont_3[5]++; }
                        if($moth=='07'){ $cont_mont_3[6]++; }
                        if($moth=='08'){ $cont_mont_3[7]++; }
                        if($moth=='09'){ $cont_mont_3[8]++; }
                        if($moth=='10'){ $cont_mont_3[9]++; }
                        if($moth=='11'){ $cont_mont_3[10]++; }
                        if($moth=='12'){ $cont_mont_3[11]++; }
                    }
                }

            }
        }

        $mmm_indicator_1_3->setData(implode(",",$cont_mont_3));
        $this->entityManager->persist($mmm_indicator_1_3);
        $this->entityManager->flush();
        $mmm_indicator_1_3_l->setData(implode(",",$cont_mont_3_2020));
        $this->entityManager->persist($mmm_indicator_1_3_l);
        $this->entityManager->flush();


        //1.4 Number of providers involuntarily terminated by the network
        $mmm_indicator_1_4_l=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>2020,'indicator'=>'Number of providers involuntarily terminated by the network'));
        $mmm_indicator_1_4=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>$current_year,'indicator'=>'Number of providers involuntarily terminated by the network'));
        $cont_mont_4_2020=[0,0,0,0,0,0,0,0,0,0,0,0,0];
        $cont_mont_4=[0,0,0,0,0,0,0,0,0,0,0,0,0];

        foreach ($providers as $provider){
            $tr=$provider->getTerminationReason();
            $dr=0;
            if($tr!=null){
                $dr=$tr->getId();
            }
            if($dr==5 or $dr==6 or $dr==7 or $dr==8 or $dr==9 or $dr==13){
                $disabled_date=$provider->getDisabledDate();
                if($disabled_date!=null and $disabled_date!=""){
                    $dd=explode('/',$disabled_date);
                    $moth=$dd[0];
                    $year=$dd[2];

                    if($year=='2020'){
                        if($moth=='01'){ $cont_mont_4_2020[0]++; }
                        if($moth=='02'){ $cont_mont_4_2020[1]++; }
                        if($moth=='03'){ $cont_mont_4_2020[2]++; }
                        if($moth=='04'){ $cont_mont_4_2020[3]++; }
                        if($moth=='05'){ $cont_mont_4_2020[4]++; }
                        if($moth=='06'){ $cont_mont_4_2020[5]++; }
                        if($moth=='07'){ $cont_mont_4_2020[6]++; }
                        if($moth=='08'){ $cont_mont_4_2020[7]++; }
                        if($moth=='09'){ $cont_mont_4_2020[8]++; }
                        if($moth=='10'){ $cont_mont_4_2020[9]++; }
                        if($moth=='11'){ $cont_mont_4_2020[10]++; }
                        if($moth=='12'){ $cont_mont_4_2020[11]++; }
                    }
                    if($year=='2021'){
                        if($moth=='01'){ $cont_mont_4[0]++; }
                        if($moth=='02'){ $cont_mont_4[1]++; }
                        if($moth=='03'){ $cont_mont_4[2]++; }
                        if($moth=='04'){ $cont_mont_4[3]++; }
                        if($moth=='05'){ $cont_mont_4[4]++; }
                        if($moth=='06'){ $cont_mont_4[5]++; }
                        if($moth=='07'){ $cont_mont_4[6]++; }
                        if($moth=='08'){ $cont_mont_4[7]++; }
                        if($moth=='09'){ $cont_mont_4[8]++; }
                        if($moth=='10'){ $cont_mont_4[9]++; }
                        if($moth=='11'){ $cont_mont_4[10]++; }
                        if($moth=='12'){ $cont_mont_4[11]++; }
                    }
                }
            }
        }

        $mmm_indicator_1_4->setData(implode(",",$cont_mont_4));
        $this->entityManager->persist($mmm_indicator_1_4);
        $this->entityManager->flush();
        $mmm_indicator_1_4_l->setData(implode(",",$cont_mont_4_2020));
        $this->entityManager->persist($mmm_indicator_1_4_l);
        $this->entityManager->flush();


        //2.1 and 2.2 Number of applications received / Number of applications reviewed
        $credentilings=$this->entityManager->getRepository('App\Entity\CredSimpleRecord')->findAll();

        $mmm_indicator_2_1=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>$current_year,'indicator'=>'Number of applications received'));
        $mmm_indicator_2_1_l=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>'2020','indicator'=>'Number of applications received'));

        $mmm_indicator_2_2=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>$current_year,'indicator'=>'Number of applications reviewed'));
        $mmm_indicator_2_2_l=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>'2020','indicator'=>'Number of applications reviewed'));

        $mmm_indicator_2_3=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>$current_year,'indicator'=>'Number of applications approved'));
        $mmm_indicator_2_3_l=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>'2020','indicator'=>'Number of applications approved'));

        $mmm_indicator_2_4=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>$current_year,'indicator'=>'Number of applications denied'));
        $mmm_indicator_2_4_l=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>'2020','indicator'=>'Number of applications denied'));

        $mmm_indicator_3_1=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>$current_year,'indicator'=>'New Cases approved in Credentialing Committee (Initial Credentialing)'));
        $mmm_indicator_3_1_l=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>'2020','indicator'=>'New Cases approved in Credentialing Committee (Initial Credentialing)'));

        $mmm_indicator_3_2=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>$current_year,'indicator'=>'New Cases denied in Credentialing Committee (Initial Credentialing)'));
        $mmm_indicator_3_2_l=$this->entityManager->getRepository('App\Entity\MMMIndicator')
            ->findOneBy(array('year'=>'2020','indicator'=>'New Cases denied in Credentialing Committee (Initial Credentialing)'));


        $cont_mont_21_2020=[0,0,0,0,0,0,0,0,0,0,0,0,0];
        $cont_mont_21=[0,0,0,0,0,0,0,0,0,0,0,0,0];

        $cont_mont_23_2020=[0,0,0,0,0,0,0,0,0,0,0,0,0];
        $cont_mont_23=[0,0,0,0,0,0,0,0,0,0,0,0,0];

        $cont_mont_24_2020=[0,0,0,0,0,0,0,0,0,0,0,0,0];
        $cont_mont_24=[0,0,0,0,0,0,0,0,0,0,0,0,0];

        foreach ($credentilings as $credentiling){
            $due_date=$credentiling->getDueDate();
            $desition_date=$credentiling->getDecisionDate();

            $dd=explode('/',$due_date);
            $moth=$dd[0];
            $year=$dd[2];

            $desition_moth="";
            $desition_year="";

            if($desition_date!=""){
                $d_date=explode('/',$desition_date);
                $desition_moth=$d_date[0];
                $desition_year=$d_date[2];
            }

            $status=$credentiling->getStatus();

            if($year=='2020'){
                if($moth=='01'){ $cont_mont_21_2020[0]++; }
                if($moth=='02'){ $cont_mont_21_2020[1]++; }
                if($moth=='03'){ $cont_mont_21_2020[2]++; }
                if($moth=='04'){ $cont_mont_21_2020[3]++; }
                if($moth=='05'){ $cont_mont_21_2020[4]++; }
                if($moth=='06'){ $cont_mont_21_2020[5]++; }
                if($moth=='07'){ $cont_mont_21_2020[6]++; }
                if($moth=='08'){ $cont_mont_21_2020[7]++; }
                if($moth=='09'){ $cont_mont_21_2020[8]++; }
                if($moth=='10'){ $cont_mont_21_2020[9]++; }
                if($moth=='11'){ $cont_mont_21_2020[10]++; }
                if($moth=='12'){ $cont_mont_21_2020[11]++; }
            }

            if($desition_year=='2020'){
                if($status=='approved'){
                    if($desition_moth=='01'){ $cont_mont_23_2020[0]++; }
                    if($desition_moth=='02'){ $cont_mont_23_2020[1]++; }
                    if($desition_moth=='03'){ $cont_mont_23_2020[2]++; }
                    if($desition_moth=='04'){ $cont_mont_23_2020[3]++; }
                    if($desition_moth=='05'){ $cont_mont_23_2020[4]++; }
                    if($desition_moth=='06'){ $cont_mont_23_2020[5]++; }
                    if($desition_moth=='07'){ $cont_mont_23_2020[6]++; }
                    if($desition_moth=='08'){ $cont_mont_23_2020[7]++; }
                    if($desition_moth=='09'){ $cont_mont_23_2020[8]++; }
                    if($desition_moth=='10'){ $cont_mont_23_2020[9]++; }
                    if($desition_moth=='11'){ $cont_mont_23_2020[10]++; }
                    if($desition_moth=='12'){ $cont_mont_23_2020[11]++; }
                }

                if($status=='denied'){
                    if($desition_moth=='01'){ $cont_mont_24_2020[0]++; }
                    if($desition_moth=='02'){ $cont_mont_24_2020[1]++; }
                    if($desition_moth=='03'){ $cont_mont_24_2020[2]++; }
                    if($desition_moth=='04'){ $cont_mont_24_2020[3]++; }
                    if($desition_moth=='05'){ $cont_mont_24_2020[4]++; }
                    if($desition_moth=='06'){ $cont_mont_24_2020[5]++; }
                    if($desition_moth=='07'){ $cont_mont_24_2020[6]++; }
                    if($desition_moth=='08'){ $cont_mont_24_2020[7]++; }
                    if($desition_moth=='09'){ $cont_mont_24_2020[8]++; }
                    if($desition_moth=='10'){ $cont_mont_24_2020[9]++; }
                    if($desition_moth=='11'){ $cont_mont_24_2020[10]++; }
                    if($desition_moth=='12'){ $cont_mont_24_2020[11]++; }
                }
            }

            if($year=='2021'){
                if($moth=='01'){ $cont_mont_21[0]++; }
                if($moth=='02'){ $cont_mont_21[1]++; }
                if($moth=='03'){ $cont_mont_21[2]++; }
                if($moth=='04'){ $cont_mont_21[3]++; }
                if($moth=='05'){ $cont_mont_21[4]++; }
                if($moth=='06'){ $cont_mont_21[5]++; }
                if($moth=='07'){ $cont_mont_21[6]++; }
                if($moth=='08'){ $cont_mont_21[7]++; }
                if($moth=='09'){ $cont_mont_21[8]++; }
                if($moth=='10'){ $cont_mont_21[9]++; }
                if($moth=='11'){ $cont_mont_21[10]++; }
                if($moth=='12'){ $cont_mont_21[11]++; }
            }

            if($desition_year=='2021'){
                if($status=='approved'){
                    if($desition_moth=='01'){ $cont_mont_23[0]++; }
                    if($desition_moth=='02'){ $cont_mont_23[1]++; }
                    if($desition_moth=='03'){ $cont_mont_23[2]++; }
                    if($desition_moth=='04'){ $cont_mont_23[3]++; }
                    if($desition_moth=='05'){ $cont_mont_23[4]++; }
                    if($desition_moth=='06'){ $cont_mont_23[5]++; }
                    if($desition_moth=='07'){ $cont_mont_23[6]++; }
                    if($desition_moth=='08'){ $cont_mont_23[7]++; }
                    if($desition_moth=='09'){ $cont_mont_23[8]++; }
                    if($desition_moth=='10'){ $cont_mont_23[9]++; }
                    if($desition_moth=='11'){ $cont_mont_23[10]++; }
                    if($desition_moth=='12'){ $cont_mont_23[11]++; }
                }
                if($status=='denied'){
                    if($desition_moth=='01'){ $cont_mont_24[0]++; }
                    if($desition_moth=='02'){ $cont_mont_24[1]++; }
                    if($desition_moth=='03'){ $cont_mont_24[2]++; }
                    if($desition_moth=='04'){ $cont_mont_24[3]++; }
                    if($desition_moth=='05'){ $cont_mont_24[4]++; }
                    if($desition_moth=='06'){ $cont_mont_24[5]++; }
                    if($desition_moth=='07'){ $cont_mont_24[6]++; }
                    if($desition_moth=='08'){ $cont_mont_24[7]++; }
                    if($desition_moth=='09'){ $cont_mont_24[8]++; }
                    if($desition_moth=='10'){ $cont_mont_24[9]++; }
                    if($desition_moth=='11'){ $cont_mont_24[10]++; }
                    if($desition_moth=='12'){ $cont_mont_24[11]++; }
                }
            }
        }

        $mmm_indicator_2_1->setData(implode(",",$cont_mont_21));
        $this->entityManager->persist($mmm_indicator_2_1);
        $mmm_indicator_2_1_l->setData(implode(",",$cont_mont_21_2020));
        $this->entityManager->persist($mmm_indicator_2_1_l);
        $this->entityManager->flush();

        $mmm_indicator_2_2->setData(implode(",",$cont_mont_21));
        $this->entityManager->persist($mmm_indicator_2_2);
        $mmm_indicator_2_2_l->setData(implode(",",$cont_mont_21_2020));
        $this->entityManager->persist($mmm_indicator_2_2_l);
        $this->entityManager->flush();

        $mmm_indicator_2_3->setData(implode(",",$cont_mont_23));
        $this->entityManager->persist($mmm_indicator_2_3);
        $mmm_indicator_2_3_l->setData(implode(",",$cont_mont_23_2020));
        $this->entityManager->persist($mmm_indicator_2_3_l);
        $this->entityManager->flush();

        $mmm_indicator_2_4->setData(implode(",",$cont_mont_24));
        $this->entityManager->persist($mmm_indicator_2_4);
        $mmm_indicator_2_4_l->setData(implode(",",$cont_mont_24_2020));
        $this->entityManager->persist($mmm_indicator_2_4_l);
        $this->entityManager->flush();

        $mmm_indicator_3_1->setData(implode(",",$cont_mont_23));
        $this->entityManager->persist($mmm_indicator_3_1);
        $mmm_indicator_3_1_l->setData(implode(",",$cont_mont_23_2020));
        $this->entityManager->persist($mmm_indicator_3_1_l);
        $this->entityManager->flush();

        $mmm_indicator_3_2->setData(implode(",",$cont_mont_24));
        $this->entityManager->persist($mmm_indicator_3_2);
        $mmm_indicator_3_2_l->setData(implode(",",$cont_mont_24_2020));
        $this->entityManager->persist($mmm_indicator_3_2_l);
        $this->entityManager->flush();

        $io->success('All proccess completed.');
        return 0;
    }
}
