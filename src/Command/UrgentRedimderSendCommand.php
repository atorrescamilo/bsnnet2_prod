<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Entity\EmailLog;
use App\Entity\EmailQueued;
use Twig\Environment;

class UrgentRedimderSendCommand extends Command
{
    protected static $defaultName = 'urgent:reminder:send';
    private $mailer;
    private $entityManager;
    private $twig;

    public function __construct( \Swift_Mailer $mailer,EntityManagerInterface $entityManager,Environment $twig)
    {
        parent::__construct();
        $this->mailer = $mailer;
        $this->entityManager = $entityManager;
        $this->twig = $twig;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output){
        $io = new SymfonyStyle($input, $output);

        $contact=$this->entityManager->getRepository('App:OrganizationContact')->find(1140);
        $email=$contact->getEmail();

        $this->sendEmail($output,$email) ;

        $io->success('All emails was sent');
    }

    public function sendEmail($output, $email){
        $date=date('F d,Y');

        $message = (new \Swift_Message('Re: Urgent Reminder: Make Sure Your Provider Directory Information Is Accurate and Up-to-Date '))
            ->setFrom('info@bsnnet.com')
            ->setTo($email)
            ->setBody(
                $this->twig->render(
                    'email/templates/urgent_reminder.html.twig',
                    array('date' =>$date)
                ),
                'text/html'
            );
        $this->mailer->send($message);
    }
}
