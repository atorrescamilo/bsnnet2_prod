<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ProviderUpdateCredstatusCommand extends Command
{
    protected static $defaultName = 'app:provider:update-credstatus';
    private $entityManager;

    /**
     * ProviderFccTrackingCommand constructor.
     * @param $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        //$credentialing=$this->entityManager->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status'=>7));
        $credentialing=$this->entityManager->getRepository('App\Entity\ProviderCredentialing')->findAll();

        $cont=1;
        foreach ($credentialing as $cred){
            $output->writeln($cont."->".$cred->getId());
            $cont++;
        }

        $io->success('All OK');
    }
}
