<?php

namespace App\Command;

use App\Entity\AddressFacilityLicense;
use App\Entity\BillingAddress;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FacilityLicensesCommand extends Command
{
    protected static $defaultName = 'app:facility_licenses';
    private $em;

    public function __construct( EntityManagerInterface $em, ParameterBagInterface $params)
    {
        parent::__construct();
        $this->em = $em;
        $this->params = $params;
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);


        $addresses=$this->em->getRepository('App:BillingAddress')->findAll();

        foreach ($addresses as $addr){
            $fn=$addr->getFacilityNumber();
            $ahca_number=$addr->getAhcaNumber();
            if($addr->getIsFacility()==true){

                if($fn!=0 and $fn!="" and $fn!=null and $fn!=1 and $fn!=2 and $fn!=3 and $fn!=6 and $fn!=11 and $fn!=321){

                    $facilityLicense=new AddressFacilityLicense();
                    $facilityLicense->setAddress($addr);
                    $facilityLicense->setLicenseNumber($fn);
                    $facilityLicense->setAhcaNumber($ahca_number);

                    $this->em->persist($facilityLicense);
                }
            }
        }

        $this->em->flush();

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');
        return Command::SUCCESS;
    }
}
