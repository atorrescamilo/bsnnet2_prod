<?php

namespace App\Command;

use App\Entity\PML2;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Entity\PML;

class UpdatePMLCommand extends Command
{
    protected static $defaultName = 'pml:update';
    private $em;
    private $conn;
    private $params;

    public function __construct( EntityManagerInterface $em, ParameterBagInterface $params)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
        $this->params = $params;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', '-1');
        $io = new SymfonyStyle($input, $output);
        $io->writeln('Downloading File');
        $origen = "http://portal.flmmis.com/FLPublic/Portals/0/StaticContent/Public/Public%20Misc%20Files/prw19002.zip";
        $projectDir=$this->params->get('kernel.project_dir');
        $file_name="pml.zip";
        $io->writeln($projectDir);

        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, $origen);


        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSLVERSION,3);

        $data = curl_exec ($ch);
        $error = curl_error($ch);
        curl_close ($ch);

        $destino = $projectDir."/public/pmls/".$file_name;
        $archivo = fopen($destino, "w+");
        fputs($archivo, $data);
        fclose($archivo);

        $zip=new \ZipArchive();

        $file_pml=$projectDir."/public/pmls/pml.zip";
        $output_path=$projectDir."/public/pmls/ouput";
        $comprimido= $zip->open($file_pml);
        if ($comprimido=== TRUE) {
            $zip->extractTo($output_path);
            $zip->close();
        }

        $pml_file_txt=$projectDir."/public/pmls/ouput/cust/prod/dsfl/data/prw19002.txt";
        $fh = fopen($pml_file_txt,'r');

        //remove all current pml records
        $pmls=$this->em->getRepository('App\Entity\PML')->findAll();
        if($pmls){
            foreach ($pmls as $pml_datum){
                $this->em->remove($pml_datum);
            }
            $this->em->flush();
        }

        $types=['042','043','044','066','067','076','119','124','172','173','174','175','176','177','178','200','201','300','301','302','303','304'
            ,'305','306','390','391','392','393','803','811','812','901','904','905','907','908','916','929','930','931','932','966','968','977','981','991'];
        $count_line=1;
        while ($line = fgets($fh)) {
            $ra=explode('|',$line);
            if($ra[13]!="" and $ra[0]!="764134600" and $ra[22]!="I"){
                if(in_array($ra[4],$types)){
                    $pml=new PML();
                    $pml->setColA($ra[0]);
                    $pml->setColB($ra[1]);
                    $pml->setColC($ra[2]);
                    $pml->setColD($ra[3]);
                    $pml->setColE($ra[4]);
                    $pml->setColF($ra[5]);
                    $pml->setColG($ra[6]);
                    $pml->setColH($ra[7]);
                    $pml->setColI($ra[8]);
                    $pml->setColJ($ra[9]);
                    $pml->setColK($ra[10]);
                    $pml->setColL($ra[11]);
                    $pml->setColM($ra[12]);
                    $pml->setColN($ra[13]);
                    $pml->setColO($ra[14]);
                    $pml->setColP($ra[15]);
                    $pml->setColQ($ra[16]);
                    $pml->setColR($ra[17]);
                    $pml->setColS($ra[18]);
                    $pml->setColT($ra[19]);
                    $pml->setColU($ra[20]);
                    $pml->setColV($ra[21]);
                    $pml->setColW($ra[22]);
                    $pml->setColX($ra[23]);
                    $pml->setColY($ra[24]);

                    $this->em->persist($pml);

                    if($count_line% 1000==0){
                        $this->em->flush();
                    }
                    $io->writeln($count_line." ".$ra[0]);
                    $count_line++;
                }
            }
        }

        $this->em->flush();

        $providers=$this->em->getRepository('App\Entity\Provider')->findAll();
        $organizations=$this->em->getRepository('App\Entity\Organization')->findBy(array('disabled'=>0));
        $pmls2=$this->em->getRepository('App\Entity\PML2')->findAll();

        foreach ($pmls2 as $pml){
            $this->em->remove($pml);
        }
        $this->em->flush();

        $npi_arrays=[];
        $cont=0;
        foreach ($providers as $provider){
            $npi=$provider->getNpiNumber();
            if(!in_array($npi,$npi_arrays) and $npi!="N/A" and $npi!="NOT AVAILABLE" and $npi!=""){
                $npi_arrays[]=$npi;
                $pmls=$this->em->getRepository('App\Entity\PML')->findBy(array('colN'=>$npi));

                if($pmls!=null){
                    foreach ($pmls as $pml){
                        $pml2=new PML2();
                        $pml2->setColA($pml->getColA());
                        $pml2->setCoLB($pml->getColB());
                        $pml2->setColC($pml->getColC());
                        $pml2->setColD($pml->getColD());
                        $pml2->setColE($pml->getColE());
                        $pml2->setColF($pml->getColF());
                        $pml2->setColG($pml->getColG());
                        $pml2->setColH($pml->getColH());
                        $pml2->setColI($pml->getColI());
                        $pml2->setColJ($pml->getColJ());
                        $pml2->setColK($pml->getCoLK());
                        $pml2->setColL($pml->getColL());
                        $pml2->setColM($pml->getColM());
                        $pml2->setColN($pml->getColN());
                        $pml2->setColO($pml->getColO());
                        $pml2->setColP($pml->getColP());
                        $pml2->setColQ($pml->getColQ());
                        $pml2->setColR($pml->getColR());
                        $pml2->setColS($pml->getColS());
                        $pml2->setColT($pml->getColT());
                        $pml2->setColU($pml->getColU());
                        $pml2->setColV($pml->getColV());
                        $pml2->setColW($pml->getColW());
                        $pml2->setColX($pml->getColX());
                        $pml2->setColY($pml->getColY());

                        $this->em->persist($pml2);
                    }
                    $this->em->flush();

                }

                $output->writeln($cont." ".$npi);
                $cont++;
            }
        }

        foreach ($organizations as $organization){
            $npi=$organization->getGroupNpi();
            $billing_addr=$this->em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$npi));

            if(!in_array($npi,$npi_arrays) and $npi!="N/A" and $npi!="NOT AVAILABLE" and $npi!=""){
                $npi_arrays[]=$npi;
                $pmls=$this->em->getRepository('App\Entity\PML')->findBy(array('colN'=>$npi));

                if($pmls!=null) {
                    foreach ($pmls as $pml) {
                        $pml2=new PML2();
                        $pml2->setColA($pml->getColA());
                        $pml2->setCoLB($pml->getColB());
                        $pml2->setColC($pml->getColC());
                        $pml2->setColD($pml->getColD());
                        $pml2->setColE($pml->getColE());
                        $pml2->setColF($pml->getColF());
                        $pml2->setColG($pml->getColG());
                        $pml2->setColH($pml->getColH());
                        $pml2->setColI($pml->getColI());
                        $pml2->setColJ($pml->getColJ());
                        $pml2->setColK($pml->getCoLK());
                        $pml2->setColL($pml->getColL());
                        $pml2->setColM($pml->getColM());
                        $pml2->setColN($pml->getColN());
                        $pml2->setColO($pml->getColO());
                        $pml2->setColP($pml->getColP());
                        $pml2->setColQ($pml->getColQ());
                        $pml2->setColR($pml->getColR());
                        $pml2->setColS($pml->getColS());
                        $pml2->setColT($pml->getColT());
                        $pml2->setColU($pml->getColU());
                        $pml2->setColV($pml->getColV());
                        $pml2->setColW($pml->getColW());
                        $pml2->setColX($pml->getColX());
                        $pml2->setColY($pml->getColY());

                        $this->em->persist($pml2);

                        $output->writeln($organization->getName()." ".$npi);
                    }
                    $this->em->flush();
                }
            }

            if($billing_addr){
                foreach ($billing_addr as $addr){
                    $location_npi=$addr->getLocationNpi();
                    if($location_npi!=$npi){
                        if(!in_array($location_npi,$npi_arrays) and $location_npi!="N/A" and $location_npi!="NOT AVAILABLE" and $location_npi!=""){
                            $npi_arrays[]=$location_npi;
                            $pmls=$this->em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$location_npi));
                            if($pmls!=null) {
                                foreach ($pmls as $pml) {
                                    $pml2=new PML2();
                                    $pml2->setColA($pml->getColA());
                                    $pml2->setCoLB($pml->getColB());
                                    $pml2->setColC($pml->getColC());
                                    $pml2->setColD($pml->getColD());
                                    $pml2->setColE($pml->getColE());
                                    $pml2->setColF($pml->getColF());
                                    $pml2->setColG($pml->getColG());
                                    $pml2->setColH($pml->getColH());
                                    $pml2->setColI($pml->getColI());
                                    $pml2->setColJ($pml->getColJ());
                                    $pml2->setColK($pml->getCoLK());
                                    $pml2->setColL($pml->getColL());
                                    $pml2->setColM($pml->getColM());
                                    $pml2->setColN($pml->getColN());
                                    $pml2->setColO($pml->getColO());
                                    $pml2->setColP($pml->getColP());
                                    $pml2->setColQ($pml->getColQ());
                                    $pml2->setColR($pml->getColR());
                                    $pml2->setColS($pml->getColS());
                                    $pml2->setColT($pml->getColT());
                                    $pml2->setColU($pml->getColU());
                                    $pml2->setColV($pml->getColV());
                                    $pml2->setColW($pml->getColW());
                                    $pml2->setColX($pml->getColX());
                                    $pml2->setColY($pml->getColY());

                                    $this->em->persist($pml2);

                                    $output->writeln($addr->getStreet()." ".$location_npi);
                                }
                                $this->em->flush();
                            }
                        }
                    }
                }
            }
        }
        $this->em->flush();
        $io->success('All Ok');

        return Command::SUCCESS;
    }
}
