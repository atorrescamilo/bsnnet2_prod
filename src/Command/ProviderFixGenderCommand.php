<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ProviderFixGenderCommand extends Command
{
    protected static $defaultName = 'provider:fix:taxonomy';
    private $em;
    private $conn;

    public function __construct( EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $providers=$this->em->getRepository('App\Entity\Provider')->findAll();
        $cont=1;
        foreach ($providers as $provider){
            $npi=$provider->getNpiNumber();

            if($npi!="" and $npi!="N/A" and $npi!="NOT AVAILABLE"){
                $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=200&skip=&pretty=on&version=2.1";
                $json=file_get_contents($url_result,false);

                $data=json_decode($json,true);

                if(count($data)>1) {
                    if ($data['result_count'] > 0) {
                        $type=$data['results'][0]['enumeration_type'];
                        if($type=='NPI-1'){
                            $taxonomiesNPPES=$data['results'][0]['taxonomies'];

                            foreach ($taxonomiesNPPES as $taxonomyNPPE){
                                $code=$taxonomyNPPE['code'];
                                $taxObj=$this->em->getRepository('App\Entity\TaxonomyCode')->findOneBy(array('code'=>$code));

                                $valid=0;
                                $current_records=$provider->getTaxonomyCodes();
                                if(count($current_records)>0){
                                    foreach ($current_records as $current_record){
                                        if($current_record->getCode()==$code){
                                            $valid++;
                                        }
                                    }
                                }

                                if($taxObj!=null and $valid==0){
                                    $provider->addTaxonomyCode($taxObj);
                                    $this->em->persist($provider);
                                    $this->em->flush();

                                    $output->writeln($cont." ".$npi);
                                    $cont++;
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->em->flush();

        $io->success('Process Finished');
    }
}
