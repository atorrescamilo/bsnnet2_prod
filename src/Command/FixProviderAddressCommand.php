<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;


class FixProviderAddressCommand extends Command
{

    protected static $defaultName = 'fcc:set:tracknumbernew';
    private $em;
    private $conn;

    public function __construct( EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
    }

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $providers=$this->em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0));

        foreach ($providers as $provider){

            $organization=$provider->getOrganization();
            $address=$provider->getBillingAddress();
            $address_org=$this->em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization->getId()));

            foreach ($address as $addr){
                $match=0;
                foreach ($address_org as $addr_org){
                    if($addr->getId()==$addr_org->getId()){
                        $match=1;
                    }
                }

                if($match==0){
                    $output->writeln($provider->getId()."->".$addr->getId());
                    $provider->removeBillingAddress($addr);
                    $this->em->persist($provider);
                    $this->em->flush();
                }
            }

        }

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return Command::SUCCESS;
    }
}
