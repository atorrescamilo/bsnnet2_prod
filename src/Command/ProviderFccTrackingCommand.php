<?php

namespace App\Command;

use App\Entity\Provider;
use Proxies\__CG__\App\Entity\Organization;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;

class ProviderFccTrackingCommand extends Command
{
    protected static $defaultName = 'app:provider:fcc-tracking';
    private $entityManager;

    /**
     * ProviderFccTrackingCommand constructor.
     * @param $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        parent::__construct();
        $this->entityManager = $entityManager;
    }


    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $conn = $this->entityManager->getConnection();

        $providers=$this->entityManager->getRepository('App\Entity\Provider')->findBy(array('fcc_tracking'=>null));
         $cont=1;
        foreach ($providers as $provider){
            //$provider=new Provider();
            $npi_number=$provider->getNpiNumber();
            $medicaid=$provider->getMedicaid();

            if($npi_number!="" and $npi_number!="NOT AVAILABLE"){
                $sql="SELECT * FROM pml p WHERE p.col_n=:pml or p.col_a=:medicaid";
                $stmt = $conn->prepare($sql);
                $stmt->execute(array('pml'=>$npi_number,'medicaid'=>$medicaid));
                $pmls= $stmt->fetchAll();

                if(count($pmls)>0){
                    $track="FCC1BSN";

                    $output->writeln($cont."->".$provider->getId());
                    $cont++;
                }

            }

        }

        $io->success('All completed.');
    }
}
