<?php

namespace App\Command;

use App\Entity\Organization;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class OrgCheckOrgClassificationCommand extends Command
{
    protected static $defaultName = 'org:check-org-classification';
    private $em;
    private $conn;
    private $params;

    public function __construct( EntityManagerInterface $em, ParameterBagInterface $params)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
        $this->params = $params;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $organizations=$this->em->getRepository('App:Organization')->findAll();

        $cont=1;
        foreach ($organizations as $organization){
           // $organization=new Organization();
            $org_clasification=$organization->getOrgSpecialtiesArray();
            $npi=$organization->getGroupNpi();
            if(count($org_clasification)==0 or $org_clasification==null){
                $is_cmhc=false;
                $pml2=$this->em->getRepository('App:PML2')->findBy(array('colN'=>$npi));
                if($pml2){
                    foreach ($pml2 as $pml){
                        $providerType=$pml->getColD();
                        if($providerType=='91'){
                            $is_cmhc=true;
                        }
                    }
                }

                if($is_cmhc==false){
                    if($npi!="" and $npi!="N/A" and $npi!="NOT AVAILABLE"){

                        $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=200&skip=&pretty=on&version=2.1";
                        $json=file_get_contents($url_result,false);
                        $data=json_decode($json,true);

                        if(!isset($data['Errors'])){
                            if($data['result_count']>0){
                                $taxonomies=$data['results'][0]['taxonomies'];
                                if(count($taxonomies)>0 and isset($taxonomies)){
                                    foreach ($taxonomies as $taxonomy){
                                        if($taxonomy['code']=='251S00000X'){
                                            $is_cmhc=true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                if($is_cmhc){
                    $cmhc=$this->em->getRepository('App:OrgSpecialty')->find(6);
                    $organization->addOrgSpecialty($cmhc);
                    $this->em->persist($organization);
                    $this->em->flush();

                    $io->writeln($cont." ".$organization->getName());
                    $cont++;
                }
            }
        }

        foreach ($organizations as $organization){
            // $organization=new Organization();
            $org_clasification=$organization->getOrgSpecialtiesArray();

            $pass=1;
            if(count($org_clasification)>0 and $org_clasification!=null){
                foreach ($org_clasification as $oc){
                    if($oc->getId()==6){
                        $pass=0;
                    }
                }
            }

            if($pass==1){
                $npi=$organization->getGroupNpi();

                $is_cmhc=false;
                $pml2=$this->em->getRepository('App:PML2')->findBy(array('colN'=>$npi));
                if($pml2){
                    foreach ($pml2 as $pml){
                        $providerType=$pml->getColD();
                        if($providerType=='91'){
                            $is_cmhc=true;
                        }
                    }
                }

                if($is_cmhc==false){
                    if($npi!="" and $npi!="N/A" and $npi!="NOT AVAILABLE"){

                        $url_result="https://npiregistry.cms.hhs.gov/api/?number=$npi&limit=200&skip=&pretty=on&version=2.1";
                        $json=file_get_contents($url_result,false);
                        $data=json_decode($json,true);

                        if(!isset($data['Errors'])){
                            if($data['result_count']>0){
                                $taxonomies=$data['results'][0]['taxonomies'];
                                if(count($taxonomies)>0 and isset($taxonomies)){
                                    foreach ($taxonomies as $taxonomy){
                                        if($taxonomy['code']=='251S00000X'){
                                            $is_cmhc=true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if($is_cmhc){
                    $cmhc=$this->em->getRepository('App:OrgSpecialty')->find(6);
                    $organization->addOrgSpecialty($cmhc);
                    $this->em->persist($organization);
                    $this->em->flush();

                    $io->writeln($cont." ".$organization->getName()." con otros tipos");
                    $cont++;
                }
            }
        }

        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');
    }
}
