<?php

namespace App\Command;

use App\Entity\EmailLog;
use App\Entity\EmailQueued;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mime\Email;
use Twig\Environment;

class SendEmailsCommand extends Command
{
    protected static $defaultName = 'app:send-emails';
    private $mailer;
    private $entityManager;
    private $twig;

    public function __construct( \Swift_Mailer $mailer,EntityManagerInterface $entityManager,Environment $twig)
    {
        parent::__construct();
        $this->mailer = $mailer;
        $this->entityManager = $entityManager;
        $this->twig = $twig;
    }


    protected function configure()
    {
        $this
            // the short description shown while running "php bin/console list"
            ->setDescription('Send email.')

            // the full command description shown when running the command with
            // the "--help" option
            ->setHelp('This command allows you to send a email...');
    }

    protected function execute(InputInterface $input, OutputInterface $output){
       $io = new SymfonyStyle($input, $output);

        $queueds=$this->entityManager->getRepository('App\Entity\EmailQueued')->findAll();

        if($queueds!=null){
            $cont=0;
            foreach ($queueds as $queued){
                if($cont<1){
                    if($queued!=null){
                            $this->sendEmail($queued,$output) ;
                            //create a email log
                            $status=$this->entityManager->getRepository('App\Entity\EmailStatus')->find(2);
                            $emailLog=new EmailLog();
                            $emailLog->setSentTo($queued->getSentTo());
                            $emailLog->setEmailType($queued->getEmailType());
                            $emailLog->setOrganization($queued->getOrganization());
                            $emailLog->setStatus($status);
                            $emailLog->setVerificationHass($queued->getVerificationHass());
                            $emailLog->setContact($queued->getContact());
                            $emailLog->setProviders($queued->getProviders());
                            $emailLog->setApprovedDate($queued->getApprovedDate());
                            $this->entityManager->persist($emailLog);

                            //remove email queued
                            $this->entityManager->remove($queued);
                            $this->entityManager->flush();
                    }
                }
                $cont++;
            }
        }

        $io->success('All completed.');
    }

    public function sendEmail($queued, $output){
        $email_to=$queued->getSentTo();
        $template=$queued->getEmailType()->getId();
        $date=date('F d,Y');

        //email for Credentialing MMM Notification
        if($template==1){
            $providersId=$queued->getProviders();
            $providersIdArray=explode(',',$providersId);

            $providersArray=array();
            foreach ($providersIdArray as $id){
                $provider= $this->entityManager->getRepository('App\Entity\Provider')->find($id);
                if($provider!=null){
                    $providersArray[]=$provider;
                }
            }

            $message = (new \Swift_Message('Re: MMM of Florida, Inc.'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->twig->render(
                        'email/templates/mmm-notification.html.twig',
                        array('date' =>$date,'queued'=>$queued,'providers'=>$providersArray)
                    ),
                    'text/html'
                );
            $this->mailer->send($message);
        }
        //email for Executed Contract
        if($template==2){
            $folderId=$queued->getOrganization()->getId();
            $pathFile=$this->path."/public/uploads/organizations/".$folderId."/provider_agreement_file_".$folderId.".pdf";

            //get the organization is facility or not
            $address= $this->entityManager->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$folderId));
            $isFacility=false;
            if($address!=null){
                foreach ($address as $addr){
                   if($addr->getIsFacility()==true){
                       $isFacility=true;
                   }
                }
            }

            $path_amendment="";
            if($isFacility==true){
                $path_amendment=$this->path."/public/uploads/organizations/amendment/BSN_FACC/BSN_FACC_".$folderId.".pdf";
            }else{
                $path_amendment=$this->path."/public/uploads/organizations/amendment/BSN_PACC/BSN_PACC_".$folderId.".pdf";
            }

            if(file_exists($pathFile)){
                $message = (new \Swift_Message('Behavioral Services Network Executed Contract'))
                    ->setFrom('info@bsnnet.com')
                    ->setTo($email_to)
                    ->setBody(
                        $this->twig->render(
                            'email/templates/excecuted-contract.html.twig',array('queued'=>$queued)
                        ),
                        'text/html'
                    )
                    ->attach(\Swift_Attachment::fromPath($pathFile)->setFilename('Provider_agreement_file.pdf'));
                $this->mailer->send($message);
            }else{
                $message = (new \Swift_Message('Behavioral Services Network Executed Contract'))
                    ->setFrom('info@bsnnet.com')
                    ->setTo($email_to)
                    ->setBody(
                        $this->twig->render(
                            'email/templates/excecuted-contract.html.twig',array('queued'=>$queued)
                        ),
                        'text/html'
                    );
                $this->mailer->send($message);
            }
        }
        // Send email for Provider Portal Login Notification
        if($template==3){
            $message = (new \Swift_Message('Re: Registration for BSN Provider Portal for ('.$queued->getOrganization()->getName().")"))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->twig->render(
                        'email/templates/provider-login-notification.html.twig',
                        array('date' =>$date,'queued'=>$queued)
                    ),
                    'text/html'
                );
            $this->mailer->send($message);
        }

        // Send email for Credentialing Notification
        if($template==4){
            $pathFile=$this->path."/public/attachment/BSN-PROVIDER-ROSTER-TEMPLATE- MULTIPLE.xlsx";
            $providersId=$queued->getProviders();
            $providersIdArray=explode(',',$providersId);

            $providersArray=array();
            foreach ($providersIdArray as $id){
                $provider= $this->entityManager->getRepository('App\Entity\Provider')->find($id);
                if($provider!=null){
                    $providersArray[]=$provider;
                }
            }

            $effective_date="";
            $approved_date=$queued->getApprovedDate();
            $decision_date_array=explode('/',$approved_date);
            $year=$decision_date_array[2];
            $moth=$decision_date_array[0];
            if($moth=='01'){
                $effective_date="02/01/".$year;
            }
            if($moth=='02'){
                $effective_date="03/01/".$year;
            }
            if($moth=='03'){
                $effective_date="04/01/".$year;
            }
            if($moth=='04'){
                $effective_date="05/01/".$year;
            }
            if($moth=='05'){
                $effective_date="06/01/".$year;
            }
            if($moth=='06'){
                $effective_date="07/01/".$year;
            }
            if($moth=='07'){
                $effective_date="08/01/".$year;
            }
            if($moth=='08'){
                $effective_date="09/01/".$year;
            }
            if($moth=='09'){
                $effective_date="10/01/".$year;
            }
            if($moth=='10'){
                $effective_date="11/01/".$year;
            }
            if($moth=='11'){
                $effective_date="12/01/".$year;
            }
            if($moth=='12'){
                $effective_date="01/01/".$year+1;
            }

            $org_id=$queued->getOrganization()->getId();
            $conn =  $this->entityManager->getConnection();

            $sql="SELECT IF(SUM(IF(ba.is_facility, 1, 0)) > 0, 'Yes', 'No') AS `is_facility`
            FROM  organization o 
            LEFT JOIN billing_address ba on o.id = ba.organization_id
            WHERE o.disabled=0 and o.id= ".$org_id."
            GROUP BY o.id
            ORDER BY o.name";

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $organizations= $stmt->fetchAll();

            $isFacility=0;
            foreach ($organizations as $org){
                if($org['is_facility']=='Yes'){
                    $isFacility=1;
                }
            }

            $message = (new \Swift_Message('Re: Behavioral Services Network Credentialing Decision'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->twig->render(
                        'email/templates/credentialing-notification.html.twig',
                        array('date' =>$date,'queued'=>$queued,'is_facility'=>$isFacility,'providers'=>$providersArray,'effective_date'=>$effective_date,'approved_date'=>$approved_date)
                    ),
                    'text/html'
                );
            $this->mailer->send($message);

        }

        // Send email for MMM Provider Survey
        if($template==5){
            $pathFile=$this->path."/public/attachment/MMMFL-Provider-Survey-2020-fillable.pdf";

            $message = (new \Swift_Message('Re: Provider Satisfaction Survey - 2020'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->twig->render(
                        'email/templates/mmm-survey.html.twig',
                        array('date' =>$date,'queued'=>$queued)
                    ),
                    'text/html'
                )
                ->attach(\Swift_Attachment::fromPath($pathFile)->setFilename('MMMFL-Provider-Survey-2020-fillable.pdf'));
            $this->mailer->send($message);

        }

        // Send email for network announcement
        if($template==6){
            $message = (new \Swift_Message('Things to come.'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->twig->render(
                        'email/templates/network-announcement.html.twig',
                        array('date' =>$date,'queued'=>$queued)
                    ),
                    'text/html'
                );
            $this->mailer->send($message);
        }

        // Send email for network announcement
        if($template==7){
            $message = (new \Swift_Message('Re: Florida Community Care.'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->twig->render(
                        'email/templates/fcc-participation-notification.html.twig',
                        array('date' =>$date,'queued'=>$queued)
                    ),
                    'text/html'
                );
            $this->mailer->send($message);
        }

        // Send email for denied credentialing
        if($template==8){

            $providersId=$queued->getProviders();
            $providersIdArray=explode(',',$providersId);

            $providersArray=array();
            foreach ($providersIdArray as $id){
                $provider= $this->entityManager->getRepository('App\Entity\Provider')->find($id);
                if($provider!=null){
                    $providersArray[]=$provider;
                }
            }

            $message = (new \Swift_Message('Re: Credentialing decision.'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->twig->render(
                        'email/templates/credentialing_denied_notification.html.twig',
                        array('date' =>$date,'queued'=>$queued,'providers'=>$providersArray)
                    ),
                    'text/html'
                );
            $this->mailer->send($message);
        }

        // Send email for FCC Welcome go live letter template
        if($template==9){

            $date=date('F d, Y');
            $pathFile1=$this->path."/public/attachment/BSN-FCC-Plan-Addendum.pdf";
            $pathFile2=$this->path."/public/attachment/FCC-Quick-Reference-Guide-BSN-FINAL.pdf";

            $message = (new \Swift_Message('Re: Credentialing decision.'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->twig->render(
                        'email/templates/fcc_welcome_go_live.html.twig',
                        array('date' =>$date,'queued'=>$queued,'date'=>$date)
                    ),
                    'text/html'
                )
                ->attach(\Swift_Attachment::fromPath($pathFile1)->setFilename('BSN FCC Plan Addendum.pdf'))
                ->attach(\Swift_Attachment::fromPath($pathFile2)->setFilename('FCC Quick Reference Guide BSN.pdf'));
            $this->mailer->send($message);
        }

        // Send email for Holiday message
        if($template==10){
            $message = (new \Swift_Message('BSNnet: Holiday message.'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->twig->render(
                        'email/templates/holiday_message.html.twig'
                    ),
                    'text/html'
                );
            $this->mailer->send($message);
        }

        // Send email for Holiday message
        if($template==11){
            $message = (new \Swift_Message('BSNnet: Missing information in Provider Portal.'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->twig->render(
                        'email/templates/missing_information_provider_portal.html.twig', array('queued'=>$queued)
                    ),
                    'text/html'
                );
            $this->mailer->send($message);
        }

        // Send email for AHCANotification
        if($template==12){
            $message = (new \Swift_Message('BSNnet: AHCA Announcment.'))
                ->setFrom('info@bsnnet.com')
                ->setTo($email_to)
                ->setBody(
                    $this->twig->render(
                        'email/templates/ahca_bsn_notification.html.twig', array('queued'=>$queued)
                    ),
                    'text/html'
                );
            $this->mailer->send($message);
        }

    }
}
