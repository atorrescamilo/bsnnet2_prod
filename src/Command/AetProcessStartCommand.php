<?php

namespace App\Command;

use App\Entity\AETData;
use App\Entity\AETDataOrg;
use App\Entity\AETNAPNVLog;
use App\Entity\OrganizationCredentialing;
use App\Utils\My_Mcript;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class AetProcessStartCommand extends Command
{
    protected static $defaultName = 'aet:process:start';
    private $em;
    private $conn;
    private $private_key;
    private $encoder;
    private $params;

    public function __construct( EntityManagerInterface $em, ParameterBagInterface $params)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
        $this->params = $params;
    }

    protected function configure()
    {
        $this
            ->addArgument('regions', InputArgument::REQUIRED, 'Filter Regions 1-> 6,7 and 11, 2->for All regions',null)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $param_region=$input->getArgument('regions');

        $this->private_key=$this->params->get('private_key');
        $this->encoder=new My_Mcript($this->private_key);

        $providers=$this->em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0,'valid_npi_number'=>1));

        $exclude_npis=[];
        $exclude_ids=[];
        $providerNPIS=[];
        $excludesOrgs=[];
        $organizations_result=[];

        $aetna_data=$this->em->getRepository('App:AETData')->findAll();
        if($aetna_data){
            foreach ($aetna_data as $aetna_datum){
                $this->em->remove($aetna_datum);
            }
            $this->em->flush();
        }

        $aetna_data_org=$this->em->getRepository('App:AETDataOrg')->findAll();
        if($aetna_data_org){
            foreach ($aetna_data_org as $aetna_datum_org){
                $this->em->remove($aetna_datum_org);
            }
            $this->em->flush();
        }

        //get the all records for Providers Excluded
        $providers_excluded=$this->em->getRepository('App:ProviderPayerExcluded')->findBy(array('payer'=>7));
        $organizations_excluded=$this->em->getRepository('App:OrganizationPayerExcluded')->findBy(array('payer'=>7));

        if($providers_excluded){
            foreach ($providers_excluded as $provider_excluded){
                $exclude_npis[]=$provider_excluded->getProvider()->getId();
            }
        }

        if($organizations_excluded){
            foreach ($organizations_excluded as $organization_excluded){
                $organization_ex=$organization_excluded->getOrganization();
                $excludesOrgs[]=$organization_ex->getId();
                $providers_org=$this->em->getRepository('App:Provider')->findBy(array('organization'=>$organization_ex->getId()));
                if($providers_org!=null){
                    foreach ($providers_org as $provider){
                        $exclude_ids[]=$provider->getId();
                    }
                }
            }
        }

        $cont=1;
        $providerTypeOrder=['25','26','30','29','31','07','32','81','97'];
        foreach ($providers as $provider){

            //check if the provider is on regions 6,7 and 11
            $on_region=false;
            $addresses=$provider->getBillingAddress();
            if($addresses){
                foreach ($addresses as $address){
                    $region=$address->getRegion();
                    if($region==6 or $region==7 or $region==11){
                        $on_region=true;
                        break;
                    }

                    if($param_region==2 and $address->getUsState()=="FL"){
                        $on_region=true;
                        break;
                    }
                }
            }

            if($on_region==true){
                $npi=$provider->getNpiNumber();
                $contCredF=0;
                $organization=$provider->getOrganization();
                $id=$provider->getId();
                $track_number_type=6;

                $provider_addrs=$provider->getBillingAddress();
                $have_addr=true;
                if(count($provider_addrs)==0){
                    $providers_npi=$this->em->getRepository('App\Entity\Provider')->findBy(array('npi_number'=>$npi,'disabled'=>0));
                    $have_addr=false;
                    if(count($providers_npi)>1){
                        foreach ($providers_npi as $item){
                            $provider_addrs1=$item->getBillingAddress();
                            if(count($provider_addrs1)>0){
                                $have_addr=true;
                            }
                        }
                    }
                }

                //1-Verification a valid NPI and not excluded NPI or Organization
                if($have_addr==true and $npi!="N/A" and $npi!="NOT AVAILABLE" and $npi!="" and !in_array($npi,$exclude_npis) and !in_array($id,$exclude_ids)) {
                    if(in_array($npi,$providerNPIS)==false and $organization->getOrganizationStatus()->getId()==2 and $organization->getDisabled()==0){

                        $org_pml=$this->em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$organization->getGroupNpi(),'colW'=>'A','colL'=>'ENROLLMENT'));

                        if($org_pml==null){
                            $track_number_type=4;
                        }

                        $track_number="";
                        $dateFac="";
                        $dateEfecFac="";
                        $credentialings=$this->em->getRepository('App:ProviderCredentialing')->findBy(array('credentialing_status'=>4, 'npi_number'=>$npi));

                        //if the provider not have a individual credentialing
                        if($credentialings==null){
                            $pro_org=$provider->getOrganization();
                            if($pro_org!=null){
                                $address=$this->em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$pro_org->getId()));
                                if($address!=null){
                                    foreach ($address as $addr){
                                        $credentialingsF=$this->em->getRepository('App:OrganizationCredentialing')->findBy(array('organization'=>$pro_org->getId(),'credentialing_status'=>4));

                                        if($credentialingsF!=null){
                                            $contCredF=count($credentialingsF);
                                            foreach ($credentialingsF as $cf){
                                                $dateFac=$cf->getCredentialingAcceptedDate();
                                                $dateEfecFac=$cf->getCredentialingEffectiveDate();
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        //2- Credentialing varification
                        if($credentialings!=null or $contCredF>0){

                            $pmls=$this->em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$npi,'colW'=>'A','colL'=>'ENROLLMENT'));
                            $current_date = strtotime(date("d-m-Y H:i:00",time()));

                            $dateF="";
                            $year="";
                            $moth="";
                            $day="";
                            $year_f="";
                            $moth_f="";
                            $day_f="";

                            if($credentialings!=null){
                                foreach ($credentialings as $credentialing){
                                    $date=$credentialing->getCredentialingEffectiveDate();
                                    $date_s_a=$credentialing->getCredentialingAccepted();
                                    if($date!=null){
                                        $dateA=explode('/',$date);
                                        $year=$dateA[2];
                                        $moth=$dateA[0];
                                        $day=$dateA[1];
                                    }

                                    if($date_s_a!=null){
                                        $date_fA=explode('/',$date_s_a);
                                        $year_f=$date_fA[2];
                                        $moth_f=$date_fA[0];
                                        $day_f=$date_fA[1];
                                    }
                                }
                            }

                            if($contCredF>0){
                                if($dateFac!=null and $dateFac!=""){
                                    $dateAceptedFacility=explode('/',$dateFac); //acepted date for facility credentialing
                                    $dateEfecFac_f=explode('/',$dateEfecFac);

                                    $cred_date=$dateAceptedFacility[2].$dateAceptedFacility[0].$dateAceptedFacility[1];
                                    $start_date=$dateEfecFac_f[2].$dateEfecFac_f[0].$dateEfecFac_f[1];
                                    $dateF=substr($dateEfecFac_f[2],2,2).$dateEfecFac_f[0];
                                }
                            }

                            if(strlen($moth)==1){
                                $moth="0".$moth;
                            }

                            if(strlen($day)==1){
                                $day="0".$day;
                            }

                            if($contCredF==0){
                                $dateF=substr($year,2,2).$moth;
                                $start_date=$year.$moth.$day;
                                $cred_date=$year_f.$moth_f.$day_f;
                            }

                            $ID_pro=$provider->getId();
                            $id_length=strlen($ID_pro);
                            if($id_length==1){
                                $ID_pro="0000".$ID_pro;
                            }
                            if($id_length==2){
                                $ID_pro="000".$ID_pro;
                            }
                            if($id_length==3){
                                $ID_pro="00".$ID_pro;
                            }
                            if($id_length==4){
                                $ID_pro="0".$ID_pro;
                            }

                            $track_number="COV1BSN".$dateF.$ID_pro;
                            if($dateF==""){
                                $track_number=0;
                            }

                            //3-PML Record Verification
                            $proTypeArray=array();
                            $pmlsArray=array();
                            if($pmls!=null){
                                $pml_pass=0;

                                foreach ($pmls as $pml) {
                                    $dateY=$pml->getColY();
                                    $proTypeCode = $pml->getColD();

                                    $dateYArray=explode('-',$dateY);
                                    $dateentry=$dateYArray[2]."-".$dateYArray[1]."-".$dateYArray[0]." 01:00:00";
                                    $date_entry = strtotime($dateentry);

                                    if($pml->getColL()!="LIMITED" and $pml->getColL()!="REGISTERED" and $current_date < $date_entry){
                                        if ($proTypeCode != "" and !in_array($proTypeCode, $proTypeArray)) {
                                            $proTypeArray[]=$proTypeCode;
                                            $pmlsArray[]=$pml;
                                            $pml_pass++;
                                        }
                                    }
                                }

                                if($pml_pass>0){
                                    $license=$provider->getStateLic();
                                    $providerTypeCodes="";
                                    $specialtiesCodes="";
                                    $medicaid_code="";
                                    $match_row=0;

                                    if($license=="not found" or $license=="NOTFOUND"){
                                        $license="";
                                    }
                                    $license=str_replace(" ","",$license);
                                    $license=trim($license);

                                    if(count($proTypeArray)>0) {
                                        foreach ($providerTypeOrder as $order) {
                                            foreach ($pmlsArray as $pt) {
                                                if($match_row==0){
                                                    if(count($pmlsArray)>1){
                                                        if ($pt->getColD() == $order  and $pt->getColL()=="ENROLLMENT") {
                                                            $providerTypeCodes = $pt->getColD();
                                                            $specialtiesCodes = $pt->getColE();
                                                            $medicaid_code = $pt->getColA();
                                                            if($license==""){
                                                                $license=$pt->getColV();
                                                            }
                                                            $match_row=1;
                                                            break 2;
                                                        }
                                                    }else{
                                                        if ($pt->getColD() == $order) {
                                                            $providerTypeCodes = $pt->getColD();
                                                            $specialtiesCodes = $pt->getColE();
                                                            $medicaid_code = $pt->getColA();
                                                            if($license==""){
                                                                $license=$pt->getColV();
                                                            }
                                                            $match_row=1;
                                                            break 2;
                                                        }
                                                    }

                                                }
                                            }
                                            //If the provider NOT have a valid license on BSNnet DB set the value on
                                            if($license==""){
                                                foreach ($pmlsArray as $pt) {
                                                    if($pt->getColV()!="" and $pt->getColV()!=null){
                                                        $license=$pt->getColV();
                                                    }
                                                }
                                            }
                                        }

                                        if (strlen($providerTypeCodes) == 1) {
                                            $providerTypeCodes = "00" . $providerTypeCodes;
                                        }
                                        if (strlen($providerTypeCodes) == 2) {
                                            $providerTypeCodes = "0" . $providerTypeCodes;
                                        }

                                        if (strlen($specialtiesCodes) < 3) {
                                            $specialtiesCodes = "0" . $specialtiesCodes;
                                        }

                                        $medicaid_length = strlen($medicaid_code);
                                        if ($medicaid_length < 9) {
                                            $rest = 9 - $medicaid_length;
                                            while ($rest > 0) {
                                                $medicaid_code = "0" . $medicaid_code;
                                                $rest--;
                                            }
                                        }
                                    }

                                    if($license==""){
                                        if($providerTypeCodes=="032"){
                                            $license="99999";
                                        }
                                    }

                                    $license=str_replace('-',"",$license);
                                    $license=str_replace(' ',"",$license);
                                    $license=trim($license);
                                    $first_latter_license=substr($license,0,1);

                                    if(stristr($license, 'ARNP')) {
                                        $license=  str_replace("ARNP","APRN",$license);
                                    }

                                    if($providerTypeCodes!="" and $license!="" and $license!="N/A" and $license!="n/a" and $first_latter_license!="I"
                                        and $first_latter_license!="i" and $license!="notavailable" and $license!="NotAvailable"){

                                        $fein_code="";
                                        $organization=$provider->getOrganization();
                                        if($organization!=null){
                                            $fein_code=$organization->getTinNumber();
                                            if($fein_code==""){
                                                $fein_code=$this->encoder->decryptthis($provider->getSocial());
                                            }
                                        }

                                        $fein_code=str_replace("-","",$fein_code);
                                        $fein_code=str_replace(" ","",$fein_code);

                                        if(strlen($fein_code)>9){
                                            $fein_code=substr($fein_code,0,9);
                                        }

                                        if(strlen($fein_code)==8){
                                            $fein_code="0".$fein_code;
                                        }
                                        if(strlen($fein_code)==7){
                                            $fein_code="00".$fein_code;
                                        }
                                        $fein_code=trim($fein_code);

                                        if($fein_code!=""){
                                            $providerNPIS[]=$npi;
                                            $name=$provider->getFirstName();
                                            $last=$provider->getLastName();

                                            $genderCode=$provider->getGender();
                                            $genderCode=substr($genderCode,0,1);
                                            $genderCode=strtoupper($genderCode);
                                            $provider_ard=$provider->getProviderApplicationReceiptDate();

                                            $pg_log=$this->em->getRepository('App:AETNAPNVLog')->findOneBy(array('record_traking_number'=>$track_number));
                                            $is_new_on_report=false;
                                            if($pg_log==null){
                                                $is_new_on_report=true;

                                                if($provider_ard==null or $provider_ard==""){
                                                    if($start_date=="20190601"){
                                                        $diffDates=['20190522',"20190507","20190403","20190428"];
                                                        $pos=rand(0,3);
                                                        $provider_application_receipt_date=$diffDates[$pos];
                                                    }
                                                    if($start_date=="20190701"){
                                                        $diffDates=['20190611',"20190623","20190505","20190515"];
                                                        $pos=rand(0,3);
                                                        $provider_application_receipt_date=$diffDates[$pos];
                                                    }
                                                    if($start_date=="20190901"){
                                                        $diffDates=['20190720',"20190707","20190601","20190614"];
                                                        $pos=rand(0,3);
                                                        $provider_application_receipt_date=$diffDates[$pos];
                                                    }
                                                    if($start_date=="20200401"){
                                                        $diffDates=['20200322',"20200301","20200227","20200218"];
                                                        $pos=rand(0,3);
                                                        $provider_application_receipt_date=$diffDates[$pos];
                                                    }
                                                    if($start_date=="20200501"){
                                                        $diffDates=['20200401',"20200430","20200327","20200308"];
                                                        $pos=rand(0,3);
                                                        $provider_application_receipt_date=$diffDates[$pos];
                                                    }
                                                    if($start_date=="20201001"){
                                                        $diffDates=['20191119',"20191210","20191203","20191105"];
                                                        $pos=rand(0,3);
                                                        $provider_application_receipt_date=$diffDates[$pos];
                                                    }
                                                    if($start_date=="20201201"){
                                                        $diffDates=['20201115',"20201101","20201020","20201013"];
                                                        $pos=rand(0,3);
                                                        $provider_application_receipt_date=$diffDates[$pos];
                                                    }
                                                    if($cred_date=="20210125"){
                                                        $diffDates=['20201215',"20201105","20201220","20201113"];
                                                        $pos=rand(0,3);
                                                        $provider_application_receipt_date=$diffDates[$pos];
                                                    }

                                                    $pg_log=new AETNAPNVLog();
                                                    $pg_log->setRecordTrakingNumber($track_number);
                                                    $pg_log->setProviderId($medicaid_code);
                                                    $pg_log->setFirstName($provider->getFirstName());
                                                    $pg_log->setLastName($provider->getLastName());
                                                    $pg_log->setLicenseNumber($license);
                                                    $pg_log->setSsnFein($fein_code);
                                                    $pg_log->setNpiNumber($npi);

                                                    $new_date=date('Ymd');
                                                    $pg_log->setProviderType($providerTypeCodes);
                                                    $pg_log->setPrimarySpecialty($specialtiesCodes);
                                                    $pg_log->setGender($genderCode);
                                                    $pg_log->setAppReceiptDate($provider_application_receipt_date);
                                                    $pg_log->setStartDate($new_date);
                                                    $pg_log->setCredentialingDate($cred_date);
                                                    $pg_log->setStatus('new');

                                                    $this->em->persist($pg_log);
                                                    $this->em->flush();

                                                } else{
                                                    $sep=explode("/",$provider_ard);
                                                    $provider_application_receipt_date=$sep[2].$sep[0].$sep[1];
                                                }
                                            } else{
                                                $provider_application_receipt_date=$pg_log->getAppReceiptDate();

                                                if($pg_log->getStatus()=="new"){
                                                    $pg_log->setStatus('active');
                                                    $this->em->persist($pg_log);
                                                    $this->em->flush();
                                                }
                                            }

                                            if($is_new_on_report==true){
                                                $Start_date_report=date('Ymd');
                                            }else{
                                                if($pg_log){
                                                    $Start_date_report= $pg_log->getStartDate();
                                                }else{
                                                    $Start_date_report= date('Ymd');
                                                }
                                            }

                                            $fein_code=str_replace(" ","",$fein_code);

                                            $record=[
                                                'record_tracking_number'=>$track_number,
                                                'provider_id'=>$medicaid_code,
                                                'first_name'=>rtrim($name),
                                                'last_name'=>rtrim($last),
                                                'license_number'=>rtrim($license),
                                                'ssn_fein'=>rtrim($fein_code),
                                                'npi_number'=>rtrim($npi),
                                                'start_date'=>$Start_date_report,
                                                'provider_type'=>$providerTypeCodes,
                                                'primary_specialty'=>$specialtiesCodes,
                                                'gender'=>$genderCode,
                                                'provider_application_receipt_date'=>$provider_application_receipt_date,
                                                'credentialed_date'=>$cred_date,
                                                'end_date'=>''
                                            ];

                                            //save the record for generate the report after to a spreadsheet
                                            $new_data=new AETData();
                                            $new_data->setProviderId($provider->getId());
                                            $new_data->setNpi($npi);
                                            $new_data->setData($record);
                                            if($is_new_on_report==true){
                                                $new_data->setStatus('new');
                                            }else{
                                                $new_data->setStatus('active');
                                            }

                                            $new_data->setTrackNumberType($track_number_type);

                                            $this->em->persist($new_data);
                                            $this->em->flush();

                                            //check if the we have more that one provider with the same NPI
                                            $providers_npi=$this->em->getRepository('App:Provider')->findBy(array('npi_number'=>$npi,'disabled'=>0));

                                            if(count($providers_npi)>1){
                                                foreach ($providers_npi as $item){
                                                    $org2=$item->getOrganization();
                                                    $id_org=$org2->getId();
                                                    if(!in_array($id_org,$organizations_result) and $org2->getOrganizationStatus()->getId()==2){
                                                        $organizations_result[]=$id_org;
                                                    }
                                                }
                                            }else{
                                                $org1=$provider->getOrganization();
                                                $id_org=$org1->getId();

                                                if(!in_array($id_org,$organizations_result)  and !in_array($id_org, $excludesOrgs) and $org1->getOrganizationStatus()->getId()==2){
                                                    $organizations_result[]=$id_org;
                                                }
                                            }

                                            $io->writeln($cont." ".$npi." ".count($provider_addrs)." ".$track_number);
                                            $cont++;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        //verify providers terminated from the last report and update the current pnv pg log
        $pg_logs=$this->em->getRepository('App:AETNAPNVLog')->findAll();
        $date_now = date('d-m-Y');
        $last_date = strtotime('-3 day', strtotime($date_now));
        $last_date = date('Ymd', $last_date);
        foreach ($pg_logs as $pg_log){
            if($pg_log->getFirstName()!="") {
                $npi_log = $pg_log->getNpiNumber();

                if($pg_log->getStatus()=='to_remove'){
                    $pg_log->setStatus('removed');
                    $this->em->persist($pg_log);
                    $this->em->flush();
                }

                if(!in_array($npi_log,$providerNPIS) and $pg_log->getStatus()!='removed'){
                    $record=[
                        'record_tracking_number'=> $pg_log->getRecordTrakingNumber(),
                        'provider_id'=> $pg_log->getProviderId(),
                        'first_name'=>$pg_log->getFirstName(),
                        'last_name'=>$pg_log->getLastName(),
                        'license_number'=>rtrim($pg_log->getLicenseNumber()),
                        'ssn_fein'=>rtrim($pg_log->getSsnFein()),
                        'npi_number'=>$pg_log->getNpiNumber(),
                        'start_date'=> $pg_log->getStartDate(),
                        'provider_type'=>$pg_log->getProviderType(),
                        'primary_specialty'=>$pg_log->getPrimarySpecialty(),
                        'gender'=>$pg_log->getGender(),
                        'provider_application_receipt_date'=>$pg_log->getAppReceiptDate(),
                        'credentialed_date'=>$pg_log->getCredentialingDate(),
                        'end_date'=>$last_date
                    ];

                    $pg_log->setStatus('to_remove');
                    $this->em->persist($pg_log);
                    $this->em->flush();

                    //save the record for generate the report after to a spreadsheet
                    $new_data=new AETData();
                    $new_data->setProviderId($provider->getId());
                    $new_data->setNpi($npi);
                    $new_data->setData($record);
                    $new_data->setStatus('to_remove');
                    $this->em->persist($new_data);
                    $this->em->flush();
                }
            }
        }

        $organization_pg_result=[];
        $organization_tn_result=[];

        $cont_org=1;

        foreach ($organizations_result as $org_id){
            if(!in_array($org_id, $excludesOrgs)){
                $organization=$this->em->getRepository('App:Organization')->find($org_id);
                $pmls = array();
                $groupNpi=$organization->getGroupNpi();
                if($groupNpi!="") {
                    $pmls = $this->em->getRepository('App:PML2')->findBy(array('colN' => $groupNpi, 'colW' => 'A','colL'=>'ENROLLMENT'));
                }

                if($pmls!=null and count($pmls)>0){
                    $address=$this->em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization->getId()));
                    $providers=$this->em->getRepository('App:Provider')->findBy(array('organization'=>$organization->getId()));

                    if($address!=null){
                        //check if the organization have one or more address on regions 6,7 or 11
                        $org_on_region=false;
                        foreach ($address as $addr){
                            if($addr->getRegion()==6 or $addr->getRegion()==7 or $addr->getRegion()==11 ){
                                $org_on_region=true;
                                break;
                            }

                            if($param_region==2 and $addr->getUsState()=="FL"){
                                $org_on_region=true;
                                break;
                            }
                        }

                        if($org_on_region==true){
                            $credentialingsF=$this->em->getRepository('App:OrganizationCredentialing')->findBy(array('organization'=>$org_id,'credentialing_status'=>4));

                            if($contCredF>0){
                                foreach ($credentialingsF as $credF){
                                    $dateC= explode('/',$credF->getCredentialingAcceptedDate());
                                    $dateC2= explode('/',$credF->getCredentialingEffectiveDate());

                                    $cred_date2=$dateC[2].$dateC[0].$dateC[1];
                                    $year=$dateC[2];
                                    $moth1=$dateC[0];
                                    $moth="";

                                    if($moth1=='01'){ $moth='02'; }
                                    if($moth1=='02'){ $moth='03'; }
                                    if($moth1=='03'){ $moth='04'; }
                                    if($moth1=='04'){ $moth='05'; }
                                    if($moth1=='05'){ $moth='06'; }
                                    if($moth1=='06'){ $moth='07'; }
                                    if($moth1=='07'){ $moth='08'; }
                                    if($moth1=='08'){ $moth='09'; }
                                    if($moth1=='09'){ $moth='10'; }
                                    if($moth1=='10'){ $moth='11'; }
                                    if($moth1=='11'){ $moth='12'; }
                                    if($moth1=='12'){
                                        $moth='01';
                                        $year=intval($year)+1;
                                        $year=strval($year);
                                    }

                                    $cred_date=$dateC2[2].$dateC2[0]."01";
                                }
                            }else{
                                foreach ($providers as $provider){
                                    $credentialings_Tem=$this->em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status'=>4,'provider'=>$provider->getId()));
                                    $datesCExist=[];
                                    if($credentialings_Tem!=null){
                                        foreach ($credentialings_Tem as $credeTem){
                                            $dateT=$credeTem->getCredentialingEffectiveDate();
                                            if($dateT!=""){
                                                if(!in_array($dateT,$datesCExist)){
                                                    $datesCExist[]=$dateT;
                                                }
                                            }
                                        }
                                    }
                                    //total of different dates from credentialing providers
                                    $totalDates=count($datesCExist);
                                    if($totalDates==1){
                                        $dateC1= explode('/',$datesCExist[0]);
                                        $yearT=$dateC1[2];
                                        $mothT=$dateC1[0];
                                        $cred_date=$yearT.$mothT."01";
                                    }
                                    if($totalDates>1){
                                        $dateC1= explode('/',$datesCExist[0]);
                                        $yearT=$dateC1[2];
                                        $mothT=$dateC1[0];
                                        $cred_date=$yearT.$mothT."01";
                                    }
                                }
                            }
                        }
                    }

                    $cont_con=1;
                    $year='2021';
                    $dateF=substr($year,2,2);
                    $consecutive="";
                    if($cont_con<10){
                        $consecutive="0".$cont_con;
                    }else{
                        $consecutive=$cont_con;
                    }

                    $track_number="";
                    if($contCredF>0){
                        if($dateFac!=null and $dateFac!=""){
                            $dateA=explode('/',$dateFac);
                            $year=$dateA[2];
                            $moth1=$dateA[0];
                            $day='01';
                            $moth='';

                            if($moth1=='01'){ $moth='02'; }
                            if($moth1=='02'){ $moth='03'; }
                            if($moth1=='03'){ $moth='04'; }
                            if($moth1=='04'){ $moth='05'; }
                            if($moth1=='05'){ $moth='06'; }
                            if($moth1=='06'){ $moth='07'; }
                            if($moth1=='07'){ $moth='08'; }
                            if($moth1=='08'){ $moth='09'; }
                            if($moth1=='10'){ $moth='11'; }
                            if($moth1=='11'){ $moth='12'; }
                            if($moth1=='12'){
                                $moth='01';
                                $year=intval($year)+1;
                                $year=strval($year);
                            }
                        }
                    }

                    $ID_pro=$organization->getId();
                    $id_length=strlen($ID_pro);
                    if($id_length==1){
                        $ID_pro="0000".$ID_pro;
                    }
                    if($id_length==2){
                        $ID_pro="000".$ID_pro;
                    }
                    if($id_length==3){
                        $ID_pro="00".$ID_pro;
                    }
                    if($id_length==4){
                        $ID_pro="0".$ID_pro;
                    }

                    $type3=false;
                    $orgClasifications=$organization->getOrgSpecialtiesArray();

                    if($orgClasifications!=null){
                        foreach ($orgClasifications as $orgC){
                            if($orgC->getName()=="State Mental Hospital" or $orgC->getName()=="General Hospital"){
                                $type3=true;
                            }
                        }
                    }

                    if($type3==false){
                        $track_number="COV2BSN".$dateF.$consecutive.$ID_pro;
                    }else{
                        $track_number="COV3BSN".$dateF.$consecutive.$ID_pro;
                    }

                    $providerTypeOrder=['01','04','16','68','66','05','77','67','08','14','25','26','30','29','31','07','32','81','97'];
                    $groupNpi=$organization->getGroupNpi();
                    $proTypeArray=array();
                    $pmlsArray=array();

                    if($pmls!=null){
                        foreach ($pmls as $pml) {
                            $proTypeCode = $pml->getColD();
                            if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                $proTypeArray[]=$proTypeArray;
                                $pmlsArray[]=$pml;
                            }
                        }
                    }

                    if(count($proTypeArray)==0){
                        if($providers!=null) {
                            foreach ($providers as $provider) {
                                $pmls=$this->em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>"A",'colL'=>'ENROLLMENT'));
                                if($pmls!=null){
                                    foreach ($pmls as $pml) {
                                        $proTypeCode = $pml->getColD();
                                        if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                            $proTypeArray[]=$proTypeArray;
                                            $pmlsArray[]=$pml;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    $pt_code="";
                    $specialty_code="";
                    $medicaid_code="";

                    foreach ($pmlsArray as $pt){
                        foreach ($providerTypeOrder as $order){
                            if($pt->getColD()==$order){
                                $pt_code=$pt->getColD();
                                $specialty_code=$pt->getColE();
                                $medicaid_code=$pt->getColA();
                                break 2;
                            }
                        }
                    }

                    if(strlen($pt_code)==1){
                        $pt_code="00".$pt_code;
                    }
                    if(strlen($pt_code)==2){
                        $pt_code="0".$pt_code;
                    }
                    if(strlen($specialty_code)<3){
                        $specialty_code="0".$specialty_code;
                    }

                    $medicaid_length=strlen($medicaid_code);
                    if($medicaid_length<9){
                        $rest=9-$medicaid_length;
                        while ($rest>0){
                            $medicaid_code="0".$medicaid_code;
                            $rest--;
                        }
                    }

                    if($pt_code!=""){
                        $fein_o=$organization->getTinNumber();
                        if($fein_o=="" or $fein_o==null){
                            $t_providers=$this->em->getRepository('App:Provider')->findBy(array('organization'=>$organization->getId()));
                            if($t_providers!=NULL){
                                foreach ($t_providers as $t_provider){
                                    $fein_o=$this->encoder->decryptthis($t_provider->getSocial());
                                }
                            }
                        }

                        $fein_o=str_replace("-","",$fein_o);
                        $fein_o=str_replace(" ","",$fein_o);

                        if(strlen($fein_o)==7){
                            $fein_o="00".$fein_o;
                        }
                        if(strlen($fein_o)==8){
                            $fein_o="0".$fein_o;
                        }
                        if(strlen($fein_o)>9){
                            $fein_o=substr($fein_o,0,9);
                        }

                        $provider_application_receipt_date="";
                        $is_new_on_report_org=0;
                        $pg_log_1=$this->em->getRepository('App:AETNAPNVLog')->findOneBy(array('record_traking_number'=>$track_number));
                        if($pg_log_1==null){
                            $is_new_on_report_org=1;
                            if($type3==true){
                                $provider_application_receipt_date=  $organization->getCreatedOn()->format('Ymd');
                            }
                        }else{
                            if($type3==true){
                                $provider_application_receipt_date=  $pg_log_1->getAppReceiptDate();
                            }
                        }

                        $start_date="";
                        if($is_new_on_report_org==1){
                            $start_date=date('Ymd');
                        }else{
                            $start_date=$pg_log_1->getStartDate();
                            if($start_date=="" or $start_date==null){
                                $start_date=date('Ymd');
                            }
                        }

                        $ahca_id="";

                        $org_cred_date="";
                        if($type3==true){
                            $address=$this->em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization->getId()));
                            if($address!=null){
                                foreach ($address as $addr){
                                    $ahca_id=$addr->getAhcaNumber();
                                    if($ahca_id!=""){
                                        if(strlen($ahca_id)==6){
                                            $ahca_id="00".$ahca_id;
                                        }
                                        break;
                                    }
                                }
                            }

                            $specialty_code="";
                            if($pg_log_1){
                                $org_cred_date=$pg_log_1->getCredentialingDate();
                            }else{
                                //get the cred date for this hospital
                            }

                            $fein_o="";
                        }

                        $name="";
                        $npi="";
                        if($type3==false){
                            $name=$organization->getName();
                            $npi=$organization->getGroupNpi();
                            $org_cred_date="";
                        }

                        if($pg_log_1==null){
                            $pg_log=new AETNAPNVLog();
                            $pg_log->setRecordTrakingNumber($track_number);
                            $pg_log->setProviderId($medicaid_code);
                            $pg_log->setLastName($name);
                            $pg_log->setSsnFein($fein_o);
                            $pg_log->setNpiNumber($npi);
                            $pg_log->setStartDate($start_date);
                            $pg_log->setPrimarySpecialty($specialty_code);
                            $pg_log->setProviderType($pt_code);

                            if($type3==true){
                                $pg_log->setAppReceiptDate($provider_application_receipt_date);
                                $pg_log->setCredentialingDate($org_cred_date);
                                $pg_log->setAHCAID($ahca_id);
                                $pg_log->setStatus('new');
                                $this->em->persist($pg_log);
                                $this->em->flush();
                            }
                        }

                        //get facility social
                        $address=$this->em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization->getId()));

                        $facility_license="";

                        if($address){
                            foreach ($address as $addr){
                                if($addr->getIsFacility()==true){
                                    $facility_license_obj=$this->em->getRepository('App:AddressFacilityLicense')->findOneBy(array('address'=>$addr->getId()));
                                    if($facility_license_obj){
                                        $facility_license=$facility_license_obj->getLicenseNumber();
                                        $ahca_id=$facility_license_obj->getAhcaNumber();
                                    }
                                    if($facility_license!="" and $ahca_id!=""){
                                        break;
                                    }
                                }
                            }
                        }

                        if(strlen($ahca_id)<8 and $ahca_id!=""){
                            while(strlen($ahca_id)<8){
                                $ahca_id='0'.$ahca_id;
                            }
                        }

                        $org_record=[
                            'record_tracking_number'=>$track_number,
                            'provider_id'=>$medicaid_code,
                            'first_name'=>'',
                            'last_name'=>$name,
                            'license_number'=>$facility_license,
                            'ssn_fein'=>rtrim($fein_o),
                            'npi_number'=>rtrim($npi),
                            'start_date'=>$start_date,
                            'provider_type'=>$pt_code,
                            'primary_specialty'=>$specialty_code,
                            'gender'=>'',
                            'provider_application_receipt_date'=>$provider_application_receipt_date,
                            'credentialed_date'=>$org_cred_date,
                            'end_date'=>'',
                            'ahca_id'=>rtrim($ahca_id)
                        ];

                        $aetna_data_org=new AETDataOrg();
                        $aetna_data_org->setStatus('active');
                        $aetna_data_org->setNpi($organization->getGroupNpi());
                        $aetna_data_org->setData($org_record);
                        $aetna_data_org->setOrganizationId($organization->getId());

                        $this->em->persist($aetna_data_org);
                        $this->em->flush();

                        $organization_pg_result[]=$organization->getId();
                        $organization_specialty[$org_id]=$specialty_code;
                        $organization_tn_result[]=$track_number;

                        $io->writeln($cont_org." ".$org_id." ".$organization->getName()." ".$track_number. " ".$fein_o );
                        $cont_org++;
                    }
                }
            }
        }

        $io->writeln("================================================================================================");
        $io->writeln("Cheking organizations facilities without providers on PMLs");

        $organizations_full=$this->em->getRepository('App\Entity\Organization')->findBy(array('disabled'=>0,'organization_status'=>2));

        $cont_org=1;
        foreach ($organizations_full as $org_full){
            $org_npi=$org_full->getGroupNpi();
            $org_id=$org_full->getId();

            $addresses=$this->em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$org_id));
            $org_on_region=false;
            foreach ($addresses as $addr){
                if($addr->getRegion()==6 or $addr->getRegion()==7 or $addr->getRegion()==11 ){
                    $org_on_region=true;
                }
            }

            if(!in_array($org_id, $excludesOrgs) and $org_on_region==true){
                $is_facility=false;
                if($addresses){
                    foreach ($addresses as $addr){
                        if($addr->getIsFacility()==true){
                            $is_facility=true;
                            break;
                        }
                    }
                }

                if($is_facility){
                    $cont_pro_pml=0;
                    $pmls=$this->em->getRepository('App:PML2')->findBy(array('colN'=>$org_npi,'colW'=>"A",'colL'=>'ENROLLMENT'));
                    if($pmls!=null){
                        $providers=$this->em->getRepository('App:Provider')->findBy(array('organization'=>$org_id));
                        if($providers!=null){
                            foreach ($providers as $pro){
                                $pro_npi=$pro->getNpiNumber();
                                $pro_pmls=$this->em->getRepository('App:PML2')->findBy(array('colN'=>$pro_npi,'colW'=>"A",'colL'=>'ENROLLMENT'));
                                if($pro_pmls!=null){
                                    $cont_pro_pml++;
                                }
                            }
                        }
                    }

                    if($cont_pro_pml==0 and $pmls!=null){
                        $org_cred=$this->em->getRepository('App:OrganizationCredentialing')->findOneBy(array('organization'=>$org_id));
                        if($org_cred!=null){
                            $organization=$this->em->getRepository('App:Organization')->find($org_id);
                            $pmls = array();
                            $groupNpi=$organization->getGroupNpi();
                            if($groupNpi!="") {
                                $pmls = $this->em->getRepository('App:PML2')->findBy(array('colN' => $groupNpi, 'colW' => 'A','colL'=>'ENROLLMENT'));

                                if($pmls!=null and count($pmls)>0){
                                    $address=$this->em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization->getId()));
                                    $providers=$this->em->getRepository('App:Provider')->findBy(array('organization'=>$organization->getId()));
                                    $contCredF=0;
                                    if($address!=null){
                                        $credentialingsF=$this->em->getRepository('App:OrganizationCredentialing')->findBy(array('organization'=>$org_id,'credentialing_status'=>4));
                                        $contCredF=count($credentialingsF);
                                        if($contCredF>0){
                                            foreach ($credentialingsF as $credF){
                                                $dateC= explode('/',$credF->getCredentialingAcceptedDate());

                                                $cred_date2=$dateC[2].$dateC[0].$dateC[1];
                                                $year=$dateC[2];
                                                $moth1=$dateC[0];
                                                $moth="";

                                                if($moth1=='01'){ $moth='02'; }
                                                if($moth1=='02'){ $moth='03'; }
                                                if($moth1=='03'){ $moth='04'; }
                                                if($moth1=='04'){ $moth='05'; }
                                                if($moth1=='05'){ $moth='06'; }
                                                if($moth1=='06'){ $moth='07'; }
                                                if($moth1=='07'){ $moth='08'; }
                                                if($moth1=='08'){ $moth='09'; }
                                                if($moth1=='09'){ $moth='10'; }
                                                if($moth1=='10'){ $moth='11'; }
                                                if($moth1=='11'){ $moth='12'; }
                                                if($moth1=='12'){
                                                    $moth='01';
                                                    $year=intval($year)+1;
                                                    $year=strval($year);
                                                }

                                            }
                                        }else{
                                            foreach ($providers as $provider){
                                                $credentialings_Tem=$this->em->getRepository('App\Entity\ProviderCredentialing')->findBy(array('credentialing_status'=>4,'provider'=>$provider->getId()));
                                                $datesCExist=[];
                                                if($credentialings_Tem!=null){
                                                    foreach ($credentialings_Tem as $credeTem){
                                                        $dateT=$credeTem->getCredentialingEffectiveDate();
                                                        if($dateT!=""){
                                                            if(!in_array($dateT,$datesCExist)){
                                                                $datesCExist[]=$dateT;
                                                            }
                                                        }
                                                    }
                                                }
                                                //total of different dates from credentialing providers
                                                $totalDates=count($datesCExist);
                                                if($totalDates==1){
                                                    $dateC1= explode('/',$datesCExist[0]);
                                                    $yearT=$dateC1[2];
                                                    $mothT=$dateC1[0];
                                                    $cred_date=$yearT.$mothT."01";
                                                }
                                                if($totalDates>1){
                                                    $dateC1= explode('/',$datesCExist[0]);
                                                    $yearT=$dateC1[2];
                                                    $mothT=$dateC1[0];
                                                    $cred_date=$yearT.$mothT."01";
                                                }
                                            }
                                        }
                                    }

                                    $cont_con=1;
                                    // $year=date('Y');
                                    $year='2021';
                                    $dateF=substr($year,2,2);
                                    $consecutive="";
                                    if($cont_con<10){
                                        $consecutive="0".$cont_con;
                                    }else{
                                        $consecutive=$cont_con;
                                    }

                                    $track_number="";
                                    if($contCredF>0){
                                        if($dateFac!=null and $dateFac!=""){
                                            $dateA=explode('/',$dateFac);
                                            $year=$dateA[2];
                                            $moth1=$dateA[0];
                                            $day='01';
                                            $moth='';

                                            if($moth1=='01'){ $moth='02'; }
                                            if($moth1=='02'){ $moth='03'; }
                                            if($moth1=='03'){ $moth='04'; }
                                            if($moth1=='04'){ $moth='05'; }
                                            if($moth1=='05'){ $moth='06'; }
                                            if($moth1=='06'){ $moth='07'; }
                                            if($moth1=='07'){ $moth='08'; }
                                            if($moth1=='08'){ $moth='09'; }
                                            if($moth1=='10'){ $moth='11'; }
                                            if($moth1=='11'){ $moth='12'; }
                                            if($moth1=='12'){
                                                $moth='01';
                                                $year=intval($year)+1;
                                                $year=strval($year);
                                            }
                                        }
                                    }

                                    $ID_pro=$organization->getId();
                                    $id_length=strlen($ID_pro);
                                    if($id_length==1){
                                        $ID_pro="0000".$ID_pro;
                                    }
                                    if($id_length==2){
                                        $ID_pro="000".$ID_pro;
                                    }
                                    if($id_length==3){
                                        $ID_pro="00".$ID_pro;
                                    }
                                    if($id_length==4){
                                        $ID_pro="0".$ID_pro;
                                    }

                                    $type3=false;
                                    $orgClasifications=$organization->getOrgSpecialtiesArray();

                                    if($orgClasifications!=null){
                                        foreach ($orgClasifications as $orgC){
                                            if($orgC->getName()=="State Mental Hospital" or $orgC->getName()=="General Hospital"){
                                                $type3=true;
                                            }
                                        }
                                    }

                                    if($type3==false){
                                        $track_number="COV2BSN".$dateF.$consecutive.$ID_pro;
                                    }else{
                                        $track_number="COV3BSN".$dateF.$consecutive.$ID_pro;
                                    }

                                    $providerTypeOrder=['01','04','16','68','66','05','77','67','08','14','25','26','30','29','31','07','32','81','97'];
                                    $proTypeArray=array();
                                    $pmlsArray=array();

                                    if($pmls!=null){
                                        foreach ($pmls as $pml) {
                                            $proTypeCode = $pml->getColD();
                                            if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                                $proTypeArray[]=$proTypeArray;
                                                $pmlsArray[]=$pml;
                                            }
                                        }
                                    }

                                    if(count($proTypeArray)==0){
                                        if($providers!=null) {
                                            foreach ($providers as $provider) {
                                                $pmls=$this->em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>"A",'colL'=>'ENROLLMENT'));
                                                if($pmls!=null){
                                                    foreach ($pmls as $pml) {
                                                        $proTypeCode = $pml->getColD();
                                                        if ($proTypeArray != "" and !in_array($proTypeCode, $proTypeArray)) {
                                                            $proTypeArray[]=$proTypeArray;
                                                            $pmlsArray[]=$pml;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    $pt_code="";
                                    $specialty_code="";
                                    $medicaid_code="";

                                    foreach ($pmlsArray as $pt){
                                        foreach ($providerTypeOrder as $order){
                                            if($pt->getColD()==$order){
                                                $pt_code=$pt->getColD();
                                                $specialty_code=$pt->getColE();
                                                $medicaid_code=$pt->getColA();
                                                break 2;
                                            }
                                        }
                                    }

                                    if(strlen($pt_code)==1){
                                        $pt_code="00".$pt_code;
                                    }
                                    if(strlen($pt_code)==2){
                                        $pt_code="0".$pt_code;
                                    }
                                    if(strlen($specialty_code)<3){
                                        $specialty_code="0".$specialty_code;
                                    }

                                    $medicaid_length=strlen($medicaid_code);
                                    if($medicaid_length<9){
                                        $rest=9-$medicaid_length;
                                        while ($rest>0){
                                            $medicaid_code="0".$medicaid_code;
                                            $rest--;
                                        }
                                    }

                                    if($pt_code!=""){
                                        $fein_o=$organization->getTinNumber();
                                        if($fein_o=="" or $fein_o==null){
                                            $t_providers=$this->em->getRepository('App\Entity\Provider')->findBy(array('organization'=>$organization->getId()));
                                            if($t_providers!=NULL){
                                                foreach ($t_providers as $t_provider){
                                                    $fein_o=$this->encoder->decryptthis($t_provider->getSocial());
                                                }
                                            }
                                        }

                                        $fein_o=str_replace("-","",$fein_o);
                                        $fein_o=str_replace(" ","",$fein_o);

                                        if(strlen($fein_o)==7){
                                            $fein_o="00".$fein_o;
                                        }
                                        if(strlen($fein_o)==8){
                                            $fein_o="0".$fein_o;
                                        }
                                        if(strlen($fein_o)>9){
                                            $fein_o=substr($fein_o,0,9);
                                        }

                                        $provider_application_receipt_date="";
                                        $is_new_on_report_org=0;
                                        $pg_log_1=$this->em->getRepository('App:AETNAPNVLog')->findOneBy(array('record_traking_number'=>$track_number));
                                        if($pg_log_1==null){
                                            $is_new_on_report_org=1;
                                            if($type3==true){
                                                $provider_application_receipt_date=  $organization->getCreatedOn()->format('Ymd');
                                            }
                                        }else{
                                            if($type3==true){
                                                $provider_application_receipt_date=  $pg_log_1->getAppReceiptDate();
                                            }
                                        }

                                        $start_date="";
                                        if($is_new_on_report_org==1){
                                            $start_date=date('Ymd');
                                        }else{
                                            $start_date=$pg_log_1->getStartDate();
                                            if($start_date=="" or $start_date==null){
                                                $start_date="20210101";
                                            }
                                        }

                                        $ahca_id="";

                                        $org_cred_date="";
                                        if($type3==true){
                                            $address=$this->em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
                                            if($address!=null){
                                                foreach ($address as $addr){
                                                    $ahca_id=$addr->getAhcaNumber();
                                                    if($ahca_id!=""){
                                                        if(strlen($ahca_id)==6){
                                                            $ahca_id="00".$ahca_id;
                                                        }
                                                        break;
                                                    }
                                                }
                                            }

                                            $specialty_code="";
                                            if($pg_log_1){
                                                $org_cred_date=$pg_log_1->getCredentialingDate();
                                            }else{
                                                //get the cred date for this hospital
                                            }

                                            $fein_o="";
                                        }

                                        $name="";
                                        $npi="";
                                        if($type3==false){
                                            $name=$organization->getName();
                                            $npi=$organization->getGroupNpi();
                                            $org_cred_date="";
                                        }

                                        if($pg_log_1==null){
                                            $pg_log=new AETNAPNVLog();
                                            $pg_log->setRecordTrakingNumber($track_number);
                                            $pg_log->setProviderId($medicaid_code);
                                            $pg_log->setLastName($name);
                                            $pg_log->setSsnFein($fein_o);
                                            $pg_log->setNpiNumber($npi);
                                            $pg_log->setStartDate($start_date);
                                            $pg_log->setPrimarySpecialty($specialty_code);
                                            $pg_log->setProviderType($pt_code);

                                            if($type3==true){
                                                $pg_log->setAppReceiptDate($provider_application_receipt_date);
                                                $pg_log->setCredentialingDate($org_cred_date);
                                                $pg_log->setAHCAID($ahca_id);
                                                $pg_log->setStatus('new');
                                                $this->em->persist($pg_log);
                                                $this->em->flush();
                                            }
                                        }

                                        $org_record=[
                                            'record_tracking_number'=>$track_number,
                                            'provider_id'=>$medicaid_code,
                                            'first_name'=>'',
                                            'last_name'=>$name,
                                            'license_number'=>'',
                                            'ssn_fein'=>rtrim($fein_o),
                                            'npi_number'=>rtrim($npi),
                                            'start_date'=>$start_date,
                                            'provider_type'=>$pt_code,
                                            'primary_specialty'=>$specialty_code,
                                            'gender'=>'',
                                            'provider_application_receipt_date'=>$provider_application_receipt_date,
                                            'credentialed_date'=>$org_cred_date,
                                            'end_date'=>'',
                                            'ahca_id'=>rtrim($ahca_id)
                                        ];

                                        $aetna_data_org=new AETDataOrg();
                                        $aetna_data_org->setStatus('active');
                                        $aetna_data_org->setNpi($organization->getGroupNpi());
                                        $aetna_data_org->setData($org_record);
                                        $aetna_data_org->setOrganizationId($organization->getId());

                                        $this->em->persist($aetna_data_org);
                                        $this->em->flush();

                                        $organization_pg_result[]=$organization->getId();
                                        $organization_specialty[$org_id]=$specialty_code;
                                        $organization_tn_result[]=$track_number;

                                        $io->writeln($cont_org." ".$org_id." ".$organization->getName()." ".$track_number. " ".$fein_o );
                                        $cont_org++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $pg_logs=$this->em->getRepository('App:AETNAPNVLog')->findAll();
        foreach ($pg_logs as $pg_log) {
            if ($pg_log->getFirstName() == null) {
                $track_number_log = $pg_log->getRecordTrakingNumber();
                if (!in_array($track_number_log, $organization_tn_result) and $pg_log->getStatus() != 'removed') {
                    $date_now = date('d-m-Y');
                    $last_date = strtotime('-3 day', strtotime($date_now));
                    $last_date = date('Ymd', $last_date);

                    $org_record=[
                        'record_tracking_number'=>$pg_log->getRecordTrakingNumber(),
                        'provider_id'=>$pg_log->getProviderId(),
                        'first_name'=>'',
                        'last_name'=>$pg_log->getLastName(),
                        'license_number'=>'',
                        'ssn_fein'=>rtrim($pg_log->getSsnFein()),
                        'npi_number'=>rtrim($pg_log->getNpiNumber()),
                        'start_date'=>$pg_log->getStartDate(),
                        'provider_type'=>$pg_log->getProviderType(),
                        'primary_specialty'=>$pg_log->getPrimarySpecialty(),
                        'gender'=>'',
                        'provider_application_receipt_date'=>$pg_log->getAppReceiptDate(),
                        'credentialed_date'=>$pg_log->getCredentialingDate(),
                        'end_date'=>$last_date,
                        'ahca_id'=>$ahca_id
                    ];

                    $record_traking_number=$pg_log->getRecordTrakingNumber();
                    $npi_3=00000000;

                    $io->writeln($record_traking_number);
                    if(stristr($record_traking_number, 'COV3')){
                        $subId=substr($record_traking_number,-3);

                        if(substr($subId,0,1)=="0"){
                            $subId=substr($subId,1);
                        }
                        $io->writeln("ORG ID AET3: ".$subId);

                        $organization_log=$this->em->getRepository('App\Entity\Organization')->find($subId);

                    }else{
                        $organization_log=$this->em->getRepository('App\Entity\Organization')->findOneBy(array('group_npi'=>$pg_log->getNpiNumber()));
                    }

                    $aet_data_org=new AETDataOrg();
                    $aet_data_org->setStatus('active');
                    $npi_t=$pg_log->getNpiNumber();

                    if($npi_t){
                        $aet_data_org->setNpi($npi_t);
                    }else{
                        $aet_data_org->setNpi($npi_3);
                    }
                    $aet_data_org->setData($org_record);
                    if($organization_log){
                        $aet_data_org->setOrganizationId($organization_log->getId());
                    }

                    $this->em->persist($aet_data_org);
                    $this->em->flush();

                }
            }
        }
        $io->writeln("Total of Organizations: ".count($organizations_result));
        $io->success('All completed.');
        //end function
    }
}
