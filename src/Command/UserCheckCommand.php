<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Security\Core\SecurityContext;

class UserCheckCommand extends Command
{
    protected static $defaultName = 'users:status';
    private $em;
    private $conn;

    public function __construct( EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
    }


    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $users=$this->em->getRepository('App\Entity\User')->findAll();

        foreach ($users as $user) {
            $output->writeln($user->getName()." ".$user->getLastName()." ".$user->isActiveNow());
        }

        $io->success('Finish the process');

        return 0;
    }
}
