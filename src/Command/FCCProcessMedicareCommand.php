<?php

namespace App\Command;

use App\Entity\Organization;
use App\Utils\My_Mcript;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Entity\FCCRosterDataMedicare;

class FCCProcessMedicareCommand extends Command
{
    protected static $defaultName = 'fcc:process:medicare';
    private $em;
    private $conn;
    private $private_key;
    private $encoder;
    private $params;

    protected function configure(): void
    {
        $this
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    public function __construct( EntityManagerInterface $em, ParameterBagInterface $params)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
        $this->params = $params;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $this->private_key=$this->params->get('private_key');
        $this->encoder=new My_Mcript($this->private_key);

        $current_records=$this->em->getRepository('App:FCCRosterDataMedicare')->findAll();
        if($current_records){
            foreach ($current_records as $current_record){
                $this->em->remove($current_record);
            }
            $this->em->flush();
        }

        $providerNPIS=[];
        $organizationIds=[];
        $organizations_result=[];
        $pmlsArray=[];

        $providers=$this->em->getRepository('App\Entity\Provider')->findBy(array('disabled'=>0,'valid_npi_number'=>1));

        $cont=1;
        foreach ($providers as $provider){

            $valid_medicare=false;
            $medicare=$provider->getMedicare();
            $opted_out=$provider->getOptionOutMedicare();
            $npi=$provider->getNpiNumber();

            $organization=$provider->getOrganization();
            $address=$this->em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization->getId()));

            if ($opted_out == false and $medicare != "" and $medicare != "-" and $medicare != "." and $medicare != "-N/A" and $medicare != "0" and $medicare != "00000" and $medicare != "000000" and $medicare != "0000000"
                and $medicare != "00000000" and $medicare != "000000000" and $medicare != "000000000000" and $medicare != "n/a" and $medicare != "55525 - opted out" and
                $medicare != "Interal Medicine" and $medicare != "Internal Medicine" and $medicare != "N/A" and $medicare != "NA" and $medicare != "None" and $medicare != "Pending" and
                $medicare != "Psychiatrist" and $medicare != "Psychologist" and $medicare != "in procress" and $medicare != "in progress" and $medicare != "submitting application" and
                $medicare != "In-progress" and $medicare != "Pending" and $medicare != "in progress" and $medicare != "In Progress" and $medicare != "N/A" and $medicare != "PENDING" and $medicare != "Pending" and
                $medicare != "pending" and $medicare != "blank" and $medicare != "none" and $medicare != "NONE" and $medicare != "Psychiatrist and Medical Director" and $medicare != "ARNP - Medical" and
                $medicare != "ARNP - Psychiatric" and $medicare != "ARNP - Medical" and $medicare != "ARNP - Psychiatric") {
                $valid_medicare = true;
            }

            if ($valid_medicare == false and $opted_out == false) {
                if ($address) {
                    foreach ($address as $addr) {
                        $medi = $addr->getGroupMedicare();
                        if ($medi != "" and $medi != "-" and $medi != "." and $medi != "-N/A" and $medi != "0" and $medi != "00000" and $medi != "000000" and $medi != "0000000"
                            and $medi != "00000000" and $medi != "000000000" and $medi != "000000000000" and $medi != "n/a" and $medi != "55525 - opted out" and
                            $medi != "Interal Medicine" and $medi != "Internal Medicine" and $medi != "N/A" and $medi != "NA" and $medi != "None" and $medi != "Pending" and
                            $medi != "Psychiatrist" and $medi != "Psychologist" and $medi != "in procress" and $medi != "in progress" and $medi != "submitting application" and
                            $medi != "In-progress" and $medi != "Pending" and $medi != "in progress" and $medi != "In Progress" and $medi != "N/A" and $medi != "PENDING" and $medi != "Pending" and
                            $medi != "pending" and $medi != "blank" and $medi != "none" and $medi != "NONE" and $medi != "Psychiatrist and Medical Director" and $medi != "ARNP - Medical" and
                            $medi != "ARNP - Psychiatric" and $medi != "ARNP - Medical" and $medi != "ARNP - Psychiatric") {

                            $valid_medicare = true;
                        }
                    }
                }
            }

            if ($valid_medicare==true and $npi!="N/A" and $npi!="NOT AVAILABLE" and $npi!="" and $organization->getOrganizationStatus()->getId()==2) {
                if (!in_array($npi, $providerNPIS)) {
                    $providerNPIS[] = $npi;

                    $is_credentialing=false;
                    //check credentialing status
                    $credentialings=$this->em->getRepository('App:ProviderCredentialing')->findBy(array('credentialing_status'=>4, 'npi_number'=>$npi));

                    if($credentialings!=null){
                        $is_credentialing=true;
                    }else{
                        $facility_credentialings=$this->em->getRepository('App:OrganizationCredentialing')->findBy(array('credentialing_status'=>4, 'organization'=>$organization->getId()));

                        if($facility_credentialings!=null){
                            $is_credentialing=true;
                        }
                    }

                    $org_id = $organization->getId();

                    if($is_credentialing==true){
                        $provider_addrs=$provider->getBillingAddress();
                        $have_addr=true;
                        if(count($provider_addrs)==0){
                            $providers_npi=$this->em->getRepository('App\Entity\Provider')->findBy(array('npi_number'=>$npi,'disabled'=>0));
                            $have_addr=false;
                            if(count($providers_npi)>1){
                                foreach ($providers_npi as $item){
                                    $provider_addrs1=$item->getBillingAddress();
                                    if(count($provider_addrs1)>0){
                                        $have_addr=true;
                                    }
                                }
                            }
                        }

                        if($have_addr==true){
                            if (!in_array($org_id, $organizationIds) and $organization->getDisabled()==0) {
                                $organizationIds[] = $org_id;
                                $organizations_result[] = $organization;
                            }

                            $current_date = strtotime(date("d-m-Y H:i:00",time()));
                            $name=$provider->getFirstName();
                            $last=$provider->getLastName();

                            $isDr="";
                            $degrees=$provider->getDegree();
                            if($degrees!=null){
                                foreach ($degrees as $degree){
                                    $isDr=$degree->getName();
                                }
                            }

                            $license=$provider->getStateLic();
                            if($license=="not found" or $license=="NOTFOUND"){
                                $license="";
                            }
                            $license=str_replace(" ","",$license);
                            $license=trim($license);

                            $specialtiesArray=[];
                            $specialty1 = "";
                            $specialty2 = "";
                            $specialty3 = "";
                            $specialty4 = "";
                            $taxonomyCode1="";
                            $taxonomyCode2="";
                            $taxonomyCode3="";

                            $providerTypeOrder=['25','26','30','29','31','07','32','81','97'];
                            $proTypeArray=[];

                            $pmls=$this->em->getRepository('App\Entity\PML2')->findBy(array('colN'=>$provider->getNpiNumber(),'colW'=>'A','colL'=>'ENROLLMENT'));
                            $pml_pass=0;
                            foreach ($pmls as $pml){
                                $proTypeCode = $pml->getColD();
                                $dateY=$pml->getColY();

                                $dateYArray=explode('-',$dateY);
                                $dateentry=$dateYArray[2]."-".$dateYArray[1]."-".$dateYArray[0]." 01:00:00";
                                $date_entry = strtotime($dateentry);

                                if($pml->getColL()!="LIMITED" and $pml->getColL()!="REGISTERED" and $current_date < $date_entry){
                                    if ($proTypeCode != "" and !in_array($proTypeCode, $proTypeArray)) {
                                        $proTypeArray[]=$proTypeCode;
                                        $pmlsArray[]=$pml;
                                        $pml_pass++;
                                    }
                                }
                            }

                            $providerTypeCodes="";

                            if($license==""){
                                foreach ($pmlsArray as $pt) {
                                    if($pt->getColV()!="" and $pt->getColV()!=null){
                                        $license=$pt->getColV();
                                    }
                                }
                            }

                            if(count($proTypeArray)>0) {
                                $match_row=0;
                                foreach ($providerTypeOrder as $order) {
                                    foreach ($pmlsArray as $pt) {
                                        if($match_row==0) {
                                            if (count($pmlsArray) > 1) {
                                                if ($pt->getColD() == $order  and $pt->getColL()=="ENROLLMENT") {
                                                    $providerTypeCodes = $pt->getColD();
                                                    if($license==""){
                                                        $license=$pt->getColV();
                                                    }
                                                    $match_row=1;
                                                    break 2;
                                                }
                                            }
                                        }else{
                                            if ($pt->getColD() == $order) {
                                                $providerTypeCodes = $pt->getColD();

                                                if($license==""){
                                                    $license=$pt->getColV();
                                                }
                                                $match_row=1;
                                                break 2;
                                            }
                                        }
                                    }
                                }
                            }

                            if (strlen($providerTypeCodes) == 1) {
                                $providerTypeCodes = "00" . $providerTypeCodes;
                            }
                            if (strlen($providerTypeCodes) == 2) {
                                $providerTypeCodes = "0" . $providerTypeCodes;
                            }

                            foreach ($pmlsArray as $pml){
                                $spe=$pml->getColE();
                                if($spe!=""){
                                    if(!in_array($spe,$specialtiesArray)){
                                        $specialtiesArray[]=$spe;
                                    }
                                }
                            }

                            $eClaimProviderType = "SP";
                            $pro_degrees = $provider->getDegree();
                            if ($pro_degrees != null) {
                                foreach ($pro_degrees as $degree) {
                                    if ($degree->getId() == 1) {
                                        $eClaimProviderType = "NP";
                                        break;
                                    }
                                    if ($degree->getId() == 19) {
                                        if($specialty1!='92901' and $specialty2!='92901' and $specialty3!='92901' and $specialty4!='92901'){
                                            $eClaimProviderType = "PA";
                                            break;
                                        }
                                    }
                                }
                            }

                            if($license==""){
                                if($providerTypeCodes=="032"){
                                    $license="99999";
                                }
                            }
                            $license=str_replace('-',"",$license);
                            $license=str_replace(' ',"",$license);
                            $license=trim($license);
                            $first_latter_license=substr($license,0,1);

                            if(stristr($license, 'ARNP')) {
                                $license=  str_replace("ARNP","APRN",$license);
                            }

                            if ($medicare == "" or $medicare == "-" or $medicare == "." or $medicare == "-N/A" or $medicare == "0" or $medicare == "00000" or $medicare == "000000" or $medicare == "0000000"
                                or $medicare == "00000000" or $medicare == "000000000" or $medicare == "000000000000" or $medicare == "n/a" or $medicare == "55525 - opted out" or
                                $medicare == "Interal Medicine" or $medicare == "Internal Medicine" or $medicare == "N/A" or $medicare == "NA" or $medicare == "None" or $medicare == "Pending" or
                                $medicare == "Psychiatrist" or $medicare == "Psychologist" or $medicare == "in procress" or $medicare == "in progress" or $medicare == "submitting application" or
                                $medicare == "In-progress" or $medicare == "Pending" or $medicare == "in progress" or $medicare == "In Progress" or $medicare == "N/A" or $medicare == "PENDING" or $medicare == "Pending" or
                                $medicare == "pending" or $medicare == "blank" or $medicare == "none" or $medicare == "NONE" or $medicare == "Psychiatrist or Medical Director" or $medicare == "ARNP - Medical" or
                                $medicare == "ARNP - Psychiatric" or $medicare == "ARNP - Medical" or $medicare == "ARNP - Psychiatric") {
                                $medicare = "";
                            }

                            $genderCode=$provider->getGender();
                            if($genderCode!=""){
                                $genderCode=substr($genderCode,0,1);
                                $genderCode=strtoupper($genderCode);
                            }else{
                                $genderCode="";
                            }

                            $language1="";$language2="";$language3="";$language4="";$language5="";$language6="";$language7="";$language8="";
                            $languages=$provider->getLanguages();
                            if($languages!=null){
                                if(count($languages)>0){
                                    $language1=$languages[0]->getLangCd();
                                }
                                if(count($languages)>1){
                                    $language2=$languages[1]->getLangCd();
                                }
                                if(count($languages)>2){
                                    $language3=$languages[2]->getLangCd();
                                }
                                if(count($languages)>3){
                                    $language4=$languages[3]->getLangCd();
                                }
                                if(count($languages)>4){
                                    $language5=$languages[4]->getLangCd();
                                }
                                if(count($languages)>5){
                                    $language6=$languages[5]->getLangCd();
                                }
                                if(count($languages)>6){
                                    $language7=$languages[6]->getLangCd();
                                }
                                if(count($languages)>7){
                                    $language8=$languages[7]->getLangCd();
                                }
                            }
                            if($language1==""){
                                $language1="ENG";
                            }

                            $tin = $organization->getTinNumber();
                            if ($tin != "" and $tin!=null) {
                                $Prac_IRSType='T';
                                $Prac_IRS_No= $tin;
                            }else{

                                $social=$provider->getSocial();
                                if($social!=null and $social!=""){
                                    $tin=$this->encoder->decryptthis($social);
                                    $Prac_IRSType='S';
                                    $Prac_IRS_No= $tin;
                                }
                            }

                            $Prac_IRS_No=str_replace("-","",$Prac_IRS_No);

                            $specialty_name="";
                            $pdataMedicare=$this->em->getRepository('App:PDataMedicare')->findOneBy(array('npi_number'=>$npi));
                            if($pdataMedicare){
                                $json=$pdataMedicare->getData();
                                $data=json_decode($json,true);

                                if(count($data)>1 and $data['result_count']>0){
                                    $taxonomies=$data['results'][0]['taxonomies'];

                                    $cont_tax=1;
                                    foreach ($taxonomies as $taxonomy){
                                        if($taxonomy['primary']==true){
                                            $specialty_name=$taxonomy['desc'];
                                        }
                                        if($cont_tax==1){
                                            $taxonomyCode1=$taxonomy['code'];
                                        }
                                        if($cont_tax==2){
                                            $taxonomyCode2=$taxonomy['code'];
                                        }

                                        if($cont_tax==3){
                                            $taxonomyCode3=$taxonomy['code'];
                                        }

                                        $cont_tax++;
                                    }
                                }
                            }

                            $reimbursement="85%";
                            $rates=$this->em->getRepository('App:OrganizationRates')->findBy(array('organization'=>$organization->getId()));
                            if($rates){
                                foreach ($rates as $rate){
                                    if($rate->getRate()->getId()==1){

                                        $new_eimbursement=$rate->getPercent();
                                        if($new_eimbursement!=""){
                                            $reimbursement=$new_eimbursement."%";
                                        }
                                    }
                                }
                            }

                            //get Other General Data from provider
                            $key_cont=1;
                            foreach ($provider_addrs as $addr){
                                $street_check=$addr->getStreet();
                                $addr_pass=true;
                                if(stristr($street_check, 'BOX') or stristr($street_check, 'Box') or stristr($street_check, 'box') or $addr->getUsState()!="FL" or $addr->getDisabled()==1) {
                                    $addr_pass=false;
                                }

                                if($addr_pass==true and $first_latter_license!="i"){

                                    $site_index="";
                                    $key="";
                                    if($key_cont<10){
                                        $key='I'.$provider->getNpiNumber()."-".'00'.$key_cont;
                                        $site_index="00".$key_cont;
                                    }else{
                                        $key=$provider->getNpiNumber()."-".'0'.$key_cont;
                                        $site_index="0".$key_cont;
                                    }
                                    $key_cont++;


                                    $exempt="";
                                    if($addr->getSeePatientsAddr()==false){
                                        $exempt="DE";
                                    }

                                    $phone_number = $addr->getPhoneNumber();
                                    $phone_number = str_replace('-', "", $phone_number);

                                    if($phone_number==""){
                                        $org_pro=$provider->getOrganization();
                                        $phone_number=$org_pro->getPhone();
                                        $phone_number = str_replace('-', "", $phone_number);
                                    }

                                    $ada = "N";
                                    if ($addr->getWheelchair() == true) {
                                        $ada = "Y";
                                    }

                                    $Mon_START="";
                                    $Mon_END="";
                                    $Tues_START="";
                                    $Tues_END="";
                                    $Wed_START="";
                                    $Wed_END="";
                                    $Thurs_START="";
                                    $Thurs_END="";
                                    $Fri_Start="";
                                    $Fri_END="";
                                    $Sat_START="";
                                    $Sat_END="";
                                    $Sun_START="";
                                    $Sun_END="";

                                    $hasWeekend="N";
                                    $hasEvening="N";

                                    $bussinesOurs = $addr->getBusinessHours();
                                    if($bussinesOurs!=""){
                                        $bussinesOurs = substr($bussinesOurs, 1);
                                        $bussinesOurs = substr($bussinesOurs, 0, -1);

                                        $bussinesOurs = str_replace('{', '', $bussinesOurs);
                                        $bussinesOurs = str_replace('}', '', $bussinesOurs);
                                        $bussinesOurs = str_replace('"', '', $bussinesOurs);

                                        $daysHours = explode(",", $bussinesOurs);

                                        //sunday
                                        $dayActive = explode(":", $daysHours[18])[1];
                                        $startHour = substr($daysHours[19], 9);
                                        $tillHour = substr($daysHours[20], 9);

                                        if ($dayActive == "true") {
                                            $Sun_START=strtoupper($startHour);
                                            $Sun_END=strtoupper($tillHour);

                                            $hasWeekend="Y";
                                            $hourTill=explode(':',$tillHour);
                                            $hourTill=intval($hourTill[0]);
                                            if($hourTill>6){
                                                $hasEvening="Y";
                                            }
                                        }

                                        //monday
                                        $dayActive = explode(":", $daysHours[0])[1];
                                        $startHour = substr($daysHours[1], 9);
                                        $tillHour = substr($daysHours[2], 9);
                                        if ($dayActive == "true") {
                                            $Mon_START=strtoupper($startHour);
                                            $Mon_END= strtoupper($tillHour);
                                            $hourTill=explode(':',$tillHour);
                                            $hourTill=intval($hourTill[0]);
                                            if($hourTill>6){
                                                $hasEvening="Y";
                                            }
                                        }

                                        //tuesday
                                        $dayActive = explode(":", $daysHours[3])[1];
                                        $startHour = substr($daysHours[4], 9);
                                        $tillHour = substr($daysHours[5], 9);
                                        if ($dayActive == "true") {
                                            $Tues_START=strtoupper($startHour);
                                            $Tues_END=strtoupper($tillHour);
                                            $hourTill=explode(':',$tillHour);
                                            $hourTill=intval($hourTill[0]);
                                            if($hourTill>6){
                                                $hasEvening="Y";
                                            }
                                        }

                                        //Wednesday
                                        $dayActive = explode(":", $daysHours[6])[1];
                                        $startHour = substr($daysHours[7], 9);
                                        $tillHour = substr($daysHours[8], 9);
                                        if ($dayActive == "true") {
                                            $Wed_START=strtoupper($startHour);
                                            $Wed_END=strtoupper($tillHour);
                                            $hourTill=explode(':',$tillHour);
                                            $hourTill=intval($hourTill[0]);
                                            if($hourTill>6){
                                                $hasEvening="Y";
                                            }
                                        }

                                        //Thurs
                                        $dayActive = explode(":", $daysHours[9])[1];
                                        $startHour = substr($daysHours[10], 9);
                                        $tillHour = substr($daysHours[11], 9);
                                        if ($dayActive == "true") {
                                            $Thurs_START=strtoupper($startHour);
                                            $Thurs_END=strtoupper($tillHour);
                                            $hourTill=explode(':',$tillHour);
                                            $hourTill=intval($hourTill[0]);
                                            if($hourTill>6){
                                                $hasEvening="Y";
                                            }
                                        }

                                        //Frid
                                        $dayActive = explode(":", $daysHours[12])[1];
                                        $startHour = substr($daysHours[13], 9);
                                        $tillHour = substr($daysHours[14], 9);
                                        if ($dayActive == "true") {
                                            $Fri_Start=strtoupper($startHour);
                                            $Fri_END=strtoupper($tillHour);
                                            $hourTill=explode(':',$tillHour);
                                            $hourTill=intval($hourTill[0]);
                                            if($hourTill>6){
                                                $hasEvening="Y";
                                            }
                                        }

                                        //sat
                                        $dayActive = explode(":", $daysHours[15])[1];
                                        $startHour = substr($daysHours[16], 9);
                                        $tillHour = substr($daysHours[17], 9);
                                        if ($dayActive == "true") {
                                            $Sat_START=strtoupper($startHour);
                                            $Sat_END=strtoupper($tillHour);

                                            $hasWeekend="Y";
                                            $hourTill=explode(':',$tillHour);
                                            $hourTill=intval($hourTill[0]);
                                            if($hourTill>6){
                                                $hasEvening="Y";
                                            }
                                        }
                                    }

                                    $payto_street = "";
                                    $payto_city = "";
                                    $payto_usState = "";
                                    $payto_zipCode = "";
                                    $payto_phoneNumber = "";

                                    //get billing Address
                                    $orgAddress = $this->em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $organization->getId()));

                                    if (count($orgAddress) == 1) {
                                        foreach ($orgAddress as $addrB) {
                                            $payto_street=$addrB->getStreet();
                                            $payto_city=$addrB->getCity();
                                            $payto_usState=$addrB->getUsState();
                                            $payto_zipCode=$addrB->getZipCode();
                                            $phone_number_B = $addrB->getPhoneNumber();
                                            $phone_number_B = str_replace('-', "", $phone_number_B);
                                            $payto_phoneNumber=$phone_number_B;
                                        }
                                    }

                                    if (count($orgAddress) > 1) {
                                        foreach ($orgAddress as $addrB) {
                                            if ($addrB->getBillingAddr() == true) {
                                                $hasBilling = true;
                                                $street = $addrB->getStreet();
                                                $city = $addrB->getCity();
                                                $usState = $addrB->getUsState();
                                                $zipCode = $addrB->getZipCode();
                                                $phone_number_B = $addrB->getPhoneNumber();
                                                $phone_number_B = str_replace('-', "", $phone_number_B);
                                            }
                                        }

                                        if ($hasBilling == true) {
                                            $payto_street=$street;
                                            $payto_city=$city;
                                            $payto_usState=$usState;
                                            $payto_zipCode=$zipCode;
                                            $payto_phoneNumber=$phone_number_B;
                                        }else{

                                            foreach ($orgAddress as $addrB) {
                                                $payto_street=$addrB->getStreet();
                                                $payto_city=$addrB->getCity();
                                                $payto_usState=$addrB->getUsState();
                                                $payto_zipCode=$addrB->getZipCode();
                                                $phone_number_B = $addrB->getPhoneNumber();
                                                $phone_number_B = str_replace('-', "", $phone_number_B);
                                                $payto_phoneNumber=$phone_number_B;
                                                break;
                                            }
                                        }
                                    }

                                    $minAge=strval($addr->getOfficeAgeLimit());
                                    $maxAge=strval($addr->getOfficeAgeLimitMax());

                                    if(strlen($minAge)==1){
                                        $minAge='0'.$minAge;
                                    }
                                    $minAge=$minAge."Y";

                                    if(strlen($maxAge)==1){
                                        $maxAge='0'.$maxAge;
                                    }
                                    $maxAge=$maxAge."Y";

                                    if($minAge=='99Y'){
                                        $minAge='00Y';
                                    }

                                    if($maxAge=='00Y'){
                                        $maxAge='99Y';
                                    }

                                    $record=[
                                        'col_a'=>$key,
                                        'col_c'=>$site_index,
                                        'col_e'=>$exempt,
                                        'col_f'=>$last,
                                        'col_g'=>$name,
                                        'col_h'=>$isDr,
                                        'col_i'=>'Y',
                                        'col_j'=>$eClaimProviderType,
                                        'col_l'=>$specialty_name,
                                        'col_m'=>"",
                                        'col_p'=>"",
                                        'col_s'=>"",
                                        'col_v'=>"",
                                        'col_z'=>$npi,
                                        'col_aa'=>$medicare,
                                        'col_ab'=>$license,
                                        'col_ac'=>'',
                                        'col_ad'=>$genderCode,
                                        'col_ae'=>$language1,
                                        'col_af'=>$language2,
                                        'col_ag'=>$language3,
                                        'col_ah'=>$language4,
                                        'col_ai'=>$language5,
                                        'col_aj'=>$language6,
                                        'col_ak'=>$language7,
                                        'col_al'=>$language8,
                                        'col_bo'=>$site_index,
                                        'col_bp'=>'20210101',
                                        'col_bq'=>$Prac_IRSType,
                                        'col_br'=>$Prac_IRS_No,
                                        'col_bs'=>$addr->getSuiteNumber(),
                                        'col_bt'=>$addr->getStreet(),
                                        'col_bu'=>$addr->getCity(),
                                        'col_bv'=>$addr->getUsState(),
                                        'col_bw'=>$addr->getZipCode(),
                                        'col_by'=>$phone_number,
                                        'col_cb'=>$ada,
                                        'col_cc'=>'Y',
                                        'col_cd'=>'B',
                                        'col_cf'=>'I',
                                        'col_cg'=>$taxonomyCode1,
                                        'col_ch'=>$taxonomyCode2,
                                        'col_ci'=>$taxonomyCode3,
                                        'col_co'=>'N',
                                        'col_cp'=>$hasEvening,
                                        'col_cq'=>$hasWeekend,
                                        'col_cr'=>$Mon_START,
                                        'col_cs'=>$Mon_END,
                                        'col_ct'=>$Tues_START,
                                        'col_cu'=>$Tues_END,
                                        'col_cv'=>$Wed_START,
                                        'col_cw'=>$Wed_END,
                                        'col_cx'=>$Thurs_START,
                                        'col_cy'=>$Thurs_END,
                                        'col_cz'=>$Fri_Start,
                                        'col_da'=>$Fri_END,
                                        'col_db'=>$Sat_START,
                                        'col_dc'=>$Sat_END,
                                        'col_dd'=>$Sun_START,
                                        'col_de'=>$Sun_END,
                                        'col_do'=>$payto_street,
                                        'col_dp'=>$payto_city,
                                        'col_dq'=>$payto_usState,
                                        'col_dr'=>$payto_zipCode,
                                        'col_dt'=>$payto_phoneNumber,
                                        'col_du'=>'MCD',
                                        'col_dv'=>$reimbursement,
                                        'col_dy'=>$minAge,
                                        'col_dz'=>$maxAge,
                                    ];

                                    $bh_pass=true;

                                    if($bussinesOurs=="" and $exempt!="DE"){
                                      $bh_pass=false;
                                    }

                                    if($bh_pass==true and $specialty_name!=""){
                                        $fccRosterData=new FCCRosterDataMedicare();
                                        $fccRosterData->setNpi($npi);
                                        $fccRosterData->setKeyLog($key);
                                        $fccRosterData->setData($record);
                                        $fccRosterData->setStatus('active');
                                        $this->em->persist($fccRosterData);

                                        $io->writeln($cont . " " .$key);
                                        $cont++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $this->em->flush();

        $io->writeln("============ Organizations =============");
        //Organization
        $cont_org=1;
        foreach ($organizations_result as $org_id){

            $organization=$this->em->getRepository('App\Entity\Organization')->find($org_id);
            $npi=$organization->getGroupNpi();

            $address=$this->em->getRepository('App\Entity\BillingAddress')->findBy(array('organization'=>$organization->getId()));
            $org_medicare="";
            $key_cont=1;

            $org_specialty_name="";
            $taxonomyCode1="";
            $taxonomyCode2="";
            $taxonomyCode3="";

            if($address!=null) {

                foreach ($address as $addr) {
                  $medicare=$addr->getGroupMedicare();
                    if ($medicare != "" and $medicare != "-" and $medicare != "." and $medicare != "-N/A" and $medicare != "0" and $medicare != "00000" and $medicare != "000000" and $medicare != "0000000"
                        and $medicare != "00000000" and $medicare != "000000000" and $medicare != "000000000000" and $medicare != "n/a" and $medicare != "55525 - opted out" and
                        $medicare != "Interal Medicine" and $medicare != "Internal Medicine" and $medicare != "N/A" and $medicare != "NA" and $medicare != "None" and $medicare != "Pending" and
                        $medicare != "Psychiatrist" and $medicare != "Psychologist" and $medicare != "in procress" and $medicare != "in progress" and $medicare != "submitting application" and
                        $medicare != "In-progress" and $medicare != "Pending" and $medicare != "in progress" and $medicare != "In Progress" and $medicare != "N/A" and $medicare != "PENDING" and $medicare != "Pending" and
                        $medicare != "pending" and $medicare != "blank" and $medicare != "none" and $medicare != "NONE" and $medicare != "Psychiatrist and Medical Director" and $medicare != "ARNP - Medical" and
                        $medicare != "ARNP - Psychiatric" and $medicare != "ARNP - Medical" and $medicare != "ARNP - Psychiatric") {
                        $org_medicare = $medicare;
                    }
                }

                $pdataMedicare=$this->em->getRepository('App:PDataMedicare')->findOneBy(array('npi_number'=>$npi));
                if($pdataMedicare){
                    $json=$pdataMedicare->getData();
                    $data=json_decode($json,true);

                    if(count($data)>1 and $data['result_count']>0){
                        $taxonomies=$data['results'][0]['taxonomies'];

                        $cont_tax=1;
                        foreach ($taxonomies as $taxonomy){
                            if($taxonomy['primary']==true){
                                $org_specialty_name=$taxonomy['desc'];
                            }
                            if($cont_tax==1){
                                $taxonomyCode1=$taxonomy['code'];
                            }
                            if($cont_tax==2){
                                $taxonomyCode2=$taxonomy['code'];
                            }

                            if($cont_tax==3){
                                $taxonomyCode3=$taxonomy['code'];
                            }

                            $cont_tax++;
                        }
                    }
                }

                $reimbursement="85%";
                $rates=$this->em->getRepository('App:OrganizationRates')->findBy(array('organization'=>$organization->getId()));
                if($rates){
                    foreach ($rates as $rate){
                        if($rate->getRate()->getId()==1){

                            $new_eimbursement=$rate->getPercent();
                            if($new_eimbursement!=""){

                                $reimbursement=$new_eimbursement."%";
                            }
                        }
                    }
                }

                foreach ($address as $addr) {
                    $street_check=$addr->getStreet();
                    $state_check=$addr->getUsState();
                    $pass=1;

                    if(stristr($street_check, 'BOX')==true or stristr($street_check, 'Box')==true or stristr($street_check, 'box')==true or $state_check!="FL" or $addr->getDisabled()==1) {
                        $pass=0;
                    }
                    if($pass==1){

                        $site_index = "";
                        $ind_or_group = "";

                        if ($key_cont < 10) {
                            $key = 'O'.$organization->getGroupNpi() . "-" . '00' . $key_cont;
                            $site_index = "00" . $key_cont;
                        } else {
                            $key = 'O'.$organization->getGroupNpi() . "-" . '0' . $key_cont;
                            $site_index = "0" . $key_cont;
                        }

                        $type3 = false;
                        $orgClasifications = $organization->getOrgSpecialtiesArray();

                        if ($orgClasifications != null) {
                            foreach ($orgClasifications as $orgC) {
                                if ($orgC->getName() == "State Mental Hospital") {
                                    $type3 = true;
                                }
                            }
                        }

                        $exempt="";
                        if($addr->getSeePatientsAddr()==false){
                            $exempt="DE";
                        }

                        $ahca_id="";

                        if($type3==true){
                            $ahca_id="";
                            $address2=$this->em->getRepository('App:BillingAddress')->findBy(array('organization'=>$organization->getId()));
                            if($address2!=null){
                                foreach ($address2 as $addr2){
                                    $ahca_id=$addr2->getAhcaNumber();
                                    if($ahca_id!=""){
                                        if(strlen($ahca_id)==6){
                                            $ahca_id="00".$ahca_id;
                                        }
                                        break;
                                    }
                                }
                            }
                        }

                        $eClaimProviderType="GR";
                        if($specialty1=="20001" or $specialty1=="20101" or $specialty1=="90101" or $specialty1=="30001" or $specialty1=="30101"){
                            $eClaimProviderType="HS";
                        }

                        if($specialty2=="20001" or $specialty2=="20101" or $specialty2=="90101" or $specialty2=="30001" or $specialty2=="30101"){
                            $eClaimProviderType="HS";
                        }

                        if($specialty3=="20001" or $specialty3=="20101" or $specialty3=="90101" or $specialty3=="30001" or $specialty3=="30101"){
                            $eClaimProviderType="HS";
                        }

                        $providers_te=$this->em->getRepository('App:Provider')->findBy(array('organization'=>$organization->getId()));
                        $languagesId=[];
                        if($providers_te!=null){
                            foreach ($providers_te as $provider){
                                $languages=$provider->getLanguages();
                                if($languages!=null){
                                    foreach ($languages as $lng){
                                        if(!in_array($lng->getId(),$languagesId)){
                                            $languagesId[]=$lng->getId();
                                        }
                                    }
                                }
                            }
                        }

                        $language1 = "";
                        $language2 = "";
                        $language3 = "";
                        $language4 = "";
                        $language5 = "";
                        $language6 = "";
                        $language7 = "";
                        $language8 = "";
                        $languagesArray = array();
                        if ($languagesId != null) {
                            foreach ($languagesId as $id) {
                                $language = $this->em->getRepository('App\Entity\Languages')->find($id);
                                if ($language != null) {
                                    $languagesArray[] = $language;
                                }
                            }
                        }

                        if ($languagesArray != null) {
                            if (count($languagesArray) > 0) {
                                $language1 = $languagesArray[0]->getLangCd();
                            }
                            if (count($languagesArray) > 1) {
                                $language2 = $languagesArray[1]->getLangCd();
                            }
                            if (count($languagesArray) > 2) {
                                $language3 = $languagesArray[2]->getLangCd();
                            }
                            if (count($languagesArray) > 3) {
                                $language4 = $languagesArray[3]->getLangCd();
                            }
                            if (count($languagesArray) > 4) {
                                $language5 = $languagesArray[4]->getLangCd();
                            }
                            if (count($languagesArray) > 5) {
                                $language6 = $languagesArray[5]->getLangCd();
                            }
                            if (count($languagesArray) > 6) {
                                $language7 = $languagesArray[6]->getLangCd();
                            }
                            if (count($languagesArray) > 7) {
                                $language8 = $languagesArray[7]->getLangCd();
                            }
                        }

                        if($language1==""){
                            $language1="ENG";
                        }

                        $ada = "N";
                        if ($addr->getWheelchair() == true) {
                            $ada = "Y";
                        }

                        $phone_number = $addr->getPhoneNumber();
                        $phone_number = str_replace('-', "", $phone_number);

                        if($phone_number)

                        $tin = $organization->getTinNumber();
                        $Prac_IRSType="";
                        $Prac_IRS_No="";

                        if ($tin != "") {
                            $Prac_IRSType='T';
                            $Prac_IRS_No= $tin;
                        }else{
                            $provider=$this->em->getRepository('App\Entity\Provider')->findOneBy(array('npi_number'=>$organization->getGroupNpi()));
                            if($provider!=null){
                                $tin=$this->encoder->decryptthis($provider->getSocial());
                                $Prac_IRSType='S';
                                $Prac_IRS_No= $tin;
                            }
                        }

                        $Prac_IRS_No=str_replace("-","",$Prac_IRS_No);

                        $Mon_START="";
                        $Mon_END="";
                        $Tues_START="";
                        $Tues_END="";
                        $Wed_START="";
                        $Wed_END="";
                        $Thurs_START="";
                        $Thurs_END="";
                        $Fri_Start="";
                        $Fri_END="";
                        $Sat_START="";
                        $Sat_END="";
                        $Sun_START="";
                        $Sun_END="";

                        $hasWeekend="N";
                        $hasEvening="N";

                        $bussinesOurs = $addr->getBusinessHours();

                        if($bussinesOurs!=""){
                            $bussinesOurs = substr($bussinesOurs, 1);
                            $bussinesOurs = substr($bussinesOurs, 0, -1);

                            $bussinesOurs = str_replace('{', '', $bussinesOurs);
                            $bussinesOurs = str_replace('}', '', $bussinesOurs);
                            $bussinesOurs = str_replace('"', '', $bussinesOurs);

                            $daysHours = explode(",", $bussinesOurs);

                            //sunday
                            $dayActive = explode(":", $daysHours[18])[1];
                            $startHour = substr($daysHours[19], 9);
                            $tillHour = substr($daysHours[20], 9);

                            if ($dayActive == "true") {
                                $Sun_START=strtoupper($startHour);
                                $Sun_END=strtoupper($tillHour);

                                $hasWeekend="Y";
                                $hourTill=explode(':',$tillHour);
                                $hourTill=intval($hourTill[0]);
                                if($hourTill>6){
                                    $hasEvening="Y";
                                }
                            }

                            //monday
                            $dayActive = explode(":", $daysHours[0])[1];
                            $startHour = substr($daysHours[1], 9);
                            $tillHour = substr($daysHours[2], 9);
                            if ($dayActive == "true") {
                                $Mon_START=strtoupper($startHour);
                                $Mon_END= strtoupper($tillHour);
                                $hourTill=explode(':',$tillHour);
                                $hourTill=intval($hourTill[0]);
                                if($hourTill>6){
                                    $hasEvening="Y";
                                }
                            }

                            //tuesday
                            $dayActive = explode(":", $daysHours[3])[1];
                            $startHour = substr($daysHours[4], 9);
                            $tillHour = substr($daysHours[5], 9);
                            if ($dayActive == "true") {
                                $Tues_START=strtoupper($startHour);
                                $Tues_END=strtoupper($tillHour);
                                $hourTill=explode(':',$tillHour);
                                $hourTill=intval($hourTill[0]);
                                if($hourTill>6){
                                    $hasEvening="Y";
                                }
                            }

                            //Wednesday
                            $dayActive = explode(":", $daysHours[6])[1];
                            $startHour = substr($daysHours[7], 9);
                            $tillHour = substr($daysHours[8], 9);
                            if ($dayActive == "true") {
                                $Wed_START=strtoupper($startHour);
                                $Wed_END=strtoupper($tillHour);
                                $hourTill=explode(':',$tillHour);
                                $hourTill=intval($hourTill[0]);
                                if($hourTill>6){
                                    $hasEvening="Y";
                                }
                            }

                            //Thurs
                            $dayActive = explode(":", $daysHours[9])[1];
                            $startHour = substr($daysHours[10], 9);
                            $tillHour = substr($daysHours[11], 9);
                            if ($dayActive == "true") {
                                $Thurs_START=strtoupper($startHour);
                                $Thurs_END=strtoupper($tillHour);
                                $hourTill=explode(':',$tillHour);
                                $hourTill=intval($hourTill[0]);
                                if($hourTill>6){
                                    $hasEvening="Y";
                                }
                            }

                            //Frid
                            $dayActive = explode(":", $daysHours[12])[1];
                            $startHour = substr($daysHours[13], 9);
                            $tillHour = substr($daysHours[14], 9);
                            if ($dayActive == "true") {
                                $Fri_Start=strtoupper($startHour);
                                $Fri_END=strtoupper($tillHour);
                                $hourTill=explode(':',$tillHour);
                                $hourTill=intval($hourTill[0]);
                                if($hourTill>6){
                                    $hasEvening="Y";
                                }
                            }

                            //sat
                            $dayActive = explode(":", $daysHours[15])[1];
                            $startHour = substr($daysHours[16], 9);
                            $tillHour = substr($daysHours[17], 9);
                            if ($dayActive == "true") {
                                $Sat_START=strtoupper($startHour);
                                $Sat_END=strtoupper($tillHour);

                                $hasWeekend="Y";
                                $hourTill=explode(':',$tillHour);
                                $hourTill=intval($hourTill[0]);
                                if($hourTill>6){
                                    $hasEvening="Y";
                                }
                            }
                        }

                        $orgAddress = $this->em->getRepository('App\Entity\BillingAddress')->findBy(array('organization' => $organization->getId()));

                        $payto_street = "";
                        $payto_city = "";
                        $payto_UsState = "";
                        $payto_zipCode = "";
                        $payto_phoneNumber = "";
                        if (count($orgAddress) == 1) {
                            foreach ($orgAddress as $addrB) {
                                $payto_street= $addrB->getStreet();
                                $payto_city= $addrB->getCity();
                                $payto_UsState= $addrB->getUsState();
                                $payto_zipCode= $addrB->getZipCode();
                                $phone_number_B = $addrB->getPhoneNumber();
                                $phone_number_B = str_replace('-', "", $phone_number_B);
                                $payto_phoneNumber= $phone_number_B;
                            }
                        }

                        if (count($orgAddress) > 1) {
                            foreach ($orgAddress as $addrB) {
                                if ($addrB->getBillingAddr() == true) {
                                    $payto_street= $addrB->getStreet();
                                    $payto_city= $addrB->getCity();
                                    $payto_UsState= $addrB->getUsState();
                                    $payto_zipCode= $addrB->getZipCode();
                                    $phone_number_B = $addrB->getPhoneNumber();
                                    $phone_number_B = str_replace('-', "", $phone_number_B);
                                    $payto_phoneNumber= $phone_number_B;
                                }
                            }

                            if($payto_street==""){
                                foreach ($orgAddress as $addrB) {
                                    $payto_street= $addrB->getStreet();
                                    $payto_city= $addrB->getCity();
                                    $payto_UsState= $addrB->getUsState();
                                    $payto_zipCode= $addrB->getZipCode();
                                    $phone_number_B = $addrB->getPhoneNumber();
                                    $phone_number_B = str_replace('-', "", $phone_number_B);
                                    $payto_phoneNumber= $phone_number_B;
                                    break;
                                }
                            }
                        }


                        $minAge=strval($addr->getOfficeAgeLimit());
                        $maxAge=strval($addr->getOfficeAgeLimitMax());

                        if(strlen($minAge)==1){
                            $minAge='0'.$minAge;
                        }
                        $minAge=$minAge."Y";

                        if(strlen($maxAge)==1){
                            $maxAge='0'.$maxAge;
                        }
                        $maxAge=$maxAge."Y";

                        if($minAge=='99Y'){
                            $minAge='00Y';
                        }

                        if($maxAge=='00Y'){
                           $maxAge='99Y';
                        }

                        $record=[
                            'col_a'=>$key,
                            'col_c'=>$site_index,
                            'col_e'=>$exempt,
                            'col_f'=>$organization->getName(),
                            'col_g'=>'',
                            'col_h'=>'',
                            'col_i'=>'N',
                            'col_j'=>$eClaimProviderType,
                            'col_k'=>'',
                            'col_l'=>$org_specialty_name,
                            'col_m'=>"",
                            'col_p'=>"",
                            'col_s'=>"",
                            'col_v'=>"",
                            'col_z'=>$npi,
                            'col_aa'=>$org_medicare,
                            'col_ab'=>'',
                            'col_ac'=>'',
                            'col_ad'=>'',
                            'col_ae'=>$language1,
                            'col_af'=>$language2,
                            'col_ag'=>$language3,
                            'col_ah'=>$language4,
                            'col_ai'=>$language5,
                            'col_aj'=>$language6,
                            'col_ak'=>$language7,
                            'col_al'=>$language8,
                            'col_bo'=>$site_index,
                            'col_bp'=>'',
                            'col_bq'=>$Prac_IRSType,
                            'col_br'=>$Prac_IRS_No,
                            'col_bs'=>$addr->getSuiteNumber(),
                            'col_bt'=>$addr->getStreet(),
                            'col_bu'=>$addr->getCity(),
                            'col_bv'=>$addr->getUsState(),
                            'col_bw'=>$addr->getZipCode(),
                            'col_by'=>$phone_number,
                            'col_cb'=>$ada,
                            'col_cc'=>'Y',
                            'col_cd'=>'B',
                            'col_cf'=>'O',
                            'col_cg'=>$taxonomyCode1,
                            'col_ch'=>$taxonomyCode2,
                            'col_ci'=>$taxonomyCode3,
                            'col_co'=>'N',
                            'col_cp'=>$hasEvening,
                            'col_cq'=>$hasWeekend,
                            'col_cr'=>$Mon_START,
                            'col_cs'=>$Mon_END,
                            'col_ct'=>$Tues_START,
                            'col_cu'=>$Tues_END,
                            'col_cv'=>$Wed_START,
                            'col_cw'=>$Wed_END,
                            'col_cx'=>$Thurs_START,
                            'col_cy'=>$Thurs_END,
                            'col_cz'=>$Fri_Start,
                            'col_da'=>$Fri_END,
                            'col_db'=>$Sat_START,
                            'col_dc'=>$Sat_END,
                            'col_dd'=>$Sun_START,
                            'col_de'=>$Sun_END,
                            'col_do'=>$payto_street,
                            'col_dp'=>$payto_city,
                            'col_dq'=>$payto_UsState,
                            'col_dr'=>$payto_zipCode,
                            'col_dt'=>$payto_phoneNumber,
                            'col_du'=>'MCD',
                            'col_dv'=>$reimbursement,
                            'col_dy'=>$minAge,
                            'col_dz'=>$maxAge,
                        ];

                        if($org_specialty_name!=""){
                            $fccRosterData=new FCCRosterDataMedicare();
                            $fccRosterData->setNpi($npi);
                            $fccRosterData->setKeyLog($key);
                            $fccRosterData->setData($record);
                            $fccRosterData->setStatus('active');

                            $this->em->persist($fccRosterData);

                            $io->writeln($cont." ".$organization->getName(). " ". $addr->getStreet());
                            $cont++;
                            $key_cont++;
                        }
                    }
                }
            }

            $io->writeln($cont_org . " " . $organization->getName());
            $cont_org++;
        }

        $this->em->flush();

        $io->success('All process finished');

        return Command::SUCCESS;
    }
}
