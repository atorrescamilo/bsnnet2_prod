<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class VividaCommand extends Command
{
    protected static $defaultName = 'vivida:process';
    private $em;
    private $conn;

    public function __construct( EntityManagerInterface $em, ParameterBagInterface $params)
    {
        parent::__construct();
        $this->em = $em;
        $this->conn = $this->em->getConnection();
        $this->params = $params;
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            ->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);

        $records=$this->em->getRepository('App\Entity\Vivida')->findAll();

        $cont=1;

        foreach ($records as $item){
                $current_practice_name=$item->getPracticeName();

                $network=$item->getNetwork();

                if($network==""){
                    $organizations=$this->em->getRepository('App\Entity\Organization')->findBy(array('disabled'=>0));

                    foreach ($organizations as $organization){
                        $org_name=$organization->getName();

                        if(strtolower($org_name)==strtolower($current_practice_name)){
                            $recordsPra=$this->em->getRepository('App\Entity\Vivida')->findBy(array('practice_name'=>$current_practice_name));

                            foreach ($recordsPra as $record){
                                $record->setNewStatus('BOTH');
                                $this->em->persist($record);

                                $io->writeln($current_practice_name);
                            }
                            break;
                        }
                    }
                }
            $cont++;
        }

        $this->em->flush();
/*
        foreach ($records as $item){
            if($item->getNetwork()=="BSN"){
                if($item->getNewStatus()=="" or $item->getNewStatus()==null){
                    $item->setNewStatus('BSN');
                    $this->em->persist($item);
                    $this->em->flush();

                    $io->writeln($item->getName()." BSN");
                }
            }
            if($item->getNetwork()=="BOTH"){
                $item->setNewStatus('BSN');
                $this->em->persist($item);
                $this->em->flush();

                $io->writeln($item->getName()." BOTH");
            }
        }*/

        $io->success('Process Finished');
    }
}
