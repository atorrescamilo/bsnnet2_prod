<?php

namespace App\Form;

use App\Entity\TaxonomyPM;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaxonomyPMType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('code',TextType::class, ['label'=>'Code: (*)','empty_data' => '', 'label_attr'=>['class'=>'col-form-label']])
            ->add('specialty1',TextType::class, ['label'=>'Specialty 1:','label_attr'=>['class'=>'col-form-label']])
            ->add('specialty2',TextType::class, ['label'=>'Specialty 2:','label_attr'=>['class'=>'col-form-label']])
            ->add('specialty3',TextType::class, ['label'=>'Specialty 3:','label_attr'=>['class'=>'col-form-label']])
            ->add('specialty4',TextType::class, ['label'=>'Specialty 4:','label_attr'=>['class'=>'col-form-label']])
            ->add('grouping',TextType::class, ['label'=>'Grouping:','label_attr'=>['class'=>'col-form-label']])
            ->add('classification',TextType::class, ['label'=>'Classification:','label_attr'=>['class'=>'col-form-label']])
            ->add('specialization',TextType::class, ['label'=>'Specialization:','label_attr'=>['class'=>'col-form-label']])
            ->add('definition',TextType::class, ['label'=>'Definition:','label_attr'=>['class'=>'col-form-label']])
            ->add('notes',TextareaType::class, ['label'=>'Notes: (*)','label_attr'=>['class'=>'col-form-label'],'attr'=>['rows'=>6]])
            ->add('action_type',HiddenType::class,['mapped' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TaxonomyPM::class,
        ]);
    }
}
