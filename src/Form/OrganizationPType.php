<?php

namespace App\Form;

use App\Entity\Accreditation;
use App\Entity\Organization;
use App\Entity\OrganizationTaxonomy;
use App\Entity\SpecialtyAreas;
use App\Entity\OrgSpecialty;
use App\Entity\TaxonomyCode;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrganizationPType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, ['label'=>'Organization Name: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('provider_type',ChoiceType::class , ['choices'=>$options['provider_type'] ,'label'=>'Organization Type: (*)','label_attr'=>['class'=>'col-form-label'],'attr'=>['class'=>'form-control select']])
            ->add('accreditations', EntityType::class , ['row_attr'=>['class'=>'sr-only', 'id'=>'row_accreditation'],'expanded'=>true,'multiple'=>true,'class' => Accreditation::class,
                'choice_label' => function($object){
                    return $object->getDisplayName();
                },
                'label'=>'Accreditation: (*)','label_attr'=>['class'=>'col-form-label pt-0 custom-top-space']])

            ->add('group_npi',TextType::class, ['label'=>'Group NPI: (*)','label_attr'=>['class'=>'col-form-label'],'attr'=>['maxlength'=>10]])
            ->add('group_npi2',TextType::class, ['label'=>'Group NPI2:','label_attr'=>['class'=>'col-form-label'],'attr'=>['maxlength'=>10]])
            ->add('billing_npi',TextType::class, ['label'=>'Billing NPI: (*)','label_attr'=>['class'=>'col-form-label'],'attr'=>['maxlength'=>10]])
            ->add('billing_npi2',TextType::class, ['label'=>'Billing NPI2:','label_attr'=>['class'=>'col-form-label'],'attr'=>['maxlength'=>10]])
            ->add('taxonomies', EntityType::class, ['choice_label' => function($object){
                return $object->getDisplayName();
            },'expanded'=>false,'multiple'=>true, 'label'=>'Taxonomies code:','attr'=>['class'=>'select-search'], 'label_attr'=>['class'=>'col-form-label pt-0 custom-top-space'],
                'class' => TaxonomyCode::class])
            ->add('tin_number', TextType::class, ['label'=>'TIN number: (*)','label_attr'=>['class'=>'col-form-label'],'attr'=>['maxlength'=>9]])
            ->add('legal_name_tin_owner', TextType::class, ['label'=>'Legal Name of TIN Owner:(*)','label_attr'=>['class'=>'col-form-label']])
            ->add('billing_phone', TextType::class, ['attr'=>['data-mask'=>'999-999-9999'], 'label'=>'Billing phone: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('phone', TextType::class, ['attr'=>['data-mask'=>'999-999-9999'], 'label'=>'Phone: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('faxno', TextType::class, ['attr'=>['data-mask'=>'999-999-9999'], 'label'=>'Faxno:','label_attr'=>['class'=>'col-form-label']])
            ->add('website', TextType::class, ['label'=>'Website:','label_attr'=>['class'=>'col-form-label']])
            ->add('american_sign_language',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'American sign language: (*)',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true
            ])
            ->add('name_of_ehr_emr', TextType::class, ['label'=>'Name of EHR EMR:','label_attr'=>['class'=>'col-form-label']])
            ->add('has_telemedicine',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Do you currently use a Telemedicine Service?',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true
            ])
            ->add('interested_in_telemedicine',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Are you interested in Telemedicine Services?',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'row_attr'=>['class'=>'sr-only', 'id'=>'cont_interested_in_telemedicine'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true
            ])
            ->add('telemedicine_company',
                TextType::class, ['label'=>'Telemedicine Company:',
                    'attr'=>['placeholder'=>'What is the name of the telehealth platform you use?'],
                    'row_attr'=>['class'=>'sr-only','id'=>'cont_telemedicine_company'],
                    'label_attr'=>['class'=>'col-form-label']])
            ->add('specialty_areas', HiddenType::class,['mapped' => false])
            ->add('validation_required',HiddenType::class,['mapped' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Organization::class,
            'provider_type'=>array()
        ]);
    }
}
