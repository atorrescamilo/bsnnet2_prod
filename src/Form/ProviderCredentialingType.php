<?php

namespace App\Form;

use App\Entity\CredentialingStatus;
use App\Entity\ProviderCredentialing;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProviderCredentialingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('credentialing_status', EntityType::class ,
                ['attr'=>['class'=>'form-control select'],'expanded'=>false,'multiple'=>false,'class' => CredentialingStatus::class,'choice_label' => function($object){
                    return $object->getDisplayName();
                },
                    'label'=>'Credentialing Status:','label_attr'=>['class'=>'col-form-label pt-0 custom-top-space']])
            ->add('npi_number')
            ->add('credentialingDate', TextType::class,['label'=>'Sent to CVO:','label_attr'=>['class'=>'col-form-label'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999']])
            ->add('credentialingEffectiveDate', TextType::class,['label'=>'Effective Date','label_attr'=>['class'=>'col-form-label'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999']])
            ->add('credentialingCompleteDate', TextType::class,['label'=>'Complete Date:','label_attr'=>['class'=>'col-form-label'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999']])
            ->add('sanctionDate', TextType::class,['label'=>'Sanction Date:','label_attr'=>['class'=>'col-form-label'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999']])
            ->add('credentialing_accepted', TextType::class,['label'=>'Accepted Date:','label_attr'=>['class'=>'col-form-label'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999']])
            ->add('credentialing_denied', TextType::class,['label'=>'Denied Date:','label_attr'=>['class'=>'col-form-label'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999']])
            ->add('credentialing_appeal', TextType::class,['label'=>'Appeal Date:','label_attr'=>['class'=>'col-form-label'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999']])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProviderCredentialing::class,
        ]);
    }
}
