<?php

namespace App\Form;

use App\Entity\AgesRange;
use App\Entity\BillingAddress;
use App\Entity\FacilityType;
use App\Entity\Provider;
use App\Entity\ServiceSettings;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AddressType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('street',TextType::class, ['label'=>'Street: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('suite_number',TextType::class, ['label'=>'Suite number: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('location_name',TextType::class, ['label'=>'Location name: ', 'help'=>'Location name if different','label_attr'=>['class'=>'col-form-label']])
            ->add('city', TextType::class, ['label'=>'City: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('county', TextType::class, ['label'=>'County: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('us_state', TextType::class, ['label'=>'US state: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('zipCode', TextType::class, ['attr'=>['maxlength'=>5],'label'=>'ZIP code: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('phone_number', TextType::class, ['attr'=>['data-mask'=>'999-999-9999'], 'label'=>'Phone number: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('see_patients_addr',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Do you see patients at this location?: (*)',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true
            ])
            ->add('service_settings', EntityType::class , ['expanded'=>true,'multiple'=>true,'class' => ServiceSettings::class,
                'choice_label' => function($service_setting){
                    return $service_setting->getDisplayName();
                },
                'label'=>'Service settings: ','label_attr'=>['class'=>'col-form-label pt-0 custom-top-space']])
            ->add('have_beds',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Have beds?',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true
            ])
            ->add('beds',IntegerType::class, ['attr'=> array(
                'min'  => 0,
                'max'  => 10000,
                'step' => 1
            ),'row_attr' => ['class' => 'row_beds_info sr-only'], 'label'=>'Beds: ','label_attr'=>['class'=>'col-form-label']])
            //->add('na_beds')
            ->add('location_npi',TextType::class, ['label'=>'Location NPI: ','label_attr'=>['class'=>'col-form-label']])
            ->add('location_npi2',TextType::class, ['label'=>'Location NPI2: ','label_attr'=>['class'=>'col-form-label']])
            ->add('group_medicaid',TextType::class, ['label'=>'Group medicaid: ','label_attr'=>['class'=>'col-form-label']])
            ->add('group_medicare',TextType::class, ['label'=>'Group medicare: ','label_attr'=>['class'=>'col-form-label']])
            ->add('taxonomy_codes', null , ['label'=>'Taxonomy codes: ','label_attr'=>['class'=>'col-form-label'],'attr'=>['class'=>'form-control select'],'help'=>'Select one or more age range'])
            ->add('age_ranges', EntityType::class , ['expanded'=>true,'multiple'=>true,'class' => AgesRange::class,
                'choice_label' => function($age_ranges){
                    return $age_ranges->getDisplayName();
                },
                'label'=>'Age ranges ','label_attr'=>['class'=>'col-form-label pt-0 custom-top-space']])
            ->add('wheelchair',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Wheelchair',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true
            ])
            ->add('tdd',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'TDD',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true
            ])
            ->add('tdd_number',TextType::class, ['row_attr' => ['class' => 'row_tdd_info'], 'label'=>'TDD number ','label_attr'=>['class'=>'col-form-label']])
            ->add('private_provider_transportation',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Private provider transportation',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true
            ])
            ->add('primary_addr',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Primary address',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true
            ])
            ->add('billing_addr',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Billing address',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true
            ])
            ->add('mailing_addr',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Mailing address',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true
            ])
            ->add('is_facility',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Is facility',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true
            ])
            ->add('facility_number',TextType::class, ['row_attr' => ['class' => 'row_facility_info sr-only'], 'label'=>'Facility number: ','label_attr'=>['class'=>'col-form-label']])
            ->add('facility_exemption_number',TextType::class, ['row_attr' => ['class' => 'row_facility_info sr-only'], 'label'=>'Facility exemption number: ','label_attr'=>['class'=>'col-form-label']])
            ->add('ahca_number',TextType::class, ['row_attr' => ['class' => 'row_facility_info sr-only'], 'label'=>'Ahca number ','label_attr'=>['class'=>'col-form-label']])
            ->add('facility_type', EntityType::class , ['row_attr' => ['class' => 'row_facility_info sr-only'], 'expanded'=>true,'multiple'=>true,'class' => FacilityType::class,
                'choice_label' => function($facility_type){
                    return $facility_type->getDisplayName();
                },
                'label'=>'Facility type ','label_attr'=>['class'=>'col-form-label pt-0 custom-top-space']])
            ->add('providers', EntityType::class, ['choice_label' => function($object){
                return $object->getDisplayName();
            },'expanded'=>true,'multiple'=>true, 'label'=>'Providers:', 'label_attr'=>['class'=>'col-form-label pt-0 custom-top-space'],
                'class'         => Provider::class,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('pro')
                        ->where('pro.organization = :org_id_')
                        ->setParameter('org_id_', $this->org_id)
                        ->orderBy('pro.id', 'ASC');
                }])
            ->add('providers_id',HiddenType::class,['mapped' => false])
            ->add('region', HiddenType::class)
            ->add('google_addr', HiddenType::class)
            ->add('business_hours', HiddenType::class)
            ->add('appointment', HiddenType::class)
            ->add('lat',HiddenType::class)
            ->add('lng',HiddenType::class)
            ->add('action_type', HiddenType::class,['mapped' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BillingAddress::class,
        ]);
    }
}
