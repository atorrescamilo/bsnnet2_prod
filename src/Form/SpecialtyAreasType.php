<?php

namespace App\Form;

use App\Entity\SpecialtyAreas;
use App\Entity\SpecialtyAreaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class SpecialtyAreasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, ['label'=>'Name: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('code', TextType::class, ['label'=>'Code:','label_attr'=>['class'=>'col-form-label']])
            ->add('specialty_area_type', EntityType::class ,
                ['expanded'=>false,'multiple'=>false,'class' => SpecialtyAreaType::class,'choice_label' => function($object){
                    return $object->getDisplayName();
                },
                    'attr'=>['class'=>'form-control select']  ,'label'=>'Specialty Area Type:',
                    'label_attr'=>['class'=>'col-form-label pt-0 custom-top-space'],'help'=>'Please select the state in which you are currently licensed to practice']
            )
            ->add('description',TextareaType::class, ['label'=>'Description:','label_attr'=>['class'=>'col-form-label'],'attr'=>['rows'=>6]])
            ->add('action_type',HiddenType::class,['mapped' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SpecialtyAreas::class,
        ]);
    }
}
