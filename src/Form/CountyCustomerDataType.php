<?php

namespace App\Form;

use App\Entity\PayerCustomerCounty;
use App\Entity\Payer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use App\Entity\CountyMatch;
use App\Entity\LineOfBusiness;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class CountyCustomerDataType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('county', EntityType::class , ['expanded'=>false,'multiple'=>false,'class' => CountyMatch::class,
            'choice_label' => function($county){
                return $county->getDisplayName();
            },
            'attr'=>['class'=>'form-control select'],  
            'label'=>'County  (*)','label_attr'=>['class'=>'col-form-label pt-0 custom-top-space']])

            ->add('lineofbusiness', EntityType::class , ['expanded'=>false,'multiple'=>false,'class' => LineOfBusiness::class,
            'choice_label' => function($line){
                return $line->getDisplayName();
            },
            'attr'=>['class'=>'form-control select'],  
            'label'=>'Line of Business (*)','label_attr'=>['class'=>'col-form-label pt-0 custom-top-space']])

            ->add('total',TextType::class, ['label'=>'Total (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('action_type',HiddenType::class,['mapped' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PayerCustomerCounty::class,
        ]);
    }
}
