<?php

namespace App\Form;

use App\Entity\ProspectTicket;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProspectTicketType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options){
        $builder
            ->add('ticket_date', TextType::class, ['label'=>'Tickect Date: (*)',
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999'],'label_attr'=>['class'=>'col-form-label']])
            ->add('description', TextareaType::class, ['label'=>'Description:','label_attr'=>['class'=>'col-form-label'],'attr'=>['rows'=>6]])
            ->add('action_type',HiddenType::class,['mapped' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver){
        $resolver->setDefaults([
            'data_class' => ProspectTicket::class,
        ]);
    }
}
