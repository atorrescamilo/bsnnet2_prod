<?php

namespace App\Form;

use App\Entity\CredentialingStatus;
use App\Entity\OrganizationCredentialing;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class FacilityCredentialingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('credentialing_status', EntityType::class ,
                ['expanded'=>false,'multiple'=>false,'class' => CredentialingStatus::class,'choice_label' => function($object){
                    return $object->getDisplayName();
                },
                    'label'=>'Credentialing Status: (*)','attr'=>['class'=>'form-control select'],'label_attr'=>['class'=>'col-form-label pt-0 custom-top-space']]
            )
            ->add('credentialing_date',TextType::class,['label'=>'Credentialing Submitted to Cvo:','label_attr'=>['class'=>'col-form-label'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999']])
            ->add('credentialing_complete_date',TextType::class,['label'=>'Credentialed complete date:','label_attr'=>['class'=>'col-form-label'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999']])
            ->add('credentialing_received_date',TextType::class,['label'=>'Credentialing App received date:','label_attr'=>['class'=>'col-form-label'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999']])
            ->add('credentialing_accepted_date',TextType::class,['label'=>'Credentialing Accepted date:','label_attr'=>['class'=>'col-form-label'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999']])
            ->add('credentialing_effective_date',TextType::class,['label'=>'Credentialing Effective date:','label_attr'=>['class'=>'col-form-label'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999']])
            ->add('credentialing_denied_date',TextType::class,['label'=>'Credentialing Denied date:','label_attr'=>['class'=>'col-form-label'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999']])
            ->add('sanction_date',TextType::class,['label'=>'Saction Date:','label_attr'=>['class'=>'col-form-label'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999']])
            ->add('action_type',HiddenType::class,['mapped' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrganizationCredentialing::class,
        ]);
    }
}
