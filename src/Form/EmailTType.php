<?php

namespace App\Form;

use App\Entity\ContactPBestChoice;
use App\Entity\EmailType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EmailTType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, ['label'=>'Name: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('subject',TextType::class, ['label'=>'Subject: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('cc',TextType::class, ['label'=>'CC:','label_attr'=>['class'=>'col-form-label']])
            ->add('bcc',TextType::class, ['label'=>'BCC:','label_attr'=>['class'=>'col-form-label']])
            ->add('best_choices', EntityType::class , ['expanded'=>true,'multiple'=>true,'class' => ContactPBestChoice::class,
                'choice_label' => function($obj){
                    return $obj->getDisplayName();
                },
                'label'=>'Best option for: (*)','label_attr'=>['class'=>'col-form-label pt-0 custom-top-space']])
            ->add('attachments', FileType::class, ['multiple'=>true,'label'=>'Attach Files:','label_attr'=>['class'=>'col-form-label'], 'mapped' => false] )
            ->add('description',TextareaType::class, ['label'=>'Description:','label_attr'=>['class'=>'col-form-label'],'attr'=>['rows'=>6]])
            ->add('action_type',HiddenType::class,['mapped' => false]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EmailType::class,
        ]);
    }
}
