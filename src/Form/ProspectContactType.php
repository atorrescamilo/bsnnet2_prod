<?php

namespace App\Form;

use App\Entity\ProspectContact;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProspectContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label'=>'Name: (*)','label_attr'=>['class'=>'col-form-label'], 'empty_data' => ''])
            ->add('phone', TextType::class, ['attr'=>['data-mask'=>'999-999-9999'],'label'=>'Phone: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('phone_ext',TextType::class, ['label'=>'Phone Ext:','label_attr'=>['class'=>'col-form-label']])
            ->add('mobile',TextType::class, ['attr'=>['data-mask'=>'999-999-9999'],'label'=>'Mobile:','label_attr'=>['class'=>'col-form-label']])
            ->add('fax',TextType::class, ['attr'=>['data-mask'=>'999-999-9999'],'label'=>'Fax:','label_attr'=>['class'=>'col-form-label']])
            ->add('email', TextType::class, ['label'=>'Email:','label_attr'=>['class'=>'col-form-label']])
            ->add('action_type',HiddenType::class,['mapped' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProspectContact::class,
        ]);
    }
}
