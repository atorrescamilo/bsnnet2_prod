<?php

namespace App\Form;

use App\Entity\OrganizationProspect;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OrganizationProspectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, ['label'=>'Name: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('provider_type')
            ->add('group_npi')
            ->add('group_npi2')
            ->add('phone')
            ->add('phone_ext')
            ->add('billing_phone')
            ->add('faxno')
            ->add('email')
            ->add('hospital_affiliations')
            ->add('fein')
            ->add('tin_number')
            ->add('name_of_ehr_emr')
            ->add('website')
            ->add('taxonomy_code')
            ->add('notes')
            ->add('standardRates')
            ->add('nonStandardRates')
            ->add('status')
            ->add('created_on')
            ->add('updated_on')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrganizationProspect::class,
        ]);
    }
}
