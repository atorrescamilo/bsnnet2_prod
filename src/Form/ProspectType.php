<?php

namespace App\Form;

use App\Entity\Prospect;
use App\Entity\ProspectStatus;
use App\Entity\ServiceSettings;
use App\Entity\TaxonomyCode;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\DomCrawler\Field\TextareaFormField;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class ProspectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('group_npi', TextType::class, ['label' => 'Group NPI:', 'label_attr' => ['class' => 'col-form-label'], 'attr' => ['maxlength' => 10]])
            ->add('name', TextType::class, ['label' => 'Organization Name:', 'label_attr' => ['class' => 'col-form-label'],'constraints' => [
                new NotBlank(),
                new Length(['min' => 3])
            ]])
            ->add('dba_name', TextType::class, ['label' => 'DBA Name:', 'label_attr' => ['class' => 'col-form-label']])
            ->add('email', EmailType::class, ['label' => 'Email:', 'label_attr' => ['class' => 'col-form-label']])
            ->add('phone', TextType::class, ['attr' => ['data-mask' => '999-999-9999'], 'label' => 'Phone:', 'label_attr' => ['class' => 'col-form-label']])
            ->add('phone_ext', TextType::class, ['label' => 'Phone Ext:', 'label_attr' => ['class' => 'col-form-label']])
            ->add('provider_type', ChoiceType::class, [
                'attr' => ['class' => 'form-check-inline'],
                'label' => 'Organization Type',
                'label_attr' => ['class' => 'col-form-label pt-0'],
                'choices' => [
                    'Individual' => 'Individual',
                    'Facility' => 'Facility',
                    'Group' => 'Group'
                ],
                'expanded' => false
            ])
            ->add('taxonomies', EntityType::class, ['choice_label' => function ($object) {
                return $object->getDisplayName();
            }, 'expanded' => false, 'multiple' => true, 'label' => 'Taxonomies code:', 'attr' => ['class' => 'select-search'], 'label_attr' => ['class' => 'col-form-label pt-0 custom-top-space'],
                'class' => TaxonomyCode::class])
            ->add('standard_rates', ChoiceType::class, [
                'attr' => ['class' => 'form-check-inline'],
                'label' => 'Standard Rates?: ',
                'label_attr' => ['class' => 'col-form-label pt-0'],
                'choices' => [
                    'Yes' => true,
                    'No' => false
                ],
                'expanded' => true
            ])
            ->add('notes', TextareaType::class, ['label' => 'Notes:', 'label_attr' => ['class' => 'col-form-label']])
            ->add('status', EntityType::class, ['expanded' => false, 'multiple' => false, 'class' => ProspectStatus::class,
                'choice_label' => function ($status) {
                    return $status->getDisplayName();
                },
                'label' => 'Status: ', 'label_attr' => ['class' => 'col-form-label pt-0 custom-top-space']])
            ->add('action_type', HiddenType::class, ['mapped' => false])
            ->add('address', HiddenType::class, ['mapped' => false]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Prospect::class,
        ]);
    }
}
