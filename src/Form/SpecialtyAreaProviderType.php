<?php

namespace App\Form;

use App\Entity\SpecialtyAreaProvider;
use App\Entity\SpecialtyAreaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class SpecialtyAreaProviderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, ['label'=>'Name (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('specialty_area_type', EntityType::class , ['expanded'=>false,'multiple'=>false,'class' => SpecialtyAreaType::class,
            'choice_label' => function($specialty_area){
                return $specialty_area->getDisplayName();
            },
            'attr'=>['class'=>'form-control select'],  
            'label'=>'Type','label_attr'=>['class'=>'col-form-label pt-0 custom-top-space']])
            ->add('description', TextareaType::class, ['label'=>'Description','label_attr'=>['class'=>'col-form-label'],'attr'=>['rows'=>6]])
            ->add('action_type',HiddenType::class,['mapped' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SpecialtyAreaProvider::class,
        ]);
    }
}
