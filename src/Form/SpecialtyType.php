<?php

namespace App\Form;

use App\Entity\Specialty;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SpecialtyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, ['label'=>'Name: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('code', TextType::class, ['label'=>'Code:','label_attr'=>['class'=>'col-form-label']])
            ->add('source',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Source:',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'AHCA'=>'AHCA',
                    'ABMS'=>'ABMS'
                ],
                'expanded'=>false
            ])
            ->add('action_type',HiddenType::class,['mapped' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Specialty::class,
        ]);
    }
}
