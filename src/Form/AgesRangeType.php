<?php

namespace App\Form;

use App\Entity\AgesRange;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AgesRangeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, ['label'=>'Name (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('min_age',NumberType::class, ['label'=>'Min Age (*)','label_attr'=>['class'=>'col-form-label'],'help'=>'Only Numbers are allowed'])
            ->add('max_age',NumberType::class, ['label'=>'MAx Age (*)','label_attr'=>['class'=>'col-form-label'],'help'=>'Only Numbers are allowed'])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AgesRange::class,
        ]);
    }
}
