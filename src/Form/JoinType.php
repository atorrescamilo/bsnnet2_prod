<?php

namespace App\Form;

use App\Entity\JOIN;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JoinType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('npi_number',TextType::class, ['label'=>'Group/ Organization / NPI (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('name',TextType::class, ['label'=>'Group/ Organization / Name (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('contact_name',TextType::class, ['label'=>'Credentialing Contact (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('contact_email',EmailType::class, ['label'=>'Credentialing Contact Email (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('fqhc')
            ->add('cmhc')
            ->add('rural_health_center')
            ->add('other_state')
            ->add('street')
            ->add('suite_number')
            ->add('city')
            ->add('county')
            ->add('phone_number')

            ->add('nppes_data')
            ->add('other_language')
            ->add('notes')
            ->add('counties')
            ->add('languages')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => JOIN::class,
        ]);
    }
}
