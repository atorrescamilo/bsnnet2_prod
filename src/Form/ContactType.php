<?php

namespace App\Form;

use App\Entity\ContactPBestChoice;
use App\Entity\OrganizationContact;
use App\Entity\Specialty;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name',TextType::class, ['label'=>'Full Name: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('title_str', TextType::class, ['label'=>'Title:','label_attr'=>['class'=>'col-form-label']])
            ->add('best_choices', EntityType::class ,
                ['expanded'=>true,'multiple'=>true,'class' => ContactPBestChoice::class,'choice_label' => function($object){
                    return $object->getDisplayName();
                },
                    'label'=>'Best option for: (*)','label_attr'=>['class'=>'col-form-label pt-0 custom-top-space']]
            )
            ->add('phone',TextType::class, ['label'=>'Phone: (*)','label_attr'=>['class'=>'col-form-label'], 'attr'=>['data-mask'=>'999-999-9999']])
            ->add('phone_ext', TextType::class, ['label'=>'Phone ext:','label_attr'=>['class'=>'col-form-label']])
            ->add('mobile', TextType::class, ['label'=>'Mobile:','label_attr'=>['class'=>'col-form-label'],'attr'=>['data-mask'=>'999-999-9999']])
            ->add('fax', TextType::class, ['label'=>'Fax:','label_attr'=>['class'=>'col-form-label'],'attr'=>['data-mask'=>'999-999-9999']])
            ->add('email', TextType::class, ['label'=>'Email: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('description', TextareaType::class , ['label'=>'Description:','label_attr'=>['class'=>'col-form-label'],'attr'=>['rows'=>6]])
            ->add('action_type',HiddenType::class,['mapped' => false])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrganizationContact::class,
        ]);
    }
}
