<?php

namespace App\Form;

use App\Entity\AgesRange;
use App\Entity\Degree;
use App\Entity\Languages;
use App\Entity\Provider;
use App\Entity\ProviderType;
use App\Entity\Specialty;
use App\Entity\TaxonomyCode;
use App\Entity\UsState;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class ProviderPType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('npi_number',TextType::class, ['label'=>'NPI Number: (*)','label_attr'=>['class'=>'col-form-label'],'attr'=>['maxlength'=>10]])
            ->add('first_name',TextType::class, ['label'=>'First Name: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('last_name',TextType::class, ['label'=>'Last Name: (*)','label_attr'=>['class'=>'col-form-label']])
            ->add('initial', TextType::class, ['label'=>'Initial:','label_attr'=>['class'=>'col-form-label']])
            ->add('gender',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Gender: (*)',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'Male'=>'Male',
                    'Female'=>'Female'
                ],
                'expanded'=>true
            ])
            ->add('email', EmailType::class, ['label'=>'Email:','label_attr'=>['class'=>'col-form-label']])
            ->add('date_of_birth',TextType::class,['label'=>'Birth date: (*)','help'=>'Please use this format mm/dd/yyyy','label_attr'=>['class'=>'col-form-label'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999']])
            ->add('social',TextType::class, ['label'=>'Social:','label_attr'=>['class'=>'col-form-label']])
            ->add('general_categories', null, ['label'=>'General categories: (*)','label_attr'=>['class'=>'col-form-label'],'attr'=>['class'=>'form-control select'],'help'=>'Select all that apply'])
            ->add('medicare',TextType::class, ['attr'=>['placeholder'=>'Enter Medicare Number or select Opted of Medicare'],'label'=>'Medicare:','label_attr'=>['class'=>'col-form-label'], 'help'=>'Enter Medicare Number or select Opted of Medicare'])
            ->add('option_out_medicare',CheckboxType::class,['label'=>'Opted out of Medicare','label_attr'=>['class'=>'col-form-label pt-0']])
            ->add('medicaid',TextType::class, ['label'=>'Medicaid:','label_attr'=>['class'=>'col-form-label']])
            ->add('none_medicaid',CheckboxType::class,['label'=>'None','label_attr'=>['class'=>'col-form-label pt-0']])
            ->add('caqh',TextType::class, ['label'=>'CAQH:','label_attr'=>['class'=>'col-form-label'],'help'=>'Please enter CAQH number or select N/A or Pending'])
            ->add('na_caqh', CheckboxType::class,['label'=>'N/A','label_attr'=>['class'=>'col-form-label pt-0']])
            ->add('pedding_caqh', CheckboxType::class,['label'=>'Pending','label_attr'=>['class'=>'col-form-label pt-0']])
            ->add('taxonomy_codes', EntityType::class ,
                ['expanded'=>false,'multiple'=>true,'class' => TaxonomyCode::class,'choice_label' => function($object){
                    return $object->getDisplayName();
                },
                    'choice_attr'=>['id'=>5],
                    'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->orderBy('u.id', 'ASC');
                    },
                    'attr'=>['class'=>'form-control select',],
                    'label'=>'Taxonomy codes:','label_attr'=>['class'=>'col-form-label pt-0 custom-top-space']]
            )
            ->add('state_lic',TextType::class, ['label'=>'State license:','label_attr'=>['class'=>'col-form-label'],'help'=>'Please enter your state license number - May include letters and numbers – Please include both if applicable. Do not add spaces.'])
            ->add('us_lic_state', EntityType::class ,
                ['expanded'=>false,'multiple'=>false,'class' => UsState::class,'choice_label' => function($object){
                    return $object->getDisplayName();
                }, 'query_builder' => function (EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                            ->orderBy('u.ord', 'ASC');
                },
                'attr'=>['class'=>'form-control select']  ,'label'=>'License state:',
                'label_attr'=>['class'=>'col-form-label pt-0 custom-top-space'],'help'=>'Please select the state in which you are currently licensed to practice']
            )
            ->add('lic_expires_date',TextType::class,['label'=>'Lic expires date:','help'=>'Please use this format mm/dd/yyyy','label_attr'=>['class'=>'col-form-label'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999'],'help'=>'When does your license expire?'])
            ->add('lic_issue_date',TextType::class,['label'=>'Lic issue date:','help'=>'Please use this format mm/dd/yyyy','label_attr'=>['class'=>'col-form-label'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999'],'help'=>'When was your licensed issued?'])
            ->add('board_certified',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Board certified:',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true
            ])
            ->add('peer_support',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Do you provide Peer Services?:',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true
            ])
            ->add('board_name',TextType::class, ['label'=>'Name of board:','label_attr'=>['class'=>'col-form-label sr-only', 'id'=>'label_board_name'],
                'attr'=>['placeholder'=>'Please enter the name of the Board that issued your certification',
                    'class'=>'sr-only']
            ])
            ->add('board_certified_date',TextType::class,['label'=>'Board certified date:','help_attr'=>['class'=>'sr-only']
                ,'help'=>'Please use this format mm/dd/yyyy','label_attr'=>['class'=>'col-form-label sr-only','id'=>'label_board_certified_date'],
                  'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999','class'=>'sr-only']])
            ->add('board_certified_specialty',TextType::class, ['label'=>'Board specialty:','label_attr'=>['class'=>'col-form-label sr-only','id'=>'label_board_certified_specialty'],
                'attr'=>['class'=>'sr-only']])
            ->add('board_certification_expires_date',TextType::class,['label'=>'Certification expires','help_attr'=>['class'=>'sr-only'],
                'help'=>'Please use this format mm/dd/yyyy','label_attr'=>['class'=>'col-form-label sr-only','id'=>'label_board_certification_expires_date'],
                'attr' => ['placeholder' => 'mm/dd/yyyy','data-mask'=>'99/99/9999','class'=>'sr-only']])
            ->add('is_hospital_affiliations',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Hospital Privileges?',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true
            ])
            ->add('hopital_privileges_type',ChoiceType::class , ['choices'=>$options['hospital_privileges_type'] , 'row_attr'=>['id'=>'label_hopital_privileges_type', 'class'=>'sr-only' ],
                'label'=>'Type of Hospital Privileges?','label_attr'=>['class'=>'col-form-label'],'attr'=>['class'=>'form-control select']])
            ->add('hopital_privileges', TextType::class, ['label'=>'Hospital name:','label_attr'=>['class'=>'col-form-label sr-only', 'id'=>'label_hopital_privileges'],
                'attr'=>['class'=>'sr-only']])
            ->add('hospital_acha_number',TextType::class, ['label'=>'Hospital AHCA number:','label_attr'=>['class'=>'col-form-label sr-only', 'id'=>'label_hospital_acha_number'],
                'attr'=>['class'=>'sr-only']])


            ->add('lineOfBusiness',null, ['label'=>'Line of Business: (*)','label_attr'=>['class'=>'col-form-label'],'attr'=>['class'=>'form-control select']])
            ->add('has_telemedicine',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Do you currently use a Telemedicine Service?',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true
            ])
            ->add('interested_in_telemedicine',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Are you interested in Telemedicine Services?',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'row_attr'=>['class'=>'sr-only', 'id'=>'cont_interested_in_telemedicine'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true
            ])
            ->add('telemedicine_company',
                TextType::class, ['label'=>'Telemedicine Company:',
                    'attr'=>['placeholder'=>'What is the name of the telehealth platform you use?'],
                    'row_attr'=>['class'=>'sr-only','id'=>'cont_telemedicine_company'],
                    'label_attr'=>['class'=>'col-form-label']])
            ->add('has_pcp',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Are you a PCP?',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true, 'multiple'=>false,
            ])
            ->add('accepting_new_patients',ChoiceType::class,[
                'attr'=>['class'=>'form-check-inline'],
                'label'=>'Are you accepting new patients:?',
                'label_attr'=>['class'=>'col-form-label pt-0'],
                'help'=>'Please note – your information will not be added to insurer directories if you select no.',
                'choices'=>[
                    'Yes'=>true,
                    'No'=>false
                ],
                'expanded'=>true, 'multiple'=>false,
                'error_bubbling' => true,
            ])
            ->add('gender_acceptance',ChoiceType::class , ['choices'=>$options['gender_acceptance'] ,
                'label'=>'Gender acceptance: (*)','label_attr'=>['class'=>'col-form-label'],'attr'=>['class'=>'form-control select']])
            ->add('age_ranges', EntityType::class , ['expanded'=>true,'multiple'=>true,'class' => AgesRange::class,
                'choice_label' => function($object){
                    return $object->getDisplayName();
                }, 
                'label'=>'Age ranges: (Please select all that apply.)','label_attr'=>['class'=>'col-form-label pt-0 custom-top-space']])
            ->add('languages', EntityType::class ,
                ['expanded'=>true,'multiple'=>true,'class' => Languages::class,'choice_label' => function($object){
                    return $object->getDisplayName();
                },
                    'label'=>'Languages: (*)','label_attr'=>['class'=>'col-form-label pt-0 custom-top-space']]
            )
            ->add('degree', EntityType::class ,
                ['expanded'=>true,'multiple'=>true,'class' => Degree::class,'choice_label' => function($object){
                    return $object->getDisplayName();
                },
                    'label'=>'Degrees and Certifications: (*)','label_attr'=>['class'=>'col-form-label pt-0 custom-top-space']]
            )
            ->add('provider_type', EntityType::class ,
                ['expanded'=>true,'multiple'=>true,'class' => ProviderType::class,'choice_label' => function($object){
                    return $object->getDisplayName();
                },
                    'label'=>'Provider Type: (*)','label_attr'=>['class'=>'col-form-label pt-0 custom-top-space']]
            )
            ->add('primary_specialties', EntityType::class ,
                ['expanded'=>true,'multiple'=>true,'class' => Specialty::class,'choice_label' => function($object){
                    return $object->getDisplayName();
                },
                    'label'=>'Primary Specialties: (*)','label_attr'=>['class'=>'col-form-label pt-0 custom-top-space']]
            )
            ->add('specialties_area', HiddenType::class,['mapped' => false])
            ->add('notes', TextareaType::class , ['label'=>'Notes:','label_attr'=>['class'=>'col-form-label'],'attr'=>['rows'=>6]])
            ->add('validation_required',HiddenType::class,['mapped' => false])
        ;

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Provider::class,
            'gender_acceptance'=>array(),
            'hospital_privileges_type'=>array(),
        ]);
    }
}
