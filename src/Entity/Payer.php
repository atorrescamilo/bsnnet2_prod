<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PayerRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Payer
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    public function __toString() {
        return $this->name;
    }

    public function getDisplayName() {
        return $this->name;
    }

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enabled;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * Payer can have different line of business.
     * @ORM\ManyToMany(targetEntity="App\Entity\LineOfBusiness")
     * @ORM\JoinTable(name="payer_lineofbusiness",
     *      joinColumns={@ORM\JoinColumn(name="payer_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="lineofbusiness_id", referencedColumnName="id",onDelete="Cascade")}
     *      )
     */
    private $lineOfBusiness;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PayerLineCounty", mappedBy="payer")
     */
    private $payer_line_county;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PaymentTier", mappedBy="payer")
     */
    private $paymenttier;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PayerUser", mappedBy="payer")
     */
    private $payer_users;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PayerCustomerCounty", mappedBy="payer")
     */
    private $payercustomer;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\MMMIndicator", mappedBy="payer")
     */
    private $indicators;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PayerContact", mappedBy="payer")
     */
    private $payer_contact;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrganizationPayerExcluded", mappedBy="payer")
     */
    private $organization_payer_excluded;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProviderPayerExcluded", mappedBy="payer")
     */
    private $provider_payer_excluded;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $alias;

    public function __construct()
    {
        $this->enabled = true;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(?bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }


    /**
     * Add lineOfBusiness
     *
     * @param \App\Entity\LineOfBusiness $lineOfBusiness
     *
     * @return Payer
     */
    public function addLineOfBussiness(\App\Entity\LineOfBusiness $lineOfBusines)
    {
        $this->lineOfBusiness[] = $lineOfBusines;
        return $this;
    }

    /**
     * Remove lineOfBusiness
     *
     * @param \App\Entity\LineOfBusiness $lineOfBusines
     */
    public function removeLineOfBusiness(\App\Entity\LineOfBusiness $lineOfBusines)
    {
        $this->lineOfBusiness->removeElement($lineOfBusines);
    }

    /**
     * Get lineOfBusiness
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLineOfBusiness()
    {
        return $this->lineOfBusiness;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    public function getAlias(): ?string
    {
        return $this->alias;
    }

    public function setAlias(?string $alias): self
    {
        $this->alias = $alias;

        return $this;
    }



}
