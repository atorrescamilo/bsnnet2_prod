<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PayerUserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PayerUser implements UserInterface , \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enabled;

    /**
     * @ORM\Column(type="json")
     */
    protected $roles = [];

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Payer", inversedBy="payer_users")
     * @ORM\JoinColumn(name="payer_id", referencedColumnName="id", nullable=false)
     */
    protected $payer;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\PayerRole")
     * @ORM\JoinTable(name="payer_user_roles",
     *     joinColumns={@ORM\JoinColumn(name="payer_user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="payer_role_id", referencedColumnName="id")}
     * )
     */
    protected $payer_user_roles;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $salt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reset_code_hass;

    /**
     * @ORM\Column(type="boolean", nullable=true,options={"default" = 1})
     */
    private $is_new;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastActivityAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedLastLogin;

    public function __construct()
    {
        $this->enabled = true;
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this; return $this->render('payeruser/check_email.html.twig');
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(?bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * String representation of object
     * @link https://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->email,
            $this->password
        ]);
    }

    /**
     * Constructs the object
     * @link https://php.net/manual/en/serializable.unserialize.php
     * @param string $serialized <p>
     * The string representation of the object.
     * </p>
     * @return void
     * @since 5.1.0
     */
    public function unserialize($string)
    {
        list($this->id,
            $this->username,
            $this->email,
            $this->password)=unserialize($string,
            ['allowed_classes'=>false]);
    }

    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return ['ROLE_USER'];
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return (Role|string)[] The user roles
     */
    public function getRoles()
    {
        // guarantee every user at least has ROLE_USER
        $rolesList=$this->getPayerUserRoles();
        $roles=array();

        if($rolesList!==null or $rolesList!==""){
            foreach ($rolesList as $rol){
                $roles[]=$rol->getName();
            }
        }

        return array_unique($roles);
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {

    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
       return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return mixed
     */
    public function getPayer()
    {
        return $this->payer;
    }

    /**
     * @param mixed $payer
     */
    public function setPayer($payer): void
    {
        $this->payer = $payer;
    }

    /**
     * @return mixed
     */
    public function getPayerUserRoles()
    {
        return $this->payer_user_roles;
    }

    /**
     * @param mixed $payer_user_roles
     */
    public function setPayerUserRoles($payer_user_roles): void
    {
        $this->payer_user_roles = $payer_user_roles;
    }


    /**
     * Add user_roles
     * @param \App\Entity\PayerRole $userRoles
     */
    public function addRole(PayerRole $userRoles) {
        $this->payer_user_roles[] = $userRoles;
    }

    /**
     * Remove payer_user_roles
     *
     * @param \App\Entity\PayerRole $role
     */
    public function removeRole(\App\Entity\PayerRole $role)
    {
        $this->payer_user_roles->removeElement($role);
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    public function getResetCodeHass(): ?string
    {
        return $this->reset_code_hass;
    }

    /**
     * @return Bool Whether the user is active or not
     */
    public function isActiveNow()
    {
        // Delay during wich the user will be considered as still active
        $delay = new \DateTime('2 minutes ago');

        return ( $this->getLastActivityAt() > $delay );
    }

    public function setResetCodeHass(?string $reset_code_hass): self
    {
        $this->reset_code_hass = $reset_code_hass;

        return $this;
    }

    public function getIsNew(): ?bool
    {
        return $this->is_new;
    }

    public function setIsNew(?bool $is_new): self
    {
        $this->is_new = $is_new;

        return $this;
    }

    public function getLastActivityAt(): ?\DateTimeInterface
    {
        return $this->lastActivityAt;
    }

    public function setLastActivityAt(?\DateTimeInterface $lastActivityAt): self
    {
        $this->lastActivityAt = $lastActivityAt;

        return $this;
    }

    public function getUpdatedLastLogin(): ?\DateTimeInterface
    {
        return $this->updatedLastLogin;
    }

    public function setUpdatedLastLogin(?\DateTimeInterface $updatedLastLogin): self
    {
        $this->updatedLastLogin = $updatedLastLogin;

        return $this;
    }

}
