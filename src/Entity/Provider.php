<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\Common\ProviderFields;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Shapecode\NYADoctrineEncryptBundle\Configuration\Encrypted;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProviderRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Provider{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $hospital_npi;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $initial;

    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     * @Assert\NotBlank()
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $date_of_birth;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $disabled_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $social;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state_lic;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lic_state;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UsState", inversedBy="provider")
     * @ORM\JoinColumn(name="us_lic_state_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     *
     */
    protected $us_lic_state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lic_issue_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lic_expires_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $board_certification_expires_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sanctionDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $emailSendDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $caqh;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $disabled;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $confirmed_doh;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    private $board_certified;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    private $peer_support;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $board_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $board_certified_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $medicaid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $medicare;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $taxonomy_code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(min=10,max=10)
     */
    private $npi_number;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    private $valid_npi_number;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $accepting_new_patients;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $has_telemedicine;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telemedicine_company;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $interested_in_telemedicine;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $interested_in_residential_services;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $has_residential_services;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *
     */
    private $has_pcp;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\NotBlank()
     */
    private $gender_acceptance;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $hospital_acha_number;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $na_caqh;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $allow_cred;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $pedding_caqh;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $provider_agreement_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialing_application_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state_license_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $provider_data_form_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $w9_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $general_liability_coverage_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $loa_loi_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fwa_coi_attestation_file;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProviderCredentialing", mappedBy="provider")
     */
    private $provider_credentialing;

    /**
     * @ORM\Column(type="text", length=255, nullable=true)
     */
    private $board_certified_specialty;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_hospital_affiliations;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hopital_privileges;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hopital_privileges_type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ahca_id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_facility;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" : false})
     */
    private $is_credentialing_exception;


    /**
     * Providers Speaks  many languages.
     * @ManyToMany(targetEntity="App\Entity\Languages")
     * @JoinTable(name="provider_languages",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="languages_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     * @Assert\NotBlank(message="Please select one or more Languages")
     * @Assert\Count(
     *      min = "1",
     *      minMessage = "You have to select at least one item"
     * )
     */
    protected $languages;

    /**
     * Providers can have different types.
     * @ManyToMany(targetEntity="App\Entity\ProviderType")
     * @JoinTable(name="provider_provider_types",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="provider_type_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     * @Assert\NotBlank(message="Please select one or more Provider Type")
     * @Assert\Count(
     *      min = "1",
     *      minMessage = "You have to select at least one item"
     * )
     */
    protected $provider_type;

    /**
     * Providers can have different types.
     * @ManyToMany(targetEntity="App\Entity\AgesRange")
     * @JoinTable(name="provider_age_range",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="age_range_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     * @Assert\NotBlank(message="Please select one or more Age range")
     * @Assert\Count(
     *      min = "1",
     *      minMessage = "You have to select at least one item"
     * )
     */
    protected $age_ranges;

    /**
     * Providers can have different specialty.
     * @ManyToMany(targetEntity="App\Entity\SpecialtyAreaProvider")
     * @JoinTable(name="provider_specialties_area",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="specialty_area_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $specialties_area;

    /**
     * Providers can have different specialty.
     * @ManyToMany(targetEntity="App\Entity\Specialty")
     * @JoinTable(name="provider_primary_specialties",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="primary_specialty_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     * @Assert\NotBlank(message="Please select one or more Primary Specialty")
     * @Assert\Count(
     *      min = "1",
     *      minMessage = "You have to select at least one item"
     * )
     */

    protected $primary_specialties;


    /**
     * Providers can have different specialty.
     * @ManyToMany(targetEntity="App\Entity\Degree")
     * @JoinTable(name="provider_degree",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="degree_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     * @Assert\NotBlank(message="Please select one or more Degree")
     * @Assert\Count(
     *      min = "1",
     *      minMessage = "You have to select at least one item"
     * )
     */
    protected $degree;

    /**
     * Providers can have different CO Morbidities.
     * @ManyToMany(targetEntity="App\Entity\Comorbidity")
     * @JoinTable(name="provider_comorbidities",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="comorbidity_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $comorbidities;

    /**
     * Providers can have different EAP Onlys.
     * @ManyToMany(targetEntity="App\Entity\EapOnly")
     * @JoinTable(name="provider_eap_onlys",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="eap_only_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $eap_onlys;

    /**
     * Providers can have different Payers.
     * @ManyToMany(targetEntity="App\Entity\Payer")
     * @JoinTable(name="provider_payer",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="payer_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $payers;

    /**
     * Providers can have different Payment Tiers.
     * @ManyToMany(targetEntity="App\Entity\PaymentTier")
     * @JoinTable(name="provider_paymenttier",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="paymenttier_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $paymenttiers;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TerminationReason", inversedBy="providers")
     * @ORM\JoinColumn(name="terminationreason_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $terminationReason;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization", inversedBy="providers")
     * @ORM\JoinColumn(name="org_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $organization;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SanctionReason", inversedBy="providers")
     * @ORM\JoinColumn(name="sanctionreason_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $sanctionReason;

    /**
     * Providers can have different General Categories.
     * @ManyToMany(targetEntity="App\Entity\GeneralCategories")
     * @JoinTable(name="provider_general_categories",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="general_category_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     * @Assert\NotBlank
     * @Assert\Count(
     *      min = "1",
     *      minMessage = "You have to select at least one item"
     * )
     */
    protected $general_categories;

    /**
     * provider can have different lineofbusiness.
     * @ORM\ManyToMany(targetEntity="App\Entity\LineOfBusiness")
     * @ORM\JoinTable(name="provider_lineofbusiness",
     *      joinColumns={@ORM\JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="lineofbusiness_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     * @Assert\NotBlank()
     * @Assert\Count(
     *      min = "1",
     *      minMessage = "You have to select at least one item"
     * )
     */
    protected $lineOfBusiness;


    /**
     * provider can have different taxonomy code.
     * @ORM\ManyToMany(targetEntity="App\Entity\TaxonomyCode")
     * @ORM\JoinTable(name="provider_taxonomy_code",
     *      joinColumns={@ORM\JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="taxonomy_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $taxonomy_codes;

    /**
     * provider can have different organizations.
     * @ORM\ManyToMany(targetEntity="App\Entity\Cvo")
     * @ORM\JoinTable(name="provider_cvo",
     *      joinColumns={@ORM\JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="cvo_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $cvos;

    /**
     * provider can have different organizations.
     * @ORM\ManyToMany(targetEntity="App\Entity\HospitalAHCA")
     * @ORM\JoinTable(name="provider_hospital",
     *      joinColumns={@ORM\JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="hospital_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $hospitals;


    /**
     * OWNING SIDE
     *
     * @ManyToMany(targetEntity="App\Entity\BillingAddress", inversedBy="providers")
     * @JoinTable(name="provider_billing_address",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="billing_address_id", referencedColumnName="id", onDelete="Cascade")})
     */
    protected $billing_address;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProviderHospitalAffiliations", mappedBy="provider")
     */
    private $provider_hospital_affiliations;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProviderAddrFCCTN", mappedBy="provider")
     */
    private $addr_fcc_track;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProviderCredFile", mappedBy="provider")
     */
    private $provider_cred_file;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProviderMedicaidPml", mappedBy="provider")
     */
    private $medicaid_pml;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProviderPayerExcluded", mappedBy="provider")
     */
    private $provider_payer_excluded;

    /**
     * @ORM\Column(type="boolean", length=255, nullable=true)
     */
    private $option_out_medicare;

    /**
     * @ORM\Column(type="boolean", length=255, nullable=true)
     */
    private $none_medicaid;

    /**
     * @ORM\Column(type="boolean", length=255, nullable=true)
     */
    private $sent_credsimple;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fcc_tracking;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $addrs_id_track_number;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $dea_expirations;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dea_number;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $dea_state;

    /**
     * Providers can be exclude for different specialty.
     * @ManyToMany(targetEntity="App\Entity\Payer")
     * @JoinTable(name="provider_payer_exclude",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="payer_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $payer_exclude;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $provider_application_receipt_date;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_hospitalist;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cds_number;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $cds_expiration_date;

    /**
     * @ORM\OneToMany(targetEntity=ProviderSpecialty::class, mappedBy="provider")
     */
    private $providerSpecialty;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $state_license_effective_date;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $medical_school_grad_year;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $medical_school_institution_name;

    public function __construct()
    {
        $this->providerSpecialties = new ArrayCollection();
    }

    public function __toString(){
        return $this->first_name." ".$this->last_name;
    }

    public function getDisplayName(){
        return $this->first_name." ".$this->last_name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId($id){
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHospitalNpi()
    {
        return $this->hospital_npi;
    }

    /**
     * @param mixed $hospital_npi
     */
    public function setHospitalNpi($hospital_npi): void
    {
        $this->hospital_npi = $hospital_npi;
    }

    public function getFullName(){
        return $this->first_name." ".$this->getLastName();
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getInitial(): ?string
    {
        return $this->initial;
    }

    public function setInitial(?string $initial): self
    {
        $this->initial = $initial;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getDateOfBirth(): ?string
    {
        return $this->date_of_birth;
    }

    public function setDateOfBirth(?string $date_of_birth): self
    {
        $this->date_of_birth = $date_of_birth;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getSocial(): ?string
    {
        return $this->social;
    }

    public function setSocial(?string $social): self
    {
        $this->social = $social;

        return $this;
    }

    public function getStateLic(): ?string
    {
        return $this->state_lic;
    }

    public function setStateLic(?string $state_lic): self
    {
        $this->state_lic = $state_lic;

        return $this;
    }

    public function getLicState(): ?string
    {
        return $this->lic_state;
    }

    public function setLicState(?string $lic_state): self
    {
        $this->lic_state = $lic_state;

        return $this;
    }

    public function getLicIssueDate(): ?string
    {
        return $this->lic_issue_date;
    }

    public function setLicIssueDate(?string $lic_issue_date): self
    {
        $this->lic_issue_date = $lic_issue_date;

        return $this;
    }

    public function getSanctionDate(): ?string
    {
        return $this->sanctionDate;
    }

    public function setSanctionDate(?string $sanctionDate): self
    {
        $this->sanctionDate = $sanctionDate;

        return $this;
    }

    public function getEmailSendDate(): ?string
    {
        return $this->emailSendDate;
    }

    public function setEmailSendDate(?string $emailSendDate): self
    {
        $this->emailSendDate = $emailSendDate;

        return $this;
    }

    public function getCaqh(): ?string
    {
        return $this->caqh;
    }

    public function setCaqh(?string $caqh): self
    {
        $this->caqh = $caqh;

        return $this;
    }

    public function getDisabled(): ?bool
    {
        return $this->disabled;
    }

    public function setDisabled(?bool $disabled): self
    {
        $this->disabled = $disabled;

        return $this;
    }

    public function getBoardCertified()
    {
        return $this->board_certified;
    }

    public function setBoardCertified(?string $board_certified): self
    {
        $this->board_certified = $board_certified;

        return $this;
    }

    public function getBoardName(): ?string
    {
        return $this->board_name;
    }

    public function setBoardName(?string $board_name): self
    {
        $this->board_name = $board_name;

        return $this;
    }

    public function getMedicaid(): ?string
    {
        return $this->medicaid;
    }

    public function setMedicaid(?string $medicaid): self
    {
        $this->medicaid = $medicaid;

        return $this;
    }

    public function getMedicare(): ?string
    {
        return $this->medicare;
    }

    public function setMedicare(?string $medicare): self
    {
        $this->medicare = $medicare;

        return $this;
    }

    public function getTaxonomyCode(): ?string
    {
        return $this->taxonomy_code;
    }

    public function setTaxonomyCode(?string $taxonomy_code): self
    {
        $this->taxonomy_code = $taxonomy_code;

        return $this;
    }

    public function getNpiNumber(): ?string
    {
        return $this->npi_number;
    }

    public function setNpiNumber(?string $npi_number): self
    {
        $this->npi_number = $npi_number;

        return $this;
    }

    public function getAcceptingNewPatients(): ?bool
    {
        return $this->accepting_new_patients;
    }

    public function setAcceptingNewPatients(?bool $accepting_new_patients): self
    {
        $this->accepting_new_patients = $accepting_new_patients;

        return $this;
    }

    public function getInterestedInTelemedicine(): ?bool
    {
        return $this->interested_in_telemedicine;
    }

    public function setInterestedInTelemedicine(?bool $interested_in_telemedicine): self
    {
        $this->interested_in_telemedicine = $interested_in_telemedicine;

        return $this;
    }

    public function getInterestedInResidentialServices(): ?bool
    {
        return $this->interested_in_residential_services;
    }

    public function setInterestedInResidentialServices(?bool $interested_in_residential_services): self
    {
        $this->interested_in_residential_services = $interested_in_residential_services;

        return $this;
    }

    public function getHasPcp(): ?bool
    {
        return $this->has_pcp;
    }

    public function setHasPcp(?bool $has_pcp): self
    {
        $this->has_pcp = $has_pcp;

        return $this;
    }

    public function getGenderAcceptance(): ?string
    {
        return $this->gender_acceptance;
    }

    public function setGenderAcceptance(?string $gender_acceptance): self
    {
        $this->gender_acceptance = $gender_acceptance;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getProviderAgreementFile(): ?string
    {
        return $this->provider_agreement_file;
    }

    public function setProviderAgreementFile(?string $provider_agreement_file): self
    {
        $this->provider_agreement_file = $provider_agreement_file;

        return $this;
    }

    public function getCredentialingApplicationFile(): ?string
    {
        return $this->credentialing_application_file;
    }

    public function setCredentialingApplicationFile(?string $credentialing_application_file): self
    {
        $this->credentialing_application_file = $credentialing_application_file;

        return $this;
    }

    public function getStateLicenseFile(): ?string
    {
        return $this->state_license_file;
    }

    public function setStateLicenseFile(?string $state_license_file): self
    {
        $this->state_license_file = $state_license_file;

        return $this;
    }

    public function getProviderDataFormFile(): ?string
    {
        return $this->provider_data_form_file;
    }

    public function setProviderDataFormFile(?string $provider_data_form_file): self
    {
        $this->provider_data_form_file = $provider_data_form_file;

        return $this;
    }

    public function getW9File(): ?string
    {
        return $this->w9_file;
    }

    public function setW9File(?string $w9_file): self
    {
        $this->w9_file = $w9_file;

        return $this;
    }

    public function getGeneralLiabilityCoverageFile(): ?string
    {
        return $this->general_liability_coverage_file;
    }

    public function setGeneralLiabilityCoverageFile(?string $general_liability_coverage_file): self
    {
        $this->general_liability_coverage_file = $general_liability_coverage_file;

        return $this;
    }

    public function getLoaLoiFile(): ?string
    {
        return $this->loa_loi_file;
    }

    public function setLoaLoiFile(?string $loa_loi_file): self
    {
        $this->loa_loi_file = $loa_loi_file;

        return $this;
    }

    /**
     * Add language
     *
     * @param \App\Entity\Languages $language
     *
     * @return Provider
     */
    public function addLanguage(\App\Entity\Languages $language)
    {
        $this->languages[] = $language;

        return $this;
    }

    /**
     * Remove language
     *
     * @param \App\Entity\Languages $language
     */
    public function removeLanguage(\App\Entity\Languages $language)
    {
        $this->languages->removeElement($language);
    }

    /**
     * Get languages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * Add provider_type
     *
     * @param \App\Entity\ProviderType $provider_type
     *
     * @return Provider
     */
    public function addProviderType(\App\Entity\ProviderType $provider_type)
    {
        $this->provider_type[] = $provider_type;

        return $this;
    }

    /**
     * Remove provider_type
     *
     * @param \App\Entity\ProviderType $provider_type
     */
    public function removeProviderType(\App\Entity\ProviderType $provider_type)
    {
        $this->provider_type->removeElement($provider_type);
    }

    /**
     * Get provider_type
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProviderType()
    {
        return $this->provider_type;
    }

    /**
     * Add primarySpecialty
     *
     * @param \App\Entity\Specialty $primarySpecialty
     *
     * @return Provider
     */
    public function addPrimarySpecialty(\App\Entity\Specialty $primarySpecialty)
    {
        $this->primary_specialties[] = $primarySpecialty;

        return $this;
    }

    /**
     * Remove primarySpecialty
     *
     * @param \App\Entity\Specialty $primarySpecialty
     */
    public function removePrimarySpecialty(\App\Entity\Specialty $primarySpecialty)
    {
        $this->primary_specialties->removeElement($primarySpecialty);
    }

    /**
     * Get primarySpecialties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrimarySpecialties()
    {
        return $this->primary_specialties;
    }

    /**
     * Add degree
     *
     * @param \App\Entity\Degree $degree
     *
     * @return Provider
     */
    public function addDegree(\App\Entity\Degree $degree)
    {
        $this->degree[] = $degree;

        return $this;
    }

    /**
     * Remove degree
     *
     * @param \App\Entity\Degree $degree
     */
    public function removeDegree(\App\Entity\Degree $degree)
    {
        $this->degree->removeElement($degree);
    }

    /**
     * Get degree
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * Add comorbidity
     *
     * @param \App\Entity\Comorbidity $comorbidity
     *
     * @return Provider
     */
    public function addComorbidity(\App\Entity\Comorbidity $comorbidity)
    {
        $this->comorbidities[] = $comorbidity;

        return $this;
    }

    /**
     * Remove comorbidity
     *
     * @param \App\Entity\Comorbidity $comorbidity
     */
    public function removeComorbidity(\App\Entity\Comorbidity $comorbidity)
    {
        $this->comorbidities->removeElement($comorbidity);
    }

    /**
     * Get comorbidity
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComorbidities()
    {
        return $this->comorbidities;
    }

    /**
     * Add eap_only
     *
     * @param \App\Entity\EapOnly $eap_only
     *
     * @return Provider
     */
    public function addEapOnly(\App\Entity\EapOnly $eap_only)
    {
        $this->eap_onlys[] = $eap_only;

        return $this;
    }

    /**
     * Remove eap_only
     *
     * @param \App\Entity\EapOnly $eap_only
     */
    public function removeEapOnly(\App\Entity\EapOnly $eap_only)
    {
        $this->eap_onlys->removeElement($eap_only);
    }

    /**
     * Get eap_only
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEapOnlys()
    {
        return $this->eap_onlys;
    }

    /**
     * @return mixed
     */
    public function getTerminationReason()
    {
        return $this->terminationReason;
    }

    /**
     * @param mixed $terminationReason
     */
    public function setTerminationReason($terminationReason): void
    {
        $this->terminationReason = $terminationReason;
    }

    /**
     * @return mixed
     */
    public function getSanctionReason()
    {
        return $this->sanctionReason;
    }

    /**
     * @param mixed $sanctionReason
     */
    public function setSanctionReason($sanctionReason): void
    {
        $this->sanctionReason = $sanctionReason;
    }

    /**
     * Add payer
     *
     * @param \App\Entity\Payer $payer
     *
     * @return Provider
     */
    public function addPayer(\App\Entity\Payer $payer)
    {
        $this->payers[] = $payer;

        return $this;
    }

    /**
     * Remove payer
     *
     * @param \App\Entity\Payer $payer
     */
    public function removePayer(\App\Entity\Payer $payer){
        $this->payers->removeElement($payer);
    }

    /**
     * Get  payers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPayers(){
        return $this->payers;
    }

    /**
     * Add general_category
     *
     * @param \App\Entity\GeneralCategories $general_category
     *
     * @return Provider
     */
    public function addGeneralCategory(\App\Entity\GeneralCategories $general_category)
    {
        $this->general_categories[] = $general_category;

        return $this;
    }

    /**
     * Remove general_category
     *
     * @param \App\Entity\GeneralCategories $general_category
     */
    public function removeGeneralCategory(\App\Entity\GeneralCategories $general_category)
    {
        $this->general_categories->removeElement($general_category);
    }

    /**
     * Get general_categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGeneralCategories()
    {
        return $this->general_categories;
    }

    /**
     * Add lineOfBusiness
     *
     * @param \App\Entity\LineOfBusiness $lineOfBusiness
     *
     * @return Provider
     */
    public function addLineOfBusines(\App\Entity\LineOfBusiness $lineOfBusiness)
    {
        $this->lineOfBusiness[] = $lineOfBusiness;

        return $this;
    }

    /**
     * Remove lineOfBusiness
     *
     * @param \App\Entity\LineOfBusiness $lineOfBusiness
     */
    public function removeLineOfBusines(\App\Entity\LineOfBusiness $lineOfBusiness)
    {
        $this->lineOfBusiness->removeElement($lineOfBusiness);
    }

    /**
     * Get lineOfBusiness
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLineOfBusiness()
    {
        return $this->lineOfBusiness;
    }


    /**
     * Add cvo
     *
     * @param \App\Entity\Cvo $cvo
     *
     * @return Provider
     */
    public function addCvo(\App\Entity\Cvo $cvo)
    {
        $this->cvos[] = $cvo;
        return $this;
    }

    /**
     * Remove cvo
     *
     * @param \App\Entity\Cvo $cvo
     */
    public function removeCvo(\App\Entity\Cvo $cvo)
    {
        $this->cvos->removeElement($cvo);
    }

    /**
     * Get cvos
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCvos()
    {
        return $this->cvos;
    }

    /**
     * Add billing_address
     *
     * @param \App\Entity\BillingAddress $billing_address
     *
     * @return Provider
     */
    public function addBillingAddress(\App\Entity\BillingAddress $billing_address){
        $this->billing_address[] = $billing_address;
        return $this;
    }

    /**
     * Remove billing_address
     *
     * @param \App\Entity\BillingAddress $billing_address
     */
    public function removeBillingAddress(\App\Entity\BillingAddress $billing_address)
    {
        $this->billing_address->removeElement($billing_address);
    }

    /**
     * Get billing_address
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBillingAddress()
    {
        return $this->billing_address;
    }

    /**
     * @return mixed
     */
    public function getBoardCertifiedSpecialty()
    {
        return $this->board_certified_specialty;
    }

    /**
     * @param mixed $board_certified_specialty
     */
    public function setBoardCertifiedSpecialty($board_certified_specialty): void
    {
        $this->board_certified_specialty = $board_certified_specialty;
    }

    /**
     * @return mixed
     */
    public function getDisabledDate()
    {
        return $this->disabled_date;
    }

    /**
     * @param mixed $disabled_date
     */
    public function setDisabledDate($disabled_date): void
    {
        $this->disabled_date = $disabled_date;
    }

    /**
     * Add taxonomy code
     *
     * @param \App\Entity\TaxonomyCode $taxonomy_code
     *
     * @return Provider
     */
    public function addTaxonomyCode(\App\Entity\TaxonomyCode $taxonomy_code)
    {
        $this->taxonomy_codes[] = $taxonomy_code;

        return $this;
    }

    /**
     * Remove taxonomy_code
     *
     * @param \App\Entity\TaxonomyCode $taxonomy_code
     */
    public function removeTaxonomyCode(\App\Entity\TaxonomyCode $taxonomy_code)
    {
        $this->taxonomy_codes->removeElement($taxonomy_code);
    }

    /**
     * Get taxonomy_codes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTaxonomyCodes()
    {
        return $this->taxonomy_codes;
    }

    /**
     * @return mixed
     */
    public function getSentCredsimple()
    {
        return $this->sent_credsimple;
    }

    /**
     * @param mixed $sent_credsimple
     */
    public function setSentCredsimple($sent_credsimple): void
    {
        $this->sent_credsimple = $sent_credsimple;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     */
    public function setOrganization($organization): void
    {
        $this->organization = $organization;
    }

    /**
     * Add payementtier
     *
     * @param \App\Entity\PaymentTier $paymenttier
     *
     * @return Provider
     */
    public function addPaymentTier(\App\Entity\PaymentTier $paymenttier)
    {
        $this->paymenttiers[] = $paymenttier;

        return $this;
    }

    /**
     * Remove paymenttier
     *
     * @param \App\Entity\PaymentTier $paymenttier
     */
    public function removePaymentTier(\App\Entity\Paymenttier $paymenttier)
    {
        $this->paymenttiers->removeElement($paymenttier);
    }

    /**
     * Get  paymentiers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaymentTiers()
    {
        return $this->paymenttiers;
    }

    /**
     * @return mixed
     */
    public function getOptionOutMedicare()
    {
        return $this->option_out_medicare;
    }

    /**
     * @param mixed $option_out_medicare
     */
    public function setOptionOutMedicare($option_out_medicare): void
    {
        $this->option_out_medicare = $option_out_medicare;
    }

    /**
     * @return mixed
     */
    public function getFccTracking()
    {
        return $this->fcc_tracking;
    }

    /**
     * @param mixed $fcc_tracking
     */
    public function setFccTracking($fcc_tracking): void
    {
        $this->fcc_tracking = $fcc_tracking;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt(): void
    {
        $this->updated_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getFwaCoiAttestationFile()
    {
        return $this->fwa_coi_attestation_file;
    }

    /**
     * @param mixed $fwa_coi_attestation_file
     */
    public function setFwaCoiAttestationFile($fwa_coi_attestation_file): void
    {
        $this->fwa_coi_attestation_file = $fwa_coi_attestation_file;
    }

    /**
     * @return mixed
     */
    public function getBoardCertifiedDate()
    {
        return $this->board_certified_date;
    }

    /**
     * @param mixed $board_certified_date
     */
    public function setBoardCertifiedDate($board_certified_date): void
    {
        $this->board_certified_date = $board_certified_date;
    }

    /**
     * @return mixed
     */
    public function getHopitalPrivileges()
    {
        return $this->hopital_privileges;
    }

    /**
     * @param mixed $hopital_privileges
     */
    public function setHopitalPrivileges($hopital_privileges): void
    {
        $this->hopital_privileges = $hopital_privileges;
    }

    /**
     * @return mixed
     */
    public function getHopitalPrivilegesType()
    {
        return $this->hopital_privileges_type;
    }

    /**
     * @param mixed $hopital_privileges_type
     */
    public function setHopitalPrivilegesType($hopital_privileges_type): void
    {
        $this->hopital_privileges_type = $hopital_privileges_type;
    }

    /**
     * @return mixed
     */
    public function getHasTelemedicine()
    {
        return $this->has_telemedicine;
    }

    /**
     * @param mixed $has_telemedicine
     */
    public function setHasTelemedicine($has_telemedicine): void
    {
        $this->has_telemedicine = $has_telemedicine;
    }

    /**
     * @return mixed
     */
    public function getTelemedicineCompany()
    {
        return $this->telemedicine_company;
    }

    /**
     * @param mixed $telemedicine_company
     */
    public function setTelemedicineCompany($telemedicine_company): void
    {
        $this->telemedicine_company = $telemedicine_company;
    }

    /**
     * @return mixed
     */
    public function getAhcaId()
    {
        return $this->ahca_id;
    }

    /**
     * @param mixed $ahca_id
     */
    public function setAhcaId($ahca_id): void
    {
        $this->ahca_id = $ahca_id;
    }

    /**
     * Add hospital
     *
     * @param \App\Entity\HospitalAHCA $hospital
     *
     * @return Provider
     */
    public function addHospital(\App\Entity\HospitalAHCA $hospitalAHCA)
    {
        $this->hospitals[] = $hospitalAHCA;
        return $this;
    }

    /**
     * Remove hospital
     *
     * @param \App\Entity\HospitalAHCA $hospital
     */
    public function removeHospital(\App\Entity\HospitalAHCA $hospitalAHCA)
    {
        $this->hospitals->removeElement($hospitalAHCA);
    }

    /**
     * Get hospitals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHospitals()
    {
        return $this->hospitals;
    }

    /**
     * @return mixed
     */
    public function getAddrsIdTrackNumber()
    {
        return $this->addrs_id_track_number;
    }

    /**
     * @param mixed $addrs_id_track_number
     */
    public function setAddrsIdTrackNumber($addrs_id_track_number): void
    {
        $this->addrs_id_track_number = $addrs_id_track_number;
    }

    /**
     * @return mixed
     */
    public function getIsFacility()
    {
        return $this->is_facility;
    }

    /**
     * @param mixed $is_facility
     */
    public function setIsFacility($is_facility): void
    {
        $this->is_facility = $is_facility;
    }

    /**
     * @return mixed
     */
    public function getNaCaqh()
    {
        return $this->na_caqh;
    }

    /**
     * @param mixed $na_caqh
     */
    public function setNaCaqh($na_caqh): void
    {
        $this->na_caqh = $na_caqh;
    }

    /**
     * @return mixed
     */
    public function getLicExpiresDate()
    {
        return $this->lic_expires_date;
    }

    /**
     * @param mixed $lic_expires_date
     */
    public function setLicExpiresDate($lic_expires_date): void
    {
        $this->lic_expires_date = $lic_expires_date;
    }

    /**
     * @return mixed
     */
    public function getIsHospitalAffiliations()
    {
        return $this->is_hospital_affiliations;
    }

    /**
     * @param mixed $is_hospital_affiliations
     */
    public function setIsHospitalAffiliations($is_hospital_affiliations): void
    {
        $this->is_hospital_affiliations = $is_hospital_affiliations;
    }

    /**
     * @return mixed
     */
    public function getBoardCertificationExpiresDate()
    {
        return $this->board_certification_expires_date;
    }

    /**
     * @param mixed $board_certification_expires_date
     */
    public function setBoardCertificationExpiresDate($board_certification_expires_date): void
    {
        $this->board_certification_expires_date = $board_certification_expires_date;
    }

    /**
     * @return mixed
     */
    public function getHasResidentialServices()
    {
        return $this->has_residential_services;
    }

    /**
     * @param mixed $has_residential_services
     */
    public function setHasResidentialServices($has_residential_services): void
    {
        $this->has_residential_services = $has_residential_services;
    }

    /**
     * @return mixed
     */
    public function getHospitalAchaNumber()
    {
        return $this->hospital_acha_number;
    }

    /**
     * @param mixed $hospital_acha_number
     */
    public function setHospitalAchaNumber($hospital_acha_number): void
    {
        $this->hospital_acha_number = $hospital_acha_number;
    }

    /**
     * @return mixed
     */
    public function getPeddingCaqh()
    {
        return $this->pedding_caqh;
    }

    /**
     * @param mixed $pedding_caqh
     */
    public function setPeddingCaqh($pedding_caqh): void
    {
        $this->pedding_caqh = $pedding_caqh;
    }

    /**
     * @return mixed
     */
    public function getUsLicState()
    {
        return $this->us_lic_state;
    }

    /**
     * @param mixed $us_lic_state
     */
    public function setUsLicState($us_lic_state): void
    {
        $this->us_lic_state = $us_lic_state;
    }

    /**
     * @return mixed
     */
    public function getConfirmedDoh()
    {
        return $this->confirmed_doh;
    }

    /**
     * @param mixed $confirmed_doh
     */
    public function setConfirmedDoh($confirmed_doh): void
    {
        $this->confirmed_doh = $confirmed_doh;
    }

    /**
     * @return mixed
     */
    public function getValidNpiNumber()
    {
        return $this->valid_npi_number;
    }

    /**
     * @param mixed $valid_npi_number
     */
    public function setValidNpiNumber($valid_npi_number): void
    {
        $this->valid_npi_number = $valid_npi_number;
    }

    /**
     * Add payer_excluded
     *
     * @param \App\Entity\Payer $payer
     *
     * @return Provider
     */
    public function addPayerExclude(\App\Entity\Payer $payer)
    {
        $this->payer_exclude[] = $payer;
        return $this;
    }

    /**
     * Remove payer
     *
     * @param \App\Entity\Payer $payer
     */
    public function removePayerExcluded(\App\Entity\Payer $payer)
    {
        $this->payer_exclude->removeElement($payer);
    }

    /**
     * Get payer
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPayerExclude()
    {
        return $this->payer_exclude;
    }

    /**
     * @return mixed
     */
    public function getProviderApplicationReceiptDate()
    {
        return $this->provider_application_receipt_date;
    }

    /**
     * @param mixed $provider_application_receipt_date
     */
    public function setProviderApplicationReceiptDate($provider_application_receipt_date): void
    {
        $this->provider_application_receipt_date = $provider_application_receipt_date;
    }

    /**
     * @return mixed
     */
    public function getNoneMedicaid()
    {
        return $this->none_medicaid;
    }

    /**
     * @param mixed $none_medicaid
     */
    public function setNoneMedicaid($none_medicaid): void
    {
        $this->none_medicaid = $none_medicaid;
    }

    /**
     * @return mixed
     */
    public function getIsCredentialingException()
    {
        return $this->is_credentialing_exception;
    }

    /**
     * @param mixed $is_credentialing_exception
     */
    public function setIsCredentialingException($is_credentialing_exception): void
    {
        $this->is_credentialing_exception = $is_credentialing_exception;
    }

    /**
     * Add age_range
     *
     * @param \App\Entity\AgesRange $age_range
     *
     * @return Provider
     */
    public function addAgeRange(\App\Entity\AgesRange $age_range)
    {
        $this->age_ranges[] = $age_range;

        return $this;
    }

    /**
     * Remove age_ranges
     *
     * @param \App\Entity\AgesRange $age_range
     */
    public function removeAgeRange(\App\Entity\AgesRange $age_range)
    {
        $this->age_ranges->removeElement($age_range);
    }

    /**
     * Get age_ranges
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAgeRanges()
    {
        return $this->age_ranges;
    }

    public function getAllowCred(): ?bool
    {
        return $this->allow_cred;
    }

    public function setAllowCred(?bool $allow_cred): self
    {
        $this->allow_cred = $allow_cred;

        return $this;
    }

    /**
     * Add specialtiesArea
     *
     * @param \App\Entity\SpecialtyAreaProvider $specialtyArea
     *
     * @return Provider
     */
    public function addSpecialtiesAreon(\App\Entity\SpecialtyAreaProvider $specialtyArea)
    {
        $this->specialties_area[] = $specialtyArea;

        return $this;
    }

    /**
     * Remove specialtiesArea
     *
     * @param \App\Entity\SpecialtyAreaProvider $specialtyArea
     */
    public function removeSpecialtiesAreon(\App\Entity\SpecialtyAreaProvider $specialtyArea)
    {
        $this->specialties_area->removeElement($specialtyArea);
    }

    /**
     * Get specialtiesArea
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpecialtiesArea()
    {
        return $this->specialties_area;
    }

    /**
     * @return mixed
     */
    public function getPeerSupport()
    {
        return $this->peer_support;
    }

    /**
     * @param mixed $peer_support
     */
    public function setPeerSupport($peer_support): void
    {
        $this->peer_support = $peer_support;
    }

    /**
     * @return mixed
     */
    public function getDeaExpirations()
    {
        return $this->dea_expirations;
    }

    /**
     * @param mixed $dea_expirations
     */
    public function setDeaExpirations($dea_expirations): void
    {
        $this->dea_expirations = $dea_expirations;
    }

    /**
     * @return mixed
     */
    public function getDeaNumber()
    {
        return $this->dea_number;
    }

    /**
     * @param mixed $dea_number
     */
    public function setDeaNumber($dea_number): void
    {
        $this->dea_number = $dea_number;
    }

    /**
     * @return mixed
     */
    public function getDeaState()
    {
        return $this->dea_state;
    }

    /**
     * @param mixed $dea_state
     */
    public function setDeaState($dea_state): void
    {
        $this->dea_state = $dea_state;
    }

    public function getIsHospitalist(): ?bool
    {
        return $this->is_hospitalist;
    }

    public function setIsHospitalist(?bool $is_hospitalist): self
    {
        $this->is_hospitalist = $is_hospitalist;

        return $this;
    }

    public function getCdsNumber(): ?string
    {
        return $this->cds_number;
    }

    public function setCdsNumber(?string $cds_number): self
    {
        $this->cds_number = $cds_number;

        return $this;
    }

    public function getCdsExpirationDate(): ?string
    {
        return $this->cds_expiration_date;
    }

    public function setCdsExpirationDate(?string $cds_expiration_date): self
    {
        $this->cds_expiration_date = $cds_expiration_date;

        return $this;
    }

    /**
     * @return Collection|ProviderSpecialty[]
     */
    public function getProviderSpecialties(): Collection
    {
        return $this->providerSpecialties;
    }

    public function addProviderSpecialty(ProviderSpecialty $providerSpecialty): self
    {
        if (!$this->providerSpecialties->contains($providerSpecialty)) {
            $this->providerSpecialties[] = $providerSpecialty;
            $providerSpecialty->setProvider($this);
        }

        return $this;
    }

    public function removeProviderSpecialty(ProviderSpecialty $providerSpecialty): self
    {
        if ($this->providerSpecialties->removeElement($providerSpecialty)) {
            // set the owning side to null (unless already changed)
            if ($providerSpecialty->getProvider() === $this) {
                $providerSpecialty->setProvider(null);
            }
        }

        return $this;
    }

    public function getStateLicenseEffectiveDate(): ?string
    {
        return $this->state_license_effective_date;
    }

    public function setStateLicenseEffectiveDate(?string $state_license_effective_date): self
    {
        $this->state_license_effective_date = $state_license_effective_date;

        return $this;
    }

    public function getMedicalSchoolGradYear(): ?string
    {
        return $this->medical_school_grad_year;
    }

    public function setMedicalSchoolGradYear(?string $medical_school_grad_year): self
    {
        $this->medical_school_grad_year = $medical_school_grad_year;

        return $this;
    }

    public function getMedicalSchoolInstitutionName(): ?string
    {
        return $this->medical_school_institution_name;
    }

    public function setMedicalSchoolInstitutionName(?string $medical_school_institution_name): self
    {
        $this->medical_school_institution_name = $medical_school_institution_name;

        return $this;
    }
}
