<?php

namespace App\Entity;

use App\Repository\ProviderSpecialtyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProviderSpecialtyRepository::class)
 */
class ProviderSpecialty
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $specialty_board_certified;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $board_certified_effective_date;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $board_certified_expiration_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $board_certified_body_name;

    /**
     * @ORM\ManyToOne(targetEntity=Provider::class, inversedBy="providerSpecialty")
     * @ORM\JoinColumn(name="provider_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    private $provider;

    /**
     * @ORM\ManyToOne(targetEntity=Specialty::class, inversedBy="providerSpecialty")
     * @ORM\JoinColumn(name="specialty_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    private $specialty;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSpecialtyBoardCertified(): ?string
    {
        return $this->specialty_board_certified;
    }

    public function setSpecialtyBoardCertified(?string $specialty_board_certified): self
    {
        $this->specialty_board_certified = $specialty_board_certified;

        return $this;
    }

    public function getBoardCertifiedEffectiveDate(): ?string
    {
        return $this->board_certified_effective_date;
    }

    public function setBoardCertifiedEffectiveDate(?string $board_certified_effective_date): self
    {
        $this->board_certified_effective_date = $board_certified_effective_date;

        return $this;
    }

    public function getBoardCertifiedExpirationDate(): ?string
    {
        return $this->board_certified_expiration_date;
    }

    public function setBoardCertifiedExpirationDate(?string $board_certified_expiration_date): self
    {
        $this->board_certified_expiration_date = $board_certified_expiration_date;

        return $this;
    }

    public function getBoardCertifiedBodyName(): ?string
    {
        return $this->board_certified_body_name;
    }

    public function setBoardCertifiedBodyName(?string $board_certified_body_name): self
    {
        $this->board_certified_body_name = $board_certified_body_name;

        return $this;
    }

    public function getProvider(): ?Provider
    {
        return $this->provider;
    }

    public function setProvider(?Provider $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getSpecialty(): ?Specialty
    {
        return $this->specialty;
    }

    /**
     * @param mixed $specialty
     */
    public function setSpecialty(?Specialty $specialty): self
    {
        $this->specialty = $specialty;

        return $this;
    }

}
