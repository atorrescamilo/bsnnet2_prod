<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Shapecode\NYADoctrineEncryptBundle\Configuration\Encrypted;


/**
 * @ORM\Entity(repositoryClass="App\Repository\OrganizationPRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class OrganizationP
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $provider_type;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $hospital_affiliations;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $billing_phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $faxno;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fein;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tin_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name_of_ehr_emr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_on;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_on;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $group_npi;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $group_npi2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $taxonomy_code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $legal_name_tin_owner;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $american_sign_language;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $caqh;

    /**
     * Organization can have different specialty.
     * @ManyToMany(targetEntity="App\Entity\OrgSpecialty")
     * @JoinTable(name="organization_p_org_specialties",
     *      joinColumns={@JoinColumn(name="organization_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="org_specialty_id", referencedColumnName="id",onDelete="Cascade")}
     *      )
     */
    protected $org_specialties;

    /**
     * Organization can have many Accreditation
     * @ManyToMany(targetEntity="App\Entity\Accreditation")
     * @JoinTable(name="organization_p_accreditation",
     *      joinColumns={@JoinColumn(name="organization_id", referencedColumnName="id",onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="accreditation_id", referencedColumnName="id",onDelete="Cascade")}
     *      )
     */
    protected $accreditations;


    /**
     * Organization can have many Accreditation
     * @ManyToMany(targetEntity="App\Entity\SpecialtyAreas")
     * @JoinTable(name="organization_p_specialty_areas",
     *      joinColumns={@JoinColumn(name="organization_id", referencedColumnName="id",onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="specialtyarea_id", referencedColumnName="id",onDelete="Cascade")}
     *      )
     */
    protected $specialty_areas;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProviderUser", inversedBy="organizationp")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $provider_user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrganizationStatus", inversedBy="organizationp")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $organization_status;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLegalNameTinOwner(): ?string
    {
        return $this->legal_name_tin_owner;
    }

    /**
     * @param mixed $legal_name_tin_owner
     */
    public function setLegalNameTinOwner($legal_name_tin_owner): void
    {
        $this->legal_name_tin_owner = $legal_name_tin_owner;
    }


    public function getProviderType(): ?string
    {
        return $this->provider_type;
    }

    public function setProviderType(?string $provider_type): self
    {
        $this->provider_type = $provider_type;

        return $this;
    }

    public function getHospitalAffiliations(): ?string
    {
        return $this->hospital_affiliations;
    }

    public function setHospitalAffiliations(?string $hospital_affiliations): self
    {
        $this->hospital_affiliations = $hospital_affiliations;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getBillingPhone(): ?string
    {
        return $this->billing_phone;
    }

    public function setBillingPhone(?string $billing_phone): self
    {
        $this->billing_phone = $billing_phone;

        return $this;
    }

    public function getFaxno(): ?string
    {
        return $this->faxno;
    }

    public function setFaxno(?string $faxno): self
    {
        $this->faxno = $faxno;

        return $this;
    }

    public function getFein(): ?string
    {
        return $this->fein;
    }

    public function setFein(?string $fein): self
    {
        $this->fein = $fein;

        return $this;
    }

    public function getTinNumber(): ?string
    {
        return $this->tin_number;
    }

    public function setTinNumber(?string $tin_number): self
    {
        $this->tin_number = $tin_number;

        return $this;
    }

    public function getNameOfEhrEmr(): ?string
    {
        return $this->name_of_ehr_emr;
    }

    public function setNameOfEhrEmr(?string $name_of_ehr_emr): self
    {
        $this->name_of_ehr_emr = $name_of_ehr_emr;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getCreatedOn(): ?\DateTimeInterface
    {
        return $this->created_on;
    }

    public function setCreatedOn(?\DateTimeInterface $created_on): self
    {
        $this->created_on = $created_on;

        return $this;
    }

    public function getUpdatedOn(): ?\DateTimeInterface
    {
        return $this->updated_on;
    }

    public function setUpdatedOn(): self
    {
        $this->updated_on = new \DateTime();

        return $this;
    }

    public function getGroupNpi(): ?string
    {
        return $this->group_npi;
    }

    public function setGroupNpi(?string $group_npi): self
    {
        $this->group_npi = $group_npi;

        return $this;
    }

    public function getGroupNpi2(): ?string
    {
        return $this->group_npi2;
    }

    public function setGroupNpi2(?string $group_npi2): self
    {
        $this->group_npi2 = $group_npi2;

        return $this;
    }

    public function getTaxonomyCode(): ?string
    {
        return $this->taxonomy_code;
    }

    public function setTaxonomyCode(?string $taxonomy_code): self
    {
        $this->taxonomy_code = $taxonomy_code;

        return $this;
    }

    /**
     * Add orgSpecialty
     *
     * @param \App\Entity\OrgSpecialty $orgSpecialty
     *
     * @return OrganizationP
     */
    public function addOrgSpecialty(\App\Entity\OrgSpecialty $orgSpecialty)
    {
        $this->org_specialties[] = $orgSpecialty;

        return $this;
    }

    /**
     * Remove orgSpecialty
     *
     * @param \App\Entity\OrgSpecialty $orgSpecialty
     */
    public function removeOrgSpecialty(\App\Entity\OrgSpecialty $orgSpecialty)
    {
        $this->org_specialties->removeElement($orgSpecialty);
    }

    /**
     * Get orgSpecialties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrgSpecialties()
    {
        return $this->org_specialties;
    }

    /**
     * Add specialtyArea
     *
     * @param \App\Entity\SpecialtyAreas $specialtyArea
     *
     * @return OrganizationP
     */
    public function addSpecialtyArea(\App\Entity\SpecialtyAreas $specialtyArea)
    {
        $this->specialty_areas[] = $specialtyArea;

        return $this;
    }

    /**
     * Remove specialtyArea
     *
     * @param \App\Entity\SpecialtyAreas $specialtyArea
     */
    public function removeSpecialtyArea(\App\Entity\SpecialtyAreas $specialtyArea)
    {
        $this->specialty_areas->removeElement($specialtyArea);
    }

    /**
     * Get specialtyAreas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpecialtyAreas()
    {
        return $this->specialty_areas;
    }

    /**
     * Add accreditation
     *
     * @param \App\Entity\Accreditation $accreditation
     *
     * @return OrganizationP
     */
    public function addAccreditation(\App\Entity\Accreditation $accreditation)
    {
        $this->accreditations[] = $accreditation;

        return $this;
    }

    /**
     * Remove accreditation
     *
     * @param \App\Entity\Accreditation $accreditation
     */
    public function removeAccreditation(\App\Entity\Accreditation $accreditation)
    {
        $this->accreditations->removeElement($accreditation);
    }

    /**
     * Get accreditations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAccreditations()
    {
        return $this->accreditations;
    }

    /**
     * @return mixed
     */
    public function getAmericanSignLanguage()
    {
        return $this->american_sign_language;
    }

    /**
     * @param mixed $american_sign_language
     */
    public function setAmericanSignLanguage($american_sign_language): void
    {
        $this->american_sign_language = $american_sign_language;
    }

    /**
     * @return mixed
     */
    public function getCaqh()
    {
        return $this->caqh;
    }

    /**
     * @param mixed $caqh
     */
    public function setCaqh($caqh): void
    {
        $this->caqh = $caqh;
    }

    /**
     * @return mixed
     */
    public function getProviderUser()
    {
        return $this->provider_user;
    }

    /**
     * @param mixed $provider_user
     */
    public function setProviderUser($provider_user): void
    {
        $this->provider_user = $provider_user;
    }


    /**
     * @ORM\PrePersist
     */
    public function setCreateOn() {
        $this->created_on = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getOrganizationStatus()
    {
        return $this->organization_status;
    }

    /**
     * @param mixed $organization_status
     */
    public function setOrganizationStatus($organization_status): void
    {
        $this->organization_status = $organization_status;
    }

}
