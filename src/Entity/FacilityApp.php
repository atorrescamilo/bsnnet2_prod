<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use App\Entity\AdditionalService;


/**
 * @ORM\Entity(repositoryClass="App\Repository\FacilityAppRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class FacilityApp
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facility_dba;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $npi_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tax_identification;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $medicare_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $medicaid_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contact_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contact_last_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contact_email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contact_phone_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contact_fax;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $other_additional_service;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facility_owner;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $licensed_by_state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state_license_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state_license_expiration_date;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $license_by_city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $license_city_county;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city_license_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city_license_expiration_date;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_accredited;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $accreditation_body;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $date_last_accreditation;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $accreditation_expiration_date;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $have_site_survey;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $date_recent_survey_conducted;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $saction_last3_year;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $accreditation_more_context;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $insurance_agency;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $policy_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $single_occurrence_amount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $aggredate_amount;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $issues_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $expiration_date;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $convictions_under_federal;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $context_convictions_under_federal;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $federal_sanstions_limitations;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $contect_federal_sanstions_limitations;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\FacilityType", inversedBy="facilityApp")
     * @ORM\JoinColumn(name="facility_type_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $facility_type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\LegalType", inversedBy="facilityApp")
     * @ORM\JoinColumn(name="legal_type_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $legal_type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\UsState", inversedBy="facilityApp3")
     * @ORM\JoinColumn(name="licensing_state_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $licensing_state;

    /**
     * FacilityApp can have different Additional Services.
     * @ManyToMany(targetEntity="App\Entity\AdditionalService")
     * @JoinTable(name="facilityApp_additionalservice",
     *      joinColumns={@JoinColumn(name="facility_app_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="additional_service_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $additional_services;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProviderUser", inversedBy="facilityApp")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $provider_user;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_on;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_on;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $date_signature;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $authorized_agent_first_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $authorized_agent_last_name;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProviderUser()
    {
        return $this->provider_user;
    }

    /**
     * @param mixed $provider_user
     */
    public function setProviderUser($provider_user): void
    {
        $this->provider_user = $provider_user;
    }


    public function getFacilityDba(): ?string
    {
        return $this->facility_dba;
    }

    public function setFacilityDba(?string $facility_dba): self
    {
        $this->facility_dba = $facility_dba;

        return $this;
    }

    public function getNpiNumber(): ?string
    {
        return $this->npi_number;
    }

    public function setNpiNumber(?string $npi_number): self
    {
        $this->npi_number = $npi_number;

        return $this;
    }

    public function getTaxIdentification(): ?string
    {
        return $this->tax_identification;
    }

    public function setTaxIdentification(?string $tax_identification): self
    {
        $this->tax_identification = $tax_identification;

        return $this;
    }

    public function getMedicareNumber(): ?string
    {
        return $this->medicare_number;
    }

    public function setMedicareNumber(?string $medicare_number): self
    {
        $this->medicare_number = $medicare_number;

        return $this;
    }

    public function getMedicaidNumber(): ?string
    {
        return $this->medicaid_number;
    }

    public function setMedicaidNumber(?string $medicaid_number): self
    {
        $this->medicaid_number = $medicaid_number;

        return $this;
    }

    public function getContactName(): ?string
    {
        return $this->contact_name;
    }

    public function setContactName(?string $contact_name): self
    {
        $this->contact_name = $contact_name;

        return $this;
    }

    public function getContactLastName(): ?string
    {
        return $this->contact_last_name;
    }

    public function setContactLastName(?string $contact_last_name): self
    {
        $this->contact_last_name = $contact_last_name;

        return $this;
    }

    public function getContactEmail(): ?string
    {
        return $this->contact_email;
    }

    public function setContactEmail(?string $contact_email): self
    {
        $this->contact_email = $contact_email;

        return $this;
    }

    public function getContactPhoneNumber(): ?string
    {
        return $this->contact_phone_number;
    }

    public function setContactPhoneNumber(?string $contact_phone_number): self
    {
        $this->contact_phone_number = $contact_phone_number;

        return $this;
    }

    public function getContactFax(): ?string
    {
        return $this->contact_fax;
    }

    public function setContactFax(?string $contact_fax): self
    {
        $this->contact_fax = $contact_fax;

        return $this;
    }

    public function getFacilityOwner(): ?string
    {
        return $this->facility_owner;
    }

    public function setFacilityOwner(?string $facility_owner): self
    {
        $this->facility_owner = $facility_owner;

        return $this;
    }

    public function getLicensedByState(): ?bool
    {
        return $this->licensed_by_state;
    }

    public function setLicensedByState(?bool $licensed_by_state): self
    {
        $this->licensed_by_state = $licensed_by_state;

        return $this;
    }

    public function getStateLicenseNumber(): ?string
    {
        return $this->state_license_number;
    }

    public function setStateLicenseNumber(?string $state_license_number): self
    {
        $this->state_license_number = $state_license_number;

        return $this;
    }

    public function getStateLicenseExpirationDate(): ?string
    {
        return $this->state_license_expiration_date;
    }

    public function setStateLicenseExpirationDate(?string $state_license_expiration_date): self
    {
        $this->state_license_expiration_date = $state_license_expiration_date;

        return $this;
    }

    public function getLicenseByCity(): ?bool
    {
        return $this->license_by_city;
    }

    public function setLicenseByCity(?bool $license_by_city): self
    {
        $this->license_by_city = $license_by_city;

        return $this;
    }

    public function getLicenseCityCounty(): ?string
    {
        return $this->license_city_county;
    }

    public function setLicenseCityCounty(?string $license_city_county): self
    {
        $this->license_city_county = $license_city_county;

        return $this;
    }

    public function getCityLicenseNumber(): ?string
    {
        return $this->city_license_number;
    }

    public function setCityLicenseNumber(?string $city_license_number): self
    {
        $this->city_license_number = $city_license_number;

        return $this;
    }

    public function getCityLicenseExpirationDate(): ?string
    {
        return $this->city_license_expiration_date;
    }

    public function setCityLicenseExpirationDate(?string $city_license_expiration_date): self
    {
        $this->city_license_expiration_date = $city_license_expiration_date;

        return $this;
    }

    public function getIsAccredited(): ?bool
    {
        return $this->is_accredited;
    }

    public function setIsAccredited(?bool $is_accredited): self
    {
        $this->is_accredited = $is_accredited;

        return $this;
    }

    public function getAccreditationBody(): ?string
    {
        return $this->accreditation_body;
    }

    public function setAccreditationBody(?string $accreditation_body): self
    {
        $this->accreditation_body = $accreditation_body;

        return $this;
    }

    public function getDateLastAccreditation(): ?string
    {
        return $this->date_last_accreditation;
    }

    public function setDateLastAccreditation(?string $date_last_accreditation): self
    {
        $this->date_last_accreditation = $date_last_accreditation;

        return $this;
    }

    public function getAccreditationExpirationDate(): ?string
    {
        return $this->accreditation_expiration_date;
    }

    public function setAccreditationExpirationDate(?string $accreditation_expiration_date): self
    {
        $this->accreditation_expiration_date = $accreditation_expiration_date;

        return $this;
    }

    public function getHaveSiteSurvey(): ?bool
    {
        return $this->have_site_survey;
    }

    public function setHaveSiteSurvey(?bool $have_site_survey): self
    {
        $this->have_site_survey = $have_site_survey;

        return $this;
    }

    public function getDateRecentSurveyConducted(): ?string
    {
        return $this->date_recent_survey_conducted;
    }

    public function setDateRecentSurveyConducted(?string $date_recent_survey_conducted): self
    {
        $this->date_recent_survey_conducted = $date_recent_survey_conducted;

        return $this;
    }

    public function getSactionLast3Year(): ?bool
    {
        return $this->saction_last3_year;
    }

    public function setSactionLast3Year(?bool $saction_last3_year): self
    {
        $this->saction_last3_year = $saction_last3_year;

        return $this;
    }

    public function getAccreditationMoreContext(): ?string
    {
        return $this->accreditation_more_context;
    }

    public function setAccreditationMoreContext(?string $accreditation_more_context): self
    {
        $this->accreditation_more_context = $accreditation_more_context;

        return $this;
    }

    public function getInsuranceAgency(): ?string
    {
        return $this->insurance_agency;
    }

    public function setInsuranceAgency(?string $insurance_agency): self
    {
        $this->insurance_agency = $insurance_agency;

        return $this;
    }

    public function getPolicyNumber(): ?string
    {
        return $this->policy_number;
    }

    public function setPolicyNumber(?string $policy_number): self
    {
        $this->policy_number = $policy_number;

        return $this;
    }

    public function getSingleOccurrenceAmount(): ?string
    {
        return $this->single_occurrence_amount;
    }

    public function setSingleOccurrenceAmount(?string $single_occurrence_amount): self
    {
        $this->single_occurrence_amount = $single_occurrence_amount;

        return $this;
    }

    public function getAggredateAmount(): ?string
    {
        return $this->aggredate_amount;
    }

    public function setAggredateAmount(?string $aggredate_amount): self
    {
        $this->aggredate_amount = $aggredate_amount;

        return $this;
    }

    public function getIssuesDate(): ?string
    {
        return $this->issues_date;
    }

    public function setIssuesDate(?string $issues_date): self
    {
        $this->issues_date = $issues_date;

        return $this;
    }

    public function getExpirationDate(): ?string
    {
        return $this->expiration_date;
    }

    public function setExpirationDate(?string $expiration_date): self
    {
        $this->expiration_date = $expiration_date;

        return $this;
    }

    public function getConvictionsUnderFederal(): ?bool
    {
        return $this->convictions_under_federal;
    }

    public function setConvictionsUnderFederal(?bool $convictions_under_federal): self
    {
        $this->convictions_under_federal = $convictions_under_federal;

        return $this;
    }

    public function getContextConvictionsUnderFederal(): ?string
    {
        return $this->context_convictions_under_federal;
    }

    public function setContextConvictionsUnderFederal(?string $context_convictions_under_federal): self
    {
        $this->context_convictions_under_federal = $context_convictions_under_federal;

        return $this;
    }

    public function getFederalSanstionsLimitations(): ?bool
    {
        return $this->federal_sanstions_limitations;
    }

    public function setFederalSanstionsLimitations(?bool $federal_sanstions_limitations): self
    {
        $this->federal_sanstions_limitations = $federal_sanstions_limitations;

        return $this;
    }

    public function getContectFederalSanstionsLimitations(): ?string
    {
        return $this->contect_federal_sanstions_limitations;
    }

    public function setContectFederalSanstionsLimitations(?string $contect_federal_sanstions_limitations): self
    {
        $this->contect_federal_sanstions_limitations = $contect_federal_sanstions_limitations;

        return $this;
    }


    /**
     * Add addional Service
     *
     * @param \App\Entity\AdditionalService $additional_service
     *
     * @return FacilityApp
     */
    public function addAdditionalService(\App\Entity\AdditionalService $additional_service)
    {
        $this->additional_services[] = $additional_service;

        return $this;
    }

    /**
     * Remove additional service
     *
     * @param \App\Entity\AdditionalService $additional_service
     */
    public function removeAdditionalService(\App\Entity\AdditionalService $additional_service)
    {
        $this->additional_services->removeElement($additional_service);
    }

    /**
     * Get  additional_services
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAdditionalServices()
    {
        return $this->additional_services;
    }

    /**
     * @return mixed
     */
    public function getFacilityType()
    {
        return $this->facility_type;
    }

    /**
     * @param mixed $facility_type
     */
    public function setFacilityType($facility_type): void
    {
        $this->facility_type = $facility_type;
    }

    /**
     * @return mixed
     */
    public function getLegalType()
    {
        return $this->legal_type;
    }

    /**
     * @param mixed $legal_type
     */
    public function setLegalType($legal_type): void
    {
        $this->legal_type = $legal_type;
    }

    /**
     * @return mixed
     */
    public function getLicensingState()
    {
        return $this->licensing_state;
    }

    /**
     * @param mixed $licensing_state
     */
    public function setLicensingState($licensing_state): void
    {
        $this->licensing_state = $licensing_state;
    }

    /**
     * @return mixed
     */
    public function getOtherAdditionalService()
    {
        return $this->other_additional_service;
    }

    /**
     * @param mixed $other_additional_service
     */
    public function setOtherAdditionalService($other_additional_service): void
    {
        $this->other_additional_service = $other_additional_service;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    public function getCreatedOn(): ?\DateTimeInterface
    {
        return $this->created_on;
    }

    public function setCreatedOn(?\DateTimeInterface $created_on): self
    {
        $this->created_on = $created_on;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreateOn() {
        $this->created_on = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedOn()
    {
        return $this->updated_on;
    }

    /**
     * @param mixed $updated_on
     */
    public function setUpdatedOn(): void
    {
        $this->updated_on = new \DateTime();
    }

    public function getDateSignature(): ?string
    {
        return $this->date_signature;
    }

    public function setDateSignature(?string $date_signature): self
    {
        $this->date_signature = $date_signature;

        return $this;
    }

    public function getAuthorizedAgentFirstName(): ?string
    {
        return $this->authorized_agent_first_name;
    }

    public function setAuthorizedAgentFirstName(?string $authorized_agent_first_name): self
    {
        $this->authorized_agent_first_name = $authorized_agent_first_name;

        return $this;
    }

    public function getAuthorizedAgentLastName(): ?string
    {
        return $this->authorized_agent_last_name;
    }

    public function setAuthorizedAgentLastName(?string $authorized_agent_last_name): self
    {
        $this->authorized_agent_last_name = $authorized_agent_last_name;

        return $this;
    }

}
