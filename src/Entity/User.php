<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class User implements UserInterface , \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    protected $email;

    /**
     * @ORM\Column(type="json")
     */
    protected $roles = [];

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ChangeLog", mappedBy="user")
     */
    private $actionlog;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DirectoryUpdate", mappedBy="user")
     */
    protected $directory_update;

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $accountNonExpired;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $credentialsNonExpired;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $accountNonLocked;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    protected $enabled;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Role")
     * @ORM\JoinTable(name="user_roles",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    protected $user_roles;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $last_name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization", inversedBy="user")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $organization;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Organization")
     * @ORM\JoinTable(name="user_organization",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="organization_id", referencedColumnName="id")}
     * )
     */
    protected $organizations;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @OneToMany(targetEntity="App\Entity\ProviderToVerify", mappedBy="user")
     */
    private $provider_to_verify;

    /**
     * @OneToMany(targetEntity="App\Entity\UserOrgRequest", mappedBy="user")
     */
    private $user_organization_request;

    /**
     * @OneToMany(targetEntity="App\Entity\UserOptions", mappedBy="user")
     */
    private $user_options;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @OneToMany(targetEntity="App\Entity\ProspectTicket", mappedBy="user")
     */
    private $tickets;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reset_code_hass;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastActivityAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedLastLogin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->username;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        // guarantee every user at least has ROLE_USER
        $rolesList=$this->getUserRoles();
        $roles=array();

        if($rolesList!==null or $rolesList!==""){
            foreach ($rolesList as $rol){
                $roles[]=$rol->getName();
            }
        }

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAccountNonExpired(): ?bool
    {
        return $this->accountNonExpired;
    }

    public function setAccountNonExpired(?bool $accountNonExpired): self
    {
        $this->accountNonExpired = $accountNonExpired;

        return $this;
    }

    public function getCredentialsNonExpired(): ?bool
    {
        return $this->credentialsNonExpired;
    }

    public function setCredentialsNonExpired(?bool $credentialsNonExpired): self
    {
        $this->credentialsNonExpired = $credentialsNonExpired;

        return $this;
    }

    public function getAccountNonLocked(): ?bool
    {
        return $this->accountNonLocked;
    }

    public function setAccountNonLocked(?bool $accountNonLocked): self
    {
        $this->accountNonLocked = $accountNonLocked;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(?bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function serialize()
    {
      return serialize([
          $this->id,
          $this->username,
          $this->email,
          $this->password
      ]);
    }

    public function unserialize($string)
    {
       list($this->id,
           $this->username,
           $this->email,
           $this->password)=unserialize($string,
           ['allowed_classes'=>false]);
    }

    /**
     * Add user_roles
     * @param \App\Entity\Role $userRoles
     */
    public function addRole(Role $userRoles) {
        $this->user_roles[] = $userRoles;
    }

    /**
     * Get user_roles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserRoles() {
        return $this->user_roles;
    }

    public function setUserRoles($user_roles) {
        $this->user_roles = $user_roles;
    }

    /**
     * Remove user_roles
     *
     * @param \App\Entity\Role $role
     */
    public function removeRole(\App\Entity\Role $role)
    {
        $this->user_roles->removeElement($role);
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(?string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     */
    public function setOrganization($organization): void
    {
        $this->organization = $organization;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed
     */
    public function setUpdatedAt(): void
    {
        $this->updated_at =  new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getResetCodeHass()
    {
        return $this->reset_code_hass;
    }

    /**
     * @param mixed $reset_code_hass
     */
    public function setResetCodeHass($reset_code_hass): void
    {
        $this->reset_code_hass = $reset_code_hass;
    }

    /**
     * Add organization
     * @param \App\Entity\Organization $organization
     */
    public function addOrganization(Organization $organization) {
        $this->organizations[] = $organization;
    }

    /**
     * Get organizations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrganizations() {
        return $this->organizations;
    }

    /**
     * Remove organization
     *
     * @param \App\Entity\Organization $organization
     */
    public function removeOrganization(\App\Entity\Organization $organization)
    {
        $this->organizations->removeElement($organization);
    }

    /**
     * @return Bool Whether the user is active or not
     */
    public function isActiveNow()
    {
        // Delay during wich the user will be considered as still active
        $delay = new \DateTime('2 minutes ago');

        return ( $this->getLastActivityAt() > $delay );
    }

    public function getLastActivityAt(): ?\DateTimeInterface
    {
        return $this->lastActivityAt;
    }

    public function setLastActivityAt(?\DateTimeInterface $lastActivityAt): self
    {
        $this->lastActivityAt = $lastActivityAt;

        return $this;
    }

    public function getUpdatedLastLogin(): ?\DateTimeInterface
    {
        return $this->updatedLastLogin;
    }

    public function setUpdatedLastLogin(?\DateTimeInterface $updatedLastLogin): self
    {
        $this->updatedLastLogin = $updatedLastLogin;

        return $this;
    }
}
