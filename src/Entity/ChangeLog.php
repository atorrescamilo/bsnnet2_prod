<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ChangeLogRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ChangeLog{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $note;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $entity_class;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $entity_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="actionlog")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ActionLog", inversedBy="actionlog")
     * @ORM\JoinColumn(name="action_id", referencedColumnName="id", nullable=true)
     */
    protected $action;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $source;

    /**
     * @ORM\PrePersist
     */
    public function setCreateOn() {
        $this->date = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(?string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getEntityClass(): ?string
    {
        return $this->entity_class;
    }

    public function setEntityClass(?string $entity_class): self
    {
        $this->entity_class = $entity_class;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser(){
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void{
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getAction(){
        return $this->action;
    }

    /**
     * @param mixed $action
     */
    public function setAction($action): void{
        $this->action = $action;
    }

    /**
     * @return mixed
     */
    public function getEntityId(){
        return $this->entity_id;
    }

    /**
     * @param mixed $entity_id
     */
    public function setEntityId($entity_id): void{
        $this->entity_id = $entity_id;
    }

    /**
     * @return mixed
     */
    public function getSource(){
        return $this->source;
    }

    /**
     * @param mixed $source
     */
    public function setSource($source): void{
        $this->source = $source;
    }
}
