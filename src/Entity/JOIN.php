<?php

namespace App\Entity;

use App\Repository\JOINRepository;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=JOINRepository::class)
 * @ORM\Table(name="`join`")
 */
class JOIN
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
    */
    private $name;

    /**
     * @ORM\Column(type="string", length=11, nullable=false)
     * @Assert\NotBlank()
     * @Assert\Unique()
     */
    private $npi_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $contact_name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $fqhc;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $cmhc;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $rural_health_center;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $other_state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $suite_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $county;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $phone_number;

    /**
     * JOIN can have many counties
     * @ManyToMany(targetEntity="App\Entity\CountyMatch")
     * @JoinTable(name="join_county",
     *      joinColumns={@JoinColumn(name="join_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="county_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $counties;

    /**
     * JOIN can have many languages
     * @ManyToMany(targetEntity="App\Entity\Languages")
     * @JoinTable(name="join_language",
     *      joinColumns={@JoinColumn(name="join_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="language_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $languages;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\JoinStatus", inversedBy="join")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $contact_email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $nppes_data;

    /**
     * @OneToMany(targetEntity="App\Entity\JoinFile", mappedBy="join")
     */
    private $join_file;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $other_language;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = $this->created_at;
    }

    /**
     * @return mixed
     */
    public function getJoinFile()
    {
        return $this->join_file;
    }

    /**
     * @param mixed $join_file
     */
    public function setJoinFile($join_file): void
    {
        $this->join_file = $join_file;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNpiNumber(): ?string
    {
        return $this->npi_number;
    }

    public function setNpiNumber(?string $npi_number): self
    {
        $this->npi_number = $npi_number;

        return $this;
    }

    public function getContactName(): ?string
    {
        return $this->contact_name;
    }

    public function setContactName(?string $contact_name): self
    {
        $this->contact_name = $contact_name;

        return $this;
    }

    public function getFqhc(): ?bool
    {
        return $this->fqhc;
    }

    public function setFqhc(?bool $fqhc): self
    {
        $this->fqhc = $fqhc;

        return $this;
    }

    public function getCmhc(): ?bool
    {
        return $this->cmhc;
    }

    public function setCmhc(?bool $cmhc): self
    {
        $this->cmhc = $cmhc;

        return $this;
    }

    public function getRuralHealthCenter(): ?bool
    {
        return $this->rural_health_center;
    }

    public function setRuralHealthCenter(?bool $rural_health_center): self
    {
        $this->rural_health_center = $rural_health_center;

        return $this;
    }

    public function getOtherState(): ?bool
    {
        return $this->other_state;
    }

    public function setOtherState(?bool $other_state): self
    {
        $this->other_state = $other_state;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getSuiteNumber(): ?string
    {
        return $this->suite_number;
    }

    public function setSuiteNumber(?string $suite_number): self
    {
        $this->suite_number = $suite_number;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCounty(): ?string
    {
        return $this->county;
    }

    public function setCounty(?string $county): self
    {
        $this->county = $county;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(?string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    /**
     * Add county
     *
     * @param \App\Entity\CountyMatch $county
     *
     * @return JOIN
     */
    public function addCounty(\App\Entity\CountyMatch $county)
    {
        $this->counties[] = $county;
        return $this;
    }

    /**
     * Remove county
     *
     * @param \App\Entity\CountyMatch $county
     */
    public function removeCounty(\App\Entity\CountyMatch $county)
    {
        $this->counties->removeElement($county);
    }

    /**
     * Get county
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCounties()
    {
        return $this->counties;
    }

    public function getContactEmail(): ?string
    {
        return $this->contact_email;
    }

    public function setContactEmail(?string $contact_email): self
    {
        $this->contact_email = $contact_email;

        return $this;
    }

    public function getNppesData(): ?string
    {
        return $this->nppes_data;
    }

    public function setNppesData(?string $nppes_data): self
    {
        $this->nppes_data = $nppes_data;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->created_at;
    }

    /**
     * @param \DateTime $created_at
     */
    public function setCreatedAt(\DateTime $created_at): void
    {
        $this->created_at = $created_at;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updated_at;
    }

    /**
     * @param \DateTime $updated_at
     */
    public function setUpdatedAt(\DateTime $updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    /**
     * Add county
     *
     * @param \App\Entity\Languages $language
     *
     * @return JOIN
     */
    public function addLanguage(\App\Entity\Languages $language)
    {
        $this->languages[] = $language;
        return $this;
    }

    /**
     * Remove county
     *
     * @param \App\Entity\Languages $language
     */
    public function removelanguage(\App\Entity\Languages $language)
    {
        $this->languages->removeElement($language);
    }

    /**
     * Get county
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    public function getOtherLanguage(): ?string
    {
        return $this->other_language;
    }

    public function setOtherLanguage(?string $other_language): self
    {
        $this->other_language = $other_language;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }
}
