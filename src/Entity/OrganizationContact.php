<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrganizationContactRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class OrganizationContact
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ContactTitle", inversedBy="organization_contact")
     * @ORM\JoinColumn(name="organization_contact_title_id", referencedColumnName="id", nullable=true, onDelete="SET NULL" )
     */
    protected $title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title_str;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Address;

    /**
     * @OneToMany(targetEntity="App\Entity\UserOrgRequest", mappedBy="contact")
     */
    private $user_organization_request;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone_ext;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mobile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $email;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization", inversedBy="contacts")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $organization;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EmailQueued", mappedBy="contact")
     */
    protected $email_queued;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EmailLog", mappedBy="contact")
     */
    protected $email_log;

    /**
     * Providers can have different specialty.
     * @ManyToMany(targetEntity="App\Entity\ContactPBestChoice")
     * @JoinTable(name="contact_best_option_choice",
     *      joinColumns={@JoinColumn(name="contact_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@JoinColumn(name="best_choice_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     *  @Assert\NotNull(message="You must select at least 1 choice.")
     *  @Assert\Count(
     *      min = "1",
     *      minMessage = "You must select at least 1 choice."
     * )
     */
    protected $best_choices;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zip_code;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPhoneExt(): ?string
    {
        return $this->phone_ext;
    }

    public function setPhoneExt(?string $phone_ext): self
    {
        $this->phone_ext = $phone_ext;

        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(?string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(?string $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     */
    public function setOrganization($organization): void
    {
        $this->organization = $organization;
    }

    /**
     * Add best_choice
     *
     * @param \App\Entity\ContactPBestChoice $best_choice
     *
     * @return ContactP
     */
    public function addBestChoice(\App\Entity\ContactPBestChoice $best_choice)
    {
        $this->best_choices[] = $best_choice;

        return $this;
    }

    /**
     * Remove best_choice
     *
     * @param \App\Entity\ContactPBestChoice $best_choice
     */
    public function removeBestChoice(\App\Entity\ContactPBestChoice $best_choice){
        $this->best_choices->removeElement($best_choice);
    }

    /**
     * Get best_choice
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBestChoices()
    {
        return $this->best_choices;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->Address;
    }

    /**
     * @param mixed $Address
     */
    public function setAddress($Address): void
    {
        $this->Address = $Address;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state): void
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getZipCode()
    {
        return $this->zip_code;
    }

    /**
     * @param mixed $zip_code
     */
    public function setZipCode($zip_code): void
    {
        $this->zip_code = $zip_code;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getTitleStr()
    {
        return $this->title_str;
    }

    /**
     * @param mixed $title_str
     */
    public function setTitleStr($title_str): void
    {
        $this->title_str = $title_str;
    }
}
