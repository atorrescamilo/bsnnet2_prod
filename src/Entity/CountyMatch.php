<?php

namespace App\Entity;

use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CountyMatchRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CountyMatch
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    public function __toString() {
        return $this->name;
    }

    public function getDisplayName() {
        return $this->name;
    }

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $county_code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PayerLineCounty", mappedBy="county")
     */
    private $payer_line_county;

    /**
     * @OneToMany(targetEntity="App\Entity\BillingAddress", mappedBy="county_match")
     */
    private $billing_address;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PayerCustomerCounty", mappedBy="county")
     */
    private $payercustomer;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $zone;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCountyCode(): ?string
    {
        return $this->county_code;
    }

    public function setCountyCode(string $county_code): self
    {
        $this->county_code = $county_code;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    public function getZone(): ?string
    {
        return $this->zone;
    }

    public function setZone(?string $zone): self
    {
        $this->zone = $zone;

        return $this;
    }
}
