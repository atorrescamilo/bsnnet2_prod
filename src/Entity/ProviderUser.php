<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProviderUserRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ProviderUser implements UserInterface , \Serializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $username;

    /**
     * @ORM\Column(type="json")
     */
    protected $roles = [];

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $last_name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $accountNonExpired;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $credentialsNonExpired;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $accountNonLocked;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $enabled;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\ProviderRole")
     * @ORM\JoinTable(name="provideruser_roles",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    protected $user_roles;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $createddate;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FacilityApp", mappedBy="provider_user")
     */
    private $facilityApp;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProviderP", mappedBy="user")
     */
    private $providers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FacilityAppLocation", mappedBy="user")
     */
    private $facility_app_location;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BillingAddressP", mappedBy="user")
     */
    private $billing_address_p;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ContactP", mappedBy="user")
     */
    private $contact_p;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrganizationP", mappedBy="provider_user")
     */
    private $organizationp;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Organization", mappedBy="provider_user")
     */
    private $organization;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $organization_name;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(?string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(?string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getAccountNonExpired(): ?bool
    {
        return $this->accountNonExpired;
    }

    public function setAccountNonExpired(?bool $accountNonExpired): self
    {
        $this->accountNonExpired = $accountNonExpired;

        return $this;
    }

    public function getCredentialsNonExpired(): ?bool
    {
        return $this->credentialsNonExpired;
    }

    public function setCredentialsNonExpired(?bool $credentialsNonExpired): self
    {
        $this->credentialsNonExpired = $credentialsNonExpired;

        return $this;
    }

    public function getAccountNonLocked(): ?bool
    {
        return $this->accountNonLocked;
    }

    public function setAccountNonLocked(?bool $accountNonLocked): self
    {
        $this->accountNonLocked = $accountNonLocked;

        return $this;
    }

    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    public function setEnabled(?bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @see UserInterface
    */
    public  function getRoles(): array {
        // guarantee every user at least has ROLE_USER
        $rolesList=$this->getUserRoles();
        $roles=array();

        if($rolesList!==null or $rolesList!==""){
            foreach ($rolesList as $rol){
                $roles[]=$rol->getName();
            }
        }

        return array_unique($roles);
    }

    public function serialize()
    {
        return serialize([
            $this->id,
            $this->username,
            $this->email,
            $this->password
        ]);
    }

    public function unserialize($string)
    {
        list($this->id,
            $this->username,
            $this->email,
            $this->password)=unserialize($string,
            ['allowed_classes'=>false]);
    }

    /**
     * Add user_roles
     * @param \App\Entity\ProviderRole $userRoles
     */
    public function addRole(ProviderRole $userRoles) {
        $this->user_roles[] = $userRoles;
    }

    /**
     * Get user_roles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserRoles() {
        return $this->user_roles;
    }

    public function setUserRoles($user_roles) {
        $this->user_roles = $user_roles;
    }

    /**
     * Remove user_roles
     *
     * @param \App\Entity\ProviderRole $role
     */
    public function removeRole(\App\Entity\ProviderRole $role)
    {
        $this->user_roles->removeElement($role);
    }

    public function getOrganizationName(): ?string
    {
        return $this->organization_name;
    }

    public function setOrganizationName(?string $organization_name): self
    {
        $this->organization_name = $organization_name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCeateddate()
    {
        return $this->ceateddate;
    }

    /**
     * @param mixed $ceateddate
     */
    public function setCeateddate($ceateddate): void
    {
        $this->ceateddate = $ceateddate;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreateOn() {
        $this->createddate = new \DateTime();
    }

}
