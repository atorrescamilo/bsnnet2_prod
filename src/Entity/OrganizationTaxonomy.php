<?php

namespace App\Entity;

use App\Repository\OrganizationTaxonomyRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinTable;

/**
 * @ORM\Entity(repositoryClass=OrganizationTaxonomyRepository::class)
 */
class OrganizationTaxonomy
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_primary;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $licence;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization", inversedBy="organization_taxonomy")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $organization;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TaxonomyCode", inversedBy="organization_taxonomy")
     * @ORM\JoinColumn(name="taxonomy_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $taxonomy;

    public function __toString(){
        return $this->getTaxonomy()->__toString();
    }

    public function getDisplayName(){
        return  $this->getTaxonomy()->__toString();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsPrimary(): ?bool
    {
        return $this->is_primary;
    }

    public function setIsPrimary(bool $is_primary): self
    {
        $this->is_primary = $is_primary;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getLicence(): ?string
    {
        return $this->licence;
    }

    public function setLicence(?string $licence): self
    {
        $this->licence = $licence;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     */
    public function setOrganization($organization): void
    {
        $this->organization = $organization;
    }

    /**
     * @return mixed
     */
    public function getTaxonomy()
    {
        return $this->taxonomy;
    }

    /**
     * @param mixed $taxonomy
     */
    public function setTaxonomy($taxonomy): void
    {
        $this->taxonomy = $taxonomy;
    }



}
