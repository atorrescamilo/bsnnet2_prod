<?php

namespace App\Entity;

use App\Repository\TaxonomyPMRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=TaxonomyPMRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class TaxonomyPM
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $specialty1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $specialty2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $specialty3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $specialty4;

    /**
     * @ORM\Column(type="string", length=25)
     * @Assert\NotBlank()
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $grouping;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $classification;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $specialization;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $definition;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSpecialty1(): ?string
    {
        return $this->specialty1;
    }

    public function setSpecialty1(?string $specialty1): self
    {
        $this->specialty1 = $specialty1;

        return $this;
    }

    public function getSpecialty2(): ?string
    {
        return $this->specialty2;
    }

    public function setSpecialty2(?string $specialty2): self
    {
        $this->specialty2 = $specialty2;

        return $this;
    }

    public function getSpecialty3(): ?string
    {
        return $this->specialty3;
    }

    public function setSpecialty3(?string $specialty3): self
    {
        $this->specialty3 = $specialty3;

        return $this;
    }

    public function getSpecialty4(): ?string
    {
        return $this->specialty4;
    }

    public function setSpecialty4(?string $specialty4): self
    {
        $this->specialty4 = $specialty4;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getGrouping(): ?string
    {
        return $this->grouping;
    }

    public function setGrouping(?string $grouping): self
    {
        $this->grouping = $grouping;

        return $this;
    }

    public function getClassification(): ?string
    {
        return $this->classification;
    }

    public function setClassification(?string $classification): self
    {
        $this->classification = $classification;

        return $this;
    }

    public function getSpecialization(): ?string
    {
        return $this->specialization;
    }

    public function setSpecialization(?string $specialization): self
    {
        $this->specialization = $specialization;

        return $this;
    }

    public function getDefinition(): ?string
    {
        return $this->definition;
    }

    public function setDefinition(?string $definition): self
    {
        $this->definition = $definition;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }
}
