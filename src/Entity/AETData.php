<?php

namespace App\Entity;

use App\Repository\AETDataRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AETDataRepository::class)
 */
class AETData
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $npi;

    /**
     * @ORM\Column(type="json")
     */
    private $data = [];

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $provider_id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $status;

    /**
     * @ORM\Column(type="integer",nullable=true)
     */
    private $track_number_type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNpi(): ?string
    {
        return $this->npi;
    }

    public function setNpi(string $npi): self
    {
        $this->npi = $npi;

        return $this;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getProviderId(): ?int
    {
        return $this->provider_id;
    }

    public function setProviderId(?int $provider_id): self
    {
        $this->provider_id = $provider_id;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getTrackNumberType(): ?int
    {
        return $this->track_number_type;
    }

    public function setTrackNumberType(int $track_number_type): self
    {
        $this->track_number_type = $track_number_type;

        return $this;
    }
}
