<?php

namespace App\Entity;

use App\Repository\AetFHKCMedicaidLogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AetFHKCMedicaidLogRepository::class)
 */
class AetFHKCMedicaidLog
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $unique_record_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $action_type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $plan_type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $plan_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $npi;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $taxonomy_code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $provider_directory;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sufix;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $license_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $accesible;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $age_limitations;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $language1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $language2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $language3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $practice_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hospital_privileges;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $specialty1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $specialty2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $specialty3;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $specialty4;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $board_certified;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $suite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zip_code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hours_operation_monday;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hours_operation_tuesday;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hours_operation_wednesday;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hours_operation_thursday;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hours_operation_friday;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hours_operation_saturday;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hours_operation_sunday;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUniqueRecordId(): ?string
    {
        return $this->unique_record_id;
    }

    public function setUniqueRecordId(?string $unique_record_id): self
    {
        $this->unique_record_id = $unique_record_id;

        return $this;
    }

    public function getActionType(): ?string
    {
        return $this->action_type;
    }

    public function setActionType(?string $action_type): self
    {
        $this->action_type = $action_type;

        return $this;
    }

    public function getPlanType(): ?string
    {
        return $this->plan_type;
    }

    public function setPlanType(?string $plan_type): self
    {
        $this->plan_type = $plan_type;

        return $this;
    }

    public function getPlanName(): ?string
    {
        return $this->plan_name;
    }

    public function setPlanName(?string $plan_name): self
    {
        $this->plan_name = $plan_name;

        return $this;
    }

    public function getNpi(): ?string
    {
        return $this->npi;
    }

    public function setNpi(?string $npi): self
    {
        $this->npi = $npi;

        return $this;
    }

    public function getTaxonomyCode(): ?string
    {
        return $this->taxonomy_code;
    }

    public function setTaxonomyCode(?string $taxonomy_code): self
    {
        $this->taxonomy_code = $taxonomy_code;

        return $this;
    }

    public function getProviderDirectory(): ?string
    {
        return $this->provider_directory;
    }

    public function setProviderDirectory(?string $provider_directory): self
    {
        $this->provider_directory = $provider_directory;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(?string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(?string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getSufix(): ?string
    {
        return $this->sufix;
    }

    public function setSufix(?string $sufix): self
    {
        $this->sufix = $sufix;

        return $this;
    }

    public function getLicenseNumber(): ?string
    {
        return $this->license_number;
    }

    public function setLicenseNumber(?string $license_number): self
    {
        $this->license_number = $license_number;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getAccesible(): ?string
    {
        return $this->accesible;
    }

    public function setAccesible(?string $accesible): self
    {
        $this->accesible = $accesible;

        return $this;
    }

    public function getAgeLimitations(): ?string
    {
        return $this->age_limitations;
    }

    public function setAgeLimitations(?string $age_limitations): self
    {
        $this->age_limitations = $age_limitations;

        return $this;
    }

    public function getLanguage1(): ?string
    {
        return $this->language1;
    }

    public function setLanguage1(?string $language1): self
    {
        $this->language1 = $language1;

        return $this;
    }

    public function getLanguage2(): ?string
    {
        return $this->language2;
    }

    public function setLanguage2(?string $language2): self
    {
        $this->language2 = $language2;

        return $this;
    }

    public function getLanguage3(): ?string
    {
        return $this->language3;
    }

    public function setLanguage3(?string $language3): self
    {
        $this->language3 = $language3;

        return $this;
    }

    public function getPracticeName(): ?string
    {
        return $this->practice_name;
    }

    public function setPracticeName(?string $practice_name): self
    {
        $this->practice_name = $practice_name;

        return $this;
    }

    public function getHospitalPrivileges(): ?string
    {
        return $this->hospital_privileges;
    }

    public function setHospitalPrivileges(?string $hospital_privileges): self
    {
        $this->hospital_privileges = $hospital_privileges;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getSpecialty1(): ?string
    {
        return $this->specialty1;
    }

    public function setSpecialty1(?string $specialty1): self
    {
        $this->specialty1 = $specialty1;

        return $this;
    }

    public function getSpecialty2(): ?string
    {
        return $this->specialty2;
    }

    public function setSpecialty2(?string $specialty2): self
    {
        $this->specialty2 = $specialty2;

        return $this;
    }

    public function getSpecialty3(): ?string
    {
        return $this->specialty3;
    }

    public function setSpecialty3(?string $specialty3): self
    {
        $this->specialty3 = $specialty3;

        return $this;
    }

    public function getSpecialty4(): ?string
    {
        return $this->specialty4;
    }

    public function setSpecialty4(?string $specialty4): self
    {
        $this->specialty4 = $specialty4;

        return $this;
    }

    public function getBoardCertified(): ?string
    {
        return $this->board_certified;
    }

    public function setBoardCertified(?string $board_certified): self
    {
        $this->board_certified = $board_certified;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getSuite(): ?string
    {
        return $this->suite;
    }

    public function setSuite(?string $suite): self
    {
        $this->suite = $suite;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zip_code;
    }

    public function setZipCode(?string $zip_code): self
    {
        $this->zip_code = $zip_code;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(?string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getHoursOperationMonday(): ?string
    {
        return $this->hours_operation_monday;
    }

    public function setHoursOperationMonday(?string $hours_operation_monday): self
    {
        $this->hours_operation_monday = $hours_operation_monday;

        return $this;
    }

    public function getHoursOperationTuesday(): ?string
    {
        return $this->hours_operation_tuesday;
    }

    public function setHoursOperationTuesday(?string $hours_operation_tuesday): self
    {
        $this->hours_operation_tuesday = $hours_operation_tuesday;

        return $this;
    }

    public function getHoursOperationWednesday(): ?string
    {
        return $this->hours_operation_wednesday;
    }

    public function setHoursOperationWednesday(?string $hours_operation_wednesday): self
    {
        $this->hours_operation_wednesday = $hours_operation_wednesday;

        return $this;
    }

    public function getHoursOperationThursday(): ?string
    {
        return $this->hours_operation_thursday;
    }

    public function setHoursOperationThursday(?string $hours_operation_thursday): self
    {
        $this->hours_operation_thursday = $hours_operation_thursday;

        return $this;
    }

    public function getHoursOperationFriday(): ?string
    {
        return $this->hours_operation_friday;
    }

    public function setHoursOperationFriday(?string $hours_operation_friday): self
    {
        $this->hours_operation_friday = $hours_operation_friday;

        return $this;
    }

    public function getHoursOperationSaturday(): ?string
    {
        return $this->hours_operation_saturday;
    }

    public function setHoursOperationSaturday(?string $hours_operation_saturday): self
    {
        $this->hours_operation_saturday = $hours_operation_saturday;

        return $this;
    }

    public function getHoursOperationSunday(): ?string
    {
        return $this->hours_operation_sunday;
    }

    public function setHoursOperationSunday(?string $hours_operation_sunday): self
    {
        $this->hours_operation_sunday = $hours_operation_sunday;

        return $this;
    }
}
