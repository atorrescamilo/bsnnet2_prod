<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PMLRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PML
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $colA;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colB;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colC;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colD;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colF;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $colG;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colH;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colI;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colJ;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colK;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colL;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colM;


    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $colN;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colO;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colP;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colQ;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colR;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colS;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colT;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colU;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colV;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colW;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colX;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $colY;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getColA(): ?string
    {
        return $this->colA;
    }

    public function setColA(?string $colA): self
    {
        $this->colA = $colA;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getColB()
    {
        return $this->colB;
    }

    /**
     * @param mixed $colB
     */
    public function setColB($colB): void
    {
        $this->colB = $colB;
    }

    /**
     * @return mixed
     */
    public function getColC()
    {
        return $this->colC;
    }

    /**
     * @param mixed $colC
     */
    public function setColC($colC): void
    {
        $this->colC = $colC;
    }

    /**
     * @return mixed
     */
    public function getColD()
    {
        return $this->colD;
    }

    /**
     * @param mixed $colD
     */
    public function setColD($colD): void
    {
        $this->colD = $colD;
    }

    /**
     * @return mixed
     */
    public function getColE()
    {
        return $this->colE;
    }

    /**
     * @param mixed $colE
     */
    public function setColE($colE): void
    {
        $this->colE = $colE;
    }

    /**
     * @return mixed
     */
    public function getColF()
    {
        return $this->colF;
    }

    /**
     * @param mixed $colF
     */
    public function setColF($colF): void
    {
        $this->colF = $colF;
    }

    /**
     * @return mixed
     */
    public function getColG()
    {
        return $this->colG;
    }

    /**
     * @param mixed $colG
     */
    public function setColG($colG): void
    {
        $this->colG = $colG;
    }

    /**
     * @return mixed
     */
    public function getColH()
    {
        return $this->colH;
    }

    /**
     * @param mixed $colH
     */
    public function setColH($colH): void
    {
        $this->colH = $colH;
    }

    /**
     * @return mixed
     */
    public function getColI()
    {
        return $this->colI;
    }

    /**
     * @param mixed $colI
     */
    public function setColI($colI): void
    {
        $this->colI = $colI;
    }

    /**
     * @return mixed
     */
    public function getColJ()
    {
        return $this->colJ;
    }

    /**
     * @param mixed $colJ
     */
    public function setColJ($colJ): void
    {
        $this->colJ = $colJ;
    }

    /**
     * @return mixed
     */
    public function getColK()
    {
        return $this->colK;
    }

    /**
     * @param mixed $colK
     */
    public function setColK($colK): void
    {
        $this->colK = $colK;
    }

    /**
     * @return mixed
     */
    public function getColL()
    {
        return $this->colL;
    }

    /**
     * @param mixed $colL
     */
    public function setColL($colL): void
    {
        $this->colL = $colL;
    }

    /**
     * @return mixed
     */
    public function getColM()
    {
        return $this->colM;
    }

    /**
     * @param mixed $colM
     */
    public function setColM($colM): void
    {
        $this->colM = $colM;
    }

    /**
     * @return mixed
     */
    public function getColN()
    {
        return $this->colN;
    }

    /**
     * @param mixed $colN
     */
    public function setColN($colN): void
    {
        $this->colN = $colN;
    }

    /**
     * @return mixed
     */
    public function getColO()
    {
        return $this->colO;
    }

    /**
     * @param mixed $colO
     */
    public function setColO($colO): void
    {
        $this->colO = $colO;
    }

    /**
     * @return mixed
     */
    public function getColP()
    {
        return $this->colP;
    }

    /**
     * @param mixed $colP
     */
    public function setColP($colP): void
    {
        $this->colP = $colP;
    }

    /**
     * @return mixed
     */
    public function getColQ()
    {
        return $this->colQ;
    }

    /**
     * @param mixed $colQ
     */
    public function setColQ($colQ): void
    {
        $this->colQ = $colQ;
    }

    /**
     * @return mixed
     */
    public function getColR()
    {
        return $this->colR;
    }

    /**
     * @param mixed $colR
     */
    public function setColR($colR): void
    {
        $this->colR = $colR;
    }

    /**
     * @return mixed
     */
    public function getColS()
    {
        return $this->colS;
    }

    /**
     * @param mixed $colS
     */
    public function setColS($colS): void
    {
        $this->colS = $colS;
    }

    /**
     * @return mixed
     */
    public function getColT()
    {
        return $this->colT;
    }

    /**
     * @param mixed $colT
     */
    public function setColT($colT): void
    {
        $this->colT = $colT;
    }

    /**
     * @return mixed
     */
    public function getColU()
    {
        return $this->colU;
    }

    /**
     * @param mixed $colU
     */
    public function setColU($colU): void
    {
        $this->colU = $colU;
    }

    /**
     * @return mixed
     */
    public function getColV()
    {
        return $this->colV;
    }

    /**
     * @param mixed $colV
     */
    public function setColV($colV): void
    {
        $this->colV = $colV;
    }

    /**
     * @return mixed
     */
    public function getColW()
    {
        return $this->colW;
    }

    /**
     * @param mixed $colW
     */
    public function setColW($colW): void
    {
        $this->colW = $colW;
    }

    /**
     * @return mixed
     */
    public function getColX()
    {
        return $this->colX;
    }

    /**
     * @param mixed $colX
     */
    public function setColX($colX): void
    {
        $this->colX = $colX;
    }

    /**
     * @return mixed
     */
    public function getColY()
    {
        return $this->colY;
    }

    /**
     * @param mixed $colY
     */
    public function setColY($colY): void
    {
        $this->colY = $colY;
    }

}
