<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ErrorPMLCheckRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ErrorPMLCheck
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $provider_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $provider_type_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $primary_specialty_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $organizationId;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProviderId(): ?int
    {
        return $this->provider_id;
    }

    public function setProviderId(?int $provider_id): self
    {
        $this->provider_id = $provider_id;

        return $this;
    }

    public function getProviderTypeId(): ?int
    {
        return $this->provider_type_id;
    }

    public function setProviderTypeId(?int $provider_type_id): self
    {
        $this->provider_type_id = $provider_type_id;

        return $this;
    }

    public function getPrimarySpecialtyId(): ?int
    {
        return $this->primary_specialty_id;
    }

    public function setPrimarySpecialtyId(?int $primary_specialty_id): self
    {
        $this->primary_specialty_id = $primary_specialty_id;

        return $this;
    }

    public function getOrganizationId(): ?int
    {
        return $this->organizationId;
    }

    public function setOrganizationId(?int $organizationId): self
    {
        $this->organizationId = $organizationId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

}
