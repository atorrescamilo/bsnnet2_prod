<?php

namespace App\Entity;

use App\Repository\OrganizationLineofBusinessRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass=OrganizationLineofBusinessRepository::class)
 */
class OrganizationLineofBusiness
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization", inversedBy="organization_lob")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $organization;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\LineOfBusiness", inversedBy="organization_lob")
     * @ORM\JoinColumn(name="line_of_business_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $line_of_business;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $value;

    /**
     * OrganizationLineofBusiness constructor.
     * @param $id
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = $this->created_at;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     */
    public function setOrganization($organization): void
    {
        $this->organization = $organization;
    }

    /**
     * @return mixed
     */
    public function getLineOfBusiness()
    {
        return $this->line_of_business;
    }

    /**
     * @param mixed $line_of_business
     */
    public function setLineOfBusiness($line_of_business): void
    {
        $this->line_of_business = $line_of_business;
    }

    public function getValue(): ?bool
    {
        return $this->value;
    }

    public function setValue(?bool $value): self
    {
        $this->value = $value;

        return $this;
    }


}
