<?php

namespace App\Entity;

use App\Repository\AgesRangeRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=AgesRangeRepository::class)
 */
class AgesRange
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     * @Assert\Length(min=1,max=2)
     * @Assert\PositiveOrZero
     */
    private $min_age;

    public function __toString() {
        return $this->name . " (" . $this->getMinAge() . " - " . $this->getMaxAge() . ")";
    }

    public function getDisplayName() {
        return $this->name . " (" . $this->getMinAge() . " - " . $this->getMaxAge() . ")";
    }

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank()
     * @Assert\PositiveOrZero
     */
    private $max_age;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMinAge(): ?int
    {
        return $this->min_age;
    }

    public function setMinAge(int $min_age): self
    {
        $this->min_age = $min_age;

        return $this;
    }

    public function getMaxAge(): ?int
    {
        return $this->max_age;
    }

    public function setMaxAge(int $max_age): self
    {
        $this->max_age = $max_age;

        return $this;
    }
}
