<?php

namespace App\Entity;

use App\Repository\FloridaHospitalRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FloridaHospitalRepository::class)
 */
class FloridaHospital
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $ahca_number;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $license_number;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $license_effective_date;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $license_expiration_date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $suite_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $zip_code;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $county;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mailing_street;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $mailing_suite_number;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $mailing_city;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $mailing_state;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $mailing_zip_code;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $mailing_county;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $license_status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $owner;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $owner_since;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $beds;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $profit_status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $web;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAhcaNumber(): ?string
    {
        return $this->ahca_number;
    }

    public function setAhcaNumber(string $ahca_number): self
    {
        $this->ahca_number = $ahca_number;

        return $this;
    }

    public function getLicenseNumber(): ?string
    {
        return $this->license_number;
    }

    public function setLicenseNumber(string $license_number): self
    {
        $this->license_number = $license_number;

        return $this;
    }

    public function getLicenseEffectiveDate(): ?string
    {
        return $this->license_effective_date;
    }

    public function setLicenseEffectiveDate(?string $license_effective_date): self
    {
        $this->license_effective_date = $license_effective_date;

        return $this;
    }

    public function getLicenseExpirationDate(): ?string
    {
        return $this->license_expiration_date;
    }

    public function setLicenseExpirationDate(?string $license_expiration_date): self
    {
        $this->license_expiration_date = $license_expiration_date;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getSuiteNumber(): ?string
    {
        return $this->suite_number;
    }

    public function setSuiteNumber(?string $suite_number): self
    {
        $this->suite_number = $suite_number;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zip_code;
    }

    public function setZipCode(?string $zip_code): self
    {
        $this->zip_code = $zip_code;

        return $this;
    }

    public function getCounty(): ?string
    {
        return $this->county;
    }

    public function setCounty(?string $county): self
    {
        $this->county = $county;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getMailingStreet(): ?string
    {
        return $this->mailing_street;
    }

    public function setMailingStreet(?string $mailing_street): self
    {
        $this->mailing_street = $mailing_street;

        return $this;
    }

    public function getMailingSuiteNumber(): ?string
    {
        return $this->mailing_suite_number;
    }

    public function setMailingSuiteNumber(?string $mailing_suite_number): self
    {
        $this->mailing_suite_number = $mailing_suite_number;

        return $this;
    }

    public function getMailingCity(): ?string
    {
        return $this->mailing_city;
    }

    public function setMailingCity(?string $mailing_city): self
    {
        $this->mailing_city = $mailing_city;

        return $this;
    }

    public function getMailingState(): ?string
    {
        return $this->mailing_state;
    }

    public function setMailingState(?string $mailing_state): self
    {
        $this->mailing_state = $mailing_state;

        return $this;
    }

    public function getMailingZipCode(): ?string
    {
        return $this->mailing_zip_code;
    }

    public function setMailingZipCode(?string $mailing_zip_code): self
    {
        $this->mailing_zip_code = $mailing_zip_code;

        return $this;
    }

    public function getMailingCounty(): ?string
    {
        return $this->mailing_county;
    }

    public function setMailingCounty(?string $mailing_county): self
    {
        $this->mailing_county = $mailing_county;

        return $this;
    }

    public function getLicenseStatus(): ?string
    {
        return $this->license_status;
    }

    public function setLicenseStatus(?string $license_status): self
    {
        $this->license_status = $license_status;

        return $this;
    }

    public function getOwner(): ?string
    {
        return $this->owner;
    }

    public function setOwner(?string $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getOwnerSince(): ?string
    {
        return $this->owner_since;
    }

    public function setOwnerSince(?string $owner_since): self
    {
        $this->owner_since = $owner_since;

        return $this;
    }

    public function getBeds(): ?string
    {
        return $this->beds;
    }

    public function setBeds(?string $beds): self
    {
        $this->beds = $beds;

        return $this;
    }

    public function getProfitStatus(): ?string
    {
        return $this->profit_status;
    }

    public function setProfitStatus(?string $profit_status): self
    {
        $this->profit_status = $profit_status;

        return $this;
    }

    public function getWeb(): ?string
    {
        return $this->web;
    }

    public function setWeb(?string $web): self
    {
        $this->web = $web;

        return $this;
    }
}
