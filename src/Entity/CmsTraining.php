<?php

namespace App\Entity;

use App\Repository\CmsTrainingRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass=CmsTrainingRepository::class)
 */
class CmsTraining
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * CMS Training can have different Payers.
     * @ManyToMany(targetEntity="App\Entity\Payer")
     * @JoinTable(name="cms_training_payer",
     *      joinColumns={@JoinColumn(name="cms_training_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="payer_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $payers;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Add payer
     *
     * @param \App\Entity\Payer $payer
     *
     * @return CmsTraining
     */
    public function addPayer(\App\Entity\Payer $payer)
    {
        $this->payers[] = $payer;

        return $this;
    }

    /**
     * Remove payer
     *
     * @param \App\Entity\Payer $payer
     */
    public function removePayer(\App\Entity\Payer $payer)
    {
        $this->payers->removeElement($payer);
    }

    /**
     * Get payers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPayers()
    {
        return $this->payers;
    }
}
