<?php

namespace App\Entity;

use App\Repository\AddressFacilityLicenseRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass=AddressFacilityLicenseRepository::class)
 */
class AddressFacilityLicense
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $ahca_number;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $license_number;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $effective_date;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $expiration_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facility_type;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $beds;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BillingAddress", inversedBy="address_facility_license")
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $address;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAhcaNumber(): ?string
    {
        return $this->ahca_number;
    }

    public function setAhcaNumber(?string $ahca_number): self
    {
        $this->ahca_number = $ahca_number;

        return $this;
    }

    public function getLicenseNumber(): ?string
    {
        return $this->license_number;
    }

    public function setLicenseNumber(?string $license_number): self
    {
        $this->license_number = $license_number;

        return $this;
    }

    public function getEffectiveDate(): ?string
    {
        return $this->effective_date;
    }

    public function setEffectiveDate(?string $effective_date): self
    {
        $this->effective_date = $effective_date;

        return $this;
    }

    public function getExpirationDate(): ?string
    {
        return $this->expiration_date;
    }

    public function setExpirationDate(?string $expiration_date): self
    {
        $this->expiration_date = $expiration_date;

        return $this;
    }

    public function getFacilityType(): ?string
    {
        return $this->facility_type;
    }

    public function setFacilityType(?string $facility_type): self
    {
        $this->facility_type = $facility_type;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getBeds()
    {
        return $this->beds;
    }

    /**
     * @param mixed $beds
     */
    public function setBeds($beds): void
    {
        $this->beds = $beds;
    }

}
