<?php

namespace App\Entity;

use App\Entity\Common\AddressFields;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\CallbackValidator;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BillingAddressRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class BillingAddress
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     * @Assert\NotBlank()
     */
    private $street;

    /**
     * @ORM\Column(type="integer", length=512, nullable=true)
     */
    private $cvo_id;

    /**
     * @ORM\Column(type="integer", length=512, nullable=true)
     */
    private $payer_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $suite_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $county;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $us_state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $zipCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private $google_addr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $business_hours;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location_npi;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location_npi2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $group_medicaid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $group_medicare;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $taxonomy_code;

    /**
     * @ORM\Column(type="boolean", nullable=true,options={"default" = 0})
     * @Assert\NotNull()
     */
    private $primary_addr;

    /**
     * @ORM\Column(type="boolean", nullable=true,options={"default" = 0})
     * @Assert\NotNull()
     */
    private $see_patients_addr;

    /**
     * @ORM\Column(type="boolean", nullable=true,options={"default" = 0})
     * @Assert\NotNull()
     */
    private $mailing_addr;

    /**
     * @ORM\Column(type="boolean", nullable=true,options={"default" = 0})
     * @Assert\NotNull()
     */
    private $billing_addr;

    /**
     * @ORM\Column(type="boolean", nullable=true,options={"default" = 0})
     * @Assert\NotNull()
     */
    private $tdd;

    /**
     * @ORM\Column(type="boolean", nullable=true,options={"default" = 0})
     * @Assert\NotNull()
     */
    private $wheelchair;

    /**
     * @ORM\Column(type="boolean", nullable=true,options={"default" = 0})
     * @Assert\NotNull()
     */
    private $wheelchair_requirements;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" = 0})
     * @Assert\NotNull()
     */
    private $private_provider_transportation;

    /**
     * @ORM\Column(type="integer", nullable=true, options={"default" = 0})
     */
    private $office_age_limit;

    /**
     * @ORM\Column(type="integer", nullable=true,options={"default" = 0})
     */
    private $office_age_limit_max;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\NotNull()
     */
    private $is_facility;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $facility_number;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $facility_exemption_number;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $beds;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $kids_beds;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialingDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialingReceivedDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sentCvoDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialingCompleteDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sanctionDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $phone_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialing_application_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state_license_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $provider_data_form_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $w9_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ownership_disclosure_form_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $roster_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $general_liability_coverage_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $site_visit_survey_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $accreditation_certificate_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $loa_loi_file;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=8, nullable=true)
     */
    private $lat;

    /**
     * @ORM\Column(type="decimal", precision=10, scale=8, nullable=true)
     */
    private $lng;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $apply_group_medicaid;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $apply_group_medicare;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" = 0})
     */
    private $disabled;

    /**
     * @ORM\Column(type="boolean", nullable=true, options={"default" = 0})
     */
    private $na_beds;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization", inversedBy="billing_address")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $organization;

    /**
     * Billing Address have many facility settings
     * @ManyToMany(targetEntity="App\Entity\FacilityType")
     * @JoinTable(name="billing_address_facility_type",
     *      joinColumns={@JoinColumn(name="billing_address_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="facility_type_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $facility_type;

    /**
     * Providers can have different types.
     * @ManyToMany(targetEntity="App\Entity\AgesRange")
     * @JoinTable(name="billing_address_age_range",
     *      joinColumns={@JoinColumn(name="billing_address_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="age_range_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     * @Assert\NotBlank()
     * @Assert\Count(
     *      min = "1",
     *      minMessage = "You have to select at least 1 item"
     * )
     */
    protected $age_ranges;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CountyMatch", inversedBy="billing_address")
     * @ORM\JoinColumn(name="county_match_id", referencedColumnName="id", nullable=true, onDelete="SET NULL" )
     */
    protected $county_match;

    /**
     * INVERSE SIDE
     *
     * @ManyToMany(targetEntity="App\Entity\Provider", mappedBy="billing_address")
     */
    private $providers;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\BillingAddressCvo", mappedBy="billing_address")
     */
    private $billing_address_cvo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AddressFacilityLicense", mappedBy="address")
     */
    private $address_facility_license;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProviderAddrFCCTN", mappedBy="address")
     */
    private $addr_fcc_track;

    /**
     * Billing Address have many locations types
     * @ManyToMany(targetEntity="App\Entity\LocationType")
     * @JoinTable(name="billing_address_location_type",
     *      joinColumns={@JoinColumn(name="billing_address_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="location_type_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $location_type;


    /**
     * Billing Address have many service settings
     * @ManyToMany(targetEntity="App\Entity\ServiceSettings")
     * @JoinTable(name="billing_address_service_settings",
     *      joinColumns={@JoinColumn(name="billing_address_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="service_settings_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $service_settings;

    /**
     * provider can have different taxonomy code.
     * @ORM\ManyToMany(targetEntity="App\Entity\TaxonomyCode")
     * @ORM\JoinTable(name="billing_address_taxonomy_code",
     *      joinColumns={@ORM\JoinColumn(name="billing_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="taxonomy_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $taxonomy_codes;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tdd_number;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private $fcc_track_number;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $supportEHR;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $ahca_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $appointment;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location_name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\NotNull()
     */
    private $have_beds;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Assert\NotNull(message="Please choose an option")
     */
    private $ada_compliant;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     *  @Assert\NotNull()
     */
    private $public_transportation;

    /**
     * Get Providers
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProviders()
    {
        return $this->providers;
    }

    public function __construct(){
        $this->setOfficeAgeLimitMax(0);
        $this->setOfficeAgeLimit(0);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getSuiteNumber(): ?string
    {
        return $this->suite_number;
    }

    public function setSuiteNumber(?string $suite_number): self
    {
        $this->suite_number = $suite_number;

        return $this;
    }

    public function getCounty(): ?string
    {
        return $this->county;
    }

    public function setCounty(?string $county): self
    {
        $this->county = $county;

        return $this;
    }

    public function getRegion(): ?string
    {
        return $this->region;
    }

    public function setRegion(?string $region): self
    {
        $this->region = $region;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getUsState(): ?string
    {
        return $this->us_state;
    }

    public function setUsState(?string $us_state): self
    {
        $this->us_state = $us_state;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getGoogleAddr(): ?string
    {
        return $this->google_addr;
    }

    public function setGoogleAddr(?string $google_addr): self
    {
        $this->google_addr = $google_addr;

        return $this;
    }

    public function getBusinessHours(): ?string
    {
        return $this->business_hours;
    }

    public function setBusinessHours(?string $business_hours): self
    {
        $this->business_hours = $business_hours;

        return $this;
    }

    public function getLocationNpi(): ?string
    {
        return $this->location_npi;
    }

    public function setLocationNpi(?string $location_npi): self
    {
        $this->location_npi = $location_npi;

        return $this;
    }

    public function getLocationNpi2(): ?string
    {
        return $this->location_npi2;
    }

    public function setLocationNpi2(?string $location_npi2): self
    {
        $this->location_npi2 = $location_npi2;

        return $this;
    }

    public function getGroupMedicaid(): ?string
    {
        return $this->group_medicaid;
    }

    public function setGroupMedicaid(?string $group_medicaid): self
    {
        $this->group_medicaid = $group_medicaid;

        return $this;
    }

    public function getGroupMedicare(): ?string
    {
        return $this->group_medicare;
    }

    public function setGroupMedicare(?string $group_medicare): self
    {
        $this->group_medicare = $group_medicare;

        return $this;
    }

    public function getTaxonomyCode(): ?string
    {
        return $this->taxonomy_code;
    }

    public function setTaxonomyCode(?string $taxonomy_code): self
    {
        $this->taxonomy_code = $taxonomy_code;

        return $this;
    }

    public function getPrimaryAddr(): ?bool
    {
        return $this->primary_addr;
    }

    public function setPrimaryAddr(?bool $primary_addr): self
    {
        $this->primary_addr = $primary_addr;

        return $this;
    }

    public function getMailingAddr(): ?bool
    {
        return $this->mailing_addr;
    }

    public function setMailingAddr(?bool $mailing_addr): self
    {
        $this->mailing_addr = $mailing_addr;

        return $this;
    }

    public function getBillingAddr(): ?bool
    {
        return $this->billing_addr;
    }

    public function setBillingAddr(?bool $billing_addr): self
    {
        $this->billing_addr = $billing_addr;

        return $this;
    }

    public function getTdd(): ?bool
    {
        return $this->tdd;
    }

    public function setTdd(?bool $tdd): self
    {
        $this->tdd = $tdd;

        return $this;
    }

    public function getWheelchair(): ?bool
    {
        return $this->wheelchair;
    }

    public function setWheelchair(?bool $wheelchair): self
    {
        $this->wheelchair = $wheelchair;

        return $this;
    }

    public function getPrivateProviderTransportation(): ?bool
    {
        return $this->private_provider_transportation;
    }

    public function setPrivateProviderTransportation(?bool $private_provider_transportation): self
    {
        $this->private_provider_transportation = $private_provider_transportation;

        return $this;
    }

    public function getOfficeAgeLimit()
    {
        return $this->office_age_limit;
    }

    public function setOfficeAgeLimit($office_age_limit): self
    {
        $this->office_age_limit = $office_age_limit;

        return $this;
    }

    public function getOfficeAgeLimitMax()
    {
        return $this->office_age_limit_max;
    }

    public function setOfficeAgeLimitMax($office_age_limit_max): self
    {
        $this->office_age_limit_max = $office_age_limit_max;

        return $this;
    }

    public function getIsFacility(): ?bool
    {
        return $this->is_facility;
    }

    public function setIsFacility(?bool $is_facility): self
    {
        $this->is_facility = $is_facility;

        return $this;
    }

    public function getFacilityNumber()
    {
        return $this->facility_number;
    }

    public function setFacilityNumber($facility_number): self
    {
        $this->facility_number = $facility_number;

        return $this;
    }

    public function getFacilityExemptionNumber()
    {
        return $this->facility_exemption_number;
    }

    public function setFacilityExemptionNumber($facility_exemption_number)
    {
        $this->facility_exemption_number = $facility_exemption_number;

        return $this;
    }

    public function getBeds()
    {
        return $this->beds;
    }

    public function setBeds($beds)
    {
        $this->beds = $beds;

        return $this;
    }

    public function getCredentialingDate()
    {
        return $this->credentialingDate;
    }

    public function setCredentialingDate($credentialingDate)
    {
        $this->credentialingDate = $credentialingDate;

        return $this;
    }

    public function getCredentialingReceivedDate(): ?string
    {
        return $this->credentialingReceivedDate;
    }

    public function setCredentialingReceivedDate(?string $credentialingReceivedDate): self
    {
        $this->credentialingReceivedDate = $credentialingReceivedDate;

        return $this;
    }

    public function getSentCvoDate(): ?string
    {
        return $this->sentCvoDate;
    }

    public function setSentCvoDate(?string $sentCvoDate): self
    {
        $this->sentCvoDate = $sentCvoDate;

        return $this;
    }

    public function getCredentialingCompleteDate(): ?string
    {
        return $this->credentialingCompleteDate;
    }

    public function setCredentialingCompleteDate(?string $credentialingCompleteDate): self
    {
        $this->credentialingCompleteDate = $credentialingCompleteDate;

        return $this;
    }

    public function getSanctionDate(): ?string
    {
        return $this->sanctionDate;
    }

    public function setSanctionDate(?string $sanctionDate): self
    {
        $this->sanctionDate = $sanctionDate;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(?string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getCredentialingApplicationFile(): ?string
    {
        return $this->credentialing_application_file;
    }

    public function setCredentialingApplicationFile(?string $credentialing_application_file): self
    {
        $this->credentialing_application_file = $credentialing_application_file;

        return $this;
    }

    public function getStateLicenseFile(): ?string
    {
        return $this->state_license_file;
    }

    public function setStateLicenseFile(?string $state_license_file): self
    {
        $this->state_license_file = $state_license_file;

        return $this;
    }

    public function getProviderDataFormFile(): ?string
    {
        return $this->provider_data_form_file;
    }

    public function setProviderDataFormFile(?string $provider_data_form_file): self
    {
        $this->provider_data_form_file = $provider_data_form_file;

        return $this;
    }

    public function getW9File(): ?string
    {
        return $this->w9_file;
    }

    public function setW9File(?string $w9_file): self
    {
        $this->w9_file = $w9_file;

        return $this;
    }

    public function getOwnershipDisclosureFormFile(): ?string
    {
        return $this->ownership_disclosure_form_file;
    }

    public function setOwnershipDisclosureFormFile(?string $ownership_disclosure_form_file): self
    {
        $this->ownership_disclosure_form_file = $ownership_disclosure_form_file;

        return $this;
    }

    public function getRosterFile(): ?string
    {
        return $this->roster_file;
    }

    public function setRosterFile(?string $roster_file): self
    {
        $this->roster_file = $roster_file;

        return $this;
    }

    public function getGeneralLiabilityCoverageFile(): ?string
    {
        return $this->general_liability_coverage_file;
    }

    public function setGeneralLiabilityCoverageFile(?string $general_liability_coverage_file): self
    {
        $this->general_liability_coverage_file = $general_liability_coverage_file;

        return $this;
    }

    public function getSiteVisitSurveyFile(): ?string
    {
        return $this->site_visit_survey_file;
    }

    public function setSiteVisitSurveyFile(?string $site_visit_survey_file): self
    {
        $this->site_visit_survey_file = $site_visit_survey_file;

        return $this;
    }

    public function getAccreditationCertificateFile(): ?string
    {
        return $this->accreditation_certificate_file;
    }

    public function setAccreditationCertificateFile(?string $accreditation_certificate_file): self
    {
        $this->accreditation_certificate_file = $accreditation_certificate_file;

        return $this;
    }

    public function getLoaLoiFile(): ?string
    {
        return $this->loa_loi_file;
    }

    public function setLoaLoiFile(?string $loa_loi_file): self
    {
        $this->loa_loi_file = $loa_loi_file;

        return $this;
    }

    public function getLat()
    {
        return $this->lat;
    }

    public function setLat($lat): self
    {
        $this->lat = $lat;

        return $this;
    }

    public function getLng()
    {
        return $this->lng;
    }

    public function setLng($lng): self
    {
        $this->lng = $lng;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     */
    public function setOrganization($organization): void
    {
        $this->organization = $organization;
    }

    /**
     * Add facilityType
     *
     * @param \App\Entity\FacilityType $facilityType
     *
     * @return BillingAddress
     */
    public function addFacilityType(\App\Entity\FacilityType $facilityType)
    {
        $this->facility_type[] = $facilityType;

        return $this;
    }

    /**
     * Remove facilityType
     *
     * @param \App\Entity\FacilityType $facilityType
     */
    public function removeFacilityType(\App\Entity\FacilityType $facilityType)
    {
        $this->facility_type->removeElement($facilityType);
    }

    public function clearFacilityTypes() {
        $this->facility_type = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get facilityType
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFacilityType()
    {
        return $this->facility_type;
    }

    /**
     * Add locationType
     *
     * @param \App\Entity\LocationType $locationType
     *
     * @return BillingAddress
     */
    public function addLocationType(\App\Entity\LocationType $locationType)
    {
        $this->location_type[] = $locationType;

        return $this;
    }

    /**
     * Remove locationType
     *
     * @param \App\Entity\LocationType $locationType
     */
    public function removeLocationType(\App\Entity\LocationType $locationType)
    {
        $this->location_type->removeElement($locationType);
    }

    /**
     * Get locationType
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocationType()
    {
        return $this->location_type;
    }


    /**
     * Add serviceSetting
     *
     * @param \App\Entity\ServiceSettings $serviceSetting
     *
     * @return BillingAddress
     */
    public function addServiceSetting(\App\Entity\ServiceSettings $serviceSetting)
    {
        $this->service_settings[] = $serviceSetting;

        return $this;
    }

    /**
     * Remove serviceSetting
     *
     * @param \App\Entity\ServiceSettings $serviceSetting
     */
    public function removeServiceSetting(\App\Entity\ServiceSettings $serviceSetting)
    {
        $this->service_settings->removeElement($serviceSetting);
    }

    /**
     * Get serviceSettings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceSettings()
    {
        return $this->service_settings;
    }

    public function clearServiceSettings() {
        $this->service_settings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add taxonomy code
     *
     * @param \App\Entity\TaxonomyCode $taxonomy_code
     *
     * @return Provider
     */
    public function addTaxonomyCode(\App\Entity\TaxonomyCode $taxonomy_code)
    {
        $this->taxonomy_codes[] = $taxonomy_code;

        return $this;
    }

    /**
     * Remove taxonomy_code
     *
     * @param \App\Entity\TaxonomyCode $taxonomy_code
     */
    public function removeTaxonomyCode(\App\Entity\TaxonomyCode $taxonomy_code)
    {
        $this->taxonomy_codes->removeElement($taxonomy_code);
    }

    /**
     * Get taxonomy_codes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTaxonomyCodes()
    {
        return $this->taxonomy_codes;
    }

    /**
     * @return mixed
     */
    public function getCountyMatch()
    {
        return $this->county_match;
    }

    /**
     * @param mixed $county_match
     */
    public function setCountyMatch($county_match): void
    {
        $this->county_match = $county_match;
    }

    /**
     * @return mixed
     */
    public function getTddNumber()
    {
        return $this->tdd_number;
    }

    /**
     * @param mixed $tdd_number
     */
    public function setTddNumber($tdd_number): void
    {
        $this->tdd_number = $tdd_number;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getSeePatientsAddr()
    {
        return $this->see_patients_addr;
    }

    /**
     * @param mixed $see_patients_addr
     */
    public function setSeePatientsAddr($see_patients_addr): void
    {
        $this->see_patients_addr = $see_patients_addr;
    }

    /**
     * @return mixed
     */
    public function getFccTrackNumber()
    {
        return $this->fcc_track_number;
    }

    /**
     * @param mixed $fcc_track_number
     */
    public function setFccTrackNumber($fcc_track_number): void
    {
        $this->fcc_track_number = $fcc_track_number;
    }

    /**
     * @return mixed
     */
    public function getApplyGroupMedicaid()
    {
        return $this->apply_group_medicaid;
    }

    /**
     * @param mixed $apply_group_medicaid
     */
    public function setApplyGroupMedicaid($apply_group_medicaid): void
    {
        $this->apply_group_medicaid = $apply_group_medicaid;
    }

    /**
     * @return mixed
     */
    public function getApplyGroupMedicare()
    {
        return $this->apply_group_medicare;
    }

    /**
     * @param mixed $apply_group_medicare
     */
    public function setApplyGroupMedicare($apply_group_medicare): void
    {
        $this->apply_group_medicare = $apply_group_medicare;
    }

    /**
     * @return mixed
     */
    public function getDisabled()
    {
        return $this->disabled;
    }

    /**
     * @param mixed $disabled
     */
    public function setDisabled($disabled): void
    {
        $this->disabled = $disabled;
    }

    /**
     * @return mixed
     */
    public function getNaBeds()
    {
        return $this->na_beds;
    }

    /**
     * @param mixed $na_beds
     */
    public function setNaBeds($na_beds): void
    {
        $this->na_beds = $na_beds;
    }

    public function getSupportEHR(): ?bool
    {
        return $this->supportEHR;
    }

    public function setSupportEHR(?bool $supportEHR): self
    {
        $this->supportEHR = $supportEHR;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAhcaNumber(){
        return $this->ahca_number;
    }

    /**
     * @param mixed $ahca_number
     */
    public function setAhcaNumber($ahca_number): void{
        $this->ahca_number = $ahca_number;
    }

    public function getAppointment(): ?string
    {
        return $this->appointment;
    }

    public function setAppointment(?string $appointment): self
    {
        $this->appointment = $appointment;

        return $this;
    }

    public function getLocationName(): ?string
    {
        return $this->location_name;
    }

    public function setLocationName(?string $location_name): self
    {
        $this->location_name = $location_name;

        return $this;
    }

    public function getHaveBeds(): ?bool
    {
        return $this->have_beds;
    }

    public function setHaveBeds(?bool $have_beds): self
    {
        $this->have_beds = $have_beds;

        return $this;
    }

    /**
     * Add age_range
     *
     * @param \App\Entity\AgesRange $age_range
     *
     * @return BillingAddress
     */
    public function addAgeRange(\App\Entity\AgesRange $age_range)
    {
        $this->age_ranges[] = $age_range;

        return $this;
    }

    /**
     * Remove age_ranges
     *
     * @param \App\Entity\AgesRange $age_range
     */
    public function removeAgeRange(\App\Entity\AgesRange $age_range)
    {
        $this->age_ranges->removeElement($age_range);
    }

    /**
     * Get age_ranges
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAgeRanges()
    {
        return $this->age_ranges;
    }

    public function getLocationNPI_info() {
        return $this->location_npi;
    }

    /**
     * Add provider
     *
     * @param \App\Entity\Provider $provider
     *
     * @return Provider
     */
    public function addProvider(\App\Entity\Provider $provider)
    {
        $this->providers[] = $provider;

        return $this;
    }

    /**
     * Remove provider
     *
     * @param \App\Entity\Provider $provider
     */
    public function removeProvider(\App\Entity\Provider $provider)
    {
        $this->providers->removeElement($provider);
    }

    /**
     * @return mixed
     */
    public function getKidsBeds()
    {
        return $this->kids_beds;
    }

    /**
     * @param mixed $kids_beds
     */
    public function setKidsBeds($kids_beds): void
    {
        $this->kids_beds = $kids_beds;
    }

    /**
     * @Assert\Callback
     */
    public function validateBedsNumberIfHaveBed(ExecutionContextInterface $context, $payload)
    {
        if ($this->getHaveBeds() === true) {

            $beds=intval($this->getBeds());
            $kid_beds=intval($this->getKidsBeds());
            $total=$beds+$kid_beds;

            if($total==""){
                $context->buildViolation('Beds number required or Child Beds required')
                    ->atPath('beds')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback
     */
    public function validateChildBedsNumberIfHaveBed(ExecutionContextInterface $context, $payload)
    {
        if ($this->getHaveBeds() === true) {
            $beds=intval($this->getBeds());
            $kid_beds=intval($this->getKidsBeds());
            $total=$beds+$kid_beds;

            if($total==""){
                $context->buildViolation('Beds number required or Child Beds required')
                    ->atPath('kids_beds')
                    ->addViolation();
            }
        }
    }

    /**
     * @Assert\Callback
     */
    public function validateTddNumberIfTdd(ExecutionContextInterface $context, $payload)
    {
        if ($this->getTdd() === true && ($this->getTddNumber() === null || $this->getTddNumber() === '')) {
            $context->buildViolation('TDD number required')
                ->atPath('tdd_number')
                ->addViolation();
        }
    }

    public function getAdaCompliant(): ?bool
    {
        return $this->ada_compliant;
    }

    public function setAdaCompliant(?bool $ada_compliant): self
    {
        $this->ada_compliant = $ada_compliant;

        return $this;
    }

    public function getPublicTransportation(): ?bool
    {
        return $this->public_transportation;
    }

    public function setPublicTransportation(?bool $public_transportation): self
    {
        $this->public_transportation = $public_transportation;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getWheelchairRequirements()
    {
        return $this->wheelchair_requirements;
    }

    /**
     * @param mixed $wheelchair_requirements
     */
    public function setWheelchairRequirements($wheelchair_requirements): void
    {
        $this->wheelchair_requirements = $wheelchair_requirements;
    }

}
