<?php

namespace App\Entity;

use App\Repository\SpecialtyAreaProviderRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=SpecialtyAreaProviderRepository::class)
 */
class SpecialtyAreaProvider
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\SpecialtyAreaType", inversedBy="specialty_area_providers")
     * @ORM\JoinColumn(name="specialty_area_type_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $specialty_area_type;

    public function __toString() {
        return $this->name;
    }

    public function getDisplayName() {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }


    public function getSpecialtyAreaType()
    {
        return $this->specialty_area_type;
    }

    public function setSpecialtyAreaType($specialty_area_type)
    {
        $this->specialty_area_type = $specialty_area_type;

        return $this;
    }
}
