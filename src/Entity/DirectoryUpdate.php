<?php

namespace App\Entity;

use App\Repository\DirectoryUpdateRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=DirectoryUpdateRepository::class)
 */
class DirectoryUpdate
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $organization_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $representative_name;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $email;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_provider_name_correct;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_practice_name_correct;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_specialties_corrected_listed;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_practice_locationes_listed;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_provider_acceptin_new_patients;

    /**
     * @ORM\Column(type="boolean")
     */
    private $is_address_phone_correct;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="directory_update")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization", inversedBy="directory_update")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $organization;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getOrganizationName(): ?string
    {
        return $this->organization_name;
    }

    public function setOrganizationName(string $organization_name): self
    {
        $this->organization_name = $organization_name;

        return $this;
    }

    public function getRepresentativeName(): ?string
    {
        return $this->representative_name;
    }

    public function setRepresentativeName(string $representative_name): self
    {
        $this->representative_name = $representative_name;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getIsProviderNameCorrect(): ?bool
    {
        return $this->is_provider_name_correct;
    }

    public function setIsProviderNameCorrect(bool $is_provider_name_correct): self
    {
        $this->is_provider_name_correct = $is_provider_name_correct;

        return $this;
    }

    public function getIsPracticeNameCorrect(): ?bool
    {
        return $this->is_practice_name_correct;
    }

    public function setIsPracticeNameCorrect(bool $is_practice_name_correct): self
    {
        $this->is_practice_name_correct = $is_practice_name_correct;

        return $this;
    }

    public function getIsSpecialtiesCorrectedListed(): ?bool
    {
        return $this->is_specialties_corrected_listed;
    }

    public function setIsSpecialtiesCorrectedListed(bool $is_specialties_corrected_listed): self
    {
        $this->is_specialties_corrected_listed = $is_specialties_corrected_listed;

        return $this;
    }

    public function getIsPracticeLocationesListed(): ?bool
    {
        return $this->is_practice_locationes_listed;
    }

    public function setIsPracticeLocationesListed(bool $is_practice_locationes_listed): self
    {
        $this->is_practice_locationes_listed = $is_practice_locationes_listed;

        return $this;
    }

    public function getIsProviderAcceptinNewPatients(): ?bool
    {
        return $this->is_provider_acceptin_new_patients;
    }

    public function setIsProviderAcceptinNewPatients(bool $is_provider_acceptin_new_patients): self
    {
        $this->is_provider_acceptin_new_patients = $is_provider_acceptin_new_patients;

        return $this;
    }

    public function getIsAddressPhoneCorrect(): ?bool
    {
        return $this->is_address_phone_correct;
    }

    public function setIsAddressPhoneCorrect(bool $is_address_phone_correct): self
    {
        $this->is_address_phone_correct = $is_address_phone_correct;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     */
    public function setOrganization($organization): void
    {
        $this->organization = $organization;
    }
}
