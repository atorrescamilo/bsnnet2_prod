<?php

namespace App\Entity;

use App\Repository\OrganizationTrainingAttestationRepository;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrganizationTrainingAttestationRepository::class)
 */
class OrganizationTrainingAttestation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $year;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $representative_name;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $representative_phone;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $representative_email;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $representative_signature;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $date;

    /**
     * Training Attestation can have different Providers.
     * @ManyToMany(targetEntity="App\Entity\Provider")
     * @JoinTable(name="organization_training_provider",
     *      joinColumns={@JoinColumn(name="training_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $providers;

    /**
     * Training Attestation can have different Providers.
     * @ManyToMany(targetEntity="App\Entity\CmsTraining")
     * @JoinTable(name="organization_training_cms_training",
     *      joinColumns={@JoinColumn(name="training_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="cms_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $cms_trainings;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization", inversedBy="training_attestation")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $organization;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getYear(): ?string
    {
        return $this->year;
    }

    public function setYear(string $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getRepresentativeName(): ?string
    {
        return $this->representative_name;
    }

    public function setRepresentativeName(string $representative_name): self
    {
        $this->representative_name = $representative_name;

        return $this;
    }

    public function getRepresentativePhone(): ?string
    {
        return $this->representative_phone;
    }

    public function setRepresentativePhone(string $representative_phone): self
    {
        $this->representative_phone = $representative_phone;

        return $this;
    }

    public function getRepresentativeEmail(): ?string
    {
        return $this->representative_email;
    }

    public function setRepresentativeEmail(string $representative_email): self
    {
        $this->representative_email = $representative_email;

        return $this;
    }

    public function getRepresentativeSignature(): ?string
    {
        return $this->representative_signature;
    }

    public function setRepresentativeSignature(string $representative_signature): self
    {
        $this->representative_signature = $representative_signature;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     */
    public function setOrganization($organization): void{
        $this->organization = $organization;
    }

    /**
     * Add provider
     *
     * @param \App\Entity\Provider $provider
     *
     * @return OrganizationTrainingAttestation
     */
    public function addProvider(\App\Entity\Provider $provider){
        $this->providers[] = $provider;

        return $this;
    }

    /**
     * Remove provider
     *
     * @param \App\Entity\Provider $provider
     */
    public function removeProvider(\App\Entity\Provider $provider){
        $this->providers->removeElement($provider);
    }

    /**
     * Get providers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProviders(){
        return $this->providers;
    }

    /**
     * Add cms_training
     *
     * @param \App\Entity\CmsTraining $cms_training
     *
     * @return OrganizationTrainingAttestation
     */
    public function addCMSTraining(\App\Entity\CmsTraining $cms_training){
        $this->cms_trainings[] = $cms_training;

        return $this;
    }

    /**
     * Remove cms_trainings
     *
     * @param \App\Entity\CmsTraining $cms_trainings
     */
    public function removeCMSTraining(\App\Entity\CmsTraining $cms_training){
        $this->cms_trainings->removeElement($cms_training);
    }

    /**
     * Get cms_trainings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCMSTrainings(){
        return $this->cms_trainings;
    }

}
