<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TaxonomyCodeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class TaxonomyCode
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code;

    public function __toString() {
        return $this->code.' "'.$this->Classification.' | '.$this->Specialization.'"';
    }

    public function getDisplayName() {
        return $this->code;
    }

    public function getDisplayName2() {
        return $this->code.' "'.$this->Specialization.'"';
    }

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Grouping;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Classification;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Specialization;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrganizationTaxonomy", mappedBy="taxonomy")
     */
    private $organization_taxonomy;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getGrouping(): ?string
    {
        return $this->Grouping;
    }

    public function setGrouping(?string $Grouping): self
    {
        $this->Grouping = $Grouping;

        return $this;
    }

    public function getClassification(): ?string
    {
        return $this->Classification;
    }

    public function setClassification(?string $Classification): self
    {
        $this->Classification = $Classification;

        return $this;
    }

    public function getSpecialization(): ?string
    {
        return $this->Specialization;
    }

    public function setSpecialization(?string $Specialization): self
    {
        $this->Specialization = $Specialization;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }
    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }
}
