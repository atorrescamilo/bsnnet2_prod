<?php

namespace App\Entity;

use App\Repository\PNVLogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PNVLogRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class PNVLog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $record_traking_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $app_receipt_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialing_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $license_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $provider_id;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $ssn_fein;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $npi_number;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $start_date;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $end_date;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $provider_type;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $primary_specialty;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $AHCA_ID;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRecordTrakingNumber(): ?string
    {
        return $this->record_traking_number;
    }

    public function setRecordTrakingNumber(string $record_traking_number): self
    {
        $this->record_traking_number = $record_traking_number;

        return $this;
    }

    public function getAppReceiptDate(): ?string
    {
        return $this->app_receipt_date;
    }

    public function setAppReceiptDate(?string $app_receipt_date): self
    {
        $this->app_receipt_date = $app_receipt_date;

        return $this;
    }

    public function getCredentialingDate(): ?string
    {
        return $this->credentialing_date;
    }

    public function setCredentialingDate(?string $credentialing_date): self
    {
        $this->credentialing_date = $credentialing_date;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(?string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(?string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getLicenseNumber(): ?string
    {
        return $this->license_number;
    }

    public function setLicenseNumber(?string $license_number): self
    {
        $this->license_number = $license_number;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getProviderId(): ?string
    {
        return $this->provider_id;
    }

    public function setProviderId(string $provider_id): self
    {
        $this->provider_id = $provider_id;

        return $this;
    }

    public function getSsnFein(): ?string
    {
        return $this->ssn_fein;
    }

    public function setSsnFein(?string $ssn_fein): self
    {
        $this->ssn_fein = $ssn_fein;

        return $this;
    }

    public function getNpiNumber(): ?string
    {
        return $this->npi_number;
    }

    public function setNpiNumber(string $npi_number): self
    {
        $this->npi_number = $npi_number;

        return $this;
    }

    public function getStartDate(): ?string
    {
        return $this->start_date;
    }

    public function setStartDate(?string $start_date): self
    {
        $this->start_date = $start_date;

        return $this;
    }

    public function getEndDate(): ?string
    {
        return $this->end_date;
    }

    public function setEndDate(?string $end_date): self
    {
        $this->end_date = $end_date;

        return $this;
    }

    public function getProviderType(): ?string
    {
        return $this->provider_type;
    }

    public function setProviderType(?string $provider_type): self
    {
        $this->provider_type = $provider_type;

        return $this;
    }

    public function getPrimarySpecialty(): ?string
    {
        return $this->primary_specialty;
    }

    public function setPrimarySpecialty(?string $primary_specialty): self
    {
        $this->primary_specialty = $primary_specialty;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    public function getAHCAID(): ?string
    {
        return $this->AHCA_ID;
    }

    public function setAHCAID(?string $AHCA_ID): self
    {
        $this->AHCA_ID = $AHCA_ID;

        return $this;
    }
}
