<?php

namespace App\Entity;

use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BillingAddressCvoRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class BillingAddressCvo
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialingDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialingReceivedDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialingCompleteDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialingAcceptedDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialingDeniedDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sanctionDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialingEffectiveDate;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cvo", inversedBy="billing_address_cvo")
     * @ORM\JoinColumn(name="cvo_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $cvo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BillingAddress", inversedBy="billing_address_cvo")
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $billing_address;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CredentialingStatus", inversedBy="billing_address_cvo")
     * @ORM\JoinColumn(name="credentialing_status_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $credentialing_status;

    /**
     * Providers can have different types.
     * @ManyToMany(targetEntity="App\Entity\Payer")
     * @JoinTable(name="billing_address_cvo_payer",
     *      joinColumns={@JoinColumn(name="billing_address_cvo_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="payer_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $payers;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCredentialingDate(): ?string
    {
        return $this->credentialingDate;
    }

    public function setCredentialingDate(?string $credentialingDate): self
    {
        $this->credentialingDate = $credentialingDate;

        return $this;
    }

    public function getCredentialingReceivedDate(): ?string
    {
        return $this->credentialingReceivedDate;
    }

    public function setCredentialingReceivedDate(?string $credentialingReceivedDate): self
    {
        $this->credentialingReceivedDate = $credentialingReceivedDate;

        return $this;
    }

    public function getCredentialingCompleteDate(): ?string
    {
        return $this->credentialingCompleteDate;
    }

    public function setCredentialingCompleteDate(?string $credentialingCompleteDate): self
    {
        $this->credentialingCompleteDate = $credentialingCompleteDate;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getCvo()
    {
        return $this->cvo;
    }

    /**
     * @param mixed $cvo
     */
    public function setCvo($cvo): void
    {
        $this->cvo = $cvo;
    }

    /**
     * @return mixed
     */
    public function getBillingAddress()
    {
        return $this->billing_address;
    }

    /**
     * @param mixed $billing_address
     */
    public function setBillingAddress($billing_address): void
    {
        $this->billing_address = $billing_address;
    }

    /**
     * @return mixed
     */
    public function getSanctionDate()
    {
        return $this->sanctionDate;
    }

    /**
     * @param mixed $sanctionDate
     */
    public function setSanctionDate($sanctionDate): void
    {
        $this->sanctionDate = $sanctionDate;
    }

    /**
     * @return mixed
     */
    public function getCredentialingStatus()
    {
        return $this->credentialing_status;
    }

    /**
     * @param mixed $credentialing_status
     */
    public function setCredentialingStatus($credentialing_status): void
    {
        $this->credentialing_status = $credentialing_status;
    }

    /**
     * @return mixed
     */
    public function getCredentialingAcceptedDate()
    {
        return $this->credentialingAcceptedDate;
    }

    /**
     * @param mixed $credentialingAcceptedDate
     */
    public function setCredentialingAcceptedDate($credentialingAcceptedDate): void
    {
        $this->credentialingAcceptedDate = $credentialingAcceptedDate;
    }

    /**
     * @return mixed
     */
    public function getCredentialingDeniedDate()
    {
        return $this->credentialingDeniedDate;
    }

    /**
     * @param mixed $credentialingDeniedDate
     */
    public function setCredentialingDeniedDate($credentialingDeniedDate): void
    {
        $this->credentialingDeniedDate = $credentialingDeniedDate;
    }

    /**
     * Add payer
     *
     * @param \App\Entity\Payer $payer
     *
     * @return BillingAddressCvo
     */
    public function addPayer(\App\Entity\Payer $payer)
    {
        $this->payers[] = $payer;

        return $this;
    }

    /**
     * Remove payer
     *
     * @param \App\Entity\Payer $payer
     */
    public function removePayer(\App\Entity\Payer $payer)
    {
        $this->payers->removeElement($payer);
    }

    /**
     * Get payer
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPayers()
    {
        return $this->payers;
    }

    /**
     * @return mixed
     */
    public function getCredentialingEffectiveDate()
    {
        return $this->credentialingEffectiveDate;
    }

    /**
     * @param mixed $credentialingEffectiveDate
     */
    public function setCredentialingEffectiveDate($credentialingEffectiveDate): void
    {
        $this->credentialingEffectiveDate = $credentialingEffectiveDate;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

}
