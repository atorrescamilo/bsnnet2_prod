<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CredSimpleRecordRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class CredSimpleRecord
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $npi;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $provider_type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credential_event;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $due_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $completed_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $flagged;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $decision_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $report_pdf;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $license;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $license_expires_on;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $license_status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNpi(): ?string
    {
        return $this->npi;
    }

    public function setNpi(string $npi): self
    {
        $this->npi = $npi;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(?string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(?string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getProviderType(): ?string
    {
        return $this->provider_type;
    }

    public function setProviderType(?string $provider_type): self
    {
        $this->provider_type = $provider_type;

        return $this;
    }

    public function getCredentialEvent(): ?string
    {
        return $this->credential_event;
    }

    public function setCredentialEvent(?string $credential_event): self
    {
        $this->credential_event = $credential_event;

        return $this;
    }

    public function getDueDate(): ?string
    {
        return $this->due_date;
    }

    public function setDueDate(?string $due_date): self
    {
        $this->due_date = $due_date;

        return $this;
    }

    public function getCompletedDate(): ?string
    {
        return $this->completed_date;
    }

    public function setCompletedDate(?string $completed_date): self
    {
        $this->completed_date = $completed_date;

        return $this;
    }

    public function getFlagged(): ?string
    {
        return $this->flagged;
    }

    public function setFlagged(?string $flagged): self
    {
        $this->flagged = $flagged;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }
    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    public function getDecisionDate(): ?string
    {
        return $this->decision_date;
    }

    public function setDecisionDate(?string $decision_date): self
    {
        $this->decision_date = $decision_date;

        return $this;
    }

    public function getReportPdf(): ?string
    {
        return $this->report_pdf;
    }

    public function setReportPdf(?string $report_pdf): self
    {
        $this->report_pdf = $report_pdf;

        return $this;
    }

    public function getLicense(): ?string
    {
        return $this->license;
    }

    public function setLicense(?string $license): self
    {
        $this->license = $license;

        return $this;
    }

    public function getLicenseExpiresOn(): ?string
    {
        return $this->license_expires_on;
    }

    public function setLicenseExpiresOn(?string $license_expires_on): self
    {
        $this->license_expires_on = $license_expires_on;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLicenseStatus()
    {
        return $this->license_status;
    }

    /**
     * @param mixed $license_status
     */
    public function setLicenseStatus($license_status): void
    {
        $this->license_status = $license_status;
    }
}
