<?php

namespace App\Entity;

use App\Repository\PDataMedicareRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PDataMedicareRepository::class)
 */
class PDataMedicare
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $npi_number;

    /**
     * @ORM\Column(type="text")
     */
    private $data;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNpiNumber(): ?string
    {
        return $this->npi_number;
    }

    public function setNpiNumber(string $npi_number): self
    {
        $this->npi_number = $npi_number;

        return $this;
    }

    public function getData(): ?string
    {
        return $this->data;
    }

    public function setData(string $data): self
    {
        $this->data = $data;

        return $this;
    }
}
