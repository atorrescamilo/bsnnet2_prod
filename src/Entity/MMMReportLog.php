<?php

namespace App\Entity;

use App\Repository\MMMReportLogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MMMReportLogRepository::class)
 */
class MMMReportLog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $npi;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $register_type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $reporting_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $change_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $specialties;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $degrees;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $suite;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $board_certified_specialty;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hospital_base_physician;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zip_code;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $state_code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $county_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $City;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $billing_address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $practice_name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $id_log;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getNpi(): ?string
    {
        return $this->npi;
    }

    public function setNpi(?string $npi): self
    {
        $this->npi = $npi;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(?string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(?string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getRegisterType(): ?string
    {
        return $this->register_type;
    }

    public function setRegisterType(?string $register_type): self
    {
        $this->register_type = $register_type;

        return $this;
    }

    public function getReportingDate(): ?string
    {
        return $this->reporting_date;
    }

    public function setReportingDate(?string $reporting_date): self
    {
        $this->reporting_date = $reporting_date;

        return $this;
    }

    public function getChangeDate(): ?string
    {
        return $this->change_date;
    }

    public function setChangeDate(?string $change_date): self
    {
        $this->change_date = $change_date;

        return $this;
    }

    public function getSpecialties(): ?string
    {
        return $this->specialties;
    }

    public function setSpecialties(?string $specialties): self
    {
        $this->specialties = $specialties;

        return $this;
    }

    public function getDegrees(): ?string
    {
        return $this->degrees;
    }

    public function setDegrees(?string $degrees): self
    {
        $this->degrees = $degrees;

        return $this;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(?string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getSuite(): ?string
    {
        return $this->suite;
    }

    public function setSuite(?string $suite): self
    {
        $this->suite = $suite;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(?string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getBoardCertifiedSpecialty(): ?string
    {
        return $this->board_certified_specialty;
    }

    public function setBoardCertifiedSpecialty(?string $board_certified_specialty): self
    {
        $this->board_certified_specialty = $board_certified_specialty;

        return $this;
    }

    public function getHospitalBasePhysician(): ?string
    {
        return $this->hospital_base_physician;
    }

    public function setHospitalBasePhysician(?string $hospital_base_physician): self
    {
        $this->hospital_base_physician = $hospital_base_physician;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zip_code;
    }

    public function setZipCode(?string $zip_code): self
    {
        $this->zip_code = $zip_code;

        return $this;
    }

    public function getStateCode(): ?string
    {
        return $this->state_code;
    }

    public function setStateCode(?string $state_code): self
    {
        $this->state_code = $state_code;

        return $this;
    }

    public function getCountyName(): ?string
    {
        return $this->county_name;
    }

    public function setCountyName(?string $county_name): self
    {
        $this->county_name = $county_name;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->City;
    }

    public function setCity(?string $City): self
    {
        $this->City = $City;

        return $this;
    }

    public function getBillingAddress(): ?string
    {
        return $this->billing_address;
    }

    public function setBillingAddress(?string $billing_address): self
    {
        $this->billing_address = $billing_address;

        return $this;
    }

    public function getPracticeName(): ?string
    {
        return $this->practice_name;
    }

    public function setPracticeName(string $practice_name): self
    {
        $this->practice_name = $practice_name;

        return $this;
    }

    public function getIdLog(): ?int
    {
        return $this->id_log;
    }

    public function setIdLog(?int $id_log): self
    {
        $this->id_log = $id_log;

        return $this;
    }
}
