<?php

namespace App\Entity;

use App\Repository\AETFHKCMedicaidDataRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AETFHKCMedicaidDataRepository::class)
 */
class AETFHKCMedicaidData
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $key_log;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $npi;

    /**
     * @ORM\Column(type="json")
     */
    private $data = [];

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getKeyLog(): ?string
    {
        return $this->key_log;
    }

    public function setKeyLog(string $key_log): self
    {
        $this->key_log = $key_log;

        return $this;
    }

    public function getNpi(): ?string
    {
        return $this->npi;
    }

    public function setNpi(string $npi): self
    {
        $this->npi = $npi;

        return $this;
    }

    public function getData(): ?array
    {
        return $this->data;
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
