<?php

namespace App\Entity;

use App\Repository\FCCSurveyRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FCCSurveyRepository::class)
 */
class FCCSurvey
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $npi_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $provider_type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $p1;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $p2;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $p3;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $p4;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $p5;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc1a;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc1b;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc1c;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc2a;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc2b;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc3a;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc3b;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc3c;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc3d;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc3e;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc3f;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc4a;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc4b;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc4c;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc4d;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc4e;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc4f;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc5a;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc5b;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc5c;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc6a;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc6b;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc6c;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc7a;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc7b;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $cc7c;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $original_npi;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNpiNumber(): ?string
    {
        return $this->npi_number;
    }

    public function setNpiNumber(string $npi_number): self
    {
        $this->npi_number = $npi_number;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProviderType(): ?string
    {
        return $this->provider_type;
    }

    public function setProviderType(?string $provider_type): self
    {
        $this->provider_type = $provider_type;

        return $this;
    }

    public function getP1(): ?string
    {
        return $this->p1;
    }

    public function setP1(string $p1): self
    {
        $this->p1 = $p1;

        return $this;
    }

    public function getP2(): ?string
    {
        return $this->p2;
    }

    public function setP2(string $p2): self
    {
        $this->p2 = $p2;

        return $this;
    }

    public function getP3(): ?string
    {
        return $this->p3;
    }

    public function setP3(string $p3): self
    {
        $this->p3 = $p3;

        return $this;
    }

    public function getP4(): ?string
    {
        return $this->p4;
    }

    public function setP4(string $p4): self
    {
        $this->p4 = $p4;

        return $this;
    }

    public function getP5(): ?string
    {
        return $this->p5;
    }

    public function setP5(string $p5): self
    {
        $this->p5 = $p5;

        return $this;
    }

    public function getCc1a(): ?string
    {
        return $this->cc1a;
    }

    public function setCc1a(string $cc1a): self
    {
        $this->cc1a = $cc1a;

        return $this;
    }

    public function getCc1b(): ?string
    {
        return $this->cc1b;
    }

    public function setCc1b(string $cc1b): self
    {
        $this->cc1b = $cc1b;

        return $this;
    }

    public function getCc1c(): ?string
    {
        return $this->cc1c;
    }

    public function setCc1c(string $cc1c): self
    {
        $this->cc1c = $cc1c;

        return $this;
    }

    public function getCc2a(): ?string
    {
        return $this->cc2a;
    }

    public function setCc2a(string $cc2a): self
    {
        $this->cc2a = $cc2a;

        return $this;
    }

    public function getCc2b(): ?string
    {
        return $this->cc2b;
    }

    public function setCc2b(string $cc2b): self
    {
        $this->cc2b = $cc2b;

        return $this;
    }

    public function getCc3a(): ?string
    {
        return $this->cc3a;
    }

    public function setCc3a(string $cc3a): self
    {
        $this->cc3a = $cc3a;

        return $this;
    }

    public function getCc3b(): ?string
    {
        return $this->cc3b;
    }

    public function setCc3b(string $cc3b): self
    {
        $this->cc3b = $cc3b;

        return $this;
    }

    public function getCc3c(): ?string
    {
        return $this->cc3c;
    }

    public function setCc3c(string $cc3c): self
    {
        $this->cc3c = $cc3c;

        return $this;
    }

    public function getCc3d(): ?string
    {
        return $this->cc3d;
    }

    public function setCc3d(string $cc3d): self
    {
        $this->cc3d = $cc3d;

        return $this;
    }

    public function getCc3e(): ?string
    {
        return $this->cc3e;
    }

    public function setCc3e(string $cc3e): self
    {
        $this->cc3e = $cc3e;

        return $this;
    }

    public function getCc3f(): ?string
    {
        return $this->cc3f;
    }

    public function setCc3f(string $cc3f): self
    {
        $this->cc3f = $cc3f;

        return $this;
    }

    public function getCc4a(): ?string
    {
        return $this->cc4a;
    }

    public function setCc4a(string $cc4a): self
    {
        $this->cc4a = $cc4a;

        return $this;
    }

    public function getCc4b(): ?string
    {
        return $this->cc4b;
    }

    public function setCc4b(string $cc4b): self
    {
        $this->cc4b = $cc4b;

        return $this;
    }

    public function getCc4c(): ?string
    {
        return $this->cc4c;
    }

    public function setCc4c(string $cc4c): self
    {
        $this->cc4c = $cc4c;

        return $this;
    }

    public function getCc4d(): ?string
    {
        return $this->cc4d;
    }

    public function setCc4d(string $cc4d): self
    {
        $this->cc4d = $cc4d;

        return $this;
    }

    public function getCc4e(): ?string
    {
        return $this->cc4e;
    }

    public function setCc4e(string $cc4e): self
    {
        $this->cc4e = $cc4e;

        return $this;
    }

    public function getCc4f(): ?string
    {
        return $this->cc4f;
    }

    public function setCc4f(string $cc4f): self
    {
        $this->cc4f = $cc4f;

        return $this;
    }

    public function getCc5a(): ?string
    {
        return $this->cc5a;
    }

    public function setCc5a(string $cc5a): self
    {
        $this->cc5a = $cc5a;

        return $this;
    }

    public function getCc5b(): ?string
    {
        return $this->cc5b;
    }

    public function setCc5b(string $cc5b): self
    {
        $this->cc5b = $cc5b;

        return $this;
    }

    public function getCc5c(): ?string
    {
        return $this->cc5c;
    }

    public function setCc5c(string $cc5c): self
    {
        $this->cc5c = $cc5c;

        return $this;
    }

    public function getCc6a(): ?string
    {
        return $this->cc6a;
    }

    public function setCc6a(string $cc6a): self
    {
        $this->cc6a = $cc6a;

        return $this;
    }

    public function getCc6b(): ?string
    {
        return $this->cc6b;
    }

    public function setCc6b(string $cc6b): self
    {
        $this->cc6b = $cc6b;

        return $this;
    }

    public function getCc6c(): ?string
    {
        return $this->cc6c;
    }

    public function setCc6c(string $cc6c): self
    {
        $this->cc6c = $cc6c;

        return $this;
    }

    public function getCc7a(): ?string
    {
        return $this->cc7a;
    }

    public function setCc7a(string $cc7a): self
    {
        $this->cc7a = $cc7a;

        return $this;
    }

    public function getCc7b(): ?string
    {
        return $this->cc7b;
    }

    public function setCc7b(string $cc7b): self
    {
        $this->cc7b = $cc7b;

        return $this;
    }

    public function getCc7c(): ?string
    {
        return $this->cc7c;
    }

    public function setCc7c(string $cc7c): self
    {
        $this->cc7c = $cc7c;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    public function getOriginalNpi(): ?string
    {
        return $this->original_npi;
    }

    public function setOriginalNpi(?string $original_npi): self
    {
        $this->original_npi = $original_npi;

        return $this;
    }
}
