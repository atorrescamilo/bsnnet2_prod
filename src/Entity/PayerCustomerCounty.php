<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PayerCustomerCountyRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PayerCustomerCounty
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Payer", inversedBy="payercustomer")
     * @ORM\JoinColumn(name="payer_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @Assert\NotNull()
     */
    protected $payer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CountyMatch", inversedBy="payercustomer")
     * @ORM\JoinColumn(name="county_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @Assert\NotNull()
     */
    protected $county;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\LineOfBusiness", inversedBy="payercustomer")
     * @ORM\JoinColumn(name="lineofbusiness_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @Assert\NotNull()
     */
    protected $lineofbusiness;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $total;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTotal(): ?string
    {
        return $this->total;
    }

    public function setTotal(?string $total): self
    {
        $this->total = $total;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPayer()
    {
        return $this->payer;
    }

    /**
     * @param mixed $payer
     */
    public function setPayer($payer): void
    {
        $this->payer = $payer;
    }

    /**
     * @return mixed
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param mixed $county
     */
    public function setCounty($county): void
    {
        $this->county = $county;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getLineofbusiness()
    {
        return $this->lineofbusiness;
    }

    /**
     * @param mixed $lineofbusiness
     */
    public function setLineofbusiness($lineofbusiness): void
    {
        $this->lineofbusiness = $lineofbusiness;
    }
}
