<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmailTypeRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class EmailType
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $subject;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EmailQueued", mappedBy="email_type")
     */
    protected $email_queued;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EmailLog", mappedBy="email_type")
     */
    protected $email_log;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\FileAttach", mappedBy="email_type")
     */
    protected $file_attach;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * Providers can have different specialty.
     * @ManyToMany(targetEntity="App\Entity\ContactPBestChoice")
     * @JoinTable(name="email_type_best_option_choice",
     *      joinColumns={@JoinColumn(name="email_type_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@JoinColumn(name="best_choice_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     *  @Assert\NotNull(message="You must select at least 1 choice.")
     *  @Assert\Count(
     *      min = "1",
     *      minMessage = "You must select at least 1 choice."
     * )
     */
    protected $best_choices;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cc;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bcc;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    /**
     * Add best_choice
     *
     * @param \App\Entity\ContactPBestChoice $best_choice
     *
     * @return ContactP
     */
    public function addBestChoice(\App\Entity\ContactPBestChoice $best_choice)
    {
        $this->best_choices[] = $best_choice;

        return $this;
    }

    /**
     * Remove best_choice
     *
     * @param \App\Entity\ContactPBestChoice $best_choice
     */
    public function removeBestChoice(\App\Entity\ContactPBestChoice $best_choice){
        $this->best_choices->removeElement($best_choice);
    }

    /**
     * Get best_choice
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBestChoices()
    {
        return $this->best_choices;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject): void
    {
        $this->subject = $subject;
    }

    public function getCc(): ?string
    {
        return $this->cc;
    }

    public function setCc(?string $cc): self
    {
        $this->cc = $cc;

        return $this;
    }

    public function getBcc(): ?string
    {
        return $this->bcc;
    }

    public function setBcc(?string $bcc): self
    {
        $this->bcc = $bcc;

        return $this;
    }
}
