<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Shapecode\NYADoctrineEncryptBundle\Configuration\Encrypted;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity(repositoryClass="App\Repository\OrganizationRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Organization
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $name;

    public function __toString() {
        return $this->name;
    }

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     */
    private $docusign_link;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $receiveDateAgmt;

    /**
     * @ORM\Column(name="path", type="string", nullable=true)
     */
    protected $image;

    /**
     * @ORM\Column(type="string", length=100,nullable=true)
     * @Assert\NotBlank()
     */
    protected $provider_type;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $hospital_affiliations;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $billing_phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $faxno;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fein;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tin_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name_of_ehr_emr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $hn1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cred_simple;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_on;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_on;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $group_npi;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $group_npi2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank()
     */
    private $billing_npi;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $billing_npi2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $taxonomy_code;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $standardRates;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $nonStandardRates;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $disabled;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $disabled_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $provider_agreement_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state_license_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $provider_data_form_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $w9_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $roster_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $general_liability_coverage_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $site_visit_survey_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $accreditation_certificate_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $loa_loi_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $amendment_file;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $provider_application_receipt_date;

    /**
     * Organization can have different specialty.
     * @ManyToMany(targetEntity="App\Entity\OrgSpecialty")
     * @JoinTable(name="organization_org_specialties",
     *      joinColumns={@JoinColumn(name="organization_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="org_specialty_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $org_specialties;

    /**
     * Organization can have many Accreditation
     * @ManyToMany(targetEntity="App\Entity\Accreditation")
     * @JoinTable(name="organization_accreditation",
     *      joinColumns={@JoinColumn(name="organization_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="accreditation_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $accreditations;

    /**
     * Organization can have many Accreditation
     * @ManyToMany(targetEntity="App\Entity\SpecialtyAreas")
     * @JoinTable(name="organization_specialty_areas",
     *      joinColumns={@JoinColumn(name="organization_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="specialtyarea_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $specialty_areas;

    /**
     * Organization can have many Accreditation
     * @ManyToMany(targetEntity="App\Entity\TaxonomyCode")
     * @JoinTable(name="organization_taxonomies",
     *      joinColumns={@JoinColumn(name="organization_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="taxonomy_id", referencedColumnName="id", onDelete="Cascade")}
     * )
     */
    protected $taxonomies;

    /**
     * Organization can have different Payers.
     * @ManyToMany(targetEntity="App\Entity\Payer")
     * @JoinTable(name="organization_payer",
     *      joinColumns={@JoinColumn(name="organization_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="payer_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $payers;

    /**
     * Organization can have different Payers.
     * @ManyToMany(targetEntity="App\Entity\PaymentTier")
     * @JoinTable(name="organization_paymenttier",
     *      joinColumns={@JoinColumn(name="organization_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="paymenttier_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $paymenttiers;

    /**
     * @OneToMany(targetEntity="App\Entity\OrganizationContact", mappedBy="organization", cascade={"remove"})
     */
    private $contacts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrganizationLineofBusiness", mappedBy="organization", cascade={"remove"})
     */
    private $organization_lob;

    /**
     * @OneToMany(targetEntity="BillingAddress", mappedBy="organization", cascade={"remove"})
     */
    private $billing_address;

    /**
     * INVERSE SIDE
     * @ORM\OneToMany(targetEntity="App\Entity\OrganizationTaxonomy", mappedBy="organization")
     */
    private $organization_taxonomy;

    /**
     * @OneToMany(targetEntity="App\Entity\Provider", mappedBy="organization")
     */
    private $providers;

    /**
     * @OneToMany(targetEntity="App\Entity\OrganizationValidateRoster", mappedBy="organization")
     */
    private $validate_roster;

    /**
     * @OneToMany(targetEntity="App\Entity\ProviderToVerify", mappedBy="organization")
     */
    private $provider_to_verify;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProviderCredentialing", mappedBy="organization")
     */
    private $provider_credentialing;

    /**
     * @OneToMany(targetEntity="App\Entity\User", mappedBy="organization")
     */
    private $user;

    /**
     * @OneToMany(targetEntity="App\Entity\OrganizationTrainingAttestation", mappedBy="organization")
     */
    private $training_attestation;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProviderUser", inversedBy="organization")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $provider_user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrganizationStatus", inversedBy="organization")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $organization_status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrgTerminationReason", inversedBy="organization")
     * @ORM\JoinColumn(name="termination_reasons_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $termination_reason;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $termination_reason_note;

    /**
     * @OneToMany(targetEntity="App\Entity\OrganizationRates", mappedBy="organization")
     */
    private $organization_rates;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EmailQueued", mappedBy="organization")
     */
    protected $email_queued;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DirectoryUpdate", mappedBy="organization")
     */
    protected $directory_update;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EmailLog", mappedBy="organization")
     */
    protected $email_log;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $legal_name_tin_owner;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $american_sign_language;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $caqh;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $bda_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $addrs_id_track_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $legal;

    /**
     * @OneToMany(targetEntity="App\Entity\UserOrgRequest", mappedBy="organization")
     */
    private $user_organization_request;

    /**
     * Providers can be exclude for different specialty.
     * @ManyToMany(targetEntity="App\Entity\Payer")
     * @JoinTable(name="organization_payer_exclude",
     *      joinColumns={@JoinColumn(name="organization_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="payer_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $payer_exclude;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrganizationCredentialing", mappedBy="organization")
     */
    private $organization_credentialing;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrganizationPayerExcluded", mappedBy="organization")
     */
    private $organization_payer_excluded;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $has_telemedicine;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $telemedicine_company;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $interested_in_telemedicine;

    /**
     * Get providers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProviders()
    {
        return $this->providers;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTinNumber()
    {
        return $this->tin_number;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCredtedDate() {
        $this->created_on = new \DateTime();
    }

    /**
     * @ORM\PrePersist
     */
    public function setProviderApplicationDate() {
        $date=date('m/d/Y');
        $this->provider_application_receipt_date = $date;
    }

    /**
     * @param mixed $tin_number
     */
    public function setTinNumber($tin_number): void
    {
        $this->tin_number = $tin_number;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProviderType(): ?string
    {
        return $this->provider_type;
    }

    public function setProviderType(?string $provider_type): self
    {
        $this->provider_type = $provider_type;

        return $this;
    }

    public function getHospitalAffiliations(): ?string
    {
        return $this->hospital_affiliations;
    }

    public function setHospitalAffiliations(?string $hospital_affiliations): self
    {
        $this->hospital_affiliations = $hospital_affiliations;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getBillingPhone(): ?string
    {
        return $this->billing_phone;
    }

    public function setBillingPhone(?string $billing_phone): self
    {
        $this->billing_phone = $billing_phone;

        return $this;
    }

    public function getFaxno(): ?string
    {
        return $this->faxno;
    }

    public function setFaxno(?string $faxno): self
    {
        $this->faxno = $faxno;

        return $this;
    }

    public function getFein(): ?string
    {
        return $this->fein;
    }

    public function setFein(?string $fein): self
    {
        $this->fein = $fein;

        return $this;
    }

    public function getNameOfEhrEmr(): ?string
    {
        return $this->name_of_ehr_emr;
    }

    public function setNameOfEhrEmr(?string $name_of_ehr_emr): self
    {
        $this->name_of_ehr_emr = $name_of_ehr_emr;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getHn1(): ?string
    {
        return $this->hn1;
    }

    public function setHn1(?string $hn1): self
    {
        $this->hn1 = $hn1;

        return $this;
    }

    public function getCredSimple(): ?string
    {
        return $this->cred_simple;
    }

    public function setCredSimple(?string $cred_simple): self
    {
        $this->cred_simple = $cred_simple;

        return $this;
    }

    public function getCreatedOn(): ?\DateTimeInterface
    {
        return $this->created_on;
    }

    public function setCreatedOn(?\DateTimeInterface $created_on): self
    {
        $this->created_on = $created_on;

        return $this;
    }

    public function getUpdatedOn(): ?\DateTimeInterface
    {
        return $this->updated_on;
    }

    public function setUpdatedOn(?\DateTimeInterface $updated_on): self
    {
        $this->updated_on = $updated_on;

        return $this;
    }

    public function getGroupNpi(): ?string
    {
        return $this->group_npi;
    }

    public function setGroupNpi(?string $group_npi): self
    {
        $this->group_npi = $group_npi;

        return $this;
    }

    public function getGroupNpi2(): ?string
    {
        return $this->group_npi2;
    }

    public function setGroupNpi2(?string $group_npi2): self
    {
        $this->group_npi2 = $group_npi2;

        return $this;
    }

    public function getTaxonomyCode(): ?string
    {
        return $this->taxonomy_code;
    }

    public function setTaxonomyCode(?string $taxonomy_code): self
    {
        $this->taxonomy_code = $taxonomy_code;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getStandardRates(): ?bool
    {
        return $this->standardRates;
    }

    public function setStandardRates(?bool $standardRates): self
    {
        $this->standardRates = $standardRates;

        return $this;
    }

    public function getNonStandardRates(): ?string
    {
        return $this->nonStandardRates;
    }

    public function setNonStandardRates(?string $nonStandardRates): self
    {
        $this->nonStandardRates = $nonStandardRates;

        return $this;
    }

    public function getDisabled(): ?bool
    {
        return $this->disabled;
    }

    public function setDisabled(?bool $disabled): self
    {
        $this->disabled = $disabled;

        return $this;
    }

    public function getProviderAgreementFile(): ?string
    {
        return $this->provider_agreement_file;
    }

    public function setProviderAgreementFile(?string $provider_agreement_file): self
    {
        $this->provider_agreement_file = $provider_agreement_file;

        return $this;
    }

    public function getStateLicenseFile(): ?string
    {
        return $this->state_license_file;
    }

    public function setStateLicenseFile(?string $state_license_file): self
    {
        $this->state_license_file = $state_license_file;

        return $this;
    }

    public function getProviderDataFormFile(): ?string
    {
        return $this->provider_data_form_file;
    }

    public function setProviderDataFormFile(?string $provider_data_form_file): self
    {
        $this->provider_data_form_file = $provider_data_form_file;

        return $this;
    }

    public function getW9File(): ?string
    {
        return $this->w9_file;
    }

    public function setW9File(?string $w9_file): self
    {
        $this->w9_file = $w9_file;

        return $this;
    }

    public function getRosterFile(): ?string
    {
        return $this->roster_file;
    }

    public function setRosterFile(?string $roster_file): self
    {
        $this->roster_file = $roster_file;

        return $this;
    }

    public function getGeneralLiabilityCoverageFile(): ?string
    {
        return $this->general_liability_coverage_file;
    }

    public function setGeneralLiabilityCoverageFile(?string $general_liability_coverage_file): self
    {
        $this->general_liability_coverage_file = $general_liability_coverage_file;

        return $this;
    }

    public function getSiteVisitSurveyFile(): ?string
    {
        return $this->site_visit_survey_file;
    }

    public function setSiteVisitSurveyFile(?string $site_visit_survey_file): self
    {
        $this->site_visit_survey_file = $site_visit_survey_file;

        return $this;
    }

    public function getAccreditationCertificateFile(): ?string
    {
        return $this->accreditation_certificate_file;
    }

    public function setAccreditationCertificateFile(?string $accreditation_certificate_file): self
    {
        $this->accreditation_certificate_file = $accreditation_certificate_file;

        return $this;
    }

    public function getLoaLoiFile(): ?string
    {
        return $this->loa_loi_file;
    }

    public function setLoaLoiFile(?string $loa_loi_file): self
    {
        $this->loa_loi_file = $loa_loi_file;

        return $this;
    }

    /**
     * Add orgSpecialty
     *
     * @param \App\Entity\OrgSpecialty $orgSpecialty
     *
     * @return Organization
     */
    public function addOrgSpecialty(\App\Entity\OrgSpecialty $orgSpecialty)
    {
        $this->org_specialties[] = $orgSpecialty;

        return $this;
    }

    /**
     * Remove orgSpecialty
     *
     * @param \App\Entity\OrgSpecialty $orgSpecialty
     */
    public function removeOrgSpecialty(\App\Entity\OrgSpecialty $orgSpecialty)
    {
        $this->org_specialties->removeElement($orgSpecialty);
    }

    /**
     * Get orgSpecialties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrgSpecialtiesArray()
    {
        return $this->org_specialties;
    }

    /**
     * Get orgSpecialties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOrgSpecialties()
    {
        return $this->org_specialties;
    }

    /**
     * Add specialtyArea
     *
     * @param \App\Entity\SpecialtyAreas $specialtyArea
     *
     * @return Organization
     */
    public function addSpecialtyArea(\App\Entity\SpecialtyAreas $specialtyArea)
    {
        $this->specialty_areas[] = $specialtyArea;

        return $this;
    }

    /**
     * Remove specialtyArea
     *
     * @param \App\Entity\SpecialtyAreas $specialtyArea
     */
    public function removeSpecialtyArea(\App\Entity\SpecialtyAreas $specialtyArea)
    {
        $this->specialty_areas->removeElement($specialtyArea);
    }

    /**
     * Get specialtyAreas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpecialtyAreas()
    {
        return $this->specialty_areas;
    }

    /**
     * Get specialtyAreas
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpecialtyAreaArray()
    {
        return $this->specialty_areas;
    }

    /**
     * Add accreditation
     *
     * @param \App\Entity\Accreditation $accreditation
     *
     * @return Organization
     */
    public function addAccreditation(\App\Entity\Accreditation $accreditation)
    {
        $this->accreditations[] = $accreditation;

        return $this;
    }

    /**
     * Remove accreditation
     *
     * @param \App\Entity\Accreditation $accreditation
     */
    public function removeAccreditation(\App\Entity\Accreditation $accreditation)
    {
        $this->accreditations->removeElement($accreditation);
    }

    /**
     * Get accreditations
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAccreditations()
    {
        return $this->accreditations;
    }

    /**
     * @return mixed
     */
    public function getBillingNpi()
    {
        return $this->billing_npi;
    }

    /**
     * @param mixed $billing_npi
     */
    public function setBillingNpi($billing_npi): void
    {
        $this->billing_npi = $billing_npi;
    }

    /**
     * @return mixed
     */
    public function getOrganizationStatus()
    {
        return $this->organization_status;
    }

    /**
     * @param mixed $organization_status
     */
    public function setOrganizationStatus($organization_status): void
    {
        $this->organization_status = $organization_status;
    }

    /**
     * @return mixed
     */
    public function getInsertedFrom()
    {
        return $this->inserted_from;
    }

    /**
     * @param mixed $inserted_from
     */
    public function setInsertedFrom($inserted_from): void
    {
        $this->inserted_from = $inserted_from;
    }

    /**
     * @return mixed
     */
    public function getProviderUser()
    {
        return $this->provider_user;
    }

    /**
     * @param mixed $provider_user
     */
    public function setProviderUser($provider_user): void
    {
        $this->provider_user = $provider_user;
    }

    /**
     * @return mixed
     */
    public function getLegalNameTinOwner()
    {
        return $this->legal_name_tin_owner;
    }

    /**
     * @param mixed $legal_name_tin_owner
     */
    public function setLegalNameTinOwner($legal_name_tin_owner): void
    {
        $this->legal_name_tin_owner = $legal_name_tin_owner;
    }

    /**
     * @return mixed
     */
    public function getAmericanSignLanguage()
    {
        return $this->american_sign_language;
    }

    /**
     * @param mixed $american_sign_language
     */
    public function setAmericanSignLanguage($american_sign_language): void
    {
        $this->american_sign_language = $american_sign_language;
    }

    /**
     * @return mixed
     */
    public function getCaqh()
    {
        return $this->caqh;
    }

    /**
     * @param mixed $caqh
     */
    public function setCaqh($caqh): void
    {
        $this->caqh = $caqh;
    }

    /**
     * Add payer
     *
     * @param \App\Entity\Payer $payer
     *
     * @return Organization
     */
    public function addPayer(\App\Entity\Payer $payer)
    {
        $this->payers[] = $payer;

        return $this;
    }

    /**
     * Remove payer
     *
     * @param \App\Entity\Payer $payer
     */
    public function removePayer(\App\Entity\Payer $payer)
    {
        $this->payers->removeElement($payer);
    }

    /**
     * Get payers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPayers()
    {
        return $this->payers;
    }

    /**
     * Add $paymenttier
     *
     * @param \App\Entity\PaymentTier $paymenttier
     *
     * @return Organization
     */
    public function addPaymentTier(\App\Entity\PaymentTier $paymenttier)
    {
        $this->paymenttiers = $paymenttier;

        return $this;
    }

    /**
     * Remove paymenttier
     *
     * @param \App\Entity\PaymentTier $paymenttier
     */
    public function removePaymentTier(\App\Entity\PaymentTier $paymenttier)
    {
        $this->paymenttiers->removeElement($paymenttier);
    }

    /**
     * Get paymenttiers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPaymentTiers()
    {
        return $this->paymenttiers;
    }

    /**
     * @return mixed
     */
    public function getAmendmentFile()
    {
        return $this->amendment_file;
    }

    /**
     * @param mixed $amendment_file
     */
    public function setAmendmentFile($amendment_file): void
    {
        $this->amendment_file = $amendment_file;
    }

    /**
     * @return mixed
     */
    public function getBdaName()
    {
        return $this->bda_name;
    }

    /**
     * @param mixed $bda_name
     */
    public function setBdaName($bda_name): void
    {
        $this->bda_name = $bda_name;
    }

    /**
     * @return mixed
     */
    public function getAddrsIdTrackNumber()
    {
        return $this->addrs_id_track_number;
    }

    /**
     * @param mixed $addrs_id_track_number
     */
    public function setAddrsIdTrackNumber($addrs_id_track_number): void
    {
        $this->addrs_id_track_number = $addrs_id_track_number;
    }

    /**
     * @return mixed
     */
    public function getBillingAddress()
    {
        return $this->billing_address;
    }

    /**
     * @param mixed $billing_address
     */
    public function setBillingAddress($billing_address): void
    {
        $this->billing_address = $billing_address;
    }

    /**
     * @return mixed
     */
    public function getOrganizationTaxonomy()
    {
        return $this->organization_taxonomy;
    }

    /**
     * @param mixed $organization_taxonomy
     */
    public function setOrganizationTaxonomy($organization_taxonomy): void
    {
        $this->organization_taxonomy = $organization_taxonomy;
    }

    /**
     * @return mixed
     */
    public function getReceiveDateAgmt()
    {
        return $this->receiveDateAgmt;
    }

    /**
     * @param mixed $receiveDateAgmt
     */
    public function setReceiveDateAgmt($receiveDateAgmt): void
    {
        $this->receiveDateAgmt = $receiveDateAgmt;
    }

    /**
     * @return mixed
     */
    public function getTerminationReason()
    {
        return $this->termination_reason;
    }

    /**
     * @param mixed $termination_reason
     */
    public function setTerminationReason($termination_reason): void
    {
        $this->termination_reason = $termination_reason;
    }

    /**
     * @return mixed
     */
    public function getDisabledDate()
    {
        return $this->disabled_date;
    }

    /**
     * @param mixed $disabled_date
     */
    public function setDisabledDate($disabled_date): void
    {
        $this->disabled_date = $disabled_date;
    }

    /**
     * @return mixed
     */
    public function getTerminationReasonNote()
    {
        return $this->termination_reason_note;
    }

    /**
     * @param mixed $termination_reason_note
     */
    public function setTerminationReasonNote($termination_reason_note): void
    {
        $this->termination_reason_note = $termination_reason_note;
    }

    /**
     * @return mixed
     */
    public function getBillingNpi2()
    {
        return $this->billing_npi2;
    }

    /**
     * @param mixed $billing_npi2
     */
    public function setBillingNpi2($billing_npi2): void
    {
        $this->billing_npi2 = $billing_npi2;
    }

    /**
     * Add payer_excluded
     *
     * @param \App\Entity\Payer $payer
     *
     * @return Organization
     */
    public function addPayerExclude(\App\Entity\Payer $payer)
    {
        $this->payer_exclude[] = $payer;
        return $this;
    }

    /**
     * Remove payer
     *
     * @param \App\Entity\Payer $payer
     */
    public function removePayerExcluded(\App\Entity\Payer $payer)
    {
        $this->payer_exclude->removeElement($payer);
    }

    /**
     * Get payer_exclude
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPayerExclude()
    {
        return $this->payer_exclude;
    }

    /**
     * @return mixed
     */
    public function getProviderApplicationReceiptDate()
    {
        return $this->provider_application_receipt_date;
    }

    /**
     * @param mixed $provider_application_receipt_date
     */
    public function setProviderApplicationReceiptDate($provider_application_receipt_date): void
    {
        $this->provider_application_receipt_date = $provider_application_receipt_date;
    }

    /**
     * Add taxonomy
     *
     * @param \App\Entity\TaxonomyCode $taxonomy
     *
     * @return Organization
     */
    public function addTaxonomy(\App\Entity\TaxonomyCode $taxonomy)
    {
        $this->taxonomies[] = $taxonomy;

        return $this;
    }

    /**
     * Remove taxonomy
     *
     * @param \App\Entity\TaxonomyCode $taxonomy
     */
    public function removeTaxonomy(\App\Entity\TaxonomyCode $taxonomy)
    {
        $this->taxonomies->removeElement($taxonomy);
    }

    /**
     * Get taxonomies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTaxonomies()
    {
        return $this->taxonomies;
    }

    /**
     * @return mixed
     */
    public function getHasTelemedicine()
    {
        return $this->has_telemedicine;
    }

    /**
     * @param mixed $has_telemedicine
     */
    public function setHasTelemedicine($has_telemedicine): void
    {
        $this->has_telemedicine = $has_telemedicine;
    }

    /**
     * @return mixed
     */
    public function getTelemedicineCompany()
    {
        return $this->telemedicine_company;
    }

    /**
     * @param mixed $telemedicine_company
     */
    public function setTelemedicineCompany($telemedicine_company): void
    {
        $this->telemedicine_company = $telemedicine_company;
    }

    /**
     * @return mixed
     */
    public function getInterestedInTelemedicine()
    {
        return $this->interested_in_telemedicine;
    }

    /**
     * @param mixed $interested_in_telemedicine
     */
    public function setInterestedInTelemedicine($interested_in_telemedicine): void
    {
        $this->interested_in_telemedicine = $interested_in_telemedicine;
    }

    /**
     * @Assert\Callback
     */
    public function validateTINNumber(ExecutionContextInterface $context, $payload)
    {

        $patron = "/^[[:digit:]]+$/";
        if (!preg_match($patron, $this->getTinNumber())) {
            $context->buildViolation('The TIN number only numbers allowed')
                ->atPath('tin_number')
                ->addViolation();
        }
    }

    /**
     * @return mixed
     */
    public function getDocusignLink()
    {
        return $this->docusign_link;
    }

    /**
     * @param mixed $docusign_link
     */
    public function setDocusignLink($docusign_link): void
    {
        $this->docusign_link = $docusign_link;
    }
}
