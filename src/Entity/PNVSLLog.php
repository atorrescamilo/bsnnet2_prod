<?php

namespace App\Entity;

use App\Repository\PNVSLLogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PNVSLLogRepository::class)
 */
class PNVSLLog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $Record_Tracking_Number;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $Provider_Group_Tracking_Number;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $Group_Location_Tracking_Number;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $NPI_Number;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $Start_Date;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $End_Date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Location_Name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Address_Line_1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Address_Line_2;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $City;

    /**
     * @ORM\Column(type="string", length=2, nullable=true)
     */
    private $State;

    /**
     * @ORM\Column(type="string", length=5, nullable=true)
     */
    private $Zip_Code;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $County_Code;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $Phone_Number;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $Is_PCP;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $Accepting_Patients;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $Current_Patients;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $Gender_Accepted;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $Age_Restriction_Low;

    /**
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $Age_Restriction_High;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $Has_After_Hours;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $Has_Weekend_Holiday_Hours;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $Has_Wheelchair_Access;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $Specialties;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Languages;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Taxonomies;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $EHR;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $status;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRecordTrackingNumber(): ?string
    {
        return $this->Record_Tracking_Number;
    }

    public function setRecordTrackingNumber(string $Record_Tracking_Number): self
    {
        $this->Record_Tracking_Number = $Record_Tracking_Number;

        return $this;
    }

    public function getProviderGroupTrackingNumber(): ?string
    {
        return $this->Provider_Group_Tracking_Number;
    }

    public function setProviderGroupTrackingNumber(string $Provider_Group_Tracking_Number): self
    {
        $this->Provider_Group_Tracking_Number = $Provider_Group_Tracking_Number;

        return $this;
    }

    public function getGroupLocationTrackingNumber(): ?string
    {
        return $this->Group_Location_Tracking_Number;
    }

    public function setGroupLocationTrackingNumber(?string $Group_Location_Tracking_Number): self
    {
        $this->Group_Location_Tracking_Number = $Group_Location_Tracking_Number;

        return $this;
    }

    public function getNPINumber(): ?string
    {
        return $this->NPI_Number;
    }

    public function setNPINumber(?string $NPI_Number): self
    {
        $this->NPI_Number = $NPI_Number;

        return $this;
    }

    public function getStartDate(): ?string
    {
        return $this->Start_Date;
    }

    public function setStartDate(?string $Start_Date): self
    {
        $this->Start_Date = $Start_Date;

        return $this;
    }

    public function getEndDate(): ?string
    {
        return $this->End_Date;
    }

    public function setEndDate(?string $End_Date): self
    {
        $this->End_Date = $End_Date;

        return $this;
    }

    public function getLocationName(): ?string
    {
        return $this->Location_Name;
    }

    public function setLocationName(?string $Location_Name): self
    {
        $this->Location_Name = $Location_Name;

        return $this;
    }

    public function getAddressLine1(): ?string
    {
        return $this->Address_Line_1;
    }

    public function setAddressLine1(?string $Address_Line_1): self
    {
        $this->Address_Line_1 = $Address_Line_1;

        return $this;
    }

    public function getAddressLine2(): ?string
    {
        return $this->Address_Line_2;
    }

    public function setAddressLine2(?string $Address_Line_2): self
    {
        $this->Address_Line_2 = $Address_Line_2;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->City;
    }

    public function setCity(?string $City): self
    {
        $this->City = $City;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->State;
    }

    public function setState(?string $State): self
    {
        $this->State = $State;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->Zip_Code;
    }

    public function setZipCode(?string $Zip_Code): self
    {
        $this->Zip_Code = $Zip_Code;

        return $this;
    }

    public function getCountyCode(): ?string
    {
        return $this->County_Code;
    }

    public function setCountyCode(?string $County_Code): self
    {
        $this->County_Code = $County_Code;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->Phone_Number;
    }

    public function setPhoneNumber(?string $Phone_Number): self
    {
        $this->Phone_Number = $Phone_Number;

        return $this;
    }

    public function getIsPCP(): ?string
    {
        return $this->Is_PCP;
    }

    public function setIsPCP(?string $Is_PCP): self
    {
        $this->Is_PCP = $Is_PCP;

        return $this;
    }

    public function getAcceptingPatients(): ?string
    {
        return $this->Accepting_Patients;
    }

    public function setAcceptingPatients(?string $Accepting_Patients): self
    {
        $this->Accepting_Patients = $Accepting_Patients;

        return $this;
    }

    public function getCurrentPatients(): ?string
    {
        return $this->Current_Patients;
    }

    public function setCurrentPatients(?string $Current_Patients): self
    {
        $this->Current_Patients = $Current_Patients;

        return $this;
    }

    public function getGenderAccepted(): ?string
    {
        return $this->Gender_Accepted;
    }

    public function setGenderAccepted(?string $Gender_Accepted): self
    {
        $this->Gender_Accepted = $Gender_Accepted;

        return $this;
    }

    public function getAgeRestrictionLow(): ?string
    {
        return $this->Age_Restriction_Low;
    }

    public function setAgeRestrictionLow(?string $Age_Restriction_Low): self
    {
        $this->Age_Restriction_Low = $Age_Restriction_Low;

        return $this;
    }

    public function getAgeRestrictionHigh(): ?string
    {
        return $this->Age_Restriction_High;
    }

    public function setAgeRestrictionHigh(?string $Age_Restriction_High): self
    {
        $this->Age_Restriction_High = $Age_Restriction_High;

        return $this;
    }

    public function getHasAfterHours(): ?string
    {
        return $this->Has_After_Hours;
    }

    public function setHasAfterHours(?string $Has_After_Hours): self
    {
        $this->Has_After_Hours = $Has_After_Hours;

        return $this;
    }

    public function getHasWeekendHolidayHours(): ?string
    {
        return $this->Has_Weekend_Holiday_Hours;
    }

    public function setHasWeekendHolidayHours(?string $Has_Weekend_Holiday_Hours): self
    {
        $this->Has_Weekend_Holiday_Hours = $Has_Weekend_Holiday_Hours;

        return $this;
    }

    public function getHasWheelchairAccess(): ?string
    {
        return $this->Has_Wheelchair_Access;
    }

    public function setHasWheelchairAccess(?string $Has_Wheelchair_Access): self
    {
        $this->Has_Wheelchair_Access = $Has_Wheelchair_Access;

        return $this;
    }

    public function getSpecialties(): ?string
    {
        return $this->Specialties;
    }

    public function setSpecialties(?string $Specialties): self
    {
        $this->Specialties = $Specialties;

        return $this;
    }

    public function getLanguages(): ?string
    {
        return $this->Languages;
    }

    public function setLanguages(?string $Languages): self
    {
        $this->Languages = $Languages;

        return $this;
    }

    public function getTaxonomies(): ?string
    {
        return $this->Taxonomies;
    }

    public function setTaxonomies(?string $Taxonomies): self
    {
        $this->Taxonomies = $Taxonomies;

        return $this;
    }

    public function getEHR(): ?string
    {
        return $this->EHR;
    }

    public function setEHR(?string $EHR): self
    {
        $this->EHR = $EHR;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
