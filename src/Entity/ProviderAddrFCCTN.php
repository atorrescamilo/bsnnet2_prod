<?php

namespace App\Entity;

use App\Repository\ProviderAddrFCCTNRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProviderAddrFCCTNRepository::class)
 */
class ProviderAddrFCCTN
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Provider", inversedBy="addr_fcc_track")
     * @ORM\JoinColumn(name="provider_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $provider;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\BillingAddress", inversedBy="addr_fcc_track")
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $track_number;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTrackNumber(): ?string
    {
        return $this->track_number;
    }

    public function setTrackNumber(?string $track_number): self
    {
        $this->track_number = $track_number;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param mixed $provider
     */
    public function setProvider($provider): void
    {
        $this->provider = $provider;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }
}
