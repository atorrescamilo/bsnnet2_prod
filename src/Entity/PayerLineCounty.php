<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PayerLineCountyRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class PayerLineCounty
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Payer", inversedBy="payer_line_county")
     * @ORM\JoinColumn(name="payer_id", referencedColumnName="id", nullable=false)
     */
    protected $payer;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\LineOfBusiness", inversedBy="payer_line_county")
     * @ORM\JoinColumn(name="line_id", referencedColumnName="id", nullable=false)
     */
    protected $lineofbusiness;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CountyMatch", inversedBy="payer_line_county")
     * @ORM\JoinColumn(name="county_id", referencedColumnName="id", nullable=false)
     */
    protected $county;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPayer()
    {
        return $this->payer;
    }

    /**
     * @param mixed $payer
     */
    public function setPayer($payer): void
    {
        $this->payer = $payer;
    }

    /**
     * @return mixed
     */
    public function getLineofbusiness()
    {
        return $this->lineofbusiness;
    }

    /**
     * @param mixed $lineofbusiness
     */
    public function setLineofbusiness($lineofbusiness): void
    {
        $this->lineofbusiness = $lineofbusiness;
    }

    /**
     * @return mixed
     */
    public function getCounty()
    {
        return $this->county;
    }

    /**
     * @param mixed $county
     */
    public function setCounty($county): void
    {
        $this->county = $county;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }
}
