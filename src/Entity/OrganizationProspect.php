<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrganizationProspectRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class OrganizationProspect
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $provider_type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $group_npi;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $group_npi2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

        /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $phone_ext;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $billing_phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $faxno;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $hospital_affiliations;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fein;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tin_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name_of_ehr_emr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $website;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $taxonomy_code;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $standardRates;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $nonStandardRates;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_on;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_on;

    /**
     * @OneToMany(targetEntity="App\Entity\OrganizationProspectTicket", mappedBy="organization_prospect", cascade={"remove"})
     */
    private $tickets;

    /**
     * Constructor
     */
    public function __construct()
    {
        if($this->id === null) {
            $this->standardRates = true;
        }

        $this->created_on = new \DateTime();
        $this->updated_on = $this->created_on;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getProviderType(): ?string
    {
        return $this->provider_type;
    }

    public function setProviderType(?string $provider_type): self
    {
        $this->provider_type = $provider_type;

        return $this;
    }

    public function getGroupNpi(): ?string
    {
        return $this->group_npi;
    }

    public function setGroupNpi(?string $group_npi): self
    {
        $this->group_npi = $group_npi;

        return $this;
    }

    public function getGroupNpi2(): ?string
    {
        return $this->group_npi2;
    }

    public function setGroupNpi2(?string $group_npi2): self
    {
        $this->group_npi2 = $group_npi2;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPhoneExt(): ?string
    {
        return $this->phone_ext;
    }

    public function setPhoneExt(?string $phone_ext): self
    {
        $this->phone = $phone_ext;

        return $this;
    }

    public function getBillingPhone(): ?string
    {
        return $this->billing_phone;
    }

    public function setBillingPhone(?string $billing_phone): self
    {
        $this->billing_phone = $billing_phone;

        return $this;
    }

    public function getFaxno(): ?string
    {
        return $this->faxno;
    }

    public function setFaxno(?string $faxno): self
    {
        $this->faxno = $faxno;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getHospitalAffiliations(): ?string
    {
        return $this->hospital_affiliations;
    }

    public function setHospitalAffiliations(?string $hospital_affiliations): self
    {
        $this->hospital_affiliations = $hospital_affiliations;

        return $this;
    }

    public function getFein(): ?string
    {
        return $this->fein;
    }

    public function setFein(?string $fein): self
    {
        $this->fein = $fein;

        return $this;
    }

    public function getTinNumber(): ?string
    {
        return $this->tin_number;
    }

    public function setTinNumber(?string $tin_number): self
    {
        $this->tin_number = $tin_number;

        return $this;
    }

    public function getNameOfEhrEmr(): ?string
    {
        return $this->name_of_ehr_emr;
    }

    public function setNameOfEhrEmr(?string $name_of_ehr_emr): self
    {
        $this->name_of_ehr_emr = $name_of_ehr_emr;

        return $this;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(?string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getTaxonomyCode(): ?string
    {
        return $this->taxonomy_code;
    }

    public function setTaxonomyCode(?string $taxonomy_code): self
    {
        $this->taxonomy_code = $taxonomy_code;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getStandardRates(): ?bool
    {
        return $this->standardRates;
    }

    public function setStandardRates(?bool $standardRates): self
    {
        $this->standardRates = $standardRates;

        return $this;
    }

    public function getNonStandardRates(): ?string
    {
        return $this->nonStandardRates;
    }

    public function setNonStandardRates(?string $nonStandardRates): self
    {
        $this->nonStandardRates = $nonStandardRates;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(?string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreatedOn(): ?\DateTimeInterface
    {
        return $this->created_on;
    }

    public function setCreatedOn(?\DateTimeInterface $created_on): self
    {
        $this->created_on = $created_on;

        return $this;
    }

    public function getUpdatedOn(): ?\DateTimeInterface
    {
        return $this->updated_on;
    }

    public function setUpdatedOn(?\DateTimeInterface $updated_on): self
    {
        $this->updated_on = $updated_on;

        return $this;
    }


}
