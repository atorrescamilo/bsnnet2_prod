<?php

namespace App\Entity;

use App\Repository\FCCRosterLogRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FCCRosterLogRepository::class)
 */
class FCCRosterLog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $id_key;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $prov_title;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $eclaims_provider_type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ahca_provider_type;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $spec1;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $spec2;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $spec3;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $spec4;

    /**
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $npi;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $medicaid_id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state_license;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $lng1;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $lng2;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $lng3;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $lng4;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $lng5;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $lng6;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $lng7;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $lng8;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prac_isr_type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prac_isr;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prac_street;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $prac_city;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $prac_state;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $prac_zipcode;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $prac_phone;

    /**
     * @ORM\Column(type="string", length=1, nullable=true)
     */
    private $ada_comp;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $taxonomy1;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $taxonomy2;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $taxonomy3;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $evening_hrs;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $weekend_hrs;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mon_start;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $mon_end;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tues_start;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tues_end;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $wed_start;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $wed_end;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $thurs_start;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $thurs_end;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fri_start;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $fri_end;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sat_start;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sat_end;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sun_start;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sun_end;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $payto_street;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $payto_city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $payto_state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $payto_zipcode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $payto_phone;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $min_age;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $max_age;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $practice_suite;

    /**
     * @ORM\Column(type="string", length=10, nullable=true)
     */
    private $name_switch_flag;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdKey(): ?string
    {
        return $this->id_key;
    }

    public function setIdKey(?string $id_key): self
    {
        $this->id_key = $id_key;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(?string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(?string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getProvTitle(): ?string
    {
        return $this->prov_title;
    }

    public function setProvTitle(?string $prov_title): self
    {
        $this->prov_title = $prov_title;

        return $this;
    }

    public function getEclaimsProviderType(): ?string
    {
        return $this->eclaims_provider_type;
    }

    public function setEclaimsProviderType(?string $eclaims_provider_type): self
    {
        $this->eclaims_provider_type = $eclaims_provider_type;

        return $this;
    }

    public function getAhcaProviderType(): ?string
    {
        return $this->ahca_provider_type;
    }

    public function setAhcaProviderType(?string $ahca_provider_type): self
    {
        $this->ahca_provider_type = $ahca_provider_type;

        return $this;
    }

    public function getSpec1(): ?string
    {
        return $this->spec1;
    }

    public function setSpec1(?string $spec1): self
    {
        $this->spec1 = $spec1;

        return $this;
    }

    public function getSpec2(): ?string
    {
        return $this->spec2;
    }

    public function setSpec2(?string $spec2): self
    {
        $this->spec2 = $spec2;

        return $this;
    }

    public function getSpec3(): ?string
    {
        return $this->spec3;
    }

    public function setSpec3(?string $spec3): self
    {
        $this->spec3 = $spec3;

        return $this;
    }

    public function getSpec4(): ?string
    {
        return $this->spec4;
    }

    public function setSpec4(?string $spec4): self
    {
        $this->spec4 = $spec4;

        return $this;
    }

    public function getNpi(): ?string
    {
        return $this->npi;
    }

    public function setNpi(?string $npi): self
    {
        $this->npi = $npi;

        return $this;
    }

    public function getMedicaidId(): ?string
    {
        return $this->medicaid_id;
    }

    public function setMedicaidId(?string $medicaid_id): self
    {
        $this->medicaid_id = $medicaid_id;

        return $this;
    }

    public function getStateLicense(): ?string
    {
        return $this->state_license;
    }

    public function setStateLicense(?string $state_license): self
    {
        $this->state_license = $state_license;

        return $this;
    }

    public function getLng1(): ?string
    {
        return $this->lng1;
    }

    public function setLng1(?string $lng1): self
    {
        $this->lng1 = $lng1;

        return $this;
    }

    public function getLng2(): ?string
    {
        return $this->lng2;
    }

    public function setLng2(?string $lng2): self
    {
        $this->lng2 = $lng2;

        return $this;
    }

    public function getLng3(): ?string
    {
        return $this->lng3;
    }

    public function setLng3(?string $lng3): self
    {
        $this->lng3 = $lng3;

        return $this;
    }

    public function getLng4(): ?string
    {
        return $this->lng4;
    }

    public function setLng4(?string $lng4): self
    {
        $this->lng4 = $lng4;

        return $this;
    }

    public function getLng5(): ?string
    {
        return $this->lng5;
    }

    public function setLng5(?string $lng5): self
    {
        $this->lng5 = $lng5;

        return $this;
    }

    public function getLng6(): ?string
    {
        return $this->lng6;
    }

    public function setLng6(?string $lng6): self
    {
        $this->lng6 = $lng6;

        return $this;
    }

    public function getLng7(): ?string
    {
        return $this->lng7;
    }

    public function setLng7(?string $lng7): self
    {
        $this->lng7 = $lng7;

        return $this;
    }

    public function getLng8(): ?string
    {
        return $this->lng8;
    }

    public function setLng8(?string $lng8): self
    {
        $this->lng8 = $lng8;

        return $this;
    }

    public function getPracIsrType(): ?string
    {
        return $this->prac_isr_type;
    }

    public function setPracIsrType(?string $prac_isr_type): self
    {
        $this->prac_isr_type = $prac_isr_type;

        return $this;
    }

    public function getPracIsr(): ?string
    {
        return $this->prac_isr;
    }

    public function setPracIsr(?string $prac_isr): self
    {
        $this->prac_isr = $prac_isr;

        return $this;
    }

    public function getPracStreet(): ?string
    {
        return $this->prac_street;
    }

    public function setPracStreet(?string $prac_street): self
    {
        $this->prac_street = $prac_street;

        return $this;
    }

    public function getPracCity(): ?string
    {
        return $this->prac_city;
    }

    public function setPracCity(?string $prac_city): self
    {
        $this->prac_city = $prac_city;

        return $this;
    }

    public function getPracState(): ?string
    {
        return $this->prac_state;
    }

    public function setPracState(?string $prac_state): self
    {
        $this->prac_state = $prac_state;

        return $this;
    }

    public function getPracZipcode(): ?string
    {
        return $this->prac_zipcode;
    }

    public function setPracZipcode(?string $prac_zipcode): self
    {
        $this->prac_zipcode = $prac_zipcode;

        return $this;
    }

    public function getPracPhone(): ?string
    {
        return $this->prac_phone;
    }

    public function setPracPhone(?string $prac_phone): self
    {
        $this->prac_phone = $prac_phone;

        return $this;
    }

    public function getAdaComp(): ?string
    {
        return $this->ada_comp;
    }

    public function setAdaComp(?string $ada_comp): self
    {
        $this->ada_comp = $ada_comp;

        return $this;
    }

    public function getTaxonomy1(): ?string
    {
        return $this->taxonomy1;
    }

    public function setTaxonomy1(?string $taxonomy1): self
    {
        $this->taxonomy1 = $taxonomy1;

        return $this;
    }

    public function getTaxonomy2(): ?string
    {
        return $this->taxonomy2;
    }

    public function setTaxonomy2(?string $taxonomy2): self
    {
        $this->taxonomy2 = $taxonomy2;

        return $this;
    }

    public function getTaxonomy3(): ?string
    {
        return $this->taxonomy3;
    }

    public function setTaxonomy3(?string $taxonomy3): self
    {
        $this->taxonomy3 = $taxonomy3;

        return $this;
    }

    public function getEveningHrs(): ?string
    {
        return $this->evening_hrs;
    }

    public function setEveningHrs(?string $evening_hrs): self
    {
        $this->evening_hrs = $evening_hrs;

        return $this;
    }

    public function getWeekendHrs(): ?string
    {
        return $this->weekend_hrs;
    }

    public function setWeekendHrs(?string $weekend_hrs): self
    {
        $this->weekend_hrs = $weekend_hrs;

        return $this;
    }

    public function getMonStart(): ?string
    {
        return $this->mon_start;
    }

    public function setMonStart(?string $mon_start): self
    {
        $this->mon_start = $mon_start;

        return $this;
    }

    public function getMonEnd(): ?string
    {
        return $this->mon_end;
    }

    public function setMonEnd(?string $mon_end): self
    {
        $this->mon_end = $mon_end;

        return $this;
    }

    public function getTuesStart(): ?string
    {
        return $this->tues_start;
    }

    public function setTuesStart(?string $tues_start): self
    {
        $this->tues_start = $tues_start;

        return $this;
    }

    public function getTuesEnd(): ?string
    {
        return $this->tues_end;
    }

    public function setTuesEnd(?string $tues_end): self
    {
        $this->tues_end = $tues_end;

        return $this;
    }

    public function getWedStart(): ?string
    {
        return $this->wed_start;
    }

    public function setWedStart(?string $wed_start): self
    {
        $this->wed_start = $wed_start;

        return $this;
    }

    public function getWedEnd(): ?string
    {
        return $this->wed_end;
    }

    public function setWedEnd(?string $wed_end): self
    {
        $this->wed_end = $wed_end;

        return $this;
    }

    public function getThursStart(): ?string
    {
        return $this->thurs_start;
    }

    public function setThursStart(?string $thurs_start): self
    {
        $this->thurs_start = $thurs_start;

        return $this;
    }

    public function getThursEnd(): ?string
    {
        return $this->thurs_end;
    }

    public function setThursEnd(?string $thurs_end): self
    {
        $this->thurs_end = $thurs_end;

        return $this;
    }

    public function getFriStart(): ?string
    {
        return $this->fri_start;
    }

    public function setFriStart(?string $fri_start): self
    {
        $this->fri_start = $fri_start;

        return $this;
    }

    public function getFriEnd(): ?string
    {
        return $this->fri_end;
    }

    public function setFriEnd(?string $fri_end): self
    {
        $this->fri_end = $fri_end;

        return $this;
    }

    public function getSatStart(): ?string
    {
        return $this->sat_start;
    }

    public function setSatStart(?string $sat_start): self
    {
        $this->sat_start = $sat_start;

        return $this;
    }

    public function getSatEnd(): ?string
    {
        return $this->sat_end;
    }

    public function setSatEnd(?string $sat_end): self
    {
        $this->sat_end = $sat_end;

        return $this;
    }

    public function getSunStart(): ?string
    {
        return $this->sun_start;
    }

    public function setSunStart(?string $sun_start): self
    {
        $this->sun_start = $sun_start;

        return $this;
    }

    public function getSunEnd(): ?string
    {
        return $this->sun_end;
    }

    public function setSunEnd(?string $sun_end): self
    {
        $this->sun_end = $sun_end;

        return $this;
    }

    public function getPaytoStreet(): ?string
    {
        return $this->payto_street;
    }

    public function setPaytoStreet(?string $payto_street): self
    {
        $this->payto_street = $payto_street;

        return $this;
    }

    public function getPaytoCity(): ?string
    {
        return $this->payto_city;
    }

    public function setPaytoCity(?string $payto_city): self
    {
        $this->payto_city = $payto_city;

        return $this;
    }

    public function getPaytoState(): ?string
    {
        return $this->payto_state;
    }

    public function setPaytoState(?string $payto_state): self
    {
        $this->payto_state = $payto_state;

        return $this;
    }

    public function getPaytoZipcode(): ?string
    {
        return $this->payto_zipcode;
    }

    public function setPaytoZipcode(?string $payto_zipcode): self
    {
        $this->payto_zipcode = $payto_zipcode;

        return $this;
    }

    public function getPaytoPhone(): ?string
    {
        return $this->payto_phone;
    }

    public function setPaytoPhone(?string $payto_phone): self
    {
        $this->payto_phone = $payto_phone;

        return $this;
    }

    public function getMinAge(): ?string
    {
        return $this->min_age;
    }

    public function setMinAge(?string $min_age): self
    {
        $this->min_age = $min_age;

        return $this;
    }

    public function getMaxAge(): ?string
    {
        return $this->max_age;
    }

    public function setMaxAge(?string $max_age): self
    {
        $this->max_age = $max_age;

        return $this;
    }

    public function getPracticeSuite(): ?string
    {
        return $this->practice_suite;
    }

    public function setPracticeSuite(?string $practice_suite): self
    {
        $this->practice_suite = $practice_suite;

        return $this;
    }

    public function getNameSwitchFlag(): ?string
    {
        return $this->name_switch_flag;
    }

    public function setNameSwitchFlag(?string $name_switch_flag): self
    {
        $this->name_switch_flag = $name_switch_flag;

        return $this;
    }
}
