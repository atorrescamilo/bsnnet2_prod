<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LanguagesRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Languages
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
    */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
    */
    private $name;

    public function __toString() {
        return $this->name;
    }

    public function getDisplayName() {
        return $this->name;
    }

    /**
     * @ORM\Column(type="string", length=255)
    */
    private $code;


    /**
     * @ORM\Column(type="string", length=255, nullable=true)
    */
    private $description;

    /**
     * @ORM\Column(type="integer", nullable=true)
    */
    private $ord;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lang_cd;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOrder(): ?int
    {
        return $this->ord;
    }

    public function setOrder(?int $ord): self
    {
        $this->ord = $ord;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    public function getLangCd(): ?string
    {
        return $this->lang_cd;
    }

    public function setLangCd(?string $lang_cd): self
    {
        $this->lang_cd = $lang_cd;

        return $this;
    }
}
