<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProviderCredentialingRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ProviderCredentialing
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cvo", inversedBy="provider_credentialing")
     * @ORM\JoinColumn(name="cvo_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $cvo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Provider", inversedBy="provider_credentialing")
     * @ORM\JoinColumn(name="provider_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $provider;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization", inversedBy="provider_credentialing")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $organization;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialingDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialingEffectiveDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialingCompleteDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sanctionDate;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialing_accepted;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialing_denied;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $credentialing_appeal;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CredentialingStatus", inversedBy="provider_credentialing")
     * @ORM\JoinColumn(name="credentialing_status_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $credentialing_status;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $npi_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $report_pdf;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id=$id;
        return $this;
    }


    public function getCredentialingDate(): ?string
    {
        return $this->credentialingDate;
    }

    public function setCredentialingDate(?string $credentialingDate): self
    {
        $this->credentialingDate = $credentialingDate;

        return $this;
    }

    public function getCredentialingEffectiveDate(): ?string
    {
        return $this->credentialingEffectiveDate;
    }

    public function setCredentialingEffectiveDate(?string $credentialingEffectiveDate): self
    {
        $this->credentialingEffectiveDate = $credentialingEffectiveDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCvo()
    {
        return $this->cvo;
    }

    /**
     * @param mixed $cvo
     */
    public function setCvo($cvo): void
    {
        $this->cvo = $cvo;
    }

    /**
     * @return mixed
     */
    public function getPayer()
    {
        return $this->payer;
    }

    /**
     * @param mixed $payer
     */
    public function setPayer($payer): void
    {
        $this->payer = $payer;
    }

    /**
     * @return mixed
     */
    public function getCredentialingStatus()
    {
        return $this->credentialing_status;
    }

    /**
     * @param mixed $credentialing_status
     */
    public function setCredentialingStatus($credentialing_status): void
    {
        $this->credentialing_status = $credentialing_status;
    }

    /**
     * @return mixed
     */
    public function getSanctionDate()
    {
        return $this->sanctionDate;
    }

    /**
     * @param mixed $sanctionDate
     */
    public function setSanctionDate($sanctionDate): void
    {
        $this->sanctionDate = $sanctionDate;
    }

    /**
     * @return mixed
     */
    public function getCredentialingAccepted()
    {
        return $this->credentialing_accepted;
    }

    /**
     * @param mixed $credentialing_accepted
     */
    public function setCredentialingAccepted($credentialing_accepted): void
    {
        $this->credentialing_accepted = $credentialing_accepted;
    }

    /**
     * @return mixed
     */
    public function getCredentialingDenied()
    {
        return $this->credentialing_denied;
    }

    /**
     * @param mixed $credentialing_denied
     */
    public function setCredentialingDenied($credentialing_denied): void
    {
        $this->credentialing_denied = $credentialing_denied;
    }

    /**
     * @return mixed
     */
    public function getCredentialingAppeal()
    {
        return $this->credentialing_appeal;
    }

    /**
     * @param mixed $credentialing_appeal
     */
    public function setCredentialingAppeal($credentialing_appeal): void
    {
        $this->credentialing_appeal = $credentialing_appeal;
    }

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param mixed $provider
     */
    public function setProvider($provider): void
    {
        $this->provider = $provider;
    }

    /**
     * @return mixed
     */
    public function getCredentialingCompleteDate()
    {
        return $this->credentialingCompleteDate;
    }

    /**
     * @param mixed $credentialingCompleteDate
     */
    public function setCredentialingCompleteDate($credentialingCompleteDate): void
    {
        $this->credentialingCompleteDate = $credentialingCompleteDate;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    /**
     * @return mixed
     */
    public function getNpiNumber()
    {
        return $this->npi_number;
    }

    /**
     * @param mixed $npi_number
     */
    public function setNpiNumber($npi_number): void
    {
        $this->npi_number = $npi_number;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     */
    public function setOrganization($organization): void
    {
        $this->organization = $organization;
    }

    /**
     * @return mixed
     */
    public function getReportPdf()
    {
        return $this->report_pdf;
    }

    /**
     * @param mixed $report_pdf
     */
    public function setReportPdf($report_pdf): void
    {
        $this->report_pdf = $report_pdf;
    }
}
