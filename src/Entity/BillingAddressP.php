<?php

namespace App\Entity;

use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BillingAddressPRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class BillingAddressP
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $street;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $suite_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $county;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $us_state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zipCode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=512, nullable=true)
     */
    private $google_addr;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $business_hours;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location_npi;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $location_npi2;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $group_medicaid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $group_medicare;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $taxonomy_code;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $primary_addr;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $mailing_addr;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $billing_addr;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $tdd;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $wheelchair;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $private_provider_transportation;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $office_age_limit;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $office_age_limit_max;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_facility;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facility_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facility_exemption_number;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $beds;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone_number;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProviderUser", inversedBy="billing_address_p")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $user;

    /**
     * Billing Address have many facility settings
     * @ManyToMany(targetEntity="App\Entity\FacilityType")
     * @JoinTable(name="billing_address_p_facility_type",
     *      joinColumns={@JoinColumn(name="billing_address_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="facility_type_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $facility_type;

    /**
     * Billing Address have many locations types
     * @ManyToMany(targetEntity="App\Entity\LocationType")
     * @JoinTable(name="billing_address_p_location_type",
     *      joinColumns={@JoinColumn(name="billing_address_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="location_type_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $location_type;

    /**
     * Billing Address have many service settings
     * @ManyToMany(targetEntity="App\Entity\ServiceSettings")
     * @JoinTable(name="billing_address_p_service_settings",
     *      joinColumns={@JoinColumn(name="billing_address_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="service_settings_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $service_settings;

    /**
     * INVERSE SIDE
     *
     * @ManyToMany(targetEntity="App\Entity\ProviderP", mappedBy="billing_address")
     */
    private $providersp;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_on;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tdd_number;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStreet(): ?string
    {
        return $this->street;
    }

    public function setStreet(string $street): self
    {
        $this->street = $street;

        return $this;
    }

    public function getSuiteNumber(): ?string
    {
        return $this->suite_number;
    }

    public function setSuiteNumber(string $suite_number): self
    {
        $this->suite_number = $suite_number;

        return $this;
    }

    public function getCounty(): ?string
    {
        return $this->county;
    }

    public function setCounty(?string $county): self
    {
        $this->county = $county;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getUsState(): ?string
    {
        return $this->us_state;
    }

    public function setUsState(?string $us_state): self
    {
        $this->us_state = $us_state;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zipCode;
    }

    public function setZipCode(?string $zipCode): self
    {
        $this->zipCode = $zipCode;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getGoogleAddr(): ?string
    {
        return $this->google_addr;
    }

    public function setGoogleAddr(?string $google_addr): self
    {
        $this->google_addr = $google_addr;

        return $this;
    }

    public function getBusinessHours(): ?string
    {
        return $this->business_hours;
    }

    public function setBusinessHours(?string $business_hours): self
    {
        $this->business_hours = $business_hours;

        return $this;
    }

    public function getLocationNpi(): ?string
    {
        return $this->location_npi;
    }

    public function setLocationNpi(?string $location_npi): self
    {
        $this->location_npi = $location_npi;

        return $this;
    }

    public function getLocationNpi2(): ?string
    {
        return $this->location_npi2;
    }

    public function setLocationNpi2(?string $location_npi2): self
    {
        $this->location_npi2 = $location_npi2;

        return $this;
    }

    public function getGroupMedicaid(): ?string
    {
        return $this->group_medicaid;
    }

    public function setGroupMedicaid(?string $group_medicaid): self
    {
        $this->group_medicaid = $group_medicaid;

        return $this;
    }

    public function getGroupMedicare(): ?string
    {
        return $this->group_medicare;
    }

    public function setGroupMedicare(?string $group_medicare): self
    {
        $this->group_medicare = $group_medicare;

        return $this;
    }

    public function getTaxonomyCode(): ?string
    {
        return $this->taxonomy_code;
    }

    public function setTaxonomyCode(?string $taxonomy_code): self
    {
        $this->taxonomy_code = $taxonomy_code;

        return $this;
    }

    public function getPrimaryAddr(): ?bool
    {
        return $this->primary_addr;
    }

    public function setPrimaryAddr(?bool $primary_addr): self
    {
        $this->primary_addr = $primary_addr;

        return $this;
    }

    public function getMailingAddr(): ?bool
    {
        return $this->mailing_addr;
    }

    public function setMailingAddr(?bool $mailing_addr): self
    {
        $this->mailing_addr = $mailing_addr;

        return $this;
    }

    public function getBillingAddr(): ?bool
    {
        return $this->billing_addr;
    }

    public function setBillingAddr(?bool $billing_addr): self
    {
        $this->billing_addr = $billing_addr;

        return $this;
    }

    public function getTdd(): ?bool
    {
        return $this->tdd;
    }

    public function setTdd(?bool $tdd): self
    {
        $this->tdd = $tdd;

        return $this;
    }

    public function getWheelchair(): ?bool
    {
        return $this->wheelchair;
    }

    public function setWheelchair(?bool $wheelchair): self
    {
        $this->wheelchair = $wheelchair;

        return $this;
    }

    public function getPrivateProviderTransportation(): ?bool
    {
        return $this->private_provider_transportation;
    }

    public function setPrivateProviderTransportation(?bool $private_provider_transportation): self
    {
        $this->private_provider_transportation = $private_provider_transportation;

        return $this;
    }

    public function getOfficeAgeLimit(): ?int
    {
        return $this->office_age_limit;
    }

    public function setOfficeAgeLimit(?int $office_age_limit): self
    {
        $this->office_age_limit = $office_age_limit;

        return $this;
    }

    public function getOfficeAgeLimitMax(): ?int
    {
        return $this->office_age_limit_max;
    }

    public function setOfficeAgeLimitMax(?int $office_age_limit_max): self
    {
        $this->office_age_limit_max = $office_age_limit_max;

        return $this;
    }

    public function getIsFacility(): ?bool
    {
        return $this->is_facility;
    }

    public function setIsFacility(?bool $is_facility): self
    {
        $this->is_facility = $is_facility;

        return $this;
    }

    public function getFacilityNumber()
    {
        return $this->facility_number;
    }

    public function setFacilityNumber($facility_number)
    {
        $this->facility_number = $facility_number;

        return $this;
    }

    public function getFacilityExemptionNumber()
    {
        return $this->facility_exemption_number;
    }

    public function setFacilityExemptionNumber($facility_exemption_number)
    {
        $this->facility_exemption_number = $facility_exemption_number;

        return $this;
    }

    public function getBeds(): ?int
    {
        return $this->beds;
    }

    public function setBeds(?int $beds): self
    {
        $this->beds = $beds;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(?string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    /**
     * Add facilityType
     *
     * @param \App\Entity\FacilityType $facilityType
     *
     * @return BillingAddress
     */
    public function addFacilityType(\App\Entity\FacilityType $facilityType)
    {
        $this->facility_type[] = $facilityType;

        return $this;
    }

    /**
     * Remove facilityType
     *
     * @param \App\Entity\FacilityType $facilityType
     */
    public function removeFacilityType(\App\Entity\FacilityType $facilityType)
    {
        $this->facility_type->removeElement($facilityType);
    }

    public function clearFacilityTypes() {
        $this->facility_type = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get facilityType
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFacilityType()
    {
        return $this->facility_type;
    }

    /**
     * Add locationType
     *
     * @param \App\Entity\LocationType $locationType
     *
     * @return BillingAddress
     */
    public function addLocationType(\App\Entity\LocationType $locationType)
    {
        $this->location_type[] = $locationType;

        return $this;
    }

    /**
     * Remove locationType
     *
     * @param \App\Entity\LocationType $locationType
     */
    public function removeLocationType(\App\Entity\LocationType $locationType)
    {
        $this->location_type->removeElement($locationType);
    }

    /**
     * Get locationType
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLocationType()
    {
        return $this->location_type;
    }


    /**
     * Add serviceSetting
     *
     * @param \App\Entity\ServiceSettings $serviceSetting
     *
     * @return BillingAddress
     */
    public function addServiceSetting(\App\Entity\ServiceSettings $serviceSetting)
    {
        $this->service_settings[] = $serviceSetting;

        return $this;
    }

    /**
     * Remove serviceSetting
     *
     * @param \App\Entity\ServiceSettings $serviceSetting
     */
    public function removeServiceSetting(\App\Entity\ServiceSettings $serviceSetting)
    {
        $this->service_settings->removeElement($serviceSetting);
    }

    /**
     * Get serviceSettings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getServiceSettings()
    {
        return $this->service_settings;
    }

    public function clearServiceSettings() {
        $this->service_settings = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    public function getCreatedOn(): ?\DateTimeInterface
    {
        return $this->created_on;
    }

    public function setCreatedOn(?\DateTimeInterface $created_on): self
    {
        $this->created_on = $created_on;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreateOn() {
        $this->created_on = new \DateTime();
    }

    public function getTddNumber(): ?string
    {
        return $this->tdd_number;
    }

    public function setTddNumber(?string $tdd_number): self
    {
        $this->tdd_number = $tdd_number;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }
}
