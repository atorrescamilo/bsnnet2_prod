<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use App\Utils\My_Mcript;
use Symfony\Component\DependencyInjection\Container;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProviderPRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ProviderP
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $initial;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $npi_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $date_of_birth;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gender;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $social;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $medicare_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $medicaid_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $caqh;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $taxonomy_code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $date_of_agmt;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state_lic;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $board_certified;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $board_name;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $interesed_in_telemedicine_services;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $alf_and_ltc_facilities;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $accepting_new_patients;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $are_you_pcp;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $gender_acceptance;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $disabled;


    /**
     * Providers can have different General Categories.
     * @ManyToMany(targetEntity="App\Entity\GeneralCategories", cascade={"remove", "persist"})
     * @JoinTable(name="providerp_general_categories",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="general_category_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $general_categories;

    /**
     * provider can have different lineofbusiness.
     * @ORM\ManyToMany(targetEntity="App\Entity\LineOfBusiness", cascade={"remove", "persist"})
     * @ORM\JoinTable(name="providerp_lineofbusiness",
     *      joinColumns={@ORM\JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="lineofbusiness_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $lineOfBusiness;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lic_state;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TerminationReason", inversedBy="providers")
     * @ORM\JoinColumn(name="terminationreason_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $terminationReason;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProviderUser", inversedBy="providers")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $user;

    /**
     * Providers Speaks  many languages.
     * @ManyToMany(targetEntity="App\Entity\Languages", cascade={"remove", "persist"})
     * @JoinTable(name="providerp_languages",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="CASCADE")},
     *      inverseJoinColumns={@JoinColumn(name="languages_id", referencedColumnName="id", onDelete="CASCADE")}
     *      )
     */
    protected $languages;

    /**
     * Providers can have different types.
     * @ManyToMany(targetEntity="App\Entity\ProviderType", cascade={"remove", "persist"})
     * @JoinTable(name="providerp_provider_types",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="provider_type_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $provider_type;

    /**
     * Providers can have different specialty.
     * @ManyToMany(targetEntity="App\Entity\Specialty", cascade={"remove", "persist"})
     * @JoinTable(name="providerp_primary_specialties",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="primary_specialty_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $primary_specialties;

    /**
     * Providers can have different specialty.
     * @ManyToMany(targetEntity="App\Entity\Degree",cascade={"remove", "persist"})
     * @JoinTable(name="providerp_degree",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="degree_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $degree;

    /**
     * OWNING SIDE
     *
     * @ManyToMany(targetEntity="App\Entity\BillingAddressP", inversedBy="providersp")
     * @JoinTable(name="providerp_billing_address",
     *      joinColumns={@JoinColumn(name="provider_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="billing_address_id", referencedColumnName="id", onDelete="Cascade")})
     */
    protected $billing_address;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_on;

    /**
     * ProviderP constructor.
     * @param $id
     */
    public function __construct()
    {
        $this->disabled = false;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getTerminationReason()
    {
        return $this->terminationReason;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getFullname()
    {
        return $this->first_name." ".$this->last_name;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }



    /**
     * @param mixed $terminationReason
     */
    public function setTerminationReason($terminationReason): void
    {
        $this->terminationReason = $terminationReason;
    }



    /**
     * @return mixed
     */
    public function getLicState()
    {
        return $this->lic_state;
    }

    /**
     * @param mixed $lic_state
     */
    public function setLicState($lic_state): void
    {
        $this->lic_state = $lic_state;
    }


    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getInitial(): ?string
    {
        return $this->initial;
    }

    public function setInitial(?string $initial): self
    {
        $this->initial = $initial;

        return $this;
    }

    public function getNpiNumber(): ?string
    {
        return $this->npi_number;
    }

    public function setNpiNumber(?string $npi_number): self
    {
        $this->npi_number = $npi_number;

        return $this;
    }

    public function getDateOfBirth(): ?string
    {
        return $this->date_of_birth;
    }

    public function setDateOfBirth(?string $date_of_birth): self
    {
        $this->date_of_birth = $date_of_birth;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(?string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getSocial(): ?string
    {
        return $this->social;
    }

    public function setSocial(?string $social): self
    {
        $this->social = $social;

        return $this;
    }

    public function getMedicareNumber(): ?string
    {
        return $this->medicare_number;
    }

    public function setMedicareNumber(?string $medicare_number): self
    {
        $this->medicare_number = $medicare_number;

        return $this;
    }

    public function getMedicaidNumber(): ?string
    {
        return $this->medicaid_number;
    }

    public function setMedicaidNumber(?string $medicaid_number): self
    {
        $this->medicaid_number = $medicaid_number;

        return $this;
    }

    public function getCaqh(): ?string
    {
        return $this->caqh;
    }

    public function setCaqh(?string $caqh): self
    {
        $this->caqh = $caqh;

        return $this;
    }

    public function getTaxonomyCode(): ?string
    {
        return $this->taxonomy_code;
    }

    public function setTaxonomyCode(?string $taxonomy_code): self
    {
        $this->taxonomy_code = $taxonomy_code;

        return $this;
    }

    public function getDateOfAgmt(): ?string
    {
        return $this->date_of_agmt;
    }

    public function setDateOfAgmt(?string $date_of_agmt): self
    {
        $this->date_of_agmt = $date_of_agmt;

        return $this;
    }

    public function getStateLic(): ?string
    {
        return $this->state_lic;
    }

    public function setStateLic(?string $state_lic): self
    {
        $this->state_lic = $state_lic;

        return $this;
    }

    public function getBoardCertified(): ?bool
    {
        return $this->board_certified;
    }

    public function setBoardCertified(?bool $board_certified): self
    {
        $this->board_certified = $board_certified;

        return $this;
    }

    public function getBoardName(): ?string
    {
        return $this->board_name;
    }

    public function setBoardName(?string $board_name): self
    {
        $this->board_name = $board_name;

        return $this;
    }

    public function getInteresedInTelemedicineServices(): ?bool
    {
        return $this->interesed_in_telemedicine_services;
    }

    public function setInteresedInTelemedicineServices(?bool $interesed_in_telemedicine_services): self
    {
        $this->interesed_in_telemedicine_services = $interesed_in_telemedicine_services;

        return $this;
    }

    public function getAlfAndLtcFacilities(): ?bool
    {
        return $this->alf_and_ltc_facilities;
    }

    public function setAlfAndLtcFacilities(?bool $alf_and_ltc_facilities): self
    {
        $this->alf_and_ltc_facilities = $alf_and_ltc_facilities;

        return $this;
    }

    public function getAcceptingNewPatients(): ?bool
    {
        return $this->accepting_new_patients;
    }

    public function setAcceptingNewPatients(?bool $accepting_new_patients): self
    {
        $this->accepting_new_patients = $accepting_new_patients;

        return $this;
    }

    public function getAreYouPcp(): ?bool
    {
        return $this->are_you_pcp;
    }

    public function setAreYouPcp(?bool $are_you_pcp): self
    {
        $this->are_you_pcp = $are_you_pcp;

        return $this;
    }

    public function getGenderAcceptance(): ?string
    {
        return $this->gender_acceptance;
    }

    public function setGenderAcceptance(?string $gender_acceptance): self
    {
        $this->gender_acceptance = $gender_acceptance;

        return $this;
    }

    public function getDisabled(): ?bool
    {
        return $this->disabled;
    }

    public function setDisabled(?bool $disabled): self
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Add general_category
     *
     * @param \App\Entity\GeneralCategories $general_category
     *
     * @return Provider
     */
    public function addGeneralCategory(\App\Entity\GeneralCategories $general_category)
    {
        $this->general_categories[] = $general_category;

        return $this;
    }

    /**
     * Remove general_category
     *
     * @param \App\Entity\GeneralCategories $general_category
     */
    public function removeGeneralCategory(\App\Entity\GeneralCategories $general_category)
    {
        $this->general_categories->removeElement($general_category);
    }

    /**
     * Get general_categories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGeneralCategories()
    {
        return $this->general_categories;
    }

    /**
     * Add lineOfBusiness
     *
     * @param \App\Entity\LineOfBusiness $lineOfBusiness
     *
     * @return Provider
     */
    public function addLineOfBusines(\App\Entity\LineOfBusiness $lineOfBusiness)
    {
        $this->lineOfBusiness[] = $lineOfBusiness;

        return $this;
    }

    /**
     * Remove lineOfBusiness
     *
     * @param \App\Entity\LineOfBusiness $lineOfBusiness
     */
    public function removeLineOfBusines(\App\Entity\LineOfBusiness $lineOfBusiness)
    {
        $this->lineOfBusiness->removeElement($lineOfBusiness);
    }

    /**
     * Get lineOfBusiness
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLineOfBusiness()
    {
        return $this->lineOfBusiness;
    }

    /**
     * Add language
     *
     * @param \App\Entity\Languages $language
     *
     * @return Provider
     */
    public function addLanguage(\App\Entity\Languages $language)
    {
        $this->languages[] = $language;

        return $this;
    }

    /**
     * Remove language
     *
     * @param \App\Entity\Languages $language
     */
    public function removeLanguage(\App\Entity\Languages $language)
    {
        $this->languages->removeElement($language);
    }

    /**
     * Get languages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * Add provider_type
     *
     * @param \App\Entity\ProviderType $provider_type
     *
     * @return Provider
     */
    public function addProviderType(\App\Entity\ProviderType $provider_type)
    {
        $this->provider_type[] = $provider_type;

        return $this;
    }

    /**
     * Remove provider_type
     *
     * @param \App\Entity\ProviderType $provider_type
     */
    public function removeProviderType(\App\Entity\ProviderType $provider_type)
    {
        $this->provider_type->removeElement($provider_type);
    }

    /**
     * Get provider_type
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProviderType()
    {
        return $this->provider_type;
    }

    /**
     * Add primarySpecialty
     *
     * @param \App\Entity\Specialty $primarySpecialty
     *
     * @return Provider
     */
    public function addPrimarySpecialty(\App\Entity\Specialty $primarySpecialty)
    {
        $this->primary_specialties[] = $primarySpecialty;

        return $this;
    }

    /**
     * Remove primarySpecialty
     *
     * @param \App\Entity\Specialty $primarySpecialty
     */
    public function removePrimarySpecialty(\App\Entity\Specialty $primarySpecialty)
    {
        $this->primary_specialties->removeElement($primarySpecialty);
    }

    /**
     * Get primarySpecialties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrimarySpecialties()
    {
        return $this->primary_specialties;
    }

    /**
     * Add degree
     *
     * @param \App\Entity\Degree $degree
     *
     * @return Provider
     */
    public function addDegree(\App\Entity\Degree $degree)
    {
        $this->degree[] = $degree;

        return $this;
    }

    /**
     * Remove degree
     *
     * @param \App\Entity\Degree $degree
     */
    public function removeDegree(\App\Entity\Degree $degree)
    {
        $this->degree->removeElement($degree);
    }

    /**
     * Get degree
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDegree()
    {
        return $this->degree;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Add billing_address
     *
     * @param \App\Entity\BillingAddressP $billing_address
     *
     * @return ProviderP
     */
    public function addBillingAddress(\App\Entity\BillingAddressP $billing_address)
    {
        $this->billing_address[] = $billing_address;
        return $this;
    }

    /**
     * Remove billing_address
     *
     * @param \App\Entity\BillingAddressP $billing_address
     */
    public function removeBillingAddress(\App\Entity\BillingAddressP $billing_address)
    {
        $this->billing_address->removeElement($billing_address);
    }

    /**
     * Get billing_address
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBillingAddress()
    {
        return $this->billing_address;
    }

    public function getCreatedOn(): ?\DateTimeInterface
    {
        return $this->created_on;
    }

    public function setCreatedOn(?\DateTimeInterface $created_on): self
    {
        $this->created_on = $created_on;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreateOn() {
        $this->created_on = new \DateTime();
    }
}
