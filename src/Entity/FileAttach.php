<?php

namespace App\Entity;

use App\Repository\FileAttachRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=FileAttachRepository::class)
 */
class FileAttach
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $path;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EmailType", inversedBy="file_attach")
     * @ORM\JoinColumn(name="email_template_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $email_type;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmailType()
    {
        return $this->email_type;
    }

    /**
     * @param mixed $email_type
     */
    public function setEmailType($email_type): void
    {
        $this->email_type = $email_type;
    }
}
