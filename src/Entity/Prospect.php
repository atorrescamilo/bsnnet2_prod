<?php

namespace App\Entity;

use App\Repository\ProspectRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=ProspectRepository::class)
 */
class Prospect
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $dba_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $provider_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $source_type;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $provider_npi;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $contact_name;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $group_npi;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\Email()
     *
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $phone_ext;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $provider_type;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $standard_rates;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $notes;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProspectStatus", inversedBy="prospect")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=true,onDelete="SET NULL")
     */
    protected $status;

    /**
     * OWNING SIDE
     *
     * @ManyToMany(targetEntity="App\Entity\ProspectAddress")
     * @JoinTable(name="propects_addresses",
     *      joinColumns={@JoinColumn(name="prospect_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="address_id", referencedColumnName="id", onDelete="Cascade")})
     */
    protected $addresses;

    /**
     * @OneToMany(targetEntity="App\Entity\ProspectTicket", mappedBy="prospect", cascade={"remove"})
     */
    private $tickets;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\EmailQueued", mappedBy="prospect")
     */
    private $email_queued;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProspectAddress", mappedBy="prospect")
     */
    private $prospect_address;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ProspectContact", mappedBy="prospect")
     */
    private $prospect_contact;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $last_name;

    /**
     * Organization can have many Accreditation
     * @ManyToMany(targetEntity="App\Entity\TaxonomyCode")
     * @JoinTable(name="prospect_taxonomies",
     *      joinColumns={@JoinColumn(name="prospect_id", referencedColumnName="id", onDelete="Cascade")},
     *      inverseJoinColumns={@JoinColumn(name="taxonomy_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
     */
    protected $taxonomies;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->updated_at = $this->created_at;
        $this->taxonomy = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getDbaName()
    {
        return $this->dba_name;
    }

    public function setDbaName($dba_name)
    {
        $this->dba_name = $dba_name;

        return $this;
    }

    public function getGroupNpi(): ?string
    {
        return $this->group_npi;
    }

    public function setGroupNpi(?string $group_npi): self
    {
        $this->group_npi = $group_npi;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getProviderType(): ?string
    {
        return $this->provider_type;
    }

    public function setProviderType(?string $provider_type): self
    {
        $this->provider_type = $provider_type;

        return $this;
    }


    public function getStandardRates(): ?bool
    {
        return $this->standard_rates;
    }

    public function setStandardRates(?bool $standard_rates): self
    {
        $this->standard_rates = $standard_rates;

        return $this;
    }

    public function getNotes(): ?string
    {
        return $this->notes;
    }

    public function setNotes(?string $notes): self
    {
        $this->notes = $notes;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(?\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(?\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * Add address
     *
     * @param \App\Entity\ProspectAddress $address
     *
     * @return Prospect
     */
    public function addAddress(\App\Entity\ProspectAddress $address){
        $this->addresses[] = $address;
        return $this;
    }

    /**
     * Removeaddress
     *
     * @param \App\Entity\ProspectAddress $address
     */
    public function removeAddress(\App\Entity\ProspectAddress $address)
    {
        $this->addresses->removeElement($address);
    }

    /**
     * Get address
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddress()
    {
        return $this->addresses;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getContactName()
    {
        return $this->contact_name;
    }

    /**
     * @param mixed $contact_name
     */
    public function setContactName($contact_name): void
    {
        $this->contact_name = $contact_name;
    }

    public function getNoProviders(): ?int
    {
        return $this->no_providers;
    }

    public function setNoProviders(?int $no_providers): self
    {
        $this->no_providers = $no_providers;

        return $this;
    }

    public function getProviderEmail(): ?string
    {
        return $this->provider_email;
    }

    public function setProviderEmail(?string $provider_email): self
    {
        $this->provider_email = $provider_email;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProviderName()
    {
        return $this->provider_name;
    }

    /**
     * @param mixed $provider_name
     */
    public function setProviderName($provider_name): void
    {
        $this->provider_name = $provider_name;
    }

    /**
     * @return mixed
     */
    public function getProviderNpi()
    {
        return $this->provider_npi;
    }

    /**
     * @param mixed $provider_npi
     */
    public function setProviderNpi($provider_npi): void
    {
        $this->provider_npi = $provider_npi;
    }

    /**
     * @return mixed
     */
    public function getSourceType()
    {
        return $this->source_type;
    }

    /**
     * @param mixed $source_type
     */
    public function setSourceType($source_type): void
    {
        $this->source_type = $source_type;
    }

    public function getPhoneExt(): ?string
    {
        return $this->phone_ext;
    }

    public function setPhoneExt(?string $phone_ext): self
    {
        $this->phone_ext = $phone_ext;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    /**
     * Add taxonomy
     *
     * @param \App\Entity\TaxonomyCode $taxonomy
     *
     * @return Prospect
     */
    public function addTaxonomy(\App\Entity\TaxonomyCode $taxonomy)
    {
        $this->taxonomies[] = $taxonomy;

        return $this;
    }

    /**
     * Remove taxonomy
     *
     * @param \App\Entity\TaxonomyCode $taxonomy
     */
    public function removeTaxonomy(\App\Entity\TaxonomyCode $taxonomy)
    {
        $this->taxonomies->removeElement($taxonomy);
    }

    /**
     * Get taxonomies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTaxonomies()
    {
        return $this->taxonomies;
    }

}
