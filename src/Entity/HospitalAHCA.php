<?php

namespace App\Entity;

use App\Repository\HospitalAHCARepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=HospitalAHCARepository::class)
 */
class HospitalAHCA
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $ahca_number;

    public function __toString() {
        return $this->ahca_number;
    }

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $licence_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $licence_effective_date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $street_address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $suite_number;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zipcode;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $county;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $web;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAhcaNumber(): ?string
    {
        return $this->ahca_number;
    }

    public function setAhcaNumber(string $ahca_number): self
    {
        $this->ahca_number = $ahca_number;

        return $this;
    }

    public function getLicenceNumber(): ?string
    {
        return $this->licence_number;
    }

    public function setLicenceNumber(?string $licence_number): self
    {
        $this->licence_number = $licence_number;

        return $this;
    }

    public function getLicenceEffectiveDate(): ?string
    {
        return $this->licence_effective_date;
    }

    public function setLicenceEffectiveDate(?string $licence_effective_date): self
    {
        $this->licence_effective_date = $licence_effective_date;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self{
        $this->name = $name;

        return $this;
    }

    public function getStreetAddress(): ?string
    {
        return $this->street_address;
    }

    public function setStreetAddress(?string $street_address): self
    {
        $this->street_address = $street_address;

        return $this;
    }

    public function getSuiteNumber(): ?string
    {
        return $this->suite_number;
    }

    public function setSuiteNumber(?string $suite_number): self
    {
        $this->suite_number = $suite_number;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    public function setZipcode(?string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    public function getCounty(): ?string
    {
        return $this->county;
    }

    public function setCounty(?string $county): self
    {
        $this->county = $county;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getWeb(): ?string
    {
        return $this->web;
    }

    public function setWeb(?string $web): self
    {
        $this->web = $web;

        return $this;
    }
}
