<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmailQueuedRepository")
 * @ORM\HasLifecycleCallbacks()
*/
class EmailQueued
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subject;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $health_plan;

    /**
     * @ORM\Column(type="text",  nullable=true)
     */
    private $providers;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sent_to;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $date_to_send;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $verification_hass;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization", inversedBy="email_queued")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $organization;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EmailStatus", inversedBy="email_queued")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EmailType", inversedBy="email_queued")
     * @ORM\JoinColumn(name="email_type_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $email_type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrganizationContact", inversedBy="email_queued")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $contact;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Prospect", inversedBy="email_queued")
     * @ORM\JoinColumn(name="prospect_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $prospect;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $approved_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $organization_name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(?string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    public function getSentTo(): ?string
    {
        return $this->sent_to;
    }

    public function setSentTo(?string $sent_to): self
    {
        $this->sent_to = $sent_to;

        return $this;
    }

    public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreated(): self
    {
        $this->created = new \DateTime();
        return $this;
    }

    public function getDateToSend(): ?\DateTimeInterface
    {
        return $this->date_to_send;
    }

    public function setDateToSend(?\DateTimeInterface $date_to_send): self
    {
        $this->date_to_send = $date_to_send;

        return $this;
    }

    public function getVerificationHass(): ?string
    {
        return $this->verification_hass;
    }

    /**
     * @ORM\PrePersist
    */
    public function setVerificationHass(): self
    {
        $this->verification_hass = uniqid('vh_',true);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getEmailType()
    {
        return $this->email_type;
    }

    /**
     * @param mixed $email_type
     */
    public function setEmailType($email_type): void
    {
        $this->email_type = $email_type;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     */
    public function setOrganization($organization): void
    {
        $this->organization = $organization;
    }

    /**
     * @return mixed
     */
    public function getProviders()
    {
        return $this->providers;
    }

    /**
     * @param mixed $providers
     */
    public function setProviders($providers): void
    {
        $this->providers = $providers;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getApprovedDate()
    {
        return $this->approved_date;
    }

    /**
     * @param mixed $approved_date
     */
    public function setApprovedDate($approved_date): void
    {
        $this->approved_date = $approved_date;
    }

    /**
     * @return mixed
     */
    public function getProspect()
    {
        return $this->prospect;
    }

    /**
     * @param mixed $prospect
     */
    public function setProspect($prospect): void
    {
        $this->prospect = $prospect;
    }

    public function getOrganizationName(): ?string
    {
        return $this->organization_name;
    }

    public function setOrganizationName(?string $organization_name): self
    {
        $this->organization_name = $organization_name;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getHealthPlan()
    {
        return $this->health_plan;
    }

    /**
     * @param mixed $health_plan
     */
    public function setHealthPlan($health_plan): void
    {
        $this->health_plan = $health_plan;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }
}
