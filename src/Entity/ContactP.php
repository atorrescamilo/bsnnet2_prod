<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ContactPRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ContactP
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $phone_ext;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $mobile;

    /**
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $fax;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $main;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $zip_code;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProviderUser", inversedBy="contact_p")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ContactTitle", inversedBy="organization_contactp")
     * @ORM\JoinColumn(name="contact_title_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $title;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_on;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_on;

    /**
     * Providers can have different specialty.
     * @ManyToMany(targetEntity="App\Entity\ContactPBestChoice")
     * @JoinTable(name="contactp_best_option_choice",
     *      joinColumns={@JoinColumn(name="contact_id", referencedColumnName="id", onDelete="cascade")},
     *      inverseJoinColumns={@JoinColumn(name="best_choice_id", referencedColumnName="id", onDelete="Cascade")}
     *      )
    */
    protected $best_choices;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPhoneExt(): ?string
    {
        return $this->phone_ext;
    }

    public function setPhoneExt(?string $phone_ext): self
    {
        $this->phone_ext = $phone_ext;

        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->mobile;
    }

    public function setMobile(?string $mobile): self
    {
        $this->mobile = $mobile;

        return $this;
    }

    public function getFax(): ?string
    {
        return $this->fax;
    }

    public function setFax(?string $fax): self
    {
        $this->fax = $fax;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getMain(): ?bool
    {
        return $this->main;
    }

    public function setMain(?bool $main): self
    {
        $this->main = $main;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->Address;
    }

    public function setAddress(?string $Address): self
    {
        $this->Address = $Address;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(?string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getZipCode(): ?string
    {
        return $this->zip_code;
    }

    public function setZipCode(?string $zip_code): self
    {
        $this->zip_code = $zip_code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }

    public function getCreatedOn(): ?\DateTimeInterface
    {
        return $this->created_on;
    }

    public function setCreatedOn(?\DateTimeInterface $created_on): self
    {
        $this->created_on = $created_on;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreateOn() {
        $this->created_on = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedOn()
    {
        return $this->updated_on;
    }

    /**
     * @param mixed $updated_on
     */
    public function setUpdatedOn($updated_on): void
    {
        $this->updated_on = $updated_on;
    }

    /**
     * Add best_choice
     *
     * @param \App\Entity\ContactPBestChoice $best_choice
     *
     * @return ContactP
     */
    public function addBestChoice(\App\Entity\ContactPBestChoice $best_choice)
    {
        $this->best_choices[] = $best_choice;

        return $this;
    }

    /**
     * Remove best_choice
     *
     * @param \App\Entity\ContactPBestChoice $best_choice
    */
    public function removeBestChoice(\App\Entity\ContactPBestChoice $best_choice){
        $this->best_choices->removeElement($best_choice);
    }

    /**
     * Get best_choice
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBestChoices()
    {
        return $this->best_choices;
    }

}
