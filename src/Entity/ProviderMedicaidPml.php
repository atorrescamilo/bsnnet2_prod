<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\ManyToMany;
use Doctrine\ORM\Mapping\JoinTable;
use Doctrine\ORM\Mapping\JoinColumn;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProviderMedicaidPmlRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ProviderMedicaidPml
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $florida_medicaid;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $taxonomy_code;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address1;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address2;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Provider", inversedBy="medicaid_pml")
     * @ORM\JoinColumn(name="provider_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $provider;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ProviderType", inversedBy="medicaid_pml")
     * @ORM\JoinColumn(name="provider_type_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $provider_type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Specialty", inversedBy="medicaid_pml")
     * @ORM\JoinColumn(name="specialty_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $primary_specialties;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFloridaMedicaid(): ?string
    {
        return $this->florida_medicaid;
    }

    public function setFloridaMedicaid(?string $florida_medicaid): self
    {
        $this->florida_medicaid = $florida_medicaid;

        return $this;
    }

    public function getTaxonomyCode(): ?string
    {
        return $this->taxonomy_code;
    }

    public function setTaxonomyCode(?string $taxonomy_code): self
    {
        $this->taxonomy_code = $taxonomy_code;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * @param mixed $provider
     */
    public function setProvider($provider): void
    {
        $this->provider = $provider;
    }

    /**
     * @return mixed
     */
    public function getProviderType()
    {
        return $this->provider_type;
    }

    /**
     * @param mixed $provider_type
     */
    public function setProviderType($provider_type): void
    {
        $this->provider_type = $provider_type;
    }

    /**
     * @return mixed
     */
    public function getPrimarySpecialties()
    {
        return $this->primary_specialties;
    }

    /**
     * @param mixed $primary_specialties
     */
    public function setPrimarySpecialties($primary_specialties): void
    {
        $this->primary_specialties = $primary_specialties;
    }

    /**
     * @return mixed
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * @param mixed $address1
     */
    public function setAddress1($address1): void
    {
        $this->address1 = $address1;
    }

    /**
     * @return mixed
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * @param mixed $address2
     */
    public function setAddress2($address2): void
    {
        $this->address2 = $address2;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
    */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

}
