<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmailLogRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class EmailLog
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_date;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $providers;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sent_to;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $verification_hass;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization", inversedBy="email_log")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $organization;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EmailStatus", inversedBy="email_log")
     * @ORM\JoinColumn(name="status_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\EmailType", inversedBy="email_log")
     * @ORM\JoinColumn(name="email_type_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $email_type;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrganizationContact", inversedBy="email_log")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $contact;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $approved_date;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->created_date;
    }

    /**
     * @ORM\PrePersist
    */
    public function setCreatedDate(): self
    {
        $this->created_date = new \DateTime();
        return $this;
    }

    public function getSentTo(): ?string
    {
        return $this->sent_to;
    }

    public function setSentTo(?string $sent_to): self
    {
        $this->sent_to = $sent_to;

        return $this;
    }

    public function getVerificationHass(): ?string
    {
        return $this->verification_hass;
    }

    public function setVerificationHass(string $verification_hass): self
    {
        $this->verification_hass = $verification_hass;

        return $this;
    }

    public function getIsReaded(): ?bool
    {
        return $this->is_readed;
    }

    public function setIsReaded(?bool $is_readed): self
    {
        $this->is_readed = $is_readed;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getEmailType()
    {
        return $this->email_type;
    }

    /**
     * @param mixed $email_type
     */
    public function setEmailType($email_type): void
    {
        $this->email_type = $email_type;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     */
    public function setOrganization($organization): void
    {
        $this->organization = $organization;
    }

    /**
     * @return mixed
     */
    public function getProviders()
    {
        return $this->providers;
    }

    /**
     * @param mixed $providers
     */
    public function setProviders($providers): void
    {
        $this->providers = $providers;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAt(): void
    {
        $this->created_at = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updated_at;
    }

    /**
     * @param mixed $updated_at
     */
    public function setUpdatedAt($updated_at): void
    {
        $this->updated_at = $updated_at;
    }

    public function getApprovedDate(): ?string
    {
        return $this->approved_date;
    }

    public function setApprovedDate(?string $approved_date): self
    {
        $this->approved_date = $approved_date;

        return $this;
    }

}
