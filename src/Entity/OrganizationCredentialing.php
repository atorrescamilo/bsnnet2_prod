<?php

namespace App\Entity;

use App\Repository\OrganizationCredentialingRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=OrganizationCredentialingRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class OrganizationCredentialing
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $credentialing_date;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $credentialing_received_date;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $credentialing_accepted_date;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $credentialing_complete_date;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $sanction_date;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $credentialing_denied_date;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $credentialing_effective_date;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\Column(type="datetime")
     */
    private $updated_at;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Cvo", inversedBy="organization_credentialing")
     * @ORM\JoinColumn(name="cvo_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $cvo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\CredentialingStatus", inversedBy="organization_credentialing")
     * @ORM\JoinColumn(name="credentialing_status_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $credentialing_status;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Organization", inversedBy="organization_credentialing")
     * @ORM\JoinColumn(name="organization_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $organization;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCredentialingDate(): ?string
    {
        return $this->credentialing_date;
    }

    public function setCredentialingDate(?string $credentialing_date): self
    {
        $this->credentialing_date = $credentialing_date;

        return $this;
    }

    public function getCredentialingReceivedDate(): ?string
    {
        return $this->credentialing_received_date;
    }

    public function setCredentialingReceivedDate(?string $credentialing_received_date): self
    {
        $this->credentialing_received_date = $credentialing_received_date;

        return $this;
    }

    public function getCredentialingAcceptedDate(): ?string
    {
        return $this->credentialing_accepted_date;
    }

    public function setCredentialingAcceptedDate(?string $credentialing_accepted_date): self
    {
        $this->credentialing_accepted_date = $credentialing_accepted_date;

        return $this;
    }

    public function getCredentialingCompleteDate(): ?string
    {
        return $this->credentialing_complete_date;
    }

    public function setCredentialingCompleteDate(?string $credentialing_complete_date): self
    {
        $this->credentialing_complete_date = $credentialing_complete_date;

        return $this;
    }

    public function getSanctionDate(): ?string
    {
        return $this->sanction_date;
    }

    public function setSanctionDate(?string $sanction_date): self
    {
        $this->sanction_date = $sanction_date;

        return $this;
    }

    public function getCredentialingDeniedDate(): ?string
    {
        return $this->credentialing_denied_date;
    }

    public function setCredentialingDeniedDate(?string $credentialing_denied_date): self
    {
        $this->credentialing_denied_date = $credentialing_denied_date;

        return $this;
    }

    public function getCredentialingEffectiveDate(): ?string
    {
        return $this->credentialing_effective_date;
    }

    public function setCredentialingEffectiveDate(?string $credentialing_effective_date): self
    {
        $this->credentialing_effective_date = $credentialing_effective_date;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    /**
     * @ORM\PrePersist
    */
    public function setCreatedAt(){
        $this->created_at = new \DateTime();
        $this->updated_at= new \DateTime();
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }


    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCvo()
    {
        return $this->cvo;
    }

    /**
     * @param mixed $cvo
     */
    public function setCvo($cvo): void
    {
        $this->cvo = $cvo;
    }

    /**
     * @return mixed
     */
    public function getCredentialingStatus()
    {
        return $this->credentialing_status;
    }

    /**
     * @param mixed $credentialing_status
     */
    public function setCredentialingStatus($credentialing_status): void
    {
        $this->credentialing_status = $credentialing_status;
    }

    /**
     * @return mixed
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @param mixed $organization
     */
    public function setOrganization($organization): void
    {
        $this->organization = $organization;
    }
}
