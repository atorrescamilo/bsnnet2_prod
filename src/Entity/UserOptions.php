<?php

namespace App\Entity;

use App\Repository\UserOptionsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=UserOptionsRepository::class)
 */
class UserOptions
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $email_notification;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="user_options")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $user;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmailNotification(): ?bool
    {
        return $this->email_notification;
    }

    public function setEmailNotification(?bool $email_notification): self
    {
        $this->email_notification = $email_notification;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }
}
