<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\OrganizationProspectTicketRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class OrganizationProspectTicket
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ticket_date;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $created_on;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updated_on;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\OrganizationProspect", inversedBy="tickets")
     * @ORM\JoinColumn(name="organization_prospect_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $organization_prospect;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $created_by;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->created_on = new \DateTime();
        $this->updated_on = $this->created_on;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTicketDate(): ?string
    {
        return $this->ticket_date;
    }

    public function setTicketDate(?string $ticket_date): self
    {
        $this->ticket_date = $ticket_date;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCreatedOn(): ?\DateTimeInterface
    {
        return $this->created_on;
    }

    public function setCreatedOn(?\DateTimeInterface $created_on): self
    {
        $this->created_on = $created_on;

        return $this;
    }

    public function getUpdatedOn(): ?\DateTimeInterface
    {
        return $this->updated_on;
    }

    public function setUpdatedOn(?\DateTimeInterface $updated_on): self
    {
        $this->updated_on = $updated_on;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getOrganizationProspect()
    {
        return $this->organization_prospect;
    }

    /**
     * @param mixed $organization_prospect
     */
    public function setOrganizationProspect($organization_prospect): void
    {
        $this->organization_prospect = $organization_prospect;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->created_by;
    }

    /**
     * @param mixed $created_by
     */
    public function setCreatedBy($created_by): void
    {
        $this->created_by = $created_by;
    }

}
